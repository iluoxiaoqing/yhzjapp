webpackJsonp([86],{

/***/ 1226:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_sanjiao_hui_bottom.png?v=70626266";

/***/ }),

/***/ 1376:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _vant = __webpack_require__(72);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Vue.use(Popup)
// Vue.use(DatetimePicker);
// import datePicker from "@/libs/datePicker.js";
// Vue.use();

exports.default = {
    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
        // datePicker
    },
    data: function data() {
        return {
            incomePayArray: [{ "select": false, "content": "刷卡", "detailCode": "SHUA_KA" }, { "select": false, "content": "扫码", "detailCode": "SAO_MA" }, { "select": false, "content": "云闪付", "detailCode": "YUN_SHAN_FU" }, { "select": false, "content": "快捷", "detailCode": "KUAI_JIE" }],
            showMenu: false,
            showIncomePay: false,
            currentTime: "", // 开始时间不能超过当前时间
            startTime: "", // 开始时间
            endTime: "", // 结束时间
            start: "",
            end: "",
            datePicker: '', // 用于判断哪个选择器的显示与隐藏
            isPopShow: false, // 弹出层隐藏与显示
            total: "",
            transType: "",
            detailArr: [],
            currentPage: 1,
            isNoData: false,
            maxDate: new Date()

        };
    },
    mounted: function mounted() {
        var bodyHeight = document.documentElement.clientHeight;
        var scroller = document.getElementById("pull-wrapper");
        var scrollerTop = scroller.getBoundingClientRect().top;
        scroller.style.height = bodyHeight - scrollerTop + "px";
        var nowDate = new Date();
        var year = nowDate.getFullYear();
        var month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;
        var day = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
        var dateStr = year + "-" + month + "-" + day;
        this.startTime = dateStr;
        this.$refs.startTime.value = dateStr;
        console.log(this.startTime);
        this.endTime = dateStr;
        this.$refs.endTime.value = dateStr;
        this.getTotal();
        this.getList();
    },

    computed: {
        minDate: function minDate() {
            var curDate = new Date().getTime();
            console.log("curDate=====", curDate);
            var one = 90 * 24 * 3600 * 1000;
            var oneYear = curDate - one;
            console.log("oneYear===", oneYear);
            return new Date(oneYear);
        }
    },
    // components: {
    //     // scrollView,
    //     freshToLoadmore,
    //     noData
    //     // rebateList,
    // },
    methods: {
        goChoseDia: function goChoseDia() {
            this.showMenu = !this.showMenu;
            this.showIncomePay = true;
        },
        checkItem: function checkItem(i, index, detailCode) {
            this.incomePayArray.map(function (el) {
                el.select = false;
            });
            this.transType = detailCode;
            i.select = true;
        },
        showDatePicker: function showDatePicker(picker) {
            //弹出层并显示时间选择器
            document.activeElement.blur();
            this.isPopShow = true;
            this.datePicker = picker;
        },
        cancelPicker: function cancelPicker() {
            // 选择器取消按钮点击事件
            this.isPopShow = false;
            this.datePicker = "";
        },
        confirmPicker: function confirmPicker(value) {
            // 确定按钮，时间格式化并显示在页面上
            console.log("---", value);
            var date = value;
            var m = date.getMonth() + 1;
            var d = date.getDate();
            if (m >= 1 && m <= 9) {
                m = "0" + m;
            }
            if (d >= 0 && d <= 9) {
                d = "0" + d;
            }
            var timer = date.getFullYear() + "-" + m + "-" + d;
            this.$refs[this.datePicker].value = timer;
            if (this.datePicker == 'startTime') {
                this.startTime = timer;
            } else if (this.datePicker == 'endTime') {
                this.endTime = timer;
            }
            this.isPopShow = false;
            this.datePicker = "";
            console.log("start", this.startTime);
            console.log("end", this.endTime);
            this.getTotal();
            this.getList();
        },
        formatter: function formatter(type, value) {
            // 格式化选择器日期
            if (type === "year") {
                return value + "\u5E74";
            } else if (type === "month") {
                return value + "\u6708";
            }
            return value;
        },
        again: function again() {
            this.currentPage = 1;
            this.detailArr = [];
            for (var i = 0; i < this.incomePayArray.length; i++) {
                this.incomePayArray[i].select = false;
            }
            this.incomePayArray[0].select = true;
            this.transType = this.incomePayArray[0].detailCode;
        },
        confirm: function confirm() {
            this.currentPage = 1;
            this.detailArr = [];
            this.showMenu = false;
            this.getTotal();
            this.getList();
            // this.again();
        },
        getTotal: function getTotal(i) {
            var _this = this;

            if (i == 1) {
                this.startTime = '';
                this.endTime = "";
            }
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "achHomepage/findCustTotalTrans",
                // "url": "https://easydoc.xyz/mock/5KvLR0ek/p/77891835/C9AP8nlH",
                "type": "post",
                "data": {
                    "customerNo": this.$route.query.customerNo,
                    "productCode": this.$route.query.productCode,
                    "startTime": this.startTime,
                    "endTime": this.endTime,
                    "transType": this.transType
                }
            }, function (data) {
                console.log(data);
                _this.total = data.data;
                // this.startTime = new Date();
                // this.endTime = new Date();
            });
        },
        getList: function getList(i) {
            var _this2 = this;

            if (i == 1) {
                this.startTime = '';
                this.endTime = "";
            }
            console.log(this.startTime);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "achHomepage/findCustTransOrder",
                // "url": "https://easydoc.xyz/mock/5KvLR0ek/p/77891835/1swoEBnA",
                "type": "post",
                "data": {
                    "customerNo": this.$route.query.customerNo,
                    "productCode": this.$route.query.productCode,
                    "startTime": this.startTime,
                    "endTime": this.endTime,
                    "transType": this.transType,
                    "page": this.currentPage
                }
            }, function (data) {
                console.log(data);
                if (data.data.object.length > 0) {
                    if (_this2.currentPage == 1) {
                        _this2.detailArr = [];
                    }
                    data.data.object.map(function (el) {
                        _this2.$set(el, "show", true);
                        _this2.detailArr.push(el);
                    });

                    _this2.isNoData = false;
                    _this2.currentPage++;
                } else {
                    if (_this2.currentPage == 1) {
                        _this2.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
                // this.startTime = new Date();
                // this.endTime = new Date();
            });
        },
        infinite: function infinite() {
            this.getList();
        },
        refresh: function refresh() {
            this.currentPage = 1;
            this.detailArr = [];
            this.getList();
        }
    }

};

/***/ }),

/***/ 1486:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1639:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_sanjiao_hui.png?v=6c00757f";

/***/ }),

/***/ 1800:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "real_main"
  }, [_c('div', {
    staticClass: "real_time"
  }, [_c('div', {
    staticClass: "text"
  }, [_vm._v("\n\t\t\t\t选择交易时间\n\t\t\t")]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }],
    ref: "startTime",
    attrs: {
      "type": "text",
      "placeholder": "开始时间"
    },
    on: {
      "click": function($event) {
        return _vm.showDatePicker('startTime')
      }
    }
  }), _vm._v(" "), _c('span', [_vm._v("-")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }],
    ref: "endTime",
    attrs: {
      "type": "text",
      "placeholder": "结束时间"
    },
    on: {
      "click": function($event) {
        return _vm.showDatePicker('endTime')
      }
    }
  })]), _vm._v(" "), _c('van-popup', {
    attrs: {
      "position": "bottom"
    },
    model: {
      value: (_vm.isPopShow),
      callback: function($$v) {
        _vm.isPopShow = $$v
      },
      expression: "isPopShow"
    }
  }, [(_vm.datePicker == 'startTime') ? _c('van-datetime-picker', {
    attrs: {
      "min-date": _vm.minDate,
      "max-date": this.maxDate,
      "type": "date",
      "formatter": _vm.formatter
    },
    on: {
      "cancel": _vm.cancelPicker,
      "confirm": _vm.confirmPicker
    },
    model: {
      value: (_vm.startTime),
      callback: function($$v) {
        _vm.startTime = $$v
      },
      expression: "startTime"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.datePicker == 'endTime') ? _c('van-datetime-picker', {
    attrs: {
      "min-date": this.minDate,
      "max-date": _vm.maxDate,
      "type": "date",
      "formatter": _vm.formatter
    },
    on: {
      "cancel": _vm.cancelPicker,
      "confirm": _vm.confirmPicker
    },
    model: {
      value: (_vm.endTime),
      callback: function($$v) {
        _vm.endTime = $$v
      },
      expression: "endTime"
    }
  }) : _vm._e()], 1), _vm._v(" "), _c('div', {
    staticClass: "detail"
  }, [_c('span', [_vm._v("总计交易：" + _vm._s(_vm.total) + "元")]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.goChoseDia()
      }
    }
  }, [_vm._v("交易类型\n\t\t\t\t\t"), (_vm.showMenu == false) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1226),
      "alt": ""
    }
  }) : _c('img', {
    attrs: {
      "src": __webpack_require__(1639),
      "alt": ""
    }
  })])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showMenu),
      expression: "showMenu"
    }],
    staticClass: "budgetDetail_menu_main"
  }, [_c('div', {
    staticClass: "budgetDetail_menu"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showIncomePay),
      expression: "showIncomePay"
    }],
    staticClass: "item"
  }, [_c('span', [_vm._v("交易类型")]), _vm._v(" "), _c('ul', _vm._l((_vm.incomePayArray), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.select
      },
      on: {
        "click": function($event) {
          return _vm.checkItem(i, index, i.detailCode)
        }
      }
    }, [_vm._v(_vm._s(i.content))])
  }), 0)])]), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "again",
    on: {
      "click": function($event) {
        return _vm.again()
      }
    }
  }, [_vm._v("重置")]), _vm._v(" "), _c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("确定")])])])], 1), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "item_main"
  }, _vm._l((_vm.detailArr), function(i, index) {
    return _c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "item_top"
    }, [_c('div', {
      staticClass: "intro_left"
    }, [_c('p', [_vm._v("\n\t\t\t\t\t\t\t交易金额（元）\n\t\t\t\t\t\t")]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.transAmount))])]), _vm._v(" "), _c('div', {
      staticClass: "intro_left"
    }, [_c('p', [_vm._v("\n\t\t\t\t\t\t\t交易类型\n\t\t\t\t\t\t")]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(i.cardType) + "\n\t\t\t\t\t\t")])])]), _vm._v(" "), _c('div', {
      staticClass: "item_bottom"
    }, [_c('p', [_vm._v("订单编号：" + _vm._s(i.orderNo))]), _vm._v(" "), _c('p', [_vm._v("交易时间：" + _vm._s(i.transTime))]), _vm._v(" "), _c('p', [_vm._v("交易机具：" + _vm._s(i.posSn))])])])
  }), 0)]), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.isNoData
    }
  }), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.showMenu) ? _c('div', {
    staticClass: "budgetDetail_mask",
    on: {
      "click": function($event) {
        return _vm.closeMenu()
      }
    }
  }) : _vm._e()])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-030f7246", module.exports)
  }
}

/***/ }),

/***/ 1947:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1486);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("239429cf", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-030f7246&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./realTrade.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-030f7246&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./realTrade.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 570:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1947)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1376),
  /* template */
  __webpack_require__(1800),
  /* scopeId */
  "data-v-030f7246",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\realTrade.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] realTrade.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-030f7246", Component.options)
  } else {
    hotAPI.reload("data-v-030f7246", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});