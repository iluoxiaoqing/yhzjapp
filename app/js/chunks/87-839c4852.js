webpackJsonp([87],{

/***/ 1482:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            phone: ''
        };
    },
    mounted: function mounted() {},

    methods: {
        goPay: function goPay() {
            var _this = this;

            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phone)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            common.Ajax({
                type: "post",
                url: _apiConfig2.default.KY_IP + "XYVIP/xyvip/wyyx",
                data: {
                    phone: this.phone,
                    htmlFlag: 'H5',
                    userNo: this.$route.query.userNo
                },
                showLoading: false
            }, function (data) {
                _this.$router.push({
                    "path": "wyyxPay",
                    "query": {
                        "orderNo": data.data.orderNo,
                        "payAmount": data.data.payAmount,
                        "userNo": _this.$route.query.userNo
                    }
                });
            });
        },
        test: function test() {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            document.documentElement.scrollTop ? document.documentElement.scrollTop = scrollTop + 'px' : document.body.scrollTop = scrollTop + 'px';
            this.goPay();
        }
    }
};

/***/ }),

/***/ 1521:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-2095d2ff] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-2095d2ff] {\n  background: #fff;\n}\n.tips_success[data-v-2095d2ff] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-2095d2ff] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-2095d2ff] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-2095d2ff],\n.fade-leave-active[data-v-2095d2ff] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-2095d2ff],\n.fade-leave-to[data-v-2095d2ff] {\n  opacity: 0;\n}\n.default_button[data-v-2095d2ff] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-2095d2ff] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-2095d2ff] {\n  position: relative;\n}\n.loading-tips[data-v-2095d2ff] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.main[data-v-2095d2ff] {\n  width: 100%;\n}\n.main .container[data-v-2095d2ff] {\n  width: 100%;\n  background: url(" + __webpack_require__(1780) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 96.38rem;\n}\n.main .container .vipList[data-v-2095d2ff] {\n  width: 91.5%;\n  margin: 0rem 4.25% 0;\n  top: 7.38rem;\n  background: url(" + __webpack_require__(1779) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 12.44rem;\n  position: absolute;\n}\n.main .container .vipList .phoneInput[data-v-2095d2ff] {\n  width: 100%;\n  margin-top: 11.16rem;\n  font-size: 0.3rem;\n  margin-left: 0.15rem;\n}\n.main .container .vipList .phoneInput input[data-v-2095d2ff] {\n  width: 4.54rem;\n  height: 0.8rem;\n  line-height: 0.8rem;\n  background: #ffffff;\n  color: #26221F;\n  display: block;\n  float: left;\n  border-bottom-left-radius: 0.06rem;\n  border-top-left-radius: 0.06rem;\n  padding-left: 0.2rem;\n}\n.main .container .vipList .phoneInput button[data-v-2095d2ff] {\n  height: 0.8rem;\n  line-height: 0.8rem;\n  background: #F2CA84;\n  color: #26221F;\n  display: block;\n  float: left;\n  width: 1.8rem;\n  text-align: center;\n  border-bottom-right-radius: 0.06rem;\n  border-top-right-radius: 0.06rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1779:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/list_4.jpg?v=62fa1464";

/***/ }),

/***/ 1780:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/wyyxbg_4.jpg?v=57190340";

/***/ }),

/***/ 1835:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "main",
    attrs: {
      "id": "main"
    }
  }, [_c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "vipList"
  }, [_c('div', {
    staticClass: "phoneInput"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "请输入您的手机号"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "blur": _vm.test,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  }), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goPay()
      }
    }
  }, [_vm._v("立即购买")])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2095d2ff", module.exports)
  }
}

/***/ }),

/***/ 1982:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1521);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("77960e98", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2095d2ff&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xyvipWyyx.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2095d2ff&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xyvipWyyx.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 693:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1982)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1482),
  /* template */
  __webpack_require__(1835),
  /* scopeId */
  "data-v-2095d2ff",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\wyyx\\xyvipWyyx.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] xyvipWyyx.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2095d2ff", Component.options)
  } else {
    hotAPI.reload("data-v-2095d2ff", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});