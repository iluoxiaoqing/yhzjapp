webpackJsonp([98],{

/***/ 1378:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            price: "",
            goods: {},
            canClick: false,
            details: [{}],
            remark: {}
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseOne",
            "data": {
                "productCode": this.$route.query.productCode
            }
        }, function (data) {
            console.log(data);
            _this.canClick = true;
            _this.goods = data.data;
            _this.price = data.data.price;
            _this.details = data.data.details;
            _this.remark = JSON.parse(data.data.remark);
        });
    },

    methods: {
        gotoPay: function gotoPay() {

            if (!this.canClick) {
                return;
            }

            var carList = [{
                currentNum: 1,
                goodsName: this.goods.goodsName,
                goodsType: this.goods.goodsType,
                imgPath: this.goods.imgPath,
                price: this.goods.price,
                productCode: this.goods.productCode,
                remark: this.goods.remark
            }];
            var purchaseJson = {};

            purchaseJson.carList = carList;
            purchaseJson.totalAmount = this.price;

            console.log(purchaseJson);

            common.youmeng("广告-荷包", "点击按钮");

            this.$store.dispatch('saveCarList', purchaseJson);

            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1607:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-a5f72080] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-a5f72080] {\n  background: #fff;\n}\n.tips_success[data-v-a5f72080] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-a5f72080] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-a5f72080] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-a5f72080],\n.fade-leave-active[data-v-a5f72080] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-a5f72080],\n.fade-leave-to[data-v-a5f72080] {\n  opacity: 0;\n}\n.default_button[data-v-a5f72080] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-a5f72080] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-a5f72080] {\n  position: relative;\n}\n.loading-tips[data-v-a5f72080] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.hebao[data-v-a5f72080] {\n  width: 100%;\n  overflow: hidden;\n  padding-bottom: 1.24rem;\n}\n.hebao .hebao_banner[data-v-a5f72080] {\n  width: 100%;\n  overflow: hidden;\n}\n.hebao .hebao_banner img[data-v-a5f72080] {\n  width: 100%;\n  display: block;\n}\n.hebao .hebao_item[data-v-a5f72080] {\n  width: 7rem;\n  overflow: hidden;\n  padding: .14rem 0;\n  margin: 0 auto .3rem;\n  background: #FF763D;\n  border-radius: .1rem;\n}\n.hebao .hebao_item .hebao_content[data-v-a5f72080] {\n  width: 6.72rem;\n  overflow: hidden;\n  margin: 0 auto;\n  background: #FDFBEE;\n  box-shadow: inset 0 .02rem .08rem 0 #C59662;\n  border-radius: .1rem;\n}\n.hebao .hebao_item .hebao_content .hebao_title[data-v-a5f72080] {\n  width: 100%;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin: .4rem 0;\n}\n.hebao .hebao_item .hebao_content .hebao_title span[data-v-a5f72080] {\n  font-size: .28rem;\n  color: #F64F3F;\n}\n.hebao .hebao_item .hebao_content .hebao_title p[data-v-a5f72080] {\n  color: #F64F3F;\n  font-size: .5rem;\n  font-weight: 700;\n  margin: 0 .16rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main[data-v-a5f72080] {\n  width: 6.24rem;\n  overflow: hidden;\n  background: #FFF3E5;\n  box-shadow: inset 0 0.02rem 0.06rem 0 rgba(167, 167, 167, 0.5);\n  border-radius: .1rem;\n  margin: 0 auto .3rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top[data-v-a5f72080] {\n  overflow: hidden;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: .42rem 0 .16rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_left[data-v-a5f72080] {\n  width: 1.72rem;\n  height: 1.76rem;\n  background: url(" + __webpack_require__(1644) + ") no-repeat;\n  background-size: contain;\n  position: relative;\n  margin-left: .28rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_left p[data-v-a5f72080] {\n  color: #F64F3F;\n  font-size: .32rem;\n  text-align: center;\n  position: absolute;\n  top: .12rem;\n  display: block;\n  width: 100%;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_left p b[data-v-a5f72080] {\n  font-size: .44rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_left i[data-v-a5f72080] {\n  color: #fff;\n  font-size: .32rem;\n  text-align: center;\n  position: absolute;\n  bottom: .24rem;\n  display: block;\n  width: 100%;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_right[data-v-a5f72080] {\n  color: #525252;\n  margin-left: .2rem;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_right b[data-v-a5f72080] {\n  font-size: .32rem;\n  display: block;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_top .item_right span[data-v-a5f72080] {\n  font-size: .24rem;\n  display: block;\n}\n.hebao .hebao_item .hebao_content .hebao_main .item_bottom[data-v-a5f72080] {\n  height: .66rem;\n  font-size: .20rem;\n  color: #fff;\n  background: #FFAC50;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hebao .buy_content[data-v-a5f72080] {\n  width: 6.14rem;\n  margin: 0 auto;\n  border-radius: .06rem;\n  padding: 0.26rem .3rem;\n  background: #fff;\n}\n.hebao .buy_content .buy_title[data-v-a5f72080] {\n  font-size: .38rem;\n  color: #5A5F79;\n  text-align: center;\n  margin-bottom: .52rem;\n  position: relative;\n  font-weight: 700;\n}\n.hebao .buy_content .buy_title[data-v-a5f72080]:after {\n  content: \"\";\n  width: 1.84rem;\n  height: .26rem;\n  opacity: 0.09;\n  background: #5C64DC;\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  bottom: 0;\n}\n.hebao .buy_content .buy_main[data-v-a5f72080] {\n  width: 100%;\n}\n.hebao .buy_content .buy_main .buy_desc[data-v-a5f72080] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  color: #313757;\n}\n.hebao .buy_content .buy_main .buy_desc b[data-v-a5f72080] {\n  font-size: .32rem;\n  opacity: 0.8;\n}\n.hebao .buy_content .buy_main .buy_desc i[data-v-a5f72080] {\n  font-size: .34rem;\n  color: #a2a4b1;\n}\n.hebao .buy_content .buy_main .buy_desc i em[data-v-a5f72080] {\n  color: #E95647;\n}\n.hebao .buy_content .buy_main p[data-v-a5f72080] {\n  color: #313757;\n  font-size: .28rem;\n  opacity: 0.6;\n}\n.hebao .buy_content .buy_main span[data-v-a5f72080] {\n  color: #313757;\n  font-size: .28rem;\n  display: block;\n  margin-top: .17rem;\n  opacity: 0.6;\n}\n.hebao .banner_button[data-v-a5f72080] {\n  width: 100%;\n  height: 1.24rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  z-index: 100;\n  background: #FADF3D;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hebao .banner_button p[data-v-a5f72080] {\n  color: #D6353D;\n  font-size: .24rem;\n}\n.hebao .banner_button p b[data-v-a5f72080] {\n  font-size: .4rem;\n}\n.hebao .banner_button p i[data-v-a5f72080] {\n  opacity: 0.7;\n}\n.hebao .banner_button span[data-v-a5f72080] {\n  font-size: .24rem;\n  color: #D6353D;\n  opacity: 0.7;\n}\n", ""]);

// exports


/***/ }),

/***/ 1643:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hebao_banner.png?v=adb88a26";

/***/ }),

/***/ 1644:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hebao_title.png?v=2f3bc457";

/***/ }),

/***/ 1920:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hebao"
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "hebao_item"
  }, [_c('div', {
    staticClass: "buy_content"
  }, [_c('div', {
    staticClass: "buy_title"
  }, [_vm._v("产品信息")]), _vm._v(" "), _c('div', {
    staticClass: "buy_main"
  }, [_c('div', {
    staticClass: "buy_desc"
  }, [_c('b', [_vm._v("【荷包】开店宝")]), _vm._v(" "), _c('i', [_c('em', [_vm._v(_vm._s(_vm.details[0].price) + "元")]), _vm._v("\n                          /台\n                      ")])]), _vm._v(" "), _c('p', [_vm._v("费率:" + _vm._s(_vm.remark.customerFeeDesc))]), _vm._v(" "), _c('p', [_vm._v("支付机构:" + _vm._s(_vm.remark.company))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.remark.commissionDesc))])])])]), _vm._v(" "), _c('div', {
    staticClass: "banner_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('p', [_c('b', [_vm._v("立即购买")]), _vm._v(" "), _c('i', [_vm._v("（" + _vm._s(_vm.details[0].count) + "台起购）")])]), _vm._v(" "), _c('span', [_vm._v("采购时间：" + _vm._s(_vm.remark.salesStartTime) + "-" + _vm._s(_vm.remark.salesEndTime))])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hebao_banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1643)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hebao_item"
  }, [_c('div', {
    staticClass: "hebao_content"
  }, [_c('div', {
    staticClass: "hebao_title"
  }, [_c('span', [_vm._v(" >> ")]), _vm._v(" "), _c('p', [_vm._v("领取现金奖励")]), _vm._v(" "), _c('span', [_vm._v(" << ")])]), _vm._v(" "), _c('div', {
    staticClass: "hebao_main"
  }, [_c('div', {
    staticClass: "item_top"
  }, [_c('div', {
    staticClass: "item_left"
  }, [_c('p', [_c('b', [_vm._v("120")]), _vm._v("元\n                          ")]), _vm._v(" "), _c('i', [_vm._v("激活返现")])]), _vm._v(" "), _c('div', {
    staticClass: "item_right"
  }, [_c('b', [_vm._v("商户交易满5000元")]), _vm._v(" "), _c('span', [_vm._v("激活时间：2019.3.1-2019.12.31")])])]), _vm._v(" "), _c('div', {
    staticClass: "item_bottom"
  }, [_vm._v("*经理奖励120元   主管奖励112.5元   推手奖励100元")])]), _vm._v(" "), _c('div', {
    staticClass: "hebao_main"
  }, [_c('div', {
    staticClass: "item_top"
  }, [_c('div', {
    staticClass: "item_left"
  }, [_c('p', [_c('b', [_vm._v("100")]), _vm._v("元\n                          ")]), _vm._v(" "), _c('i', [_vm._v("交易返现")])]), _vm._v(" "), _c('div', {
    staticClass: "item_right"
  }, [_c('b', [_vm._v("商户累计交易满10万元")]), _vm._v(" "), _c('span', [_vm._v("激活之日起90天之内")])])]), _vm._v(" "), _c('div', {
    staticClass: "item_bottom"
  }, [_vm._v("*经理奖励100元   主管奖励92.5元   推手奖励80元")])])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a5f72080", module.exports)
  }
}

/***/ }),

/***/ 2068:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1607);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("60bed281", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a5f72080&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hebao.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a5f72080&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hebao.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 572:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2068)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1378),
  /* template */
  __webpack_require__(1920),
  /* scopeId */
  "data-v-a5f72080",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\hebao.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] hebao.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a5f72080", Component.options)
  } else {
    hotAPI.reload("data-v-a5f72080", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});