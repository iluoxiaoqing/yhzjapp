webpackJsonp([82],{

/***/ 1424:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            animate: true,
            noticeList: [],
            activityTime: '',
            menuArray: [{
                title: "激活返现",
                active: "",
                num: ""
            }],
            allShopList: [{
                list: [],
                isNoData: false
            }],
            currentPage: 1,
            hasData: false,
            isLoading: true,
            mySwiper: null,
            index: 0,
            minHeight: 0,
            des: '',
            jumpUrl: '',
            thumbnail: '',
            title: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getList();
        this.getActive();
        this.index = 0;
        this.$nextTick(function () {
            _this.getHeight();
        });
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            // autoHeight: true,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.currentPage = 1;
                _this.getActive();
            }
        });
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default
    },
    methods: {
        getActive: function getActive() {
            var _this2 = this;

            console.log("页数1", this.currentPage);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "payEasy/incomeDetails",
                data: {
                    "page": this.currentPage
                },
                showLoading: false
            }, function (data) {

                _this2.$set(_this2.allShopList[0], "isNoData", false);
                if (data.data.totalResult == 0) {
                    _this2.allShopList[0].list = [];
                    _this2.$set(_this2.allShopList[0], "isNoData", true);
                } else if (data.data.length > 0) {
                    if (_this2.currentPage == 1) {
                        _this2.allShopList[0].list = [];
                    }
                    data.data.map(function (el) {
                        _this2.allShopList[0].list.push(el);
                    });
                    console.log("222222222", _this2.allShopList);

                    _this2.currentPage++;
                    console.log("页数", _this2.currentPage);
                } else {
                    if (_this2.currentPage > 1) {
                        // common.toast({
                        //     content: '无更多数据'
                        // });
                    } else {
                        _this2.$set(_this2.allShopList[0], "isNoData", true);
                    }
                }
            });
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = scroller.getBoundingClientRect().top;
            console.log("bodyHeight", bodyHeight);
            console.log("scroller", scroller);
            console.log("scrollerTop", scrollerTop);
            scroller.style.height = 300 + "px";
            this.minHeight = 529 + "px";
            console.log("高度为", this.minHeight);
            // let bodyHeight = document.documentElement.clientHeight;
            // let scroller = this.$refs.scroller;
            // let scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            // scroller.style.height = (bodyHeight - scrollerTop) + "px";
        },
        goShare: function goShare() {
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail,
                "jumpUrl": this.jumpUrl,
                "shareType": "url",
                "callbackName": ""

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            }
        },

        showMarquee: function showMarquee() {
            var _this3 = this;

            this.animate = true;
            setTimeout(function () {
                _this3.noticeList.push(_this3.noticeList[0]);
                _this3.noticeList.shift();
                _this3.animate = false;
            }, 500);
        },
        getList: function getList() {
            var _this4 = this;

            common.Ajax({
                type: "post",
                url: _apiConfig2.default.KY_IP + "payEasy/propaganda",
                data: {},
                showLoading: false
            }, function (data) {
                console.log(data.data);
                _this4.noticeList = data.data.scrollBar;
                _this4.activityTime = data.data.activityTime;
                _this4.des = data.data.weChatShare.des;
                _this4.jumpUrl = data.data.weChatShare.jumpUrl;
                _this4.thumbnail = data.data.weChatShare.thumbnail;
                _this4.title = data.data.weChatShare.title;
                setInterval(_this4.showMarquee, 2000);
            });
        },
        infinite: function infinite() {
            var _this5 = this;

            setTimeout(function () {
                _this5.getActive();
            }, 1000);
        },
        refresh: function refresh() {
            var _this6 = this;

            setTimeout(function () {
                _this6.currentPage = 1;
                _this6.getActive();
                // this.mySwiper.onResize();
            }, 1000);
        },
        loadmore: function loadmore() {
            var _this7 = this;

            setTimeout(function () {
                _this7.getActive();
                // this.mySwiper.getTranslate();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1611:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-b32e900a] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-b32e900a] {\n  background: #fff;\n}\n.tips_success[data-v-b32e900a] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-b32e900a] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-b32e900a] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-b32e900a],\n.fade-leave-active[data-v-b32e900a] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-b32e900a],\n.fade-leave-to[data-v-b32e900a] {\n  opacity: 0;\n}\n.default_button[data-v-b32e900a] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-b32e900a] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-b32e900a] {\n  position: relative;\n}\n.loading-tips[data-v-b32e900a] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.container[data-v-b32e900a] {\n  width: 100%;\n}\n.container .top_main[data-v-b32e900a] {\n  width: 100%;\n  height: 9.95rem;\n  background: url(" + __webpack_require__(1679) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.container .top_main .time_main[data-v-b32e900a] {\n  width: 92%;\n  height: 0.6rem;\n  margin: 0 4%;\n  top: 2.49rem;\n  line-height: 0.6rem;\n  position: relative;\n  background: url(" + __webpack_require__(1681) + ") no-repeat;\n  background-size: 100% 100%;\n  text-align: center;\n  font-size: 0.28rem;\n  font-weight: 500;\n  color: #e0cdff;\n}\n.container .xia_main[data-v-b32e900a] {\n  width: 100%;\n  background: #202293;\n}\n.container .xia_main .zfy_gg[data-v-b32e900a] {\n  width: 88%;\n  padding: 0.6rem 6% 0.41rem;\n}\n.container .xia_main .zfy_gg .marquee[data-v-b32e900a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: .41rem;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-radius: 2px;\n  position: relative;\n}\n.container .xia_main .zfy_gg .marquee i[data-v-b32e900a] {\n  width: .57rem;\n  height: .41rem;\n  background: url(" + __webpack_require__(1680) + ");\n  background-size: 100% 100%;\n  display: block;\n}\n.container .xia_main .zfy_gg .marquee span[data-v-b32e900a] {\n  font-size: 0.24rem;\n  font-weight: 800;\n  color: #ffffff;\n  line-height: 0.6rem;\n  width: 2.3rem;\n}\n.container .xia_main .zfy_gg .marquee .marquee_box[data-v-b32e900a] {\n  display: block;\n  position: relative;\n  width: 100%;\n  height: .6rem;\n  line-height: .6rem;\n  overflow: hidden;\n}\n.container .xia_main .zfy_gg .marquee .marquee_box .marquee_top[data-v-b32e900a] {\n  -webkit-transition: all 0.5s;\n  transition: all 0.5s;\n  margin-top: -0.6rem;\n}\n.container .xia_main .zfy_gg .marquee .marquee_box .marquee_list[data-v-b32e900a] {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n.container .xia_main .zfy_gg .marquee .marquee_box .marquee_list li[data-v-b32e900a] {\n  height: .6rem;\n  line-height: .6rem;\n  font-size: .24rem;\n  color: rgba(255, 255, 255, 0.8);\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n  top: .02rem;\n}\n.container .xia_main .zfy_step[data-v-b32e900a] {\n  width: 94%;\n  margin: 0 3%;\n  height: 2.46rem;\n  background: #1E117A;\n  border-radius: 0.2rem;\n}\n.container .xia_main .zfy_step .step_top[data-v-b32e900a] {\n  padding: 0.36rem 0.3rem 0.3rem;\n  display: inline-block;\n  float: left;\n}\n.container .xia_main .zfy_step .step_bottom[data-v-b32e900a] {\n  width: 100%;\n  display: inline-block;\n  float: left;\n}\n.container .xia_main .zfy_step .step_bottom .list1[data-v-b32e900a] {\n  width: 33%;\n  text-align: center;\n  font-size: 0.24rem;\n  font-family: PingFang SC;\n  font-weight: 400;\n  color: #ffffff;\n  line-height: 0.3rem;\n  float: left;\n}\n.container .xia_main .zfy_title[data-v-b32e900a] {\n  width: 100%;\n  margin: 0.54rem 0 0rem;\n  display: inline-block;\n}\n.container .xia_main .zfy_title span[data-v-b32e900a] {\n  font-size: 0.36rem;\n  font-weight: 400;\n  color: #ffffff;\n  line-height: 34px;\n  margin: 0 0.24rem;\n  float: left;\n}\n.container .xia_main .sy_main[data-v-b32e900a] {\n  width: 94%;\n  margin: 0 3%;\n  height: 6.02rem;\n  background: #1E117A;\n  border-radius: 0.2rem;\n}\n.container .xia_main .sy_main .orderList[data-v-b32e900a] {\n  width: 100%;\n  overflow: hidden;\n  border-radius: 0.08rem;\n  font-size: 0.28rem;\n  font-family: PingFang SC;\n  font-weight: 400;\n  color: #ffffff;\n}\n.container .xia_main .sy_main .orderList .item[data-v-b32e900a] {\n  width: 100%;\n  margin: 0 auto;\n  padding-top: 0.4rem;\n}\n.container .xia_main .sy_main .orderList .item table[data-v-b32e900a] {\n  width: 100%;\n}\n.container .xia_main .sy_main .orderList .item table .ac[data-v-b32e900a] {\n  width: 100%;\n  /* width: 701px; */\n  height: 1.56rem;\n  background: #1e117a;\n}\n.container .xia_main .sy_main .orderList .item table .ac span[data-v-b32e900a] {\n  margin: 0 0.6rem;\n}\n.container .xia_main .sy_main .orderList .item table .active1[data-v-b32e900a] {\n  background: #28198e !important;\n  height: 1.25rem !important;\n}\n.container .xia_main .empty[data-v-b32e900a] {\n  width: 100%;\n  height: 1.5rem;\n}\n.container .xia_main button[data-v-b32e900a] {\n  width: 92%;\n  margin: 0 4%;\n  height: 0.8rem;\n  line-height: 0.8rem;\n  background: -webkit-linear-gradient(bottom, #e21b21, #ff7c80);\n  background: linear-gradient(0deg, #e21b21, #ff7c80);\n  border-radius: 0.4rem;\n  font-size: 0.4rem;\n  font-family: PingFang SC;\n  font-weight: 800;\n  color: #ffffff;\n}\n", ""]);

// exports


/***/ }),

/***/ 1679:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/banner_bg.jpg?v=1ca32646";

/***/ }),

/***/ 1680:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/gg.png?v=b6648f9c";

/***/ }),

/***/ 1681:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/timeBg.png?v=9eccbacb";

/***/ }),

/***/ 1924:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-b32e900a", module.exports)
  }
}

/***/ }),

/***/ 2072:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1611);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0cd4a89c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b32e900a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b32e900a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 627:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2072)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1424),
  /* template */
  __webpack_require__(1924),
  /* scopeId */
  "data-v-b32e900a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\zfyIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] zfyIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b32e900a", Component.options)
  } else {
    hotAPI.reload("data-v-b32e900a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});