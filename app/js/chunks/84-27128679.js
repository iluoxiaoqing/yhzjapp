webpackJsonp([84],{

/***/ 1381:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            price: "",
            goods: {},
            canClick: false,
            details: [{}],
            remark: {}
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseOne",
            "data": {
                "productCode": this.$route.query.productCode
            }
        }, function (data) {
            console.log(data);
            _this.canClick = true;
            _this.goods = data.data;
            _this.price = data.data.price;
            _this.details = data.data.details;
            _this.remark = JSON.parse(data.data.remark);
        });
    },

    methods: {
        gotoPay: function gotoPay() {

            if (!this.canClick) {
                return;
            }

            var carList = [{
                currentNum: 1,
                goodsName: this.goods.goodsName,
                goodsType: this.goods.goodsType,
                imgPath: this.goods.imgPath,
                price: this.goods.price,
                productCode: this.goods.productCode,
                remark: this.goods.remark
            }];
            var purchaseJson = {};

            purchaseJson.carList = carList;
            purchaseJson.totalAmount = this.price;

            console.log(purchaseJson);

            common.youmeng("广告-友刷", "点击按钮");

            this.$store.dispatch('saveCarList', purchaseJson);

            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1548:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-4033be4d] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-4033be4d] {\n  background: #fff;\n}\n.tips_success[data-v-4033be4d] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-4033be4d] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-4033be4d] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-4033be4d],\n.fade-leave-active[data-v-4033be4d] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-4033be4d],\n.fade-leave-to[data-v-4033be4d] {\n  opacity: 0;\n}\n.default_button[data-v-4033be4d] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-4033be4d] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-4033be4d] {\n  position: relative;\n}\n.loading-tips[data-v-4033be4d] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.youshua[data-v-4033be4d] {\n  width: 100%;\n  overflow: hidden;\n  padding-bottom: 1.24rem;\n}\n.youshua .youshua_banner[data-v-4033be4d] {\n  width: 100%;\n  overflow: hidden;\n}\n.youshua .youshua_banner img[data-v-4033be4d] {\n  width: 100%;\n  display: block;\n}\n.youshua .youshua_title[data-v-4033be4d] {\n  width: 100%;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: .49rem 0 .32rem;\n  margin-bottom: .4rem;\n}\n.youshua .youshua_title img[data-v-4033be4d] {\n  width: .42rem;\n  height: .26rem;\n  display: block;\n}\n.youshua .youshua_title p[data-v-4033be4d] {\n  color: #FFD35F;\n  font-size: .36rem;\n  font-weight: 700;\n  margin: 0 .16rem;\n}\n.youshua .youshua_item[data-v-4033be4d] {\n  width: 6.66rem;\n  margin: 0 auto;\n  border: 1px solid #FAC993;\n  padding: .24rem 0;\n  border-radius: .1rem;\n  position: relative;\n  margin-bottom: .8rem;\n}\n.youshua .youshua_item .item_title[data-v-4033be4d] {\n  width: 3.74rem;\n  height: 1.12rem;\n  border: 1px solid #FAC993;\n  background: #5C64DC;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-radius: .1rem;\n  position: absolute;\n  top: -0.46rem;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n}\n.youshua .youshua_item .item_title b[data-v-4033be4d] {\n  width: 3.56rem;\n  height: .96rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: -webkit-linear-gradient(top, #FDDAAF 0%, #F9C186 100%);\n  background-image: linear-gradient(-180deg, #FDDAAF 0%, #F9C186 100%);\n  border-radius: .1rem;\n  font-size: .4rem;\n  color: #434343;\n}\n.youshua .youshua_item .item_top[data-v-4033be4d] {\n  width: 6.18rem;\n  margin: 0 auto;\n  background: #7478E6;\n  border-top-left-radius: .1rem;\n  border-top-right-radius: .1rem;\n  padding: .8rem 0 .34rem;\n}\n.youshua .youshua_item .item_top p[data-v-4033be4d] {\n  font-size: .32rem;\n  color: #fff;\n  text-align: center;\n}\n.youshua .youshua_item .item_top span[data-v-4033be4d] {\n  display: block;\n  text-align: center;\n  font-size: .24rem;\n  color: #fff;\n}\n.youshua .youshua_item .item_bottom[data-v-4033be4d] {\n  width: 6.18rem;\n  margin: 0 auto;\n  font-size: .24rem;\n  opacity: 0.6;\n  text-align: center;\n  overflow: hidden;\n  padding: .16rem 0 .16rem;\n  background: #666EDF;\n  border-bottom-left-radius: .1rem;\n  border-bottom-right-radius: .1rem;\n}\n.youshua .youshua_buy[data-v-4033be4d] {\n  width: 6.66rem;\n  overflow: hidden;\n  margin: -0.4rem auto 0.4rem;\n  padding: .34rem 0;\n  background: #fff;\n  border-radius: .1rem;\n}\n.youshua .youshua_buy .buy_content[data-v-4033be4d] {\n  width: 5.56rem;\n  margin: 0 auto;\n  border-radius: .06rem;\n  padding: 0.26rem .3rem;\n  position: relative;\n}\n.youshua .youshua_buy .buy_content[data-v-4033be4d]:after {\n  content: '';\n  position: absolute;\n  left: -50%;\n  bottom: -50%;\n  width: 200%;\n  height: 200%;\n  -webkit-transform: scale(0.5);\n      -ms-transform: scale(0.5);\n          transform: scale(0.5);\n  border: 1px solid #e5e5e5;\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  border-radius: .06rem;\n}\n.youshua .youshua_buy .buy_content .buy_title[data-v-4033be4d] {\n  font-size: .38rem;\n  color: #5A5F79;\n  text-align: center;\n  margin-bottom: .52rem;\n  position: relative;\n  font-weight: 700;\n}\n.youshua .youshua_buy .buy_content .buy_title[data-v-4033be4d]:after {\n  content: \"\";\n  width: 1.84rem;\n  height: .26rem;\n  opacity: 0.09;\n  background: #5C64DC;\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  bottom: 0;\n}\n.youshua .youshua_buy .buy_content .buy_main[data-v-4033be4d] {\n  width: 100%;\n}\n.youshua .youshua_buy .buy_content .buy_main .buy_desc[data-v-4033be4d] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  color: #313757;\n}\n.youshua .youshua_buy .buy_content .buy_main .buy_desc b[data-v-4033be4d] {\n  font-size: .32rem;\n  opacity: 0.8;\n}\n.youshua .youshua_buy .buy_content .buy_main .buy_desc i[data-v-4033be4d] {\n  font-size: .34rem;\n  color: #a2a4b1;\n}\n.youshua .youshua_buy .buy_content .buy_main .buy_desc i em[data-v-4033be4d] {\n  color: #E95647;\n}\n.youshua .youshua_buy .buy_content .buy_main p[data-v-4033be4d] {\n  color: #313757;\n  font-size: .28rem;\n  opacity: 0.6;\n}\n.youshua .youshua_buy .buy_content .buy_main span[data-v-4033be4d] {\n  color: #313757;\n  font-size: .28rem;\n  display: block;\n  margin-top: .17rem;\n  opacity: 0.6;\n}\n.youshua .banner_button[data-v-4033be4d] {\n  width: 100%;\n  height: 1.24rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  z-index: 100;\n  background: #FADF3D;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.youshua .banner_button p[data-v-4033be4d] {\n  color: #D6353D;\n  font-size: .24rem;\n}\n.youshua .banner_button p b[data-v-4033be4d] {\n  font-size: .4rem;\n}\n.youshua .banner_button p i[data-v-4033be4d] {\n  opacity: 0.7;\n}\n.youshua .banner_button span[data-v-4033be4d] {\n  font-size: .24rem;\n  color: #D6353D;\n  opacity: 0.7;\n}\n", ""]);

// exports


/***/ }),

/***/ 1651:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/youshua_banner.png?v=ab2a429a";

/***/ }),

/***/ 1652:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/youshua_you.png?v=181895e0";

/***/ }),

/***/ 1653:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/youshua_zuo.png?v=b4faaefa";

/***/ }),

/***/ 1862:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "youshua"
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "youshua_buy"
  }, [_c('div', {
    staticClass: "buy_content"
  }, [_c('div', {
    staticClass: "buy_title"
  }, [_vm._v("产品信息")]), _vm._v(" "), _c('div', {
    staticClass: "buy_main"
  }, [_c('div', {
    staticClass: "buy_desc"
  }, [_c('b', [_vm._v("【友刷】开店宝")]), _vm._v(" "), _c('i', [_c('em', [_vm._v(_vm._s(_vm.details[0].price) + "元")]), _vm._v("\n                          /台\n                      ")])]), _vm._v(" "), _c('p', [_vm._v("费率:" + _vm._s(_vm.remark.customerFeeDesc))]), _vm._v(" "), _c('p', [_vm._v("支付机构:" + _vm._s(_vm.remark.company))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.remark.commissionDesc))])])])]), _vm._v(" "), _c('div', {
    staticClass: "banner_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('p', [_c('b', [_vm._v("立即购买")]), _vm._v(" "), _c('i', [_vm._v("（" + _vm._s(_vm.details[0].count) + "台起购）")])]), _vm._v(" "), _c('span', [_vm._v("采购时间：" + _vm._s(_vm.remark.salesStartTime) + "-" + _vm._s(_vm.remark.salesEndTime))])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "youshua_banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1651)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "youshua_title"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1653),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("两重奖励等你拿")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1652),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "youshua_item"
  }, [_c('div', {
    staticClass: "item_title"
  }, [_c('b', [_vm._v("激活返现150")])]), _vm._v(" "), _c('div', {
    staticClass: "item_top"
  }, [_c('p', [_vm._v("成功收取商户196元服务费")]), _vm._v(" "), _c('span', [_vm._v("激活时间：2019.3.1-2019.12.31")])]), _vm._v(" "), _c('div', {
    staticClass: "item_bottom"
  }, [_vm._v("*经理奖励150元   主管奖励139.5元   推手奖励122元")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "youshua_item"
  }, [_c('div', {
    staticClass: "item_title"
  }, [_c('b', [_vm._v("交易返现200")])]), _vm._v(" "), _c('div', {
    staticClass: "item_top"
  }, [_c('p', [_vm._v("商户累计交易满10万元")]), _vm._v(" "), _c('span', [_vm._v("激活之日起90天之内")])]), _vm._v(" "), _c('div', {
    staticClass: "item_bottom"
  }, [_vm._v("*经理奖励200元   主管奖励192.5元   推手奖励180元")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-4033be4d", module.exports)
  }
}

/***/ }),

/***/ 2009:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1548);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("687c8fa2", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4033be4d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./youshua.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4033be4d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./youshua.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 575:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2009)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1381),
  /* template */
  __webpack_require__(1862),
  /* scopeId */
  "data-v-4033be4d",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\youshua.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] youshua.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4033be4d", Component.options)
  } else {
    hotAPI.reload("data-v-4033be4d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});