webpackJsonp([35],{

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(603)

var Component = __webpack_require__(11)(
  /* script */
  null,
  /* template */
  __webpack_require__(562),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\register\\privacyPolicy.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] privacyPolicy.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5e7aa380", Component.options)
  } else {
    hotAPI.reload("data-v-5e7aa380", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.agreement {\n  width: 92%;\n  margin: 0 auto;\n  overflow: hidden;\n  padding-bottom: 1rem;\n  font-family: serif;\n}\n.agreement h1 {\n  font-size: 0.3rem;\n  text-align: center;\n  overflow: hidden;\n  display: block;\n  padding: 0.38rem 0 0.4rem;\n}\n.agreement h2 {\n  font-size: 0.26rem;\n  line-height: 0.48rem;\n  padding-bottom: 0.2rem;\n}\n.agreement h3 {\n  font-size: 0.3rem;\n  text-align: center;\n  overflow: hidden;\n  display: block;\n  padding: 0 0 0.4rem;\n}\n.agreement div {\n  font-size: 0.26rem;\n  line-height: 0.48rem;\n  text-align: justify;\n  text-indent: 2em;\n  color: #666666;\n}\n.agreement div span span {\n  letter-spacing: 0.15rem;\n}\n.agreement-aligning {\n  text-indent: inherit !important;\n  padding-top: 0.2rem;\n}\n.agreement-retract {\n  text-indent: 2em;\n  padding: 0.2rem 0rem 0.2rem 0rem;\n}\n.agreement span {\n  color: #151515;\n  font-weight: bold;\n}\n", ""]);

// exports


/***/ }),

/***/ 562:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "agreement"
  }, [_c('h1', [_vm._v("用户之家隐私保护政策")]), _vm._v(" "), _c('h2', [_vm._v("保护隐私的承诺")]), _vm._v(" "), _c('div', [_vm._v("无锡好刷信息科技有限公司（以下简称“我们”），深知个人隐私的重要性，我们将按法律法规要求，采取相应安全保护措施，尽力保护您的个人信息安全可控。我们希望通过《用户之家隐私保护政策》（以下简称“本政策”）向您说明我们在您使用产品或服务时如何收集、存储、使用及对外共享您的个人信息，以及我们为您提供的访问、更新、删除和保护这些信息的方式。为了保证对您的个人隐私信息合法、合理、适度的收集、使用，并在安全、可控的情况下进行传输、存储，我们制定了本政策，其中要点如下： ")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("1"), _c('span', [_vm._v(".")])]), _vm._v("为了便于您了解您在使用我们的服务时，我们需要收集的信息类型与用途，我们将结合具体服务向您逐一说明。")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("2"), _c('span', [_vm._v(".")])]), _vm._v("为了向您提供服务所需，我们会按照合法、正当、必要的原则收集您的信息。 ")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("3"), _c('span', [_vm._v(".")])]), _vm._v("如果为了向您提供服务而需要将您的信息共享至第三方，我们将评估该第三方收集信息的合法性、正当性、必要性。我们将要求第三方对您的信息采取保护措施并且严格遵守相关法律法规与监管要求。另外，我们会按照法律法规及国家标准的要求以确认协议、弹窗提示等形式征得您的同意或确认第三方已经征得您的同意。")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("4"), _c('span', [_vm._v(".")])]), _vm._v("如果为了向您提供服务而需要从第三方获取您的信息，我们将要求第三方说明信息来源，并要求第三方保障其提供信息的合法性；如果我们开展业务需进行的个人信息处理活动超出您原本向第三方提供个人信息时的授权范围，我们将征得您的明确同意。")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("5"), _c('span', [_vm._v(".")])]), _vm._v("您可以通过本政策介绍的方式访问和管理您的信息、设置隐私功能、注销账户或进行投诉举报。")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-aligning"
  }, [_vm._v("您可以根据以下索引阅读相应章节，进一步了解本政策的具体约定：")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("一、")]), _vm._v("我们如何收集信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("二、")]), _vm._v("我们如何使用信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("三、")]), _vm._v("我们如何使用Cookie及相关技术")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("四、")]), _vm._v("我们如何存储和保护信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("五、")]), _vm._v("我们如何共享、转让、公开披露您的个人信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("六、")]), _vm._v("您如何访问和管理自己的信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("七、")]), _vm._v("我们如何保护未成年人的信息")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("八、")]), _vm._v("本政策的适用及更新")]), _vm._v(" "), _c('div', [_c('span', [_vm._v("九、")]), _vm._v("本政策中关键词说明")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("本政策与您使用我们的服务关系紧密，我们建议您仔细阅读并理解本政策全部内容，做出您认为适当的选择。请您仔细阅读本政策并确认了解我们对您个人信息的处理规则。如您就本政策点击或勾选“同意”并确认提交，即视为您同意本政策,并同意我们将按照本政策来收集、使用、存储和共享您的相关信息。")]), _vm._v(" "), _c('h2', [_vm._v("一、我们如何收集信息 ")]), _vm._v(" "), _c('div', [_vm._v("在您使用以下各项业务功能（以下简称服务）的过程中，我们需要收集您的一些信息，用以向您提供服务、提升我们的服务质量、保障您的账户和资金安全以及符合国家法律法规及监管规定：")]), _vm._v(" "), _c('div', [_vm._v("1.在您注册用户之家账户时，您需提供手机号码作为账户登录名。如您不提供前述信息，可能无法注册用户之家账户。根据相关法律法规的规定，您需通过身份基本信息多重交叉验证后方可使用我们的服务，例如您使用账户提现时，需要提供本人身份证信息、本人银行卡认证以完成身份基本信息多重交叉验证。如您不提供前述信息，可能无法使用需要通过多重交叉验证后方可使用的部分服务，但不影响您使用我们提供的其他服务；")]), _vm._v(" "), _c('div', [_vm._v("2.安全管理 为了保障向您提供的服务的安全稳定运行，预防交易和资金风险，我们需要记录您使用的服务类别、方式及设备型号、IP地址、设备软件版本信息、设备识别码、设备标识符、所在地区、网络使用习惯以及其他与服务相关的日志信息。如您不同意我们记录前述信息，可能无法完成验证。")]), _vm._v(" "), _c('div', [_vm._v("3.客户服务 我们会收集您使用服务时的搜索记录、您与客户服务团队联系时提供的信息及您参与问卷调查时向我们发送的信息。如您不提供前述信息，不影响您使用我们提供的其他服务。")]), _vm._v(" "), _c('div', [_vm._v("4.设备权限调用 以下情形中，您可选择是否授权我们收集、使用您的个人信息：")]), _vm._v(" "), _c('div', [_vm._v("（1）设备状态，用于确定设备识别码，以保证账号登录的安全性。拒绝授权后，将无法登录用户之家App，但这不影响您使用未登录情况下用户之家App可用的功能。")]), _vm._v(" "), _c('div', [_vm._v("（2）存储权限，用于缓存您在使用用户之家App过程中产生的文本、图像、视频内容，拒绝授权后，将无法登录用户之家App，但这不影响您使用未登录情况下用户之家App可用的功能。")]), _vm._v(" "), _c('div', [_vm._v("（3）相机（摄像头），用于身份证识别、银行卡识别、识别二维码、人脸识别、设置头像、录像、拍照，在扫一扫、人脸识别登录/转账/支付/提升认证、头像设置、录像场景、图片备注中使用。拒绝授权后，上述功能将无法使用。对于某些品牌或型号的手机终端自身的本地生物特征认证功能，如人脸识别功能，其信息由手机终端提供商进行处理，我们不留存您应用手机终端相关功能的信息。 ")]), _vm._v(" "), _c('div', [_vm._v("（4）相册，用于上传照片设置您的头像、用于备注您的交易信息。我们获得的图片信息，加密后存储于数据库中。拒绝授权后，上述功能将无法使用。")]), _vm._v(" "), _c('div', [_vm._v("（5）麦克风，用于录音、智能机器人交互服务场景中使用。拒绝授权后，上述功能将无法使用。")]), _vm._v(" "), _c('div', [_vm._v("（6）手机通讯录，在短信通知过程中，我们将收集您的手机通讯录用于协助您通过通讯录快速选取电话号码，无需您手动输入。拒绝授权后，上述功能仍可以使用，但需要手工输入手机号码。")]), _vm._v(" "), _c('div', [_vm._v("（7）手机短信，用于短信分享链接，或用于拦截欺诈短信。如您拒绝通讯信息（短信）授权或未启用欺诈短信过滤功能，我们将不读取短信内容，系统后台不保存短信内容。")]), _vm._v(" "), _c('div', [_vm._v("（8）网络通讯，用于与服务端进行通讯。拒绝授权后，APP所有功能无法使用。我们系统后台保存客户交易时所使用设备的网络信息，包括IP、端口信息。")]), _vm._v(" "), _c('div', [_vm._v("（9）蓝牙，用于通过蓝牙将客户手机与第三方设备连接并交互。拒绝授权后，无法通过蓝牙激活第三方设备。我们系统后台不保存客户手机蓝牙配置信息。 上述功能可能需要您在您的设备中向我们开启您的设备、存储、相机（摄像头）、相册、手机通讯录、短信、麦克风、网络通讯、蓝牙的访问权限，以实现这些功能所涉及的信息的收集和使用。请您注意，您开启这些权限即代表您授权我们可以收集和使用这些信息来实现上述功能，如您取消了这些授权，则我们将不再继续收集和使用您的这些信息，也无法为您提供上述与这些授权所对应的功能。")]), _vm._v(" "), _c('div', [_vm._v("5.根据相关法律法规及国家标准，在以下情形中，我们可能会依法收集并使用您的个人信息无需征得您的同意 ")]), _vm._v(" "), _c('div', [_vm._v("（1）与国家安全、国防安全直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（2）与公共安全、公共卫生、重大公共利益直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（3）与犯罪侦查、起诉、审判和判决执行等直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（4）出于维护您或他人的生命、财产等重大合法权益但又很难得到您本人同意的； ")]), _vm._v(" "), _c('div', [_vm._v("（5）所收集的个人信息是您自行向社会公众公开的；")]), _vm._v(" "), _c('div', [_vm._v("（6）从合法公开披露的信息中收集个人信息，例如：合法的新闻报道、政府信息公开等渠道；")]), _vm._v(" "), _c('div', [_vm._v("（7）根据您的要求签订和履行合同所必需的；")]), _vm._v(" "), _c('div', [_vm._v("（8）用于维护所提供的服务的安全稳定运行所必需的，例如：发现、处置服务的故障；")]), _vm._v(" "), _c('div', [_vm._v("（9）法律法规规定的其他情形。")]), _vm._v(" "), _c('div', [_vm._v("6.其他 ")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("请您理解，我们向您提供的服务是不断更新和发展的。如您选择使用了前述说明当中尚未涵盖的其他服务，基于该服务我们需要收集您的信息的，我们会通过页面提示、交互流程、协议约定的方式另行向您说明信息收集的范围与目的，并征得您的同意。我们会按照本政策以及相应的用户协议约定使用、存储、对外提供及保护您的信息；如您选择不提供前述信息，您可能无法使用某项或某部分服务，但不影响您使用我们提供的其他服务。此外，第三方主体可能会通过本产品向您提供服务。 当您进入第三方主体运营的服务页面时，请注意相关服务由第三方主体向您提供。涉及到第三方主体向您收集个人信息的，建议您仔细查看第三方主体的隐私政策或协议约定。")]), _vm._v(" "), _c('h2', [_vm._v("二、我们如何使用信息 ")]), _vm._v(" "), _c('div', [_vm._v("1.为了遵守国家法律法规及监管要求，以及向您提供服务及提升服务质量，或保障您的账户安全，我们会在以下情形中使用您的信息： ")]), _vm._v(" "), _c('div', [_vm._v("（1）实现本政策中我们如何收集信息所述目的；")]), _vm._v(" "), _c('div', [_vm._v("（2）为了使您知晓使用服务的状态，我们会向您发送服务提醒。")]), _vm._v(" "), _c('div', [_vm._v("（3）为了保障服务的稳定性与安全性，我们会将您的信息用于身份验证、安全防范、诈骗监测、预防或禁止非法活动、降低风险、存档和备份用途；")]), _vm._v(" "), _c('div', [_vm._v("（4）根据法律法规或监管要求向相关部门进行报告；")]), _vm._v(" "), _c('div', [_vm._v("（5）我们的在线客服会使用您的账号信息和服务信息。为保证您的账号安全，我们的在线客服会使用您的账号信息与您核验您的身份。当您需要我们提供与您服务信息相关的客服与售后服务时，我们将会查询您的服务信息；")]), _vm._v(" "), _c('div', [_vm._v("（6）为了保证您及他人的合法权益，如您被他人投诉或投诉他人，在必要时，我们会将您的姓名及身份证号码、投诉相关内容提供给消费者权益保护部门及监管机关，以便及时解决投诉纠纷，但法律法规明确禁止提供的除外；")]), _vm._v(" "), _c('div', [_vm._v("（7）我们会采取脱敏、去标识化等方式对您的信息进行综合统计、分析加工，以便为您提供更加准确、个性、流畅及便捷的服务，或帮助我们评估、改善或设计服务及运营活动；")]), _vm._v(" "), _c('div', [_vm._v("（8）我们可能会根据用户信息形成群体特征标签，用于向用户提供其可能感兴趣的营销活动通知、商业性电子信息或广告。")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("2.当我们要将信息用于本政策未载明的其他用途时，会按照法律法规及国家标准的要求以确认协议、具体场景下的文案确认动作等形式再次征得您的同意。 ")]), _vm._v(" "), _c('h2', [_vm._v("三、我们如何使用Cookie及相关技术 ")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("为确保用户之家App正常运转，我们会在您移动设备上存储名为Cookie的小数据文件。Cookie通常包含标识符、站点名称以及一些号码和字符。借助于Cookie，用户之家App能够存储您的偏好数据。我们不会将Cookie用于本政策所述目的之外的任何用途。您可根据自己的偏好管理或删除Cookie。您可以清除移动设备上保存的所有Cookie，大部分网络浏览器都设有阻止Cookie的功能。但如果您这么做，则需要在每一次访问用户之家App时更改用户设置。")]), _vm._v(" "), _c('h2', [_vm._v("四、我们如何存储和保护信息 ")]), _vm._v(" "), _c('div', [_vm._v("1.个人信息的保存 您的个人信息将存储于中华人民共和国境内。我们仅在本政策所述目的所必需期间和法律法规及监管规定的时限内保存您的个人信息。 ")]), _vm._v(" "), _c('div', [_vm._v("2.安全措施 我们会采用符合业界标准的安全防护措施以及行业内通行的安全技术来防止您的个人信息遭到未经授权的访问、修改，避免您的个人信息泄露、损坏或丢失：  ")]), _vm._v(" "), _c('div', [_vm._v("（1）采用加密技术对您的个人信息进行加密存储。")]), _vm._v(" "), _c('div', [_vm._v("（2）我们的网络服务采取了传输层安全协议等加密技术，通过https等方式提供浏览服务，确保您的个人信息在传输过程中的安全。")]), _vm._v(" "), _c('div', [_vm._v("（3）在使用您的个人信息使用时，例如个人信息展示、个人信息关联计算，我们会采用包括内容替换在内的多种数据脱敏技术增强个人信息在使用中的安全性。")]), _vm._v(" "), _c('div', [_vm._v("（4）我们建立了相关内控制度，对可能接触到您的信息的工作人员采取最小够用授权原则；对工作人员处理您的信息的行为进行系统监控，不断对工作人员培训相关法律法规及隐私安全准则和安全意识强化宣导。 ")]), _vm._v(" "), _c('div', [_vm._v("3.安全事件处置 ")]), _vm._v(" "), _c('div', [_vm._v("（1）一旦发生个人信息安全事件，我们将按照法律法规的要求，及时向您告知安全事件的基本情况和可能的影响、我们已采取或将要采取的处置措施、您可自主防范和降低风险的建议、对您的补救措施等。我们同时将及时将事件相关情况以邮件、信函、电话、推送通知等方式告知您，难以逐一告知个人信息主体时，我们会采取合理、有效的方式发布公告。同时，我们还将按照监管部门要求，主动上报个人信息安全事件的处置情况。")]), _vm._v(" "), _c('div', [_vm._v("（2）我们会及时处置系统漏洞、网络攻击、病毒侵袭及网络侵入等安全风险。在发生危害网络安全的事件时，我们会按照网络安全事件应急预案，及时采取相应的补救措施。")]), _vm._v(" "), _c('div', [_vm._v("（3）如因我们的物理、技术或管理防护设施遭到破坏，导致信息被非授权访问、公开披露、篡改或损毁，导致您的合法权益受损，我们将严格依照法律的规定承担相应的责任。 ")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("（4）特别提示\n    "), _c('div', [_vm._v(" 请您务必妥善保管好您的登录名及其他身份要素。您在使用服务时，我们会通过您的登录名及其他身份要素来识别您的身份。一旦您泄漏了前述信息，您可能会蒙受损失，并可能对您产生不利。如您发现登录名及或其他身份要素可能或已经泄露时，请您立即和我们取得联系，以便我们及时采取相应措施以避免或降低相关损失。")])]), _vm._v(" "), _c('h2', [_vm._v(" 五、我们如何共享、转让、公开披露您的个人信息 ")]), _vm._v(" "), _c('div', [_vm._v("1.共享 ")]), _vm._v(" "), _c('div', [_vm._v("（一）授权合作共享")]), _vm._v(" "), _c('div', [_vm._v("我们可能会向关联公司、合作伙伴等第三方共享您的服务信息、账户信息及设备信息，以保障为您提供的服务顺利完成。但我们仅会出于合法、正当、必要、特定、明确的目的共享您的个人信息，并且只会共享提供服务所必要的个人信息。我们的合作伙伴无权将共享的个人信息用于任何其他用途。 我们的合作伙伴包括以下类型：")]), _vm._v(" "), _c('div', [_vm._v("（1）提供技术、咨询服务的供应商。我们可能会将您的身份证、银行卡、手机号共享给支持我们提供服务的第三方。这些机构包括为我们提供基础设施技术服务、数据处理服务、电信服务、法律服务等的机构。但我们要求这些服务提供商只能出于为我们提供服务的目的使用您的信息，而不得出于其自身利益使用您的信息。")]), _vm._v(" "), _c('div', [_vm._v("（2）合作金融机构及保险机构，这些机构可以向我们提供金融服务产品。除非您同意将这些信息用于其他用途，否则这些金融机构不得将此类信息用于相关产品之外的其他目的。")]), _vm._v(" "), _c('div', [_vm._v("（3）为了实现APP内部分业务功能，我们接入了第三方SDK，如推送服务、地图服务，这些第三方SDK为实现产品推荐或广告服务、使用第三方账户登录、支付数据整理、PUSH推送、完成您的信息分享指令等，可能会收集您的IMEI、IP、mac地址等个人信息。 对我们与之共享个人信息的公司、组织和个人，我们会与其签署严格的保密协定，要求他们按照我们的说明、本隐私政策以及其他任何相关的保密和安全措施来处理个人信息。在个人敏感数据使用上，我们要求第三方采用数据脱敏和加密技术，从而更好地保护用户数据。")]), _vm._v(" "), _c('div', [_vm._v("（二）投诉处理 当您投诉他人或被他人投诉时，为了保护您及他人的合法权益，我们可能会将您的姓名及有效证件号码、联系方式、投诉相关内容提供给消费者权益保护部门及监管机关，以便及时解决投诉纠纷，但法律法规明确禁止提供的除外。")]), _vm._v(" "), _c('div', [_vm._v("2.转让 我们不会将您的个人信息转让给任何公司、组织和个人，但以下情况除外：")]), _vm._v(" "), _c('div', [_vm._v("（1）事先获得您的明确同意；")]), _vm._v(" "), _c('div', [_vm._v("（2）根据法律法规或强制性的行政或司法要求；")]), _vm._v(" "), _c('div', [_vm._v("（3）在涉及资产转让、收购、兼并、重组或破产清算时，如涉及到个人信息转让，我们会向您告知有关情况，并要求新的持有您个人信息的公司、组织继续受本政策的约束。如变更个人信息使用目的时，我们将要求该公司、组织重新取得您的明确同意。 ")]), _vm._v(" "), _c('div', [_vm._v("3.公开披露 除在公布中奖活动名单时会脱敏展示中奖者手机号或登录名外，原则上我们不会将您的信息进行公开披露。如确需公开披露时，我们会向您告知公开披露的目的、披露信息的类型及可能涉及的敏感信息，并征得您的明确同意。")]), _vm._v(" "), _c('div', [_vm._v("4.委托处理 为了提升信息处理效率，降低信息处理成本，或提高信息处理准确性，我们可能会委托有能力的我们的关联公司或其他专业机构代表我们来处理信息。我们会通过书面协议等方式要求受托公司遵守严格的保密义务及采取有效的保密措施，禁止其将这些信息用于未经您授权的用途。")]), _vm._v(" "), _c('div', [_vm._v("5.根据相关法律法规及国家标准，在以下情形中，我们可能会依法共享、转让、公开披露您的个人信息无需征得您的同意：")]), _vm._v(" "), _c('div', [_vm._v("（1）与国家安全、国防安全直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（2）与公共安全、公共卫生、重大公共利益直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（3）与犯罪侦查、起诉、审判和判决执行等直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（4）出于维护您或其他个人的生命、财产等重大合法权益但又很难得到您本人同意的；")]), _vm._v(" "), _c('div', [_vm._v("（5）您自行向社会公众公开的个人信息；")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("（6）从合法公开披露的信息中收集的个人信息，例如：合法的新闻报道、政府信息公开等渠道。 ")]), _vm._v(" "), _c('h2', [_vm._v("六、您如何访问和管理自己的信息 ")]), _vm._v(" "), _c('div', [_vm._v("在您使用服务期间，为了您可以更加便捷地访问和管理您的信息，同时保障您注销账户的权利，我们在客户端中为您提供了相应的操作设置，您可以参考下面的指引进行操作。 ")]), _vm._v(" "), _c('div', [_vm._v("1.管理您的信息您登录用户之家App后，可以在“我的-个人信息”中，进行个人信息设置、收货地址设置。")]), _vm._v(" "), _c('div', [_vm._v("2.尽管有上述约定，但按照相关法律法规及国家标准，在以下情形中，我们可能无法响应您的请求：")]), _vm._v(" "), _c('div', [_vm._v("（1）与国家安全、国防安全直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（2）与公共安全、公共卫生、重大公共利益直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（3）与犯罪侦查、起诉、审判和执行判决等直接相关的；")]), _vm._v(" "), _c('div', [_vm._v("（4）有充分证据表明您存在主观恶意或滥用权利的；")]), _vm._v(" "), _c('div', [_vm._v("（5）响应您的请求将导致其他个人、组织的合法权益受到严重损害的；")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("（6）涉及商业秘密的。 ")]), _vm._v(" "), _c('h2', [_vm._v("七、我们如何保护未成年人的信息 ")]), _vm._v(" "), _c('div', [_vm._v("1.我们不建议未成年人注册成为我们的用户或者使用我们的APP。如果在特殊情况下必须使用，我们期望经由父母或监护人指导。我们将根据国家相关法律法规的规定保护未成年人的信息的保密性及安全性。 ")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("2.如您为未成年人，建议请您的父母或监护人阅读本政策，并在征得您父母或监护人同意的前提下使用我们的服务或向我们提供您的信息。对于经父母或监护人同意而收集您的信息的情况，我们只会在受到法律允许、父母或监护人明确同意或者保护您的权益所必要的情况下使用或公开披露此信息。如您的监护人不同意您按照本政策使用我们的服务或向我们提供信息，请您立即终止使用我们的服务并及时通知我们，以便我们采取相应的措施。 ")]), _vm._v(" "), _c('h2', [_vm._v("八、本政策的适用及更新 ")]), _vm._v(" "), _c('div', [_vm._v("用户之家所有服务均适用本政策，除非相关服务已有独立的隐私政策或相应的用户服务协议当中存在特殊约定。 发生下列重大变化情形时，我们会适时对本政策进行更新： ")]), _vm._v(" "), _c('div', [_vm._v("（1）我们的基本情况发生变化，例如：兼并、收购、重组引起的所有者变更；")]), _vm._v(" "), _c('div', [_vm._v("（2）收集、存储、使用个人信息的范围、目的、规则发生变化；")]), _vm._v(" "), _c('div', [_vm._v("（3）对外提供个人信息的对象、范围、目的发生变化；")]), _vm._v(" "), _c('div', [_vm._v("（4）您访问和管理个人信息的方式发生变化；")]), _vm._v(" "), _c('div', [_vm._v("（5）数据安全能力、信息安全风险发生变化；")]), _vm._v(" "), _c('div', [_vm._v("（6）用户询问、投诉的渠道和机制，以及外部纠纷解决机构及联络方式发生变化；")]), _vm._v(" "), _c('div', {
    staticClass: "agreement-retract"
  }, [_vm._v("（7）其他可能对您的个人信息权益产生重大影响的变化。 如本政策发生更新，我们将以推送通知、弹窗提示、发送邮件短消息或者在官方网站发布公告的方式来通知您。为了您能及时接收到通知，建议您在联系方式更新时及时通知我们。如您在本政策更新生效后继续使用我们的服务，即表示您已充分阅读、理解并接受更新后的政策并愿意受更新后的政策约束。我们鼓励您在每次使用时都查阅我们的隐私政策。 ")]), _vm._v(" "), _c('h2', [_vm._v("九、本政策中关键词说明 ")]), _vm._v(" "), _c('div', [_vm._v("1.本政策中的身份要素是指：用于识别您身份的信息要素，包括：您的登录名、密码、短信校验码、手机号码、证件号码及生物识别信息。 2.本政策中的个人敏感信息是指：个人电话号码、身份证件号码、个人生物识别信息、银行账号、财产信息、交易信息、精准定位信息等个人信息。")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5e7aa380", module.exports)
  }
}

/***/ }),

/***/ 603:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(496);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(13)("4498bc48", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5e7aa380!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./privacyPolicy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5e7aa380!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./privacyPolicy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});