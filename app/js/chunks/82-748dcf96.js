webpackJsonp([82],{

/***/ 1424:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            animate: true,
            noticeList: [],
            activityTime: '',
            menuArray: [{
                title: "激活返现",
                active: "",
                num: ""
            }],
            allShopList: [{
                list: [],
                isNoData: false
            }],
            currentPage: 1,
            hasData: false,
            isLoading: true,
            mySwiper: null,
            index: 0,
            minHeight: 0,
            des: '',
            jumpUrl: '',
            thumbnail: '',
            title: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getList();
        this.getActive();
        this.index = 0;
        this.$nextTick(function () {
            _this.getHeight();
        });
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            // autoHeight: true,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.currentPage = 1;
                _this.getActive();
            }
        });
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default
    },
    methods: {
        getActive: function getActive() {
            var _this2 = this;

            console.log("页数1", this.currentPage);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "payEasy/incomeDetails",
                data: {
                    "page": this.currentPage
                },
                showLoading: false
            }, function (data) {

                _this2.$set(_this2.allShopList[0], "isNoData", false);
                if (data.data.totalResult == 0) {
                    _this2.allShopList[0].list = [];
                    _this2.$set(_this2.allShopList[0], "isNoData", true);
                } else if (data.data.length > 0) {
                    if (_this2.currentPage == 1) {
                        _this2.allShopList[0].list = [];
                    }
                    data.data.map(function (el) {
                        _this2.allShopList[0].list.push(el);
                    });
                    console.log("222222222", _this2.allShopList);

                    _this2.currentPage++;
                    console.log("页数", _this2.currentPage);
                } else {
                    if (_this2.currentPage > 1) {
                        // common.toast({
                        //     content: '无更多数据'
                        // });
                    } else {
                        _this2.$set(_this2.allShopList[0], "isNoData", true);
                    }
                }
            });
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = scroller.getBoundingClientRect().top;
            console.log("bodyHeight", bodyHeight);
            console.log("scroller", scroller);
            console.log("scrollerTop", scrollerTop);
            scroller.style.height = 300 + "px";
            this.minHeight = 529 + "px";
            console.log("高度为", this.minHeight);
            // let bodyHeight = document.documentElement.clientHeight;
            // let scroller = this.$refs.scroller;
            // let scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            // scroller.style.height = (bodyHeight - scrollerTop) + "px";
        },
        goShare: function goShare() {
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail,
                "jumpUrl": this.jumpUrl,
                "shareType": "url",
                "callbackName": ""

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            }
        },

        showMarquee: function showMarquee() {
            var _this3 = this;

            this.animate = true;
            setTimeout(function () {
                _this3.noticeList.push(_this3.noticeList[0]);
                _this3.noticeList.shift();
                _this3.animate = false;
            }, 500);
        },
        getList: function getList() {
            var _this4 = this;

            common.Ajax({
                type: "post",
                url: _apiConfig2.default.KY_IP + "payEasy/propaganda",
                data: {},
                showLoading: false
            }, function (data) {
                console.log(data.data);
                _this4.noticeList = data.data.scrollBar;
                _this4.activityTime = data.data.activityTime;
                _this4.des = data.data.weChatShare.des;
                _this4.jumpUrl = data.data.weChatShare.jumpUrl;
                _this4.thumbnail = data.data.weChatShare.thumbnail;
                _this4.title = data.data.weChatShare.title;
                setInterval(_this4.showMarquee, 2000);
            });
        },
        infinite: function infinite() {
            var _this5 = this;

            setTimeout(function () {
                _this5.getActive();
            }, 1000);
        },
        refresh: function refresh() {
            var _this6 = this;

            setTimeout(function () {
                _this6.currentPage = 1;
                _this6.getActive();
                // this.mySwiper.onResize();
            }, 1000);
        },
        loadmore: function loadmore() {
            var _this7 = this;

            setTimeout(function () {
                _this7.getActive();
                // this.mySwiper.getTranslate();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1611:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1924:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-b32e900a", module.exports)
  }
}

/***/ }),

/***/ 2072:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1611);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0cd4a89c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b32e900a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b32e900a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 627:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2072)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1424),
  /* template */
  __webpack_require__(1924),
  /* scopeId */
  "data-v-b32e900a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\zfyIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] zfyIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b32e900a", Component.options)
  } else {
    hotAPI.reload("data-v-b32e900a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});