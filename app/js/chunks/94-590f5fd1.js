webpackJsonp([94],{

/***/ 1421:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            bussGroups: [],
            business: {},
            imgUrl1: _apiConfig2.default.KY_IP,
            bannerList: [],
            mainCode: "",
            mainCodeName: ""
        };
    },
    mounted: function mounted() {
        this.getBusiness();
        this.getBanner();

        common.youmeng("推荐贷款", "进入推荐贷款");
    },

    methods: {
        getBusiness: function getBusiness() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/appBusiness",
                "data": {
                    "buss": "LOAN_CODE"
                }
            }, function (data) {
                console.log(data.data);
                _this.business = data.data.business;
                _this.bussGroups = data.data.groups;
                _this.mainCode = data.data.business.productCode;
                _this.mainCodeName = data.data.business.productName;
                data.data.groups.map(function (el) {
                    el.products.map(function (value) {
                        var userDesc = value.userDesc || "";
                        _this.$set(value, "userDesc", userDesc ? JSON.parse(userDesc) : {});
                    });
                });
            });
        },
        getBanner: function getBanner() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "wonderfulActivityInfo/findPageBanner",
                "data": {
                    "bannerType": "LOAN_CODE"
                }
            }, function (data) {
                _this2.bannerList = data.data || [];
                _this2.$nextTick(_this2.mySwiper);
            });
        },
        gotoLoan: function gotoLoan() {

            common.youmeng("贷款申请", "点击我要贷款");

            this.$router.push({
                "path": "applyLoan",
                "query": {
                    "buss": "LOAN_CODE"
                }
            });
        },
        gotoShare: function gotoShare(j) {

            if (!j.isShare) {
                return;
            }

            this.$router.push({
                "path": "share",
                "query": {
                    "product": j.productCode,
                    "productName": j.productName,
                    "channel": "recommendLoan"
                }
            });
            common.youmeng("推荐贷款", "点击" + j.productName);
        },
        mySwiper: function mySwiper() {
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2000, //可选选项，自动滑动
                loop: true,
                pagination: ".swiper-pagination"
            });
        }
    }
};

/***/ }),

/***/ 1566:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1675:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/banner_juxing.png?v=3a413061";

/***/ }),

/***/ 1676:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_logo.png?v=07409f54";

/***/ }),

/***/ 1879:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "creditCard"
  }, [(_vm.bannerList.length > 0) ? _c('div', {
    staticClass: "creditCard_banner"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.bannerList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide"
    }, [_c('a', {
      attrs: {
        "href": item.h5Url
      }
    }, [(item.imgUrl == '') ? _c('img', {
      attrs: {
        "src": __webpack_require__(1675)
      }
    }) : _c('img', {
      attrs: {
        "src": _vm.imgUrl1 + 'file/downloadFile?filePath=' + item.imgUrl,
        "alt": ""
      }
    })])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]) : _vm._e(), _vm._v(" "), _vm._l((_vm.bussGroups), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "creditCard_bank"
    }, [_c('h1', [_vm._v(_vm._s(i.group) + " "), _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.gotoLoan()
        }
      }
    }, [_vm._v("我要贷款")])]), _vm._v(" "), _c('div', {
      staticClass: "bankList"
    }, [_vm._l((i.products), function(j, index) {
      return [_c('div', {
        staticClass: "item",
        on: {
          "click": function($event) {
            return _vm.gotoShare(j)
          }
        }
      }, [(j.logo != '') ? _c('div', {
        staticClass: "logo"
      }, [_c('img', {
        attrs: {
          "src": _vm.imgUrl1 + 'file/downloadFile?filePath=' + j.logo,
          "alt": ""
        }
      })]) : _c('div', {
        staticClass: "logo"
      }, [_c('img', {
        attrs: {
          "src": __webpack_require__(1676),
          "alt": ""
        }
      })]), _vm._v(" "), _c('b', [_vm._v(_vm._s(j.productName))]), _vm._v(" "), (j.isShow) ? _c('p', {
        domProps: {
          "innerHTML": _vm._s(j.userDesc.feature ? j.userDesc.feature.join('<br>') : '')
        }
      }) : _c('p', [_vm._v("\n                        暂未开放"), _c('br'), _vm._v("敬请期待\n                    ")]), _vm._v(" "), (j.isShow) ? _c('span', [_vm._v(_vm._s(j.userDesc.rewardAmount || ""))]) : _vm._e()])]
    }), _vm._v(" "), (i.products.length % 3 == 2) ? _c('div', {
      staticClass: "item height0"
    }) : _vm._e()], 2)])
  }), _vm._v(" "), _c('div', {
    staticClass: "creditCard_btn"
  }, [_c('a', {
    staticClass: "default_button",
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoShare(_vm.business)
      }
    }
  }, [_vm._v("全部推荐")])])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-54824209", module.exports)
  }
}

/***/ }),

/***/ 2027:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1566);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("03b7df6a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-54824209!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recommendLoan.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-54824209!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recommendLoan.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 620:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2027)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1421),
  /* template */
  __webpack_require__(1879),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\recommendLoan.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] recommendLoan.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-54824209", Component.options)
  } else {
    hotAPI.reload("data-v-54824209", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});