webpackJsonp([47],{

/***/ 1300:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_android.png?v=21a8b9f1";

/***/ }),

/***/ 1325:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_iphone.png?v=f726c911";

/***/ }),

/***/ 1447:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showDia: false,
            showDia1: false,
            showDia2: false,
            index: 0,
            downUrl: _apiConfig2.default.KY_IP,
            showLoading: false,
            androidUrl: "",
            iosUrl: "",
            ios: false,
            android: false,
            isios: false,
            isAndioid: false,
            isApi: false,
            isOpt: false
        };
    },
    mounted: function mounted() {
        this.getUrl();
        document.getElementById("container").style.minHeight = window.screen.height + "px";
        common.youmeng("下载", "进入下载页面");
    },

    methods: {
        isWeixin: function isWeixin() {

            var ua = window.navigator.userAgent.toLowerCase();

            if (common.isClient() == "ios") {
                this.isios = true;
                this.isAndioid = false;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                    this.showDia2 = false;
                    this.showDia1 = true;
                    return true;
                } else {
                    this.showDia2 = true;
                    this.showDia1 = false;
                    return false;
                }
            } else {
                console.log(this.androidUrl);
                this.isios = false;
                this.isAndioid = true;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                    this.showDia2 = false;
                    this.showDia1 = true;
                    return true;
                } else {
                    this.showDia2 = true;
                    this.showDia1 = false;
                    return false;
                }
            }
        },
        show: function show() {
            this.showDia = true;
        },
        iosClick: function iosClick() {
            common.youmeng("ios-下载", "按钮点击");
        },
        androidClick: function androidClick() {
            common.youmeng("Android-下载", "按钮点击");
        },
        getUrl: function getUrl() {
            var _this = this;

            common.Ajax({
                type: "get",
                url: _apiConfig2.default.KY_IP + "appVersionInfo/downApp",
                data: {
                    appCode: "HM_PARTNER"
                },
                showLoading: false
            }, function (data) {
                console.log(data.data);
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].operSys == "IOS") {
                        _this.iosUrl = data.data[i].appDownloadUrl;
                        console.log("iosurl:", _this.iosUrl);
                    } else {
                        _this.androidUrl = data.data[i].appDownloadUrl;
                        console.log("androidUrl:", _this.androidUrl);
                        if (_this.androidUrl.substr(0, 4).toLowerCase() == "http") {
                            _this.androidUrl = _this.androidUrl;
                            console.log("androidUrl", _this.androidUrl);
                            _this.isOpt = true;
                        } else {
                            _this.androidUrl = "app";
                            _this.isApi = true;
                            // this.androidUrl = this.androidUrl;
                        }
                    }
                }
                // this.androidUrl = data.data[1].appDownloadUrl;
                _this.isWeixin();
            });
        }
    }
};

/***/ }),

/***/ 1510:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-5400d9a6] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-5400d9a6] {\n  background: #fff;\n}\n.tips_success[data-v-5400d9a6] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-5400d9a6] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-5400d9a6] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-5400d9a6],\n.fade-leave-active[data-v-5400d9a6] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-5400d9a6],\n.fade-leave-to[data-v-5400d9a6] {\n  opacity: 0;\n}\n.default_button[data-v-5400d9a6] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-5400d9a6] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-5400d9a6] {\n  position: relative;\n}\n.loading-tips[data-v-5400d9a6] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.container[data-v-5400d9a6] {\n  width: 100%;\n  background: url(" + __webpack_require__(1712) + ") no-repeat;\n  background-size: cover;\n  background-attachment: fixed;\n  /*弹窗*/\n}\n.container .dialog[data-v-5400d9a6] {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.container .dialog img[data-v-5400d9a6] {\n  width: 5.6rem;\n  height: 2.17rem;\n}\n.container .wx_main[data-v-5400d9a6] {\n  width: 100%;\n}\n.container .wx_main .app_logo[data-v-5400d9a6] {\n  display: inline-block;\n  margin: 1.92rem auto 3.26rem;\n  width: 100%;\n  text-align: center;\n  font-size: 0.4rem;\n  color: #ffffff;\n}\n.container .wx_main .app_logo img[data-v-5400d9a6] {\n  width: 1.58rem;\n  height: 1.58rem;\n  vertical-align: middle;\n}\n.container .wx_main .app_logo p[data-v-5400d9a6] {\n  margin-top: 0.12rem;\n  line-height: 0.56rem;\n}\n.container .wx_main .andiro_down[data-v-5400d9a6] {\n  margin: 0 21%;\n  width: 58%;\n  height: 0.88rem;\n  line-height: 0.88rem;\n  opacity: 0.8;\n  background: #3F83FF;\n  border-radius: 4px;\n  font-size: 0.34rem;\n  color: #ffffff;\n  text-align: left;\n}\n.container .wx_main .andiro_down img[data-v-5400d9a6] {\n  width: 0.34rem;\n  height: 0.4rem;\n  margin-right: 0.2rem;\n  margin-left: 0.8rem;\n  vertical-align: middle;\n}\n.container .wx_main .andiro_down p[data-v-5400d9a6] {\n  line-height: 0.48rem;\n  display: inline;\n  vertical-align: middle;\n}\n.container .wx_main .ios_down[data-v-5400d9a6] {\n  margin: 0 21%;\n  width: 58%;\n  height: 0.88rem;\n  line-height: 0.88rem;\n  background: #ffffff;\n  border-radius: 4px;\n  font-size: 0.34rem;\n  color: #3D4A5B;\n  text-align: left;\n  margin-bottom: 2.46rem;\n}\n.container .wx_main .ios_down img[data-v-5400d9a6] {\n  width: 0.34rem;\n  height: 0.4rem;\n  margin-left: 0.8rem;\n  margin-right: 0.2rem;\n  vertical-align: middle;\n}\n.container .wx_main .ios_down p[data-v-5400d9a6] {\n  line-height: 0.48rem;\n  display: inline;\n  vertical-align: middle;\n}\n", ""]);

// exports


/***/ }),

/***/ 1712:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_bg.png?v=0e11e4d7";

/***/ }),

/***/ 1713:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_zhishi.png?v=06a58886";

/***/ }),

/***/ 1714:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1792:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    attrs: {
      "id": "container"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1713),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "wx_main"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia1),
      expression: "showDia1"
    }],
    staticClass: "wx"
  }, [_c('button', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isAndioid),
      expression: "isAndioid"
    }],
    staticClass: "andiro_down",
    on: {
      "click": function($event) {
        return _vm.show();
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1300),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])]), _vm._v(" "), _c('button', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isios),
      expression: "isios"
    }],
    staticClass: "ios_down",
    on: {
      "click": function($event) {
        return _vm.show();
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1325),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("IOS版下载")])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia2),
      expression: "showDia2"
    }],
    staticClass: "sefire"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isAndioid),
      expression: "isAndioid"
    }]
  }, [_c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isApi),
      expression: "isApi"
    }],
    attrs: {
      "href": _vm.downUrl + 'appVersionInfo/h5DownHhm'
    },
    on: {
      "click": function($event) {
        return _vm.androidClick()
      }
    }
  }, [_vm._m(1)]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isOpt),
      expression: "isOpt"
    }],
    attrs: {
      "href": _vm.androidUrl
    },
    on: {
      "click": function($event) {
        return _vm.androidClick()
      }
    }
  }, [_vm._m(2)])]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isios),
      expression: "isios"
    }],
    attrs: {
      "href": _vm.iosUrl
    },
    on: {
      "click": function($event) {
        return _vm.iosClick()
      }
    }
  }, [_vm._m(3)])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "app_logo"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1714),
      "alt": ""
    }
  }), _c('br'), _vm._v(" "), _c('p', [_vm._v("逍遥推手")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "andiro_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1300),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "andiro_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1300),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "ios_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1325),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("IOS版下载")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5400d9a6", module.exports)
  }
}

/***/ }),

/***/ 1926:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1510);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("e11b6706", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5400d9a6&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wxDown.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5400d9a6&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wxDown.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 677:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1926)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1447),
  /* template */
  __webpack_require__(1792),
  /* scopeId */
  "data-v-5400d9a6",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\wxDown\\wxDown.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] wxDown.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5400d9a6", Component.options)
  } else {
    hotAPI.reload("data-v-5400d9a6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});