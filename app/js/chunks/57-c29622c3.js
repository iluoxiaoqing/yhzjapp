webpackJsonp([57],{

/***/ 1340:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_reward.png?v=a7395104";

/***/ }),

/***/ 1427:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            placeholder: "",
            isFocus: false,
            inputValue: "",
            detailList: [],
            isShow: false,
            isShowNOsearchdate: false,
            nodataText: "",
            activityCode: this.$route.query.activityCode
        };
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        next(function (vm) {
            console.log("from===", vm);
            var title = vm.$route.query.title;
            document.title = title;
        });
    },

    props: {
        placeholderText: {
            type: String,
            default: "输入手机号搜索商户"
        },
        type: {
            type: Boolean,
            default: false
        },
        inputText: String
    },
    components: {},
    mounted: function mounted() {
        // this.$refs.inputValue="";
        this.commonAjax();
    },

    methods: {
        commonAjax: function commonAjax() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardDetails",
                "data": {
                    // phoneNo:null,
                    activityCode: this.$route.query.activityCode
                }
            }, function (data) {
                _this.detailList = data.data;
                if (_this.detailList == "") {
                    _this.isShow = true;
                }
            });
        },
        confirm: function confirm() {
            var _this2 = this;

            this.$emit("confirm", this.inputValue);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardDetails",
                "data": {
                    phoneNo: this.inputValue,
                    activityCode: this.$route.query.activityCode
                }
            }, function (data) {
                _this2.detailList = data.data;
                if (_this2.detailList == "") {
                    _this2.isShowNOsearchdate = true;
                    _this2.nodataText = _this2.inputValue;
                }
            });
        }
    }
};

/***/ }),

/***/ 1507:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1687:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_noreward.png?v=217dba0c";

/***/ }),

/***/ 1688:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_trading.png?v=37ee37f8";

/***/ }),

/***/ 1689:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_notattend.png?v=f81aedbe";

/***/ }),

/***/ 1821:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "detailMain"
  }, [(_vm.isShow == true) ? _c('div', {
    staticClass: "noDetail"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1689),
      "alt": ""
    }
  }), _c('br'), _vm._v(" "), _c('p', [_vm._v("您还没有参加活动哦")])]) : _vm._e(), _vm._v(" "), (_vm.isShow == false) ? _c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    ref: "searchBox",
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])]) : _vm._e(), _vm._v(" "), _vm._l((_vm.detailList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "detailList"
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "itemHead"
    }, [_vm._v("\n                    " + _vm._s(i.custName) + "\n                    "), _c('b', [_vm._v(_vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.registerDate) + "注册")])]), _vm._v(" "), _vm._l((i.stages), function(j, index) {
      return _c('div', {
        key: index,
        staticClass: "itemList"
      }, [
        [_c('div', {
          staticClass: "logo"
        }, [(j.status == 0) ? _c('img', {
          attrs: {
            "src": __webpack_require__(1687),
            "alt": ""
          }
        }) : _c('img', {
          attrs: {
            "src": __webpack_require__(1688),
            "alt": ""
          }
        })]), _vm._v(" "), (index == 0) ? _c('div', {
          staticClass: "itemCenter"
        }, [(j.status == 0) ? _c('div', {
          staticClass: "centerTextWait"
        }, [_vm._v("交易2000 可领10元")]) : (j.status == 1) ? _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易2000 "), _c('img', {
          attrs: {
            "src": __webpack_require__(1340),
            "alt": ""
          }
        }), _c('span', [_vm._v("+10元")])]) : _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易2000 可领10元")]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "centerTimeWait"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")]) : _c('div', {
          staticClass: "centerTime"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")])]) : _c('div', {
          staticClass: "itemCenter"
        }, [(j.status == 0) ? _c('div', {
          staticClass: "centerTextWait"
        }, [_vm._v("交易60000 可领10元")]) : (j.status == 1) ? _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易60000 "), _c('img', {
          attrs: {
            "src": __webpack_require__(1340),
            "alt": ""
          }
        }), _c('span', [_vm._v("+10元")])]) : _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易60000 可领10元")]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "centerTimeWait"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")]) : _c('div', {
          staticClass: "centerTime"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")])]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "itemRightWait"
        }, [_vm._v("\n                            待达标\n                        ")]) : (j.status == 1) ? _c('div', {
          staticClass: "itemRightDone"
        }, [_vm._v("\n                            已达标\n                        ")]) : _c('div', {
          staticClass: "itemRightEd"
        }, [_vm._v("\n                            已过期\n                        ")])]
      ], 2)
    })], 2), _vm._v(" "), _c('div', {
      staticClass: "emptyDiv"
    })])
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isShowNOsearchdate == true),
      expression: "isShowNOsearchdate==true"
    }],
    staticClass: "noSearchData"
  }, [_vm._v("\n            未搜索到“" + _vm._s(_vm.nodataText) + "”相关结果\n        ")])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1176a46b", module.exports)
  }
}

/***/ }),

/***/ 1968:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1507);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2758314c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1176a46b!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1176a46b!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 630:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1968)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1427),
  /* template */
  __webpack_require__(1821),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\rewardDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1176a46b", Component.options)
  } else {
    hotAPI.reload("data-v-1176a46b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});