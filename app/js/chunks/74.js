webpackJsonp([74],{

/***/ 1430:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            hasAddress: true,
            address: {},
            goodInfo: [],
            goodNum: 1,
            payShow: false,
            totalAmount: "",
            imgUrl: _apiConfig2.default.KY_IP,
            accountInfo: {},
            checkOut: false,
            balanceDeduc: 0
        };
    },
    activated: function activated() {
        var _this = this;

        window.alipayResult = this.alipayResult;
        var purchaseJson = this.$store.state.purchaseJson;
        var address = this.$store.state.address;
        console.log("22222", address);

        console.log("1111111", purchaseJson);

        this.goodInfo = purchaseJson.carList || "";
        console.log("商品信息为=====", this.goodInfo);
        this.totalAmount = purchaseJson.totalAmount;

        if (JSON.stringify(address) == "{}") {
            console.log("122222");
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/one"
            }, function (data) {
                if (data.data) {
                    _this.hasAddress = true;
                    _this.address = data.data;
                    console.log("地址为", _this.address);
                } else {
                    _this.hasAddress = false;
                }
            });
        } else {
            console.log("234444");
            this.address = address;
            this.hasAddress = true;
        }

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "acc/accountInfo"
        }, function (data) {

            _this.accountInfo = data.data;

            // if(data.data.balance>0){
            // 	this.checkOut = true;
            // }
            // else{
            // 	this.checkOut = false;
            // }
        });

        common.youmeng("提交订单", "进入提交订单");
    },
    mounted: function mounted() {},

    watch: {
        checkOut: function checkOut(value) {

            var balance = this.accountInfo.balance * 1;

            if (this.accountInfo.accStatus == 'NORMAL') {

                if (value) {
                    if (balance >= this.totalAmount * 1) {
                        this.balanceDeduc = this.totalAmount * 1;
                    } else {
                        this.balanceDeduc = balance;
                    }
                } else {
                    this.balanceDeduc = 0;
                }
            } else {
                common.toast({
                    "content": "您的账户暂时不允许使用余额抵扣"
                });
                this.checkOut = false;
            }

            //console.log(this.balanceDeduc)
        }
    },
    methods: {
        addYoumeng: function addYoumeng() {
            common.youmeng("提交订单", "点击添加地址");
        },
        alipayResult: function alipayResult(state) {
            var _this2 = this;

            if (state == "true" || state == 1 || state == true || state == '1') {
                common.toast({
                    "content": "支付成功"
                });
                common.youmeng("提交订单", "支付成功");
            } else {
                common.toast({
                    "content": "支付失败"
                });
                common.youmeng("提交订单", "支付失败");
            }

            setTimeout(function () {
                _this2.$router.push({
                    "path": "myOrder",
                    "query": {
                        "type": 1
                    }
                });
            }, 1000);
        },
        gotoAddress: function gotoAddress() {

            common.youmeng("提交订单", "打开地址列表选择");

            this.$router.push({
                "path": "addressList",
                "query": {
                    "appTitle": "",
                    "dispayInNative": ""
                }
            });
        },
        submitOrder: function submitOrder() {
            var _this3 = this;

            if (!this.hasAddress) {
                common.toast({
                    "content": "请先选择收货地址"
                });
                return;
            }

            common.youmeng("提交订单", "点击支付按钮");

            if (this.checkOut) {
                common.youmeng("提交订单", "使用账户余额抵扣");
            } else {
                common.youmeng("提交订单", "不使用使用账户余额抵扣");
            }

            var carList = this.$store.state.purchaseJson.carList;
            var goods = [];
            carList.map(function (el) {
                console.log(el);
                goods.push({
                    "type": el.goodsType,
                    "count": el.currentNum
                });
            });
            console.log("goods", goods);

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
                "data": {
                    "address": this.address.id,
                    "goods": JSON.stringify(goods),
                    "payAmount": this.totalAmount - this.balanceDeduc,
                    "accAmount": this.balanceDeduc
                }
            }, function (data) {

                console.log(data.data);

                if (data.data.alipay) {
                    var aliPay = {
                        "alipay": data.data.alipay,
                        "redirectFunc": "alipayResult"
                    };

                    if (common.isClient() == "ios") {
                        window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    } else {
                        window.android.nativeAlipay(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    }
                } else {

                    common.toast({
                        "content": "恭喜你下单成功！"
                    });

                    setTimeout(function () {
                        _this3.$router.push({
                            "path": "myOrder",
                            "query": {
                                "type": 1
                            }
                        });
                    }, 1000);
                }
            });
        }
    }
};

/***/ }),

/***/ 1530:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-6a4009ba] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-6a4009ba] {\n  background: #fff;\n}\n.tips_success[data-v-6a4009ba] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-6a4009ba] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-6a4009ba] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-6a4009ba],\n.fade-leave-active[data-v-6a4009ba] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-6a4009ba],\n.fade-leave-to[data-v-6a4009ba] {\n  opacity: 0;\n}\n.default_button[data-v-6a4009ba] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-6a4009ba] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-6a4009ba] {\n  position: relative;\n}\n.loading-tips[data-v-6a4009ba] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.mall_order[data-v-6a4009ba] {\n  width: 100%;\n  overflow: hidden;\n  padding-bottom: 1rem;\n}\n.mall_order .addAddress[data-v-6a4009ba] {\n  width: 100%;\n  height: 1.2rem;\n  background: #fff;\n  margin-bottom: 0.32rem;\n}\n.mall_order .addAddress a[data-v-6a4009ba] {\n  width: 6.9rem;\n  margin: 0 auto;\n  height: 1.2rem;\n  line-height: 1.2rem;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  display: block;\n  font-weight: 700;\n  background: url(" + __webpack_require__(783) + ") no-repeat right center;\n  background-size: 0.16rem 0.26rem;\n}\n.mall_order .addAddress a span[data-v-6a4009ba] {\n  background: url(" + __webpack_require__(1648) + ") no-repeat left center;\n  background-size: 0.64rem 0.64rem;\n  display: block;\n  padding-left: 0.96rem;\n}\n.mall_order .address[data-v-6a4009ba] {\n  width: 92%;\n  padding: 0.3rem 4%;\n  margin-bottom: 0.32rem;\n  background: url(" + __webpack_require__(783) + ") #fff no-repeat right 0.4rem center;\n  background-size: 0.14rem 0.24rem;\n  display: block;\n  padding-left: 0.4rem;\n  position: relative;\n}\n.mall_order .address .title[data-v-6a4009ba] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 0.4rem;\n  margin-bottom: 0.1rem;\n}\n.mall_order .address .title i[data-v-6a4009ba] {\n  width: 0.88rem;\n  height: 0.4rem;\n  background: #3F83FF;\n  font-size: 0.24rem;\n  display: block;\n  color: #fff;\n  border-radius: 0.2rem;\n  margin-right: 0.2rem;\n  text-align: center;\n  -moz-box-sizing: content-box;\n       box-sizing: content-box;\n  line-height: 0.44rem;\n}\n.mall_order .address .title p[data-v-6a4009ba] {\n  color: #3d4a5b;\n  opacity: 0.6;\n  font-size: 0.28rem;\n  height: 0.4rem;\n  line-height: 0.4rem;\n}\n.mall_order .address .content[data-v-6a4009ba] {\n  font-size: 0.4rem;\n  color: #3d4a5b;\n  font-weight: 700;\n}\n.mall_order .address .info[data-v-6a4009ba] {\n  font-size: 0.3rem;\n  color: #3d4a5b;\n  opacity: 0.6;\n}\n.mall_order .order_goods[data-v-6a4009ba] {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.mall_order .order_goods .mall_top[data-v-6a4009ba] {\n  width: 6.9rem;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  overflow: hidden;\n  padding: 0.3rem 0;\n  position: relative;\n}\n.mall_order .order_goods .mall_top[data-v-6a4009ba]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.mall_order .order_goods .mall_top[data-v-6a4009ba]:last-child:after {\n  content: '';\n  background: none;\n}\n.mall_order .order_goods .mall_top .mall_img[data-v-6a4009ba] {\n  width: 1.84rem;\n  height: 1.84rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n  margin-right: 0.3rem;\n}\n.mall_order .order_goods .mall_top .mall_img img[data-v-6a4009ba] {\n  width: 1.84rem;\n  height: 1.84rem;\n  display: block;\n}\n.mall_order .order_goods .mall_top .mall_buy[data-v-6a4009ba] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_description h2[data-v-6a4009ba] {\n  color: #3d4a5b;\n  font-size: 0.28rem;\n  display: block;\n  margin-bottom: 0.1rem;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_description p[data-v-6a4009ba] {\n  color: #3d4a5b;\n  font-size: 0.24rem;\n  display: block;\n  opacity: 0.6;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_price[data-v-6a4009ba] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_price span[data-v-6a4009ba] {\n  color: #3d4a5b;\n  font-size: 0.28rem;\n  display: block;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_price .addGoods[data-v-6a4009ba] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.mall_order .order_goods .mall_top .mall_buy .goods_price .addGoods i[data-v-6a4009ba] {\n  color: #3d4a5b;\n  font-size: 0.28rem;\n}\n.mall_order .useDiscount[data-v-6a4009ba] {\n  width: 92%;\n  padding: 0 4%;\n  height: 1.5rem;\n  background: #fff;\n  margin-top: 0.32rem;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.mall_order .useDiscount p[data-v-6a4009ba] {\n  font-size: 0.32rem;\n  color: #3D4A5B;\n}\n.mall_order .useDiscount p em[data-v-6a4009ba] {\n  color: #E95647;\n}\n.mall_order .useDiscount span[data-v-6a4009ba] {\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  opacity: 0.6;\n  display: block;\n}\n.mall_order .useDiscount label[data-v-6a4009ba] {\n  position: absolute;\n  right: 4%;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n      -ms-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.mall_order .useDiscount .a-switch[data-v-6a4009ba] {\n  width: 1.02rem;\n  height: 0.62rem;\n  border-radius: 0.4rem;\n  -webkit-appearance: none;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  outline: none;\n  background-color: #e0e0e0;\n  box-shadow: #c2c2c2 0 0 0 0 inset;\n  position: relative;\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\n.mall_order .useDiscount .a-switch[data-v-6a4009ba]:before {\n  content: '';\n  width: 0.56rem;\n  height: 0.56rem;\n  border-radius: 100%;\n  background-color: #fff;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.4);\n  position: absolute;\n  left: 0;\n  top: 1px;\n  -webkit-transition: 0.3s;\n  transition: 0.3s;\n}\n.mall_order .useDiscount .a-switch[data-v-6a4009ba]:checked {\n  border-color: #3F83FF;\n  background-color: #3F83FF;\n}\n.mall_order .useDiscount .a-switch[data-v-6a4009ba]:checked:before {\n  left: 0.46rem;\n}\n.mall_order .order_button[data-v-6a4009ba] {\n  width: 100%;\n  height: 1.0rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n}\n.mall_order .order_button p[data-v-6a4009ba] {\n  height: 1.0rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding-left: 0.3rem;\n}\n.mall_order .order_button p img[data-v-6a4009ba] {\n  width: 0.4rem;\n  height: 0.3rem;\n  display: block;\n  margin-right: 0.2rem;\n}\n.mall_order .order_button p span[data-v-6a4009ba] {\n  font-size: 0.28rem;\n  color: #3d4a5b;\n  display: block;\n}\n.mall_order .order_button p i[data-v-6a4009ba] {\n  font-size: 0.36rem;\n  color: #3F83FF;\n  display: block;\n}\n.mall_order .order_button p i em[data-v-6a4009ba] {\n  font-size: 0.24rem;\n}\n.mall_order .order_button a[data-v-6a4009ba] {\n  width: 2.2rem;\n  height: 1.0rem;\n  line-height: 1.0rem;\n  text-align: center;\n  font-size: 0.34rem;\n  color: #fff;\n  display: block;\n  background-image: -webkit-linear-gradient(315deg, #4886FF 0%, #56CCF2 100%);\n  background-image: linear-gradient(-225deg, #4886FF 0%, #56CCF2 100%);\n  margin-left: 0.4rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1648:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_add.png?v=18028be8";

/***/ }),

/***/ 1652:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_pay_alipay.png?v=185475a9";

/***/ }),

/***/ 1812:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "mall_order"
  }, [(_vm.hasAddress) ? _c('div', {
    staticClass: "address",
    on: {
      "click": function($event) {
        return _vm.gotoAddress()
      }
    }
  }, [_c('div', {
    staticClass: "title"
  }, [(_vm.address.isDefault) ? _c('i', [_vm._v("默认")]) : _vm._e(), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.address.province) + " " + _vm._s(_vm.address.city) + " " + _vm._s(_vm.address.area))])]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_vm._v(_vm._s(_vm.address.addressDetail))]), _vm._v(" "), _c('div', {
    staticClass: "info"
  }, [_vm._v(_vm._s(_vm.address.receivePerson) + " " + _vm._s(_vm.address.phoneNo))])]) : _c('div', {
    staticClass: "addAddress"
  }, [_c('router-link', {
    attrs: {
      "to": "addAddress"
    }
  }, [_c('span', {
    on: {
      "click": function($event) {
        return _vm.addYoumeng()
      }
    }
  }, [_vm._v("添加收货地址")])])], 1), _vm._v(" "), _c('div', {
    staticClass: "order_goods"
  }, _vm._l((_vm.goodInfo), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "mall_top"
    }, [_c('div', {
      staticClass: "mall_img"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.imgPath
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "mall_buy"
    }, [_c('div', {
      staticClass: "goods_description"
    }, [_c('h2', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('p', {
      domProps: {
        "innerHTML": _vm._s(JSON.parse(i.remark).goodsDesc)
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "goods_price"
    }, [_c('span', [_vm._v("¥" + _vm._s(i.price))]), _vm._v(" "), _c('div', {
      staticClass: "addGoods"
    }, [_c('i', [_vm._v("X" + _vm._s(i.currentNum))])])])])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "order_button"
  }, [_c('p', [_c('img', {
    attrs: {
      "src": __webpack_require__(1652)
    }
  }), _vm._v(" "), _c('span', [_vm._v("合计金额：")]), _vm._v(" "), _c('i', [_vm._v("￥" + _vm._s((_vm.totalAmount - _vm.balanceDeduc).toFixed(2)))])]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.submitOrder()
      }
    }
  }, [_vm._v("确认付款")])])]), _vm._v(" "), _c('choose-bank', {
    attrs: {
      "visible": _vm.payShow
    },
    on: {
      "update:visible": function($event) {
        _vm.payShow = $event
      }
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6a4009ba", module.exports)
  }
}

/***/ }),

/***/ 1946:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1530);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("518da492", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6a4009ba&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_order.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6a4009ba&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_order.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 654:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1946)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1430),
  /* template */
  __webpack_require__(1812),
  /* scopeId */
  "data-v-6a4009ba",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\shop\\mall_order.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mall_order.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6a4009ba", Component.options)
  } else {
    hotAPI.reload("data-v-6a4009ba", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 783:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ })

});