webpackJsonp([50],{

/***/ 1437:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0,
            listData: ""
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '杉徳');
        this.getList();
    },

    methods: {
        go: function go() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "sd/showTraditionMessage",
                "data": {}
            }, function (data) {
                _this.listData = data.data;
            });
        }
    }
};

/***/ }),

/***/ 1520:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-5c041908] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-5c041908] {\n  background: #fff;\n}\n.tips_success[data-v-5c041908] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-5c041908] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-5c041908] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-5c041908],\n.fade-leave-active[data-v-5c041908] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-5c041908],\n.fade-leave-to[data-v-5c041908] {\n  opacity: 0;\n}\n.default_button[data-v-5c041908] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-5c041908] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-5c041908] {\n  position: relative;\n}\n.loading-tips[data-v-5c041908] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.shande_main[data-v-5c041908] {\n  width: 100%;\n  background: #2c2da3;\n}\n.shande_main .logo[data-v-5c041908] {\n  width: 82%;\n  height: 1.34rem;\n  margin: 0 9%;\n  background: #ffffff;\n  border-radius: 0px 0px 0.2rem 0.2rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.shande_main .logo img[data-v-5c041908] {\n  width: 5.25rem;\n  height: 0.44rem;\n  vertical-align: middle;\n}\n.shande_main .content_main[data-v-5c041908] {\n  background: url(" + __webpack_require__(1688) + ") no-repeat;\n  background-size: 100% 100%;\n  width: 100%;\n  height: 16.82rem;\n}\n.shande_main .content_main .ac_time[data-v-5c041908] {\n  width: 100%;\n  text-align: center;\n  font-size: .28rem;\n  font-weight: 500;\n  color: #FFFFFF;\n  padding-top: 8.4rem;\n}\n.shande_main .content_main .pro_tit[data-v-5c041908] {\n  width: 74%;\n  margin: 0 13%;\n  height: 1.1rem;\n  background: url(" + __webpack_require__(1686) + ") no-repeat;\n  background-size: 100% 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-top: 0.47rem;\n}\n.shande_main .content_main .pro_tit img[data-v-5c041908] {\n  width: 2.47rem;\n  height: 0.55rem;\n  margin-top: 0.2rem;\n}\n.shande_main .content_main .content[data-v-5c041908] {\n  width: 90%;\n  height: 4.43rem;\n  margin: 0.37rem 5%;\n  background: -webkit-linear-gradient(top, #8923F0, #2D30DE);\n  background: linear-gradient(180deg, #8923F0, #2D30DE);\n  box-shadow: 0px 0.35rem 0.35rem 0px rgba(56, 56, 56, 0.15);\n  border-radius: 0.29rem;\n}\n.shande_main .content_main .content .contact[data-v-5c041908] {\n  display: inline-block;\n  float: left;\n  width: 92%;\n  margin: 0.03rem 5% 0;\n}\n.shande_main .content_main .content .contact .contact_left[data-v-5c041908] {\n  font-size: 0.24rem;\n  font-weight: bold;\n  width: 23%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  text-align: left;\n}\n.shande_main .content_main .content .contact .contact_left .leftLogo[data-v-5c041908] {\n  width: 0.23rem;\n  height: 0.23rem;\n  background: #FFF600;\n  box-shadow: 0px 0.03rem 0.08rem 0px rgba(255, 246, 0, 0.31);\n  border-radius: 0.12rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.shande_main .content_main .content .contact .contact_right[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  margin-top: 0.13rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 73%;\n}\n.shande_main .content_main .content .contact .contact_right p[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.shande_main .content_main .content .contact .remark[data-v-5c041908] {\n  font-weight: 500;\n  color: #F17C41;\n  font-size: 0.2rem;\n  white-space: nowrap;\n  display: inline-block;\n  float: left;\n  margin-left: 0.17rem;\n}\n.shande_main .content_main .content .contact .contact_left1[data-v-5c041908] {\n  font-size: 0.24rem;\n  width: 27.5%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  text-align: left;\n}\n.shande_main .content_main .content .contact .contact_left1 .leftLogo[data-v-5c041908] {\n  width: 0.23rem;\n  height: 0.23rem;\n  background: #FFF600;\n  box-shadow: 0px 0.03rem 0.08rem 0px rgba(255, 246, 0, 0.31);\n  border-radius: 0.12rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.shande_main .content_main .content .contact .contact_right1[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  margin-top: 0.13rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 65%;\n}\n.shande_main .content_main .content .contact .contact_right1 p[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.shande_main .content[data-v-5c041908] {\n  width: 90%;\n  height: 4.38rem;\n  margin: 0.37rem 5% 0;\n  background: -webkit-linear-gradient(top, #8923F0, #2D30DE);\n  background: linear-gradient(180deg, #8923F0, #2D30DE);\n  box-shadow: 0px 0.35rem 0.35rem 0px rgba(56, 56, 56, 0.15);\n  border-radius: 0.29rem;\n}\n.shande_main .content .contact[data-v-5c041908] {\n  display: inline-block;\n  float: left;\n  width: 92%;\n  margin: 0.06rem 5% 0;\n}\n.shande_main .content .contact .contact_left[data-v-5c041908] {\n  font-size: 0.24rem;\n  font-weight: bold;\n  width: 23%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  text-align: left;\n}\n.shande_main .content .contact .contact_left .leftLogo[data-v-5c041908] {\n  width: 0.23rem;\n  height: 0.23rem;\n  background: #FFF600;\n  box-shadow: 0px 0.03rem 0.08rem 0px rgba(255, 246, 0, 0.31);\n  border-radius: 0.12rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.shande_main .content .contact .contact_right[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  margin-top: 0.13rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 73%;\n}\n.shande_main .content .contact .contact_right p[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.shande_main .content .contact .remark[data-v-5c041908] {\n  font-weight: 500;\n  color: #F17C41;\n  font-size: 0.2rem;\n  white-space: nowrap;\n  display: inline-block;\n  float: left;\n  margin-left: 0.17rem;\n}\n.shande_main .content .contact .contact_left1[data-v-5c041908] {\n  font-size: 0.24rem;\n  width: 30%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  font-weight: bold;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  text-align: left;\n}\n.shande_main .content .contact .contact_left1 .leftLogo[data-v-5c041908] {\n  width: 0.23rem;\n  height: 0.23rem;\n  background: #FFF600;\n  box-shadow: 0px 0.03rem 0.08rem 0px rgba(255, 246, 0, 0.31);\n  border-radius: 0.12rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.shande_main .content .contact .contact_right1[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  margin-top: 0.12rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 60%;\n}\n.shande_main .content .contact .contact_right1 p[data-v-5c041908] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.shande_main button[data-v-5c041908] {\n  width: 90%;\n  margin: 0.3rem 5% 0.26rem;\n  height: 1rem;\n  background: -webkit-linear-gradient(top, #A95AF9, #585AEB);\n  background: linear-gradient(180deg, #A95AF9, #585AEB);\n  box-shadow: 0px 0.35rem 0.35rem 0px rgba(56, 56, 56, 0.15);\n  border-radius: 0.3rem;\n  text-align: center;\n  font-size: 0.4rem;\n  font-weight: bold;\n  color: #FFFFFF;\n  line-height: 1rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1686:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/btn.png?v=83e0a44e";

/***/ }),

/***/ 1687:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/shande_logo.png?v=32dfaab7";

/***/ }),

/***/ 1688:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/shande_main.png?v=e93540ba";

/***/ }),

/***/ 1689:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/shande_pro.png?v=0be8b680";

/***/ }),

/***/ 1690:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/shande_rule.png?v=ba439f3d";

/***/ }),

/***/ 1802:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "shande_main"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "content_main"
  }, [_c('div', {
    staticClass: "ac_time"
  }, [_vm._v("活动时间：" + _vm._s(_vm.listData.activityTime))]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [(_vm.listData.paymentCompany) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.paymentCompany) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.cardRate) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.cardRate), function(item) {
    return _c('p', [_vm._v(_vm._s(item))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.commRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.commRule) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.productFeatures) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.productFeatures) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.settleRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.settleRule) + "\r\n                                        ")])]) : _vm._e()]), _vm._v(" "), _vm._m(7)]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.posPrice) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "contact_right1"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.posPrice) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.actRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.actRecurrence), function(i) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.standardRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(10), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.standardRecurrence), function(item) {
    return _c('p', [_vm._v(_vm._s(item))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.rewardStandard) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(11), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.rewardStandard) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.platformStatement) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(12), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.platformStatement) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }, [_vm._v("立即预订")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1687),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pro_tit"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1689),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                支付公司:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                刷卡费率:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                分润规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                产品特点:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                结算规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pro_tit"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1690),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left1"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                机具采购价格:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                激活返现:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                达标返现:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                达标奖励:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                平台声明:\r\n                                        ")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5c041908", module.exports)
  }
}

/***/ }),

/***/ 1936:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1520);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0a5e49e4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5c041908&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./shande.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5c041908&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./shande.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 662:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1936)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1437),
  /* template */
  __webpack_require__(1802),
  /* scopeId */
  "data-v-5c041908",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\shop\\shandeIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] shandeIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5c041908", Component.options)
  } else {
    hotAPI.reload("data-v-5c041908", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});