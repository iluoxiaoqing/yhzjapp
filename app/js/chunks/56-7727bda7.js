webpackJsonp([56],{

/***/ 1345:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_wechat.png?v=70e9026c";

/***/ }),

/***/ 1430:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _html2canvas = __webpack_require__(165);

var _html2canvas2 = _interopRequireDefault(_html2canvas);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            hotList: [],
            isLoading: true,
            currentPage: 1,
            hasData: false,
            index: 0,
            imgUrl: _apiConfig2.default.KY_IP,
            showDia: false,
            showShare: false,
            show1: false,
            pushDetail: {},
            htmlUrl: "",
            tmpTimer: ""

        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        html2canvas: _html2canvas2.default
    },
    activated: function activated() {
        window.saveImg = this.saveImg;

        this.getHeight();
        common.youmeng("每日一推", "进入每日一推");
    },
    mounted: function mounted() {
        this.tmpTimer = setTimeout(function () {
            console.log();
        }, 2000);
        window.goBack = this.goBack;
        this.getHeight();

        this.getPageList();
        window.shareSucces = this.shareSucces;
    },

    methods: {
        toImage: function toImage(type) {
            var _this = this;

            common.loading("show");
            // 第一个参数是需要生成截图的元素,第二个是自己需要配置的参数,宽高等
            (0, _html2canvas2.default)(this.$refs.imageTofile, {
                useCORS: true
            }).then(function (canvas) {
                var url = canvas.toDataURL('image/png');
                console.log(url);
                _this.htmlUrl = url;
                common.loading("hide");
                if (type == "wechatSession") {
                    _this.wechatSession();
                } else if (type == "wechatTimeline") {
                    _this.wechatTimeline();
                } else if (type == "nativeSaveImage") {
                    _this.nativeSaveImage();
                }
            });
        },
        getImgDetail: function getImgDetail(id) {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "dailyPush/detail",
                data: {
                    "pushId": id
                },
                showLoading: false
            }, function (data) {
                data.data.pic = _this2.getImg(data.data.pic);
                data.data.userPic = _this2.getImg(data.data.userPic);
                _this2.pushDetail = data.data;
                console.log("211", _this2.pushDetail);
            });
        },
        getImg: function getImg(url) {
            if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        loadmore: function loadmore() {
            this.getPageList();
        },
        refresh: function refresh() {
            this.currentPage = 1;
            this.hotList = [];
            this.hasData = false;
            this.getPageList();
        },
        getPageList: function getPageList() {
            var _this3 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "dailyPush/pushList",
                data: {
                    "page": this.currentPage
                },
                showLoading: false
            }, function (data) {
                console.log("111", data.data);
                var curPage = _this3.currentPage;
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        el.pic = "url(" + _this3.getImg(el.pic) + ")";
                        _this3.hotList.push(el);
                    });
                    curPage++;
                    _this3.hasData = false;
                } else {
                    if (curPage == 1) {
                        _this3.hasData = true;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
                _this3.currentPage = curPage;
            });
        },
        Shareshow: function Shareshow(id) {
            this.showDia = true;
            this.show1 = true;
            this.getImgDetail(id);
        },
        wechatSession: function wechatSession() {
            // this.toImage();
            var thumbnail = this.htmlUrl;
            var shareJson = {
                "type": "wechatSession",
                "base64String": thumbnail,
                "shareType": "img",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShareBase64(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            // this.toImage();
            var thumbnail = this.htmlUrl;
            var shareJson = {
                "type": "wechatTimeline",
                "base64String": thumbnail,
                "shareType": "img",
                "callbackName": "shareSucces"

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShareBase64(JSON.stringify(shareJson));
            }
        },
        nativeSaveImage: function nativeSaveImage() {
            // this.toImage();
            var imgJson = {};
            imgJson.base64String = this.htmlUrl;
            imgJson.callbackName = "saveImg";
            console.log(imgJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeSaveImageBase64.postMessage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            } else {
                window.android.nativeSaveImageBase64(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            }
        },
        saveImg: function saveImg(j) {
            console.log("返回状态值：" + j);
            if (j == true || j == 0) {
                common.youmeng("每日一推", "图片保存成功");
                console.log("保存成功");
            } else {
                common.youmeng("每日一推", "图片保存失败");
                console.log("保存失败");
            }
        },
        shareSucces: function shareSucces() {
            this.showDia = false;
            this.show1 = false;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },
        showBottom: function showBottom() {
            this.showDia = true;
        }
    }
};

/***/ }),

/***/ 1620:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1699:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_circle_of_riends.png?v=52f5eba2";

/***/ }),

/***/ 1700:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_save.png?v=609461df";

/***/ }),

/***/ 1702:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_1.png?v=667d0bc9";

/***/ }),

/***/ 1703:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_2.png?v=d1ca3468";

/***/ }),

/***/ 1933:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    ref: "imageTofile",
    staticClass: "main"
  }, [_c('div', {
    staticClass: "mainImg"
  }, [_c('div', {
    staticClass: "imgDetail"
  }, [_c('img', {
    attrs: {
      "crossorigin": "anonymous",
      "src": _vm.pushDetail.pic,
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "pText",
    domProps: {
      "innerHTML": _vm._s(_vm.pushDetail.text)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "rightTime"
  }, [_vm._v("\n                        " + _vm._s(_vm.pushDetail.date) + "\n                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "crossorigin": "anonymous",
      "src": __webpack_require__(1345),
      "alt": ""
    }
  })]), _vm._v(" "), (JSON.stringify(_vm.pushDetail) != '{}') ? _c('div', {
    staticClass: "bottomMain"
  }, [_c('div', {
    staticClass: "boLeft"
  }, [_c('div', {
    staticClass: "boTopLeft"
  }, [_c('p', {
    staticClass: "boTopLeft"
  }, [_c('span', {
    staticClass: "boName"
  }, [_vm._v(_vm._s(_vm.pushDetail.userName))]), _vm._v(" "), _c('span', {
    staticClass: "personLevel"
  }, [_vm._v(_vm._s(_vm.pushDetail.userRole))])])]), _vm._v(" "), _c('div', {
    staticClass: "boBottom"
  }, [_c('p', [_c('img', {
    attrs: {
      "src": __webpack_require__(1702),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("工号：" + _vm._s(_vm.pushDetail.userNo))])]), _vm._v(" "), _c('p', [_c('img', {
    attrs: {
      "src": __webpack_require__(1703),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("手机号：" + _vm._s(_vm.pushDetail.phoneNo))])])])]), _vm._v(" "), _c('div', {
    staticClass: "boRight"
  }, [_c('img', {
    attrs: {
      "crossorigin": "anonymous",
      "src": _vm.imgUrl + 'news/shareQrCode?productCode=' + _vm.pushDetail.productCode + '&u=' + _vm.pushDetail.userNo + '&temp=' + Math.random(),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("加入逍遥推手")])])]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('wechatSession')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1345)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('wechatTimeline')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1699)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('nativeSaveImage')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1700)
    }
  }), _vm._v(" "), _c('span', [_vm._v("保存图片")])])])])]), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, _vm._l((_vm.hotList), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "shareMain"
    }, [_c('div', {
      staticClass: "share_left"
    }, [_c('div', {
      staticClass: "share_time"
    }, [_vm._v("\n                    " + _vm._s(j.timeLine) + "\n                ")])]), _vm._v(" "), _c('div', {
      staticClass: "share_right"
    }, [_c('div', {
      staticClass: "right_main"
    }, [_c('div', {
      staticClass: "right_top",
      style: ({
        background: j.pic
      })
    }, [_c('div', {
      staticClass: "content",
      domProps: {
        "innerHTML": _vm._s(j.text)
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "time"
    }, [_vm._v("\n                            " + _vm._s(j.date) + "\n                        ")])]), _vm._v(" "), _c('div', {
      staticClass: "right_center"
    }, [_vm._v("\n                        已推广" + _vm._s(j.spreadSum) + "次\n                    ")]), _vm._v(" "), _c('div', {
      staticClass: "right_bottom",
      on: {
        "click": function($event) {
          return _vm.Shareshow(j.id)
        }
      }
    }, [_vm._v("\n                        一键推广\n                    ")])])])])
  }), 0)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-d0fa167a", module.exports)
  }
}

/***/ }),

/***/ 2081:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1620);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("1cfbea05", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d0fa167a!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./dayShare.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d0fa167a!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./dayShare.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 633:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2081)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1430),
  /* template */
  __webpack_require__(1933),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\dayShare.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] dayShare.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d0fa167a", Component.options)
  } else {
    hotAPI.reload("data-v-d0fa167a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});