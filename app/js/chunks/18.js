webpackJsonp([18],{

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(463)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(365),
  /* template */
  __webpack_require__(439),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\applyRecord\\applyRecord.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] applyRecord.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cbea4e66", Component.options)
  } else {
    hotAPI.reload("data-v-cbea4e66", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            total: '',
            pageNum: 1,
            applyRecordList: []
        };
    },
    created: function created() {
        var _this = this;
        _this.init();
    },

    methods: {
        init: function init() {
            var _this = this;
            _this.detailsList();
        },
        detailsList: function detailsList() {
            var _this = this;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posApply/applyList",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    pageNum: _this.pageNum
                }
            }, function (data) {
                _this.applyRecordList = data.data.posApplyRecordList;
                _this.total = data.data.total;
                for (var i in _this.applyRecordList) {
                    _this.applyRecordList[i].details = '查看详情';
                }
            });
        },
        applyDetails: function applyDetails(item, index) {
            if (item.applyStatus == 'PENDING') {
                this.$router.push({
                    "path": "changePending",
                    "query": {
                        Data: JSON.stringify(item)
                    }
                });
            }
            if (item.applyStatus == 'SUCCESS') {
                this.$router.push({
                    "path": "changeAgreed",
                    "query": {
                        Data: JSON.stringify(item)
                    }
                });
            }
            if (item.applyStatus == 'FAIL') {
                this.$router.push({
                    "path": "changeRejected",
                    "query": {
                        Data: JSON.stringify(item)
                    }
                });
            }
        }
    }
};

/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.applyRecord {\n  width: 100%;\n  overflow: hidden;\n  background: #FFFFFF;\n  /*弹窗*/\n}\n.applyRecord .applyRecord-total {\n  padding: 0.2rem 0.293rem;\n  height: 0.793rem;\n  font-size: 0.28rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #3F3F50;\n  line-height: 0.793rem;\n  border-top: 0.013rem solid #F7F8FA;\n}\n.applyRecord .applyRecord-list {\n  padding: 0.2rem 0.293rem;\n  height: 1.6rem;\n  line-height: 0.9rem;\n  border-bottom: 0.013rem solid #F7F8FA;\n}\n.applyRecord .applyRecord-list .applyRecord-list-top {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.applyRecord .applyRecord-list .applyRecord-list-top .applyTitle {\n  font-size: 0.313rem;\n  font-family: PingFangSC-Medium, PingFang SC;\n  font-weight: 500;\n  color: #333333;\n}\n.applyRecord .applyRecord-list .applyRecord-list-top .applyType {\n  font-size: 0.287rem;\n  font-family: PingFangSC-Medium, PingFang SC;\n  font-weight: 500;\n  color: #2D79FF;\n}\n.applyRecord .applyRecord-list .applyRecord-list-bottom {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.applyRecord .applyRecord-list .applyRecord-list-bottom .applyTime {\n  font-size: 0.16rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #9e9090;\n}\n.applyRecord .applyRecord-list .applyRecord-list-bottom .applyDetails {\n  font-size: 0.29rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #F3782C;\n  width: 2.067rem;\n  height: 0.673rem;\n  border-radius: 0.103rem;\n  border: 0.025rem solid #F3782C;\n  line-height: 0.573;\n}\n.applyRecord .dialog {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.applyRecord .dialog .dialog1 {\n  width: 82%;\n  margin: 3.2rem 9% 0;\n  background: #ffffff;\n  height: 4.4rem;\n  border-radius: 0.04rem;\n}\n.applyRecord .dialog .dialog1 .dia_title {\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  padding-top: 0.64rem;\n  font-weight: bold;\n}\n.applyRecord .dialog .dialog1 .dia_text {\n  width: 80%;\n  margin: 0.4rem 10% 0.48rem;\n  opacity: 0.8;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  text-align: left;\n  line-height: 0.48rem;\n  font-weight: 100;\n  font-weight: bold;\n}\n.applyRecord .dialog .dialog1 button {\n  width: 80%;\n  margin: 0 10%;\n  background: #3F83FF;\n  border-radius: 0.06rem;\n  font-size: 0.32rem;\n  color: #FFFFFF;\n  text-align: center;\n  line-height: 0.8rem;\n  height: 0.8rem;\n  float: left;\n  font-weight: bold;\n}\n.applyRecord .dialog .dialog2 {\n  width: 82%;\n  margin: 1.38rem 9% 0;\n}\n.applyRecord .dialog .dialog2 .dia2_can {\n  float: right;\n  width: 0.6rem;\n  height: 0.6rem;\n  margin: 0 0 0.16rem;\n}\n.applyRecord .dialog .dialog2 .dialog2_main {\n  width: 100%;\n  background: #FFFFFF;\n  border-radius: 0.04rem;\n  height: 7.28rem;\n  display: inline-block;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_title {\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  padding-top: 0.64rem;\n  font-weight: bold;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_text {\n  margin: 0 10% 0rem;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  text-align: left;\n  font-weight: bold;\n  width: 80%;\n  float: left;\n  line-height: 0.98rem;\n  display: inline-block;\n  border-bottom: 0.02rem solid #ECF0FF;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_input {\n  margin: 0.15rem 10% 0rem;\n  width: 80%;\n  height: 0.8rem;\n  display: inline-block;\n  float: left;\n  border-bottom: 0.02rem solid #ECF0FF;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_input input {\n  width: 2.92rem;\n  font-size: 0.28rem;\n  height: 0.8rem;\n  display: inline-block;\n  float: left;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_input .yzm {\n  width: 1.96rem;\n  height: 0.8rem;\n  float: right;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_input .yzm img {\n  width: 100%;\n  height: 100%;\n  display: block;\n}\n.applyRecord .dialog .dialog2 .dialog2_main .dia_input span {\n  font-size: 0.32rem;\n  color: #3F83FF;\n  float: right;\n  margin-top: 0.16rem;\n}\n.applyRecord .dialog .dialog2 .dialog2_main button {\n  width: 80%;\n  margin: 0 10%;\n  background: #3F83FF;\n  border-radius: 0.06rem;\n  font-size: 0.32rem;\n  color: #FFFFFF;\n  text-align: center;\n  line-height: 0.8rem;\n  height: 0.8rem;\n  float: left;\n  font-weight: bold;\n  margin-top: 0.48rem;\n}\n.applyRecord .queryHead {\n  width: 100%;\n  overflow: hidden;\n}\n.applyRecord .queryHead .queryHead_menu {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n  overflow: hidden;\n  position: relative;\n}\n.applyRecord .queryHead .queryHead_menu:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #ECF0FF !important;\n  width: 100%;\n  height: 0.02rem;\n  -webkit-transform: scaleY(0.5);\n  -ms-transform: scaleY(0.5);\n  transform: scaleY(0.5);\n}\n.applyRecord .queryHead .queryHead_menu:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.applyRecord .queryHead .queryHead_menu:last-child:after {\n  content: '';\n  background: none;\n}\n.applyRecord .queryHead .queryHead_menu a {\n  color: #3D4A5B;\n  font-size: 0.28rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 100%;\n  text-align: center;\n  position: relative;\n  line-height: 0.8rem;\n  opacity: 0.6;\n}\n.applyRecord .queryHead .queryHead_menu a.active {\n  font-size: 0.32rem;\n  color: #3F83FF;\n  font-weight: 700;\n  opacity: 1 !important;\n}\n.applyRecord .queryHead .queryHead_menu a.active:after {\n  content: \"\";\n  position: absolute;\n  bottom: 0;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  width: 0.6rem;\n  height: 0.06rem;\n  background: #3F83FF;\n}\n", ""]);

// exports


/***/ }),

/***/ 439:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "applyRecord"
  }, [_c('div', {
    staticClass: "applyRecord-total"
  }, [_vm._v("共计"), _c('span', [_vm._v(_vm._s(_vm.total))]), _vm._v("条")]), _vm._v(" "), _vm._l((_vm.applyRecordList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "applyRecord-list"
    }, [_c('div', {
      staticClass: "applyRecord-list-top"
    }, [_c('div', {
      staticClass: "applyTitle"
    }, [_vm._v(_vm._s(item.productName || '---'))]), _vm._v(" "), _c('div', {
      staticClass: "applyType"
    }, [_vm._v(_vm._s(item.applyStatus == 'PENDING' ? '待处理' : item.applyStatus == 'SUCCESS' ? '已同意' : '已驳回'))])]), _vm._v(" "), _c('div', {
      staticClass: "applyRecord-list-bottom"
    }, [_c('div', {
      staticClass: "applyTime"
    }, [_vm._v("申请时间：" + _vm._s(item.applyTime))]), _vm._v(" "), _c('button', {
      staticClass: "applyDetails",
      on: {
        "click": function($event) {
          return _vm.applyDetails(item, index)
        }
      }
    }, [_vm._v(_vm._s(item.details))])])])
  })], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-cbea4e66", module.exports)
  }
}

/***/ }),

/***/ 463:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(401);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("593ff809", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cbea4e66!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./applyRecord.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cbea4e66!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./applyRecord.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});