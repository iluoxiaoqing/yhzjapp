webpackJsonp([125],{

/***/ 1402:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	data: function data() {
		return {
			hotList: [],
			imgUrl: _apiConfig2.default.KY_IP,
			page: 1
		};
	},

	components: {
		noData: _noData2.default,
		freshToLoadmore: _freshToLoadmore2.default
	},
	mounted: function mounted() {
		this.getHotList();
	},

	methods: {
		getHotList: function getHotList() {
			var _this = this;

			common.Ajax({
				url: _apiConfig2.default.KY_IP + "bussMenu/hotBusiness",
				data: {
					"page": 1
				},
				showLoading: false
			}, function (data) {
				_this.hotList = data.data;
				console.log("hotList", _this.hotList);
			});
		},
		apply: function apply(goodsType) {
			common.Ajax({
				url: _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
				data: {
					address: "",
					goods: JSON.stringify([{ type: goodsType, count: 1 }]),
					payAmount: 0,
					accAmount: 0
				},
				showLoading: false
			}, function (data) {
				console.log("返回值", data);
				window.location.href = data.data.ky_no_pos;
			});
		},
		gotoMall: function gotoMall() {
			common.youmeng("备货开店", "点击备货开店");
			this.$router.push({
				"path": "mall"
			});
		},
		share: function share(i) {
			console.log("分享", "分享");
			this.$router.push({
				"path": "share",
				"query": {
					"product": i.product.shareCode,
					"productName": i.product.title,
					"channel": "hotProduct"
				}
			});
		}
	}
};

/***/ }),

/***/ 1599:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\hotProduct\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1912:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hot_main"
  }, _vm._l((_vm.hotList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "mainList"
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "goodsImg"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.product.pic
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "goodsDeatil"
    }, [_c('b', [_vm._v(_vm._s(i.product.title))]), _vm._v(" "), _c('div', {
      staticClass: "number"
    }, [_c('i', [_vm._v("费率：" + _vm._s(i.product.fee))])]), _vm._v(" "), _c('p', [_vm._v(_vm._s(i.hotDesc))]), _vm._v(" "), _c('button', {
      on: {
        "click": function($event) {
          return _vm.share(i)
        }
      }
    }, [_vm._v(_vm._s(i.product.shareBtnText))]), _vm._v(" "), (i.product.needBuyMall == true) ? _c('button', {
      staticClass: "gray",
      on: {
        "click": function($event) {
          return _vm.gotoMall()
        }
      }
    }, [_vm._v("\n                            " + _vm._s(i.product.buyBtnText) + "\n                    ")]) : _c('button', {
      staticClass: "gray",
      on: {
        "click": function($event) {
          return _vm.apply(i.product.buyCode)
        }
      }
    }, [_vm._v("\n                        " + _vm._s(i.product.buyBtnText) + "\n                    ")])])]), _vm._v(" "), _c('div', {
      staticClass: "company"
    }, [_vm._v("\n                " + _vm._s(i.product.company) + "\n            ")])])
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-8895f322", module.exports)
  }
}

/***/ }),

/***/ 2060:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1599);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("48f28188", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8895f322&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hotProduct.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8895f322&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hotProduct.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 598:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2060)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1402),
  /* template */
  __webpack_require__(1912),
  /* scopeId */
  "data-v-8895f322",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\hotProduct\\hotProduct.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] hotProduct.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8895f322", Component.options)
  } else {
    hotAPI.reload("data-v-8895f322", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});