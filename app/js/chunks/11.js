webpackJsonp([11],{

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(454)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(373),
  /* template */
  __webpack_require__(430),
  /* scopeId */
  "data-v-51a47fea",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\shop\\adressList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] adressList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-51a47fea", Component.options)
  } else {
    hotAPI.reload("data-v-51a47fea", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

var _vant = __webpack_require__(66);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            addArray: [],
            id: ''
        };
    },
    created: function created() {
        this.init();
    },

    methods: {
        init: function init() {
            var _this = this;
            _this.addArray = [];
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/list",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    pageNum: _this.pageNum
                }
            }, function (data) {
                _this.addArray = data.data;
            });
        },
        onAddress: function onAddress() {
            this.$router.push({
                "path": "addAdress"
            });
        },
        onEditor: function onEditor(item, index) {
            this.$router.push({
                "path": "editorAdress",
                "query": {
                    Data: JSON.stringify(item)
                }
            });
        },
        deleteList: function deleteList(item, index) {
            var _this = this;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/update",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    receivePerson: item.receivePerson,
                    phoneNo: item.phoneNo,
                    provinceCode: item.provinceCode,
                    province: item.province,
                    cityCode: item.cityCode,
                    city: item.city,
                    areaCode: item.areaCode,
                    area: item.area,
                    addressDetail: item.addressDetail,
                    isDefault: item.isDefault,
                    addressId: item.id,
                    status: 'DISABLE'

                }
            }, function (data) {
                if (data.code == '0000') {
                    _this.init();
                    common.toast({
                        content: '删除成功'
                    });
                }
            });
        },
        defaultList: function defaultList(item, index) {
            var _this = this;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/update",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    receivePerson: item.receivePerson,
                    phoneNo: item.phoneNo,
                    provinceCode: item.provinceCode,
                    province: item.province,
                    cityCode: item.cityCode,
                    city: item.city,
                    areaCode: item.areaCode,
                    area: item.area,
                    addressDetail: item.addressDetail,
                    isDefault: 'true',
                    addressId: item.id,
                    status: 'ENABLE'

                }
            }, function (data) {
                if (data.code == '0000') {
                    _this.init();
                }
            });
        },
        clickJump: function clickJump(item, index) {
            this.$router.push({
                "path": "machinesToolsIsApply",
                "query": {
                    "addressId": item.id
                }
            });
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 392:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-51a47fea] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-51a47fea] {\n  background: #fff;\n}\n.tips_success[data-v-51a47fea] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-51a47fea] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-51a47fea] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-51a47fea],\n.fade-leave-active[data-v-51a47fea] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-51a47fea],\n.fade-leave-to[data-v-51a47fea] {\n  opacity: 0;\n}\n.default_button[data-v-51a47fea] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-51a47fea] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-51a47fea] {\n  position: relative;\n}\n.loading-tips[data-v-51a47fea] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.addressMy[data-v-51a47fea] {\n  width: 100%;\n  margin-bottom: 1.6rem;\n  overflow: hidden;\n}\n.addressMy .content[data-v-51a47fea] {\n  width: 100vw;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0.4rem 0rem;\n}\n.addressMy .content .content-left[data-v-51a47fea] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n  -webkit-align-items: flex-start;\n     -moz-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  min-width: 6.16rem;\n  margin-left: 0.32rem;\n}\n.addressMy .content .content-left .content-left-top[data-v-51a47fea] {\n  height: 0.34rem;\n  font-size: 0.34rem;\n  font-family: PingFangSC-Medium, PingFang SC;\n  font-weight: 500;\n  color: #333333;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n  -webkit-justify-content: flex-start;\n     -moz-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.addressMy .content .content-left .content-left-top .name[data-v-51a47fea] {\n  font-size: 0.34rem;\n  font-family: PingFangSC-Medium, PingFang SC;\n  font-weight: 500;\n  color: #333333;\n}\n.addressMy .content .content-left .content-left-top .mobile[data-v-51a47fea] {\n  margin: 0rem 0.36rem 0rem 0.32rem;\n  font-size: 0.34rem;\n  font-family: PingFangSC-Medium, PingFang SC;\n  font-weight: 500;\n  color: #333333;\n}\n.addressMy .content .content-left .content-left-top .default[data-v-51a47fea] {\n  width: 0.6rem;\n  height: 0.34rem;\n  background: url(" + __webpack_require__(420) + ") no-repeat;\n  background-size: 0.6rem 0.34rem;\n}\n.addressMy .content .content-left .content-left-bottom[data-v-51a47fea] {\n  min-height: 0.24rem;\n  font-size: 0.24rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #858585;\n  margin-top: 0.16rem;\n}\n.addressMy .content .content-right[data-v-51a47fea] {\n  width: 0.38rem;\n  height: 0.38rem;\n  background: url(" + __webpack_require__(419) + ") no-repeat;\n  background-size: 0.38rem 0.38rem;\n  margin-left: 0.32rem;\n  margin-right: 0.32rem;\n}\n.addressMy .line[data-v-51a47fea] {\n  width: 6.88rem;\n  height: 0.02rem;\n  background: rgba(51, 51, 51, 0.1);\n  position: absolute;\n  bottom: 0;\n  margin-left: 0.32rem;\n}\n.addressMy .right[data-v-51a47fea] {\n  height: 100%;\n  line-height: 0px;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.addressMy .right .van-button[data-v-51a47fea] {\n  border: 0px;\n  height: 100%;\n  width: 1.4rem;\n  padding: 0;\n}\n.addressMy .right .van-button .van-button__text[data-v-51a47fea] {\n  font-size: 0.28rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #FFFFFF;\n}\n.addressMy .right .van-button[data-v-51a47fea]:nth-child(1) {\n  background: #C9C6C5;\n}\n.addressMy .right .van-button[data-v-51a47fea]:nth-child(2) {\n  background: #FF8C4B;\n}\n.addressMy .btn[data-v-51a47fea] {\n  height: 1rem;\n  width: 100%;\n  background: #FF6D0C;\n  line-height: 1rem;\n  text-align: center;\n  font-size: 0.32rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #FFFFFF;\n  position: fixed;\n  bottom: 0;\n}\n", ""]);

// exports


/***/ }),

/***/ 419:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/bj@2x.png?v=351c2175";

/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/default.png?v=d43c65ec";

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "addressMy"
  }, [_vm._l((_vm.addArray), function(item, index) {
    return _c('van-swipe-cell', {
      key: index,
      scopedSlots: _vm._u([{
        key: "right",
        fn: function() {
          return [_c('div', {
            staticClass: "right"
          }, [_c('van-button', {
            attrs: {
              "square": "",
              "type": "danger",
              "text": "设置默认"
            },
            on: {
              "click": function($event) {
                return _vm.defaultList(item, index)
              }
            }
          }), _vm._v(" "), _c('van-button', {
            attrs: {
              "square": "",
              "type": "primary",
              "text": "删除"
            },
            on: {
              "click": function($event) {
                return _vm.deleteList(item, index)
              }
            }
          })], 1)]
        },
        proxy: true
      }], null, true)
    }, [_c('div', {
      staticClass: "content"
    }, [_c('div', {
      staticClass: "content-left",
      on: {
        "click": function($event) {
          return _vm.clickJump(item, index)
        }
      }
    }, [_c('div', {
      staticClass: "content-left-top"
    }, [_c('div', {
      staticClass: "name"
    }, [_vm._v(_vm._s(item.receivePerson))]), _vm._v(" "), _c('div', {
      staticClass: "mobile"
    }, [_vm._v(_vm._s(_vm.$startPhone(item.phoneNo)))]), _vm._v(" "), (item.isDefault) ? _c('div', {
      staticClass: "default"
    }) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "content-left-bottom"
    }, [_vm._v("\n                    " + _vm._s(item.province) + _vm._s(item.city) + _vm._s(item.area) + _vm._s(item.addressDetail) + "\n                ")])]), _vm._v(" "), _c('div', {
      staticClass: "content-right",
      on: {
        "click": function($event) {
          return _vm.onEditor(item, index)
        }
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "line"
    })])
  }), _vm._v(" "), _c('div', {
    staticClass: "btn",
    on: {
      "click": _vm.onAddress
    }
  }, [_vm._v("新增收货地址")])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-51a47fea", module.exports)
  }
}

/***/ }),

/***/ 454:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(392);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("e7ad5002", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-51a47fea&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-51a47fea&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});