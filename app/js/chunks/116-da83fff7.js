webpackJsonp([116],{

/***/ 1443:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            shopGroups: [],
            eyetxt: "",
            context: '',
            title: "",
            seen: '',
            unseenImg: "./image/no_see.png", //看不见
            seenImg: "./image/see_icon.png", //看得见密码
            pwdType: false //此时文本框隐藏，显示密码框 	
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        this.getList();
    },

    methods: {
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/autoShopBusiness",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this.shopGroups = data.data.groups;
                console.log("shopGroups", _this.shopGroups);
            });
        },
        gotoMall: function gotoMall() {
            window.location.href = _apiConfig2.default.WEB_URL + 'cxDownMall';
        },

        //密码的显示隐藏
        changeType: function changeType() {
            this.seen = !this.seen; //小眼睛的变化
            this.pwdType = !this.pwdType; //跟着小眼睛变化，密码框隐藏显示文本框，内容就显示了
        }
    }
};

/***/ }),

/***/ 1550:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1864:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mallShop"
  }, _vm._l((_vm.shopGroups), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "main"
    }, [_c('div', {
      staticClass: "title"
    }, [_vm._v("\n\t\t\t\t" + _vm._s(i.name) + "\n\t\t\t")]), _vm._v(" "), _vm._l((i.details), function(j, index) {
      return _c('div', {
        staticClass: "shopMain"
      }, [(i.hasOnline == true) ? _c('div', {
        staticClass: "shopTop"
      }, [_c('div', {
        staticClass: "listImg"
      }, [_c('img', {
        attrs: {
          "src": _vm.imgUrl + 'file/downloadFile?filePath=' + j.logo
        }
      })]), _vm._v(" "), _c('div', {
        staticClass: "listDeatil"
      }, [_c('div', {
        staticClass: "titleLeft"
      }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
        staticClass: "titleRight"
      }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.fee) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
        staticClass: "acFee"
      }, [_vm._v(_vm._s(j.activityFee))]), _vm._v(" "), _c('div', {
        staticClass: "time"
      }, [_c('i', [_vm._v(_vm._s(j.company))]), _vm._v(" "), _c('i', [_vm._v(_vm._s(j.feature))]), _vm._v(" "), (j.share == true) ? _c('div', {
        staticClass: "aaaa"
      }, [_c('a', {
        attrs: {
          "href": "javascript:;"
        },
        on: {
          "click": function($event) {
            return _vm.share(j)
          }
        }
      }, [_vm._v(_vm._s(j.shareBtnText))])]) : _vm._e()])])]) : _vm._e(), _vm._v(" "), (i.hasOnline == true) ? _c('div', {
        staticClass: "shopBottom"
      }, [(j.showBuyBtnText == true) ? _c('div', {
        on: {
          "click": function($event) {
            return _vm.gotoMall()
          }
        }
      }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(j.openBtnText) + "\n\t\t\t\t\t")]) : _c('div', [_c('span', [_vm._v("登录诚享Plus初始密码：")]), _vm._v(" "), (_vm.pwdType) ? _c('input', {
        directives: [{
          name: "model",
          rawName: "v-model",
          value: (j.downBtnText),
          expression: "j.downBtnText"
        }],
        attrs: {
          "type": "text"
        },
        domProps: {
          "value": (j.downBtnText)
        },
        on: {
          "input": function($event) {
            if ($event.target.composing) { return; }
            _vm.$set(j, "downBtnText", $event.target.value)
          }
        }
      }) : _c('input', {
        directives: [{
          name: "model",
          rawName: "v-model",
          value: (j.downBtnText),
          expression: "j.downBtnText"
        }],
        attrs: {
          "type": "password"
        },
        domProps: {
          "value": (j.downBtnText)
        },
        on: {
          "input": function($event) {
            if ($event.target.composing) { return; }
            _vm.$set(j, "downBtnText", $event.target.value)
          }
        }
      }), _vm._v(" "), _c('img', {
        staticClass: "eye_img",
        attrs: {
          "src": _vm.seen ? './image/see_icon.png' : './image/no_see.png'
        },
        on: {
          "click": function($event) {
            return _vm.changeType()
          }
        }
      })])]) : _vm._e()])
    })], 2)
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-421857f8", module.exports)
  }
}

/***/ }),

/***/ 2011:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1550);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("cebe96d8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-421857f8&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-421857f8&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 647:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2011)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1443),
  /* template */
  __webpack_require__(1864),
  /* scopeId */
  "data-v-421857f8",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\cxShop.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] cxShop.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-421857f8", Component.options)
  } else {
    hotAPI.reload("data-v-421857f8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});