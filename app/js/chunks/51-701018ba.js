webpackJsonp([51],{

/***/ 1474:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _common2 = __webpack_require__(7);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            scrollArray: [],
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            showBtn: true
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.showBtn = this.$route.params.showBtn == null ? true : this.$route.params.showBtn;
        // alert(this.showBtn);
        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "recommOrder/scroll",
            "type": "GET"
        }, function (data) {
            console.log(data);
            _this.scrollArray = data.data;
            _this.$nextTick(function () {
                var mySwiper = new Swiper('.swiper-container', {
                    autoplay: 2500, //可选选项，自动滑动
                    loop: true,
                    noSwiping: true,
                    pagination: ".swiper-pagination",
                    direction: 'vertical'
                });
            });
        });

        common.youmeng("推手权益介绍", "进入推手权益介绍");
    },

    methods: {
        gotoPay: function gotoPay() {

            common.youmeng("推手权益介绍", "点击我要当推手");

            this.$router.push({
                path: "upgradeRight"
            });
        }
    }
};

/***/ }),

/***/ 1563:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1767:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_people1.png?v=35d3abd1";

/***/ }),

/***/ 1768:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_people2.png?v=7e6721b6";

/***/ }),

/***/ 1876:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "upgradeRight_ad"
  }, [_c('div', {
    staticClass: "ad_banner"
  }, [_c('div', {
    staticClass: "ad_swiper"
  }, [_c('div', {
    staticClass: "ad_content"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.scrollArray), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide swiper-no-swiping"
    }, [_c('p', [_vm._v(_vm._s(i))])])
  }), 0)])])])]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "ad_channel"
  }, [_vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showBtn !== 'false'),
      expression: "showBtn!=='false'"
    }],
    staticClass: "ad_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('a', {
    attrs: {
      "href": "javascript:;"
    }
  }, [_vm._v("我要当推手")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ad_channel"
  }, [_c('div', {
    staticClass: "title"
  }, [_c('p', [_c('span'), _vm._v(" "), _c('b', [_vm._v("四大收益渠道")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('i', [_vm._v("样样带你创新高")])]), _vm._v(" "), _c('div', {
    staticClass: "channel_content"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "item_top"
  }), _vm._v(" "), _c('div', {
    staticClass: "item_title"
  }, [_c('span'), _vm._v(" "), _c('p', [_vm._v("推荐办卡")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('span', [_vm._v("佣金最高达")]), _vm._v(" "), _c('b', [_vm._v("400元/张")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("卡种全")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("高收益")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("结算快")])])]), _vm._v(" "), _c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "item_top"
  }), _vm._v(" "), _c('div', {
    staticClass: "item_title"
  }, [_c('span'), _vm._v(" "), _c('p', [_vm._v("推荐开店")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('span', [_vm._v("佣金最高达")]), _vm._v(" "), _c('b', [_vm._v("14元/交易1万")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("多品牌")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("高收益")])])]), _vm._v(" "), _c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "item_top"
  }), _vm._v(" "), _c('div', {
    staticClass: "item_title"
  }, [_c('span'), _vm._v(" "), _c('p', [_vm._v("推荐贷款")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('span', [_vm._v("佣金最高达")]), _vm._v(" "), _c('b', [_vm._v("贷款金额3%")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("多品牌")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("高收益")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("不扣量")])])]), _vm._v(" "), _c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "item_top"
  }), _vm._v(" "), _c('div', {
    staticClass: "item_title"
  }, [_c('span'), _vm._v(" "), _c('p', [_vm._v("邀请推手")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('span', [_vm._v("佣金最高达")]), _vm._v(" "), _c('b', [_vm._v("100元/人")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("素材丰富")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("社交升级")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("永久锁粉")])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "title"
  }, [_c('p', [_c('span', {
    staticStyle: {
      "width": "1.06rem"
    }
  }), _vm._v(" "), _c('b', [_vm._v("白领和自由职业者")]), _vm._v(" "), _c('span', {
    staticStyle: {
      "width": "1.06rem"
    }
  })]), _vm._v(" "), _c('i', [_vm._v("都在逍遥推手")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ad_person"
  }, [_c('div', {
    staticClass: "person_item"
  }, [_c('div', {
    staticClass: "person_img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1767)
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "introduct"
  }, [_c('h2', [_c('i', [_vm._v("张  潇")]), _c('span', [_vm._v("销售")])]), _vm._v(" "), _c('p', [_vm._v("加入原因：利用业余时间赚大钱")])])]), _vm._v(" "), _c('div', {
    staticClass: "person_item"
  }, [_c('div', {
    staticClass: "person_img"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1768)
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "introduct"
  }, [_c('h2', [_c('i', [_vm._v("周天圣")]), _c('span', [_vm._v("个体老板")])]), _vm._v(" "), _c('p', [_vm._v("加入原因：时间自由，比上班赚的多")])])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-50e0bcba", module.exports)
  }
}

/***/ }),

/***/ 2024:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1563);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("5b26f18a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50e0bcba&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight_ad.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50e0bcba&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight_ad.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 682:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2024)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1474),
  /* template */
  __webpack_require__(1876),
  /* scopeId */
  "data-v-50e0bcba",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\upgradeRight_ad.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] upgradeRight_ad.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-50e0bcba", Component.options)
  } else {
    hotAPI.reload("data-v-50e0bcba", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});