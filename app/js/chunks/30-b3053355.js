webpackJsonp([30],{

/***/ 1243:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1246)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(960),
  /* template */
  __webpack_require__(1244),
  /* scopeId */
  "data-v-21be181a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox4.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox4.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-21be181a", Component.options)
  } else {
    hotAPI.reload("data-v-21be181a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 1244:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-21be181a", module.exports)
  }
}

/***/ }),

/***/ 1246:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(962);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("69b749b1", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21be181a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox4.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21be181a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox4.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1404:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(1243);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = {
    components: {
        alertBox: _alertBox2.default,
        searchBox: _searchBox2.default,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    data: function data() {
        return _defineProperty({
            liushiArray: [{ "select": true, "content": "全部", "detailCode": "ALL" }, { "select": false, "content": "未绑定", "detailCode": "INIT" }, { "select": false, "content": "已绑定", "detailCode": "BIND" }, { "select": false, "content": "已激活", "detailCode": "AWARDED" }],
            activeStateArray: [{ "select": true, "content": "全部", "detailCode": "ALL" }, { "select": false, "content": "未过期", "detailCode": "NOEXPIRE" }, { "select": false, "content": "已过期", "detailCode": "EXPIRE" }, { "select": false, "content": "30天过期", "detailCode": "30EXPIRE" }],
            menuList: [{
                producName: "嘉联电签",
                productCode: "aa",
                active: true
            }, {
                producName: "拉卡拉押金",
                productCode: "bb",
                active: false
            }, {
                producName: "嘉联电签",
                productCode: "cc",
                active: false
            }, {
                producName: "拉卡拉押金",
                productCode: "dd",
                active: false
            }],
            posNoStart: "", //起始机具编号
            posNoEnd: "", //结束机具编号
            totalNum: "0", //机具台数
            poslist: [], //机具列表
            carList: [{ posSn: "000000302Q3NL0503", expireTime: "2020-12-11" }, { posSn: "000000302Q3NL0503", expireTime: "2020-12-11" }, { posSn: "000000302Q3NL0503", expireTime: "2020-12-11" }], //弹窗机具列表
            showCar: false,
            diaMenuList: [{
                producName: "全部",
                productCode: "aa",
                active: true
            }, {
                producName: "拉卡拉押金",
                productCode: "bb",
                active: false
            }],
            productCode: "",
            showMenu: false,
            showIncomePay: false,
            shaixunMenu: false,
            liushi: false,
            defaultText: "",
            isNoData: false,
            hasData: false,
            sortMode: "DESC",
            sortType: "OPEN_TIME",
            potentialType: "UN_SELECT",
            activeState: "UN_SELECT",
            currentPage: 1
        }, "isNoData", false);
    },
    mounted: function mounted() {
        this.poslist = JSON.parse(sessionStorage.getItem("diaList"));
        this.totalNum = this.poslist.length;
        console.log("list2数据=====", this.poslist);
        // this.$refs.searchBox4.inputValue = "";
        // let bodyHeight = document.documentElement.clientHeight;
        // let scroller = document.getElementById("pull-wrapper");
        // let scrollerTop = scroller.getBoundingClientRect().top;
        // scroller.style.height = (bodyHeight - scrollerTop) + "px";
        // this.getMenuList(); //获取菜单列表
        // this.getMachineList(); //获取机具列表
        // window.freshWeb = this.freshWeb;
        window.goNew = this.goNew;
    },

    methods: {
        //清空按钮操作
        empty: function empty() {
            var json = {
                "callbackName": "freshWeb"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.openSubmitWindow.postMessage(JSON.stringify(json));
            } else {
                window.android.openSubmitWindow(JSON.stringify(json));
            }
        },
        cancleYes: function cancleYes() {
            var path = _apiConfig2.default.WEB_URL + 'conTransfer';;
            var json = {
                "path": path
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.openEmptyWindow.postMessage(JSON.stringify(json));
            } else {
                window.android.openEmptyWindow(JSON.stringify(json));
            }
        },

        // 获取顶部菜单列表
        getMenuList: function getMenuList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "terminalManagement/findProductList",
                "type": "post",
                "data": {}
            }, function (data) {
                // console.log(data.data);
                var _arr = data.data;
                _arr.map(function (item, index) {
                    _arr.push(Object.assign({}, item, { active: 'false' }));
                });
                _arr[0].active = 'true';
                _this.menuList = _arr;
                _this.productCode = _this.menuList[0].productCode;
            });
        },

        // 获取机具列表
        getMachineList: function getMachineList() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "terminalManagement/findPosList",
                "type": "post",
                "data": {
                    "productCode": this.productCode,
                    "posStatus": this.$refs.searchBox4.inputValue, //机具状态
                    "activityTime": "", //激活有效时间
                    "posNoStart": "", //起始机具编号
                    "posNoEnd": "" //结束机具编号
                }
            }, function (data) {
                _this2.totalNum = data.totalNum;
                var _arr = data.poslist;
                _arr.map(function (item, index) {
                    _arr.push(Object.assign({}, item, { isSelected: 'false' }));
                });
                _this2.menuList = _arr;
                _this2.poslist = data.poslist;
                if (data.poslist.object.length > 0) {
                    if (_this2.currentPage == 1) {
                        _this2.poslist = [];
                    }
                    data.poslist.object.map(function (el) {
                        _this2.$set(el, "show", true);
                        _this2.poslist.push(el);
                    });

                    _this2.isNoData = false;
                    _this2.currentPage++;
                } else {
                    if (_this2.currentPage == 1) {
                        _this2.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
            });
        },

        //客户端回调页面
        freshWeb: function freshWeb() {
            location.reload();
        },

        //删除机具
        reduceGoods: function reduceGoods(i) {
            this.poslist.splice(i, 1);
            console.log("删除后数组数据为===", this.poslist);
            this.totalNum = this.poslist.length;
            if (this.poslist.length == 0) {
                this.empty();
            }
            sessionStorage.setItem("diaList", JSON.stringify(this.poslist));
        },
        changeState: function changeState(isChecked) {
            var chk_list = document.getElementsByTagName("input");
            for (var i = 0; i < chk_list.length; i++) {
                if (chk_list[i].type == "checkbox") {
                    chk_list[i].checked = isChecked;
                }
            }
        },
        confirm: function confirm() {
            this.showMenu = false;
            this.currentPage = 1;
            this.poslist = [];
            this.getMerchantList();
            // this.again();
        },
        searchBtn: function searchBtn(search) {
            this.currentPage = 1;
            this.poslist = [];
            this.getMerchantList();
        }
    }

};

/***/ }),

/***/ 1596:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\machines\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1909:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "merchant_main"
  }, [_c('div', {
    staticClass: "buyList"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "menu"
  }, [_c('ul', _vm._l((_vm.diaMenuList), function(itm, idx) {
    return _c('li', {
      class: {
        'active': itm.active
      },
      on: {
        "click": function($event) {
          return _vm.changeTab(idx + 1, _vm.productCode)
        }
      }
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(itm.producName) + "\n\t\t\t\t\t\t")])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "buyList_body",
    class: {
      'scroll': _vm.poslist.length > 4
    }
  }, _vm._l((_vm.poslist), function(i) {
    return _c('div', {
      key: i,
      staticClass: "item"
    }, [_c('div', {
      staticClass: "list"
    }, [_c('div', {
      staticClass: "list_left"
    }, [_c('span', [_vm._v(_vm._s(i.posSn))]), _vm._v(" "), _c('i', [_vm._v("有效期：" + _vm._s(i.expireTime))])]), _vm._v(" "), _c('div', {
      staticClass: "list_right"
    }, [_c('em', [_c('a', {
      on: {
        "click": function($event) {
          return _vm.reduceGoods(i)
        }
      }
    })])])])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "buy_bot"
  }, [_c('button', {
    staticClass: "empty",
    on: {
      "click": function($event) {
        return _vm.empty()
      }
    }
  }, [_vm._v("清空")]), _vm._v(" "), _c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.cancleYes()
      }
    }
  }, [_vm._v("确认划拨"), _c('span', [_vm._v(_vm._s(_vm.totalNum))]), _vm._v("台")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "buyList_head"
  }, [_c('p', [_vm._v("共计12台")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7e5734d0", module.exports)
  }
}

/***/ }),

/***/ 2057:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1596);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("8abd384a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7e5734d0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./diaPage.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7e5734d0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./diaPage.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 600:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2057)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1404),
  /* template */
  __webpack_require__(1909),
  /* scopeId */
  "data-v-7e5734d0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\machines\\diaPage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] diaPage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7e5734d0", Component.options)
  } else {
    hotAPI.reload("data-v-7e5734d0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 960:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 962:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ })

});