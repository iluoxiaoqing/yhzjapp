webpackJsonp([97],{

/***/ 1383:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            rankList: [],
            showLoading: true,
            rankOneself: ""
        };
    },
    mounted: function mounted() {
        this.getList();
    },

    methods: {
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "activation/rank",
                "type": "post",
                "data": {}
            }, function (data) {
                console.log(data);
                _this.rankList = data.data.rankList;
                _this.rankOneself = data.data.rankOneself;
            });
        }
    }
};

/***/ }),

/***/ 1598:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\businessRank\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1654:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/empty.png?v=4a57060c";

/***/ }),

/***/ 1911:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "total"
  }, [_c('div', {
    staticClass: "top-con"
  }), _vm._v(" "), _c('div', {
    staticClass: "detail-con"
  }, [_c('div', {
    staticClass: "ownRank"
  }, [_c('div', {
    staticClass: "ownMain"
  }, [_c('div', {
    staticClass: "ownleft"
  }, [_c('span', [_vm._v(_vm._s(_vm.rankOneself.realName) + "（" + _vm._s(_vm.rankOneself.phoneNo) + "）")]), _c('br'), _vm._v("\n                        激活台数：" + _vm._s(_vm.rankOneself.activationNum) + "台\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "ownright"
  }, [_vm._v("\n                        昨日排行"), _c('br'), _vm._v("No." + _vm._s(_vm.rankOneself.rank) + "\n                    ")])])]), _vm._v(" "), (_vm.rankList.length > 0) ? _c('div', {
    staticClass: "detail-list"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('ul', _vm._l((_vm.rankList), function(el, i) {
    return _c('li', {
      key: i
    }, [(i == 0 || i == 1 || i == 2) ? _c('div', {
      staticClass: "logo"
    }, [_c('div', {
      staticClass: "logo_top"
    }, [_c('span', [_vm._v("No." + _vm._s(el.rank))]), _vm._v(" "), _c('img', {
      attrs: {
        "src": './image/perImg' + (i + 2) + '.png',
        "alt": ""
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "dis"
    }, [_c('p', [_vm._v(_vm._s(el.realName) + "（" + _vm._s(el.phoneNo) + "）")])])]) : _c('div', {
      staticClass: "logoNum"
    }, [_c('span', [_vm._v("No." + _vm._s(el.rank))]), _c('br'), _vm._v(" "), _c('div', {
      staticClass: "dis"
    }, [_c('p', [_vm._v(_vm._s(el.realName) + "（" + _vm._s(el.phoneNo) + "）")])])]), _vm._v(" "), _c('div', {
      staticClass: "right"
    }, [_c('p', [_vm._v("昨日激活台数"), _c('br'), _vm._v(" "), _c('span', [_vm._v(_vm._s(el.activationNum))]), _vm._v("台\n                                ")])])])
  }), 0)])]) : _c('div', {
    staticClass: "empty"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1654),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("暂无数据")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-82e99116", module.exports)
  }
}

/***/ }),

/***/ 2059:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1598);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2ec6bfea", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-82e99116&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activeRank.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-82e99116&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activeRank.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2059)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1383),
  /* template */
  __webpack_require__(1911),
  /* scopeId */
  "data-v-82e99116",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\businessRank\\activeRank.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] activeRank.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-82e99116", Component.options)
  } else {
    hotAPI.reload("data-v-82e99116", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});