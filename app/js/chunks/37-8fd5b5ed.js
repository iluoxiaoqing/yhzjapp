webpackJsonp([37],{

/***/ 1225:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/close_btn.png?v=97f71807";

/***/ }),

/***/ 1242:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1247)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(959),
  /* template */
  __webpack_require__(1245),
  /* scopeId */
  "data-v-5140d656",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\confirmInfo.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] confirmInfo.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5140d656", Component.options)
  } else {
    hotAPI.reload("data-v-5140d656", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 1245:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "confirmInfo"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_mask",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_box"
  }, [_c('div', {
    staticClass: "confirmInfo_close",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_text"
  }, [_c('h1', [_vm._v("确认信息")]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("姓名")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.realname))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("身份证号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.identityNo))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("手机号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.userName))])]), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_line"
  }), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.boxInfo.content))]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.submitBox()
      }
    }
  }, [_vm._v("确认办理")])])]) : _vm._e()])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5140d656", module.exports)
  }
}

/***/ }),

/***/ 1247:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(963);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("48f57cb6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1388:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _confirmInfo = __webpack_require__(1242);

var _confirmInfo2 = _interopRequireDefault(_confirmInfo);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            bankList: [],
            showConfirm: false,
            imgUrl: _apiConfig2.default.KY_IP,
            productCode: "",
            bankName: "",
            comfirmText: {
                "content": "请确认以上信息与信用卡申请信息完全一致，填写错误将会导致信用卡申请无法通过，或者无法查询卡片办理。"
            },
            repeatShow: false,
            repeatInfo: {
                "title": "",
                "content": "",
                "confirmText": "我知道了",
                "cancelText": "继续办理"
            }
        };
    },

    components: {
        confirmInfo: _confirmInfo2.default,
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        var _this = this;

        window.goBack = this.goBack;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "bussMenu/selfBusiness",
            "data": {
                "buss": "CREDIT_CARD_CODE"
            }
        }, function (data) {
            _this.bankList = data.data.groups;
            _this.bankList.map(function (el) {
                el.products.map(function (i) {
                    _this.$set(i, "custDesc", JSON.parse(i.custDesc));
                });
            });
            console.log(_this.bankList);
        });
        common.youmeng("推荐办卡", "进入推荐办卡");
    },

    methods: {
        goBack: function goBack() {
            this.$router.push({
                "path": "creditCard"
            });
        },
        confirmCard: function confirmCard(j) {
            var _this2 = this;

            common.youmeng("在线办卡", j.productName);

            this.productCode = j.productCode;
            this.bankName = j.productName;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "user/isShow",
                "data": {
                    "businessCode": "CREDIT_CARD_CODE",
                    "productCode": this.productCode
                }
            }, function (data) {
                if (data.data.isShow) {
                    _this2.repeatShow = true;
                    _this2.repeatInfo.content = data.data.showMessage;
                } else {
                    _this2.showConfirm = true;
                }
            });
        },
        comfirmInfo: function comfirmInfo() {

            common.youmeng("在线办卡", this.bankName + "-确认办理");

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "creditCard/transact",
                "data": {
                    "productCode": this.productCode
                }
            }, function (data) {
                window.location.href = data.data;
            });
        },
        repeatConfirm: function repeatConfirm() {
            this.showConfirm = true;
        }
    }
};

/***/ }),

/***/ 1616:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.onlineCard {\n  width: 100%;\n  overflow: hidden;\n  padding-top: .2rem;\n  background: #fff;\n}\n.onlineCard .onlineCard_title {\n  font-size: .28rem;\n  color: #333;\n  font-weight: 700;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding-left: .30rem;\n}\n.onlineCard .onlineCard_title img {\n  width: 0.312rem;\n  height: 0.336rem;\n  display: block;\n  margin-right: .14rem;\n}\n.onlineCard .onlineCard_content {\n  width: 100%;\n  overflow: hidden;\n  margin-top: .18rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n  position: relative;\n}\n.onlineCard .onlineCard_content:after {\n  content: \"\";\n  position: absolute;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  background: #f4f5fb;\n  left: 0;\n  top: 0;\n}\n.onlineCard .onlineCard_content .item {\n  width: 33.3333%;\n  height: 2.3rem;\n  position: relative;\n  overflow: hidden;\n}\n.onlineCard .onlineCard_content .item .item_tag {\n  width: 1.5rem;\n  height: .28rem;\n  position: absolute;\n  top: .12rem;\n  right: -0.4rem;\n  -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n          transform: rotate(45deg);\n  background: #FF323C;\n}\n.onlineCard .onlineCard_content .item .item_tag span {\n  font-size: .24rem;\n  color: #fff;\n  width: 100%;\n  height: .28rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-transform: scale(0.9);\n      -ms-transform: scale(0.9);\n          transform: scale(0.9);\n}\n.onlineCard .onlineCard_content .item:after {\n  content: \"\";\n  position: absolute;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  background: #f4f5fb;\n  left: 0;\n  bottom: 0;\n}\n.onlineCard .onlineCard_content .item .item_line {\n  width: 100%;\n  height: 100%;\n  padding-top: .4rem;\n  position: relative;\n  overflow: hidden;\n}\n.onlineCard .onlineCard_content .item .item_line img {\n  width: .6rem;\n  height: .6rem;\n  display: block;\n  margin: 0 auto .16rem;\n}\n.onlineCard .onlineCard_content .item .item_line b {\n  display: block;\n  color: #3D4A5B;\n  text-align: center;\n  font-size: .28rem;\n}\n.onlineCard .onlineCard_content .item .item_line p {\n  color: #E95647;\n  text-align: center;\n  font-size: .24rem;\n  white-space: nowrap;\n  -webkit-transform: scale(0.9);\n      -ms-transform: scale(0.9);\n          transform: scale(0.9);\n}\n.onlineCard .onlineCard_content .item .item_line:after {\n  content: \"\";\n  position: absolute;\n  width: 1px;\n  height: 100%;\n  -webkit-transform: scaleX(0.5);\n      -ms-transform: scaleX(0.5);\n          transform: scaleX(0.5);\n  background: #f4f5fb;\n  right: 0;\n  bottom: 0;\n}\n.onlineCard .onlineCard_content .item .item_line.grey {\n  opacity: 0.2;\n  -webkit-filter: grayscale(100%);\n}\n", ""]);

// exports


/***/ }),

/***/ 1659:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/online_fire.png?v=07c30e04";

/***/ }),

/***/ 1929:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "onlineCard"
  }, [_vm._l((_vm.bankList), function(i, index) {
    return _c('div', {
      key: index
    }, [_c('div', {
      staticClass: "onlineCard_title"
    }, [_c('img', {
      attrs: {
        "src": __webpack_require__(1659),
        "alt": ""
      }
    }), _vm._v("\n                " + _vm._s(i.group) + "\n            ")]), _vm._v(" "), _c('div', {
      staticClass: "onlineCard_content"
    }, _vm._l((i.products), function(j, index) {
      return _c('div', {
        key: index,
        staticClass: "item",
        on: {
          "click": function($event) {
            return _vm.confirmCard(j)
          }
        }
      }, [_c('div', {
        staticClass: "item_line",
        class: {
          'grey': !j.isShow
        }
      }, [_c('img', {
        attrs: {
          "src": _vm.imgUrl + 'file/downloadFile?filePath=' + j.logo,
          "alt": ""
        }
      }), _vm._v(" "), _c('b', [_vm._v(_vm._s(j.productName))]), _vm._v(" "), (j.isShow) ? _c('p', [_vm._v(_vm._s(j.custDesc.feature[0]))]) : _vm._e()]), _vm._v(" "), (j.custDesc.label) ? _c('div', {
        staticClass: "item_tag"
      }, [_c('span', [_vm._v(_vm._s(j.custDesc.label))])]) : _vm._e()])
    }), 0)])
  }), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.repeatShow,
      "boxInfo": _vm.repeatInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.repeatShow = $event
      },
      "cancel": _vm.repeatConfirm
    }
  }), _vm._v(" "), _c('confirm-info', {
    attrs: {
      "visible": _vm.showConfirm,
      "boxInfo": _vm.comfirmText
    },
    on: {
      "update:visible": function($event) {
        _vm.showConfirm = $event
      },
      "submitBox": _vm.comfirmInfo
    }
  })], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-c466952e", module.exports)
  }
}

/***/ }),

/***/ 2077:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1616);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("245443ca", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-c466952e!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./onlineCard.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-c466952e!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./onlineCard.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 584:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2077)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1388),
  /* template */
  __webpack_require__(1929),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\creditCard\\onlineCard.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] onlineCard.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c466952e", Component.options)
  } else {
    hotAPI.reload("data-v-c466952e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 959:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            userInfo: {}
        };
    },

    props: {
        visible: {
            type: Boolean,
            default: false
        },
        boxInfo: {
            type: Object,
            default: ""
        }
    },
    watch: {
        visible: function visible(value) {
            var _this = this;

            if (value) {
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "user/userIC"
                }, function (data) {
                    console.log(data);
                    _this.userInfo = data.data;
                });
            }
        }
    },
    mounted: function mounted() {},

    methods: {
        closeBox: function closeBox() {
            this.$emit("update:visible", false);
        },
        submitBox: function submitBox() {
            this.$emit("submitBox");
            this.$emit("update:visible", false);
        }
    }
};

/***/ }),

/***/ 963:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.confirmInfo_mask[data-v-5140d656] {\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 10;\n}\n.confirmInfo_box[data-v-5140d656] {\n  width: 6.3rem;\n  background: #fff;\n  padding: .62rem 0;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n      -ms-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  z-index: 10;\n  border-radius: .08rem;\n}\n.confirmInfo_box .confirmInfo_close[data-v-5140d656] {\n  width: .6rem;\n  height: .6rem;\n  background: url(" + __webpack_require__(1225) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  right: 0;\n  top: -0.92rem;\n}\n.confirmInfo_box .confirmInfo_text[data-v-5140d656] {\n  width: 4.72rem;\n  overflow: hidden;\n  margin: 0 auto;\n  color: #3D4A5B;\n}\n.confirmInfo_box .confirmInfo_text h1[data-v-5140d656] {\n  display: block;\n  font-size: .56rem;\n}\n.confirmInfo_box .confirmInfo_text .cell span[data-v-5140d656] {\n  font-size: .24rem;\n  opacity: 0.8;\n  display: block;\n  margin-top: .16rem;\n}\n.confirmInfo_box .confirmInfo_text .cell b[data-v-5140d656] {\n  font-size: .32rem;\n  display: block;\n}\n.confirmInfo_box .confirmInfo_text .confirmInfo_line[data-v-5140d656] {\n  width: 100%;\n  height: 1px;\n  background: #3F83FF;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  margin: .46rem 0 .3rem;\n}\n.confirmInfo_box .confirmInfo_text p[data-v-5140d656] {\n  font-size: .24rem;\n  text-align: justify;\n  opacity: 0.8;\n}\n.confirmInfo_box .confirmInfo_text a[data-v-5140d656] {\n  width: 100%;\n  height: .8rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: .32rem;\n  color: #fff;\n  border-radius: .08rem;\n  margin-top: .32rem;\n}\n", ""]);

// exports


/***/ })

});