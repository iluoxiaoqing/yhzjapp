webpackJsonp([57],{

/***/ 1340:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_reward.png?v=a7395104";

/***/ }),

/***/ 1341:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_search.png?v=3ed8dc18";

/***/ }),

/***/ 1427:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            placeholder: "",
            isFocus: false,
            inputValue: "",
            detailList: [],
            isShow: false,
            isShowNOsearchdate: false,
            nodataText: "",
            activityCode: this.$route.query.activityCode
        };
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        next(function (vm) {
            console.log("from===", vm);
            var title = vm.$route.query.title;
            document.title = title;
        });
    },

    props: {
        placeholderText: {
            type: String,
            default: "输入手机号搜索商户"
        },
        type: {
            type: Boolean,
            default: false
        },
        inputText: String
    },
    components: {},
    mounted: function mounted() {
        // this.$refs.inputValue="";
        this.commonAjax();
    },

    methods: {
        commonAjax: function commonAjax() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardDetails",
                "data": {
                    // phoneNo:null,
                    activityCode: this.$route.query.activityCode
                }
            }, function (data) {
                _this.detailList = data.data;
                if (_this.detailList == "") {
                    _this.isShow = true;
                }
            });
        },
        confirm: function confirm() {
            var _this2 = this;

            this.$emit("confirm", this.inputValue);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardDetails",
                "data": {
                    phoneNo: this.inputValue,
                    activityCode: this.$route.query.activityCode
                }
            }, function (data) {
                _this2.detailList = data.data;
                if (_this2.detailList == "") {
                    _this2.isShowNOsearchdate = true;
                    _this2.nodataText = _this2.inputValue;
                }
            });
        }
    }
};

/***/ }),

/***/ 1507:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.detailMain {\n  width: 100%;\n}\n.detailMain .noDetail img {\n  width: 38%;\n  height: auto;\n  margin: 1.8rem 31% 0.32rem;\n}\n.detailMain .noDetail p {\n  width: 100%;\n  text-align: center;\n  opacity: 0.5;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n}\n.detailMain .wrap {\n  height: 1rem;\n  overflow: hidden;\n  background: #fff;\n  position: relative;\n  z-index: 10;\n}\n.detailMain .searchBox {\n  width: 100%;\n  background: #fff;\n  padding-top: 0.18rem;\n  padding-bottom: 0.18rem;\n  font-family: 'PingFangSC-Regular';\n  left: 0;\n  z-index: 100;\n}\n.detailMain .searchBox.grey {\n  background: #f6f8f7;\n  top: 2.58rem !important;\n  position: fixed;\n}\n.detailMain .searchBox.grey .searchBox_content input {\n  background: url(" + __webpack_require__(1341) + ") #fff no-repeat left 0.25rem center;\n  background-size: 0.28rem 0.28rem;\n}\n.detailMain .searchBox .searchBox_content {\n  width: 6.86rem;\n  height: 0.7rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin: 0 auto;\n}\n.detailMain .searchBox .searchBox_content input {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: block;\n  font-size: 0.28rem;\n  border-radius: 0.35rem;\n  line-height: 0.6rem;\n  padding-left: 0.7rem;\n  background: url(" + __webpack_require__(1341) + ") #F4F5FB no-repeat left 0.25rem center;\n  background-size: 0.28rem 0.28rem;\n  -webkit-transition: 0.3s ease;\n  transition: 0.3s ease;\n}\n.detailMain .searchBox .searchBox_content input::-webkit-input-placeholder {\n  color: #3D4A5B;\n  opacity: 0.15;\n}\n.detailMain .searchBox .searchBox_content a {\n  width: 0.9rem;\n  height: 0.6rem;\n  line-height: 0.6rem;\n  text-align: center;\n  color: #E95647;\n  border-radius: 0.5rem;\n  display: block;\n  font-size: 0.28rem;\n  margin-left: 0.2rem;\n}\n.detailMain .detailList {\n  width: 100%;\n  background: #ffffff;\n}\n.detailMain .detailList .item {\n  width: 92%;\n  margin: 0 4%;\n}\n.detailMain .detailList .item .itemHead {\n  width: 100%;\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  font-weight: bold;\n  padding: 0.3rem 0 0.25rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n.detailMain .detailList .item .itemHead:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #EAEAEE ;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.detailMain .detailList .item .itemHead b {\n  margin-left: 0.16rem;\n}\n.detailMain .detailList .item .itemHead span {\n  float: right;\n  opacity: 0.4;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  right: 0;\n  position: absolute;\n}\n.detailMain .detailList .item .itemList {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 0.31rem 0 0.33rem;\n  position: relative;\n}\n.detailMain .detailList .item .itemList:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #EAEAEE ;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.detailMain .detailList .item .itemList .logo {\n  float: left;\n  width: 0.66rem;\n  height: 0.66rem;\n  margin-top: 0.12rem;\n}\n.detailMain .detailList .item .itemList .logo img {\n  width: 0.66rem;\n  height: 0.66rem;\n  display: inherit;\n}\n.detailMain .detailList .item .itemList .itemCenter {\n  width: 60%;\n  float: left;\n  height: 0.7rem;\n  margin-left: 0.16rem;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerText {\n  font-size: 0.3rem;\n  color: #3D4A5B;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerText span {\n  font-size: 0.3rem;\n  color: #ED9856;\n  text-align: left;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerText img {\n  width: 0.24rem;\n  height: 0.24rem;\n  margin-left: 0.34rem;\n  display: inline-block;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTime {\n  opacity: 0.4;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  float: left;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTextWait {\n  font-size: 0.3rem;\n  color: #3D4A5B;\n  opacity: 0.4;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTextWait span {\n  font-size: 0.3rem;\n  color: #ED9856;\n  text-align: left;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTextWait img {\n  width: 0.24rem;\n  height: 0.24rem;\n  margin-left: 0.34rem;\n  display: inline-block;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTimeWait {\n  opacity: 0.1;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  float: left;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTextEd {\n  font-size: 0.3rem;\n  color: #3D4A5B;\n}\n.detailMain .detailList .item .itemList .itemCenter .centerTimeEd {\n  opacity: 0.4;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  float: left;\n}\n.detailMain .detailList .item .itemList .itemRightDone {\n  float: right;\n  font-size: 0.26rem;\n  color: #3F83FF;\n  font-weight: bold;\n  margin-top: 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n  right: 0;\n}\n.detailMain .detailList .item .itemList .itemRightWait {\n  opacity: 0.4;\n  float: right;\n  font-size: 0.26rem;\n  color: #3F83FF;\n  font-weight: bold;\n  margin-top: 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n  right: 0;\n}\n.detailMain .detailList .item .itemList .itemRightEd {\n  float: right;\n  font-size: 0.26rem;\n  color: #FE5E7C;\n  font-weight: bold;\n  margin-top: 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n  right: 0;\n}\n.detailMain .detailList .emptyDiv {\n  width: 100%;\n  height: 0.16rem;\n  background: #F4F5FB;\n}\n.detailMain .noSearchData {\n  opacity: 0.8;\n  font-size: 0.28rem;\n  color: #313757;\n  margin-top: 0.42rem;\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1687:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_noreward.png?v=217dba0c";

/***/ }),

/***/ 1688:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_trading.png?v=37ee37f8";

/***/ }),

/***/ 1689:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_notattend.png?v=f81aedbe";

/***/ }),

/***/ 1821:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "detailMain"
  }, [(_vm.isShow == true) ? _c('div', {
    staticClass: "noDetail"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1689),
      "alt": ""
    }
  }), _c('br'), _vm._v(" "), _c('p', [_vm._v("您还没有参加活动哦")])]) : _vm._e(), _vm._v(" "), (_vm.isShow == false) ? _c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    ref: "searchBox",
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])]) : _vm._e(), _vm._v(" "), _vm._l((_vm.detailList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "detailList"
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "itemHead"
    }, [_vm._v("\n                    " + _vm._s(i.custName) + "\n                    "), _c('b', [_vm._v(_vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.registerDate) + "注册")])]), _vm._v(" "), _vm._l((i.stages), function(j, index) {
      return _c('div', {
        key: index,
        staticClass: "itemList"
      }, [
        [_c('div', {
          staticClass: "logo"
        }, [(j.status == 0) ? _c('img', {
          attrs: {
            "src": __webpack_require__(1687),
            "alt": ""
          }
        }) : _c('img', {
          attrs: {
            "src": __webpack_require__(1688),
            "alt": ""
          }
        })]), _vm._v(" "), (index == 0) ? _c('div', {
          staticClass: "itemCenter"
        }, [(j.status == 0) ? _c('div', {
          staticClass: "centerTextWait"
        }, [_vm._v("交易2000 可领10元")]) : (j.status == 1) ? _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易2000 "), _c('img', {
          attrs: {
            "src": __webpack_require__(1340),
            "alt": ""
          }
        }), _c('span', [_vm._v("+10元")])]) : _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易2000 可领10元")]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "centerTimeWait"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")]) : _c('div', {
          staticClass: "centerTime"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")])]) : _c('div', {
          staticClass: "itemCenter"
        }, [(j.status == 0) ? _c('div', {
          staticClass: "centerTextWait"
        }, [_vm._v("交易60000 可领10元")]) : (j.status == 1) ? _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易60000 "), _c('img', {
          attrs: {
            "src": __webpack_require__(1340),
            "alt": ""
          }
        }), _c('span', [_vm._v("+10元")])]) : _c('div', {
          staticClass: "centerText"
        }, [_vm._v("交易60000 可领10元")]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "centerTimeWait"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")]) : _c('div', {
          staticClass: "centerTime"
        }, [_vm._v("有效日期 " + _vm._s(j.expiredTime) + "前")])]), _vm._v(" "), (j.status == 0) ? _c('div', {
          staticClass: "itemRightWait"
        }, [_vm._v("\n                            待达标\n                        ")]) : (j.status == 1) ? _c('div', {
          staticClass: "itemRightDone"
        }, [_vm._v("\n                            已达标\n                        ")]) : _c('div', {
          staticClass: "itemRightEd"
        }, [_vm._v("\n                            已过期\n                        ")])]
      ], 2)
    })], 2), _vm._v(" "), _c('div', {
      staticClass: "emptyDiv"
    })])
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isShowNOsearchdate == true),
      expression: "isShowNOsearchdate==true"
    }],
    staticClass: "noSearchData"
  }, [_vm._v("\n            未搜索到“" + _vm._s(_vm.nodataText) + "”相关结果\n        ")])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1176a46b", module.exports)
  }
}

/***/ }),

/***/ 1968:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1507);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2758314c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1176a46b!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1176a46b!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 630:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1968)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1427),
  /* template */
  __webpack_require__(1821),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\rewardDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1176a46b", Component.options)
  } else {
    hotAPI.reload("data-v-1176a46b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});