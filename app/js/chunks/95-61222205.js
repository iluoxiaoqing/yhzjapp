webpackJsonp([95],{

/***/ 1339:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/fhList.png?v=afcf0927";

/***/ }),

/***/ 1397:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showLoading: false,
            ruleList: [],
            totalBonus: '',
            pushHandsHomeBonus: {},
            tremBonus: {}
        };
    },
    mounted: function mounted() {
        this.getList();
        this.getRule();
    },

    filters: {
        keepTwoNum: function keepTwoNum(value) {
            value = Number(value);
            return value.toFixed(2);
        }
    },
    methods: {
        getRule: function getRule() {
            var _this = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "bonus/rule",
                data: {
                    // product: this.$route.query.product
                },
                showLoading: true
            }, function (data) {
                console.log(data);
                _this.showLoading = false;
                _this.ruleList = data.data;
            });
        },
        getList: function getList() {
            var _this2 = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "bonus/bonus",
                data: {},
                showLoading: true
            }, function (data) {
                console.log(data);
                _this2.showLoading = false;
                _this2.totalBonus = data.data.totalBonus;
                _this2.pushHandsHomeBonus = data.data.pushHandsHomeBonus;
                _this2.tremBonus = data.data.tremBonus;
            });
        },
        goFhList: function goFhList(i) {
            this.$router.push({
                "path": "fhList",
                query: {
                    "type": i
                }
            });
        }
    }
};

/***/ }),

/***/ 1546:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\fh\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1860:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "fhIndex"
  }, [_c('div', {
    staticClass: "topImg"
  }, [_c('button', [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.totalBonus)) + "元")])]), _vm._v(" "), _c('div', {
    staticClass: "fhcenter"
  }, [_c('div', {
    staticClass: "centerTitle"
  }, [_vm._v("\n      " + _vm._s(_vm.pushHandsHomeBonus.rewardRuleName) + "\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "top"
  }, [_c('div', {
    staticClass: "topTitle"
  }, [_vm._v("本月将分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.willBonus)))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1339),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.goFhList(_vm.pushHandsHomeBonus.rewardType)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "topTitle"
  }, [_vm._v("上月分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.alreadyBonus)))]), _vm._v(" "), _c('ul', {
    staticClass: "income-con"
  }, [_c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("达标人数")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm.pushHandsHomeBonus.alreadyNum))])]), _vm._v(" "), _c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("人均分红")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.alreadyAvgBonus)))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "fhcenter"
  }, [_c('div', {
    staticClass: "centerTitle"
  }, [_vm._v("\n      " + _vm._s(_vm.tremBonus.rewardRuleName) + "\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "top"
  }, [_c('div', {
    staticClass: "topTitle"
  }, [_vm._v("本月将分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.willBonus)))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1339),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.goFhList(_vm.tremBonus.rewardType)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "topTitle"
  }, [_vm._v("上月分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.alreadyBonus)))]), _vm._v(" "), _c('ul', {
    staticClass: "income-con"
  }, [_c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("达标人数")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm.tremBonus.alreadyNum))])]), _vm._v(" "), _c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("人均分红")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.alreadyAvgBonus)))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "fhText"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n      分红规则\n    ")]), _vm._v(" "), _vm._l((_vm.ruleList), function(el, i) {
    return (i !== _vm.ruleList.length - 1) ? _c('p', {
      key: i,
      domProps: {
        "innerHTML": _vm._s(el)
      }
    }) : _c('div', {
      staticClass: "text"
    }, [_vm._v("\n     以上所有分红奖励可叠加参与，推手最高可参与平分当月交易总量万0.2的分红。分红政策自2021年7月1日开始实施，当月所有奖励将于次月15日进行发放。活动有效期为2021年7月1日至2022年3月31日，逍遥推手拥有本次活动的最终解释权。\n    ")])
  })], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-3ddec85a", module.exports)
  }
}

/***/ }),

/***/ 2007:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1546);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("234b6c16", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3ddec85a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3ddec85a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 593:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2007)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1397),
  /* template */
  __webpack_require__(1860),
  /* scopeId */
  "data-v-3ddec85a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\fh\\fhIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] fhIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ddec85a", Component.options)
  } else {
    hotAPI.reload("data-v-3ddec85a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});