webpackJsonp([85],{

/***/ 1333:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/xinghui_zuo.png?v=4e0bc867";

/***/ }),

/***/ 1380:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            price: "",
            goods: {},
            canClick: false,
            details: [{}],
            remark: {}
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseOne",
            "data": {
                "productCode": this.$route.query.productCode
            }
        }, function (data) {
            console.log(data);
            _this.canClick = true;
            _this.goods = data.data;
            _this.price = data.data.price;
            _this.details = data.data.details;
            _this.remark = JSON.parse(data.data.remark);
        });
    },

    methods: {
        gotoPay: function gotoPay() {

            if (!this.canClick) {
                return;
            }

            var carList = [{
                currentNum: 1,
                goodsName: this.goods.goodsName,
                goodsType: this.goods.goodsType,
                imgPath: this.goods.imgPath,
                price: this.goods.price,
                productCode: this.goods.productCode,
                remark: this.goods.remark
            }];
            var purchaseJson = {};

            purchaseJson.carList = carList;
            purchaseJson.totalAmount = this.price;

            console.log(purchaseJson);

            common.youmeng("广告-信汇易付", "点击按钮");

            this.$store.dispatch('saveCarList', purchaseJson);

            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1491:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1649:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/xinghui_banner.png?v=715b7892";

/***/ }),

/***/ 1805:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "xinghui"
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "xinghui_buy"
  }, [_c('div', {
    staticClass: "buy_content"
  }, [_c('div', {
    staticClass: "buy_title"
  }, [_vm._v("产品信息")]), _vm._v(" "), _c('div', {
    staticClass: "buy_main"
  }, [_c('div', {
    staticClass: "buy_desc"
  }, [_c('b', [_vm._v("【信汇易付】开店宝")]), _vm._v(" "), _c('i', [_c('em', [_vm._v(_vm._s(_vm.details[0].price) + "元")]), _vm._v("\n                          /台\n                      ")])]), _vm._v(" "), _c('p', [_vm._v("费率:" + _vm._s(_vm.remark.customerFeeDesc))]), _vm._v(" "), _c('p', [_vm._v("支付机构:" + _vm._s(_vm.remark.company))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.remark.commissionDesc))])])])]), _vm._v(" "), _c('div', {
    staticClass: "banner_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('p', [_c('b', [_vm._v("立即购买")]), _vm._v(" "), _c('i', [_vm._v("（" + _vm._s(_vm.details[0].count) + "台起购）")])]), _vm._v(" "), _c('span', [_vm._v("采购时间：" + _vm._s(_vm.remark.salesStartTime) + "-" + _vm._s(_vm.remark.salesEndTime))])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "xinghui_banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1649)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "xinghui_content"
  }, [_c('div', {
    staticClass: "xinghui_title"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1333),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("奖励规则")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1333),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "xinghui_item"
  }, [_c('div', {
    staticClass: "item_title"
  }, [_vm._v("\n                  激活返现"), _c('b', [_vm._v("289")]), _vm._v("元\n              ")]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('b', [_vm._v("成功收取商户199元服务费")]), _vm._v(" "), _c('p', [_vm._v("激活时间：2019.3.1-2019.12.31")])]), _vm._v(" "), _c('div', {
    staticClass: "item_footer"
  }, [_vm._v("\n                  *经理奖励289元   主管奖励274元   推手奖励249元\n              ")])]), _vm._v(" "), _c('div', {
    staticClass: "xinghui_item"
  }, [_c('div', {
    staticClass: "item_title"
  }, [_vm._v("\n                  费率降到"), _c('b', [_vm._v("0.50%+3")])]), _vm._v(" "), _c('div', {
    staticClass: "item_body"
  }, [_c('b', [_vm._v("自激活之日起180内")])]), _vm._v(" "), _c('div', {
    staticClass: "item_footer"
  }, [_vm._v("\n                  期间推手没有分润\n              ")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0599fd5f", module.exports)
  }
}

/***/ }),

/***/ 1952:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1491);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("68aad719", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0599fd5f&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xinghui.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0599fd5f&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xinghui.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 574:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1952)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1380),
  /* template */
  __webpack_require__(1805),
  /* scopeId */
  "data-v-0599fd5f",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\xinghui.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] xinghui.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0599fd5f", Component.options)
  } else {
    hotAPI.reload("data-v-0599fd5f", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});