webpackJsonp([65],{

/***/ 1312:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/showFail.png?v=ab955561";

/***/ }),

/***/ 1313:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/showSuccess.png?v=d20731c1";

/***/ }),

/***/ 1470:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var errorMsg = "";

var checkName = function checkName(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.name.empty;
        return false;
    } else {

        var reg = _base2.default.name.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.name.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};

//验证手机号
var checkMobile = function checkMobile(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.phoneNumber.empty;
        return false;
    } else {

        var reg = _base2.default.phoneNumber.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.phoneNumber.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};
exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            name: '',
            custPhone: '',
            posSn: "",
            idCard: "",
            bankCard: "",
            showDia: false,
            showSuccess: false,
            showFail: false,
            content: "",
            title: ""
        };
    },
    mounted: function mounted() {},

    methods: {
        closeShare: function closeShare() {
            this.showDia = false;
        },
        goLq: function goLq() {
            var _this = this;

            if (!this.posSn) {
                common.toast({
                    "content": "请输入您的机具号"
                });
                return;
            }
            if (!this.name) {
                common.toast({
                    "content": "请输入您的姓名"
                });
                return;
            }
            if (!this.idCard) {
                common.toast({
                    "content": "请输入您身份证号"
                });
                return;
            }
            var idReg = _base2.default.idCard.reg;
            if (idReg.test(this.idCard)) {} else {
                common.toast({
                    content: '请输入您正确的身份证号'
                });
                return;
            }
            if (!checkName(this.name)) {
                common.toast({
                    "content": "请正确输入您的姓名"
                });
                return;
            }
            if (!this.custPhone) {
                common.toast({
                    "content": "请输入您的银行预留手机号"
                });
                return;
            }
            if (!checkMobile(common.trim(this.custPhone))) {
                common.toast({
                    "content": "请正确输入友刷商户的手机号"
                });
                return;
            }
            if (!this.bankCard) {
                common.toast({
                    "content": "请输入您的银行卡号"
                });
                return;
            }
            // let bankReg = base.bankCard.reg;
            // if (bankReg.test(this.idCard)) {

            // } else {
            //     common.toast({
            //         content: '银行卡号格式不正确'
            //     });
            //     return;
            // }
            if (this.$route.query.userNo) {
                var params = {
                    userNo: this.$route.query.userNo, //从哪里来的
                    soure: 'SHARE',
                    posSn: this.posSn,
                    name: this.name,
                    idCard: this.idCard,
                    phone: this.custPhone,
                    bankCard: this.bankCard
                };
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "posCustomerReward/apply",

                    data: params,
                    showLoading: false
                }, function (data) {
                    if (data.code == '0000') {
                        // 成功
                        _this.showDia = true;
                        _this.showSuccess = true;
                        _this.showFail = false;
                        _this.title = '提交成功';
                        _this.content = data.msg;
                    } else {
                        _this.showDia = true;
                        _this.showFail = true;
                        _this.showSuccess = false;
                        _this.title = '提交失败';
                        _this.content = data.msg;
                    }
                });
            } else {
                common.AjaxMerchant({
                    url: _apiConfig2.default.KY_IP + "posCustomerReward/apply",

                    data: {
                        soure: 'APP',
                        posSn: this.posSn,
                        name: this.name,
                        idCard: this.idCard,
                        phone: this.custPhone,
                        bankCard: this.bankCard
                    },
                    showLoading: false
                }, function (data) {
                    console.log("返回值", data);
                    if (data.code == '0000') {
                        // 成功
                        _this.showDia = true;
                        _this.showSuccess = true;
                        _this.showFail = false;
                        _this.title = '提交成功';
                        _this.content = data.msg;
                    } else {
                        _this.showDia = true;
                        _this.showFail = true;
                        _this.showSuccess = false;
                        _this.title = '提交失败';
                        _this.content = data.msg;
                    }
                });
            }
        }
    }
};

/***/ }),

/***/ 1504:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1818:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "merchant_main",
    attrs: {
      "id": "free_main"
    }
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  }, [_c('div', {
    staticClass: "tips1"
  }, [(_vm.showSuccess == true) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1313),
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), (_vm.showFail == true) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1312),
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "a"
  }, [_vm._v(_vm._s(_vm.content))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "main_form"
  }, [_c('div', {
    staticClass: "form"
  }, [_vm._m(0), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.posSn),
      expression: "posSn"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入您的机具号"
    },
    domProps: {
      "value": (_vm.posSn)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.posSn = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(1), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入您的姓名"
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(2), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.idCard),
      expression: "idCard"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入您的身份证号"
    },
    domProps: {
      "value": (_vm.idCard)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.idCard = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.custPhone),
      expression: "custPhone"
    }],
    attrs: {
      "maxlength": "11",
      "type": "text",
      "placeholder": "请输入您的银行预留手机号"
    },
    domProps: {
      "value": (_vm.custPhone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.custPhone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(4), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.bankCard),
      expression: "bankCard"
    }],
    attrs: {
      "type": "number",
      "placeholder": "请输入您的银行卡号"
    },
    domProps: {
      "value": (_vm.bankCard)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.bankCard = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('i', [_vm._v("注：姓名，手机号请与商户注册信息保持一致")]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goLq()
      }
    }
  }, [_vm._v("立即领取")])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("机 具 号：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("姓     名：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("身份证号：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("手 机 号：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("银行卡号：")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0f581390", module.exports)
  }
}

/***/ }),

/***/ 1965:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1504);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("b61a0de6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0f581390&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./freeChange.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0f581390&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./freeChange.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 676:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1965)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1470),
  /* template */
  __webpack_require__(1818),
  /* scopeId */
  "data-v-0f581390",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\shjl.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] shjl.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f581390", Component.options)
  } else {
    hotAPI.reload("data-v-0f581390", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});