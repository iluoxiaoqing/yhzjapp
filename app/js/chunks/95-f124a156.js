webpackJsonp([95],{

/***/ 1339:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/fhList.png?v=afcf0927";

/***/ }),

/***/ 1397:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showLoading: false,
            ruleList: [],
            totalBonus: '',
            pushHandsHomeBonus: {},
            tremBonus: {}
        };
    },
    mounted: function mounted() {
        this.getList();
        this.getRule();
    },

    filters: {
        keepTwoNum: function keepTwoNum(value) {
            value = Number(value);
            return value.toFixed(2);
        }
    },
    methods: {
        getRule: function getRule() {
            var _this = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "bonus/rule",
                data: {
                    // product: this.$route.query.product
                },
                showLoading: true
            }, function (data) {
                console.log(data);
                _this.showLoading = false;
                _this.ruleList = data.data;
            });
        },
        getList: function getList() {
            var _this2 = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "bonus/bonus",
                data: {},
                showLoading: true
            }, function (data) {
                console.log(data);
                _this2.showLoading = false;
                _this2.totalBonus = data.data.totalBonus;
                _this2.pushHandsHomeBonus = data.data.pushHandsHomeBonus;
                _this2.tremBonus = data.data.tremBonus;
            });
        },
        goFhList: function goFhList(i) {
            this.$router.push({
                "path": "fhList",
                query: {
                    "type": i
                }
            });
        }
    }
};

/***/ }),

/***/ 1546:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.fhIndex[data-v-3ddec85a] {\n  width: 100%;\n  background: -webkit-linear-gradient(top, #34148A 0%, #6410AA 100%);\n  background: linear-gradient(180deg, #34148A 0%, #6410AA 100%);\n}\n.fhIndex .topImg[data-v-3ddec85a] {\n  background: url(" + __webpack_require__(1669) + ") no-repeat;\n  width: 100%;\n  height: 6.32rem;\n  background-size: 100%;\n}\n.fhIndex .topImg button[data-v-3ddec85a] {\n  width: 74%;\n  margin: 3.82rem 13% 0;\n  height: 0.94rem;\n  background: -webkit-linear-gradient(top, #10A1F7 0%, rgba(55, 92, 223, 0.66) 100%);\n  background: linear-gradient(180deg, #10A1F7 0%, rgba(55, 92, 223, 0.66) 100%);\n  border-radius: .48rem;\n  line-height: 0.94rem;\n  font-size: .42rem;\n  font-weight: 900;\n  color: #ffffff;\n}\n.fhIndex .fhcenter[data-v-3ddec85a] {\n  width: 92%;\n  margin: 0 4%;\n  height: 5.22rem;\n  background: #ffffff;\n  border-bottom-left-radius: .12rem;\n  border-bottom-right-radius: .12rem;\n  margin-bottom: 0.4rem;\n}\n.fhIndex .fhcenter .centerTitle[data-v-3ddec85a] {\n  width: 100%;\n  background: #E2EDFF;\n  height: 0.8rem;\n  text-align: center;\n  color: #3F83FF;\n  font-size: 0.32rem;\n  line-height: 0.8rem;\n  font-weight: 500;\n}\n.fhIndex .fhcenter .top[data-v-3ddec85a] {\n  width: 100%;\n  height: 1.72rem;\n  border-bottom: 1px solid #DCDCDC;\n  display: inline-block;\n}\n.fhIndex .fhcenter .top .topTitle[data-v-3ddec85a] {\n  text-align: center;\n  width: 100%;\n  color: #666666;\n  font-size: .24rem;\n  margin: 0.4rem auto 0rem;\n}\n.fhIndex .fhcenter .top .topText[data-v-3ddec85a] {\n  text-align: center;\n  width: 100%;\n  font-size: .48rem;\n  color: #3F83FF;\n  font-weight: bold;\n}\n.fhIndex .fhcenter .top .income-con[data-v-3ddec85a] {\n  margin-top: .08rem;\n  padding-left: .08rem;\n  padding-right: .06rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.fhIndex .fhcenter .top .income-con li[data-v-3ddec85a] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n  position: relative;\n}\n.fhIndex .fhcenter .top .income-con li .type[data-v-3ddec85a] {\n  font-family: PingFangSC-Regular;\n  font-size: .24rem;\n  color: #666666;\n}\n.fhIndex .fhcenter .top .income-con li .money[data-v-3ddec85a] {\n  margin-top: .04rem;\n  font-family: PingFangSC-Semibold;\n  font-size: .32rem;\n  color: #3F83FF;\n}\n.fhIndex .fhcenter .top .income-con li[data-v-3ddec85a]:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n  background: #DCDCDC;\n  width: 1px;\n  height: 100%;\n  border-right: 1px;\n  opacity: 0.3;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.fhIndex .fhcenter .top .income-con li[data-v-3ddec85a]:last-child {\n  text-align: center;\n}\n.fhIndex .fhcenter .top .income-con li[data-v-3ddec85a]:last-child:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #DCDCDC;\n  width: 0px !important;\n  height: 100%;\n  border-left: 1px;\n  opacity: 0.3;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.fhIndex .fhcenter .top .income-con li[data-v-3ddec85a]:first-child {\n  text-align: center;\n}\n.fhIndex .fhcenter .bottom[data-v-3ddec85a] {\n  width: 100%;\n  height: 2.68rem;\n  display: inline-block;\n}\n.fhIndex .fhcenter .bottom .income-con[data-v-3ddec85a] {\n  margin-top: .08rem;\n  padding-left: .08rem;\n  padding-right: .06rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.fhIndex .fhcenter .bottom .income-con li[data-v-3ddec85a] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n  position: relative;\n}\n.fhIndex .fhcenter .bottom .income-con li .type[data-v-3ddec85a] {\n  font-family: PingFangSC-Regular;\n  font-size: .24rem;\n  color: #666666;\n}\n.fhIndex .fhcenter .bottom .income-con li .money[data-v-3ddec85a] {\n  margin-top: .04rem;\n  font-family: PingFangSC-Semibold;\n  font-size: .32rem;\n  color: #3F83FF;\n}\n.fhIndex .fhcenter .bottom .income-con li[data-v-3ddec85a]:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n  background: #DCDCDC;\n  width: 1px;\n  height: 100%;\n  border-right: 1px;\n  opacity: 0.3;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.fhIndex .fhcenter .bottom .income-con li[data-v-3ddec85a]:last-child {\n  text-align: center;\n}\n.fhIndex .fhcenter .bottom .income-con li[data-v-3ddec85a]:last-child:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #DCDCDC;\n  width: 0px !important;\n  height: 100%;\n  border-left: 1px;\n  opacity: 0.3;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.fhIndex .fhcenter .bottom .income-con li[data-v-3ddec85a]:first-child {\n  text-align: center;\n}\n.fhIndex .fhcenter .bottom img[data-v-3ddec85a] {\n  width: 0.24rem;\n  height: 0.24rem;\n  position: absolute;\n  right: 0;\n  margin-right: 0.5rem;\n  margin-top: 0.2rem;\n}\n.fhIndex .fhcenter .bottom .topTitle[data-v-3ddec85a] {\n  text-align: center;\n  width: 100%;\n  color: #666666;\n  font-size: .24rem;\n  margin: 0.4rem auto 0rem;\n}\n.fhIndex .fhcenter .bottom .topText[data-v-3ddec85a] {\n  text-align: center;\n  width: 100%;\n  font-size: .48rem;\n  color: #3F83FF;\n  font-weight: bold;\n}\n.fhIndex .fhText[data-v-3ddec85a] {\n  width: 92%;\n  margin: 0 4%;\n  color: #FFFFFF;\n  font-size: 0.28rem;\n  text-align: left;\n  line-height: .56rem;\n}\n.fhIndex .fhText .title[data-v-3ddec85a] {\n  font-size: 0.32rem;\n  text-align: center !important;\n}\n.fhIndex .fhText p[data-v-3ddec85a] {\n  white-space: pre-wrap;\n}\n.fhIndex .fhText .text[data-v-3ddec85a] {\n  color: #F14466;\n  font-size: 0.24rem;\n  padding-bottom: .42rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1669:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/fhbg.png?v=41027a0f";

/***/ }),

/***/ 1860:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "fhIndex"
  }, [_c('div', {
    staticClass: "topImg"
  }, [_c('button', [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.totalBonus)) + "元")])]), _vm._v(" "), _c('div', {
    staticClass: "fhcenter"
  }, [_c('div', {
    staticClass: "centerTitle"
  }, [_vm._v("\n      " + _vm._s(_vm.pushHandsHomeBonus.rewardRuleName) + "\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "top"
  }, [_c('div', {
    staticClass: "topTitle"
  }, [_vm._v("本月将分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.willBonus)))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1339),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.goFhList(_vm.pushHandsHomeBonus.rewardType)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "topTitle"
  }, [_vm._v("上月分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.alreadyBonus)))]), _vm._v(" "), _c('ul', {
    staticClass: "income-con"
  }, [_c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("达标人数")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm.pushHandsHomeBonus.alreadyNum))])]), _vm._v(" "), _c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("人均分红")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.pushHandsHomeBonus.alreadyAvgBonus)))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "fhcenter"
  }, [_c('div', {
    staticClass: "centerTitle"
  }, [_vm._v("\n      " + _vm._s(_vm.tremBonus.rewardRuleName) + "\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "top"
  }, [_c('div', {
    staticClass: "topTitle"
  }, [_vm._v("本月将分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.willBonus)))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1339),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.goFhList(_vm.tremBonus.rewardType)
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "topTitle"
  }, [_vm._v("上月分红总金额")]), _vm._v(" "), _c('div', {
    staticClass: "topText"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.alreadyBonus)))]), _vm._v(" "), _c('ul', {
    staticClass: "income-con"
  }, [_c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("达标人数")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm.tremBonus.alreadyNum))])]), _vm._v(" "), _c('li', [_c('p', {
    staticClass: "type"
  }, [_vm._v("人均分红")]), _vm._v(" "), _c('p', {
    staticClass: "money"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.tremBonus.alreadyAvgBonus)))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "fhText"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n      分红规则\n    ")]), _vm._v(" "), _vm._l((_vm.ruleList), function(el, i) {
    return (i !== _vm.ruleList.length - 1) ? _c('p', {
      key: i,
      domProps: {
        "innerHTML": _vm._s(el)
      }
    }) : _c('div', {
      staticClass: "text"
    }, [_vm._v("\n     以上所有分红奖励可叠加参与，推手最高可参与平分当月交易总量万0.2的分红。分红政策自2021年7月1日开始实施，当月所有奖励将于次月15日进行发放。活动有效期为2021年7月1日至2022年3月31日，逍遥推手拥有本次活动的最终解释权。\n    ")])
  })], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-3ddec85a", module.exports)
  }
}

/***/ }),

/***/ 2007:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1546);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("234b6c16", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3ddec85a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3ddec85a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 593:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2007)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1397),
  /* template */
  __webpack_require__(1860),
  /* scopeId */
  "data-v-3ddec85a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\fh\\fhIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] fhIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ddec85a", Component.options)
  } else {
    hotAPI.reload("data-v-3ddec85a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});