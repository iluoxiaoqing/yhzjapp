webpackJsonp([67],{

/***/ 1466:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log("进入到页面中");
exports.default = {
    data: function data() {
        return {
            currentGroup: 1,
            current: 1,
            /** 1（今日）、2（昨日）、3（本月） */
            detailArr: [],
            showLoading: true,
            liName1: "今日",
            liName2: "昨日",
            rankType: "1",
            showDia: false,
            rankName: "今日"
        };
    },
    mounted: function mounted() {
        this.groupIncome();
    },

    methods: {
        groupIncome: function groupIncome() {
            var _this = this;

            /** 分组收益 */
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "v3/achievement/rankList",
                // "url": api.KY_IP + "income/groupIncome",
                "type": "post",
                "data": {
                    "rankType": this.rankType
                }
            }, function (data) {
                console.log(data);
                _this.detailArr = data.data;
            });
        },
        changeTab: function changeTab(idx) {
            if (idx == this.currentGroup) {
                return;
            }
            var _pageName = "我的收益";
            if (this.from == "today") {
                _pageName = "今日收益";
            } else {
                _pageName = "我的收益";
            }
            if (idx == 1) {
                this.liName1 = "今日";
                this.liName2 = "昨日";
                this.rankName = "今日";
                this.current = 1;
                this.rankType = 1;
                common.youmeng(_pageName, "点击今日明细");
            } else if (idx == 2) {
                this.liName1 = "本周";
                this.liName2 = "上周";
                this.rankName = "本周";
                this.current = 1;
                this.rankType = 3;
                common.youmeng(_pageName, "点击昨日明细");
            } else if (idx == 3) {
                this.liName1 = "本月";
                this.liName2 = "上月";
                this.rankName = "本月";
                this.current = 1;
                this.rankType = 5;
                common.youmeng(_pageName, "点击本月明细");
            }
            this.currentGroup = idx;
            this.groupIncome();
        },
        changeDate: function changeDate(idx) {
            if (idx == this.current) {
                return;
            }
            if (this.currentGroup == 1) {
                if (this.current == 1) {
                    this.rankType = 2;
                    this.rankName = "昨日";
                } else {
                    this.rankType = 1;
                    this.rankName = "今日";
                }
                console.log("rankType====", this.rankType);
            } else if (this.currentGroup == 2) {
                if (this.current == 1) {
                    this.rankType = 4;
                    this.rankName = "上周";
                } else {
                    this.rankType = 3;
                    this.rankName = "本周";
                }
                console.log("rankType1====", this.rankType);
            } else {
                if (this.current == 1) {
                    this.rankType = 6;
                    this.rankName = "上月";
                } else {
                    this.rankType = 5;
                    this.rankName = "本月";
                }
                console.log("rankType2====", this.rankType);
            }
            this.current = idx;
            this.groupIncome();
        },
        rule: function rule() {
            this.showDia = true;
        },
        cancel: function cancel() {
            this.showDia = false;
        }
    }
};

/***/ }),

/***/ 1625:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1705:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/empty.png?v=4a57060c";

/***/ }),

/***/ 1709:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_close.png?v=9484bf6f";

/***/ }),

/***/ 1735:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/per5.png?v=65ad9554";

/***/ }),

/***/ 1938:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "total"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    }
  }, [_c('div', {
    staticClass: "rule"
  }, [_c('h5', [_vm._v("激活排行榜规则说明")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1709),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.cancel()
      }
    }
  }), _vm._v(" "), _c('p', {
    staticStyle: {
      "font-weight": "bold"
    }
  }, [_vm._v("排行规则：")]), _vm._v(" "), _c('p', [_vm._v("日排行：统计当日新增商户当日累计交易量前三名")]), _vm._v(" "), _c('p', [_vm._v("周排行：统计当周新增商户当周累计交易量前三名")]), _vm._v(" "), _c('p', [_vm._v("月排行：统计当月新增商户当月累计交易量前三名")]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('p', {
    staticStyle: {
      "font-weight": "bold"
    }
  }, [_vm._v("奖励金额：")]), _vm._v(" "), _c('p', [_vm._v("日排行：前三名分别奖励150元、100元、50元")]), _vm._v(" "), _c('p', [_vm._v("周排行：前三名分别奖励500元、200元、100元")]), _vm._v(" "), _c('p', [_vm._v("月排行：前三名分别奖励1000元、500元、200元")]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('p', [_vm._v("                 概不计入排名，且平台有权停止一切奖")]), _vm._v(" "), _c('p', [_vm._v("                 励发放")])])]), _vm._v(" "), _c('div', {
    staticClass: "top-con"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n\t\t\t\t业绩排行榜\n\t\t\t")]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_vm._v("每天中午12点更新")]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.rule()
      }
    }
  }, [_vm._v("点击查看规则说明")])]), _vm._v(" "), _c('div', {
    staticClass: "detail-con"
  }, [_c('ul', {
    staticClass: "tab"
  }, [_c('li', {
    class: {
      'active': _vm.currentGroup == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(1)
      }
    }
  }, [_vm._v("日排行榜")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroup == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(2)
      }
    }
  }, [_vm._v("周排行榜")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroup == 3
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(3)
      }
    }
  }, [_vm._v("月排行榜")])]), _vm._v(" "), _c('ul', {
    staticClass: "datetab"
  }, [_c('li', {
    class: {
      'active': _vm.current == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeDate(1)
      }
    }
  }, [_vm._v(_vm._s(_vm.liName1))]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.current == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeDate(2)
      }
    }
  }, [_vm._v(_vm._s(_vm.liName2))])]), _vm._v(" "), ((_vm.detailArr.rankList).length > 0) ? _c('div', {
    staticClass: "ownRank"
  }, [_c('div', {
    staticClass: "ownMain"
  }, [_c('div', {
    staticClass: "ownleft"
  }, [_vm._v("\n                        " + _vm._s(_vm.detailArr.name) + "（" + _vm._s(_vm.detailArr.no) + "）"), _c('br'), _vm._v("\n                        交易金额：" + _vm._s(_vm.detailArr.amount) + "元\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "ownright"
  }, [_vm._v("\n                        " + _vm._s(_vm.rankName) + "排行：NO." + _vm._s(_vm.detailArr.score) + "\n                    ")])])]) : _vm._e(), _vm._v(" "), ((_vm.detailArr.rankList).length > 0) ? _c('div', {
    staticClass: "detail-list"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('ul', _vm._l((_vm.detailArr.rankList), function(el, i) {
    return _c('li', {
      key: i
    }, [(i == 0 || i == 1 || i == 2) ? _c('div', {
      staticClass: "logo"
    }, [_c('img', {
      attrs: {
        "src": './image/per' + (i + 2) + '.png',
        "alt": ""
      }
    })]) : _c('div', {
      staticClass: "logoNum"
    }, [_vm._v(_vm._s(el.score))]), _vm._v(" "), _c('div', {
      staticClass: "dis"
    }, [_c('p', [_vm._v(_vm._s(el.name) + "（" + _vm._s(el.no) + "）")]), _vm._v(" "), _c('p', [_vm._v("交易金额：" + _vm._s(el.amount) + "元")])]), _vm._v(" "), (el.status == 0) ? _c('div', {
      staticClass: "logo1"
    }, [_c('img', {
      attrs: {
        "src": __webpack_require__(1735),
        "alt": ""
      }
    })]) : (el.status == 1 && el.score >= 4) ? _c('div', {
      staticClass: "text1"
    }, [_vm._v("--")]) : (el.custCount) ? _c('div', {
      staticClass: "text"
    }, [_vm._v(_vm._s(el.custCount))]) : _c('div', {
      staticClass: "text1"
    }, [_vm._v("--")])])
  }), 0)])]) : _c('div', {
    staticClass: "empty"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1705),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("暂无数据")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('b', [_vm._v("发放时间：")]), _vm._v("考核结束次日中午12点发放")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('b', [_vm._v("平台声明：")]), _vm._v("活动期间若发现恶意套取奖励行为，一")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e04c3180", module.exports)
  }
}

/***/ }),

/***/ 2086:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1625);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("3cda457f", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e04c3180&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./performanceRank.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e04c3180&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./performanceRank.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 672:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2086)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1466),
  /* template */
  __webpack_require__(1938),
  /* scopeId */
  "data-v-e04c3180",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\perRank.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] perRank.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e04c3180", Component.options)
  } else {
    hotAPI.reload("data-v-e04c3180", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});