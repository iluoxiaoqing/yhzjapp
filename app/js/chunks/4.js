webpackJsonp([4],{

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(458)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(383),
  /* template */
  __webpack_require__(434),
  /* scopeId */
  "data-v-6f5670eb",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\reward\\rewardList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6f5670eb", Component.options)
  } else {
    hotAPI.reload("data-v-6f5670eb", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/bianji@2x.png?v=f369b782";

/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon-jiant@2x.png?v=29c35291";

/***/ }),

/***/ 361:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/iconjiant@2x.png?v=f6a92fc8";

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

var _common2 = __webpack_require__(15);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            emptyConDia: false,
            conDia: false,
            loginKey: common.getLoginKey(),
            rewardList: [],
            bagList: []
        };
    },

    props: {},
    components: {},
    mounted: function mounted() {
        var _this = this;
        _this.rewardArray();
    },

    methods: {
        rewardArray: function rewardArray() {
            var _this2 = this;

            var _this = this;
            _this.rewardList = [];
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "dawnPlan/taskList",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey()
                }
            }, function (data) {
                var bagList = data.data.taskList;
                for (var i in bagList) {
                    bagList[i].changeJian = false;
                    bagList[i].rewardShow = true;
                }
                if (data.data.dawnPlanStatus == 'LIMING_NOJOIN') {
                    _this.emptyConDia = true;
                }
                if (data.data.dawnPlanStatus == 'LIMING_JOIN_FINISH') {
                    _this.conDia = true;
                }
                _this2.rewardList = bagList;
            });
        },
        clickUnfold: function clickUnfold(item, index) {
            var _this = this;
            if (_this.rewardList[index].changeJian == true) {
                _this.rewardList[index].changeJian = false;
                _this.rewardList[index].rewardShow = true;
                return;
            }
            for (var _item in _this.rewardList) {
                _this.rewardList[_item].changeJian = false;
                _this.rewardList[_item].rewardShow = true;
            }
            _this.rewardList[index].changeJian = true;
            _this.rewardList[index].rewardShow = false;
        },
        clickGetReward: function clickGetReward(item, index, type) {
            var _this = this;
            var dawnPlan = _this.rewardList[index].dawnPlan;
            if (type == 'PEND') {
                common.Ajax({
                    "url": _apiConfig2.default.KY_IP + "dawnPlan/receive",
                    "type": "post",
                    "data": {
                        loginKey: common.getLoginKey(),
                        dawnPlan: dawnPlan
                    }
                }, function (data) {
                    _this.$nextTick(function () {
                        _this.rewardArray();
                    });
                });
            }
        },

        //返回首页
        goBackPage: function goBackPage() {
            window.history.back();
        },

        //关闭
        confirmDia: function confirmDia() {
            var _this = this;
            _this.emptyConDia = false;
            _this.conDia = false;
            // this.$router.go(-1)
            // window.history.go(-1);
        }
    }
};

/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-6f5670eb] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-6f5670eb] {\n  background: #fff;\n}\n.tips_success[data-v-6f5670eb] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-6f5670eb] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-6f5670eb] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-6f5670eb],\n.fade-leave-active[data-v-6f5670eb] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-6f5670eb],\n.fade-leave-to[data-v-6f5670eb] {\n  opacity: 0;\n}\n.default_button[data-v-6f5670eb] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-6f5670eb] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-6f5670eb] {\n  position: relative;\n}\n.loading-tips[data-v-6f5670eb] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.reward[data-v-6f5670eb] {\n  width: 100%;\n  height: auto;\n  overflow: hidden;\n}\n.reward .reward-bg[data-v-6f5670eb] {\n  width: 100%;\n  overflow: hidden;\n  background: #FD614C;\n  position: relative;\n}\n.reward .reward-bg img[data-v-6f5670eb] {\n  width: 100%;\n  display: block;\n}\n.reward .reward-bg .reward-bg-title[data-v-6f5670eb] {\n  position: absolute;\n  bottom: 0.39rem;\n  left: 2.299rem;\n  font-size: 0.257rem;\n  font-family: \"PingFang SC Regular\";\n  color: #FFDF10;\n}\n.reward .reward-bottom[data-v-6f5670eb] {\n  background: #FD614C;\n  width: 100%;\n  height: auto;\n  padding: 0 0.25rem 0.2rem;\n  margin-top: -1px;\n}\n.reward .reward-bottom .reward-bottom-list[data-v-6f5670eb] {\n  width: 7.023rem;\n  max-height: 3.96rem;\n  border-radius: 0.093rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #ffffff;\n  position: relative;\n}\n.reward .reward-bottom .reward-bottom-list .rewardLeft[data-v-6f5670eb] {\n  width: 2.233rem;\n  font-size: 0.5rem;\n  font-family: SourceHanSansCN-Bold;\n  font-weight: bold;\n  color: #FF391E;\n  line-height: 1.76rem;\n  text-align: center;\n  border-radius: 0.093rem;\n  background: #fff6bc;\n}\n.reward .reward-bottom .reward-bottom-list .rewardImg[data-v-6f5670eb] {\n  width: 1.107rem;\n  height: 1.107rem;\n  background: url(" + __webpack_require__(359) + ");\n  background-size: 1.107rem 1.107rem;\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight[data-v-6f5670eb] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-left: 0.28rem;\n  border-radius: 0.093rem;\n  position: relative;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight .rewardRight-title[data-v-6f5670eb] {\n  width: 2.567rem;\n  font-size: 0.237rem;\n  font-weight: bold;\n  font-family: \"PingFang SC Heavy\";\n  color: #333333;\n  margin-top: 0.3rem;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight .rewardRight-title .rewardRightRow[data-v-6f5670eb] {\n  width: 0.227rem;\n  height: 0.227rem;\n  background: url(" + __webpack_require__(361) + ") no-repeat;\n  background-size: 0.227rem 0.227rem;\n  display: inline-block;\n  margin-left: 0.2rem ;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight .rewardRight-title .rewardRightCow[data-v-6f5670eb] {\n  width: 0.227rem;\n  height: 0.227rem;\n  background: url(" + __webpack_require__(360) + ") no-repeat;\n  background-size: 0.227rem 0.227rem;\n  display: inline-block;\n  margin-left: 0.2rem ;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight .rewardRight-hide[data-v-6f5670eb] {\n  width: 1.3rem;\n  height: 0.507rem;\n  font-size: 0.267rem;\n  color: #FFFFFF;\n  background: #D4D4D4;\n  border-radius: 2rem;\n  line-height: 0.537rem;\n  text-align: center;\n  margin: -0.5rem auto 0rem;\n  position: absolute;\n  right: 0.2rem;\n  top: 0.8rem;\n}\n.reward .reward-bottom .reward-bottom-list .rewardRight .rewardRight-show[data-v-6f5670eb] {\n  width: 1.3rem;\n  height: 0.507rem;\n  font-size: 0.267rem;\n  color: #FFFFFF;\n  background: -webkit-linear-gradient(left, #FF7F15 0%, #FF5548 100%);\n  background: linear-gradient(90deg, #FF7F15 0%, #FF5548 100%);\n  border-radius: 2rem;\n  line-height: 0.537rem;\n  text-align: center;\n  margin: -0.5rem auto 0rem;\n  position: absolute;\n  right: 0.2rem;\n  top: 0.8rem;\n}\n.reward .reward-bottom .reward-bottom-listShow[data-v-6f5670eb] {\n  width: 7.023rem;\n  border-radius: 0.093rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #ffffff;\n  position: relative;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardLeft[data-v-6f5670eb] {\n  width: 2.233rem;\n  font-size: 0.5rem;\n  font-family: SourceHanSansCN-Bold;\n  font-weight: bold;\n  color: #FF391E;\n  line-height: 1.96rem;\n  text-align: center;\n  border-radius: 0.093rem;\n  background: #fff6bc;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardImg[data-v-6f5670eb] {\n  width: 1.107rem;\n  height: 1.107rem;\n  background: url(" + __webpack_require__(359) + ");\n  background-size: 1.107rem 1.107rem;\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight[data-v-6f5670eb] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-left: 0.28rem;\n  border-radius: 0.093rem;\n  position: relative;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-title[data-v-6f5670eb] {\n  width: 2.567rem;\n  font-size: 0.237rem;\n  font-weight: 600;\n  font-family: \"PingFang SC Heavy\";\n  margin-top: 0.3rem;\n  color: #333333;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-title .rewardRightRow[data-v-6f5670eb] {\n  width: 0.227rem;\n  height: 0.227rem;\n  background: url(" + __webpack_require__(361) + ") no-repeat;\n  background-size: 0.227rem 0.227rem;\n  display: inline-block;\n  margin-left: 0.2rem ;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-title .rewardRightCow[data-v-6f5670eb] {\n  width: 0.227rem;\n  height: 0.227rem;\n  background: url(" + __webpack_require__(360) + ") no-repeat;\n  background-size: 0.227rem 0.227rem;\n  display: inline-block;\n  margin-left: 0.2rem ;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-title .rewardRight-unFoldTitle[data-v-6f5670eb] {\n  width: 4.23rem;\n  font-size: 0.237rem;\n  font-weight: 600;\n  font-family: PingFang SC;\n  padding-top: 0.2rem;\n  margin-bottom: 0.2rem;\n  color: #999999;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-hide[data-v-6f5670eb] {\n  width: 1.3rem;\n  height: 0.507rem;\n  font-size: 0.267rem;\n  color: #FFFFFF;\n  background: #D4D4D4;\n  border-radius: 2rem;\n  line-height: 0.537rem;\n  text-align: center;\n  position: absolute;\n  right: 0.2rem;\n  top: 0.3rem;\n}\n.reward .reward-bottom .reward-bottom-listShow .rewardRight .rewardRight-show[data-v-6f5670eb] {\n  width: 1.3rem;\n  height: 0.507rem;\n  font-size: 0.267rem;\n  color: #FFFFFF;\n  background: -webkit-linear-gradient(left, #FF7F15 0%, #FF5548 100%);\n  background: linear-gradient(90deg, #FF7F15 0%, #FF5548 100%);\n  border-radius: 2rem;\n  line-height: 0.537rem;\n  text-align: center;\n  position: absolute;\n  right: 0.2rem;\n  top: 0.3rem;\n}\n.reward .reward-letter[data-v-6f5670eb] {\n  width: 100%;\n  background: #FD614C;\n  overflow: hidden;\n  margin-top: -1px;\n}\n.reward .reward-letter .reward-letter-text[data-v-6f5670eb] {\n  font-size: 0.353rem;\n  color: #FFFFFF;\n  text-align: center;\n}\n.reward .reward-letter .reward-letter-log[data-v-6f5670eb] {\n  text-indent: 2em;\n  margin-bottom: 0.6rem;\n}\n.reward .reward-letter .reward-letter-margin[data-v-6f5670eb] {\n  margin-bottom: 0.6rem;\n}\n.reward .reward-letter div[data-v-6f5670eb] {\n  color: #FFFFFF;\n  padding: 0rem 0.2rem;\n  font-size: 0.287rem;\n  font-family: PingFangSC-Regular;\n  font-weight: 400;\n}\n.reward .backImg[data-v-6f5670eb] {\n  width: 100vw;\n  height: 100vh;\n  background: rgba(0, 0, 0, 0.4);\n  position: fixed;\n  top: 0;\n  left: 0;\n}\n.reward .conDia[data-v-6f5670eb] {\n  width: 6.3rem;\n  height: 3.98rem;\n  background: #ffffff;\n  border-radius: 6px;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  margin-left: -3.15rem;\n  margin-top: -1.79rem;\n  text-align: center;\n  z-index: 25;\n}\n.reward .conDia .title[data-v-6f5670eb] {\n  width: 70%;\n  text-align: center;\n  font-weight: 500;\n  color: #3F3F50;\n  margin: 0.5rem 15% 0.52rem;\n  font-size: 0.36rem;\n  display: inline-block;\n}\n.reward .conDia .btn[data-v-6f5670eb] {\n  width: 92%;\n  margin: 0 4%;\n  display: inline-block;\n  float: left;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-grid-column-align: center;\n      justify-items: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.reward .conDia .btn .cancle[data-v-6f5670eb] {\n  width: 2.4rem;\n  height: 0.8rem;\n  border-radius: 4px;\n  border: 1px solid rgba(63, 131, 255, 0.6);\n  font-size: 0.32rem;\n  color: #3666FF;\n  line-height: 0.8rem;\n  display: inline-block;\n  float: left;\n}\n.reward .conDia .btn .confirm[data-v-6f5670eb] {\n  width: 2.4rem;\n  height: 0.8rem;\n  border-radius: 4px;\n  font-size: 0.32rem;\n  color: #ffffff;\n  background: #3f83ff;\n  line-height: 0.8rem;\n  display: inline-block;\n  margin: 0 auto;\n}\n", ""]);

// exports


/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/liming@2x.png?v=5cf6d374";

/***/ }),

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward"
  }, [_vm._m(0), _vm._v(" "), _vm._l((_vm.rewardList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "reward-bottom"
    }, [(item.rewardShow) ? _c('div', {
      staticClass: "reward-bottom-list"
    }, [_c('div', {
      staticClass: "rewardLeft"
    }, [_vm._v(_vm._s(item.selfRewardAmount) + "元")]), _vm._v(" "), _c('div', {
      class: item.standardStatus == 'SATISFY' ? 'rewardImg ' : ''
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight"
    }, [_c('div', {
      staticClass: "rewardRight-title"
    }, [_vm._v(_vm._s(item.taskTitle) + "\n                        "), _c('span', {
      class: item.changeJian ? 'rewardRightCow' : 'rewardRightRow',
      on: {
        "click": function($event) {
          return _vm.clickUnfold(item, index)
        }
      }
    })]), _vm._v(" "), _c('div', {
      class: item.sendStatus == 'NO_RECEIVE' ? 'rewardRight-hide' : item.sendStatus == 'PEND' ? 'rewardRight-show' : 'rewardRight-hide',
      on: {
        "click": function($event) {
          $event.stopPropagation();
          return _vm.clickGetReward(item, index, item.sendStatus)
        }
      }
    }, [_vm._v(_vm._s(item.sendStatus == 'NO_RECEIVE' ? '领取' : item.sendStatus == 'PEND' ? '领取' : '已领取'))])])]) : _vm._e(), _vm._v(" "), (!item.rewardShow) ? _c('div', {
      staticClass: "reward-bottom-listShow"
    }, [_c('div', {
      staticClass: "rewardLeft"
    }, [_vm._v(_vm._s(item.selfRewardAmount) + "元")]), _vm._v(" "), _c('div', {
      class: item.standardStatus == 'SATISFY' ? 'rewardImg ' : ''
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight"
    }, [_c('div', {
      staticClass: "rewardRight-title"
    }, [_vm._v(_vm._s(item.taskTitle) + "\n                        "), _c('span', {
      class: item.changeJian ? 'rewardRightCow' : 'rewardRightRow',
      on: {
        "click": function($event) {
          return _vm.clickUnfold(item, index)
        }
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight-unFoldTitle"
    }, [_vm._v(_vm._s(item.taskContent))])]), _vm._v(" "), _c('div', {
      class: item.sendStatus == 'NO_RECEIVE' ? 'rewardRight-hide' : item.sendStatus == 'PEND' ? 'rewardRight-show' : 'rewardRight-hide',
      on: {
        "click": function($event) {
          $event.stopPropagation();
          return _vm.clickGetReward(item, index, item.sendStatus)
        }
      }
    }, [_vm._v(_vm._s(item.sendStatus == 'NO_RECEIVE' ? '领取' : item.sendStatus == 'PEND' ? '领取' : '已领取'))])])]) : _vm._e()])
  }), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.emptyConDia) ? _c('div', {
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      },
      "mousewheel": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "backImg"
  }), _vm._v(" "), _c('div', {
    staticClass: "conDia"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n                        亲爱的推手，您不符合活动参与条件，请关注推手平台其他活动，谢谢。\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.confirmDia(1)
      }
    }
  }, [_vm._v("关闭")])])])]) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.conDia) ? _c('div', {
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      },
      "mousewheel": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "backImg"
  }), _vm._v(" "), _c('div', {
    staticClass: "conDia"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n                        亲爱的推手，恭喜您，您已完成全部任务，可继续参与推手平台其他任务，谢谢。\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.confirmDia(1)
      }
    }
  }, [_vm._v("关闭")])])])]) : _vm._e()])], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward-bg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(418)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "reward-bg-title"
  }, [_vm._v("奖励只可从上到下依次领取")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward-letter"
  }, [_c('div', {
    staticClass: "reward-letter-text"
  }, [_vm._v("规则说明")]), _vm._v(" "), _c('div', [_vm._v("活动面向人群：")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-log"
  }, [_vm._v("本活动仅对2021年9月1日后注册逍遥推手的会员以及2021年9月1日前注册但无机具采购、无上级划拨机具、无团队、无任何激活、无任何增值业务的会员有效。")]), _vm._v(" "), _c('div', [_vm._v("活动参与方式：")]), _vm._v(" "), _c('div', [_vm._v("1、参与本活动的会员需要根据逍遥推手APP内发布的任务1-任务6的任务指引按照顺序逐个完成每项任务，并领取该项任务的奖励，")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-margin"
  }, [_vm._v(" 2、任务7、任务8、任务9三项若在一个月内完成，每个月只能按顺序领取一项任务奖励；")]), _vm._v(" "), _c('div', [_vm._v("活动时间：")]), _vm._v(" "), _c('div', [_vm._v("1、本活动任务截止日期为2022年8月31日，结束后将发布新的活动任务;")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-margin"
  }, [_vm._v("2、本活动最终解释权归逍遥推手所有。")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6f5670eb", module.exports)
  }
}

/***/ }),

/***/ 458:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(396);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("468fe7ad", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6f5670eb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewarList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6f5670eb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewarList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});