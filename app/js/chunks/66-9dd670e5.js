webpackJsonp([66],{

/***/ 1468:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0,
            listData: ""
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '盛付通MPOS');
        this.getList();
    },

    methods: {
        go: function go() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "sft/showPopMessage",
                "data": {}
            }, function (data) {
                _this.listData = data.data;
            });
        }
    }
};

/***/ }),

/***/ 1501:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1745:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/top.png?v=8dba4f03";

/***/ }),

/***/ 1815:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "sft"
  }, [_c('div', {
    staticClass: "sftTop"
  }, [_c('div', {
    staticClass: "mainBg"
  }, [_c('div', {
    staticClass: "time_main"
  }, [_vm._v("\r\n                                活动时间：" + _vm._s(_vm.listData.activityTime) + "\r\n                        ")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1745),
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "sftBottom"
  }, [_c('div', {
    staticClass: "contantMain"
  }, [_c('div', {
    staticClass: "contant_title"
  }), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.paymentCompany) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.paymentCompany) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.cardRate) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.cardRate), function(item) {
    return _c('p', [_vm._v(_vm._s(item))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.commRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.commRule) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.productFeatures) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.productFeatures) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.settleRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.settleRule) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "contantMain contantMain2"
  }, [_c('div', {
    staticClass: "contant_title1"
  }), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.posPrice) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "contact_right1"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.posPrice) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.actRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.actRecurrence), function(i) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "remark"
  }, [_vm._v("\r\n                                                        备注：首刷扣除手续费及39元服务费后，不足99的，D+1到账\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.platformStatement) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.platformStatement) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "button",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }, [_vm._v("\r\n                立即邀请\r\n                ")])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                支付公司:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                刷卡费率:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                分润规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                产品特点:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                结算规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left1"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                机具采购价格:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                激活返现:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                平台申明:\r\n                                        ")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0ea4ceec", module.exports)
  }
}

/***/ }),

/***/ 1962:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1501);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("24b27b68", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ea4ceec&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./sftIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ea4ceec&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./sftIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 674:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1962)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1468),
  /* template */
  __webpack_require__(1815),
  /* scopeId */
  "data-v-0ea4ceec",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\sftIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] sftIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0ea4ceec", Component.options)
  } else {
    hotAPI.reload("data-v-0ea4ceec", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});