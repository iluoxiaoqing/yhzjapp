webpackJsonp([128],{

/***/ 1396:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            active: 0,
            toptext: [],
            title: "",
            text: ""
        };
    },
    mounted: function mounted() {
        this.getQuestion();
    },

    methods: {
        getQuestion: function getQuestion() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getQuestionDetail",
                data: {
                    questionNo: this.$route.query.id
                },
                showLoading: false
            }, function (data) {
                _this.toptext = data.data;
                _this.title = data.data.title;
                _this.text = data.data.questionDesc;
            });
        }
    }
};

/***/ }),

/***/ 1496:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-08db3ad0] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-08db3ad0] {\n  background: #fff;\n}\n.tips_success[data-v-08db3ad0] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-08db3ad0] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-08db3ad0] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-08db3ad0],\n.fade-leave-active[data-v-08db3ad0] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-08db3ad0],\n.fade-leave-to[data-v-08db3ad0] {\n  opacity: 0;\n}\n.default_button[data-v-08db3ad0] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-08db3ad0] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-08db3ad0] {\n  position: relative;\n}\n.loading-tips[data-v-08db3ad0] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.container[data-v-08db3ad0] {\n  width: 100%;\n  height: 100%;\n}\n.container .question_main[data-v-08db3ad0] {\n  width: 92%;\n  margin: 0 4%;\n}\n.container .question_main .title[data-v-08db3ad0] {\n  font-family: PingFangSC-Semibold;\n  font-size: 0.32rem;\n  color: #333333;\n  letter-spacing: 0.19px;\n  display: inline-block;\n  margin: 0.16rem 0 0.16rem;\n  font-weight: bold;\n}\n.container .question_main .text[data-v-08db3ad0] {\n  opacity: 0.8;\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #333333;\n  letter-spacing: 0.17px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1810:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "question_main"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n            " + _vm._s(_vm.title) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "text",
    domProps: {
      "innerHTML": _vm._s(_vm.text)
    }
  })])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-08db3ad0", module.exports)
  }
}

/***/ }),

/***/ 1957:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1496);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("645d2606", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-08db3ad0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./question_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-08db3ad0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./question_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 592:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1957)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1396),
  /* template */
  __webpack_require__(1810),
  /* scopeId */
  "data-v-08db3ad0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\feedBack\\question_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] question_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-08db3ad0", Component.options)
  } else {
    hotAPI.reload("data-v-08db3ad0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});