webpackJsonp([39],{

/***/ 1464:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

var _searchBox = __webpack_require__(755);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            menuArray: [{
                title: "推荐",
                active: ""
            }, {
                title: "备货",
                active: ""
            }],
            recommendShop: [],
            orderList: [],
            expressFee: "",
            deleteShow: false,
            deleteInfo: {
                "title": "温馨提示",
                "content": "确定要删除此订单吗？",
                "confirmText": "确定删除"
            },
            bussFlowNo: "",
            orderIndex: 0,
            payShow: false,
            isLoading: true,
            currentPage: 1,
            mySwiper: null,
            index: this.$route.query.type || 0,
            defaultText: "",
            search: 0,
            userNo: common.getUserNo(),
            isNoData: false
        };
    },

    components: {
        alertBox: _alertBox2.default,
        searchBox: _searchBox2.default,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        if (!sessionStorage.askPositon || from.path == '/') {
            sessionStorage.askPositon = '';
            next();
        } else {
            next(function (vm) {
                if (vm && vm.$refs.my_scroller) {
                    //通过vm实例访问this
                    setTimeout(function () {
                        vm.$refs.my_scroller.scrollTo(0, sessionStorage.askPositon, false);
                    }, 20); //同步转异步操作
                }
            });
        }
    },
    beforeRouteLeave: function beforeRouteLeave(to, from, next) {
        //记录离开时的位置
        sessionStorage.askPositon = this.$refs.my_scroller && this.$refs.my_scroller.getPosition() && this.$refs.my_scroller.getPosition().top;
        next();
    },
    mounted: function mounted() {
        var _this = this;

        this.$refs.searchBox.inputValue = "";
        window.goBack = this.goBack;

        if (this.index == 0) {
            this.search = 1;
            this.getShopList();
        } else {
            this.search = 0;
            this.getOrderList();
        }

        this.getHeight();

        this.mySwiper = new Swiper('.swiper-container', {
            autoplay: false, //可选选项，自动滑动
            loop: false,
            freeMode: false,
            resistanceRatio: 0,
            observer: true, //修改swiper自己或子元素时，自动初始化swiper 
            observeParents: true,
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {

                _this.mySwiper.update();
                var index = swiper.activeIndex;
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.menuArray[index].active = true;
                _this.$refs.searchBox.inputValue = "";

                _this.index = index;
                _this.currentPage = 1;

                document.getElementById("scrollContainer").scrollTop = 0;

                console.log("index:" + _this.index);

                setTimeout(function () {

                    _this.getHeight();

                    // let scrollContainer = document.getElementById("scrollContainer");
                    // let scrollTop = scrollContainer.scrollTop;

                    // if(scrollTop>0){
                    //     scrollContainer.scrollTop = 0;
                    // }

                    if (_this.index == 0) {
                        _this.search = 1;
                        _this.getShopList();
                    } else {
                        _this.search = 0;
                        _this.getOrderList();
                    }
                }, 300);
            }
        });

        var type = this.$route.query.type || 0;
        this.changeList(this.menuArray[type], type);
    },

    methods: {
        alipayResult: function alipayResult(state) {
            var _this2 = this;

            if (state == "true" || state == 1 || state == true || state == '1') {
                common.toast({
                    "content": "支付成功"
                });
            } else {
                common.toast({
                    "content": "支付失败"
                });
            }

            setTimeout(function () {
                _this2.$router.push({
                    "path": "myOrder",
                    "query": {
                        "type": 1
                    }
                });
            }, 1000);
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        searchBtn: function searchBtn(search) {
            //alert(search)
            this.currentPage = 1;
            this.recommendShop = [];
            this.getShopList();
        },
        changeList: function changeList(i, index) {

            this.index = index;
            // this.$refs.searchBox.inputValue="";
            this.currentPage = 1;
            this.defaultText = "";
            this.menuArray.map(function (el) {
                el.active = false;
            });
            i.active = true;
            // this.refresh();
            this.mySwiper.slideTo(index, 300, true);
        },
        refresh: function refresh() {
            this.currentPage = 1;
            if (this.index == 0) {
                this.search = 1;
                this.getShopList();
            } else {
                this.search = 0;
                this.getOrderList();
            }
        },
        loadmore: function loadmore() {
            if (this.index == 0) {
                this.getShopList();
            } else {
                this.getOrderList();
            }
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        deleteOrder: function deleteOrder(i, index) {
            this.deleteShow = true;
            this.bussFlowNo = i.bussFlowNo;
            this.orderIndex = index;
        },
        gotoPay: function gotoPay(i) {
            console.log(i);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "alipay/evokeAliPay",
                "data": {
                    "outTradeNo": i.orderNo,
                    "subject": "",
                    "body": "",
                    "businessCode": "APP_SHOP_CODE"
                }
            }, function (data) {

                var aliPay = {
                    "alipay": data.data,
                    "redirectFunc": "alipayResult"
                };

                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                } else {
                    window.android.nativeAlipay(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                }
            });
        },
        gotoDetail: function gotoDetail(i) {

            common.setCookie("goodsDetail", JSON.stringify(i));
            common.setCookie("expressFee", this.expressFee);

            this.$router.push({
                "path": "orderDetail",
                "query": {
                    orderNo: i.orderNo
                }
            });
        },
        getShopList: function getShopList() {
            var _this3 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "salesBill/querySalesBill",
                data: {
                    "page": this.currentPage,
                    "userName": this.$refs.searchBox.inputValue,
                    "pageSource": "bill"
                },
                showLoading: false
            }, function (data) {

                if (data.data.object.length > 0) {
                    if (_this3.currentPage == 1) {
                        _this3.recommendShop = [];
                    }
                    data.data.object.map(function (el) {
                        _this3.$set(el, "show", true);
                        _this3.recommendShop.push(el);
                    });

                    console.log(_this3.recommendShop);
                    _this3.isNoData = false;
                    _this3.currentPage++;
                } else {

                    if (_this3.currentPage == 1) {
                        _this3.isNoData = true;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        },
        getOrderList: function getOrderList() {
            var _this4 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "salesBill/querySalesBillBySelf",
                data: {
                    "page": this.currentPage,
                    "billStatus": "SUCCESS",
                    "userName": this.$refs.searchBox.inputValue
                },
                showLoading: false
            }, function (data) {

                if (data.data.object.length > 0) {
                    if (_this4.currentPage == 1) {
                        _this4.orderList = [];
                    }
                    data.data.object.map(function (el) {
                        _this4.$set(el, "show", true);
                        _this4.orderList.push(el);
                    });

                    _this4.isNoData = false;

                    _this4.currentPage++;
                } else {

                    if (_this4.currentPage == 1) {
                        _this4.isNoData = true;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1533:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1847:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('div', {
    staticClass: "queryHead"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.menuArray), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.title))])
  }), 0), _vm._v(" "), _c('search-box', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.index == 0),
      expression: "index==0"
    }],
    ref: "searchBox",
    attrs: {
      "placeholderText": '请输入姓名搜索',
      "type": false
    },
    on: {
      "confirm": _vm.searchBtn
    }
  })], 1), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    ref: "swiper",
    staticClass: "swiper-outer"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      height: _vm.index == 0 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "recommendList",
    attrs: {
      "id": "recommendList"
    }
  }, _vm._l((_vm.recommendShop), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.buyUserName) + "购买" + _vm._s(i.goodsCount) + "台设备")])]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.createTime))])])
  }), 0)]), _vm._v(" "), _c('div', {
    ref: "choiceList",
    staticClass: "swiper-slide",
    style: ({
      height: _vm.index == 1 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "choiceList",
    attrs: {
      "id": "choiceList"
    }
  }, _vm._l((_vm.orderList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('div', {
      staticClass: "item_head"
    }, [_c('p', [_vm._v("订单编号：" + _vm._s(i.orderNo))]), _vm._v(" "), (i.status == 'REPAY') ? _c('span', {
      staticStyle: {
        "color": "#E95647"
      }
    }, [_vm._v("待付款")]) : _vm._e(), _vm._v(" "), (i.status == 'INIT') ? _c('span', [_vm._v("待发货")]) : _vm._e(), _vm._v(" "), (i.status == 'SUCCESS') ? _c('span', [_vm._v("已发货")]) : _vm._e(), _vm._v(" "), (i.status == 'DELIVERY') ? _c('span', [_vm._v("发货中")]) : _vm._e(), _vm._v(" "), (i.status == 'DISABLE') ? _c('span', [_vm._v("已失效")]) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "item_body"
    }, [(i.status == 'DISABLE') ? _c('b', {
      staticStyle: {
        "color": "#979ea7"
      }
    }, [_vm._v("开店礼包" + _vm._s(i.count) + "件商品")]) : _vm._e(), _vm._v(" "), (i.status == 'DISABLE') ? _c('strong', {
      staticStyle: {
        "color": "#979ea7"
      }
    }, [_vm._v("¥" + _vm._s(i.amount))]) : _vm._e(), _vm._v(" "), (i.status == 'SUCCESS' || i.status == 'INIT' || i.status == 'REPAY' || i.status == 'DELIVERY') ? _c('b', [_vm._v("开店礼包" + _vm._s(i.count) + "件商品")]) : _vm._e(), _vm._v(" "), (i.status == 'SUCCESS' || i.status == 'INIT' || i.status == 'REPAY' || i.status == 'DELIVERY') ? _c('strong', [_vm._v("¥" + _vm._s(i.amount))]) : _vm._e(), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.createTime))])]), _vm._v(" "), _c('div', {
      staticClass: "item_footer"
    }, [_c('button', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (i.status == 'INIT' || i.status == 'REPAY' || i.status == 'SUCCESS' || i.status == 'DELIVERY'),
        expression: "i.status=='INIT' || i.status=='REPAY' || i.status=='SUCCESS'||i.status=='DELIVERY'"
      }],
      on: {
        "click": function($event) {
          return _vm.gotoDetail(i)
        }
      }
    }, [_vm._v("查看详情")]), _vm._v(" "), _c('button', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (i.status == 'REPAY'),
        expression: "i.status=='REPAY'"
      }],
      staticClass: "active",
      on: {
        "click": function($event) {
          return _vm.gotoPay(i)
        }
      }
    }, [_vm._v("去付款")])])])
  }), 0)])])])])])], 1), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.deleteShow,
      "boxInfo": _vm.deleteInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.deleteShow = $event
      },
      "confirm": _vm.deleteConfirm
    }
  }), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.isNoData
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2d6cccd9", module.exports)
  }
}

/***/ }),

/***/ 1994:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1533);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("1e270869", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2d6cccd9&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./myOrder.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2d6cccd9&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./myOrder.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 669:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1994)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1464),
  /* template */
  __webpack_require__(1847),
  /* scopeId */
  "data-v-2d6cccd9",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\myOrder.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] myOrder.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2d6cccd9", Component.options)
  } else {
    hotAPI.reload("data-v-2d6cccd9", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 751:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(757)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(750),
  /* template */
  __webpack_require__(756),
  /* scopeId */
  "data-v-7cf3f18a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7cf3f18a", Component.options)
  } else {
    hotAPI.reload("data-v-7cf3f18a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7cf3f18a", module.exports)
  }
}

/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(751);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05a769e3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});