webpackJsonp([83],{

/***/ 1412:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _loading = __webpack_require__(166);

var _loading2 = _interopRequireDefault(_loading);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            routerIndex: 1 //1. 我的业绩 2.我的交易 3.我的团队
        };
    },

    components: {
        Loading: _loading2.default, alertBox: _alertBox2.default
    },
    computed: {},
    watch: {
        $route: function $route(to, from) {}
    },
    created: function created() {
        var _url = window.location.href;
        var _index = _url.lastIndexOf("/");
        var _path = _url.substring(_index + 1, _url.length);
        this.currentPath = _path;
        if (this.currentPath === "") {
            this.routerIndex = 1;
            this.$router.push(this.currentPath + '/' + this.routerIndex);
        }
    },
    mounted: function mounted() {
        window.backTypeFn = this.goBack;
        this.getContentHeight();
        this.routerIndex = this.getRouterIndex();
        if (this.routerIndex == 1) {
            common.youmeng("我的业绩", "进入我的业绩页面");
        } else if (this.routerIndex == 2) {
            common.youmeng("我的交易", "进入我的交易页面");
        } else {
            common.youmeng("我的团队", "进入我的团队页面");
        }
    },

    methods: {
        //获取路由索引
        getRouterIndex: function getRouterIndex() {
            var name = this.$route.name;

            if (name === "source") {
                return 3;
            } else if (name === "transaction") {
                return 2;
            } else {
                return 1;
            }
        },

        //计算显示区域高度
        getContentHeight: function getContentHeight() {
            // let header = document.getElementsByClassName('top-nav-con')[0];
            // let pageHeader = document.getElementsByClassName('page_header')[0];
            // let headerHeight = parseFloat(getComputedStyle(header).height);
            // let pageHeaderHeight = parseFloat(getComputedStyle(pageHeader).height);
            // let content = document.querySelector(".page_content");

            // let winHeight = (document.documentElement || document.body).clientHeight;
            // let diff = winHeight - headerHeight - pageHeaderHeight - 18;
            // content.style.height = diff + 'px';
        },

        //跳转路由
        go: function go(path, index) {
            this.routerIndex = index;
            if (this.routerIndex == 1) {
                common.youmeng("我的业绩", "进入我的业绩页面");
            } else if (this.routerIndex == 2) {
                common.youmeng("我的交易", "进入我的交易页面");
            } else {
                common.youmeng("我的团队", "进入我的团队页面");
            }
            this.$router.replace(path);
        },
        goBack: function goBack() {
            var _close = this.$route.query.closePage || common.getUrlParam("closePage");
            console.log(_close);
            if (_close == "true") {
                var _os = common.isClient();
                if (_os == "ios") {
                    window.webkit.messageHandlers.nativeCloseWindow.postMessage("nativeCloseWindow");
                } else {
                    window.Android.finshActivity();
                }
            } else {
                window.history.back();
            }
        }
    }
};

/***/ }),

/***/ 1551:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\nbody{\n    overflow: hidden;\n}\n", ""]);

// exports


/***/ }),

/***/ 1552:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-4247dcb3] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-4247dcb3] {\n  background: #fff;\n}\n.tips_success[data-v-4247dcb3] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-4247dcb3] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-4247dcb3] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-4247dcb3],\n.fade-leave-active[data-v-4247dcb3] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-4247dcb3],\n.fade-leave-to[data-v-4247dcb3] {\n  opacity: 0;\n}\n.default_button[data-v-4247dcb3] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-4247dcb3] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-4247dcb3] {\n  position: relative;\n}\n.loading-tips[data-v-4247dcb3] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n#page[data-v-4247dcb3] {\n  overflow: hidden !important;\n}\n.top-nav-con[data-v-4247dcb3] {\n  top: 0;\n  left: 0;\n  padding: 0 .32rem;\n  width: 100%;\n  height: 1rem;\n  position: absolute;\n  background: #fff;\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 99;\n}\n.top-nav-con span[data-v-4247dcb3] {\n  width: .56rem;\n  height: .34rem;\n  display: block;\n  background: url(" + __webpack_require__(1635) + ") no-repeat;\n  background-size: contain;\n  background-position: left;\n}\n.top-nav-con p[data-v-4247dcb3] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  font-family: PingFangSC-Medium;\n  font-size: .36rem;\n  color: #3D4A5B;\n  text-align: center;\n  position: relative;\n  left: -0.36rem;\n}\n.page_header[data-v-4247dcb3] {\n  padding-top: 0.266rem;\n  z-index: 10;\n  width: 100%;\n  background: #FFF;\n  overflow: hidden;\n}\n.page_header ul[data-v-4247dcb3] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: .8rem;\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-align-content: center;\n      -ms-flex-line-pack: center;\n          align-content: center;\n  background: #FFF;\n}\n.page_header ul li[data-v-4247dcb3] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: .28rem;\n  text-align: center;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n.page_header ul li.active > a[data-v-4247dcb3] {\n  font-family: \"PingFangSC-Medium\";\n  color: #3D4A5B;\n}\n.page_header ul li.active > a[data-v-4247dcb3]::after {\n  content: '';\n  width: .32rem;\n  height: .06rem;\n  position: absolute;\n  bottom: .1rem;\n  background: #3D4A5B;\n  border-radius: 2px;\n  left: 50%;\n  margin-left: -0.16rem;\n}\n.page_header ul li a[data-v-4247dcb3] {\n  color: #89929c;\n  display: inline-block;\n  padding: .24rem 0 ;\n  position: relative;\n}\n.page_content[data-v-4247dcb3] {\n  width: 100%;\n  overflow: auto;\n  margin-top: .16rem;\n  -webkit-overflow-scrolling: touch;\n  background: #FFF;\n}\n@media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n.page_content[data-v-4247dcb3]:after {\n    content: \"\";\n    height: constant(safe-area-inset-bottom);\n    height: env(safe-area-inset-bottom);\n}\n}\n@media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) {\n.page_content[data-v-4247dcb3]:after {\n    content: \"\";\n    height: constant(safe-area-inset-bottom);\n    height: env(safe-area-inset-bottom);\n}\n}\n@media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) {\n.page_content[data-v-4247dcb3]:after {\n    content: \"\";\n    height: constant(safe-area-inset-bottom);\n    height: env(safe-area-inset-bottom);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1635:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_return.png?v=d62b1689";

/***/ }),

/***/ 1865:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "statistics-wrap"
    }
  }, [_c('div', {
    staticClass: "page_header"
  }, [_c('ul', [_c('li', {
    class: [_vm.routerIndex === 1 ? 'active' : '']
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        return _vm.go('/perAnalysis/statistics', 1)
      }
    }
  }, [_vm._v("我的业绩")])]), _vm._v(" "), _c('li', {
    class: [_vm.routerIndex === 2 ? 'active' : '']
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        return _vm.go('/perAnalysis/transaction', 2)
      }
    }
  }, [_vm._v("我的交易")])]), _vm._v(" "), _c('li', {
    class: [_vm.routerIndex === 3 ? 'active' : '']
  }, [_c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        return _vm.go('/perAnalysis/source', 3)
      }
    }
  }, [_vm._v("我的团队")])])])]), _vm._v(" "), _c('div', {
    ref: "page_content",
    staticClass: "page_content",
    attrs: {
      "id": "page_content"
    }
  }, [_c('keep-alive', [(_vm.$route.meta.keepAlive) ? _c('router-view') : _vm._e()], 1), _vm._v(" "), (!_vm.$route.meta.keepAlive) ? _c('router-view') : _vm._e()], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-4247dcb3", module.exports)
  }
}

/***/ }),

/***/ 2012:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1551);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("993e9caa", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4247dcb3!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./statisticsWrap.vue", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4247dcb3!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./statisticsWrap.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 2013:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1552);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7d409f02", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4247dcb3&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./statisticsWrap.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-4247dcb3&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./statisticsWrap.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 609:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2012)
__webpack_require__(2013)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1412),
  /* template */
  __webpack_require__(1865),
  /* scopeId */
  "data-v-4247dcb3",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\perAnalysis\\statisticsWrap.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] statisticsWrap.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4247dcb3", Component.options)
  } else {
    hotAPI.reload("data-v-4247dcb3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});