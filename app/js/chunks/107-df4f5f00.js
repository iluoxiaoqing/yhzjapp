webpackJsonp([107],{

/***/ 1416:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            allHotList: [{
                title: "政策解读",
                active: true,
                hotList: [],
                page: 1,
                isNoData: false
            }],
            hotList: [],
            isLoading: true,
            currentPage: 1,
            page: "",
            hasData: false,
            mySwiper: null,
            defaultText: "",
            index: 0,
            imgUrl: _apiConfig2.default.KY_IP
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        var _this = this;

        window.goBack = this.goBack;
        this.getHeight();
        var type = 0;
        this.index = type;

        this.getPageList();

        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            followFinger: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {

                document.getElementById("scrollContainer").scrollTop = 0;

                var index = swiper.activeIndex;
                _this.currentPage = 1;
                _this.allHotList.map(function (el) {
                    el.active = false;
                });
                _this.allHotList[index].active = true;

                _this.index = index;
                if (_this.allHotList[_this.index].hotList.length == 0) {
                    _this.getPageList();
                }
            }
        });
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        loadmore: function loadmore() {
            this.getPageList();
        },
        refresh: function refresh() {
            this.$set(this.allHotList[this.index], "page", 1);
            this.$set(this.allHotList[this.index], "hotList", []);
            this.getPageList();
        },
        getImg: function getImg(url) {
            if (url.substr(0, 7).toLowerCase() == "http://" || url.substr(0, 8).toLowerCase() == "https://") {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        },
        getPageList: function getPageList() {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "policy/policies",
                data: {
                    "page": this.allHotList[this.index].page
                },
                showLoading: false
            }, function (data) {

                var curPage = _this2.allHotList[_this2.index].page;
                console.log("this.allHotList", _this2.allHotList);

                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        _this2.$set(el, "show", false);
                        _this2.allHotList[_this2.index].hotList.push(el);
                    });

                    curPage++;
                    _this2.$set(_this2.allHotList[_this2.index], "page", curPage);
                    _this2.$set(_this2.allHotList[_this2.index], "isNoData", false);
                } else {
                    if (curPage == 1) {
                        _this2.$set(_this2.allHotList[_this2.index], "isNoData", true);
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1484:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\policy\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1674:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_xiaoyaotuishou.png?v=7c9d178e";

/***/ }),

/***/ 1798:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "swiper-container",
    attrs: {
      "id": "swiper-container"
    }
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, _vm._l((_vm.allHotList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide",
      style: ({
        'height': index == 0 ? 'auto' : '0px'
      })
    }, _vm._l((i.hotList), function(j, index) {
      return _c('div', {
        key: index,
        staticClass: "main"
      }, [_c('router-link', {
        attrs: {
          "to": {
            name: 'policyDetail',
            query: {
              id: j.id,
              title: j.title
            }
          }
        }
      }, [_c('div', {
        staticClass: "nickLogo"
      }, [_c('img', {
        attrs: {
          "src": __webpack_require__(1674),
          "alt": ""
        }
      })]), _vm._v(" "), _c('div', {
        staticClass: "content"
      }, [_c('div', {
        staticClass: "topMain"
      }, [_c('div', {
        staticClass: "title"
      }, [_vm._v("\n                                        逍遥推手\n                                        ")]), _vm._v(" "), _c('div', {
        staticClass: "time"
      }, [_vm._v("\n                                            " + _vm._s(j.date) + "\n                                        ")])]), _vm._v(" "), _c('div', {
        staticClass: "detail"
      }, [_vm._v("\n                                        " + _vm._s(j.title) + "\n                                    ")]), _vm._v(" "), _c('div', {
        staticClass: "policyImg"
      }, [_c('img', {
        attrs: {
          "src": _vm.getImg(j.pic)
        }
      })])])])], 1)
    }), 0)
  }), 0)])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-011302ef", module.exports)
  }
}

/***/ }),

/***/ 1945:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1484);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7591fe0f", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-011302ef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./policy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-011302ef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./policy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 613:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1945)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1416),
  /* template */
  __webpack_require__(1798),
  /* scopeId */
  "data-v-011302ef",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\policy\\policy.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] policy.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-011302ef", Component.options)
  } else {
    hotAPI.reload("data-v-011302ef", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});