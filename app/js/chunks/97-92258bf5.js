webpackJsonp([97],{

/***/ 1383:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            rankList: [],
            showLoading: true,
            rankOneself: ""
        };
    },
    mounted: function mounted() {
        this.getList();
    },

    methods: {
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "activation/rank",
                "type": "post",
                "data": {}
            }, function (data) {
                console.log(data);
                _this.rankList = data.data.rankList;
                _this.rankOneself = data.data.rankOneself;
            });
        }
    }
};

/***/ }),

/***/ 1598:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-82e99116] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-82e99116] {\n  background: #fff;\n}\n.tips_success[data-v-82e99116] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-82e99116] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-82e99116] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-82e99116],\n.fade-leave-active[data-v-82e99116] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-82e99116],\n.fade-leave-to[data-v-82e99116] {\n  opacity: 0;\n}\n.default_button[data-v-82e99116] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-82e99116] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-82e99116] {\n  position: relative;\n}\n.loading-tips[data-v-82e99116] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.total[data-v-82e99116] {\n  width: 100%;\n  /*弹窗*/\n}\n.total .top-con[data-v-82e99116] {\n  background: url(" + __webpack_require__(1656) + ") no-repeat;\n  width: 100%;\n  height: 4rem;\n  background-size: 100% 100%;\n  background-repeat: no-repeat;\n}\n.total .detail-con .tab[data-v-82e99116] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.total .detail-con .tab li[data-v-82e99116] {\n  padding-bottom: .2rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  -ms-grid-column-align: flex-start;\n      justify-items: flex-start;\n  font-family: PingFangSC-Regular;\n  font-size: .34rem;\n  color: rgba(61, 74, 91, 0.6);\n  text-align: center;\n  position: relative;\n}\n.total .detail-con .tab li[data-v-82e99116]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #BFC5D1;\n  width: 100%;\n  height: 0px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  opacity: 0.6;\n}\n.total .detail-con .tab li.active[data-v-82e99116] {\n  font-weight: bold;\n  color: #3f83ff;\n}\n.total .detail-con .tab li.active[data-v-82e99116]:after {\n  background: #3F83FF;\n  opacity: 1;\n  height: 0.12rem;\n  width: 0.56rem;\n  margin-left: 38%;\n  border-radius: 2px;\n}\n.total .detail-con .datetab[data-v-82e99116] {\n  width: 64%;\n  margin: 0.28rem 18% 0.18rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.total .detail-con .datetab li[data-v-82e99116] {\n  width: 1.68rem;\n  height: 0.56rem;\n  line-height: 0.56rem;\n  background: #F6F6F6;\n  font-size: .28rem;\n  color: #3f3f50;\n  text-align: center;\n  border-radius: 0.28rem;\n  float: left;\n}\n.total .detail-con .datetab li.active[data-v-82e99116] {\n  font-weight: bold;\n  color: #3F83FF;\n  background: #F1F6FF;\n  border: 1px solid rgba(63, 131, 255, 0.5);\n}\n.total .detail-con .datetab li[data-v-82e99116]:nth-of-type(2) {\n  margin-left: 1.56rem;\n}\n.total .detail-con .ownRank[data-v-82e99116] {\n  width: 100%;\n  height: 1.94rem;\n  background: #F7F8FA;\n}\n.total .detail-con .ownRank .ownMain[data-v-82e99116] {\n  width: 100%;\n  margin: 0.24rem 0;\n  height: 1.46rem;\n  display: inline-block;\n  background: #ffffff;\n}\n.total .detail-con .ownRank .ownMain .ownleft[data-v-82e99116] {\n  width: 50%;\n  float: left;\n  font-size: 0.28rem;\n  padding: 0.32rem 0 0 0.36rem;\n  color: #424242;\n}\n.total .detail-con .ownRank .ownMain .ownleft span[data-v-82e99116] {\n  color: #333333;\n  font-size: 0.32rem;\n  font-weight: 500;\n}\n.total .detail-con .ownRank .ownMain .ownright[data-v-82e99116] {\n  text-align: right;\n  margin-right: 0.36rem;\n  float: right;\n  width: 40%;\n  margin-top: 0.4rem;\n  font-size: 0.28rem;\n  color: #424242;\n}\n.total .detail-con .detail-list .item ul[data-v-82e99116] {\n  background: #FFFFFF;\n}\n.total .detail-con .detail-list .item ul li[data-v-82e99116] {\n  height: 1.46rem;\n  background-position: right;\n  background-size: .16rem;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.total .detail-con .detail-list .item ul li .logo[data-v-82e99116] {\n  width: 70%;\n  float: left;\n  font-size: 0.3rem;\n  color: #333333;\n}\n.total .detail-con .detail-list .item ul li .logo .logo_top[data-v-82e99116] {\n  margin-left: 0.36rem;\n  float: left;\n  display: inline-block;\n  width: 100%;\n}\n.total .detail-con .detail-list .item ul li .logo .logo_top span[data-v-82e99116] {\n  float: left;\n  font-weight: 500;\n}\n.total .detail-con .detail-list .item ul li .logo .logo_top img[data-v-82e99116] {\n  width: 0.4rem;\n  height: 0.34rem;\n  margin-top: -0.1rem;\n  margin-left: 0.1rem;\n  display: inline-block;\n  vertical-align: middle;\n}\n.total .detail-con .detail-list .item ul li .logo .dis[data-v-82e99116] {\n  width: 100%;\n  margin-left: 0.36rem;\n}\n.total .detail-con .detail-list .item ul li .right[data-v-82e99116] {\n  margin-right: 0.34rem;\n  text-align: right;\n}\n.total .detail-con .detail-list .item ul li .right span[data-v-82e99116] {\n  font-weight: bold;\n  text-align: right;\n  font-size: 0.28rem;\n}\n.total .detail-con .detail-list .item ul li .logoNum[data-v-82e99116] {\n  font-size: 0.3rem;\n  width: 60%;\n  float: left;\n  color: #333333;\n  margin-left: 0.36rem;\n}\n.total .detail-con .detail-list .item ul li .logoNum span[data-v-82e99116] {\n  float: left;\n  font-weight: 500;\n}\n.total .detail-con .detail-list .item ul li .logoNum .dis[data-v-82e99116] {\n  width: 100%;\n  float: left;\n}\n.total .detail-con .detail-list .item ul li .logo1[data-v-82e99116] {\n  width: 30%;\n  float: left;\n  margin-right: 0.36rem;\n  text-align: right;\n  font-size: 0.28rem;\n  color: #FF1C43;\n}\n.total .detail-con .detail-list .item ul li .logo1 img[data-v-82e99116] {\n  width: 0.8rem;\n  height: 0.7rem;\n  vertical-align: middle;\n}\n.total .detail-con .detail-list .item ul li .logo1 .dis[data-v-82e99116] {\n  width: 55%;\n  float: left;\n}\n.total .detail-con .detail-list .item ul li .text[data-v-82e99116] {\n  margin-right: 0.36rem;\n  width: 30%;\n  text-align: right;\n  font-size: 0.28rem;\n  color: #FF1C43;\n  float: left;\n}\n.total .detail-con .detail-list .item ul li .text1[data-v-82e99116] {\n  width: 30%;\n  float: left;\n  margin-right: 0.36rem;\n  text-align: right;\n  font-size: 0.28rem;\n  color: #999999;\n}\n.total .detail-con .detail-list .item ul li[data-v-82e99116]:after {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #313757;\n  width: 100%;\n  height: 1px;\n  opacity: 0.1;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.total .detail-con .detail-list .item ul li[data-v-82e99116]:last-child:after {\n  height: 0;\n}\n.total .detail-con .detail-list .item ul li p[data-v-82e99116] {\n  font-family: PingFangSC-Regular;\n  font-size: .28rem;\n  color: #3D4A5B;\n}\n.total .detail-con .empty[data-v-82e99116] {\n  font-size: 0.16rem;\n  color: #AFAFAF;\n  line-height: 22px;\n  text-align: center;\n}\n.total .detail-con .empty img[data-v-82e99116] {\n  width: 3.54rem;\n  height: 1.92rem;\n  margin-top: 2rem;\n}\n.total .dialog[data-v-82e99116] {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.total .dialog .rule[data-v-82e99116] {\n  width: 7.08rem;\n  height: 7.5rem;\n  background: #ffffff;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-left: -3.54rem;\n  margin-top: -3.75rem;\n  border-radius: 6px;\n  color: #333333;\n}\n.total .dialog .rule h5[data-v-82e99116] {\n  font-size: 0.36rem;\n  text-align: center;\n  font-weight: 400;\n  margin: 0.44rem 0 0.4rem;\n}\n.total .dialog .rule p[data-v-82e99116] {\n  font-size: 0.28rem;\n  line-height: 0.48rem;\n  margin-left: 0.42rem;\n}\n.total .dialog .rule p b[data-v-82e99116] {\n  font-weight: bold;\n}\n.total .dialog .rule img[data-v-82e99116] {\n  position: fixed;\n  right: 0.32rem;\n  width: 0.32rem;\n  height: 0.32rem;\n  right: 0.6rem;\n  margin-top: -0.8rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1654:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/empty.png?v=4a57060c";

/***/ }),

/***/ 1656:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/perImg1.png?v=0df14533";

/***/ }),

/***/ 1911:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "total"
  }, [_c('div', {
    staticClass: "top-con"
  }), _vm._v(" "), _c('div', {
    staticClass: "detail-con"
  }, [_c('div', {
    staticClass: "ownRank"
  }, [_c('div', {
    staticClass: "ownMain"
  }, [_c('div', {
    staticClass: "ownleft"
  }, [_c('span', [_vm._v(_vm._s(_vm.rankOneself.realName) + "（" + _vm._s(_vm.rankOneself.phoneNo) + "）")]), _c('br'), _vm._v("\n                        激活台数：" + _vm._s(_vm.rankOneself.activationNum) + "台\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "ownright"
  }, [_vm._v("\n                        昨日排行"), _c('br'), _vm._v("No." + _vm._s(_vm.rankOneself.rank) + "\n                    ")])])]), _vm._v(" "), (_vm.rankList.length > 0) ? _c('div', {
    staticClass: "detail-list"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('ul', _vm._l((_vm.rankList), function(el, i) {
    return _c('li', {
      key: i
    }, [(i == 0 || i == 1 || i == 2) ? _c('div', {
      staticClass: "logo"
    }, [_c('div', {
      staticClass: "logo_top"
    }, [_c('span', [_vm._v("No." + _vm._s(el.rank))]), _vm._v(" "), _c('img', {
      attrs: {
        "src": './image/perImg' + (i + 2) + '.png',
        "alt": ""
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "dis"
    }, [_c('p', [_vm._v(_vm._s(el.realName) + "（" + _vm._s(el.phoneNo) + "）")])])]) : _c('div', {
      staticClass: "logoNum"
    }, [_c('span', [_vm._v("No." + _vm._s(el.rank))]), _c('br'), _vm._v(" "), _c('div', {
      staticClass: "dis"
    }, [_c('p', [_vm._v(_vm._s(el.realName) + "（" + _vm._s(el.phoneNo) + "）")])])]), _vm._v(" "), _c('div', {
      staticClass: "right"
    }, [_c('p', [_vm._v("昨日激活台数"), _c('br'), _vm._v(" "), _c('span', [_vm._v(_vm._s(el.activationNum))]), _vm._v("台\n                                ")])])])
  }), 0)])]) : _c('div', {
    staticClass: "empty"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1654),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("暂无数据")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-82e99116", module.exports)
  }
}

/***/ }),

/***/ 2059:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1598);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2ec6bfea", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-82e99116&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activeRank.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-82e99116&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activeRank.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2059)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1383),
  /* template */
  __webpack_require__(1911),
  /* scopeId */
  "data-v-82e99116",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\businessRank\\activeRank.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] activeRank.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-82e99116", Component.options)
  } else {
    hotAPI.reload("data-v-82e99116", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});