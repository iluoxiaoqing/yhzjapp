webpackJsonp([88],{

/***/ 1465:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            orderNo: this.$route.query.orderNo,
            status: "",
            orderDetail: {},
            minute: 0,
            second: 0,
            countText: "0分0秒",
            overtime: false,
            discountAmount: "",
            expressName: "",
            expressNo: "",
            imgUrl: _apiConfig2.default.KY_IP
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "salesBill/querySalesBillDetail",
            "data": {
                "orderNo": this.orderNo
            }
        }, function (data) {

            _this.orderDetail = data.data;
            _this.status = data.data.status;
            //console.log(data.data);
            _this.discountAmount = data.data.discountAmount;
            _this.expressName = data.data.expressName;
            _this.expressNo = data.data.expressNo;

            if (data.data.status == 'REPAY') {

                var timer = null;
                _this.minute = data.data.closeTime.split(":")[0] * 1;
                _this.second = data.data.closeTime.split(":")[1] * 1;

                timer = setInterval(function () {

                    if (_this.minute == 0 && _this.second == 0) {
                        _this.overtime = true;
                        _this.status = "DISABLE";
                        clearInterval(timer);
                    } else if (_this.minute >= 0) {
                        if (_this.second > 0) {
                            _this.second--;
                        } else if (_this.second == 0) {
                            _this.minute--;
                            _this.second = 59;
                        }
                    }

                    _this.countText = _this.addZero(_this.minute) + "分" + _this.addZero(_this.second) + "秒";
                }, 1000);
            }
        });

        window.alipayResult = this.alipayResult;
    },

    methods: {
        alipayResult: function alipayResult(state) {
            var _this2 = this;

            if (state == "true" || state == 1 || state == true || state == '1') {
                common.toast({
                    "content": "支付成功"
                });
            } else {
                common.toast({
                    "content": "支付失败"
                });
            }

            setTimeout(function () {
                _this2.$router.push({
                    "path": "myOrder",
                    "query": {
                        "type": 1
                    }
                });
            }, 1000);
        },
        addZero: function addZero(n) {
            if (n < 10) {
                return "0" + n;
            } else {
                return n;
            }
        },
        gotoPay: function gotoPay() {

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "alipay/evokeAliPay",
                "data": {
                    "outTradeNo": this.orderNo,
                    "subject": "",
                    "body": "",
                    "businessCode": "APP_SHOP_CODE"
                }
            }, function (data) {

                var aliPay = {
                    "alipay": data.data,
                    "redirectFunc": "alipayResult"
                };

                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                } else {
                    window.android.nativeAlipay(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                }
            });
        }
    }
};

/***/ }),

/***/ 1606:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1919:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "orderDetail"
  }, [_c('div', {
    staticClass: "orderDetail_banner"
  }, [(_vm.status == 'INIT') ? _c('div', {
    staticClass: "orderDetail_status waiting"
  }, [_c('b', [_vm._v("待发货")]), _vm._v(" "), _c('p', [_vm._v("包裹正在出库，请耐心等待")])]) : (_vm.status == 'REPAY') ? _c('div', {
    staticClass: "orderDetail_status paying"
  }, [(!_vm.overtime) ? _c('b', [_vm._v("待付款")]) : _vm._e(), _vm._v(" "), (!_vm.overtime) ? _c('p', [_vm._v("剩 " + _vm._s(_vm.countText) + " 后自动关闭")]) : _vm._e(), _vm._v(" "), (!_vm.overtime) ? _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_vm._v("去付款")]) : _vm._e()]) : (_vm.status == 'DISABLE') ? _c('div', {
    staticClass: "orderDetail_status paying gray"
  }, [_c('b', [_vm._v("已失效")]), _vm._v(" "), _c('p', [_vm._v("订单已自动关闭")])]) : (_vm.status == 'DELIVERY') ? _c('div', {
    staticClass: "orderDetail_status send"
  }, [_c('b', [_vm._v("发货中")]), _vm._v(" "), _c('p', [_vm._v("快递方式：" + _vm._s(_vm.expressName))]), _vm._v(" "), _c('p', [_vm._v("快递编号：" + _vm._s(_vm.expressNo))])]) : _c('div', {
    staticClass: "orderDetail_status send"
  }, [_c('b', [_vm._v("已发货")]), _vm._v(" "), _c('p', [_vm._v("快递方式：" + _vm._s(_vm.expressName))]), _vm._v(" "), _c('p', [_vm._v("快递编号：" + _vm._s(_vm.expressNo))])])]), _vm._v(" "), _c('div', {
    staticClass: "orderDetail_address",
    class: {
      'gray': _vm.status == 'DISABLE'
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.orderDetail.province) + " " + _vm._s(_vm.orderDetail.city))]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.orderDetail.address))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.orderDetail.userName) + " " + _vm._s(_vm.orderDetail.phone))])]), _vm._v(" "), _c('div', {
    staticClass: "orderDetail_goods",
    class: {
      'gray': _vm.status == 'DISABLE'
    }
  }, [_vm._l((_vm.orderDetail.orderList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "goods_body"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.picturePath
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "goods_desc"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), (i.remark) ? _c('span', [_vm._v(_vm._s(JSON.parse(i.remark).goodsDesc))]) : _vm._e()]), _vm._v(" "), _c('p', {
      staticStyle: {
        "display": "flex",
        "justify-content": "space-between"
      }
    }, [_c('em', [_vm._v("¥" + _vm._s(i.price))]), _vm._v(" "), _c('i', [_vm._v("X" + _vm._s(i.goodsCount))])])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "goods_footer"
  }, [_c('p', [_vm._v("订单编号：" + _vm._s(_vm.orderDetail.orderNo))]), _vm._v(" "), _c('p', [_vm._v("共1件商品 实付："), _c('b', [_vm._v(_vm._s(_vm.orderDetail.amount))]), _vm._v("元")])])], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a48f59d4", module.exports)
  }
}

/***/ }),

/***/ 2067:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1606);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0c81fcb2", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a48f59d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./orderDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a48f59d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./orderDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 670:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2067)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1465),
  /* template */
  __webpack_require__(1919),
  /* scopeId */
  "data-v-a48f59d4",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\orderDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] orderDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a48f59d4", Component.options)
  } else {
    hotAPI.reload("data-v-a48f59d4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});