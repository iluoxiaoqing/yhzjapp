webpackJsonp([4],{

/***/ 1238:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num1.png?v=509e4726";

/***/ }),

/***/ 1239:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num2.png?v=8bacfc23";

/***/ }),

/***/ 1240:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num3.png?v=3e5e7cf3";

/***/ }),

/***/ 1241:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num4.png?v=e00b3d29";

/***/ }),

/***/ 1449:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0,
            isShow: false
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '合利宝大POS');
    },

    methods: {
        go: function go() {
            common.setCookie("apiType", '20200720_HELIBAO');
            window.location.href = _apiConfig2.default.WEB_URL + 'jlHkrt';
            // window.location.reload();
        },
        seeJl: function seeJl() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';

            // window.location.reload();
        }
    }
};

/***/ }),

/***/ 1554:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1707:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliposBg5.png?v=c45b65c6";

/***/ }),

/***/ 1867:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "helipos_main"
  }, [_c('div', {
    staticClass: "time_pos"
  }, [_vm._v("\r\n                        活动时间：即日起至2021年6月30日\r\n        ")]), _vm._v(" "), _vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "title_right"
  }, [_vm._v("\r\n                活动规则\r\n        ")]), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "heli3_pos"
  }, [_c('div', {
    staticClass: "send_btn_heli_pos",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "seeJl_heli_pos",
    on: {
      "click": function($event) {
        return _vm.seeJl()
      }
    }
  })])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "title_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1707),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "center_one_pos"
  }, [_c('ul', [_c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1238),
      "alt": ""
    }
  }), _vm._v("\r\n                                广州合利宝支付科技有限公司：银联认证+国企护航\r\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1239),
      "alt": ""
    }
  }), _vm._v("\r\n                                刷  卡  费  率： 刷卡 "), _c('em', [_vm._v("0.60%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("           \r\n                                                   \r\n                                        小额双免")]), _vm._v(" "), _c('em', [_vm._v("0.38%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("扫  码  费  率： 支付宝/微信")]), _vm._v(" "), _c('em', [_vm._v("0.38%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("VIP刷卡费率： ")]), _c('em', [_vm._v("0.55%")]), _c('br')]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1240),
      "alt": ""
    }
  }), _vm._v("\r\n                                分润规则：刷卡分润万5~万9\r\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1241),
      "alt": ""
    }
  }), _vm._v("\r\n                                产品特点：超低VIP刷卡手续费 "), _c('span', [_vm._v("帮您省钱")]), _vm._v(" "), _c('br')])]), _vm._v(" "), _c('div', {
    staticClass: "heli_down_pos"
  }, [_vm._v("\r\n                        商户自行下载【小利生活】APP进行注册"), _c('br'), _vm._v("\r\n                        商户注册邀请码：VGMA9T\r\n                ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "center_two_pos"
  }, [_c('ul', [_c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1238),
      "alt": ""
    }
  }), _vm._v("\r\n                                机具采购价格：199元/台\r\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1239),
      "alt": ""
    }
  }), _vm._v("\r\n                                达标返现条件：缴纳299服务费且激活90天内"), _c('br'), _vm._v(" "), _c('span', {
    staticStyle: {
      "margin-left": "2.2rem"
    }
  }, [_vm._v("交易金额≥10000")]), _c('br'), _vm._v(" "), _c('table', {
    attrs: {
      "width": "100%",
      "border": "0",
      "cellspacing": "1",
      "cellpadding": "0"
    }
  }, [_c('tr', [_c('td', [_vm._v("礼包")]), _vm._v(" "), _c('td', [_vm._v("返直属推手")]), _vm._v(" "), _c('td', [_vm._v("返上级推手")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("1台")]), _vm._v(" "), _c('td', [_vm._v("439元/台")]), _vm._v(" "), _c('td', [_vm._v("60元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("5台")]), _vm._v(" "), _c('td', [_vm._v("459元/台")]), _vm._v(" "), _c('td', [_vm._v("40元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("10台")]), _vm._v(" "), _c('td', [_vm._v("479元/台")]), _vm._v(" "), _c('td', [_vm._v("20元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("30台")]), _vm._v(" "), _c('td', [_vm._v("499元/台")]), _vm._v(" "), _c('td', [_vm._v("0元/台")])])]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_vm._v("\r\n                                        注意：采购对应级别礼包才能获得对应级别返现，不累加\r\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1240),
      "alt": ""
    }
  }), _vm._v("\r\n                                累计交易返现：激活180天内刷卡金额≥88万"), _c('br'), _vm._v(" "), _c('span', [_vm._v("返  现  金  额：200元/台")]), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_vm._v("\r\n                                        注意：累计交易量仅统计VIP刷卡与普通刷卡的交易量\r\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1241),
      "alt": ""
    }
  }), _vm._v("\r\n                                最终解释权归逍遥推手所有\r\n                        ")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-43306491", module.exports)
  }
}

/***/ }),

/***/ 2015:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1554);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("380b50b4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-43306491&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./ybzf.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-43306491&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./ybzf.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 654:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2015)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1449),
  /* template */
  __webpack_require__(1867),
  /* scopeId */
  "data-v-43306491",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\heliPos.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] heliPos.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-43306491", Component.options)
  } else {
    hotAPI.reload("data-v-43306491", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});