webpackJsonp([90],{

/***/ 1307:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_3.png?v=168acea9";

/***/ }),

/***/ 1450:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

common.youmeng("备货开店", "进入备货开店");

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            allShareCode: {},
            shopGroups: [],
            showAd: "1",
            context: '',
            title: ""
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        this.getList();
        //        this.getShowAd();
    },

    methods: {
        share: function share(j) {
            console.log("分享", "分享");
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j.shareCode,
                    "productName": j.title,
                    "channel": "hlwShop"
                }
            });
        },
        close: function close() {
            this.showAd = !this.showAd;
        },
        allShare: function allShare(j) {
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j,
                    "productName": "全部推荐",
                    "channel": "hlwShop"
                }
            });
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/jiFenBusiness",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this.allShareCode = data.data.shareCode; //全部分享code
                _this.shopGroups = data.data.groups;
                console.log("shopGroups", _this.shopGroups);
            });
        },
        apply: function apply(goodsType) {
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "jiFenDui/createOrder",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                window.location.href = data.data;
            });
        },
        gotoMall: function gotoMall(i, j) {
            console.log("====", i);
            common.youmeng("备货开店", "点击备货开店");
            common.setCookie("proCode", i);
            console.log("url===", _apiConfig2.default.WEB_URL + 'mall?j=' + j);
            window.location.href = _apiConfig2.default.WEB_URL + 'mall?j=' + j;
            window.location.reload();
        }
    }
};

/***/ }),

/***/ 1569:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-5834ade0] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-5834ade0] {\n  background: #fff;\n}\n.tips_success[data-v-5834ade0] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-5834ade0] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-5834ade0] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-5834ade0],\n.fade-leave-active[data-v-5834ade0] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-5834ade0],\n.fade-leave-to[data-v-5834ade0] {\n  opacity: 0;\n}\n.default_button[data-v-5834ade0] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-5834ade0] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-5834ade0] {\n  position: relative;\n}\n.loading-tips[data-v-5834ade0] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.mallShop[data-v-5834ade0] {\n  width: 100%;\n  overflow: hidden;\n  background: #ffffff;\n  margin-bottom: 3rem;\n}\n.mallShop .main[data-v-5834ade0] {\n  width: 92%;\n  margin: 0 4%;\n}\n.mallShop .main .title[data-v-5834ade0] {\n  opacity: 0.6;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  margin: 0.32rem 0 0;\n}\n.mallShop .main .shopMain[data-v-5834ade0] {\n  width: 100%;\n  margin-top: 0.16rem;\n  box-shadow: 0 2px 12px 0 rgba(99, 113, 136, 0.12);\n  border-radius: 4px;\n}\n.mallShop .main .shopMain .shopTop[data-v-5834ade0] {\n  padding: 0.32rem 0.3rem;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: absolute;\n}\n.mallShop .main .shopMain .shopTop[data-v-5834ade0]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #ECF0FF !important;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n  -ms-transform: scaleY(0.5);\n  transform: scaleY(0.5);\n  width: 92%;\n  margin: 0 4%;\n}\n.mallShop .main .shopMain .shopTop .listImg[data-v-5834ade0] {\n  width: 1.4rem;\n  height: 1.4rem;\n}\n.mallShop .main .shopMain .shopTop .listImg img[data-v-5834ade0] {\n  width: 100%;\n  height: 100%;\n  display: block;\n}\n.mallShop .main .shopMain .shopTop .listDeatil[data-v-5834ade0] {\n  width: 4.7rem;\n  height: 1.4rem;\n  margin-left: 0.24rem;\n  position: relative;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .titleLeft[data-v-5834ade0] {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.28rem;\n  line-height: 0.44rem;\n  float: left;\n  font-weight: bold;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .titleRight[data-v-5834ade0] {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.36rem;\n  color: #FF7F96;\n  float: right;\n}\n.mallShop .main .shopMain .shopTop .listDeatil p[data-v-5834ade0] {\n  color: #3D4A5B;\n  font-size: 0.24rem;\n  opacity: 0.6;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .acFee[data-v-5834ade0] {\n  display: inline-block;\n  font-size: 0.26rem;\n  margin-top: 0.08rem;\n  float: left;\n  color: rgba(61, 74, 91, 0.4);\n}\n.mallShop .main .shopMain .shopTop .listDeatil .time[data-v-5834ade0] {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  top: 0.52rem;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .time i[data-v-5834ade0] {\n  color: rgba(61, 74, 91, 0.4);\n  font-size: 0.26rem;\n  display: block;\n  width: 3rem;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .time .aaaa[data-v-5834ade0] {\n  float: right;\n  top: 0.3rem;\n  right: 0.2rem;\n  position: absolute;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .time .aaaa .bgGray[data-v-5834ade0] {\n  background: #313757;\n  opacity: 0.4 !important;\n}\n.mallShop .main .shopMain .shopTop .listDeatil .time .aaaa a[data-v-5834ade0] {\n  width: 1.2rem;\n  height: 0.48rem;\n  line-height: 0.48rem;\n  display: block;\n  font-size: 0.28rem;\n  text-align: center;\n  background-size: contain;\n  color: #FFFFFF;\n  background: #3F83FF;\n  border-radius: 4px;\n}\n.mallShop .main .shopMain .shopBottom[data-v-5834ade0] {\n  text-align: center;\n  font-size: 0.32rem;\n  color: #3F83FF;\n  letter-spacing: 0;\n  font-weight: bold;\n  display: inline-block;\n  margin-top: 2.3rem;\n  width: 100%;\n}\n.mallShop .main .shopMain .shopBottom span[data-v-5834ade0] {\n  float: left;\n  color: #3D4A5B;\n  font-size: 0.3rem;\n  margin-left: 0.7rem;\n  font-weight: 400 !important;\n}\n.mallShop .main .shopMain .shopBottom input[data-v-5834ade0] {\n  width: 1.7rem;\n  float: left;\n  line-height: 0.48rem;\n}\n.mallShop .main .shopMain .shopBottom img[data-v-5834ade0] {\n  width: 0.4rem;\n  opacity: 0.6;\n  float: left;\n  margin-top: 0.05rem;\n}\n.mallShop .main .shopMain .gray[data-v-5834ade0] {\n  color: rgba(61, 74, 91, 0.4) !important;\n  font-weight: bold;\n}\n.mallShop .fixed[data-v-5834ade0] {\n  position: fixed;\n  left: 0px;\n  bottom: 0.16rem;\n  width: 92%;\n  margin: 0 4%;\n  height: 0.92rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  z-index: 9999;\n  line-height: 0.92rem;\n  text-align: center;\n  font-size: 0.32rem;\n  color: #FFFFFF;\n}\n.whyShop_bg[data-v-5834ade0] {\n  width: 100%;\n  height: 100%;\n  overflow: hidden;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n      -ms-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  z-index: 100;\n  background: rgba(0, 0, 0, 0.65);\n}\n.whyShop_bg .btn_close[data-v-5834ade0] {\n  float: right;\n  margin-right: 0.36rem;\n  margin-top: 1.3rem;\n}\n.whyShop_bg .btn_close img[data-v-5834ade0] {\n  width: 0.6rem;\n  height: 0.6rem;\n}\n.whyShop[data-v-5834ade0] {\n  width: 68%;\n  background: #ffffff;\n  border-radius: 0.04rem;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-left: -32%;\n  margin-top: -4.04rem;\n}\n.whyShop img[data-v-5834ade0] {\n  width: 100%;\n}\n.whyShop h1[data-v-5834ade0] {\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  margin-top: 0.64rem;\n  margin-bottom: 0.48rem;\n}\n.whyShop p[data-v-5834ade0] {\n  color: #3D4A5B;\n  font-size: 0.28rem;\n  padding: 0 0.44rem;\n  text-align: left;\n  margin-bottom: 0.16rem;\n}\n.whyShop a[data-v-5834ade0] {\n  width: 4.76rem;\n  height: 0.8rem;\n  background: #3F83FF;\n  border-radius: 0.08rem;\n  font-size: 0.32rem;\n  font-weight: 700;\n  display: block;\n  text-align: center;\n  line-height: 0.8rem;\n  margin: 0.56rem auto 0.64rem;\n  color: #fff;\n}\n", ""]);

// exports


/***/ }),

/***/ 1882:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mallShop"
  }, [_vm._l((_vm.shopGroups), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "main"
    }, [_c('div', {
      staticClass: "title"
    }, [_vm._v("\n\t\t\t\t" + _vm._s(i.name) + "\n\t\t\t")]), _vm._v(" "), _vm._l((i.details), function(j, index) {
      return _c('div', {
        staticClass: "shopMain"
      }, [(i.hasOnline == true) ? _c('div', {
        staticClass: "shopTop"
      }, [_c('div', {
        staticClass: "listImg"
      }, [_c('img', {
        attrs: {
          "src": _vm.imgUrl + 'file/downloadFile?filePath=' + j.logo
        }
      })]), _vm._v(" "), _c('div', {
        staticClass: "listDeatil"
      }, [_c('div', {
        staticClass: "titleLeft"
      }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
        staticClass: "titleRight"
      }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.fee) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
        staticClass: "acFee"
      }, [_vm._v(_vm._s(j.activityFee))]), _vm._v(" "), _c('div', {
        staticClass: "time"
      }, [_c('i', [_vm._v(_vm._s(j.company))]), _vm._v(" "), _c('i', [_vm._v(_vm._s(j.feature))]), _vm._v(" "), (j.share == true) ? _c('div', {
        staticClass: "aaaa"
      }, [_c('a', {
        attrs: {
          "href": "javascript:;"
        },
        on: {
          "click": function($event) {
            return _vm.share(j)
          }
        }
      }, [_vm._v(_vm._s(j.shareBtnText))])]) : _vm._e()])])]) : _c('div', {
        staticClass: "shopTop"
      }, [_vm._m(0, true), _vm._v(" "), _c('div', {
        staticClass: "listDeatil"
      }, [_c('div', {
        staticClass: "titleLeft"
      }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _vm._m(1, true)])]), _vm._v(" "), (i.hasOnline == true) ? _c('div', {
        staticClass: "shopBottom"
      }, [(j.needBuyMall == true) ? _c('div', {
        on: {
          "click": function($event) {
            return _vm.gotoMall(i.name, j.buyCode)
          }
        }
      }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(j.buyBtnText) + "\n\t\t\t\t\t")]) : _c('div', {
        on: {
          "click": function($event) {
            return _vm.apply(j.buyCode)
          }
        }
      }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(j.buyBtnText) + "\n\t\t\t\t\t")])]) : _c('div', {
        staticClass: "shopBottom gray"
      }, [_vm._v("\n\t\t\t\t\t开发中\n\t\t\t\t")])])
    })], 2)
  }), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.showAd == 0) ? _c('div', {
    staticClass: "whyShop_bg",
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "btn_close"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(953),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.close();
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "whyShop"
  }, [_c('h1', [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), (_vm.context[0]) ? _c('p', [_vm._v(_vm._s(_vm.context[0]))]) : _vm._e(), _vm._v(" "), (_vm.context[1]) ? _c('p', [_vm._v(_vm._s(_vm.context[1]))]) : _vm._e(), _vm._v(" "), (_vm.context[2]) ? _c('p', [_vm._v(_vm._s(_vm.context[2]))]) : _vm._e(), _vm._v(" "), (_vm.context[3]) ? _c('p', [_vm._v(_vm._s(_vm.context[3]))]) : _vm._e(), _vm._v(" "), (_vm.context[4]) ? _c('p', [_vm._v(_vm._s(_vm.context[4]))]) : _vm._e(), _vm._v(" "), (_vm.context[5]) ? _c('p', [_vm._v(_vm._s(_vm.context[5]))]) : _vm._e(), _vm._v(" "), (_vm.context[6]) ? _c('p', [_vm._v(_vm._s(_vm.context[6]))]) : _vm._e(), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        return _vm.close()
      }
    }
  }, [_vm._v("我知道了")])])]) : _vm._e()])], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1307),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v("敬请期待，即将上线")]), _vm._v(" "), _c('em', [_c('a', {
    staticClass: "bgGray",
    attrs: {
      "href": "javascript:;"
    }
  }, [_vm._v("分享")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5834ade0", module.exports)
  }
}

/***/ }),

/***/ 2030:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1569);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("1719677b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5834ade0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5834ade0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 655:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2030)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1450),
  /* template */
  __webpack_require__(1882),
  /* scopeId */
  "data-v-5834ade0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\hlwShop.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] hlwShop.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5834ade0", Component.options)
  } else {
    hotAPI.reload("data-v-5834ade0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 953:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/close.png?v=ff0c5a72";

/***/ })

});