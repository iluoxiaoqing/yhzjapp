webpackJsonp([95],{

/***/ 1215:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/cx_bg.png?v=d7472c89";

/***/ }),

/***/ 1390:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(158);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            text: "",
            isJump: ""
        };
    },
    mounted: function mounted() {
        this.getReg();
    },

    methods: {
        getReg: function getReg() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cx/autoRegister",
                data: {},
                showLoading: true
            }, function (data) {
                console.log(data.data);
                _this.text = data.data.depict;
                _this.isJump = data.data.appDownloadUrl;
            });
        },
        goDown: function goDown(url) {
            var url1 = url;
            if (common.isClient() == "ios") {
                try {
                    window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify({ url: url1 }));
                } catch (error) {
                    window.location.href = url1;
                }
            } else {
                try {
                    window.android.nativeOpenBrowser(JSON.stringify({ url: url1 }));
                } catch (error) {
                    window.location.href = url1;
                }
            }
        }
    }
};

/***/ }),

/***/ 1496:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.zfy_main {\n  width: 100%;\n  height: auto;\n  display: inline-block;\n}\n.zfy_main .cx_main {\n  width: 100%;\n}\n.zfy_main .cx_main .cx_logo {\n  width: 100%;\n  height: 3.54rem;\n  background: url(" + __webpack_require__(1215) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.zfy_main .cx_main .zc_main {\n  width: 100%;\n  background: #ffffff;\n  border-radius: 0.32rem;\n  top: -0.32rem;\n  position: relative;\n}\n.zfy_main .cx_main .zc_main .regMain {\n  width: 80%;\n  margin: 0 10% 0;\n  padding-top: 0.5rem;\n}\n.zfy_main .cx_main .zc_main .regMain .leftInput {\n  width: 60%;\n  border: 2px solid #f2f2f2;\n  float: left;\n  height: 0.88rem;\n  display: -webkit-inline-box;\n  display: -webkit-inline-flex;\n  display: -moz-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  margin-bottom: 0.25rem;\n}\n.zfy_main .cx_main .zc_main .regMain .leftInput input {\n  margin: 0 auto;\n  padding-left: 0.3rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  font-size: 0.28rem;\n  color: #000;\n  position: relative;\n  z-index: 5;\n  line-height: 0.4rem;\n  padding-top: 0.05rem;\n}\n.zfy_main .cx_main .zc_main .regMain .rightBtn {\n  width: 32%;\n  border: 2px solid #f2f2f2;\n  float: right;\n  height: 0.88rem;\n  font-size: 0.24rem;\n  margin-bottom: 0.25rem;\n}\n.zfy_main .cx_main .zc_main .regMain .rightBtn button {\n  font-size: 0.28rem;\n  color: #232121;\n  position: relative;\n  text-align: center;\n  line-height: 0.88rem;\n  z-index: 5;\n  display: block;\n  white-space: nowrap;\n  width: 100%;\n}\n.zfy_main .cx_main .zc_main .regMain .rightBtn button:disabled {\n  color: #f1f1f1;\n}\n.zfy_main .cx_main .zc_main .regMain .rightBtn button .activity {\n  color: #ff9f00;\n}\n.zfy_main .cx_main .zc_main .regMain p {\n  width: 100%;\n  height: 0.88rem;\n  margin: 0 auto;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border: 2px solid #f2f2f2;\n  border-radius: 0.1rem;\n  margin-bottom: 0.25rem;\n}\n.zfy_main .cx_main .zc_main .regMain p input {\n  margin: 0 auto;\n  padding-left: 0.3rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  font-size: 0.28rem;\n  color: #000;\n  position: relative;\n  z-index: 5;\n  line-height: 0.4rem;\n  padding-top: 0.05rem;\n}\n.zfy_main .cx_main .zc_main .regMain p img {\n  width: 1.72rem;\n  height: 0.48rem;\n}\n.zfy_main .cx_main .zc_main .regMain p button {\n  font-size: 0.28rem;\n  color: #ff9f00;\n  position: relative;\n  z-index: 5;\n  display: block;\n  white-space: nowrap;\n}\n.zfy_main .cx_main .zc_main .regMain p button:disabled {\n  color: #f1f1f1;\n}\n.zfy_main .cx_main .zc_main .regMain p button .activity {\n  color: #ff9f00;\n}\n.zfy_main .main {\n  width: 74%;\n  margin: 0 13%;\n}\n.zfy_main .main .zfy_logo {\n  width: 4.5rem;\n  height: 0.7rem;\n  margin-top: 0.64rem;\n  display: inline-block;\n}\n.zfy_main .main .zfy_logo img {\n  width: 100%;\n}\n.zfy_main .main .zfy_title {\n  font-size: 0.46rem;\n  font-weight: 500;\n  color: #333333;\n  margin-top: 0.6rem;\n  margin-bottom: 0.8rem;\n}\n.zfy_main .main .zc_main {\n  width: 100%;\n}\n.zfy_main .main .zc_main p {\n  width: 100%;\n  height: 1.07rem;\n  margin: 0 auto;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-bottom: 2px solid #f2f2f2;\n}\n.zfy_main .main .zc_main p input {\n  margin: 0 auto;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  font-size: 0.28rem;\n  color: #000;\n  position: relative;\n  z-index: 5;\n  line-height: 0.4rem;\n  padding-top: 0.05rem;\n}\n.zfy_main .main .zc_main p img {\n  width: 1.72rem;\n  height: 0.48rem;\n}\n.zfy_main .main .zc_main p button {\n  font-size: 0.28rem;\n  color: #ff9f00;\n  position: relative;\n  z-index: 5;\n  display: block;\n  white-space: nowrap;\n}\n.zfy_main .main .zc_main p button:disabled {\n  color: #f1f1f1;\n}\n.zfy_main .main .zc_main p button .activity {\n  color: #ff9f00;\n}\n.zfy_main a {\n  margin: 0.6rem 4% 0;\n  width: 92%;\n  height: 0.8rem;\n  background: -webkit-linear-gradient(bottom, #e47114, #ffc600);\n  background: linear-gradient(0deg, #e47114, #ffc600);\n  border-radius: 0.4rem;\n  display: inline-block;\n  text-align: center;\n  line-height: 0.8rem;\n  font-size: 0.36rem;\n  color: #fff;\n  font-weight: 700;\n}\n.zfy_main .btn {\n  width: 80%;\n  margin: 0 10%;\n  background: #023E90;\n  color: #ffffff;\n  font-size: 0.36rem;\n  border-radius: 0.1rem;\n  height: 0.88rem;\n  line-height: 0.88rem;\n}\n.zfy_main .btn:disabled {\n  opacity: 0.6;\n}\n.zfy_main .zfy_bottom {\n  width: 92%;\n  margin: 0.8rem 4% 0;\n  height: 1.9rem;\n  background: #f2f2f2;\n  border-radius: 0.2rem;\n}\n.zfy_main .zfy_bottom .text {\n  font-size: 0.24rem;\n  color: #000000;\n  text-align: center;\n  padding-top: 0.5rem;\n}\n.zfy_main .zfy_bottom .bottom_left {\n  float: left;\n  padding: 0.3rem 0 0 0.4rem;\n  width: 42%;\n  font-size: 0.24rem;\n  font-weight: 400;\n  color: #333333;\n}\n.zfy_main .zfy_bottom .bottom_left img {\n  width: 0.5rem;\n  height: 0.5rem;\n  float: left;\n  margin: 0.12rem 0.1rem 0 0;\n}\n.zfy_main .zfy_bottom .bottom_right {\n  float: left;\n  padding: 0.3rem 0 0 0.4rem;\n  width: 43%;\n  font-size: 0.24rem;\n  font-weight: 400;\n  color: #333333;\n}\n.zfy_main .zfy_bottom .bottom_right img {\n  width: 0.5rem;\n  height: 0.5rem;\n  float: left;\n  margin: 0.12rem 0.18rem 0 0;\n}\n.zfy_main .zfy_bottom em {\n  width: 100%;\n  font-size: 0.24rem;\n  color: #333333;\n  float: left;\n  text-align: center;\n  margin-top: 0.25rem;\n}\n.zfy_main .cx_bottom {\n  width: 92%;\n  margin: 0.5rem 4% 0;\n  height: 0.9rem;\n  background: #f2f2f2;\n  border-radius: 0.2rem;\n}\n.zfy_main .cx_bottom em {\n  width: 100%;\n  font-size: 0.24rem;\n  color: #333333;\n  float: left;\n  text-align: center;\n  margin-top: 0.25rem;\n}\n.registerEd {\n  width: 100%;\n}\n.registerEd p {\n  width: 60%;\n  text-align: center;\n  margin: 3rem 20% 1rem;\n  font-size: 0.32rem;\n  color: #000000;\n}\n.registerEd button {\n  width: 80%;\n  margin: 0 10%;\n  background: #023E90;\n  color: #ffffff;\n  font-size: 0.36rem;\n  border-radius: 0.1rem;\n  height: 0.88rem;\n  line-height: 0.88rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1778:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "registerEd"
  }, [_c('p', [_vm._v(_vm._s(_vm.text))]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goDown(_vm.isJump);
      }
    }
  }, [_vm._v("下载")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-45f4a73f", module.exports)
  }
}

/***/ }),

/***/ 1912:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1496);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("bf7531ce", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-45f4a73f!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-45f4a73f!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 610:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1912)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1390),
  /* template */
  __webpack_require__(1778),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\regZfy\\cxDownMall.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] cxDownMall.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45f4a73f", Component.options)
  } else {
    hotAPI.reload("data-v-45f4a73f", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});