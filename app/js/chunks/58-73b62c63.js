webpackJsonp([58],{

/***/ 1301:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ }),

/***/ 1395:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showTel: false,
            showWx: false,
            showDia: false,
            questionList: [],
            personInfo: [],
            index: 0,
            imgUrl: _apiConfig2.default.KY_IP,
            wxImg: "",
            touxiangImg: "",
            phone: "",
            userNo: common.getUserNo(),
            currentPage: 1,
            isLoading: true
        };
    },

    // activated () {

    // }
    activated: function activated() {

        this.getPersonInfo();
        window.saveImg = this.saveImg;

        this.getHeight();
        this.getQuestion();

        common.youmeng("专属客服", "进入专属客服");
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop - 70 + "px";
        },
        addWx: function addWx() {
            var _this = this;

            common.youmeng("专属客服", "点击加我微信");

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getCustService",
                data: {},
                showLoading: false
            }, function (data) {
                _this.wxImg = data.data.wxQrcodePath;
                if (_this.wxImg == "" || _this.wxImg == undefined) {
                    common.toast({
                        content: "您的客服还未上传微信二维码哟"
                    });
                } else {
                    _this.showDia = true;
                    _this.showWx = true;
                }
            });
        },
        callPhone: function callPhone(phone) {

            common.youmeng("专属客服", "点击拨打电话");

            var phoneJson = {};
            phoneJson.phone = phone;
            console.log(phoneJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeCall.postMessage(JSON.stringify(phoneJson));
            } else {
                window.android.nativeCall(JSON.stringify(phoneJson));
            }
        },
        wxCancel: function wxCancel() {
            this.showDia = false;
            this.showWx = false;
            common.youmeng("专属客服", "关闭二维码弹窗");
        },
        getQuestion: function getQuestion() {
            var _this2 = this;

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "custService/getQuestionByLabel",
                    data: {
                        label: "COMMON",
                        page: this.currentPage
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this2.currentPage == 1) {
                            _this2.questionList = [];
                        }
                        data.data.object.map(function (el) {
                            _this2.questionList.push(el);
                        });
                        _this2.currentPage++;
                        _this2.isLoading = true;
                        _this2.$refs.my_scroller.finishInfinite(true);
                    } else {
                        //done(2);
                        _this2.$refs.my_scroller.finishInfinite(2);
                    }
                    // this.questionList = data.data.object;
                });
            } else {
                //done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        },
        getPersonInfo: function getPersonInfo() {
            var _this3 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getCustService",
                data: {},
                showLoading: false
            }, function (data) {
                _this3.personInfo = data.data;
                _this3.wxImg = data.data.wxQrcodePath;
                _this3.phone = data.data.phoneNo;
                _this3.touxiangImg = data.data.touxiangPath;
            });
        },
        nativeSaveImage: function nativeSaveImage(url) {
            var imgJson = {};
            imgJson.imgUrl = url;
            imgJson.callbackName = "saveImg";
            console.log(imgJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeSaveImage.postMessage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            } else {
                window.android.nativeSaveImage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            }
        },
        saveImg: function saveImg(j) {
            console.log("返回状态值：" + j);
            if (j == true || j == 0) {
                common.youmeng("专属客服", "图片保存成功");
                console.log("保存成功");
            } else {
                common.youmeng("专属客服", "图片保存失败");
                console.log("保存失败");
            }
        },
        more_question: function more_question() {
            common.youmeng("专属客服", "点击更多问题");
        },
        refresh: function refresh() {
            var _this4 = this;

            var timer = null;
            this.currentPage = 1;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this4.getQuestion();
                _this4.$refs.my_scroller.finishPullToRefresh();
            }, 500);
        },
        infinite: function infinite() {
            var _this5 = this;

            var timer = null;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this5.getQuestion();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1567:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\feedBack\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1662:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_contact_phone.png?v=4d5a2282";

/***/ }),

/***/ 1663:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_contact_wechat.png?v=a66698d0";

/***/ }),

/***/ 1666:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump2.png?v=be22bebb";

/***/ }),

/***/ 1667:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_popwindow_close.png?v=3af1fd99";

/***/ }),

/***/ 1880:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showWx),
      expression: "showWx"
    }],
    staticClass: "addWx",
    attrs: {
      "id": "addWx"
    }
  }, [_c('img', {
    staticClass: "wx_can",
    attrs: {
      "src": __webpack_require__(1667),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.wxCancel()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "save_wx"
  }, [_c('img', {
    key: _vm.index,
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.wxImg,
      "alt": ""
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "btn_submit",
    on: {
      "click": function($event) {
        return _vm.nativeSaveImage(_vm.imgUrl + 'file/downloadFile?filePath=' + _vm.wxImg)
      }
    }
  }, [_vm._v("保存图片")])])])]), _vm._v(" "), _c('div', {
    staticClass: "service_top"
  }, [_c('div', {
    staticClass: "service_left"
  }, [_c('img', {
    key: _vm.index,
    attrs: {
      "src": _vm.touxiangImg,
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "service_right"
  }, [_c('div', {
    staticClass: "service_pic"
  }, [_vm._v("\n                    " + _vm._s(_vm.personInfo.name) + "\n                ")]), _vm._v(" "), _c('div', {
    staticClass: "service_text"
  }, [_vm._v("\n                    专属服务经理是我们面对用户的第一责任人，\n在享有平台相关权益的同时，也肩负指导、\n培新和为用户排异解惑的责任与义务\n                ")])])]), _vm._v(" "), _c('div', {
    staticClass: "service_connect"
  }, [_c('div', {
    staticClass: "connect_left",
    on: {
      "click": function($event) {
        return _vm.addWx()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1663),
      "alt": ""
    }
  }), _c('br'), _vm._v(" "), _c('p', [_vm._v("加我微信")])]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "connect_right"
  }, [_c('a', {
    attrs: {
      "href": 'tel:' + _vm.phone
    },
    on: {
      "click": function($event) {
        return _vm.callPhone(_vm.phone)
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1662),
      "alt": ""
    }
  }), _c('br')]), _vm._v(" "), _c('p', [_vm._v("给我打电话")])])]), _vm._v(" "), (!_vm.userNo) ? _c('div', {
    staticClass: "feedBack_title"
  }, [_vm._v("\n            常见问题\n        ")]) : _vm._e(), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('scroller', {
    ref: "my_scroller",
    attrs: {
      "noDataText": _vm.defaultText,
      "on-refresh": _vm.refresh,
      "on-infinite": _vm.infinite
    }
  }, [(!_vm.userNo) ? _c('div', {
    staticClass: "help_question"
  }, [_c('ul', _vm._l((_vm.questionList), function(i, index) {
    return _c('li', {
      key: index
    }, [_c('router-link', {
      attrs: {
        "to": {
          name: 'question_detail',
          query: {
            id: i.questionNo
          }
        }
      }
    }, [_c('p', [_vm._v(_vm._s(i.title))]), _vm._v(" "), _c('span', [_c('img', {
      attrs: {
        "src": __webpack_require__(1301),
        "alt": ""
      }
    })])])], 1)
  }), 0)]) : _vm._e()])], 1), _vm._v(" "), (!_vm.userNo) ? _c('router-link', {
    attrs: {
      "to": "feedBack_help"
    }
  }, [_c('div', {
    staticClass: "more_question",
    on: {
      "click": function($event) {
        return _vm.more_question()
      }
    }
  }, [_vm._v("\n                更多问题"), _c('img', {
    attrs: {
      "src": __webpack_require__(1666),
      "alt": ""
    }
  })])]) : _vm._e()], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "connect_center"
  }, [_c('div', {
    staticClass: "partlistline"
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5497a085", module.exports)
  }
}

/***/ }),

/***/ 2028:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1567);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("a8278b16", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5497a085&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_service.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5497a085&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_service.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 591:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2028)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1395),
  /* template */
  __webpack_require__(1880),
  /* scopeId */
  "data-v-5497a085",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\feedBack\\feedBack_service.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] feedBack_service.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5497a085", Component.options)
  } else {
    hotAPI.reload("data-v-5497a085", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});