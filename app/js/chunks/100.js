webpackJsonp([100],{

/***/ 1458:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-0ac1f4eb] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-0ac1f4eb] {\n  background: #fff;\n}\n.tips_success[data-v-0ac1f4eb] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-0ac1f4eb] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-0ac1f4eb] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-0ac1f4eb],\n.fade-leave-active[data-v-0ac1f4eb] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-0ac1f4eb],\n.fade-leave-to[data-v-0ac1f4eb] {\n  opacity: 0;\n}\n.default_button[data-v-0ac1f4eb] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-0ac1f4eb] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-0ac1f4eb] {\n  position: relative;\n}\n.loading-tips[data-v-0ac1f4eb] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.container[data-v-0ac1f4eb] {\n  /*弹窗*/\n}\n.container .dialog[data-v-0ac1f4eb] {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.container .dialog img[data-v-0ac1f4eb] {\n  width: 5.6rem;\n  height: 2.17rem;\n}\n.container .wx_main[data-v-0ac1f4eb] {\n  width: 100%;\n  height: 6.4rem;\n  background: url(" + __webpack_require__(1622) + ") no-repeat;\n  background-size: cover;\n  background-attachment: fixed;\n}\n.container .wx_main .app_logo[data-v-0ac1f4eb] {\n  display: inline-block;\n  margin: 1rem auto 0;\n  width: 100%;\n  text-align: center;\n  font-size: 0.4rem;\n  color: #ffffff;\n}\n.container .wx_main .app_logo img[data-v-0ac1f4eb] {\n  width: 3.4rem;\n  height: 1.16rem;\n  vertical-align: middle;\n}\n.container .wx_main .andiro_down[data-v-0ac1f4eb] {\n  margin: 0 10%;\n  width: 80%;\n  height: 0.8rem;\n  line-height: 0.8rem;\n  background: #f7a84d;\n  border-radius: 4px;\n  font-size: 0.34rem;\n  color: #1f1c17;\n  margin-top: 0.5rem;\n}\n.container .wx_main .andiro_down img[data-v-0ac1f4eb] {\n  width: 0.34rem;\n  height: 0.4rem;\n  margin-right: 0.2rem;\n  margin-left: 0.8rem;\n  vertical-align: middle;\n}\n.container .wx_main .andiro_down p[data-v-0ac1f4eb] {\n  line-height: 0.48rem;\n  display: inline;\n  vertical-align: middle;\n}\n.container .wx_main .ios_down[data-v-0ac1f4eb] {\n  margin: 0 10%;\n  width: 80%;\n  height: 0.8rem;\n  line-height: 0.8rem;\n  background: #f7a84d;\n  border-radius: 4px;\n  font-size: 0.34rem;\n  color: #1f1c17;\n  margin-top: 0.5rem;\n}\n.container .wx_main .ios_down img[data-v-0ac1f4eb] {\n  width: 0.34rem;\n  height: 0.4rem;\n  margin-left: 0.8rem;\n  margin-right: 0.2rem;\n  vertical-align: middle;\n}\n.container .wx_main .ios_down p[data-v-0ac1f4eb] {\n  line-height: 0.48rem;\n  display: inline;\n  vertical-align: middle;\n}\n.container .wx_main .ios_xr[data-v-0ac1f4eb] {\n  width: 46%;\n  margin: 0 27%;\n  height: 0.82rem;\n  border: 1px solid #e8c490;\n  border-radius: 0.1rem;\n  line-height: 0.82rem;\n  color: #e8c490;\n  font-size: 0.3rem;\n}\n.container .wx_main p[data-v-0ac1f4eb] {\n  color: #eeb260;\n  font-size: 0.2rem;\n  text-align: center;\n  margin-top: 0.2rem;\n}\n.container .detail_bottom[data-v-0ac1f4eb] {\n  width: 100%;\n}\n.container .detail_bottom .title[data-v-0ac1f4eb] {\n  width: 92%;\n  margin: 0.5rem 4% 0.4rem;\n  font-size: 0.3rem;\n  color: #51525d;\n}\n.container .detail_bottom .title img[data-v-0ac1f4eb] {\n  width: 0.6rem;\n  height: 0.6rem;\n}\n.container .detail_bottom .main[data-v-0ac1f4eb] {\n  width: 92%;\n  margin: 0 4% ;\n}\n.container .detail_bottom .main .list1[data-v-0ac1f4eb] {\n  width: 35%;\n  text-align: center;\n  float: left;\n}\n.container .detail_bottom .main .list1 img[data-v-0ac1f4eb] {\n  width: 0.65rem;\n  height: 0.6rem;\n  margin-top: -1.6rem;\n}\n.container .detail_bottom .main .list1 p[data-v-0ac1f4eb] {\n  font-size: 0.28rem;\n  color: #51525d;\n  margin-top: -1.1rem;\n}\n.container .detail_bottom .main .list1 span[data-v-0ac1f4eb] {\n  font-size: 0.24rem;\n  color: #51525d;\n  float: left ;\n  margin-left: 0.4rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1622:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/zfy_downBg.png?v=d7ecf2ac";

/***/ }),

/***/ 1740:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    attrs: {
      "id": "container"
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0ac1f4eb", module.exports)
  }
}

/***/ }),

/***/ 1874:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1458);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("da0b1c28", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ac1f4eb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyDown.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ac1f4eb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./zfyDown.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 613:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1874)

var Component = __webpack_require__(2)(
  /* script */
  null,
  /* template */
  __webpack_require__(1740),
  /* scopeId */
  "data-v-0ac1f4eb",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\regZfy\\zfyDown.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] zfyDown.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0ac1f4eb", Component.options)
  } else {
    hotAPI.reload("data-v-0ac1f4eb", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});