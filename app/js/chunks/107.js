webpackJsonp([107],{

/***/ 1385:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            detail: []
        };
    },
    mounted: function mounted() {
        common.youmeng("政策解读", "政策详情" + this.$route.query.title);
        this.getDetail();
    },

    methods: {
        getDetail: function getDetail() {
            var _this = this;

            var id = this.$route.query.id;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "policy/policyDetail",
                data: {
                    id: id
                },
                showLoading: false
            }, function (data) {
                _this.detail = data.data;
            });
        }
    }
};

/***/ }),

/***/ 1578:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.contain {\n  margin-bottom: 0.5rem;\n}\n.contain .detail_main {\n  width: 92%;\n  margin: 0 4%;\n  overflow: hidden;\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n}\n.contain .detail_main h1 {\n  font-size: 0.4rem;\n  color: #3D4A5B;\n  margin: 0.32rem 0 0;\n}\n.contain .detail_main .time {\n  opacity: 0.4;\n  font-family: PingFangSC-Regular;\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  letter-spacing: 0.14px;\n  text-align: center;\n  margin: 0.32rem 0 .46rem;\n}\n.contain .detail_main .line {\n  height: 1px;\n  width: 100%;\n  background: #2C2628;\n  overflow: hidden;\n  opacity: 0.1;\n  margin-bottom: 0.46rem;\n}\n.contain .detail_main .content .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content p {\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  line-height: 0.4rem;\n}\n.contain .detail_main .content p .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content p .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content p .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content p .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content p .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content img {\n  width: 100%;\n  height: auto;\n  margin: 0.48rem 0 0;\n}\n.contain .detail_main .content h2 {\n  font-family: PingFangSC-Medium;\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  margin: 0.48rem 0 0.16rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1859:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('div', {
    staticClass: "detail_main"
  }, [_c('h1', [_vm._v(_vm._s(_vm.detail.title))]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_vm._v("\n            " + _vm._s(_vm.detail.date) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "line"
  }), _vm._v(" "), _c('div', {
    staticClass: "detail",
    domProps: {
      "innerHTML": _vm._s(_vm.detail.content)
    }
  }, [_vm._v("\n           " + _vm._s(_vm.detail.content) + "\n        ")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-d8d92704", module.exports)
  }
}

/***/ }),

/***/ 1994:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1578);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("4e3bf2c9", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d8d92704!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./policyDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d8d92704!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./policyDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 601:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1994)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1385),
  /* template */
  __webpack_require__(1859),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\policy\\policyDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] policyDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d8d92704", Component.options)
  } else {
    hotAPI.reload("data-v-d8d92704", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});