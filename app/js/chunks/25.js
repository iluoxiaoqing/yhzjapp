webpackJsonp([25],{

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(601)

var Component = __webpack_require__(11)(
  /* script */
  __webpack_require__(463),
  /* template */
  __webpack_require__(560),
  /* scopeId */
  "data-v-5c27652d",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\register\\register.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] register.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5c27652d", Component.options)
  } else {
    hotAPI.reload("data-v-5c27652d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 463:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _base = __webpack_require__(129);

var _base2 = _interopRequireDefault(_base);

var _common = __webpack_require__(18);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            butnTitle: "获取验证码",
            imgUrl: '',
            vCode: "",
            phoneNo: "",
            code: "",
            password: "",
            imgKey: "",
            timerNum: 60,
            clock: '',
            isNotClick: false,
            isAcitive: false,
            inviteName: ''
        };
    },

    watch: {
        timerNum: function timerNum(newValue) {
            var _this = this;
            if (newValue > 0) {
                _this.butnTitle = newValue + 's';
            } else {
                clearInterval(this.clock);
                _this.isAcitive = false;
                _this.isNotClick = false;
                _this.butnTitle = '获取验证码';
            }
        }
    },
    mounted: function mounted() {
        this.getName();
    },

    methods: {
        jumpUser: function jumpUser() {
            window.location.href = _apiConfig2.default.WEB_URL + 'userAgreement';
        },
        jumpZheng: function jumpZheng() {
            window.location.href = _apiConfig2.default.WEB_URL + 'privacyPolicy';
        },
        getName: function getName() {
            var _this = this;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "expansion/findUserName",
                "data": {
                    "sourceUser": this.$route.query.sourceUser
                    // "sourceUser": "95191739" //测试用
                },
                showLoading: true
            }, function (data) {
                if (data.code === "0000") {
                    _this.inviteName = data.data;
                }
            });
        },
        downloadUser: function downloadUser() {
            window.location.href = _apiConfig2.default.WEB_URL + 'wxDown';
        },

        //发送验证码
        requestVeriCode: function requestVeriCode() {
            var _this2 = this;

            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "user/sendCheckCode",
                data: {
                    'phoneNo': this.phoneNo,
                    'businessType': 'SIGNUP',
                    'appCode': 'YHZJ',
                    'version': '',
                    'osType': common.isClient()
                },
                showLoading: true
            }, function (data) {
                if (data.code === "0000") {
                    common.toast({
                        content: "验证码已经发送"
                    });
                    _this2.sendVericdoe();
                } else if (data.code === "0001") {
                    _this2.sendVericdoe();
                    common.toast({
                        content: data.msg
                    });
                }
            });
        },
        sendVericdoe: function sendVericdoe() {
            var _this = this;
            _this.timerNum = 60;
            _this.butnTitle = this.timerNum + 's';
            _this.isAcitive = true;
            _this.isNotClick = true;
            _this.clock = setInterval(function () {
                _this.timerNum--;
            }, 1000);
        },
        openUser: function openUser() {
            var _this3 = this;

            // 验证手机号是否正确
            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            // 验证密码
            var veriReg = _base2.default.password3.reg;
            if (!veriReg.test(this.password)) {
                var veriErrorMsg = _base2.default.password3.error;
                common.toast({
                    content: veriErrorMsg
                });
                return;
            }
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "user/signUp",
                data: {
                    phoneNo: this.phoneNo,
                    // parentUserNo: '95191739',
                    parentUserNo: this.$route.query.sourceUser,
                    password: this.password,
                    checkCode: this.code,
                    appCode: 'YHZJ'
                },
                showLoading: true
            }, function (data) {
                if (data.code === '0000') {
                    common.toast({
                        content: '注册成功'
                    });
                    _this3.$router.push({
                        "path": "wxDown"
                    });
                }
            });
        }
    }
};

/***/ }),

/***/ 494:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-5c27652d] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-5c27652d] {\n  background: #fff;\n}\n.tips_success[data-v-5c27652d] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-5c27652d] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-5c27652d] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-5c27652d],\n.fade-leave-active[data-v-5c27652d] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-5c27652d],\n.fade-leave-to[data-v-5c27652d] {\n  opacity: 0;\n}\n.default_button[data-v-5c27652d] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-5c27652d] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-5c27652d] {\n  position: relative;\n}\n.loading-tips[data-v-5c27652d] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.logo[data-v-5c27652d] {\n  width: 1.56rem;\n  height: 1.56rem;\n  border-radius: 50%;\n  margin: .48rem auto .3rem;\n  display: block;\n  position: fixed;\n  left: 50%;\n  top: -0.5rem;\n  z-index: 13;\n  -webkit-transform: translate3d(-50%, 0%, 0);\n          transform: translate3d(-50%, 0%, 0);\n}\n[data-v-5c27652d]::-webkit-input-placeholder {\n  /* WebKit browsers */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-5c27652d]::-moz-placeholder {\n  /* Mozilla Firefox 19+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-5c27652d]:-ms-input-placeholder {\n  /* Internet Explorer 10+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n.van-popup[data-v-5c27652d] {\n  width: 87%;\n}\n.van-cell[data-v-5c27652d] {\n  background: #F6F6F6;\n  border-radius: 30px;\n  margin-top: .2rem;\n}\n.userHome[data-v-5c27652d] {\n  width: 100%;\n  overflow: hidden;\n  /* .creditCard_bank{\n     width: 6.94rem;\n     margin: 0 auto;\n     background: #ffffff;\n     box-shadow: 0px 1px 11px 0px rgba(6, 0, 1, 0.2);\n     border-radius: 0.22rem !important;\n     .bankList{\n         width: 100%;\n         display: flex;\n         flex-wrap: wrap;\n         justify-content: flex-start;\n         border-bottom: 1px solid #FF9900 !important;\n         // flex-direction:row;\n         .item{\n             width:1.89rem;\n             min-height: 2.7-0.4rem;\n             position: relative;\n             // background: #FFFFFF;\n             border-right: 1px solid #E8E8E8;\n             // border-top: 1px solid #E8E8E8;\n             border-bottom: 1px solid #E8E8E8;\n             border-collapse: collapse;!* 合并相邻边框*!\n             // box-shadow: 0 0 0.32rem 0 rgba(63,131,255,0.15);\n             border-radius: 0.08rem;\n             padding: 0.3rem 0.2rem 0;\n             // margin-bottom: 0.54rem;\n             border-radius:0rem;\n             // padding-top: 0.4rem;\n             &.height0{\n                 height: 0px;\n                 visibility: hidden;\n             }\n             &:nth-of-type(3){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(6){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(9){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(12){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(15){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(18){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(21){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(24){\n                 border-right: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(1){\n                 border-top: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(2){\n                 border-top: 0px solid #E8E8E8 !important;\n             }\n             &:nth-of-type(3){\n                 border-top: 0px solid #E8E8E8 !important;\n             }\n             .logo{\n                 width: 0.76rem;\n                 height: 0.76rem;\n                 // margin-left: 0.42rem;\n                 position: absolute;\n                 transform: translateX(-50%);\n                 left: 50%;\n                 // top: 0.2rem;\n                 img{\n                     display: block;\n                     width: 100%;\n                     height: 100%;\n                 }\n             }\n             b{\n                 color: #3D4A5B;\n                 opacity: 0.9;\n                 font-size: 0.30rem;\n                 display: block;\n                 text-align: center;\n                 margin-top: 0.8rem;\n                 margin-bottom: 0.1rem;\n             }\n             p{\n                 color: #3D4A5B;\n                 opacity: 0.5;\n                 font-size: 0.26rem;\n                 text-align: center;\n                 line-height: 0.36rem;\n                 .border-bottom(#E0E0E0);\n                 padding-bottom: 0.18rem;\n                 margin-bottom: 0.2rem;\n                 height: 0.70rem;\n             }\n             span{\n                 display: block;\n                 color: #FF9900;\n                 font-size: 0.2rem;\n                 text-align: center;\n                 // font-weight: 700;\n                 margin-bottom: 0.1rem;\n             }\n         }\n         .forward{\n             width:2.20rem;\n             height: 2.70rem;\n             display: flex;\n             flex-direction:column;\n             justify-content: center;\n             align-items: center;\n             background: #FFFFFF;\n             box-shadow: 0 0 0.32rem 0 rgba(63,131,255,0.15);\n             border-radius: 0.08rem;\n             margin-bottom: 0.54rem;\n             p{\n                 width: 100%;\n                 font-weight: 700;\n                 font-size: 0.3rem;\n                 color: #3D4A5B;\n                 opacity: 0.3;\n                 text-align: center;\n             }\n         }\n     }\n }*/\n  /*.creditCard_btn{\n      width: 92%;\n      height: 1.58rem;\n      position: fixed;\n      left: 0;\n      bottom: 0;\n      background-image: linear-gradient(0deg, #FFFFFF 0%, rgba(255,255,255,0) 100%);\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      padding: 0 4%;\n  }*/\n}\n.userHome .userLogin_container[data-v-5c27652d] {\n  width: 6.5rem;\n  height: 8.5rem;\n  margin-left: .65rem;\n}\n.userHome .userLogin_container .userLogin_wrap[data-v-5c27652d] {\n  width: 6.2rem;\n  height: 7.22rem;\n}\n.userHome .userLogin_container .userLogin_wrap .close[data-v-5c27652d] {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n.userHome .userLogin_container .userLogin_wrap .close i[data-v-5c27652d] {\n  margin: .2rem;\n  display: block;\n  width: .28rem;\n  height: .28rem;\n  background-size: cover;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner[data-v-5c27652d] {\n  font-size: .34rem;\n  padding: .3rem .34rem;\n  position: relative;\n  background: #FFFFFF;\n  box-shadow: 0px 0px 20px 0px rgba(102, 102, 102, 0.2);\n  border-radius: 0.2rem;\n  margin: .5rem 0 .5rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner h4[data-v-5c27652d] {\n  text-align: center;\n  font-size: .36rem;\n  color: #313757;\n  letter-spacing: 0;\n  width: 2.88rem;\n  margin: 1.2rem auto 0;\n  line-height: .7rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form[data-v-5c27652d] {\n  margin: .12rem 0 .4rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form .userForm_group[data-v-5c27652d] {\n  font-size: 0.187rem;\n  font-weight: 400;\n  color: #333333;\n  padding-bottom: .26rem;\n  padding-left: 0.39rem;\n  border-bottom: 1px solid #ECF0FF;\n  position: relative;\n  background: #F4F4F4;\n  border-radius: 0.55rem;\n  margin-top: .4rem;\n  height: 0.78rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form .userForm_group input[data-v-5c27652d]::-webkit-input-placeholder {\n  font-size: .3rem;\n  font-weight: 400;\n  color: #30303D;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form .userForm_group .get_password[data-v-5c27652d] {\n  position: absolute;\n  top: 25%;\n  right: 10%;\n  font-size: 0.30rem;\n  color: #f3782c;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form .userForm_group input[data-v-5c27652d] {\n  width: 100%;\n  color: #313757;\n  margin-top: .28rem;\n  font-size: .34rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_form .userForm_group span[data-v-5c27652d] {\n  color: #F3782C;\n  letter-spacing: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  top: 0;\n  right: 0;\n  font-size: .32rem;\n  height: 100%;\n  line-height: 100%;\n  margin-right: .2rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_btn[data-v-5c27652d] {\n  background: #F3782C;\n  border-radius: 0.55rem;\n  display: block;\n  height: 0.9rem;\n  line-height: .9rem;\n  font-size: .32rem;\n  color: #FFF;\n  letter-spacing: 0;\n  text-align: center;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_agreement[data-v-5c27652d] {\n  font-size: .24rem;\n  margin-top: 0.3rem;\n  text-align: center;\n  color: #333333;\n  width: 5.7rem;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_agreement span[data-v-5c27652d] {\n  color: #F3782C;\n  display: inline-block;\n}\n.userHome .userLogin_container .userLogin_wrap .userLogin_wrap_inner .userLogin_agreement a[data-v-5c27652d] {\n  color: #F3782C;\n  position: relative;\n  left: -0.06rem;\n}\n.userHome .userLogin_container .userLogin_wrap .download[data-v-5c27652d] {\n  color: #333333;\n  margin-top: .28rem;\n  font-size: 0.28rem;\n  text-align: center;\n}\n.userHome .userLogin_container .userLogin_wrap .download img[data-v-5c27652d] {\n  width: 0.28rem;\n  height: 0.38rem;\n  margin-right: 0.1rem;\n}\n.userHome .banner[data-v-5c27652d] {\n  text-align: center;\n  margin-top: 0.5rem;\n}\n.userHome .banner img[data-v-5c27652d] {\n  width: 2.333rem;\n}\n.userHome .tuijian[data-v-5c27652d] {\n  margin: 0.2rem 0.9rem;\n  height: auto;\n}\n.userHome .tuijian .tuijian-top[data-v-5c27652d] {\n  min-height: 0.7rem;\n  border: 1px solid #F3782C;\n  border-radius: 20px;\n  background: #f3782c;\n  font-family: 'PingFangSC-Regular';\n  font-weight: 400;\n  font-size: .3rem;\n  text-shadow: 0 0.027rem 0.053rem rgba(0, 0, 0, 0.1);\n  padding: 0 0.7rem;\n  color: #ffffff;\n  text-align: center;\n  line-height: .65rem;\n}\n.userHome .tuijian .tuijian-top .test[data-v-5c27652d] {\n  font-size: .15rem;\n}\n.userHome .tuijian .tuijian-top1[data-v-5c27652d] {\n  padding: 0 0.3rem !important;\n}\n.userHome .model[data-v-5c27652d] {\n  padding: .35rem;\n}\n.userHome .model .tip[data-v-5c27652d] {\n  font-weight: 400;\n  color: #FE3232;\n  font-size: .12rem;\n  padding: .2rem 0;\n}\n.userHome .model .title-text[data-v-5c27652d] {\n  position: relative;\n  height: .6rem;\n}\n.userHome .model .title-text .close[data-v-5c27652d] {\n  position: absolute;\n  right: .2rem;\n  font-size: .3rem;\n}\n.userHome .model .title-text .title[data-v-5c27652d] {\n  position: absolute;\n  font-weight: 400;\n  color: #333333;\n  font-size: .3rem;\n}\n.userHome .model .btn[data-v-5c27652d] {\n  text-align: center;\n}\n.userHome .model .btn button[data-v-5c27652d] {\n  width: 3rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/down@3x.png?v=8f04d017";

/***/ }),

/***/ 526:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo@3x.png?v=9c07e6cd";

/***/ }),

/***/ 560:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "userHome"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "tuijian"
  }, [_c('div', {
    staticClass: "tuijian-top",
    class: _vm.inviteName.length > 4 ? 'tuijian-top1' : ''
  }, [_vm._v("\n           " + _vm._s(_vm.inviteName || 'XX') + "邀请您加入用户之家\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "userLogin_container"
  }, [_c('div', {
    staticClass: "userLogin_wrap"
  }, [_c('div', {
    staticClass: "userLogin_wrap_inner"
  }, [_c('div', {
    staticClass: "userLogin_form"
  }, [_c('section', {
    staticClass: "userForm_group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phoneNo),
      expression: "phoneNo"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "手机号"
    },
    domProps: {
      "value": (_vm.phoneNo)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phoneNo = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('section', {
    staticClass: "userForm_group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.code),
      expression: "code"
    }],
    attrs: {
      "type": "tel",
      "placeholder": "验证码",
      "maxlength": "6"
    },
    domProps: {
      "value": (_vm.code)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.code = $event.target.value
      }
    }
  }), _vm._v(" "), _c('button', {
    staticClass: "get_password",
    attrs: {
      "disabled": _vm.butnTitle !== '获取验证码'
    },
    on: {
      "click": _vm.requestVeriCode
    }
  }, [_vm._v(_vm._s(_vm.butnTitle))])]), _vm._v(" "), _c('section', {
    staticClass: "userForm_group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    attrs: {
      "placeholder": "密码",
      "type": "password",
      "maxlength": "18"
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('span', {
    staticClass: "userLogin_btn",
    on: {
      "click": function($event) {
        return _vm.openUser('sourceUser')
      }
    }
  }, [_vm._v("马上加入")]), _vm._v(" "), _c('p', {
    staticClass: "userLogin_agreement"
  }, [_vm._v("\n                    *注册代表已同意"), _c('span', {
    on: {
      "click": _vm.jumpUser
    }
  }, [_vm._v("《用户协议》")]), _vm._v("与"), _c('span', {
    attrs: {
      "target": "_blank"
    },
    on: {
      "click": _vm.jumpZheng
    }
  }, [_vm._v("《隐私保护政策》")])])]), _vm._v(" "), _c('div', {
    staticClass: "download",
    on: {
      "click": _vm.downloadUser
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(524),
      "alt": ""
    }
  }), _vm._v("下载用户之家\n            ")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(526),
      "alt": ""
    }
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5c27652d", module.exports)
  }
}

/***/ }),

/***/ 601:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(494);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(13)("7c414c02", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5c27652d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./register.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5c27652d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./register.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});