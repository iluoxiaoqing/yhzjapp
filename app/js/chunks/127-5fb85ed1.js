webpackJsonp([127],{

/***/ 1398:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            currentPage: 1,
            showLoading: false,
            mySelf: {},
            isNoData: false,
            rankList: []
            // rankList: [{ bonusNum: "尚未获得", name: "齐术炎" }, { bonusNum: "尚未获得", name: "齐术炎" }, { bonusNum: "尚未获得", name: "齐术炎" }],
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        this.getHeight();
        this.getList();
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        getList: function getList() {
            var _this = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "bonus/bonusDetail",
                data: {
                    type: this.$route.query.type,
                    pageNo: this.currentPage
                },
                showLoading: true
            }, function (data) {
                console.log(data);
                _this.showLoading = false;
                _this.mySelf = data.data.mySelf;
                if (data.data.rankList.length > 0) {
                    if (_this.currentPage == 1) {
                        _this.rankList = [];
                    }
                    data.data.rankList.map(function (el) {
                        _this.$set(el, "show", true);
                        _this.rankList.push(el);
                    });

                    _this.isNoData = false;
                    _this.currentPage++;
                } else {
                    console.log(_this.currentPage);
                    if (_this.currentPage == 1) {
                        _this.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
                // this.rankList = data.data.rankList;
            });
        },
        refresh: function refresh(loaded) {
            this.currentPage = 1;
            this.getList();
        },
        loadmore: function loadmore(loaded) {
            this.getList();
        }
    }
};

/***/ }),

/***/ 1612:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.fhList[data-v-b971cca6] {\n  width: 100%;\n  background: #F7F8FA;\n}\n.fhList .top[data-v-b971cca6] {\n  width: 92%;\n  margin: 0.3rem 4% 0.1rem;\n  height: 1.6rem;\n  line-height: 1.6rem;\n  background: #E2EDFF;\n}\n.fhList .top .left[data-v-b971cca6] {\n  color: #3F83FF;\n  font-size: .32rem;\n  float: left;\n  text-align: left;\n  padding-left: 0.6rem;\n}\n.fhList .top .right[data-v-b971cca6] {\n  color: #3F83FF;\n  font-size: .28rem;\n  float: right;\n  text-align: right;\n  padding-right: 0.6rem;\n}\n.fhList .bottom_content .bottom[data-v-b971cca6] {\n  width: 92%;\n  margin: 0 4%;\n  height: 1.6rem;\n  line-height: 1.6rem;\n  background: #ffffff;\n  border-bottom: 1px solid #F7F8FA;\n}\n.fhList .bottom_content .bottom .left[data-v-b971cca6] {\n  color: #333333;\n  font-size: .32rem;\n  float: left;\n  text-align: left;\n  padding-left: 0.6rem;\n}\n.fhList .bottom_content .bottom .right[data-v-b971cca6] {\n  color: #3F83FF;\n  font-size: .28rem;\n  float: right;\n  text-align: right;\n  padding-right: 0.6rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1925:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "fhList"
  }, [_c('div', {
    staticClass: "top"
  }, [_c('div', {
    staticClass: "left"
  }, [_vm._v(_vm._s(_vm.mySelf.name))]), _vm._v(" "), _c('div', {
    staticClass: "right"
  }, [_vm._v(_vm._s(_vm.mySelf.bonusNum == '尚未获得' ? _vm.mySelf.bonusNum : _vm.mySelf.bonusNum + '元'))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom_content wrapper",
    staticStyle: {
      "overflow": "scroll"
    }
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, _vm._l((_vm.rankList), function(el, i) {
    return _c('div', {
      key: i,
      staticClass: "bottom"
    }, [_c('div', {
      staticClass: "left"
    }, [_vm._v(_vm._s(el.realName))]), _vm._v(" "), _c('div', {
      staticClass: "right"
    }, [_vm._v(_vm._s(el.detailNum == '尚未获得' ? el.detailNum : el.detailNum + '元'))])])
  }), 0)], 1), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.isNoData
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-b971cca6", module.exports)
  }
}

/***/ }),

/***/ 2073:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1612);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("a0682346", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b971cca6&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-b971cca6&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./fhList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 594:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2073)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1398),
  /* template */
  __webpack_require__(1925),
  /* scopeId */
  "data-v-b971cca6",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\fh\\fhList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] fhList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b971cca6", Component.options)
  } else {
    hotAPI.reload("data-v-b971cca6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});