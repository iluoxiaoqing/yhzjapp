webpackJsonp([54],{

/***/ 1452:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0,
            listData: ""
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '嘉联电签');
        this.getList();
    },

    methods: {
        go: function go() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "jl/showDQPopMessage",
                "data": {}
            }, function (data) {
                _this.listData = data.data;
            });
        }
    }
};

/***/ }),

/***/ 1565:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-53244702] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-53244702] {\n  background: #fff;\n}\n.tips_success[data-v-53244702] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-53244702] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-53244702] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-53244702],\n.fade-leave-active[data-v-53244702] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-53244702],\n.fade-leave-to[data-v-53244702] {\n  opacity: 0;\n}\n.default_button[data-v-53244702] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-53244702] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-53244702] {\n  position: relative;\n}\n.loading-tips[data-v-53244702] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.jialian[data-v-53244702] {\n  width: 100%;\n}\n.jialian .jlTop[data-v-53244702] {\n  width: 100%;\n  height: 9.46rem;\n  background: -webkit-linear-gradient(#2564ff, #5aa0ff);\n  background: linear-gradient(#2564ff, #5aa0ff);\n  display: inline-block;\n  float: left;\n}\n.jialian .jlTop .logo[data-v-53244702] {\n  width: 100%;\n  height: 0.98rem;\n  text-align: center;\n  position: relative;\n}\n.jialian .jlTop .logo img[data-v-53244702] {\n  width: 1.86rem;\n  height: 0.44rem;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-top: -0.22rem;\n  margin-left: -0.93rem;\n}\n.jialian .jlTop .title[data-v-53244702] {\n  width: 100%;\n  height: 1.12rem;\n  position: relative;\n}\n.jialian .jlTop .title img[data-v-53244702] {\n  width: 5.7rem;\n  height: 0.66rem;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-top: -0.33rem;\n  margin-left: -2.85rem;\n}\n.jialian .jlTop .mainBg img[data-v-53244702] {\n  width: 100%;\n}\n.jialian .jlBottom[data-v-53244702] {\n  width: 100%;\n  background: #5aa0ff;\n  display: inline-block;\n  float: left;\n}\n.jialian .jlBottom .contantMain[data-v-53244702] {\n  width: 100%;\n}\n.jialian .jlBottom .contantMain .contant_title[data-v-53244702] {\n  width: 5.42rem;\n  height: 0.87rem;\n  background: url(" + __webpack_require__(1725) + ") no-repeat;\n  position: relative;\n  top: 50%;\n  left: 50%;\n  margin-top: -0.43rem;\n  margin-left: -2.71rem;\n  font-size: .32rem;\n  background-size: 100% 100%;\n  font-weight: bold;\n  color: #ffffff;\n  text-align: center;\n  line-height: 0.8rem;\n}\n.jialian .jlBottom .contantMain .contant_bottom[data-v-53244702] {\n  width: 92%;\n  margin: 0 4%;\n  background: #e7eefe;\n  border-radius: 0.36rem;\n  margin-top: -0.5rem;\n  display: inline-block;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact[data-v-53244702] {\n  width: 100%;\n  display: inline-block;\n  float: left;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact .contact_left[data-v-53244702] {\n  width: 2.1rem;\n  height: 0.52rem;\n  background: #5298ff;\n  border-radius: 0px 26px 26px 0px;\n  font-size: 0.3rem;\n  font-weight: bold;\n  color: #ffffff;\n  line-height: 0.52rem;\n  text-align: center;\n  margin-top: 0.5rem;\n  display: inline-block;\n  float: left;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact .contact_right[data-v-53244702] {\n  width: 65%;\n  font-size: .28rem;\n  font-weight: 500;\n  color: #2e548e;\n  line-height: 0.52rem;\n  margin-top: 0.5rem;\n  display: inline-block;\n  float: right;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact .contact_right p[data-v-53244702] {\n  font-size: .28rem;\n  font-weight: 500;\n  color: #2e548e;\n  line-height: 0.52rem;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact[data-v-53244702]:nth-of-type(1) {\n  margin-top: 0.5rem;\n}\n.jialian .jlBottom .contantMain .contant_bottom .contact[data-v-53244702]:last-child {\n  margin-bottom: 0.5rem;\n}\n.jialian .jlBottom .contantMain[data-v-53244702]:last-child {\n  margin-top: 1rem;\n}\n.jialian .jlBottom .button[data-v-53244702] {\n  width: 92%;\n  height: 0.95rem;\n  margin: 0.9rem 4% 1.58rem;\n  background: url(" + __webpack_require__(1721) + ") no-repeat;\n  background-size: 100% 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ 1721:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/jianlianbutton.png?v=7ac2332d";

/***/ }),

/***/ 1722:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/jianlianindex.png?v=e46c2a34";

/***/ }),

/***/ 1723:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/jianlianlogo.png?v=4d9f4979";

/***/ }),

/***/ 1724:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/jianliantitle.png?v=e9604337";

/***/ }),

/***/ 1725:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/titleBg.png?v=35306a61";

/***/ }),

/***/ 1878:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "jialian"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "jlBottom"
  }, [_c('div', {
    staticClass: "contantMain"
  }, [_c('div', {
    staticClass: "contant_title"
  }, [_vm._v("\r\n                                —— 产品介绍 ——\r\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.paymentCompany) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                支付公司\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.paymentCompany) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.cardRate) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                刷卡费率\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.cardRate), function(item) {
    return _c('p', [_vm._v(_vm._s(item))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.commRule) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                分润规则\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.commRule) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.productFeatures) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                产品特点\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.productFeatures) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.settleRule) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                结算规则\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.settleRule) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "contantMain"
  }, [_c('div', {
    staticClass: "contant_title"
  }, [_vm._v("\r\n                                —— 活动规则 ——\r\n                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.posPrice) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                机具采购价格\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.posPrice) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.cashBackOfPurchase) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                采购返现\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.cashBackOfPurchase) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.actRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                激活返现\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.actRecurrence), function(i) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.transRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                交易返现\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.listData.transRecurrence))])])]) : _vm._e(), _vm._v(" "), (_vm.listData.actSubsidy) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                激活补贴\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.actSubsidy) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.actSubsidyExplain) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                激活补贴说明\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.actSubsidyExplain) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.rewardStandard) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                达标奖励\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.rewardStandard) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.rewardNew) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                拉新奖励\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                               " + _vm._s(_vm.listData.rewardNew) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.platformState) ? _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "contact_left"
  }, [_vm._v("\r\n                                                平台申明\r\n                                        ")]), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.platformState) + "\r\n                                        ")])]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "button",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  })])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "jlTop"
  }, [_c('div', {
    staticClass: "logo"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1723),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1724),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "mainBg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1722),
      "alt": ""
    }
  })])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-53244702", module.exports)
  }
}

/***/ }),

/***/ 2026:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1565);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("fb3c3338", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-53244702&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./jialianIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-53244702&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./jialianIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 657:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2026)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1452),
  /* template */
  __webpack_require__(1878),
  /* scopeId */
  "data-v-53244702",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\jialianIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] jialianIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-53244702", Component.options)
  } else {
    hotAPI.reload("data-v-53244702", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});