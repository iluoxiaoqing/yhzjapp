webpackJsonp([25],{

/***/ 1283:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_success.png?v=d5c38576";

/***/ }),

/***/ 1296:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_popwindow_close.png?v=3af1fd99";

/***/ }),

/***/ 1318:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_payment_failed.png?v=011459d2";

/***/ }),

/***/ 1473:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            recommOrderArray: [],
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            goodsName: "",
            goodsPrice: "",
            needAddress: "",
            productCode: "",
            paySuccess: false,
            payFail: false,
            fishSuccess: false,
            qhyyRegisterUrl: ""

        };
    },
    mounted: function mounted() {
        var _this2 = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "recommOrder/goods"
        }, function (data) {

            console.log(data.data);
            _this2.recommOrderArray = data.data;
            _this2.goodsName = data.data[0].name;
            _this2.goodsPrice = data.data[0].price;
            _this2.productCode = data.data[0].productCode;
            _this2.needAddress = data.data[0].needAddress;
            var _this = _this2;

            _this2.$nextTick(function () {
                _this2.mySwiper = new Swiper('.swiper-container', {
                    effect: 'coverflow', //3d滑动
                    //grabCursor: true,
                    centeredSlides: true,
                    initialSlide: _this2.currentIndex,
                    loop: false,
                    slidesPerView: 2,
                    spaceBetween: 15,
                    coverflow: {
                        rotate: 0, //设置为0
                        stretch: 0,
                        depth: 0,
                        modifier: 2,
                        slideShadows: false
                    },
                    onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                        var index = swiper.realIndex;
                        var json = _this.recommOrderArray[index];
                        console.log(json);
                        _this.chooseGoods(json);
                    }
                });
            });
        });

        common.youmeng("升级推手", "进入升级推手");

        window.alipayResult = this.alipayResult;
    },

    methods: {
        payAgain: function payAgain() {
            common.youmeng("升级推手", "点击重新付款手");
            this.hideMask();
            this.gotoPay();
        },
        hideMask: function hideMask() {
            this.paySuccess = false;
            this.payFail = false;
            this.fishSuccess = false;
        },
        alipayResult: function alipayResult(state) {

            if (state == "true" || state == 1 || state == true || state == '1') {

                if (this.productCode == "GIFT_B") {
                    this.paySuccess = false;
                    this.fishSuccess = true;
                } else {
                    this.paySuccess = true;
                    this.fishSuccess = false;
                }

                common.youmeng("升级推手", this.productCode + "支付成功");
            } else {
                common.youmeng("升级推手", this.productCode + "支付失败");
                this.payFail = true;
            }
        },
        gotoHome: function gotoHome() {

            this.paySuccess = false;
            this.payFail = false;
            this.fishSuccess = false;

            var openPath = {
                "path": "xytsapp/kpi/mylevel",
                "isNeedClosePage": "true"
            };

            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeOpenPage.postMessage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            } else {
                window.android.nativeOpenPage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            }
        },
        gotoFish: function gotoFish() {
            window.location.href = this.qhyyRegisterUrl;
        },
        chooseGoods: function chooseGoods(i) {
            this.goodsName = i.name;
            this.goodsPrice = i.price;
            this.needAddress = i.needAddress;
            this.productCode = i.productCode;
        },
        gotoPay: function gotoPay() {
            var _this3 = this;

            common.youmeng("升级推手", "选择" + this.productCode);

            if (this.needAddress) {
                this.$router.push({
                    path: "upgradeRight_pay",
                    query: {
                        "productCode": this.productCode,
                        "goodsPrice": this.goodsPrice
                    }
                });
            } else {

                common.Ajax({
                    "url": _apiConfig2.default.KY_IP + "recommOrder/purchase",
                    "data": {
                        "productCode": this.productCode,
                        "payAmount": this.goodsPrice
                    }
                }, function (data) {

                    _this3.qhyyRegisterUrl = data.data.qhyyRegisterUrl;
                    // this.alipayResult(1);
                    common.youmeng("升级推手", _this3.productCode + "调用支付宝");

                    var aliPay = {
                        "alipay": data.data.alipay,
                        "redirectFunc": "alipayResult"
                    };

                    if (common.isClient() == "ios") {
                        window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    } else {
                        window.android.nativeAlipay(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    }
                });
            }
        }
    }
};

/***/ }),

/***/ 1586:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-71276fef] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-71276fef] {\n  background: #fff;\n}\n.tips_success[data-v-71276fef] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-71276fef] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-71276fef] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-71276fef],\n.fade-leave-active[data-v-71276fef] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-71276fef],\n.fade-leave-to[data-v-71276fef] {\n  opacity: 0;\n}\n.default_button[data-v-71276fef] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-71276fef] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-71276fef] {\n  position: relative;\n}\n.loading-tips[data-v-71276fef] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.upgradeRight[data-v-71276fef] {\n  width: 100%;\n  overflow: hidden;\n}\n.upgradeRight .shareSwiper[data-v-71276fef] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.upgradeRight .shareSwiper .upgradeRight_title[data-v-71276fef] {\n  width: 90%;\n  margin: 0 auto;\n  color: #333;\n  font-size: 0.34rem;\n  font-weight: 700;\n  background: #fff;\n  padding: 0 5%;\n  padding-top: 0.4rem;\n}\n.upgradeRight .shareSwiper .swiper-container[data-v-71276fef] {\n  width: 172%;\n  height: 100%;\n  position: relative;\n  margin-left: -38.5%;\n  padding: 0.32rem 0;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide[data-v-71276fef] {\n  width: 6.3rem;\n  height: 2.6rem;\n  border-radius: 0.16rem;\n  background: #fff;\n  padding: 0.32rem 0;\n  box-shadow: 0 0 0.24rem 0 rgba(49, 55, 87, 0.13);\n  position: relative;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide.swiper-slide-active[data-v-71276fef]:after {\n  content: \"\";\n  position: absolute;\n  width: 200%;\n  height: 200%;\n  border: 1px solid #FFB740;\n  -webkit-transform: scale(0.5, 0.5);\n      -ms-transform: scale(0.5, 0.5);\n          transform: scale(0.5, 0.5);\n  left: -50%;\n  top: -50%;\n  border-radius: 0.32rem;\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide.swiper-slide-active .active_yes[data-v-71276fef] {\n  width: 0.4rem;\n  height: 0.34rem;\n  background: url(" + __webpack_require__(1769) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  right: 0;\n  bottom: 0;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide.active .active_hot[data-v-71276fef] {\n  width: 0.76rem;\n  height: 0.36rem;\n  background: url(" + __webpack_require__(1766) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  left: 0;\n  top: -0.1rem;\n  z-index: 10;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item[data-v-71276fef] {\n  width: 5.56rem;\n  margin: 0 auto;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_top[data-v-71276fef] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_top h1[data-v-71276fef] {\n  color: #3D4A5B;\n  font-size: 0.36rem;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_top b[data-v-71276fef] {\n  color: #FFB740;\n  font-size: 0.44rem;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_top b i[data-v-71276fef] {\n  font-size: 0.24rem;\n  margin-left: 0.05rem;\n  font-weight: 100;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_body[data-v-71276fef] {\n  width: 100%;\n  overflow: hidden;\n  padding-bottom: 0.32rem;\n  position: relative;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_body p[data-v-71276fef] {\n  color: #3D4A5B;\n  font-size: 0.28rem;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_body[data-v-71276fef]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #ECF0FF;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_body[data-v-71276fef]:last-child:after {\n  content: '';\n  background: none;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_footer[data-v-71276fef] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.3rem;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_footer p[data-v-71276fef] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_footer p span[data-v-71276fef] {\n  color: #3D4A5B;\n  font-size: 0.24rem;\n  display: block;\n}\n.upgradeRight .shareSwiper .swiper-container .swiper-slide .item .item_footer i[data-v-71276fef] {\n  display: block;\n  color: #E95647;\n  text-align: right;\n  font-size: 0.24rem;\n  margin-top: 0.05rem;\n}\n.upgradeRight .upgradeRight_process[data-v-71276fef] {\n  width: 90%;\n  overflow: hidden;\n  margin: 0 auto;\n  margin-top: 0.16rem;\n  background: #fff;\n  padding: 0 5%;\n  padding-bottom: 0.48rem;\n}\n.upgradeRight .upgradeRight_process h1[data-v-71276fef] {\n  color: #3D4A5B;\n  font-size: 0.34rem;\n  display: block;\n  overflow: hidden;\n  padding: 0.48rem 0;\n}\n.upgradeRight .upgradeRight_process p[data-v-71276fef] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.upgradeRight .upgradeRight_process p span[data-v-71276fef] {\n  margin: 0 0.18rem;\n  display: inline-block;\n}\n.upgradeRight .upgradeRight_process p span img[data-v-71276fef] {\n  width: 0.64rem;\n  height: 0.64rem;\n  display: block;\n  margin: 0 auto 0.2rem;\n}\n.upgradeRight .upgradeRight_process p span em[data-v-71276fef] {\n  display: block;\n  text-align: center;\n  font-size: 0.26rem;\n}\n.upgradeRight .upgradeRight_process p i[data-v-71276fef] {\n  font-size: 0.24rem;\n  padding-top: 0.2rem;\n  opacity: 0.2;\n  margin: 0 0.15rem;\n}\n.upgradeRight .upgradeRight_grade[data-v-71276fef] {\n  width: 90%;\n  overflow: hidden;\n  margin: 0 auto;\n  margin-top: 0.16rem;\n  background: #fff;\n  padding: 0 5%;\n  padding-bottom: 0.48rem;\n}\n.upgradeRight .upgradeRight_grade h1[data-v-71276fef] {\n  color: #3D4A5B;\n  font-size: 0.34rem;\n  display: block;\n  overflow: hidden;\n  padding: 0.48rem 0;\n}\n.upgradeRight .upgradeRight_grade p[data-v-71276fef] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  opacity: 0.8;\n  margin-bottom: 0.32rem;\n}\n.upgradeRight .upgradeRight_grade p b[data-v-71276fef] {\n  color: #f8c566;\n  opacity: 1;\n}\n.upgradeRight .upgradeRight_grade a[data-v-71276fef] {\n  width: 6.86rem;\n  height: 1.1rem;\n  background: #3F83FF;\n  display: block;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  border-radius: 0.04rem;\n}\n.upgradeRight .upgradeRight_grade a strong[data-v-71276fef] {\n  color: #fff;\n  font-size: 0.32rem;\n}\n.upgradeRight .upgradeRight_grade a em[data-v-71276fef] {\n  color: #fff;\n  font-size: 0.24rem;\n  opacity: 0.5;\n}\n.upgradeRight .upgradeRight_mask[data-v-71276fef] {\n  width: 100%;\n  height: 100%;\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 200;\n  background: rgba(0, 0, 0, 0.6);\n}\n.upgradeRight .upgradeRight_success[data-v-71276fef] {\n  width: 6.2rem;\n  padding: 0.64rem 0;\n  background: #fff;\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  z-index: 210;\n  top: 30%;\n  border-radius: 0.08rem;\n}\n.upgradeRight .upgradeRight_success em[data-v-71276fef] {\n  width: 0.6rem;\n  height: 0.6rem;\n  background: url(" + __webpack_require__(1296) + ") no-repeat #fff;\n  background-size: contain;\n  display: block;\n  border-radius: 100%;\n  text-align: center;\n  line-height: 0.6rem;\n  font-size: 0.5rem;\n  position: absolute;\n  right: 0;\n  top: -0.86rem;\n}\n.upgradeRight .upgradeRight_success img[data-v-71276fef] {\n  width: 1.6rem;\n  height: 1.6rem;\n  display: block;\n  margin: 0 auto 0.1rem;\n}\n.upgradeRight .upgradeRight_success h1[data-v-71276fef] {\n  display: block;\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  margin-bottom: 0.1rem;\n}\n.upgradeRight .upgradeRight_success p[data-v-71276fef] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  opacity: 0.8;\n  text-align: center;\n  margin-bottom: 0.18rem;\n  display: block;\n}\n.upgradeRight .upgradeRight_success a[data-v-71276fef] {\n  width: 4.92rem;\n  height: 0.8rem;\n  font-weight: 700;\n  color: #fff;\n  font-size: 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #3F83FF;\n  border-radius: 0.06rem;\n  margin: 0.48rem auto 0;\n}\n.upgradeRight .upgradeRight_success i[data-v-71276fef] {\n  width: 5.2rem;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.upgradeRight .upgradeRight_success i strong[data-v-71276fef] {\n  width: 2.24rem;\n  height: 0.8rem;\n  display: block;\n  color: #3F83FF;\n  font-size: 0.32rem;\n  text-align: center;\n  line-height: 0.8rem;\n  border-radius: 0.06rem;\n  position: relative;\n}\n.upgradeRight .upgradeRight_success i strong.active[data-v-71276fef] {\n  color: #fff;\n  background: #3F83FF;\n}\n.upgradeRight .upgradeRight_success i strong.active[data-v-71276fef]:after {\n  content: \"\";\n  border: none;\n}\n.upgradeRight .upgradeRight_success i strong[data-v-71276fef]:after {\n  content: \"\";\n  width: 200%;\n  height: 200%;\n  position: absolute;\n  left: -50%;\n  top: -50%;\n  border: 1px solid #3F83FF;\n  -webkit-transform: scale(0.5);\n      -ms-transform: scale(0.5);\n          transform: scale(0.5);\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  border-radius: 0.06rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1756:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_goumailibao.png?v=f8b3a20d";

/***/ }),

/***/ 1757:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jihuodianpu.png?v=540997c2";

/***/ }),

/***/ 1759:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_kaishishoukuan.png?v=9d36407a";

/***/ }),

/***/ 1766:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_corner_mark.png?v=847f2a69";

/***/ }),

/***/ 1769:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_selected.png?v=8b440dde";

/***/ }),

/***/ 1899:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "upgradeRight"
  }, [_c('div', {
    staticClass: "shareSwiper"
  }, [_c('div', {
    staticClass: "upgradeRight_title"
  }, [_vm._v("\n            选择礼包\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.recommOrderArray), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide",
      class: {
        'active': index == 0
      },
      on: {
        "click": function($event) {
          return _vm.chooseGoods(i)
        }
      }
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "item_top"
    }, [_c('h1', [_vm._v(_vm._s(i.name))]), _vm._v(" "), _c('b', [_vm._v(_vm._s(i.price)), _c('i', [_vm._v("元")])])]), _vm._v(" "), _c('div', {
      staticClass: "item_body"
    }, [_c('p', [_vm._v(_vm._s(i.desc))])]), _vm._v(" "), _c('div', {
      staticClass: "item_footer"
    }, [_c('p', [_c('span', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.commonRemark))])]), _vm._v(" "), _c('i', [_vm._v(_vm._s(i.redRemark))])])]), _vm._v(" "), _c('div', {
      staticClass: "active_hot"
    }), _vm._v(" "), _c('div', {
      staticClass: "active_yes"
    })])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]), _vm._v(" "), (_vm.productCode == 'GIFT_A') ? _c('div', {
    staticClass: "upgradeRight_process"
  }, [_c('h1', [_vm._v("礼包使用流程")]), _vm._v(" "), _vm._m(0)]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "upgradeRight_grade"
  }, [_c('h1', [_vm._v("奖金等级")]), _vm._v(" "), _vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('strong', [_vm._v("升级为推手")]), _vm._v(" "), _c('em', [_vm._v("已选择 " + _vm._s(_vm.goodsName) + " " + _vm._s(_vm.goodsPrice) + "元")])])]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess || _vm.payFail || _vm.fishSuccess) ? _c('div', {
    staticClass: "upgradeRight_mask",
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.payFail) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1318)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("支付失败")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.payAgain()
      }
    }
  }, [_vm._v("重新付款")])]) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1283)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("升级成功")]), _vm._v(" "), _c('p', [_vm._v("您已经成功升级为推手")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoHome()
      }
    }
  }, [_vm._v("马上去赚钱")])]) : _vm._e()]), _vm._v(" "), (_vm.fishSuccess) ? _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1283)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("升级成功")]), _vm._v(" "), _c('p', [_vm._v("您已经成功升级为推手")]), _vm._v(" "), _c('i', [_c('strong', {
    on: {
      "click": function($event) {
        return _vm.gotoHome()
      }
    }
  }, [_vm._v("马上去赚钱")]), _vm._v(" "), _c('strong', {
    staticClass: "active",
    on: {
      "click": function($event) {
        return _vm.gotoFish()
      }
    }
  }, [_vm._v("激活乾海有鱼")])])])]) : _vm._e()], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1756)
    }
  }), _vm._v(" "), _c('em', [_vm._v("1.购买礼包")])]), _vm._v(" "), _c('i', [_vm._v(" ▶ ▶ ")]), _vm._v(" "), _c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1757)
    }
  }), _vm._v(" "), _c('em', [_vm._v("2.激活店铺")])]), _vm._v(" "), _c('i', [_vm._v(" ▶ ▶ ")]), _vm._v(" "), _c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1759)
    }
  }), _vm._v(" "), _c('em', [_vm._v("3.开始收款")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            购买礼包成为"), _c('b', [_vm._v("推手")]), _vm._v("享受"), _c('b', [_vm._v("60%奖金奖励")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            完成30单业务成为"), _c('b', [_vm._v("主管")]), _vm._v("享受"), _c('b', [_vm._v("85%奖金奖励")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            完成80单业务成为"), _c('b', [_vm._v("经理")]), _vm._v("享受"), _c('b', [_vm._v("100%奖金奖励")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-71276fef", module.exports)
  }
}

/***/ }),

/***/ 2047:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1586);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("d15fa15a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-71276fef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-71276fef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 681:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2047)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1473),
  /* template */
  __webpack_require__(1899),
  /* scopeId */
  "data-v-71276fef",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\upgradeRight.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] upgradeRight.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-71276fef", Component.options)
  } else {
    hotAPI.reload("data-v-71276fef", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});