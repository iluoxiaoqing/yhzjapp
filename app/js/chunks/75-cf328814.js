webpackJsonp([75],{

/***/ 1426:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            // currentGroup: 1,
            allRankList: [{
                active: true,
                rankList: []
            }, {
                active: false,
                rankList: []
            }],
            gatherList: [],
            rankList: [],
            userName0: "",
            userName1: "",
            userName2: "",
            userName3: "",
            userName4: "",
            amount0: "",
            amount1: "",
            amount2: "",
            amount3: "",
            amount4: "",
            userLogo0: "",
            userLogo1: "",
            userLogo2: "",
            userLogo3: "",
            userLogo4: "",
            rewardName: "",
            amountTotal: "",
            custTotal: "",
            activeTotal: "",
            transTotal: "",
            activityCode: ""
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getRewardGather();
        this.commonAjax();
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            followFinger: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                var index = swiper.activeIndex;
                // this.actiove = index;
                _this.allRankList.map(function (el) {
                    el.active = false;
                });
                _this.allRankList[index].active = true;
                _this.index = index;
            }
        });
    },

    methods: {
        commonAjax: function commonAjax() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rank",
                "data": {}
            }, function (data) {
                _this2.rankList = data.data;
                _this2.userName0 = data.data[0] ? data.data[0].userName : "";
                _this2.userName1 = data.data[1] ? data.data[1].userName : "";
                _this2.userName2 = data.data[2] ? data.data[2].userName : "";
                _this2.userName3 = data.data[3] ? data.data[3].userName : "";
                _this2.userName4 = data.data[4] ? data.data[4].userName : "";
                _this2.amount0 = data.data[0] ? data.data[0].amount : "";
                _this2.amount1 = data.data[1] ? data.data[1].amount : "";
                _this2.amount2 = data.data[2] ? data.data[2].amount : "";
                _this2.amount3 = data.data[3] ? data.data[3].amount : "";
                _this2.amount4 = data.data[4] ? data.data[4].amount : "";
                _this2.userLogo0 = data.data[0] ? data.data[0].userLogo : "";
                _this2.userLogo1 = data.data[1] ? data.data[1].userLogo : "";
                _this2.userLogo2 = data.data[2] ? data.data[2].userLogo : "";
                _this2.userLogo3 = data.data[3] ? data.data[3].userLogo : "";
                _this2.userLogo4 = data.data[4] ? data.data[4].userLogo : "";
            });
        },
        getRewardGather: function getRewardGather() {
            var _this3 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardGather",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this3.gatherList = data.data;
                _this3.rewardName = data.data[1].rewardName;
                _this3.amountTotal = data.data[1].amountTotal;
                _this3.custTotal = data.data[1].custTotal;
                _this3.activeTotal = data.data[1].activeTotal;
                _this3.transTotal = data.data[1].transTotal;
                _this3.activityCode = data.data[1].activityCode;
            });
        },
        gotoDetail: function gotoDetail(title, activityCode) {
            this.$router.push({
                "path": "rewardDetail",
                "query": {
                    title: title,
                    activityCode: activityCode
                }
            });
        },
        getImg: function getImg(url) {
            if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        }
    }
};

/***/ }),

/***/ 1622:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.rewardMain {\n  width: 100%;\n  overflow: hidden;\n}\n.rewardMain .rewardHead {\n  width: 100%;\n  background: url(" + __webpack_require__(1683) + ") repeat;\n  background-size: 100% 100%;\n  display: inline-block;\n  height: 5.36rem;\n}\n.rewardMain .rewardHead .headMain {\n  width: 92%;\n  height: 5.36rem;\n  margin: 0.16rem 4%;\n  background: #FFFFFF;\n  box-shadow: 0 2px 12px 0 rgba(99, 113, 136, 0.12);\n  border-radius: 0.04px;\n}\n.rewardMain .rewardHead .headMain .headTitle {\n  background: #FF834D;\n  border-radius: 0.04rem 1rem 1rem 0;\n  font-size: 0.26rem;\n  color: #FFFFFF;\n  text-align: center;\n  width: 1.74rem;\n  height: 0.6rem;\n  line-height: 0.6rem;\n  float: left;\n}\n.rewardMain .rewardHead .headMain .headLogoMain {\n  width: 2.24rem;\n  height: 2.94rem;\n  margin: 0 auto;\n  background: url(" + __webpack_require__(1684) + ") no-repeat;\n  background-size: 100% 100%;\n  display: block;\n}\n.rewardMain .rewardHead .headMain .headLogoMain .headLogo {\n  width: 1rem;\n  height: 1rem;\n  margin: 0 auto;\n  padding-top: 0.222rem;\n}\n.rewardMain .rewardHead .headMain .headLogoMain .headLogo img {\n  display: inherit;\n  width: 1rem;\n  height: 1rem;\n  border-radius: 0.5rem;\n}\n.rewardMain .rewardHead .headMain .headLogoMain p {\n  width: 1.6rem;\n  height: 0.38rem;\n  line-height: 0.4rem;\n  font-size: 0.22rem;\n  color: #FFFFFF;\n  text-align: center;\n  background-image: -webkit-linear-gradient(left, #F9754E 0%, #FFB96C 100%);\n  background-image: linear-gradient(90deg, #F9754E 0%, #FFB96C 100%);\n  border-radius: 9.5px;\n  margin: 0.13rem auto 0;\n}\n.rewardMain .rewardHead .headMain .headLogoMain .totalAmount {\n  font-size: 0.26rem;\n  color: #BF5800;\n  text-align: center;\n  font-weight: bold;\n  margin-top: 0.22rem;\n}\n.rewardMain .rewardHead .headMain .rankMain {\n  width: 100%;\n  display: inline-block;\n  float: left;\n  margin-top: 2.1rem;\n}\n.rewardMain .rewardHead .headMain .rankMain .tab {\n  width: 5%;\n  height: 0.06rem;\n  background: rgba(63, 131, 255, 0.3);\n  border-radius: 8px;\n  margin: 0 auto;\n}\n.rewardMain .rewardHead .headMain .rankMain .tab li {\n  width: 0.14rem;\n  height: 0.06rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  -ms-grid-column-align: flex-start;\n      justify-items: flex-start;\n  font-family: PingFangSC-Regular;\n  font-size: .28rem;\n  color: #3D4A5B;\n  text-align: center;\n  position: relative;\n  float: left;\n}\n.rewardMain .rewardHead .headMain .rankMain .tab li.active {\n  width: 0.2rem;\n  height: 0.06rem;\n  background: #3F83FF;\n  border-radius: 8px;\n}\n.rewardMain .rewardHead .headMain .swiMain {\n  width: 100%;\n  display: block;\n  height: auto;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail {\n  width: 86%;\n  margin: 0 7%;\n  display: inline-block;\n  position: absolute;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList {\n  width: 100%;\n  float: left;\n  padding: 0.15rem 0;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList .rankLogo {\n  width: 10%;\n  opacity: 0.6;\n  font-size: 0.34rem;\n  color: #3D4A5B;\n  letter-spacing: -0.31px;\n  text-align: center;\n  font-weight: 400;\n  float: left;\n  line-height: 0.7rem;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList .nickLogo {\n  width: 0.72rem;\n  height: 0.72rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.32rem;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList .nickLogo img {\n  width: 0.72rem;\n  height: 0.72rem;\n  display: inherit;\n  border-radius: 0.36rem;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList span {\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  font-weight: bold;\n  float: left;\n  line-height: 0.7rem;\n  margin-left: 0.16rem;\n}\n.rewardMain .rewardHead .headMain .swiMain .rankDetail .rankList .numRight {\n  float: right;\n  font-size: 0.26rem;\n  color: #ED9856;\n  line-height: 0.7rem;\n}\n.rewardMain .rewardBottom {\n  width: 92%;\n  margin: 0.48rem 4% 0.22rem;\n}\n.rewardMain .rewardBottom .bottomTitle {\n  font-size: 0.34rem;\n  color: #3D4A5B;\n  font-weight: bold;\n  margin-bottom: 0.32rem;\n}\n.rewardMain .rewardBottom .rewardList {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.rewardMain .rewardBottom .rewardList .item {\n  width: 100%;\n  height: 5.04rem;\n  position: relative;\n  background: #FFFFFF;\n  box-shadow: 0 2px 12px 0 rgba(99, 113, 136, 0.12);\n  border-radius: 4px;\n  padding: 0.32rem 0.11rem;\n}\n.rewardMain .rewardBottom .rewardList .item .itemTitle {\n  font-size: 0.3rem;\n  color: #3D4A5B;\n  text-align: center;\n}\n.rewardMain .rewardBottom .rewardList .item .itemTitle img {\n  width: 0.36rem;\n  height: 0.36rem;\n  margin: 0 0.16rem 0 0;\n}\n.rewardMain .rewardBottom .rewardList .item b {\n  width: 100%;\n  text-align: center;\n  display: block;\n  font-size: 0.56rem;\n  color: #3F83FF;\n  margin-top: 0.32rem;\n}\n.rewardMain .rewardBottom .rewardList .item span {\n  width: 100%;\n  opacity: 0.4;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  display: inherit;\n  text-align: center;\n}\n.rewardMain .rewardBottom .rewardList .item .detail {\n  height: 1.98rem;\n  background: #F6FBFF;\n  border-radius: 17px;\n  margin-top: 0.2rem;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailText {\n  margin-bottom: 0.16rem;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailText b {\n  width: 1.04rem;\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  text-align: left;\n  float: left;\n  margin-top: 0;\n  margin-bottom: 0.16rem;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailText span {\n  opacity: 0.4;\n  font-size: 0.16rem;\n  color: #3D4A5B;\n  text-align: left;\n  width: 1.04rem;\n  float: left;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailText h1 {\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  text-align: right;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailMain {\n  width: 33%;\n  text-align: center;\n  float: left;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailMain span {\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  padding-top: 0.48rem;\n  font-weight: bold;\n  opacity: 1;\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailMain .detailIntro {\n  margin-top: 0.22rem;\n  opacity: 0.8;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  border-right: 1px solid rgba(61, 74, 91, 0.3);\n}\n.rewardMain .rewardBottom .rewardList .item .detail .detailMain .detailIntroLast {\n  margin-top: 0.22rem;\n  opacity: 0.8;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n}\n.rewardMain .rewardBottom .rewardList .item p {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  font-weight: bold;\n  letter-spacing: 0;\n  text-align: center;\n  margin-top: 0.22rem;\n}\n.rewardMain .rewardBottom .rewardList .item p img {\n  margin-left: 0.06rem;\n  width: 0.1rem;\n  height: 0.15rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1683:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/bg_background.png?v=278a3fb7";

/***/ }),

/***/ 1684:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/bg_top.png?v=4fb48100";

/***/ }),

/***/ 1685:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ico_unionpay.png?v=c2663128";

/***/ }),

/***/ 1686:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump_reward.png?v=716cb1b4";

/***/ }),

/***/ 1935:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "rewardMain"
  }, [_c('div', {
    staticClass: "rewardHead"
  }, [_c('div', {
    staticClass: "headMain"
  }, [_c('div', {
    staticClass: "headTitle"
  }, [_vm._v("\n                    奖励排行榜\n                ")]), _vm._v(" "), _c('div', {
    staticClass: "headLogoMain"
  }, [_c('div', {
    staticClass: "headLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo0),
      "alt": ""
    }
  })]), _vm._v(" "), _c('p', [_vm._v("top1 " + _vm._s(_vm.userName0))]), _vm._v(" "), _c('div', {
    staticClass: "totalAmount"
  }, [_vm._v("总额 " + _vm._s(_vm.amount0) + "元")])]), _vm._v(" "), _c('div', {
    staticClass: "swiMain"
  }, [_c('div', {
    staticClass: "swiper-container",
    staticStyle: {
      "overflow": "inherit !important"
    },
    attrs: {
      "id": "swiper-container"
    }
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "rankDetail"
  }, [_c('div', {
    staticClass: "rankList",
    staticStyle: {
      "border-bottom": "1px solid rgba(234, 234, 238, 0.4)"
    }
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        02\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo1),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName1))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount1) + "元\n                                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "rankList"
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        03\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo2),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName2))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount2) + "元\n                                    ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "rankDetail"
  }, [_c('div', {
    staticClass: "rankList",
    staticStyle: {
      "border-bottom": "1px solid rgba(234, 234, 238, 0.4)"
    }
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        04\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo3),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName3))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount3) + "元\n                                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "rankList"
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        05\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo4),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName4))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount4) + "元\n                                    ")])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "rankMain"
  }, [_c('ul', {
    staticClass: "tab"
  }, _vm._l((_vm.allRankList), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.active
      }
    })
  }), 0)])])]), _vm._v(" "), _c('div', {
    staticClass: "rewardBottom"
  }, [_c('div', {
    staticClass: "bottomTitle"
  }, [_vm._v("\n                我的奖励\n            ")]), _vm._v(" "), _c('div', {
    staticClass: "rewardList"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "itemTitle"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1685),
      "alt": ""
    }
  }), _vm._v("\n                        " + _vm._s(_vm.rewardName) + "\n                    ")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.amountTotal))]), _vm._v(" "), _c('span', [_vm._v("奖励总额（元）")]), _vm._v(" "), _c('div', {
    staticClass: "rewardDetail"
  }, [_c('div', {
    staticClass: "detail"
  }, [_c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.custTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntro"
  }, [_vm._v("\n                                    商户总数（个）\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.activeTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntro"
  }, [_vm._v("\n                                    激活达标（个）\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.transTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntroLast"
  }, [_vm._v("\n                                    交易达标（个）\n                                ")])])])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.gotoDetail(_vm.rewardName, _vm.activityCode);
      }
    }
  }, [_vm._v("查看明细"), _c('img', {
    attrs: {
      "src": __webpack_require__(1686),
      "alt": ""
    }
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-d7049322", module.exports)
  }
}

/***/ }),

/***/ 2083:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1622);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("22ed77eb", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d7049322!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardCenter.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d7049322!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardCenter.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 629:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2083)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1426),
  /* template */
  __webpack_require__(1935),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\rewardCenter.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardCenter.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d7049322", Component.options)
  } else {
    hotAPI.reload("data-v-d7049322", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});