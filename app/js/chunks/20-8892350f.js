webpackJsonp([20],{

/***/ 1454:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(954);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            menuArray: [{
                title: "",
                active: "",
                num: ""
            }, {
                title: "",
                active: "",
                num: ""
            }, {
                title: "",
                active: "",
                num: ""
            }],
            totalActAmount: '',
            activeShop: [],
            shopTrans: [],
            sleepShop: [],
            allShopList: [{
                list: [],
                isNoData: false
            }, {
                list: [],
                isNoData: false
            }, {
                list: [],
                isNoData: false
            }],
            currentPage: 1,
            page: "",
            hasData: false,
            expressFee: "",
            isLoading: true,
            mySwiper: null,
            index: 0,
            defaultText: "",
            minHeight: 0,
            count: ""

        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getTotalMoney();
        // this.$refs.searchBox.inputValue = "";
        window.goBack = this.goBack;
        var type = this.$route.query.type || 0;
        this.index = type;
        if (type == 0) {
            this.getActive();
        }
        this.$nextTick(function () {
            _this.getHeight();
        });

        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            // autoHeight: true,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {

                var index = swiper.activeIndex;
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.menuArray[index].active = true;
                // var wrapper = document.getElementById("swiper-wrapper");
                // wrapper.style.height = 'auto';
                _this.index = index;
                _this.currentPage = 1;

                if (index == 0) {
                    _this.$set(_this.allShopList[0], "isNoData", false);
                    _this.changeList(_this.menuArray[index], index);
                    // this.getActive();
                }
            }
        });
        // this.changeList(this.menuArray[type], type);
        document.querySelector(".jlMain").scrollTop = 10000 + "px";
    },

    components: {
        searchBox: _searchBox2.default,
        freshToLoadmore: _freshToLoadmore2.default
    },
    methods: {
        getTotalMoney: function getTotalMoney() {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "rewardCenter/lakalaReward",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("金额返回值", data);
                _this2.totalActAmount = data.data.totalActAmount;
                _this2.count = data.data.count;
            });
        },
        searchBtn: function searchBtn(search, done) {
            this.currentPage = 1;
            console.log(this.index);
            if (this.index == 0) {
                console.log("跳转奖励页面1");
                this.getActive();
            }
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = scroller.getBoundingClientRect().top;
            console.log("bodyHeight", bodyHeight);
            console.log("scroller", scroller);
            console.log("scrollerTop", scrollerTop);
            scroller.style.height = bodyHeight - 200 + "px";
            this.minHeight = 529 + "px";
            console.log("高度为", this.minHeight);
        },
        changeList: function changeList(i, index) {
            console.log("222");
            this.index = index;
            this.currentPage = 1;
            this.defaultText = "";
            this.menuArray.map(function (el) {
                el.active = false;
            });
            i.active = true;
            if (this.index == '0') {
                this.allShopList[0].list = [];
                this.$set(this.allShopList[0], "isNoData", false);
                this.getActive();
            }
            this.mySwiper.slideTo(index, 500, false);
        },
        callPhone: function callPhone(phone) {
            var phoneJson = {};
            phoneJson.phone = phone;
            console.log(phoneJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeCall.postMessage(JSON.stringify(phoneJson));
            } else {
                window.android.nativeCall(JSON.stringify(phoneJson));
            }
        },
        getActive: function getActive() {
            var _this3 = this;

            if (this.currentPage === 1) {
                this.allShopList[0].list = [];
            }
            console.log("页数1", this.currentPage);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "rewardCenter/lakalaReturnCase",
                data: {
                    "page": this.currentPage
                },
                showLoading: false
            }, function (data) {

                _this3.$set(_this3.allShopList[0], "isNoData", false);
                //				this.allShopList[0].list = data.data.object;
                // this.menuArray[0].num = data.data.totalResult ? data.data.totalResult : "0";
                if (data.data.totalResult == 0) {
                    _this3.allShopList[0].list = [];
                    _this3.$set(_this3.allShopList[0], "isNoData", true);
                } else if (data.data.length > 0) {
                    if (_this3.currentPage == 1) {
                        _this3.allShopList[0].list = [];
                    }
                    data.data.map(function (el) {
                        _this3.allShopList[0].list.push(el);
                    });
                    console.log("222222222", _this3.allShopList);

                    _this3.currentPage++;
                    console.log("页数", _this3.currentPage);
                } else {
                    if (_this3.currentPage > 1) {
                        common.toast({
                            content: '无更多数据'
                        });
                    } else {
                        _this3.$set(_this3.allShopList[0], "isNoData", true);
                    }
                }
            });
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        infinite: function infinite() {
            var _this4 = this;

            setTimeout(function () {
                if (_this4.index == 0) {
                    _this4.getActive();
                }
            }, 1000);
        },
        refresh: function refresh() {
            var _this5 = this;

            setTimeout(function () {
                _this5.currentPage = 1;
                if (_this5.index == 0) {
                    _this5.getActive();
                }
                _this5.mySwiper.onResize();
            }, 1000);
        },
        loadmore: function loadmore() {
            var _this6 = this;

            setTimeout(function () {
                if (_this6.index == 0) {
                    _this6.getActive();
                }
                _this6.mySwiper.onResize();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1508:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-13605d42] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-13605d42] {\n  background: #fff;\n}\n.tips_success[data-v-13605d42] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-13605d42] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-13605d42] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-13605d42],\n.fade-leave-active[data-v-13605d42] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-13605d42],\n.fade-leave-to[data-v-13605d42] {\n  opacity: 0;\n}\n.default_button[data-v-13605d42] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-13605d42] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-13605d42] {\n  position: relative;\n}\n.loading-tips[data-v-13605d42] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.jlMain[data-v-13605d42] {\n  width: 100%;\n}\n.jlMain .top-con[data-v-13605d42] {\n  background: url(" + __webpack_require__(1726) + ") no-repeat;\n  width: 100%;\n  height: 2.64rem;\n  background-size: 100% 100%;\n}\n.jlMain .top-con ul[data-v-13605d42] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n  -webkit-justify-content: flex-start;\n     -moz-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n.jlMain .top-con ul li[data-v-13605d42] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  margin: 0.54rem auto 0;\n  text-align: center;\n  position: relative;\n}\n.jlMain .top-con ul li .little-title[data-v-13605d42] {\n  font-size: .7rem;\n  color: #FFFFFF;\n  font-weight: bold;\n}\n.jlMain .top-con ul li .little-number[data-v-13605d42] {\n  opacity: 0.6;\n  font-size: 0.24rem;\n  color: #E0EDFF;\n}\n.jlMain .jlTitle[data-v-13605d42] {\n  width: 100%;\n  height: .85rem;\n  background: #ffffff;\n  margin-top: 0.167rem;\n}\n.jlMain .jlTitle span[data-v-13605d42] {\n  font-size: 0.32rem;\n  color: #333333;\n  display: inherit;\n  line-height: .85rem;\n  margin-left: 0.28rem;\n}\n.jlMain .queryHead[data-v-13605d42] {\n  width: 100%;\n  overflow: hidden;\n}\n.jlMain .queryHead .queryHead_menu[data-v-13605d42] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n  overflow: hidden;\n  position: relative;\n}\n.jlMain .queryHead .queryHead_menu[data-v-13605d42]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.jlMain .queryHead .queryHead_menu[data-v-13605d42]:last-child:after {\n  content: '';\n  background: none;\n}\n.jlMain .queryHead .queryHead_menu a[data-v-13605d42] {\n  color: rgba(61, 74, 91, 0.6);\n  font-size: 0.26rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 100%;\n  text-align: center;\n  position: relative;\n  line-height: 0.8rem;\n}\n.jlMain .queryHead .queryHead_menu a.active[data-v-13605d42] {\n  font-size: 0.26rem;\n  color: #3F83FF;\n  font-weight: 700;\n  color: #3d4a5b;\n}\n.jlMain .queryHead .queryHead_menu a.active[data-v-13605d42]:after {\n  content: \"\";\n  position: absolute;\n  bottom: 0;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  width: 0.24rem;\n  height: 0.06rem;\n  background: #3F83FF;\n  border-radius: 0.2rem;\n}\n.jlMain .queryHead1[data-v-13605d42] {\n  width: 100%;\n  overflow: hidden;\n  margin-top: 0.01rem;\n  border-radius: 0.46rem;\n}\n.jlMain .queryHead1 .queryHead_menu[data-v-13605d42] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n  overflow: hidden;\n  position: relative;\n}\n.jlMain .queryHead1 .queryHead_menu[data-v-13605d42]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.jlMain .queryHead1 .queryHead_menu[data-v-13605d42]:last-child:after {\n  content: '';\n  background: none;\n}\n.jlMain .queryHead1 .queryHead_menu a[data-v-13605d42] {\n  color: #cccccc;\n  font-size: 0.28rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 100%;\n  text-align: center;\n  position: relative;\n  line-height: 0.8rem;\n}\n.jlMain .queryHead1 .queryHead_menu a.active[data-v-13605d42] {\n  font-size: 0.28rem;\n  color: #3F83FF;\n  font-weight: 700;\n}\n.jlMain .queryHead1 .queryHead_menu a.active[data-v-13605d42]:after {\n  content: \"\";\n  position: absolute;\n  bottom: 0;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  width: 0.6rem;\n  height: 0.06rem;\n  background: #3F83FF;\n  border-radius: 0.2rem;\n}\n.jlMain .orderList[data-v-13605d42] {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n  border-radius: 0.08rem;\n}\n.jlMain .orderList .item[data-v-13605d42] {\n  width: 92%;\n  margin: 0 auto;\n  height: 2.62rem;\n  position: relative;\n  overflow: hidden;\n  padding: 0.4rem 0;\n}\n.jlMain .orderList .item[data-v-13605d42]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.jlMain .orderList .item[data-v-13605d42]:last-child:after {\n  content: '';\n  background: none;\n}\n.jlMain .orderList .item img[data-v-13605d42] {\n  width: 0.6rem;\n  height: 0.6rem;\n  float: left;\n}\n.jlMain .orderList .item b[data-v-13605d42] {\n  font-size: 0.26rem;\n  color: #333333;\n  float: left;\n  line-height: 0.6rem;\n  margin-left: 0.3rem;\n  font-weight: 300;\n}\n.jlMain .orderList .item b.phone[data-v-13605d42] {\n  width: 0.72rem;\n  height: 0.72rem;\n  display: block;\n  background: url(" + __webpack_require__(799) + ") no-repeat;\n  background-size: 0.72rem 0.72rem;\n  float: right;\n  position: absolute;\n  top: 1.3rem;\n  right: 0.2rem;\n}\n.jlMain .orderList .item b.noTrans[data-v-13605d42] {\n  background: #FFF3DE;\n  border-radius: 0.08rem;\n  width: 1.5rem;\n  height: 0.38rem;\n  line-height: 0.38rem;\n  font-size: 0.24rem;\n  color: #FFAE29;\n  letter-spacing: 0.34px;\n  text-align: center;\n  font-weight: 100;\n  margin-top: 0.06rem;\n  margin-left: 0.06rem;\n}\n.jlMain .orderList .item b.noActive[data-v-13605d42] {\n  background: #FFE4E4;\n  border-radius: 0.08rem;\n  width: 1.44rem;\n  height: 0.38rem;\n  line-height: 0.38rem;\n  font-size: 0.24rem;\n  color: #E95647;\n  letter-spacing: 0.34px;\n  text-align: center;\n  font-weight: 100;\n  margin-top: 0.06rem;\n  margin-left: 0.06rem;\n}\n.jlMain .orderList .item[data-v-13605d42]::after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #ECF0FF;\n  width: 100%;\n  height: 5px;\n  -webkit-transform: scaleY(0.5);\n  -ms-transform: scaleY(0.5);\n  transform: scaleY(0.5);\n}\n.jlMain .orderList .item p[data-v-13605d42] {\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-bottom: 0.1rem;\n  width: 100%;\n}\n.jlMain .orderList .item p b[data-v-13605d42] {\n  font-size: 0.3rem;\n  color: #3D4A5B;\n  display: block;\n  float: left;\n}\n.jlMain .orderList .item p b.phone[data-v-13605d42] {\n  width: 0.72rem;\n  height: 0.72rem;\n  display: block;\n  background: url(" + __webpack_require__(799) + ") no-repeat;\n  background-size: 0.72rem 0.72rem;\n  float: right;\n}\n.jlMain .orderList .item p b.noTrans[data-v-13605d42] {\n  background: #FFF3DE;\n  border-radius: 0.08rem;\n  width: 1.5rem;\n  height: 0.38rem;\n  line-height: 0.38rem;\n  font-size: 0.24rem;\n  color: #FFAE29;\n  letter-spacing: 0.34px;\n  text-align: center;\n  font-weight: 100;\n  margin-top: 0.06rem;\n  margin-left: 0.06rem;\n}\n.jlMain .orderList .item p b.noActive[data-v-13605d42] {\n  background: #FFE4E4;\n  border-radius: 0.08rem;\n  width: 1.44rem;\n  height: 0.38rem;\n  line-height: 0.38rem;\n  font-size: 0.24rem;\n  color: #E95647;\n  letter-spacing: 0.34px;\n  text-align: center;\n  font-weight: 100;\n  margin-top: 0.06rem;\n  margin-left: 0.06rem;\n}\n.jlMain .orderList .item p i[data-v-13605d42] {\n  font-size: 0.26rem;\n  color: #FF5F5F;\n  display: block;\n  font-weight: 700;\n  float: right;\n}\n.jlMain .orderList .item p i strong[data-v-13605d42] {\n  text-align: right;\n  display: block;\n  font-size: 0.32rem;\n}\n.jlMain .orderList .item em[data-v-13605d42] {\n  display: inline-block;\n  color: #3D4A5B;\n  font-size: 0.26rem;\n  width: 40%;\n  float: left;\n  opacity: 0.6;\n  margin-top: 0.2rem;\n  margin-left: 0.9rem;\n}\n.jlMain .orderList .item i[data-v-13605d42] {\n  font-size: 0.26rem;\n  color: #666666;\n  display: block;\n  float: right;\n  margin-top: 0.2rem;\n}\n.jlMain .orderList .item i strong[data-v-13605d42] {\n  text-align: right;\n  display: block;\n  font-size: 0.32rem;\n}\n.jlMain .orderList .item span[data-v-13605d42] {\n  float: left;\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  position: relative;\n  opacity: 0.6;\n  height: 0.54rem;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n", ""]);

// exports


/***/ }),

/***/ 1726:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/lklBg.png?v=e03f4eaf";

/***/ }),

/***/ 1727:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/lklNo.png?v=a04e1b46";

/***/ }),

/***/ 1728:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/lklShop.png?v=76f4f34f";

/***/ }),

/***/ 1822:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "jlMain"
  }, [_c('div', {
    staticClass: "top-con"
  }, [_c('ul', [_c('li', [_c('p', {
    staticClass: "little-title"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.totalActAmount)))]), _vm._v(" "), _c('p', {
    staticClass: "little-number"
  }, [_vm._v("活动返现（元）")])]), _vm._v(" "), _c('li', [_c('p', {
    staticClass: "little-title"
  }, [_vm._v(_vm._s(_vm._f("keepTwoNum")(_vm.count)))]), _vm._v(" "), _c('p', {
    staticClass: "little-number"
  }, [_vm._v("激活数量（台）")])])])]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "queryHead1"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.menuArray), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.title))])
  }), 0)]), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? (_vm.allShopList[0].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [(_vm.allShopList[0].list != '') ? _c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allShopList[0].list), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('img', {
      attrs: {
        "src": __webpack_require__(1728),
        "alt": ""
      }
    }), _c('b', [_vm._v("商户名称:" + _vm._s(i.customerName))])]), _vm._v(" "), _c('em', [_vm._v("返现金额：")]), _vm._v(" "), _c('i', {
      staticStyle: {
        "color": "red"
      }
    }, [_vm._v(_vm._s(i.rewardAmount) + "元")]), _vm._v(" "), _c('em', [_vm._v("序列号：")]), _vm._v(" "), _c('i', [_vm._v(_vm._s(i.posSn))]), _vm._v(" "), _c('em', [_vm._v("激活日期：")]), _vm._v(" "), _c('i', [_vm._v(_vm._s(i.createTime))])])
  }), 0) : _c('div', [_c('img', {
    staticStyle: {
      "width": "40%",
      "margin": "2rem 30% 0.2rem"
    },
    attrs: {
      "src": __webpack_require__(1727),
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "center",
      "color": "#999999",
      "font-size": "0.28rem"
    }
  }, [_vm._v("暂无数据"), _c('br'), _vm._v("快去采购激活机器吧")])])])])])])], 1)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "jlTitle"
  }, [_c('span', [_vm._v("返现明细")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-13605d42", module.exports)
  }
}

/***/ }),

/***/ 1969:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1508);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7f43b332", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-13605d42&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./jlLkl.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-13605d42&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./jlLkl.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 659:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1969)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1454),
  /* template */
  __webpack_require__(1822),
  /* scopeId */
  "data-v-13605d42",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\jlLkl.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] jlLkl.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-13605d42", Component.options)
  } else {
    hotAPI.reload("data-v-13605d42", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 707:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_search.png?v=dcd14e7b";

/***/ }),

/***/ 799:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_contact_phone.png?v=4d5a2282";

/***/ }),

/***/ 948:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 949:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.searchBox_blank[data-v-21a1e918] {\n  height: 1rem;\n}\n.wrap[data-v-21a1e918] {\n  overflow: hidden;\n  background: #fff;\n  position: relative;\n  z-index: 10;\n}\n.searchBox2[data-v-21a1e918] {\n  width: 100%;\n  background: #fff;\n  font-family: 'PingFangSC-Regular';\n  left: 0;\n  top: 0.8rem;\n  z-index: 100;\n}\n.searchBox2.grey[data-v-21a1e918] {\n  background: #f6f8f7;\n  top: 2.58rem !important;\n  position: fixed;\n}\n.searchBox2.grey .searchBox_content input[data-v-21a1e918] {\n  background: url(" + __webpack_require__(707) + ") #fff no-repeat left 0.25rem center;\n  background-size: 0.28rem 0.28rem;\n}\n.searchBox2 .searchBox_content[data-v-21a1e918] {\n  width: 6.9rem;\n  height: 0.6rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin: 0 auto;\n}\n.searchBox2 .searchBox_content input[data-v-21a1e918] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: block;\n  font-size: 0.28rem;\n  border-radius: 0.08rem;\n  line-height: 0.6rem;\n  padding-left: 0.7rem;\n  background: url(" + __webpack_require__(707) + ") #ffffff no-repeat left 0.25rem center;\n  background-size: 0.28rem 0.28rem;\n  -webkit-transition: 0.3s ease;\n  transition: 0.3s ease;\n}\n.searchBox2 .searchBox_content input[data-v-21a1e918]::-webkit-input-placeholder {\n  color: #3D4A5B;\n  opacity: 0.15;\n}\n.searchBox2 .searchBox_content a[data-v-21a1e918] {\n  width: 0.9rem;\n  height: 0.6rem;\n  line-height: 0.6rem;\n  text-align: center;\n  color: #E95647;\n  border-radius: 0.5rem;\n  display: block;\n  font-size: 0.28rem;\n  margin-left: 0.2rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 954:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(956)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(948),
  /* template */
  __webpack_require__(955),
  /* scopeId */
  "data-v-21a1e918",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox2.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox2.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-21a1e918", Component.options)
  } else {
    hotAPI.reload("data-v-21a1e918", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 955:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox2",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox2"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-21a1e918", module.exports)
  }
}

/***/ }),

/***/ 956:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(949);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("72996842", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21a1e918&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox2.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21a1e918&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox2.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});