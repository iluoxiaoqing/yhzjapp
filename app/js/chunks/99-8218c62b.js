webpackJsonp([99],{

/***/ 1294:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips{\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n\n.tips_error{\n  background: #fff;\n}\n\n.tips_success{\n  background: #04be02;\n}\n\n/*顶部报错提示*/\n\n//加载更多；\n\n.gray(){\n    -webkit-filter: grayscale(100%);\n    -moz-filter: grayscale(100%);\n    -ms-filter: grayscale(100%);\n    -o-filter: grayscale(100%);\n    filter: grayscale(100%);\n    filter: gray;\n}\n\n#contentPage{\n    width: 100%;\n    -webkit-overflow-scrolling: touch;\n    overflow-scrolling: touch;\n    overflow: hidden;\n    height: 100%;\n    position: relative;\n}\n\n.boxSizing(){\n    box-sizing: border-box;\n    -webkit-box-sizing: border-box;\n    -moz-box-sizing: border-box;\n}\n\n.border-bottom(@color:#e5e5e5,@height:\\1px){\n  position:relative;\n  &:after{\n      content:'';\n      position: absolute;\n      left: 0;\n      bottom:0;\n      background: @color;\n      width: 100%;\n      height: @height;\n      transform: scaleY(0.5);\n  }\n  &:last-child{\n      &:after{\n        content:'';\n        background: none;\n      }\n  }\n}\n\n.border-1px(@color:#e5e5e5){\n    position:relative;\n    &:after{\n        content:'';\n        position: absolute;\n        left: -50%;\n        bottom:-50%;\n        width: 200%;\n        height: 200%;\n        transform: scale(0.5);\n        border: 1px solid @color;\n        box-sizing: border-box;\n        border-radius: .06rem;\n    }\n  }\n\n.border-right(@color:#e5e5e5){\n  position:relative;\n  &:after{\n      content:'';\n      position: absolute;\n      right: 0;\n      bottom:0;\n      background: @color;\n      width: 96%;\n      height: 1px;\n      transform: scaleY(0.5);\n  }\n}\n\n.border-center(@color:#e5e5e5){\n  position:relative;\n  &:after{\n      content:'';\n      position: absolute;\n      left: 50%;\n      bottom:0;\n      background: @color;\n      width: 6.9rem;\n      height: 1px;\n      transform: scaleY(0.5) translateX(-50%);\n  }\n}\n\n.border-top(@color:#E8E8EA){\n  position:relative;\n  &:after{\n      content:'';\n      position: absolute;\n      left: 0;\n      top:0;\n      background: @color;\n      width: 100%;\n      height: 1px;\n      transform: scaleY(0.5);\n  }\n}\n\n\n\n.v_center(){\n    position: absolute;\n    top: 50%;\n    transform:translateY(-50%);\n    -webkit-transform:translateY(-50%);\n}\n\n.h_center(){\n    position: absolute;\n    left: 50%;\n    transform:translateX(-50%);\n    -webkit-transform:translateX(-50%);\n}\n\n.center(){\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform:translate(-50%,-50%);\n    -webkit-transform:translate(-50%,-50%);\n}\n\n.over(){\n    overflow: hidden;\n    white-space: nowrap;\n    text-overflow: ellipsis;\n}\n\n\n.transition(@time){\n    transition:@time;\n    -webkit-transition:@time\n}\n\n\n.pull-to-refresh-layer{\n  height: 1.6rem;\n}\n\n.disableSelect(){\n  -webkit-touch-callout:none;\n  -webkit-user-select:none;\n    -khtml-user-select:none;\n    -moz-user-select:none;\n    -ms-user-select:none;\n    user-select:none;\n    -o-user-select:none;\n    -webkit-tap-highlight-color:rgba(0,0,0,0);\n    -moz-tap-highlight-color:rgba(0,0,0,0);\n}\n\n@blue:#3F83FF;\n@dark:#3d4a5b;\n@light_dark:#acb3b9;\n@default:#e4e5e8;\n@grey:#B7CAE1;\n@backgroundColor:#EBF0F7;\n@orange:#feba00;\n@purple:#5200a6;\n\n\n.fade-enter-active, .fade-leave-active {\n  transition: opacity .5s\n}\n.fade-enter, .fade-leave-to{\n  opacity: 0\n}\n\n.default_button{\n    width: 100%;\n    height: 0.94rem;\n    background:@blue;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    font-size: 0.36rem;\n    border-radius: 0.08rem;\n    color: #fff;\n    margin: 0.1rem 0;\n}\n\n.bold{\n  font-size: \"PingFangSC-Semibold\";\n}\n\n.flex(@pos:center){\n    display: flex;\n    justify-content:@pos;\n    align-items:center;\n}\n\n.scroller{\n    position: relative;\n    //overflow: hidden;\n    //margin-bottom: 0.2rem;\n}\n\n.loading-tips{\n    padding: 0 .15rem;\n    height: .5rem;\n    text-align: center;\n    background: rgba(0,0,0,0.6);\n    position: fixed;\n    left: 50%;\n    bottom: .5rem;\n    transform: translateX(-50%);\n    font-size: .24rem;\n    color: #fff;\n    border-radius: .05rem;\n    line-height: .5rem;\n    \n}\n\n.iphoneX_padding_bottom(){\n  @media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n    &:after{\n      content: \"\";\n      height:constant(safe-area-inset-bottom);\n      height:env(safe-area-inset-bottom);\n    }\n  }\n  @media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio:3) {\n    &:after{\n      content: \"\";\n      height:constant(safe-area-inset-bottom);\n      height:env(safe-area-inset-bottom);\n    }\n  }\n  @media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio:2) {\n    &:after{\n      content: \"\";\n      height:constant(safe-area-inset-bottom);\n      height:env(safe-area-inset-bottom);\n    }\n  }\n}\n\n.iphoneX_padding_fixed(){\n  @media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n    &{\n      bottom:constant(safe-area-inset-bottom);\n      bottom:env(safe-area-inset-bottom);\n    }\n  }\n  @media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio:3) {\n    &{\n      bottom:constant(safe-area-inset-bottom);\n      bottom:env(safe-area-inset-bottom);\n    }\n  }\n  @media only screen and (device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio:2) {\n    &{\n      bottom:constant(safe-area-inset-bottom);\n      bottom:env(safe-area-inset-bottom);\n    }\n  }\n}", ""]);

// exports


/***/ }),

/***/ 1580:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports
exports.i(__webpack_require__(1294), "");

// module
exports.push([module.i, "\n.suc_main[data-v-66689a54]{\r\n    width: 100%;\r\n    text-align: center;\n}\n.suc_mark[data-v-66689a54]{\r\n    width:21%;\r\n    margin:0.86rem 39.5% 0.26rem;\n}\nspan[data-v-66689a54]{\r\n    font-size: .36rem;\r\n    color: #333333;\n}\nh5[data-v-66689a54]{\r\n    font-size: .3rem;\r\n    color: #666666;\r\n    margin: 0.2rem 0 1rem;\n}\nbutton[data-v-66689a54]{\r\n    background: url(" + __webpack_require__(1678) + ") no-repeat;\r\n    width: 70%;\r\n    background-size: 100% 100%;\r\n    color: #ffffff;\r\n    font-size: 0.3rem;\r\n    height: 0.88rem;\n}\n.cover_back[data-v-66689a54]{\r\n    position: fixed;\r\n    left: 0;\r\n    top: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    background: rgba(0, 0, 0, 0.6);\n}\n.arrow_mark[data-v-66689a54]{\r\n    position: absolute;\r\n    display: block;\r\n    right: .3rem;\r\n    top: 0;\r\n    width: 73%;\n}", ""]);

// exports


/***/ }),

/***/ 1678:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/btn_successful.png?v=9737cedd";

/***/ }),

/***/ 1893:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "suc_main"
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-66689a54", module.exports)
  }
}

/***/ }),

/***/ 2041:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1580);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("508b10c3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-66689a54&scoped=true!./regZdyqSuccess.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-66689a54&scoped=true!./regZdyqSuccess.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 621:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2041)

var Component = __webpack_require__(2)(
  /* script */
  null,
  /* template */
  __webpack_require__(1893),
  /* scopeId */
  "data-v-66689a54",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZdyq\\regZdyqSuccess.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] regZdyqSuccess.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-66689a54", Component.options)
  } else {
    hotAPI.reload("data-v-66689a54", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});