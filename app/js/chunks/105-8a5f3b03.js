webpackJsonp([105],{

/***/ 1423:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            butnTitle: "获取验证码",
            imgUrl: '',
            vCode: "",
            phoneNo: "",
            code: "",
            password: "",
            imgKey: "",
            timerNum: 60,
            clock: '',
            isNotClick: false,
            verfKey: "",
            isAcitive: false,
            token: "",
            name: "",
            isJump: ""

        };
    },
    mounted: function mounted() {
        this.getReg();
    },

    watch: {
        timerNum: function timerNum(newValue) {
            if (newValue > 0) {
                this.butnTitle = newValue + 's';
            } else {
                clearInterval(this.clock);
                this.isAcitive = false;
                this.isNotClick = false;
                this.butnTitle = '获取验证码';
            }
        }
    },
    methods: {
        getReg: function getReg() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cx/checkRegister",
                data: {},
                showLoading: true
            }, function (data) {
                console.log(data.data);
                _this.isJump = data.data.isJump;
                if (data.data.isJump == 'Y') {
                    var url1 = data.data.jumpAddress;
                    common.setCookie('url1', url1);
                    if (common.isClient() == "ios") {
                        try {
                            window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify({ url: url1 }));
                        } catch (error) {
                            window.location.href = url1;
                        }
                    } else {
                        try {
                            window.android.nativeOpenBrowser(JSON.stringify({ url: url1 }));
                        } catch (error) {
                            window.location.href = url1;
                        }
                    }
                } else {
                    _this.name = data.data.custName;
                    _this.phoneNo = data.data.mobile;
                }
            });
        },
        requestVeriCode: function requestVeriCode() {
            var _this2 = this;

            var myName = _base2.default.name.reg;
            if (!myName.test(this.name)) {
                var errorMsg = _base2.default.name.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cx/sendMsm",
                data: {
                    'phoneNo': this.phoneNo
                },
                showLoading: true
            }, function (data) {
                if (data.code == "0000") {
                    common.toast({
                        content: "验证码已经发送"
                    });
                    _this2.token = data.data;
                    _this2.sendVericdoe();
                } else if (data.code == "0001") {
                    _this2.token = data.data;
                    _this2.sendVericdoe();
                    common.toast({
                        content: data.msg
                    });
                }
            });
        },
        sendVericdoe: function sendVericdoe() {
            this.timerNum = 60;
            this.butnTitle = this.timerNum + 's';
            console.log("111");
            this.isAcitive = true;
            var that = this;
            this.isNotClick = true;
            console.log("333");
            this.clock = setInterval(function () {
                console.log("2222");
                that.timerNum--;
                console.log("4444");
            }, 1000);
        },
        registerRequest: function registerRequest() {
            var myName = _base2.default.name.reg;
            if (!myName.test(this.name)) {
                var errorMsg = _base2.default.name.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            if (this.code == "") {
                common.toast({
                    content: "验证码不能为空"
                });
                return;
            }
            var veriReg = _base2.default.password3.reg;
            if (!veriReg.test(this.password)) {
                var veriErrorMsg = _base2.default.password3.error;
                common.toast({
                    content: veriErrorMsg
                });
                return;
            }

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cx/cxRegister",
                data: {
                    mobile: this.phoneNo, //注册手机号，登陆账号
                    password: this.password, //用户登陆密码
                    token: this.token, //短信验证码接口返回的唯一标志
                    code: this.code, //短信验证码
                    directNo: this.$route.query.directNo || '',
                    orderNo: this.$route.query.orderNo || '',
                    custName: this.name

                },
                showLoading: true
            }, function (data) {
                console.log("注册成功===", data);
                if (data.code == '0000') {
                    common.toast({
                        content: '注册成功'
                    });
                    //                	window.location.href=data.data;

                    var url1 = data.data;
                    if (common.isClient() == "ios") {
                        try {
                            window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify({ url: url1 }));
                        } catch (error) {
                            window.location.href = url1;
                            //                            window.location.reload();
                        }
                    } else {
                        try {
                            window.android.nativeOpenBrowser(JSON.stringify({ url: url1 }));
                        } catch (error) {
                            window.location.href = url1;
                            //                            window.location.reload();
                        }
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1483:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1797:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.isJump == 'N') ? _c('div', {
    staticClass: "zfy_main"
  }, [_c('div', {
    staticClass: "cx_main"
  }, [_c('div', {
    staticClass: "cx_logo"
  }), _vm._v(" "), _c('div', {
    staticClass: "zc_main"
  }, [_c('div', {
    staticClass: "regMain"
  }, [_c('p', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    attrs: {
      "type": "text",
      "disabled": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phoneNo),
      expression: "phoneNo"
    }],
    attrs: {
      "maxlength": "11",
      "type": "tel",
      "disabled": ""
    },
    domProps: {
      "value": (_vm.phoneNo)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phoneNo = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "leftInput"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.code),
      expression: "code"
    }],
    attrs: {
      "maxlength": "6",
      "placeholder": "请输入验证码",
      "type": "tel"
    },
    domProps: {
      "value": (_vm.code)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.code = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "rightBtn"
  }, [_c('button', {
    class: {
      activity: _vm.isAcitive
    },
    attrs: {
      "disabled": _vm.butnTitle !== '获取验证码'
    },
    on: {
      "click": _vm.requestVeriCode
    }
  }, [_vm._v(_vm._s(_vm.butnTitle))])]), _vm._v(" "), _c('p', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    attrs: {
      "placeholder": "设置6-18位数字与字母组合密码",
      "type": "password",
      "maxlength": "18"
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])])])]), _vm._v(" "), _c('button', {
    staticClass: "btn",
    attrs: {
      "disabled": (_vm.name.length == 0 && _vm.phoneNo.length == 0 && _vm.code.length == 0 && _vm.password.length == 0) || (_vm.name.length != 0 && _vm.phoneNo.length == 0 && _vm.code.length == 0 && _vm.password.length == 0) || (_vm.name.length != 0 && _vm.phoneNo.length != 0 && _vm.code.length == 0 && _vm.password.length == 0) || (_vm.name.length != 0 && _vm.phoneNo.length != 0 && _vm.code.length != 0 && _vm.password.length == 0) || (_vm.name.length == 0 && _vm.phoneNo.length == 0 && _vm.code.length == 0 && _vm.password.length != 0)
    },
    on: {
      "click": function($event) {
        return _vm.registerRequest()
      }
    }
  }, [_vm._v("完成注册")]), _vm._v(" "), _vm._m(0)]) : _c('div', {
    staticClass: "registerEd"
  }, [_c('p', [_vm._v("您已注册过诚享Plus，点击确认下载诚享APP")]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.getReg();
      }
    }
  }, [_vm._v("下载")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "cx_bottom"
  }, [_c('em', {
    staticStyle: {
      "color": "#023E90"
    }
  }, [_vm._v("三方支付公司产品 资金安全 秒到账")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-009495d9", module.exports)
  }
}

/***/ }),

/***/ 1944:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1483);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("8aab4636", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-009495d9!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-009495d9!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 624:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1944)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1423),
  /* template */
  __webpack_require__(1797),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\cxReg.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] cxReg.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-009495d9", Component.options)
  } else {
    hotAPI.reload("data-v-009495d9", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});