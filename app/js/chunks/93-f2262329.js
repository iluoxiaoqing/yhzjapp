webpackJsonp([93],{

/***/ 1428:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {};
    },

    props: {},
    components: {},
    mounted: function mounted() {},

    methods: {
        gotoZdyq: function gotoZdyq() {
            this.$router.push({
                "path": "activityZdyq",
                "query": {}
            });
        },
        gotoYlsf: function gotoYlsf() {
            this.$router.push({
                "path": "activityYlsf",
                "query": {}
            });
        }
    }
};

/***/ }),

/***/ 1597:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.actiMain {\n  width: 100%;\n}\n.actiMain .actiList {\n  width: 91.4%;\n  margin: 0 4.3%;\n}\n.actiMain .actiList .item {\n  background: #ffffff;\n  width: 100%;\n  border-radius: 8px;\n  margin-top: 0.16rem;\n  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.08);\n}\n.actiMain .actiList .item img {\n  width: 100%;\n  height: 3.96rem;\n}\n.actiMain .actiList .item p {\n  font-weight: bold;\n  font-size: 0.32rem;\n  color: #3D4A5B;\n  margin: 0.3rem 0 0 0.25rem;\n}\n.actiMain .actiList .item span {\n  opacity: 0.6;\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  letter-spacing: 0.14px;\n  display: inherit;\n  margin: 0 0.25rem;\n  padding-bottom: 0.3rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1694:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/zdyq.png?v=76c5b985";

/***/ }),

/***/ 1695:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/zdyq1.png?v=4b41ee62";

/***/ }),

/***/ 1910:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "actiMain"
  }, [_c('div', {
    staticClass: "actiList"
  }, [_c('div', {
    staticClass: "item",
    on: {
      "click": function($event) {
        return _vm.gotoZdyq();
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1694),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("超大活动！！！！")]), _vm._v(" "), _c('span', [_vm._v("账单延期返现最高可拿100元")])]), _vm._v(" "), _c('div', {
    staticClass: "item",
    on: {
      "click": function($event) {
        return _vm.gotoYlsf()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1695),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("低费率产品也有返现啦！！！")]), _vm._v(" "), _c('span', [_vm._v("银联闪付（收款计划）费率低至0.38%，还有最高20元激活返现等您拿！")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-8299c396", module.exports)
  }
}

/***/ }),

/***/ 2058:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1597);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2a45783b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8299c396!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wonderActivity.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8299c396!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wonderActivity.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 631:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2058)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1428),
  /* template */
  __webpack_require__(1910),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\wonderActivity.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] wonderActivity.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8299c396", Component.options)
  } else {
    hotAPI.reload("data-v-8299c396", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});