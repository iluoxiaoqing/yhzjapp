webpackJsonp([94],{

/***/ 1421:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            bussGroups: [],
            business: {},
            imgUrl1: _apiConfig2.default.KY_IP,
            bannerList: [],
            mainCode: "",
            mainCodeName: ""
        };
    },
    mounted: function mounted() {
        this.getBusiness();
        this.getBanner();

        common.youmeng("推荐贷款", "进入推荐贷款");
    },

    methods: {
        getBusiness: function getBusiness() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/appBusiness",
                "data": {
                    "buss": "LOAN_CODE"
                }
            }, function (data) {
                console.log(data.data);
                _this.business = data.data.business;
                _this.bussGroups = data.data.groups;
                _this.mainCode = data.data.business.productCode;
                _this.mainCodeName = data.data.business.productName;
                data.data.groups.map(function (el) {
                    el.products.map(function (value) {
                        var userDesc = value.userDesc || "";
                        _this.$set(value, "userDesc", userDesc ? JSON.parse(userDesc) : {});
                    });
                });
            });
        },
        getBanner: function getBanner() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "wonderfulActivityInfo/findPageBanner",
                "data": {
                    "bannerType": "LOAN_CODE"
                }
            }, function (data) {
                _this2.bannerList = data.data || [];
                _this2.$nextTick(_this2.mySwiper);
            });
        },
        gotoLoan: function gotoLoan() {

            common.youmeng("贷款申请", "点击我要贷款");

            this.$router.push({
                "path": "applyLoan",
                "query": {
                    "buss": "LOAN_CODE"
                }
            });
        },
        gotoShare: function gotoShare(j) {

            if (!j.isShare) {
                return;
            }

            this.$router.push({
                "path": "share",
                "query": {
                    "product": j.productCode,
                    "productName": j.productName,
                    "channel": "recommendLoan"
                }
            });
            common.youmeng("推荐贷款", "点击" + j.productName);
        },
        mySwiper: function mySwiper() {
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2000, //可选选项，自动滑动
                loop: true,
                pagination: ".swiper-pagination"
            });
        }
    }
};

/***/ }),

/***/ 1566:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.creditCard {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.3rem;\n  padding-bottom: 1.5rem;\n}\n.creditCard .creditCard_banner {\n  width: 6.9rem;\n  height: 1.8rem;\n  margin: 0 auto 0.4rem;\n  background: #ccc;\n  border-radius: 0.08rem;\n  overflow: hidden;\n}\n.creditCard .creditCard_banner .swiper-container {\n  width: 100%;\n  height: 100%;\n  position: relative;\n}\n.creditCard .creditCard_banner .swiper-container .swiper-slide {\n  width: 100%;\n  height: 100%;\n}\n.creditCard .creditCard_banner .swiper-container .swiper-slide img {\n  width: 100%;\n  height: 100%;\n  display: block;\n}\n.creditCard .creditCard_banner .swiper-container .swiper-pagination {\n  bottom: 0.1rem;\n  height: 0.2rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.creditCard .creditCard_banner .swiper-container .swiper-pagination span.swiper-pagination-bullet {\n  width: 0.1rem;\n  height: 0.1rem;\n  display: block;\n  background: #A3A4A4;\n  opacity: 0.5;\n  margin: 0 0.05rem;\n}\n.creditCard .creditCard_banner .swiper-container .swiper-pagination span.swiper-pagination-bullet-active {\n  opacity: 1;\n}\n.creditCard .creditCard_bank {\n  width: 6.9rem;\n  margin: 0 auto;\n}\n.creditCard .creditCard_bank h1 {\n  color: #3D4A5B;\n  opacity: 0.8;\n  font-size: 0.32rem;\n  position: relative;\n}\n.creditCard .creditCard_bank h1 a {\n  position: absolute;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n      -ms-transform: translateY(-50%);\n          transform: translateY(-50%);\n  right: 0;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  font-weight: 100;\n  background: url(" + __webpack_require__(164) + ") no-repeat right center;\n  background-size: 0.16rem 0.26rem;\n  padding-right: 0.26rem;\n  opacity: 0.9;\n}\n.creditCard .creditCard_bank .bankList {\n  width: 100%;\n  padding-top: 0.5rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.creditCard .creditCard_bank .bankList .item {\n  width: 1.8rem;\n  position: relative;\n  background: #FFFFFF;\n  box-shadow: 0 0 0.32rem 0 rgba(63, 131, 255, 0.15);\n  border-radius: 0.08rem;\n  padding: 0.3rem 0.2rem;\n  margin-bottom: 0.54rem;\n  padding-top: 0.4rem;\n}\n.creditCard .creditCard_bank .bankList .item.height0 {\n  height: 0px;\n  visibility: hidden;\n}\n.creditCard .creditCard_bank .bankList .item .logo {\n  width: 0.5rem;\n  height: 0.52rem;\n  position: absolute;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  left: 50%;\n  top: -0.26rem;\n}\n.creditCard .creditCard_bank .bankList .item .logo img {\n  display: block;\n  width: 100%;\n  height: 100%;\n}\n.creditCard .creditCard_bank .bankList .item b {\n  color: #3D4A5B;\n  opacity: 0.9;\n  font-size: 0.30rem;\n  display: block;\n  text-align: center;\n  margin-bottom: 0.1rem;\n}\n.creditCard .creditCard_bank .bankList .item p {\n  color: #3D4A5B;\n  opacity: 0.5;\n  font-size: 0.26rem;\n  text-align: center;\n  line-height: 0.36rem;\n  position: relative;\n  padding-bottom: 0.18rem;\n  margin-bottom: 0.2rem;\n}\n.creditCard .creditCard_bank .bankList .item p:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #E0E0E0;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.creditCard .creditCard_bank .bankList .item p:last-child:after {\n  content: '';\n  background: none;\n}\n.creditCard .creditCard_bank .bankList .item span {\n  display: block;\n  color: #3F83FF;\n  font-size: 0.28rem;\n  text-align: center;\n  font-weight: 700;\n}\n.creditCard .creditCard_bank .bankList .forward {\n  width: 2.20rem;\n  height: 2.80rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #FFFFFF;\n  box-shadow: 0 0 0.32rem 0 rgba(63, 131, 255, 0.15);\n  border-radius: 0.08rem;\n  margin-bottom: 0.54rem;\n}\n.creditCard .creditCard_bank .bankList .forward p {\n  width: 100%;\n  font-weight: 700;\n  font-size: 0.3rem;\n  color: #3D4A5B;\n  opacity: 0.3;\n  text-align: center;\n}\n.creditCard .creditCard_btn {\n  width: 92%;\n  height: 1.58rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  background-image: -webkit-linear-gradient(bottom, #FFFFFF 0%, rgba(255, 255, 255, 0) 100%);\n  background-image: linear-gradient(0deg, #FFFFFF 0%, rgba(255, 255, 255, 0) 100%);\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0 4%;\n}\n", ""]);

// exports


/***/ }),

/***/ 1675:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/banner_juxing.png?v=3a413061";

/***/ }),

/***/ 1676:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_logo.png?v=07409f54";

/***/ }),

/***/ 1879:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "creditCard"
  }, [(_vm.bannerList.length > 0) ? _c('div', {
    staticClass: "creditCard_banner"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.bannerList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide"
    }, [_c('a', {
      attrs: {
        "href": item.h5Url
      }
    }, [(item.imgUrl == '') ? _c('img', {
      attrs: {
        "src": __webpack_require__(1675)
      }
    }) : _c('img', {
      attrs: {
        "src": _vm.imgUrl1 + 'file/downloadFile?filePath=' + item.imgUrl,
        "alt": ""
      }
    })])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]) : _vm._e(), _vm._v(" "), _vm._l((_vm.bussGroups), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "creditCard_bank"
    }, [_c('h1', [_vm._v(_vm._s(i.group) + " "), _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.gotoLoan()
        }
      }
    }, [_vm._v("我要贷款")])]), _vm._v(" "), _c('div', {
      staticClass: "bankList"
    }, [_vm._l((i.products), function(j, index) {
      return [_c('div', {
        staticClass: "item",
        on: {
          "click": function($event) {
            return _vm.gotoShare(j)
          }
        }
      }, [(j.logo != '') ? _c('div', {
        staticClass: "logo"
      }, [_c('img', {
        attrs: {
          "src": _vm.imgUrl1 + 'file/downloadFile?filePath=' + j.logo,
          "alt": ""
        }
      })]) : _c('div', {
        staticClass: "logo"
      }, [_c('img', {
        attrs: {
          "src": __webpack_require__(1676),
          "alt": ""
        }
      })]), _vm._v(" "), _c('b', [_vm._v(_vm._s(j.productName))]), _vm._v(" "), (j.isShow) ? _c('p', {
        domProps: {
          "innerHTML": _vm._s(j.userDesc.feature ? j.userDesc.feature.join('<br>') : '')
        }
      }) : _c('p', [_vm._v("\n                        暂未开放"), _c('br'), _vm._v("敬请期待\n                    ")]), _vm._v(" "), (j.isShow) ? _c('span', [_vm._v(_vm._s(j.userDesc.rewardAmount || ""))]) : _vm._e()])]
    }), _vm._v(" "), (i.products.length % 3 == 2) ? _c('div', {
      staticClass: "item height0"
    }) : _vm._e()], 2)])
  }), _vm._v(" "), _c('div', {
    staticClass: "creditCard_btn"
  }, [_c('a', {
    staticClass: "default_button",
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoShare(_vm.business)
      }
    }
  }, [_vm._v("全部推荐")])])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-54824209", module.exports)
  }
}

/***/ }),

/***/ 2027:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1566);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("03b7df6a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-54824209!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recommendLoan.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-54824209!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recommendLoan.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 620:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2027)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1421),
  /* template */
  __webpack_require__(1879),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\recommendLoan.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] recommendLoan.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-54824209", Component.options)
  } else {
    hotAPI.reload("data-v-54824209", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});