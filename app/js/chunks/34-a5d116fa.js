webpackJsonp([34],{

/***/ 1461:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(755);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	data: function data() {
		return {
			menuArray: [{
				title: "活跃店铺",
				active: "",
				num: ""
			}, {
				title: "店铺交易",
				active: "",
				num: ""
			}, {
				title: "沉睡店铺",
				active: "",
				num: ""
			}],
			activeShop: [],
			shopTrans: [],
			sleepShop: [],
			allShopList: [{
				list: [],
				isNoData: false
			}, {
				list: [],
				isNoData: false
			}, {
				list: [],
				isNoData: false
			}],
			currentPage: 1,
			page: "",
			hasData: false,
			expressFee: "",
			isLoading: true,
			mySwiper: null,
			index: 0,
			defaultText: "",
			minHeight: 0
		};
	},

	filters: {
		keepTwoNum: function keepTwoNum(value) {
			value = Number(value);
			return value.toFixed(2);
		}
	},
	components: {
		searchBox: _searchBox2.default,
		freshToLoadmore: _freshToLoadmore2.default,
		noData: _noData2.default
	},
	beforeRouteEnter: function beforeRouteEnter(to, from, next) {
		if (!sessionStorage.askPositon || from.path == '/') {
			sessionStorage.askPositon = '';
			next();
		} else {
			next(function (vm) {
				if (vm && vm.$refs.my_scroller) {
					//通过vm实例访问this
					setTimeout(function () {
						vm.$refs.my_scroller.scrollTo(0, sessionStorage.askPositon, false);
					}, 20); //同步转异步操作
				}
			});
		}
	},
	beforeRouteLeave: function beforeRouteLeave(to, from, next) {
		//记录离开时的位置
		sessionStorage.askPositon = this.$refs.my_scroller && this.$refs.my_scroller.getPosition() && this.$refs.my_scroller.getPosition().top;
		next();
	},
	mounted: function mounted() {
		var _this = this;

		this.$refs.searchBox.inputValue = "";
		window.goBack = this.goBack;
		var type = this.$route.query.type || 0;
		this.index = type;
		if (type == 0) {
			this.getShop();
			this.getSleep();
		} else if (type == 1) {
			this.getActive();
			this.getSleep();
		} else {
			this.getActive();
			this.getShop();
		}
		this.getHeight();

		this.mySwiper = new Swiper('.swiper-container', {
			slidesPerView: "auto",
			autoplay: false, //可选选项，自动滑动
			loop: false,
			// autoHeight: true,
			resistanceRatio: 0,
			observer: true,
			observeParents: true, //修改swiper的父元素时，自动初始化swiper
			onSlideChangeEnd: function onSlideChangeEnd(swiper) {

				var index = swiper.activeIndex;
				_this.menuArray.map(function (el) {
					el.active = false;
				});
				_this.menuArray[index].active = true;
				_this.$refs.searchBox.inputValue = "";
				// var wrapper = document.getElementById("swiper-wrapper");
				// wrapper.style.height = 'auto';
				_this.index = index;
				_this.currentPage = 1;

				if (index == 0) {
					_this.$set(_this.allShopList[0], "isNoData", false);
					_this.changeList(_this.menuArray[index], index);
					//					this.getActive();
				} else if (index == 1) {
					_this.$set(_this.allShopList[1], "isNoData", false);
					_this.changeList(_this.menuArray[index], index);
					_this.getShop();
				} else {
					_this.$set(_this.allShopList[2], "isNoData", false);
					_this.changeList(_this.menuArray[index], index);
					_this.getSleep();
				}
			}
		});
		this.changeList(this.menuArray[type], type);
	},

	methods: {
		searchBtn: function searchBtn(search, done) {
			this.currentPage = 1;
			console.log(this.index);
			if (this.index == '0') {
				this.getActive();
			} else if (this.index == '1') {
				this.getShop();
			} else if (this.index == '2') {
				this.getSleep();
			}
		},
		getHeight: function getHeight() {
			var bodyHeight = document.documentElement.clientHeight;
			var scroller = this.$refs.scroller;
			var scrollerTop = scroller.getBoundingClientRect().top;
			scroller.style.height = bodyHeight - scrollerTop + "px";
			this.minHeight = bodyHeight - scrollerTop + "px";
		},
		changeList: function changeList(i, index) {
			console.log("222");
			this.index = index;
			this.$refs.searchBox.inputValue = "";
			this.currentPage = 1;
			this.defaultText = "";
			this.menuArray.map(function (el) {
				el.active = false;
			});
			i.active = true;
			if (this.index == '0') {
				this.allShopList[0].list = [];
				this.$set(this.allShopList[0], "isNoData", false);
				this.getActive();
			} else if (this.index == '1') {
				this.allShopList[1].list = [];
				this.$set(this.allShopList[1], "isNoData", false);
				this.getShop();
			} else if (this.index == '2') {
				this.allShopList[2].list = [];
				this.$set(this.allShopList[2], "isNoData", false);
				this.getSleep();
			}
			this.mySwiper.slideTo(index, 500, false);
		},
		callPhone: function callPhone(phone) {
			var phoneJson = {};
			phoneJson.phone = phone;
			console.log(phoneJson);
			if (common.isClient() == "ios") {
				window.webkit.messageHandlers.nativeCall.postMessage(JSON.stringify(phoneJson));
			} else {
				window.android.nativeCall(JSON.stringify(phoneJson));
			}
		},
		getActive: function getActive() {
			var _this2 = this;

			common.Ajax({
				url: _apiConfig2.default.KY_IP + "pos/shopInfo",
				data: {
					"page": this.currentPage,
					"isSleep": false,
					"userName": this.$refs.searchBox.inputValue
				},
				showLoading: false
			}, function (data) {
				_this2.$set(_this2.allShopList[0], "isNoData", false);
				//				this.allShopList[0].list = data.data.object;
				_this2.menuArray[0].num = data.data.totalResult ? data.data.totalResult : "0";
				if (data.data.totalResult == 0) {
					_this2.allShopList[0].list = [];
					_this2.$set(_this2.allShopList[0], "isNoData", true);
				} else if (data.data.object.length > 0) {
					if (_this2.currentPage == 1) {
						_this2.allShopList[0].list = [];
					}
					data.data.object.map(function (el) {
						_this2.allShopList[0].list.push(el);
					});

					_this2.currentPage++;
				} else {
					if (_this2.currentPage > 1) {
						common.toast({
							content: '无更多数据'
						});
					}
				}
			});
		},
		getShop: function getShop() {
			var _this3 = this;

			common.Ajax({
				url: _apiConfig2.default.KY_IP + "customer/getCustomerTransInfo",
				data: {
					"page": this.currentPage,
					customerName: this.$refs.searchBox.inputValue
				},
				showLoading: false
			}, function (data) {
				_this3.$set(_this3.allShopList[1], "isNoData", false);
				_this3.menuArray[1].num = data.data.totalResult ? data.data.totalResult : "0";
				if (data.data.totalResult == 0) {
					_this3.$set(_this3.allShopList[1], "isNoData", true);
					_this3.allShopList[1].list = [];
				} else if (data.data.object.length > 0) {
					if (_this3.currentPage == 1) {
						_this3.allShopList[1].list = [];
					}
					data.data.object.map(function (el) {
						_this3.allShopList[1].list.push(el);
					});

					_this3.currentPage++;
				} else {
					if (_this3.currentPage > 1) {
						common.toast({
							content: '无更多数据'
						});
					}
				}
			});
		},
		getSleep: function getSleep() {
			var _this4 = this;

			common.Ajax({
				url: _apiConfig2.default.KY_IP + "pos/shopInfo",
				data: {
					"page": this.currentPage,
					userName: this.$refs.searchBox.inputValue,
					"isSleep": true
				},
				showLoading: false
			}, function (data) {
				_this4.$set(_this4.allShopList[2], "isNoData", false);
				//				this.allShopList[2].list = data.data.object;
				_this4.menuArray[2].num = data.data.totalResult ? data.data.totalResult : "0";
				if (data.data.totalResult == 0) {
					_this4.$set(_this4.allShopList[2], "isNoData", true);
					_this4.allShopList[2].list = [];
				}
				if (data.data.object.length > 0) {
					if (_this4.currentPage == 1) {
						_this4.allShopList[2].list = [];
					}
					data.data.object.map(function (el) {
						_this4.allShopList[2].list.push(el);
					});

					_this4.currentPage++;
				} else {
					if (_this4.currentPage > 1) {
						common.toast({
							content: '无更多数据'
						});
					}
				}
			});
		},
		scrollBy: function scrollBy(ev) {
			console.log(ev);
		},
		infinite: function infinite() {
			var _this5 = this;

			setTimeout(function () {
				if (_this5.index == 0) {
					_this5.getActive();
				} else if (_this5.index == 1) {
					_this5.getShop();
				} else {
					_this5.getSleep();
				}
			}, 1000);
		},
		refresh: function refresh() {
			var _this6 = this;

			setTimeout(function () {
				_this6.currentPage = 1;
				if (_this6.index == 0) {
					_this6.getActive();
				} else if (_this6.index == 1) {
					_this6.getShop();
				} else {
					_this6.getSleep();
				}
				_this6.mySwiper.onResize();
			}, 1000);
		},
		loadmore: function loadmore() {
			var _this7 = this;

			setTimeout(function () {
				if (_this7.index == 0) {
					_this7.getActive();
				} else if (_this7.index == 1) {
					_this7.getShop();
				} else {
					_this7.getSleep();
				}
				_this7.mySwiper.onResize();
			}, 500);
		}
	}
};

/***/ }),

/***/ 1520:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1834:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('div', {
    staticClass: "queryHead"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.menuArray), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.title) + "(" + _vm._s(i.num) + ")")])
  }), 0), _vm._v(" "), _c('search-box', {
    ref: "searchBox",
    attrs: {
      "placeholderText": '请输入姓名搜索',
      "type": false
    },
    on: {
      "confirm": _vm.searchBtn
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "blank",
    staticStyle: {
      "height": "1.8rem"
    }
  }), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? (_vm.allShopList[0].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allShopList[0].list), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.customerName))]), _vm._v(" "), _c('a', {
      attrs: {
        "href": 'tel:' + i.phone
      },
      on: {
        "click": function($event) {
          return _vm.callPhone(i.phone)
        }
      }
    }, [_c('b', {
      staticClass: "phone"
    })])]), _vm._v(" "), _c('em', [_vm._v("设备名称：" + _vm._s(i.productName))]), _vm._v(" "), _c('em', [_vm._v("SN：" + _vm._s(i.posSN))]), _vm._v(" "), _c('span', [_vm._v("\n                                        " + _vm._s(i.createTime) + "\n                                    ")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allShopList[0].isNoData
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 1 ? (_vm.allShopList[1].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allShopList[1].list), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.customerName))]), _vm._v(" "), _c('i', [_vm._v("\n                                            交易成功，已返佣\n                                            "), _c('strong', [_vm._v("+" + _vm._s(_vm._f("keepTwoNum")(i.commissionAmount)))])])]), _vm._v(" "), _c('em', [_vm._v("交易金额：" + _vm._s(_vm._f("keepTwoNum")(i.amount)))]), _vm._v(" "), _c('em', [_vm._v("设备名称：" + _vm._s(i.productName))]), _vm._v(" "), _c('span', [_vm._v("\n                                        " + _vm._s(i.createTime) + "\n                                    ")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allShopList[1].isNoData
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 2 ? (_vm.allShopList[2].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allShopList[2].list), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.customerName))]), _vm._v(" "), (i.status == "SLEEP") ? _c('b', {
      staticClass: "noTrans"
    }, [_vm._v("30天未交易")]) : _vm._e(), _vm._v(" "), (i.status == "UNACTIVATED") ? _c('b', {
      staticClass: "noActive"
    }, [_vm._v("店铺未激活")]) : _vm._e(), _vm._v(" "), _c('a', {
      attrs: {
        "href": 'tel:' + i.phone
      },
      on: {
        "click": function($event) {
          return _vm.callPhone(i.phone)
        }
      }
    }, [_c('b', {
      staticClass: "phone"
    })])]), _vm._v(" "), _c('em', [_vm._v("设备名称：" + _vm._s(i.productName))]), _vm._v(" "), _c('em', [_vm._v("SN：" + _vm._s(i.posSN))]), _vm._v(" "), _c('span', [_vm._v("\n                                        " + _vm._s(i.createTime) + "\n                                    ")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allShopList[2].isNoData
    }
  })], 1)])])])], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-206fb433", module.exports)
  }
}

/***/ }),

/***/ 1981:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1520);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7f37794f", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-206fb433&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-206fb433&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 666:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1981)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1461),
  /* template */
  __webpack_require__(1834),
  /* scopeId */
  "data-v-206fb433",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\mall_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mall_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-206fb433", Component.options)
  } else {
    hotAPI.reload("data-v-206fb433", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 751:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(757)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(750),
  /* template */
  __webpack_require__(756),
  /* scopeId */
  "data-v-7cf3f18a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7cf3f18a", Component.options)
  } else {
    hotAPI.reload("data-v-7cf3f18a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7cf3f18a", module.exports)
  }
}

/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(751);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05a769e3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});