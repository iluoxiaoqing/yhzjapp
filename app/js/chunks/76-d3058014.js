webpackJsonp([76],{

/***/ 1425:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {};
    },

    props: {},
    components: {},
    mounted: function mounted() {
        this.commonAjax();
    },

    methods: {
        commonAjax: function commonAjax() {
            var _this = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "posters/tg",
                data: {
                    product: "YS_NOPOS"
                }
            }, function (data) {
                console.log(data);
                _this.showLoading = false;
                _this.thumbnail = data.data.shareTypes[0].shareChannel.thumbnail;
                _this.title = data.data.shareTypes[0].shareChannel.title;
                _this.jumpUrl = data.data.shareTypes[0].shareChannel.jumpUrl;
                _this.des = data.data.shareTypes[0].shareChannel.des;
            });
        },
        gotoShare: function gotoShare() {
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail,
                "jumpUrl": this.jumpUrl,
                "shareType": "url",
                "callbackName": ""

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
                console.log('jumpUrl:' + this.jumpUrl);
            }
        }
    }
};

/***/ }),

/***/ 1489:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.zdyqMain img {\n  width: 100%;\n}\n.zdyqMain .zdyqBottom {\n  width: 100%;\n  height: 1.4rem;\n}\n.zdyqMain .zdyqBottom .main {\n  padding: 0.25rem 0.25rem;\n  width: 100%;\n}\n.zdyqMain .zdyqBottom .main .logo {\n  width: 0.82rem;\n  height: 0.82rem;\n  float: left;\n  margin-right: 0.08rem;\n}\n.zdyqMain .zdyqBottom .main .logo img {\n  display: inherit;\n}\n.zdyqMain .zdyqBottom .main p {\n  font-size: 0.32rem;\n  color: #3D4A5B;\n}\n.zdyqMain .zdyqBottom .main span {\n  opacity: 0.6;\n  font-size: 0.2rem;\n  color: #3D4A5B;\n  float: left;\n}\n.zdyqMain .zdyqBottom .main button {\n  width: 1.98rem;\n  height: 0.84rem;\n  font-size: 0.28rem;\n  color: #FFFFFF;\n  text-align: center;\n  line-height: 0.84rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  float: right;\n  margin-top: -0.5rem;\n  margin-right: 0.5rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1690:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ylsf1.png?v=7255773c";

/***/ }),

/***/ 1691:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ylsf2.png?v=faf732e0";

/***/ }),

/***/ 1692:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ylsf3.png?v=40d7a359";

/***/ }),

/***/ 1693:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ysLogo.png?v=f724464d";

/***/ }),

/***/ 1803:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "zdyqMain"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1690),
      "alt": ""
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1691),
      "alt": ""
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1692),
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "zdyqBottom"
  }, [_c('div', {
    staticClass: "main"
  }, [_vm._m(0), _vm._v(" "), _c('p', [_vm._v(" 无机具大额闪付")]), _vm._v(" "), _c('span', [_vm._v("费率0.38%，分润万6起，最高奖励20元")]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.gotoShare();
      }
    }
  }, [_vm._v("分享到微信")])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1693),
      "alt": ""
    }
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0468cc40", module.exports)
  }
}

/***/ }),

/***/ 1950:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1489);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("6f9a4e0b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0468cc40!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activityYlsf.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0468cc40!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./activityYlsf.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 628:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1950)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1425),
  /* template */
  __webpack_require__(1803),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\activityYlsf.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] activityYlsf.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0468cc40", Component.options)
  } else {
    hotAPI.reload("data-v-0468cc40", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});