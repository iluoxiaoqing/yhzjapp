webpackJsonp([106],{

/***/ 1422:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            text: "",
            isJump: ""
        };
    },
    mounted: function mounted() {
        this.getReg();
    },

    methods: {
        getReg: function getReg() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cx/autoRegister",
                data: {},
                showLoading: true
            }, function (data) {
                console.log(data.data);
                _this.text = data.data.depict;
                _this.isJump = data.data.appDownloadUrl;
            });
        },
        goDown: function goDown(url) {
            var url1 = url;
            if (common.isClient() == "ios") {
                try {
                    window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify({ url: url1 }));
                } catch (error) {
                    window.location.href = url1;
                }
            } else {
                try {
                    window.android.nativeOpenBrowser(JSON.stringify({ url: url1 }));
                } catch (error) {
                    window.location.href = url1;
                }
            }
        }
    }
};

/***/ }),

/***/ 1505:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1819:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "registerEd"
  }, [_c('p', [_vm._v(_vm._s(_vm.text))]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goDown(_vm.isJump);
      }
    }
  }, [_vm._v("下载")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0fb6b63e", module.exports)
  }
}

/***/ }),

/***/ 1966:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1505);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("01614b01", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0fb6b63e!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0fb6b63e!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./regZfy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 623:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1966)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1422),
  /* template */
  __webpack_require__(1819),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\regZfy\\cxDownMall.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] cxDownMall.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0fb6b63e", Component.options)
  } else {
    hotAPI.reload("data-v-0fb6b63e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});