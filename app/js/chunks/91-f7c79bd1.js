webpackJsonp([91],{

/***/ 1445:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var errorMsg = "";

var checkName = function checkName(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.name.empty;
        return false;
    } else {

        var reg = _base2.default.name.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.name.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};

//验证手机号
var checkMobile = function checkMobile(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.phoneNumber.empty;
        return false;
    } else {

        var reg = _base2.default.phoneNumber.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.phoneNumber.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};
exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            name: '',
            custPhone: '',
            phone: "",
            city: '',
            area: '',
            province: '',
            address: ''
        };
    },
    mounted: function mounted() {
        // this.getDetail();
    },

    methods: {
        goLq: function goLq() {
            if (!this.$route.query.userNo) {
                common.toast({
                    "content": "无法获得商户编号"
                });
                return;
            }
            if (!this.name) {
                common.toast({
                    "content": "请输入友刷商户的真实姓名"
                });
                return;
            }
            if (!checkName(this.name)) {
                common.toast({
                    "content": "请正确输入友刷商户的真实姓名"
                });
                return;
            }
            if (!this.custPhone) {
                common.toast({
                    "content": "请输入友刷商户的手机号"
                });
                return;
            }
            if (!checkMobile(common.trim(this.custPhone))) {
                common.toast({
                    "content": "请正确输入友刷商户的手机号"
                });
                return;
            }

            if (!this.province) {
                common.toast({
                    "content": "请输入所在省份"
                });
                return;
            }
            if (!this.city) {
                common.toast({
                    "content": "请输入所在市"
                });
                return;
            }
            if (!this.area) {
                common.toast({
                    "content": "请输入所在区"
                });
                return;
            }
            if (!this.address) {
                common.toast({
                    "content": "请输入您的详细地址"
                });
                return;
            }
            if (!this.phone) {
                common.toast({
                    "content": "请输入您的收货电话"
                });
                return;
            }
            if (!checkMobile(common.trim(this.phone))) {
                common.toast({
                    "content": "请输入您的收货电话"
                });
                return;
            }
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "youShuaReplacement/apply",
                data: {
                    userNo: this.$route.query.userNo, //从哪里来的
                    customerName: this.name,
                    customerPhone: this.custPhone,
                    phone: this.phone,
                    province: this.province,
                    city: this.city,
                    area: this.area,
                    address: this.address
                },
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                common.toast({
                    "content": data.data
                });
                // this.activityTime = data.data.activityTime;
                // this.activityIntroduce = data.data.prodIntrod;
                // this.activityRules = data.data.activityRules;
            });
        },
        getDetail: function getDetail() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "ptx/landingPage",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                _this.activityTime = data.data.activityTime;
                _this.activityIntroduce = data.data.prodIntrod;
                _this.activityRules = data.data.activityRules;
            });
        },
        gotoShare: function gotoShare(j) {
            this.$router.push({
                "path": "share",
                "query": {
                    "product": 'PING_TOU_XIANG_NOPOS',
                    "productName": '【钱宝】招钱宝贝',
                    "channel": "qianbaoIndex"
                }
            });
            common.youmeng("推荐贷款", "点击钱宝首页");
        }
    }
};

/***/ }),

/***/ 1576:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1889:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "free_main"
  }, [_c('div', {
    staticClass: "main_img"
  }), _vm._v(" "), _c('div', {
    staticClass: "main_form"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\r\n                        填写邮寄地址立即领取\r\n                ")]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(0), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入友刷商户的真实姓名"
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(1), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.custPhone),
      expression: "custPhone"
    }],
    attrs: {
      "maxlength": "11",
      "type": "text",
      "placeholder": "请输入友刷商户的手机号"
    },
    domProps: {
      "value": (_vm.custPhone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.custPhone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(2), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.province),
      expression: "province"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入所在省份"
    },
    domProps: {
      "value": (_vm.province)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.province = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.city),
      expression: "city"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入所在市"
    },
    domProps: {
      "value": (_vm.city)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.city = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(4), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.area),
      expression: "area"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入所在区"
    },
    domProps: {
      "value": (_vm.area)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.area = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(5), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.address),
      expression: "address"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入您的详细地址"
    },
    domProps: {
      "value": (_vm.address)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.address = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form"
  }, [_vm._m(6), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }],
    staticStyle: {
      "margin-bottom": "0.56rem"
    },
    attrs: {
      "maxlength": "11",
      "type": "text",
      "placeholder": "请输入收货电话"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goLq()
      }
    }
  }, [_vm._v("免费领取")]), _vm._v(" "), _c('i', [_vm._v("预计3个工作日内发货")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("友刷商户姓名：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("友刷注册手机号：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("所在省份：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("所在市：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("所在区：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("详细地址：")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_vm._v("*")]), _vm._v("收货电话：")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-626dec10", module.exports)
  }
}

/***/ }),

/***/ 2037:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1576);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2e716618", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-626dec10&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./freeChange.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-626dec10&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./freeChange.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 680:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2037)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1445),
  /* template */
  __webpack_require__(1889),
  /* scopeId */
  "data-v-626dec10",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\yszhsft.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] yszhsft.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-626dec10", Component.options)
  } else {
    hotAPI.reload("data-v-626dec10", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});