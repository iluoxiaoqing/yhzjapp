webpackJsonp([114],{

/***/ 1463:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            allShareCode: {},
            shopGroups: [],
            showAd: "1",
            context: '',
            title: "",
            dataList: "",
            feature: ""
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        this.getList();
        //        this.getShowAd();
    },

    methods: {
        share: function share() {
            console.log("分享", "分享");
            this.$router.push({
                "path": "shareMerchantReward",
                "query": {
                    "channel": "shareMerchantReward"
                }
            });
        },
        close: function close() {
            this.showAd = !this.showAd;
        },
        allShare: function allShare(j) {
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j,
                    "productName": "全部推荐",
                    "channel": "shareMerchantReward"
                }
            });
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/posCustomerRewardExplain",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this.dataList = data.data;
                _this.feature = _this.dataList.feature;
                console.log("dddd===", _this.feature);
                // this.allShareCode = data.data.shareCode; //全部分享code
                // this.shopGroups = data.data.groups;
                console.log("dataList", _this.dataList);
            });
        },
        apply: function apply(goodsType) {
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "jiFenDui/createOrder",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                window.location.href = data.data;
            });
        },
        go: function go() {
            window.location.href = _apiConfig2.default.WEB_URL + 'shjl?j=';
        },
        gotoMall: function gotoMall(i, j) {
            console.log("====", i);
            common.setCookie("proCode", i);
            console.log("url===", _apiConfig2.default.WEB_URL + 'mall?j=' + j);
            window.location.href = _apiConfig2.default.WEB_URL + 'mall?j=' + j;
            window.location.reload();
        }
    }
};

/***/ }),

/***/ 1581:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1894:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mallShop"
  }, [_c('div', {
    staticClass: "main"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n\t\t\t\t" + _vm._s(_vm.dataList.title) + "\n\t\t\t")]), _vm._v(" "), _c('div', {
    staticClass: "shopMain"
  }, [_c('div', {
    staticClass: "shopTop"
  }, [_c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.dataList.logo
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('div', {
    staticClass: "titleLeft"
  }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(_vm.dataList.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
    staticClass: "titleRight"
  }), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.dataList.company))]), _vm._v(" "), _c('i', [_vm._v(_vm._s(_vm.feature.desc))]), _vm._v(" "), _c('div', {
    staticClass: "aaaa"
  }, [_c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.share()
      }
    }
  }, [_vm._v(_vm._s(_vm.dataList.shareBtnText))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "shopBottom"
  }, [_c('div', {
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(_vm.dataList.buyBtinText) + "\n\t\t\t\t\t")])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-676c19e0", module.exports)
  }
}

/***/ }),

/***/ 2042:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1581);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("79419028", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-676c19e0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-676c19e0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 668:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2042)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1463),
  /* template */
  __webpack_require__(1894),
  /* scopeId */
  "data-v-676c19e0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\merchantReward.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] merchantReward.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-676c19e0", Component.options)
  } else {
    hotAPI.reload("data-v-676c19e0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});