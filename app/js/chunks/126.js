webpackJsonp([126],{

/***/ 1400:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

common.youmeng("政策解读", "政策解读更多");

exports.default = {
    data: function data() {
        return {
            menuList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            caseList: [],
            currentPage: 1,
            id: ""
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        window.goBack = this.goBack;
        this.nativeWebviewDisabledBounces();
        this.getHeight();
        this.getLiist();
    },

    methods: {
        nativeWebviewDisabledBounces: function nativeWebviewDisabledBounces() {
            if (common.isClient() == "ios") {
                try {
                    window.webkit.messageHandlers.nativeWebviewDisabledBounces.postMessage('');
                } catch (err) {
                    console.log("err", "出错了");
                }
            } else {}
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        loadmore: function loadmore() {
            this.getCaseList(this.id);
        },
        refresh: function refresh() {
            this.getCaseList(this.id);
        },
        changeGoods: function changeGoods(i, index) {
            this.caseList = [];
            this.menuList.map(function (el) {
                el.active = false;
                el.page = 1;
                el.caseList = [];
            });
            this.menuList[index].active = true;
            i.active = true;
            this.id = this.menuList[index].id;
            this.getCaseList(this.menuList[index].id);
            common.youmeng("推手案例", "推手案例更多");
        },
        getLiist: function getLiist() {
            var _this = this;

            console.log("this.menuList", this.menuList);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/group",
                data: {},
                showLoading: false
            }, function (data) {
                _this.menuList = data.data;
                _this.menuList[0].active = true;
                _this.menuList[0].page = 1;
                _this.index = 0;
                _this.menuList[0].caseList = [];
                _this.id = _this.menuList[0].id;
                _this.getCaseList(_this.id);
            });
        },
        getCaseList: function getCaseList(u) {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/caseList",
                data: {
                    "id": u,
                    "page": this.menuList[this.index].page
                },
                showLoading: false
            }, function (data) {
                var curPage = _this2.menuList[_this2.index].page;
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        _this2.$set(el, "show", false);
                        _this2.caseList.push(el);
                    });
                    curPage++;
                    _this2.$set(_this2.menuList[_this2.index], "page", curPage);
                    _this2.$set(_this2.menuList[_this2.index], "isNoData", false);
                } else {
                    if (curPage == 1) {
                        _this2.caseList = data.data;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1575:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-60f9c53b] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-60f9c53b] {\n  background: #fff;\n}\n.tips_success[data-v-60f9c53b] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-60f9c53b] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-60f9c53b] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-60f9c53b],\n.fade-leave-active[data-v-60f9c53b] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-60f9c53b],\n.fade-leave-to[data-v-60f9c53b] {\n  opacity: 0;\n}\n.default_button[data-v-60f9c53b] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-60f9c53b] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-60f9c53b] {\n  position: relative;\n}\n.loading-tips[data-v-60f9c53b] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.mall[data-v-60f9c53b] {\n  width: 100%;\n  overflow: hidden;\n}\n.mall .mall_wrap[data-v-60f9c53b] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  overflow: hidden;\n}\n.mall .mall_content[data-v-60f9c53b] {\n  width: 100%;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  background: #fff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.mall .mall_content .mall_left[data-v-60f9c53b] {\n  background: #f4f5fb;\n  height: 100%;\n  width: 1.8rem;\n  overflow: hidden;\n}\n.mall .mall_content .mall_left span[data-v-60f9c53b] {\n  width: 100%;\n  height: 1.5rem;\n  font-size: 0.24rem;\n  color: rgba(61, 74, 91, 0.6);\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  cursor: pointer;\n  display: inherit;\n  text-align: center;\n}\n.mall .mall_content .mall_left span.active[data-v-60f9c53b] {\n  color: #3d4a5b;\n  background: #fff;\n  font-weight: 700;\n}\n.mall .mall_content .mall_left span img[data-v-60f9c53b] {\n  width: 0.68rem;\n  height: 0.68rem;\n  float: left;\n  opacity: 1;\n  margin: 0 0.41rem 0.12rem 0.41rem;\n}\n.mall .mall_content .mall_right[data-v-60f9c53b] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  padding-left: 0.48rem;\n  height: 100%;\n  overflow: auto;\n  -webkit-overflow-scrolling: touch;\n}\n.mall .mall_content .mall_right .item[data-v-60f9c53b] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 0.3rem 0;\n  position: relative;\n}\n.mall .mall_content .mall_right .item[data-v-60f9c53b]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.mall .mall_content .mall_right .item[data-v-60f9c53b]:last-child:after {\n  content: '';\n  background: none;\n}\n.mall .mall_content .mall_right .item[data-v-60f9c53b]:last-child:after {\n  content: \"\";\n  background: none;\n}\n.mall .mall_content .mall_right .item .goodsImg[data-v-60f9c53b] {\n  width: 1.84rem;\n  height: 1.84rem;\n  background: #f4f5fb;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n}\n.mall .mall_content .mall_right .item .goodsImg span[data-v-60f9c53b] {\n  background: #FFB740;\n  width: .6rem;\n  height: .28rem;\n  position: absolute;\n  left: 0;\n  top: 0;\n  color: #fff;\n  font-size: .24rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .mall_content .mall_right .item .goodsImg span i[data-v-60f9c53b] {\n  -webkit-transform: scale(0.8);\n      -ms-transform: scale(0.8);\n          transform: scale(0.8);\n  display: block;\n}\n.mall .mall_content .mall_right .item .goodsImg img[data-v-60f9c53b] {\n  width: 1.1rem;\n  height: 1.26rem;\n  display: block;\n}\n.mall .mall_content .mall_right .item .goodsDeatil[data-v-60f9c53b] {\n  width: 4.92rem;\n  height: auto;\n  position: relative;\n}\n.mall .mall_content .mall_right .item .goodsDeatil button[data-v-60f9c53b] {\n  width: 2.16rem;\n  height: 0.64rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  font-size: 0.28rem;\n  color: #ffffff;\n  text-align: center;\n  float: left;\n  margin-top: 0.06rem;\n}\n.mall .mall_content .mall_right .item .goodsDeatil p[data-v-60f9c53b] {\n  color: #3D4A5B;\n  font-size: 0.28rem;\n  opacity: 0.6;\n  text-align: justify;\n  line-height: .4rem;\n  overflow: hidden;\n  -webkit-line-clamp: 2;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n}\n.mall .buyList_mask[data-v-60f9c53b] {\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.65);\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 10;\n}\n@-webkit-keyframes addCount {\n0% {\n    -webkit-transform: scale(1) translate(0, 0);\n}\n50% {\n    -webkit-transform: scale(1.3) translate(0, -15px);\n}\n100% {\n    -webkit-transform: scale(1) translate(0, 0);\n}\n}\n@keyframes addCount {\n0% {\n    -webkit-transform: scale(1) translate(0, 0);\n            transform: scale(1) translate(0, 0);\n}\n50% {\n    -webkit-transform: scale(1.3) translate(0, -15px);\n            transform: scale(1.3) translate(0, -15px);\n}\n100% {\n    -webkit-transform: scale(1) translate(0, 0);\n            transform: scale(1) translate(0, 0);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1888:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mall"
  }, [_c('div', {
    ref: "mall_wrap",
    staticClass: "mall_wrap"
  }, [_c('div', {
    staticClass: "mall_content"
  }, [_c('div', {
    staticClass: "mall_left"
  }, _vm._l((_vm.menuList), function(i, index) {
    return _c('span', {
      key: index,
      class: {
        'active': i.active
      },
      on: {
        "click": function($event) {
          return _vm.changeGoods(i, index)
        }
      }
    }, [_c('img', {
      attrs: {
        "src": './image/icon_' + index + '.png',
        "alt": ""
      }
    }), _vm._v("\n\t\t\t\t\t" + _vm._s(i.name) + "\n\t\t\t\t")])
  }), 0), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "mall_right"
  }, _vm._l((_vm.caseList), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('router-link', {
      attrs: {
        "to": {
          name: 'caseDetail',
          query: {
            id: j.id,
            title: j.title
          }
        }
      }
    }, [_c('div', {
      staticClass: "goodsDeatil"
    }, [_c('p', [_vm._v(_vm._s(j.title))])])])], 1)
  }), 0)])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-60f9c53b", module.exports)
  }
}

/***/ }),

/***/ 2036:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1575);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("712f41ba", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-60f9c53b&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseMore.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-60f9c53b&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseMore.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 596:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2036)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1400),
  /* template */
  __webpack_require__(1888),
  /* scopeId */
  "data-v-60f9c53b",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\handPushCase\\caseMore.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] caseMore.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-60f9c53b", Component.options)
  } else {
    hotAPI.reload("data-v-60f9c53b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});