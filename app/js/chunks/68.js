webpackJsonp([68],{

/***/ 1321:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1399:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            detail: [],
            showDia: false,
            title: "",
            des: "",
            thumbnail: "",
            u: "",
            showShare: false,
            show1: false
        };
    },
    mounted: function mounted() {
        this.getDetail();
        window.shareSucces = this.shareSucces;
    },

    methods: {
        Shareshow: function Shareshow() {
            this.showDia = true;
            this.show1 = true;
        },
        getDetail: function getDetail() {
            var _this = this;

            var u = this.$route.query.u;
            if (u == undefined) {
                var _u = "";
                this.showShare = true;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "news/detail",
                    data: {
                        u: _u,
                        news: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.title = data.data.title;
                    _this.des = data.data.desc;
                    _this.u = data.data.u;
                    _this.thumbnail = data.data.thumbnail;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            } else {
                var _u2 = this.$route.query.u;
                this.showShare = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "news/detail",
                    data: {
                        u: _u2,
                        news: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.title = data.data.title;
                    _this.des = data.data.desc;
                    _this.u = data.data.u;
                    _this.thumbnail = data.data.thumbnail;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            }
        },
        wechatSession: function wechatSession() {
            var thumbnail = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail;
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            var thumbnail = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail;
            var shareJson = {
                "type": "wechatTimeline",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        shareSucces: function shareSucces() {
            this.showDia = false;
            this.show1 = false;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },
        showBottom: function showBottom() {
            this.showDia = true;
        }
    }
};

/***/ }),

/***/ 1539:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.contain {\n  /*弹窗*/\n}\n.contain .detail_main {\n  width: 92%;\n  margin: 0 4%;\n  overflow: hidden;\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n}\n.contain .detail_main h1 {\n  font-size: 0.4rem;\n  color: #3D4A5B;\n  margin: 0.32rem 0 0;\n}\n.contain .detail_main .materialList {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.contain .detail_main .materialList .list {\n  width: 100%;\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 0.48rem 0 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .detail_main .materialList .list .listImg {\n  width: 0.8rem;\n  height: 0.8rem;\n}\n.contain .detail_main .materialList .list .listImg img {\n  width: 100%;\n  height: 100%;\n  display: block;\n  border-radius: 0.4rem;\n  margin: 0 0 ;\n}\n.contain .detail_main .materialList .list .listDeatil {\n  width: 4.7rem;\n  height: 0.8rem;\n  margin-left: 0.16rem;\n  position: relative;\n}\n.contain .detail_main .materialList .list .listDeatil b {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  line-height: 0.44rem;\n}\n.contain .detail_main .materialList .list .listDeatil .time {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  line-height: 0.34rem;\n}\n.contain .detail_main .materialList .list .listDeatil .time i {\n  color: rgba(61, 74, 91, 0.4);\n  font-size: 0.24rem;\n  display: block;\n}\n.contain .detail_main .content .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content p {\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  line-height: 0.4rem;\n}\n.contain .detail_main .content p .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content p .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content p .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content p .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content p .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content img {\n  width: 100%;\n  height: auto;\n  margin: 0.48rem 0 0;\n}\n.contain .detail_main .content h2 {\n  font-family: PingFangSC-Medium;\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  margin: 0.48rem 0 0.16rem;\n}\n.contain .detail_main .erweima img {\n  width: 37%;\n  margin: 0 31.5%;\n}\n.contain .detail_main .erweima_text {\n  text-align: center;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  margin: 0.16rem 0 0.8rem;\n}\n.contain .detail_main .wx_erweima {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.contain .detail_main .wx_erweima .list {\n  width: 38%;\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 0.96rem 0 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .detail_main .wx_erweima .list .listImg {\n  width: 0.96rem;\n  height: 0.96rem;\n}\n.contain .detail_main .wx_erweima .list .listImg img {\n  width: 100%;\n  height: 100%;\n  display: block;\n  border-radius: 0.48rem;\n  margin: 0 0 ;\n}\n.contain .detail_main .wx_erweima .list .listDeatil {\n  width: 1.44rem;\n  height: 0.96rem;\n  margin-left: 0.16rem;\n  position: relative;\n}\n.contain .detail_main .wx_erweima .list .listDeatil b {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  line-height: 0.44rem;\n}\n.contain .detail_main .wx_erweima .list .listDeatil .time {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  line-height: 0.34rem;\n}\n.contain .detail_main .wx_erweima .list .listDeatil .time i {\n  color: rgba(61, 74, 91, 0.8);\n  font-size: 0.24rem;\n  display: block;\n  white-space: nowrap;\n}\n.contain .detail_main .share {\n  position: fixed;\n  bottom: 1.16rem;\n  right: 4%;\n}\n.contain .detail_main .share img {\n  width: 1.24rem;\n  height: 1.24rem;\n}\n.contain .dialog {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.contain .dialog img {\n  width: 5.6rem;\n  height: 2.17rem;\n}\n.contain .bottomBar {\n  z-index: 10000;\n  width: 100%;\n  height: 3.9rem;\n  background: #FAFBFF;\n  border-top-left-radius: 0.08rem;\n  border-top-right-radius: 0.08rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n}\n.contain .bottomBar .bottomBar_line {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.6rem;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 0.6rem;\n}\n.contain .bottomBar .bottomBar_line em {\n  margin: 0 0.2rem;\n}\n.contain .bottomBar .bottomBar_line span {\n  width: 1.0rem;\n  height: 1px;\n  display: block;\n  background: #C4C7CC;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.contain .bottomBar .bottomBar_content {\n  width: 92%;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .bottomBar .bottomBar_content p {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n}\n.contain .bottomBar .bottomBar_content p img {\n  width: 1.0rem;\n  height: 1.0rem;\n  display: block;\n  margin: 0 auto 0.2rem;\n}\n.contain .bottomBar .bottomBar_content p span {\n  display: block;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  font-weight: 700;\n}\n", ""]);

// exports


/***/ }),

/***/ 1641:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share.png?v=8ae7e7dd";

/***/ }),

/***/ 1821:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("分享至")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatSession()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(739)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatTimeline()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(738)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('h1', [_vm._v(_vm._s(_vm.detail.title))]), _vm._v(" "), _c('div', {
    staticClass: "materialList"
  }, [_c('div', {
    staticClass: "list"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v("逍遥推手")]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.time))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "content",
    domProps: {
      "innerHTML": _vm._s(_vm.detail.content)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "wx_erweima"
  }, [_c('div', {
    staticClass: "list"
  }, [_c('div', {
    staticClass: "listImg"
  }, [(_vm.detail.touxiang == '' || _vm.detail.touxiang == undefined) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1321)
    }
  }) : _c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.detail.touxiang
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.userName))]), _vm._v(" "), _vm._m(1)])])]), _vm._v(" "), _c('div', {
    staticClass: "erweima"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'news/shareQrCode?productCode=' + _vm.detail.pro + '&u=' + _vm.detail.u
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "erweima_text"
  }, [_vm._v("\n            " + _vm._s(_vm.detail.shareLang) + "\n        ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showShare),
      expression: "showShare"
    }],
    staticClass: "share"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1641),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.Shareshow()
      }
    }
  })])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1321)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v("您的专属客服")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-764278d7", module.exports)
  }
}

/***/ }),

/***/ 1955:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1539);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("f5485190", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-764278d7!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./material_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-764278d7!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./material_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 621:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1955)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1399),
  /* template */
  __webpack_require__(1821),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\share\\material_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] material_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-764278d7", Component.options)
  } else {
    hotAPI.reload("data-v-764278d7", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 738:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 739:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ })

});