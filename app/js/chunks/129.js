webpackJsonp([129],{

/***/ 1377:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _methods;

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = {
    data: function data() {
        return {
            currentGroup: "1",
            currentGroupWd: "1",
            productName: this.$route.query.productName, //客户端放到cookie中
            productCode: this.$route.query.productCode, //客户端放到cookie中
            type: "PERSONAL",
            timeType: "DAY",
            page: "1",
            detailArr: [],
            // detailArr: [{
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }],
            isMore: "init", //默认是init 加载完毕没有数据为 'nodata'
            isLoading: false,
            total: "",
            currentPage: 1,
            isNoData: false
        };
    },

    components: {
        // scrollView,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
        // rebateList,
    },
    mounted: function mounted() {
        // TODO
        if (this.$route.query.from == 'push') {
            document.title = "推手数据详情-" + this.$route.query.productName;
        } else {
            document.title = "交易额统计-" + this.$route.query.productName;
        }

        var bodyHeight = document.documentElement.clientHeight;
        var scroller = document.getElementById("pull-wrapper");
        var scrollerTop = scroller.getBoundingClientRect().top;
        scroller.style.height = bodyHeight - scrollerTop + "px";
        this.getList();
    },

    methods: (_methods = {
        infinite: function infinite() {
            this.getList();
        },
        refresh: function refresh() {
            this.currentPage = 1;
            this.detailArr = [];
            this.getList();
        },
        changeTab: function changeTab(idx) {
            if (idx == this.currentGroup) {
                return;
            }
            this.currentPage = 1;
            this.detailArr = [];
            this.currentGroupWd = 1;
            this.timeType = "DAY";
            this.currentGroup = idx;
            if (this.currentGroup == 1) {
                this.type = 'PERSONAL';
            } else {
                this.type = 'TEAM';
            }
            this.getList();
        },
        changeTabWd: function changeTabWd(id) {
            if (id == this.currentGroupWd) {
                return;
            }
            this.currentPage = 1;
            this.detailArr = [];
            this.currentGroupWd = id;
            if (this.currentGroupWd == 1) {
                this.timeType = 'DAY';
            } else {
                this.timeType = 'MONTH';
            }
            this.getList();
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "achHomepage/transInfo",
                "type": "post",
                "data": {
                    "type": this.type,
                    "productCode": this.$route.query.productCode,
                    "timeType": this.timeType,
                    "page": this.currentPage,
                    "countUserNo": this.$route.query.countUserNo || ''
                }
            }, function (data) {
                console.log('数据列表', data);
                _this.total = data.data.totalTrans;
                if (data.data.subInfo.object.length > 0) {
                    if (_this.currentPage == 1) {
                        _this.detailArr = [];
                    }
                    data.data.subInfo.object.map(function (el) {
                        _this.$set(el, "show", true);
                        _this.detailArr.push(el);
                    });

                    _this.isNoData = false;
                    _this.currentPage++;
                } else {
                    if (_this.currentPage == 1) {
                        _this.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
            });
        }
    }, _defineProperty(_methods, "refresh", function refresh(loaded) {
        this.currentPage = 1;
        this.detailArr = [];
        this.getList();
    }), _defineProperty(_methods, "loadmore", function loadmore(loaded) {
        this.getList();
    }), _methods)

};

/***/ }),

/***/ 1608:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-aba55904] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-aba55904] {\n  background: #fff;\n}\n.tips_success[data-v-aba55904] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-aba55904] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-aba55904] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-aba55904],\n.fade-leave-active[data-v-aba55904] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-aba55904],\n.fade-leave-to[data-v-aba55904] {\n  opacity: 0;\n}\n.default_button[data-v-aba55904] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-aba55904] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-aba55904] {\n  position: relative;\n}\n.loading-tips[data-v-aba55904] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.trade_main[data-v-aba55904] {\n  width: 92%;\n  margin: 0 4%;\n}\n.trade_main .detail-con[data-v-aba55904] {\n  margin-top: .34rem;\n}\n.trade_main .detail-con .tab[data-v-aba55904] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.trade_main .detail-con .tab li[data-v-aba55904] {\n  padding-bottom: .2rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  -ms-grid-column-align: flex-start;\n      justify-items: flex-start;\n  font-family: PingFangSC-Regular;\n  font-size: .32rem;\n  color: rgba(63, 63, 80, 0.6);\n  text-align: center;\n  position: relative;\n}\n.trade_main .detail-con .tab li[data-v-aba55904]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #BFC5D1;\n  width: 100%;\n  height: 0px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  opacity: 0.6;\n}\n.trade_main .detail-con .tab li.active[data-v-aba55904] {\n  font-weight: bold;\n  color: #3f83ff;\n}\n.trade_main .detail-con .tab li.active[data-v-aba55904]:after {\n  background: #3f83ff;\n  opacity: 1;\n  height: 6px;\n  width: 0.6rem;\n  margin-left: 42%;\n  border-radius: 2px;\n}\n.trade_main .detail-con .volume_main[data-v-aba55904] {\n  width: 100%;\n  height: 2.52rem;\n  background: #3f83ff;\n  box-shadow: 0px 2px 16px 0px rgba(0, 0, 0, 0.08);\n  border-radius: 6px;\n  margin-top: 0.32rem;\n  display: inline-block;\n}\n.trade_main .detail-con .volume_main .total[data-v-aba55904] {\n  text-align: center;\n  font-size: 0.24rem;\n  font-weight: 400;\n  color: #ffffff;\n  margin-top: 0.7rem;\n  display: inline-block;\n  width: 100%;\n  float: left;\n}\n.trade_main .detail-con .volume_main .num[data-v-aba55904] {\n  display: inline-block;\n  width: 100%;\n  float: left;\n  text-align: center;\n  font-size: 0.56rem;\n  color: #ffffff;\n  font-weight: bold;\n}\n.trade_main .detail-con .trade_detail[data-v-aba55904] {\n  width: 100%;\n}\n.trade_main .detail-con .trade_detail .tabTwo[data-v-aba55904] {\n  width: 28%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 0.4rem;\n  margin-left: 71%;\n  margin-bottom: 0.32rem;\n}\n.trade_main .detail-con .trade_detail .tabTwo li[data-v-aba55904] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  -ms-grid-column-align: flex-start;\n      justify-items: flex-start;\n  line-height: 0.4rem;\n  font-size: .24rem;\n  color: #3f3f50;\n  position: relative;\n  background: #ffffff;\n  border: 1px solid #ffb62b;\n  box-sizing: border-box;\n  -moz-box-sizing: border-box;\n  /* Firefox */\n  -webkit-box-sizing: border-box;\n  /* Safari */\n  text-align: center;\n  height: 0.4rem;\n}\n.trade_main .detail-con .trade_detail .tabTwo li.active[data-v-aba55904] {\n  font-weight: bold;\n  background: #ffb62b;\n}\n.trade_main .detail-con .trade_detail .detail-list .item[data-v-aba55904] {\n  margin-bottom: .32rem;\n  background: #f7f8fa;\n  border-radius: 4px;\n}\n.trade_main .detail-con .trade_detail .detail-list .item .time[data-v-aba55904] {\n  width: 100%;\n  height: 0.68rem;\n  background: #e5ebff;\n  font-size: .28rem;\n  font-weight: 500;\n  color: #3f83ff;\n  line-height: .68rem;\n  border-top-left-radius: 4px;\n  border-top-right-radius: 4px;\n}\n.trade_main .detail-con .trade_detail .detail-list .item .time span[data-v-aba55904] {\n  padding-left: 0.3rem;\n}\n.trade_main .detail-con .trade_detail .detail-list .item ul[data-v-aba55904] {\n  margin-top: .08rem;\n  padding: 0 .3rem;\n  border-radius: .08rem;\n}\n.trade_main .detail-con .trade_detail .detail-list .item ul li[data-v-aba55904] {\n  line-height: 0.36rem;\n  padding-bottom: 0.08rem;\n  background-position: right;\n  background-size: .17rem;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.trade_main .detail-con .trade_detail .detail-list .item ul li p[data-v-aba55904] {\n  font-family: PingFangSC-Regular;\n  font-size: .24rem;\n  color: rgba(63, 63, 80, 0.6);\n}\n.trade_main .detail-con .trade_detail .detail-list .item ul li p[data-v-aba55904]:last-child {\n  padding-right: .3rem;\n  font-family: PingFangSC-Semibold;\n  font-size: .24rem;\n  color: #3f3f50;\n}\n", ""]);

// exports


/***/ }),

/***/ 1921:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "trade_main"
  }, [_c('div', {
    staticClass: "detail-con"
  }, [_c('ul', {
    staticClass: "tab",
    attrs: {
      "id": "tab"
    }
  }, [_c('li', {
    class: {
      'active': _vm.currentGroup == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(1)
      }
    }
  }, [_vm._v("个人交易")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroup == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(2)
      }
    }
  }, [_vm._v("团队交易")])]), _vm._v(" "), _c('div', {
    staticClass: "volume_main"
  }, [_c('div', {
    staticClass: "total"
  }, [_vm._v("累计交易额(元)")]), _vm._v(" "), _c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.total))])]), _vm._v(" "), _c('div', {
    staticClass: "trade_detail"
  }, [_c('ul', {
    staticClass: "tabTwo"
  }, [_c('li', {
    class: {
      'active': _vm.currentGroupWd == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeTabWd(1)
      }
    }
  }, [_vm._v("日维度")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroupWd == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeTabWd(2)
      }
    }
  }, [_vm._v("月维度")])]), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [(_vm.detailArr.length > 0) ? _c('div', {
    staticClass: "detail-list"
  }, _vm._l((_vm.detailArr), function(item, j) {
    return _c('div', {
      staticClass: "item"
    }, [
      [_c('div', [_c('div', {
        staticClass: "time"
      }, [_c('span', [_vm._v(_vm._s(item.time))])]), _vm._v(" "), _c('ul', [_c('li', [_c('p', [_vm._v("全部交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.totalSubTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("全部笔数")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.totalNum))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("刷卡借记卡")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.shuaKaDebitTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("刷卡贷记卡")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.shuaKaCreditTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("扫码交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.saomaTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("云闪付交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.yunshanfuTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("快捷交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.kuaijieTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("其他")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.otherTrans))])])])])]
    ], 2)
  }), 0) : _vm._e()])], 1), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.isNoData
    }
  })], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-aba55904", module.exports)
  }
}

/***/ }),

/***/ 2069:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1608);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("37b5d7ee", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aba55904&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./tradeVolume.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aba55904&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./tradeVolume.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 571:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2069)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1377),
  /* template */
  __webpack_require__(1921),
  /* scopeId */
  "data-v-aba55904",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\tradeVolume.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] tradeVolume.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-aba55904", Component.options)
  } else {
    hotAPI.reload("data-v-aba55904", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});