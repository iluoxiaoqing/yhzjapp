webpackJsonp([88],{

/***/ 1465:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            orderNo: this.$route.query.orderNo,
            status: "",
            orderDetail: {},
            minute: 0,
            second: 0,
            countText: "0分0秒",
            overtime: false,
            discountAmount: "",
            expressName: "",
            expressNo: "",
            imgUrl: _apiConfig2.default.KY_IP
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "salesBill/querySalesBillDetail",
            "data": {
                "orderNo": this.orderNo
            }
        }, function (data) {

            _this.orderDetail = data.data;
            _this.status = data.data.status;
            //console.log(data.data);
            _this.discountAmount = data.data.discountAmount;
            _this.expressName = data.data.expressName;
            _this.expressNo = data.data.expressNo;

            if (data.data.status == 'REPAY') {

                var timer = null;
                _this.minute = data.data.closeTime.split(":")[0] * 1;
                _this.second = data.data.closeTime.split(":")[1] * 1;

                timer = setInterval(function () {

                    if (_this.minute == 0 && _this.second == 0) {
                        _this.overtime = true;
                        _this.status = "DISABLE";
                        clearInterval(timer);
                    } else if (_this.minute >= 0) {
                        if (_this.second > 0) {
                            _this.second--;
                        } else if (_this.second == 0) {
                            _this.minute--;
                            _this.second = 59;
                        }
                    }

                    _this.countText = _this.addZero(_this.minute) + "分" + _this.addZero(_this.second) + "秒";
                }, 1000);
            }
        });

        window.alipayResult = this.alipayResult;
    },

    methods: {
        alipayResult: function alipayResult(state) {
            var _this2 = this;

            if (state == "true" || state == 1 || state == true || state == '1') {
                common.toast({
                    "content": "支付成功"
                });
            } else {
                common.toast({
                    "content": "支付失败"
                });
            }

            setTimeout(function () {
                _this2.$router.push({
                    "path": "myOrder",
                    "query": {
                        "type": 1
                    }
                });
            }, 1000);
        },
        addZero: function addZero(n) {
            if (n < 10) {
                return "0" + n;
            } else {
                return n;
            }
        },
        gotoPay: function gotoPay() {

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "alipay/evokeAliPay",
                "data": {
                    "outTradeNo": this.orderNo,
                    "subject": "",
                    "body": "",
                    "businessCode": "APP_SHOP_CODE"
                }
            }, function (data) {

                var aliPay = {
                    "alipay": data.data,
                    "redirectFunc": "alipayResult"
                };

                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                } else {
                    window.android.nativeAlipay(JSON.stringify(aliPay));
                    console.log(JSON.stringify(aliPay));
                }
            });
        }
    }
};

/***/ }),

/***/ 1606:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-a48f59d4] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-a48f59d4] {\n  background: #fff;\n}\n.tips_success[data-v-a48f59d4] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-a48f59d4] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-a48f59d4] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-a48f59d4],\n.fade-leave-active[data-v-a48f59d4] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-a48f59d4],\n.fade-leave-to[data-v-a48f59d4] {\n  opacity: 0;\n}\n.default_button[data-v-a48f59d4] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-a48f59d4] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-a48f59d4] {\n  position: relative;\n}\n.loading-tips[data-v-a48f59d4] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.gray[data-v-a48f59d4] {\n  -webkit-filter: grayscale(100%);\n  opacity: 0.8;\n  -webkit-filter: gray;\n          filter: gray;\n}\n.orderDetail[data-v-a48f59d4] {\n  width: 100%;\n  overflow: hidden;\n}\n.orderDetail .orderDetail_banner[data-v-a48f59d4] {\n  width: 100%;\n  height: 2.28rem;\n}\n.orderDetail .orderDetail_banner .orderDetail_status[data-v-a48f59d4] {\n  width: 100%;\n  height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  position: relative;\n}\n.orderDetail .orderDetail_banner .orderDetail_status.paying[data-v-a48f59d4] {\n  background: #E95647;\n}\n.orderDetail .orderDetail_banner .orderDetail_status.waiting[data-v-a48f59d4] {\n  background: url(" + __webpack_require__(1719) + ") no-repeat;\n  background-size: contain;\n}\n.orderDetail .orderDetail_banner .orderDetail_status.send[data-v-a48f59d4] {\n  background: url(" + __webpack_require__(1716) + ") no-repeat;\n  background-size: contain;\n}\n.orderDetail .orderDetail_banner .orderDetail_status b[data-v-a48f59d4] {\n  font-size: 0.32rem;\n  color: #fff;\n  display: block;\n  margin-left: 0.32rem;\n}\n.orderDetail .orderDetail_banner .orderDetail_status p[data-v-a48f59d4] {\n  font-size: 0.32rem;\n  color: #fff;\n  margin-left: 0.32rem;\n}\n.orderDetail .orderDetail_banner .orderDetail_status a[data-v-a48f59d4] {\n  font-size: 0.26rem;\n  color: #fff;\n  display: block;\n  width: 1.5rem;\n  height: 0.54rem;\n  position: absolute;\n  -webkit-transform: translateY(-50%);\n      -ms-transform: translateY(-50%);\n          transform: translateY(-50%);\n  top: 50%;\n  right: 0.3rem;\n  text-align: center;\n  line-height: 0.54rem;\n}\n.orderDetail .orderDetail_banner .orderDetail_status a[data-v-a48f59d4]:after {\n  content: \"\";\n  width: 200%;\n  height: 200%;\n  left: -50%;\n  top: -50%;\n  border: 1px solid #fff;\n  -webkit-transform: scale(0.5);\n      -ms-transform: scale(0.5);\n          transform: scale(0.5);\n  position: absolute;\n  border-radius: 0.08rem;\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n}\n.orderDetail .orderDetail_address[data-v-a48f59d4] {\n  width: 92%;\n  padding: 4%;\n  overflow: hidden;\n  background: #fff;\n  margin-bottom: 0.16rem;\n}\n.orderDetail .orderDetail_address span[data-v-a48f59d4] {\n  display: block;\n  opacity: 0.5;\n  color: #3D4A5B;\n  font-size: 0.28rem;\n}\n.orderDetail .orderDetail_address b[data-v-a48f59d4] {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  margin: 0.05rem 0 0.1rem;\n}\n.orderDetail .orderDetail_goods[data-v-a48f59d4] {\n  width: 92%;\n  padding: 4%;\n  overflow: hidden;\n  background: #fff;\n}\n.orderDetail .orderDetail_goods .goods_body[data-v-a48f59d4] {\n  width: 100%;\n  height: 1.84rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 0.3rem;\n}\n.orderDetail .orderDetail_goods .goods_body img[data-v-a48f59d4] {\n  width: 1.84rem;\n  height: 1.84rem;\n  display: block;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc[data-v-a48f59d4] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding-left: 0.32rem;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc p[data-v-a48f59d4] {\n  color: #3D4A5B;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc p b[data-v-a48f59d4] {\n  font-size: 0.28rem;\n  display: block;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc p span[data-v-a48f59d4] {\n  font-size: 0.24rem;\n  opacity: 0.6;\n  display: block;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc p em[data-v-a48f59d4] {\n  font-size: 0.28rem;\n  display: block;\n}\n.orderDetail .orderDetail_goods .goods_body .goods_desc p i[data-v-a48f59d4] {\n  font-size: 0.28rem;\n  display: block;\n  color: #333;\n}\n.orderDetail .orderDetail_goods .goods_footer[data-v-a48f59d4] {\n  width: 100%;\n  font-size: 0.32rem;\n  overflow: hidden;\n  padding-top: 0.64rem;\n}\n.orderDetail .orderDetail_goods .goods_footer p[data-v-a48f59d4] {\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  text-align: right;\n}\n.orderDetail .orderDetail_goods .goods_footer p b[data-v-a48f59d4] {\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  margin-right: 0.05rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1716:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_shop_delivery.png?v=d8a43acd";

/***/ }),

/***/ 1719:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_shop_waiting.png?v=1b8e04f3";

/***/ }),

/***/ 1919:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "orderDetail"
  }, [_c('div', {
    staticClass: "orderDetail_banner"
  }, [(_vm.status == 'INIT') ? _c('div', {
    staticClass: "orderDetail_status waiting"
  }, [_c('b', [_vm._v("待发货")]), _vm._v(" "), _c('p', [_vm._v("包裹正在出库，请耐心等待")])]) : (_vm.status == 'REPAY') ? _c('div', {
    staticClass: "orderDetail_status paying"
  }, [(!_vm.overtime) ? _c('b', [_vm._v("待付款")]) : _vm._e(), _vm._v(" "), (!_vm.overtime) ? _c('p', [_vm._v("剩 " + _vm._s(_vm.countText) + " 后自动关闭")]) : _vm._e(), _vm._v(" "), (!_vm.overtime) ? _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_vm._v("去付款")]) : _vm._e()]) : (_vm.status == 'DISABLE') ? _c('div', {
    staticClass: "orderDetail_status paying gray"
  }, [_c('b', [_vm._v("已失效")]), _vm._v(" "), _c('p', [_vm._v("订单已自动关闭")])]) : (_vm.status == 'DELIVERY') ? _c('div', {
    staticClass: "orderDetail_status send"
  }, [_c('b', [_vm._v("发货中")]), _vm._v(" "), _c('p', [_vm._v("快递方式：" + _vm._s(_vm.expressName))]), _vm._v(" "), _c('p', [_vm._v("快递编号：" + _vm._s(_vm.expressNo))])]) : _c('div', {
    staticClass: "orderDetail_status send"
  }, [_c('b', [_vm._v("已发货")]), _vm._v(" "), _c('p', [_vm._v("快递方式：" + _vm._s(_vm.expressName))]), _vm._v(" "), _c('p', [_vm._v("快递编号：" + _vm._s(_vm.expressNo))])])]), _vm._v(" "), _c('div', {
    staticClass: "orderDetail_address",
    class: {
      'gray': _vm.status == 'DISABLE'
    }
  }, [_c('span', [_vm._v(_vm._s(_vm.orderDetail.province) + " " + _vm._s(_vm.orderDetail.city))]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.orderDetail.address))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.orderDetail.userName) + " " + _vm._s(_vm.orderDetail.phone))])]), _vm._v(" "), _c('div', {
    staticClass: "orderDetail_goods",
    class: {
      'gray': _vm.status == 'DISABLE'
    }
  }, [_vm._l((_vm.orderDetail.orderList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "goods_body"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.picturePath
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "goods_desc"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), (i.remark) ? _c('span', [_vm._v(_vm._s(JSON.parse(i.remark).goodsDesc))]) : _vm._e()]), _vm._v(" "), _c('p', {
      staticStyle: {
        "display": "flex",
        "justify-content": "space-between"
      }
    }, [_c('em', [_vm._v("¥" + _vm._s(i.price))]), _vm._v(" "), _c('i', [_vm._v("X" + _vm._s(i.goodsCount))])])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "goods_footer"
  }, [_c('p', [_vm._v("订单编号：" + _vm._s(_vm.orderDetail.orderNo))]), _vm._v(" "), _c('p', [_vm._v("共1件商品 实付："), _c('b', [_vm._v(_vm._s(_vm.orderDetail.amount))]), _vm._v("元")])])], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a48f59d4", module.exports)
  }
}

/***/ }),

/***/ 2067:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1606);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0c81fcb2", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a48f59d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./orderDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a48f59d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./orderDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 670:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2067)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1465),
  /* template */
  __webpack_require__(1919),
  /* scopeId */
  "data-v-a48f59d4",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\orderDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] orderDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a48f59d4", Component.options)
  } else {
    hotAPI.reload("data-v-a48f59d4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});