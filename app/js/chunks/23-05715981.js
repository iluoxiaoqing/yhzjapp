webpackJsonp([23],{

/***/ 1283:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_success.png?v=d5c38576";

/***/ }),

/***/ 1296:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_popwindow_close.png?v=3af1fd99";

/***/ }),

/***/ 1318:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_payment_failed.png?v=011459d2";

/***/ }),

/***/ 1476:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

var _jsonp = __webpack_require__(730);

var _jsonp2 = _interopRequireDefault(_jsonp);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var errorMsg = "";

var checkName = function checkName(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.name.empty;
        return false;
    } else {

        var reg = _base2.default.name.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.name.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};

//验证手机号
var checkMobile = function checkMobile(obj) {

    errorMsg = false;
    if (obj === '') {
        errorMsg = _base2.default.phoneNumber.empty;
        return false;
    } else {

        var reg = _base2.default.phoneNumber.reg;
        if (!reg.test(obj)) {
            errorMsg = _base2.default.phoneNumber.error;
            return false;
        } else {
            return true;
            this.disabled = false;
        }
    }
};

exports.default = {
    data: function data() {
        return {
            addressShow: false,
            addressValue: "请选择",
            address: [{
                values: [],
                defaultIndex: 0
            }, {
                values: [],
                defaultIndex: 0
            }, {
                values: [],
                defaultIndex: 0
            }],
            provinceArray: [],
            provinceCode: "",
            cityCode: "",
            province: "",
            city: "",
            area: "",
            detailArea: "",
            name: "",
            cellPhone: "",
            isDefault: false,
            goodsPrice: this.$route.query.goodsPrice,
            productCode: this.$route.query.productCode,
            paySuccess: false,
            payFail: false
        };
    },
    created: function created() {
        var _this = this;

        (0, _jsonp2.default)("https://vip.cardinfo.com.cn/dataservice/getAllProvinceByJson", {
            timeout: 30000
        }, function (error, data) {
            _this.address[0].values = data;
        });

        (0, _jsonp2.default)("https://vip.cardinfo.com.cn/dataservice/getCityByJson/11", {
            timeout: 30000
        }, function (error, data) {
            _this.address[1].values = data;
        });

        (0, _jsonp2.default)("https://vip.cardinfo.com.cn/dataservice/getDistrictByJson/1101", {
            timeout: 30000
        }, function (error, data) {
            _this.address[2].values = data;
        });

        window.alipayResult = this.alipayResult;
        common.youmeng("礼包支付", "进入礼包支付");
    },

    watch: {
        cellPhone: function cellPhone(newValue, oldValue) {

            if (newValue.trim().length > 0) {
                this.canClick = true;
            } else {
                this.canClick = false;
                return;
            }

            if (newValue > oldValue) {
                if (newValue.length === 4 || newValue.length === 9) {

                    var pre = newValue.substring(0, newValue.length - 1);
                    var last = newValue.substr(newValue.length - 1, 1);
                    this.cellPhone = pre + ' ' + last;
                } else {
                    this.cellPhone = newValue;
                }
            } else {
                if (newValue.length === 4 || newValue.length === 9) {
                    this.cellPhone = this.cellPhone.trim();
                } else {
                    this.cellPhone = newValue;
                }
            }
        }
    },
    methods: {
        payAgain: function payAgain() {
            common.youmeng("升级推手", "点击重新付款手");
            this.hideMask();
            this.gotoPay();
        },
        hideMask: function hideMask() {
            this.paySuccess = false;
            this.payFail = false;
        },
        alipayResult: function alipayResult(state) {
            if (state == "true" || state == 1 || state == true || state == '1') {
                this.paySuccess = true;
            } else {
                this.payFail = true;
            }
        },
        gotoHome: function gotoHome() {

            this.paySuccess = false;
            this.payFail = false;

            var openPath = {
                "path": "xytsapp/kpi/mylevel",
                "isNeedClosePage": "true"
            };

            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeOpenPage.postMessage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            } else {
                window.android.nativeOpenPage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            }
        },
        chooseAddress: function chooseAddress() {
            this.addressShow = true;
        },
        changeProvince: function changeProvince(picker) {
            var _this2 = this;

            var code = picker.getValues()[0].code;
            (0, _jsonp2.default)("https://vip.cardinfo.com.cn/dataservice/getCityByJson/" + code, {
                timeout: 30000
            }, function (error, data) {

                _this2.address[1].values = data;
                var cityIndex = picker.getIndexes()[1];
                var cityCode = data[cityIndex].code;
                (0, _jsonp2.default)("https://vip.cardinfo.com.cn/dataservice/getDistrictByJson/" + cityCode, {
                    timeout: 30000
                }, function (error, data) {
                    _this2.address[2].values = data;
                });
            });

            //console.log( this.address )
        },
        confirmAddress: function confirmAddress(picker) {
            this.addressValue = picker.getValues()[0].name + picker.getValues()[1].name + picker.getValues()[2].name;
            this.provinceCode = picker.getValues()[0].code;
            this.cityCode = picker.getValues()[1].code;
            this.province = picker.getValues()[0].name;
            this.city = picker.getValues()[1].name;
            this.area = picker.getValues()[2].name;
        },
        addAddress: function addAddress() {
            var _this3 = this;

            if (!checkName(this.name)) {
                common.toast({
                    "content": "请填写收货人姓名"
                });
                return;
            }

            if (!checkMobile(common.trim(this.cellPhone))) {
                common.toast({
                    "content": "请填写收货人手机号"
                });
                return;
            }

            if (!this.cityCode) {
                common.toast({
                    "content": "请选择收货地区"
                });
                return;
            }

            if (!this.detailArea) {
                common.toast({
                    "content": "请填写详细地址"
                });
                return;
            }

            common.youmeng("礼包支付", "点击确认付款");

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/save",
                "data": {
                    "receivePerson": this.name,
                    "phoneNo": common.trim(this.cellPhone),
                    "provinceCode": this.provinceCode,
                    "cityCode": this.cityCode,
                    "province": this.province,
                    "city": this.city,
                    "area": this.area,
                    "addressDetail": this.detailArea,
                    "isDefault": this.isDefault
                }
            }, function (data) {

                console.log(data);

                common.Ajax({
                    "url": _apiConfig2.default.KY_IP + "recommOrder/purchase",
                    "data": {
                        "productCode": _this3.productCode,
                        "payAmount": _this3.goodsPrice,
                        "addressId": data.data
                    }
                }, function (data) {

                    var aliPay = {
                        "alipay": data.data.alipay,
                        "redirectFunc": "alipayResult"
                    };

                    common.youmeng("升级推手", "调用支付宝");

                    if (common.isClient() == "ios") {
                        window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    } else {
                        window.android.nativeAlipay(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    }
                });
            });
        }
    },
    filters: {
        pickerValueFilter: function pickerValueFilter(val) {
            if (Array.isArray(val)) {
                return val.toString();
            } else {
                return '请选择';
            }
        }
    }
};

/***/ }),

/***/ 1537:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-32c3afd8] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-32c3afd8] {\n  background: #fff;\n}\n.tips_success[data-v-32c3afd8] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-32c3afd8] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-32c3afd8] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-32c3afd8],\n.fade-leave-active[data-v-32c3afd8] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-32c3afd8],\n.fade-leave-to[data-v-32c3afd8] {\n  opacity: 0;\n}\n.default_button[data-v-32c3afd8] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-32c3afd8] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-32c3afd8] {\n  position: relative;\n}\n.loading-tips[data-v-32c3afd8] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.upgradeRight_pay[data-v-32c3afd8] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.32rem;\n}\n.upgradeRight_pay p[data-v-32c3afd8] {\n  width: 96%;\n  padding: 0.24rem 4% 4% 0.28rem;\n  background: #fff;\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.upgradeRight_pay p[data-v-32c3afd8]:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 96%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.upgradeRight_pay p[data-v-32c3afd8]:nth-last-of-type(1):after {\n  content: '';\n  background: none;\n}\n.upgradeRight_pay p small[data-v-32c3afd8] {\n  display: block;\n  font-size: 0.32rem;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n.upgradeRight_pay p small em[data-v-32c3afd8] {\n  width: 100%;\n  display: block;\n  color: #BACCE3;\n  background: url(" + __webpack_require__(800) + ") no-repeat right 0.4rem center;\n  background-size: 0.14rem 0.24rem;\n}\n.upgradeRight_pay p small em.active[data-v-32c3afd8] {\n  color: #333;\n}\n.upgradeRight_pay p span[data-v-32c3afd8] {\n  width: 1.6rem;\n  display: block;\n  color: #333;\n  font-size: 0.32rem;\n}\n.upgradeRight_pay p input[data-v-32c3afd8] {\n  font-size: 0.32rem;\n  display: block;\n}\n.upgradeRight_pay p textarea[data-v-32c3afd8] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 1.9rem;\n  font-size: 0.32rem;\n  display: block;\n}\n.upgradeRight_pay p textarea[data-v-32c3afd8]::-webkit-input-placeholder {\n  color: #BACCE3;\n}\n.upgradeRight_mask[data-v-32c3afd8] {\n  width: 100%;\n  height: 100%;\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 200;\n  background: rgba(0, 0, 0, 0.6);\n}\n.upgradeRight_success[data-v-32c3afd8] {\n  width: 6.2rem;\n  padding: 0.64rem 0;\n  background: #fff;\n  position: fixed;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  z-index: 210;\n  top: 30%;\n  border-radius: 0.08rem;\n}\n.upgradeRight_success em[data-v-32c3afd8] {\n  width: 0.6rem;\n  height: 0.6rem;\n  background: url(" + __webpack_require__(1296) + ") no-repeat #fff;\n  background-size: contain;\n  display: block;\n  border-radius: 100%;\n  text-align: center;\n  line-height: 0.6rem;\n  font-size: 0.5rem;\n  position: absolute;\n  right: 0;\n  top: -0.86rem;\n}\n.upgradeRight_success img[data-v-32c3afd8] {\n  width: 1.6rem;\n  height: 1.6rem;\n  display: block;\n  margin: 0 auto 0.1rem;\n}\n.upgradeRight_success h1[data-v-32c3afd8] {\n  display: block;\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  margin-bottom: 0.1rem;\n}\n.upgradeRight_success p[data-v-32c3afd8] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  opacity: 0.8;\n  text-align: center;\n  margin-bottom: 0.18rem;\n  display: block;\n}\n.upgradeRight_success a[data-v-32c3afd8] {\n  width: 4.92rem;\n  height: 0.8rem;\n  font-weight: 700;\n  color: #fff;\n  font-size: 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #3F83FF;\n  border-radius: 0.06rem;\n  margin: 0.48rem auto 0;\n}\n.upgradeRight_success i[data-v-32c3afd8] {\n  width: 5.2rem;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.upgradeRight_success i strong[data-v-32c3afd8] {\n  width: 2.24rem;\n  height: 0.8rem;\n  display: block;\n  color: #3F83FF;\n  font-size: 0.32rem;\n  text-align: center;\n  line-height: 0.8rem;\n  border-radius: 0.06rem;\n  position: relative;\n}\n.upgradeRight_success i strong.active[data-v-32c3afd8] {\n  color: #fff;\n  background: #3F83FF;\n}\n.upgradeRight_success i strong.active[data-v-32c3afd8]:after {\n  content: \"\";\n  border: none;\n}\n.upgradeRight_success i strong[data-v-32c3afd8]:after {\n  content: \"\";\n  width: 200%;\n  height: 200%;\n  position: absolute;\n  left: -50%;\n  top: -50%;\n  border: 1px solid #3F83FF;\n  -webkit-transform: scale(0.5);\n      -ms-transform: scale(0.5);\n          transform: scale(0.5);\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  border-radius: 0.06rem;\n}\n.upgradeRight_submit[data-v-32c3afd8] {\n  width: 100%;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  height: 1.0rem;\n  background: #fff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.upgradeRight_submit p[data-v-32c3afd8] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  background: url(" + __webpack_require__(1760) + ") no-repeat left center;\n  background-size: 0.4rem 0.3rem;\n  padding-left: 0.58rem;\n  margin-left: 0.3rem;\n}\n.upgradeRight_submit p span[data-v-32c3afd8] {\n  font-size: 0.36rem;\n}\n.upgradeRight_submit a[data-v-32c3afd8] {\n  display: block;\n  width: 2.2rem;\n  height: 1.0rem;\n  background: #3F83FF;\n  text-align: center;\n  line-height: 1.0rem;\n  color: #fff;\n  font-size: 0.34rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1760:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_pay_alipay.png?v=185475a9";

/***/ }),

/***/ 1851:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "upgradeRight_pay"
  }, [_c('p', [_c('span', [_vm._v("联系人")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    attrs: {
      "type": "text",
      "placeholder": "请输入姓名"
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_c('span', [_vm._v("手机号码")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.cellPhone),
      expression: "cellPhone"
    }],
    attrs: {
      "maxlength": "13",
      "type": "tel",
      "placeholder": "请输入您的手机号码"
    },
    domProps: {
      "value": (_vm.cellPhone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.cellPhone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_c('span', [_vm._v("所在省市")]), _vm._v(" "), _c('small', {
    on: {
      "click": function($event) {
        return _vm.chooseAddress()
      }
    }
  }, [_c('em', {
    class: {
      'active': _vm.addressValue !== '请选择'
    }
  }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(_vm.addressValue) + "\n\t\t\t\t\t")])])]), _vm._v(" "), _c('p', {
    staticStyle: {
      "align-items": "baseline"
    }
  }, [_c('span', [_vm._v("详细地址")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.detailArea),
      expression: "detailArea"
    }],
    attrs: {
      "placeholder": "街道门牌信息"
    },
    domProps: {
      "value": (_vm.detailArea)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.detailArea = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('wv-picker', {
    attrs: {
      "visible": _vm.addressShow,
      "columns": _vm.address,
      "value-key": "name"
    },
    on: {
      "update:visible": function($event) {
        _vm.addressShow = $event
      },
      "confirm": _vm.confirmAddress,
      "change": _vm.changeProvince
    }
  })], 1), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess || _vm.payFail) ? _c('div', {
    staticClass: "upgradeRight_mask",
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1283)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("升级成功")]), _vm._v(" "), _c('p', [_vm._v("您已经成功升级为推手")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoHome()
      }
    }
  }, [_vm._v("马上去赚钱")])]) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.payFail) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1318)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("支付失败")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.payAgain()
      }
    }
  }, [_vm._v("重新付款")])]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "upgradeRight_submit"
  }, [_c('p', [_vm._v("\n\t\t\t\t付款:"), _c('span', [_vm._v(_vm._s(_vm.goodsPrice))]), _vm._v("元\n\t\t\t")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.addAddress()
      }
    }
  }, [_vm._v("确认付款")])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-32c3afd8", module.exports)
  }
}

/***/ }),

/***/ 1998:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1537);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("3c6c23a4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32c3afd8&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight_pay.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32c3afd8&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight_pay.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 687:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1998)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1476),
  /* template */
  __webpack_require__(1851),
  /* scopeId */
  "data-v-32c3afd8",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\upgradeRight_pay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] upgradeRight_pay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32c3afd8", Component.options)
  } else {
    hotAPI.reload("data-v-32c3afd8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 730:
/***/ (function(module, exports, __webpack_require__) {

/**
 * Module dependencies
 */

var debug = __webpack_require__(752)('jsonp');

/**
 * Module exports.
 */

module.exports = jsonp;

/**
 * Callback index.
 */

var count = 0;

/**
 * Noop function.
 */

function noop(){}

/**
 * JSONP handler
 *
 * Options:
 *  - param {String} qs parameter (`callback`)
 *  - prefix {String} qs parameter (`__jp`)
 *  - name {String} qs parameter (`prefix` + incr)
 *  - timeout {Number} how long after a timeout error is emitted (`60000`)
 *
 * @param {String} url
 * @param {Object|Function} optional options / callback
 * @param {Function} optional callback
 */

function jsonp(url, opts, fn){
  if ('function' == typeof opts) {
    fn = opts;
    opts = {};
  }
  if (!opts) opts = {};

  var prefix = opts.prefix || '__jp';

  // use the callback name that was passed if one was provided.
  // otherwise generate a unique name by incrementing our counter.
  var id = opts.name || (prefix + (count++));

  var param = opts.param || 'callback';
  var timeout = null != opts.timeout ? opts.timeout : 60000;
  var enc = encodeURIComponent;
  var target = document.getElementsByTagName('script')[0] || document.head;
  var script;
  var timer;


  if (timeout) {
    timer = setTimeout(function(){
      cleanup();
      if (fn) fn(new Error('Timeout'));
    }, timeout);
  }

  function cleanup(){
    if (script.parentNode) script.parentNode.removeChild(script);
    window[id] = noop;
    if (timer) clearTimeout(timer);
  }

  function cancel(){
    if (window[id]) {
      cleanup();
    }
  }

  window[id] = function(data){
    debug('jsonp got', data);
    cleanup();
    if (fn) fn(null, data);
  };

  // add qs component
  url += (~url.indexOf('?') ? '&' : '?') + param + '=' + enc(id);
  url = url.replace('?&', '?');

  debug('jsonp req "%s"', url);

  // create script
  script = document.createElement('script');
  script.src = url;
  target.parentNode.insertBefore(script, target);

  return cancel;
}


/***/ }),

/***/ 752:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = __webpack_require__(753);
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  'lightseagreen',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // NB: In an Electron preload script, document will be defined but not fully
  // initialized. Since we know we're in Chrome, we'll just detect this case
  // explicitly
  if (typeof window !== 'undefined' && window.process && window.process.type === 'renderer') {
    return true;
  }

  // is webkit? http://stackoverflow.com/a/16459606/376773
  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
  return (typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (typeof window !== 'undefined' && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
    // double check webkit in userAgent just in case we are in a worker
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  try {
    return JSON.stringify(v);
  } catch (err) {
    return '[UnexpectedJSONParseError]: ' + err.message;
  }
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs(args) {
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return;

  var c = 'color: ' + this.color;
  args.splice(1, 0, c, 'color: inherit')

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-zA-Z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = exports.storage.debug;
  } catch(e) {}

  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG
  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = process.env.DEBUG;
  }

  return r;
}

/**
 * Enable namespaces listed in `localStorage.debug` initially.
 */

exports.enable(load());

/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */

function localstorage() {
  try {
    return window.localStorage;
  } catch (e) {}
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(18)))

/***/ }),

/***/ 753:
/***/ (function(module, exports, __webpack_require__) {


/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = createDebug.debug = createDebug['default'] = createDebug;
exports.coerce = coerce;
exports.disable = disable;
exports.enable = enable;
exports.enabled = enabled;
exports.humanize = __webpack_require__(754);

/**
 * The currently active debug mode names, and names to skip.
 */

exports.names = [];
exports.skips = [];

/**
 * Map of special "%n" handling functions, for the debug "format" argument.
 *
 * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
 */

exports.formatters = {};

/**
 * Previous log timestamp.
 */

var prevTime;

/**
 * Select a color.
 * @param {String} namespace
 * @return {Number}
 * @api private
 */

function selectColor(namespace) {
  var hash = 0, i;

  for (i in namespace) {
    hash  = ((hash << 5) - hash) + namespace.charCodeAt(i);
    hash |= 0; // Convert to 32bit integer
  }

  return exports.colors[Math.abs(hash) % exports.colors.length];
}

/**
 * Create a debugger with the given `namespace`.
 *
 * @param {String} namespace
 * @return {Function}
 * @api public
 */

function createDebug(namespace) {

  function debug() {
    // disabled?
    if (!debug.enabled) return;

    var self = debug;

    // set `diff` timestamp
    var curr = +new Date();
    var ms = curr - (prevTime || curr);
    self.diff = ms;
    self.prev = prevTime;
    self.curr = curr;
    prevTime = curr;

    // turn the `arguments` into a proper Array
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }

    args[0] = exports.coerce(args[0]);

    if ('string' !== typeof args[0]) {
      // anything else let's inspect with %O
      args.unshift('%O');
    }

    // apply any `formatters` transformations
    var index = 0;
    args[0] = args[0].replace(/%([a-zA-Z%])/g, function(match, format) {
      // if we encounter an escaped % then don't increase the array index
      if (match === '%%') return match;
      index++;
      var formatter = exports.formatters[format];
      if ('function' === typeof formatter) {
        var val = args[index];
        match = formatter.call(self, val);

        // now we need to remove `args[index]` since it's inlined in the `format`
        args.splice(index, 1);
        index--;
      }
      return match;
    });

    // apply env-specific formatting (colors, etc.)
    exports.formatArgs.call(self, args);

    var logFn = debug.log || exports.log || console.log.bind(console);
    logFn.apply(self, args);
  }

  debug.namespace = namespace;
  debug.enabled = exports.enabled(namespace);
  debug.useColors = exports.useColors();
  debug.color = selectColor(namespace);

  // env-specific initialization logic for debug instances
  if ('function' === typeof exports.init) {
    exports.init(debug);
  }

  return debug;
}

/**
 * Enables a debug mode by namespaces. This can include modes
 * separated by a colon and wildcards.
 *
 * @param {String} namespaces
 * @api public
 */

function enable(namespaces) {
  exports.save(namespaces);

  exports.names = [];
  exports.skips = [];

  var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
  var len = split.length;

  for (var i = 0; i < len; i++) {
    if (!split[i]) continue; // ignore empty strings
    namespaces = split[i].replace(/\*/g, '.*?');
    if (namespaces[0] === '-') {
      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
    } else {
      exports.names.push(new RegExp('^' + namespaces + '$'));
    }
  }
}

/**
 * Disable debug output.
 *
 * @api public
 */

function disable() {
  exports.enable('');
}

/**
 * Returns true if the given mode name is enabled, false otherwise.
 *
 * @param {String} name
 * @return {Boolean}
 * @api public
 */

function enabled(name) {
  var i, len;
  for (i = 0, len = exports.skips.length; i < len; i++) {
    if (exports.skips[i].test(name)) {
      return false;
    }
  }
  for (i = 0, len = exports.names.length; i < len; i++) {
    if (exports.names[i].test(name)) {
      return true;
    }
  }
  return false;
}

/**
 * Coerce `val`.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function coerce(val) {
  if (val instanceof Error) return val.stack || val.message;
  return val;
}


/***/ }),

/***/ 754:
/***/ (function(module, exports) {

/**
 * Helpers.
 */

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var y = d * 365.25;

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} [options]
 * @throws {Error} throw an error if val is not a non-empty string or a number
 * @return {String|Number}
 * @api public
 */

module.exports = function(val, options) {
  options = options || {};
  var type = typeof val;
  if (type === 'string' && val.length > 0) {
    return parse(val);
  } else if (type === 'number' && isNaN(val) === false) {
    return options.long ? fmtLong(val) : fmtShort(val);
  }
  throw new Error(
    'val is not a non-empty string or a valid number. val=' +
      JSON.stringify(val)
  );
};

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = String(str);
  if (str.length > 100) {
    return;
  }
  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(
    str
  );
  if (!match) {
    return;
  }
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
    default:
      return undefined;
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtShort(ms) {
  if (ms >= d) {
    return Math.round(ms / d) + 'd';
  }
  if (ms >= h) {
    return Math.round(ms / h) + 'h';
  }
  if (ms >= m) {
    return Math.round(ms / m) + 'm';
  }
  if (ms >= s) {
    return Math.round(ms / s) + 's';
  }
  return ms + 'ms';
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtLong(ms) {
  return plural(ms, d, 'day') ||
    plural(ms, h, 'hour') ||
    plural(ms, m, 'minute') ||
    plural(ms, s, 'second') ||
    ms + ' ms';
}

/**
 * Pluralization helper.
 */

function plural(ms, n, name) {
  if (ms < n) {
    return;
  }
  if (ms < n * 1.5) {
    return Math.floor(ms / n) + ' ' + name;
  }
  return Math.ceil(ms / n) + ' ' + name + 's';
}


/***/ }),

/***/ 800:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ })

});