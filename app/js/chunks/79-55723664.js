webpackJsonp([79],{

/***/ 1379:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            goodsPrice: "",
            productCode: "",
            canClick: false
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "recommOrder/good",
            "data": {
                "productCode": this.$route.query.productCode
            }
        }, function (data) {
            console.log(data);
            _this.canClick = true;
            _this.productCode = data.data.productCode;
            _this.goodsPrice = data.data.price;
        });
    },

    methods: {
        gotoPay: function gotoPay() {

            if (!this.canClick) {
                return;
            }

            common.youmeng("广告-成为推手", "点击申请按钮");
            this.$router.push({
                path: "upgradeRight_pay",
                query: {
                    "productCode": this.productCode,
                    "goodsPrice": this.goodsPrice
                }
            });
        }
    }
};

/***/ }),

/***/ 1526:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1645:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_banner.png?v=e9f749fe";

/***/ }),

/***/ 1647:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_you.png?v=3071e036";

/***/ }),

/***/ 1648:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_zuo.png?v=f4e62bc2";

/***/ }),

/***/ 1840:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "pusher_content"
  }, [_vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "pusher_price"
  }, [_vm._v("\n              礼包价格：" + _vm._s(_vm.goodsPrice) + "元\n          ")])]), _vm._v(" "), _c('div', {
    staticClass: "banner_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_vm._m(3), _vm._v(" "), _c('span', [_vm._v("申请时间：2019.3.1-2019.6.30.")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1645)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_title"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1648),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("购买推手礼包")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1647),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_main"
  }, [_c('p', [_c('i'), _vm._v("成为推手，马上开启赚钱之路")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("获赠一台MPOS"), _c('span', [_vm._v("(自用省钱，推荐赚钱)")])]), _vm._v(" "), _c('p', [_c('i'), _vm._v("90天0.50%+3超低费率")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('b', [_vm._v("立即申请")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-24e8ae4a", module.exports)
  }
}

/***/ }),

/***/ 1987:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1526);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("264c7164", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-24e8ae4a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./pusher.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-24e8ae4a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./pusher.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 573:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1987)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1379),
  /* template */
  __webpack_require__(1840),
  /* scopeId */
  "data-v-24e8ae4a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\pusher.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] pusher.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-24e8ae4a", Component.options)
  } else {
    hotAPI.reload("data-v-24e8ae4a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});