webpackJsonp([40],{

/***/ 1420:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(755);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            loanList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            repeatShow: false,
            repeatInfo: {
                "title": "",
                "content": "",
                "confirmText": "我知道了",
                "cancelText": "继续办理"
            },
            selectObject: {},
            allShareCode: ""
        };
    },

    components: {
        searchBox: _searchBox2.default,
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        var _this = this;

        // window.goBack = this.goBack;
        common.youmeng("贷款申请", "进入贷款申请");
        common.Ajax({
            url: _apiConfig2.default.KY_IP + "bussMenu/loanBusiness",
            "data": {}
        }, function (data) {
            console.log(data.data);
            _this.loanList = data.data.details;
            _this.allShareCode = data.data.shareCode;
            console.log("allShareCode", _this.allShareCode);
        });
    },

    methods: {
        goBack: function goBack() {
            this.$router.push({
                "path": "recommendLoan"
            });
        },
        gotoAgent: function gotoAgent(j) {
            var _this2 = this;

            common.youmeng("贷款申请", "点击立即申请");

            this.selectObject = j;
            var buss = this.$route.query.buss;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "user/isShow",
                "data": {
                    "businessCode": "LOAN_CODE",
                    "productCode": j.applyCode
                }
            }, function (data) {
                if (data.data.isShow) {
                    _this2.repeatShow = true;
                    _this2.repeatInfo.content = data.data.showMessage;
                } else {
                    _this2.showInfoContent(j);
                }
            });
        },
        showInfoContent: function showInfoContent(j) {
            this.$store.dispatch("saveLoanData", j);

            this.$router.push({
                "path": "loanAgent"
            });
        },
        repeatConfirm: function repeatConfirm() {
            this.showInfoContent(this.selectObject);
        },
        share: function share(j) {
            console.log("分享", j);
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j.shareCode,
                    "productName": j.title,
                    "channel": "recoLoan"
                }
            });
        },
        allShare: function allShare(j) {
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j,
                    "productName": "全部推荐",
                    "channel": "recoLoan"
                }
            });
        }
    }
};

/***/ }),

/***/ 1594:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1907:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "applyLoan"
  }, [_vm._l((_vm.loanList), function(i, index) {
    return _c('div', {
      key: index
    }, [_c('div', {
      staticClass: "applyLoan_content"
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "item_top"
    }, [_c('p', [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file/downloadFile?filePath=' + i.logo,
        "alt": ""
      }
    }), _vm._v(" "), _c('b', [_vm._v(_vm._s(i.title))]), _vm._v(" "), (i.features[0] != '') ? _c('span', [_vm._v(_vm._s(i.features[0]))]) : _vm._e(), _vm._v(" "), (i.features[1] != '') ? _c('span', [_vm._v(_vm._s(i.features[1]))]) : _vm._e()]), _vm._v(" "), _c('i', [_vm._v(_vm._s(i.applyPersonSum) + "人已申请")])]), _vm._v(" "), _c('div', {
      staticClass: "item_main"
    }, [_c('div', {
      staticClass: "item_left"
    }, [_c('div', {
      staticClass: "num"
    }, [_vm._v("\n                                " + _vm._s(i.amountInterval) + "\n                            ")]), _vm._v(" "), _c('span', [_vm._v("额度范围(元)")])]), _vm._v(" "), _vm._m(0, true), _vm._v(" "), _c('div', {
      staticClass: "item_center"
    }, [_c('span', [_c('i', [_vm._v("放款时长: " + _vm._s(i.auditTime))])]), _vm._v(" "), _c('span', [_c('i', [_vm._v("利息:" + _vm._s(i.interest))])]), _vm._v(" "), _c('span', [_c('i', [_vm._v("贷款期限:" + _vm._s(i.remandLimit))])])]), _vm._v(" "), _c('div', {
      staticClass: "item_right"
    }, [(i.share == true) ? _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.share(i)
        }
      }
    }, [_vm._v(_vm._s(i.shareBtnText))]) : _vm._e()])]), _vm._v(" "), _c('div', {
      staticClass: "item_footer"
    }, [_c('span', {
      on: {
        "click": function($event) {
          return _vm.gotoAgent(i)
        }
      }
    }, [_vm._v("立即申请")])])])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "creditCard_btn"
  }, [_c('a', {
    staticClass: "default_button",
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.allShare(_vm.allShareCode)
      }
    }
  }, [_vm._v("全部推荐")])]), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.repeatShow,
      "boxInfo": _vm.repeatInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.repeatShow = $event
      },
      "cancel": _vm.repeatConfirm
    }
  })], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('em')])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7c15f1c2", module.exports)
  }
}

/***/ }),

/***/ 2055:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1594);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("0619f4e8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7c15f1c2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recoLoan.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7c15f1c2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./recoLoan.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 619:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2055)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1420),
  /* template */
  __webpack_require__(1907),
  /* scopeId */
  "data-v-7c15f1c2",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\recoLoan.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] recoLoan.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7c15f1c2", Component.options)
  } else {
    hotAPI.reload("data-v-7c15f1c2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 751:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(757)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(750),
  /* template */
  __webpack_require__(756),
  /* scopeId */
  "data-v-7cf3f18a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7cf3f18a", Component.options)
  } else {
    hotAPI.reload("data-v-7cf3f18a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7cf3f18a", module.exports)
  }
}

/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(751);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05a769e3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});