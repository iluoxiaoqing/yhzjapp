webpackJsonp([25],{

/***/ 1283:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_success.png?v=d5c38576";

/***/ }),

/***/ 1318:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_payment_failed.png?v=011459d2";

/***/ }),

/***/ 1473:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            recommOrderArray: [],
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            goodsName: "",
            goodsPrice: "",
            needAddress: "",
            productCode: "",
            paySuccess: false,
            payFail: false,
            fishSuccess: false,
            qhyyRegisterUrl: ""

        };
    },
    mounted: function mounted() {
        var _this2 = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "recommOrder/goods"
        }, function (data) {

            console.log(data.data);
            _this2.recommOrderArray = data.data;
            _this2.goodsName = data.data[0].name;
            _this2.goodsPrice = data.data[0].price;
            _this2.productCode = data.data[0].productCode;
            _this2.needAddress = data.data[0].needAddress;
            var _this = _this2;

            _this2.$nextTick(function () {
                _this2.mySwiper = new Swiper('.swiper-container', {
                    effect: 'coverflow', //3d滑动
                    //grabCursor: true,
                    centeredSlides: true,
                    initialSlide: _this2.currentIndex,
                    loop: false,
                    slidesPerView: 2,
                    spaceBetween: 15,
                    coverflow: {
                        rotate: 0, //设置为0
                        stretch: 0,
                        depth: 0,
                        modifier: 2,
                        slideShadows: false
                    },
                    onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                        var index = swiper.realIndex;
                        var json = _this.recommOrderArray[index];
                        console.log(json);
                        _this.chooseGoods(json);
                    }
                });
            });
        });

        common.youmeng("升级推手", "进入升级推手");

        window.alipayResult = this.alipayResult;
    },

    methods: {
        payAgain: function payAgain() {
            common.youmeng("升级推手", "点击重新付款手");
            this.hideMask();
            this.gotoPay();
        },
        hideMask: function hideMask() {
            this.paySuccess = false;
            this.payFail = false;
            this.fishSuccess = false;
        },
        alipayResult: function alipayResult(state) {

            if (state == "true" || state == 1 || state == true || state == '1') {

                if (this.productCode == "GIFT_B") {
                    this.paySuccess = false;
                    this.fishSuccess = true;
                } else {
                    this.paySuccess = true;
                    this.fishSuccess = false;
                }

                common.youmeng("升级推手", this.productCode + "支付成功");
            } else {
                common.youmeng("升级推手", this.productCode + "支付失败");
                this.payFail = true;
            }
        },
        gotoHome: function gotoHome() {

            this.paySuccess = false;
            this.payFail = false;
            this.fishSuccess = false;

            var openPath = {
                "path": "xytsapp/kpi/mylevel",
                "isNeedClosePage": "true"
            };

            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeOpenPage.postMessage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            } else {
                window.android.nativeOpenPage(JSON.stringify(openPath));
                console.log("openPath：" + JSON.stringify(openPath));
            }
        },
        gotoFish: function gotoFish() {
            window.location.href = this.qhyyRegisterUrl;
        },
        chooseGoods: function chooseGoods(i) {
            this.goodsName = i.name;
            this.goodsPrice = i.price;
            this.needAddress = i.needAddress;
            this.productCode = i.productCode;
        },
        gotoPay: function gotoPay() {
            var _this3 = this;

            common.youmeng("升级推手", "选择" + this.productCode);

            if (this.needAddress) {
                this.$router.push({
                    path: "upgradeRight_pay",
                    query: {
                        "productCode": this.productCode,
                        "goodsPrice": this.goodsPrice
                    }
                });
            } else {

                common.Ajax({
                    "url": _apiConfig2.default.KY_IP + "recommOrder/purchase",
                    "data": {
                        "productCode": this.productCode,
                        "payAmount": this.goodsPrice
                    }
                }, function (data) {

                    _this3.qhyyRegisterUrl = data.data.qhyyRegisterUrl;
                    // this.alipayResult(1);
                    common.youmeng("升级推手", _this3.productCode + "调用支付宝");

                    var aliPay = {
                        "alipay": data.data.alipay,
                        "redirectFunc": "alipayResult"
                    };

                    if (common.isClient() == "ios") {
                        window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    } else {
                        window.android.nativeAlipay(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    }
                });
            }
        }
    }
};

/***/ }),

/***/ 1586:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1756:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_goumailibao.png?v=f8b3a20d";

/***/ }),

/***/ 1757:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jihuodianpu.png?v=540997c2";

/***/ }),

/***/ 1759:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_kaishishoukuan.png?v=9d36407a";

/***/ }),

/***/ 1899:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "upgradeRight"
  }, [_c('div', {
    staticClass: "shareSwiper"
  }, [_c('div', {
    staticClass: "upgradeRight_title"
  }, [_vm._v("\n            选择礼包\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.recommOrderArray), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide",
      class: {
        'active': index == 0
      },
      on: {
        "click": function($event) {
          return _vm.chooseGoods(i)
        }
      }
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "item_top"
    }, [_c('h1', [_vm._v(_vm._s(i.name))]), _vm._v(" "), _c('b', [_vm._v(_vm._s(i.price)), _c('i', [_vm._v("元")])])]), _vm._v(" "), _c('div', {
      staticClass: "item_body"
    }, [_c('p', [_vm._v(_vm._s(i.desc))])]), _vm._v(" "), _c('div', {
      staticClass: "item_footer"
    }, [_c('p', [_c('span', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.commonRemark))])]), _vm._v(" "), _c('i', [_vm._v(_vm._s(i.redRemark))])])]), _vm._v(" "), _c('div', {
      staticClass: "active_hot"
    }), _vm._v(" "), _c('div', {
      staticClass: "active_yes"
    })])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]), _vm._v(" "), (_vm.productCode == 'GIFT_A') ? _c('div', {
    staticClass: "upgradeRight_process"
  }, [_c('h1', [_vm._v("礼包使用流程")]), _vm._v(" "), _vm._m(0)]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "upgradeRight_grade"
  }, [_c('h1', [_vm._v("奖金等级")]), _vm._v(" "), _vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_c('strong', [_vm._v("升级为推手")]), _vm._v(" "), _c('em', [_vm._v("已选择 " + _vm._s(_vm.goodsName) + " " + _vm._s(_vm.goodsPrice) + "元")])])]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess || _vm.payFail || _vm.fishSuccess) ? _c('div', {
    staticClass: "upgradeRight_mask",
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.payFail) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1318)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("支付失败")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.payAgain()
      }
    }
  }, [_vm._v("重新付款")])]) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.paySuccess) ? _c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1283)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("升级成功")]), _vm._v(" "), _c('p', [_vm._v("您已经成功升级为推手")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoHome()
      }
    }
  }, [_vm._v("马上去赚钱")])]) : _vm._e()]), _vm._v(" "), (_vm.fishSuccess) ? _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    staticClass: "upgradeRight_success"
  }, [_c('em', {
    on: {
      "click": function($event) {
        return _vm.hideMask()
      }
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1283)
    }
  }), _vm._v(" "), _c('h1', [_vm._v("升级成功")]), _vm._v(" "), _c('p', [_vm._v("您已经成功升级为推手")]), _vm._v(" "), _c('i', [_c('strong', {
    on: {
      "click": function($event) {
        return _vm.gotoHome()
      }
    }
  }, [_vm._v("马上去赚钱")]), _vm._v(" "), _c('strong', {
    staticClass: "active",
    on: {
      "click": function($event) {
        return _vm.gotoFish()
      }
    }
  }, [_vm._v("激活乾海有鱼")])])])]) : _vm._e()], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1756)
    }
  }), _vm._v(" "), _c('em', [_vm._v("1.购买礼包")])]), _vm._v(" "), _c('i', [_vm._v(" ▶ ▶ ")]), _vm._v(" "), _c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1757)
    }
  }), _vm._v(" "), _c('em', [_vm._v("2.激活店铺")])]), _vm._v(" "), _c('i', [_vm._v(" ▶ ▶ ")]), _vm._v(" "), _c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1759)
    }
  }), _vm._v(" "), _c('em', [_vm._v("3.开始收款")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            购买礼包成为"), _c('b', [_vm._v("推手")]), _vm._v("享受"), _c('b', [_vm._v("60%奖金奖励")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            完成30单业务成为"), _c('b', [_vm._v("主管")]), _vm._v("享受"), _c('b', [_vm._v("85%奖金奖励")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("\n            完成80单业务成为"), _c('b', [_vm._v("经理")]), _vm._v("享受"), _c('b', [_vm._v("100%奖金奖励")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-71276fef", module.exports)
  }
}

/***/ }),

/***/ 2047:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1586);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("d15fa15a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-71276fef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-71276fef&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./upgradeRight.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 681:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2047)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1473),
  /* template */
  __webpack_require__(1899),
  /* scopeId */
  "data-v-71276fef",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\upgradeRight\\upgradeRight.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] upgradeRight.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-71276fef", Component.options)
  } else {
    hotAPI.reload("data-v-71276fef", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});