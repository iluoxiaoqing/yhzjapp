webpackJsonp([77],{

/***/ 1282:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1399:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            detail: [],
            showDia: false,
            title: "",
            desc: "",
            thumbnail: "",
            u: "",
            showShare: false,
            show1: false,
            collect: "",
            isWx: false
        };
    },
    mounted: function mounted() {
        this.getDetail();
        window.shareSucces = this.shareSucces;
    },

    methods: {
        Shareshow: function Shareshow() {
            this.showDia = true;
            this.show1 = true;
        },
        getDetail: function getDetail() {
            var _this = this;

            var u = this.$route.query.u;
            if (u == undefined) {
                var _u = "";
                this.showShare = true;
                this.isWx = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "tuiShouCase/detail",
                    data: {
                        shareUser: _u,
                        id: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.u = data.data.shareUser;
                    _this.title = data.data.title;
                    _this.thumbnail = data.data.pic;
                    _this.collect = data.data.hasCollect;
                    _this.desc = data.data.desc;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            } else {
                var _u2 = this.$route.query.u;
                this.showShare = false;
                this.isWx = true;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "tuiShouCase/detail",
                    data: {
                        shareUser: _u2,
                        id: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.u = data.data.shareUser;
                    _this.title = data.data.title;
                    _this.thumbnail = data.data.pic;

                    _this.desc = data.data.desc;
                    _this.collect = data.data.hasCollect;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            }
        },
        wechatSession: function wechatSession() {
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.desc,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            var shareJson = {
                "type": "wechatTimeline",
                "image": "",
                "title": this.title,
                "des": this.desc,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        shareSucces: function shareSucces() {
            this.showDia = false;
            this.show1 = false;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },
        showBottom: function showBottom() {
            this.showDia = true;
        },
        toCollect: function toCollect() {
            var _this2 = this;

            common.youmeng("推手案例", "收藏" + this.$route.query.title);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/collect",
                data: {
                    id: this.$route.query.id
                },
                showLoading: false
            }, function (data) {
                if (data.code == "0000") {
                    _this2.collect = true;
                    common.toast({
                        content: data.msg
                    });
                } else {
                    common.toast({
                        content: data.msg
                    });
                }
            });
        }
    }
};

/***/ }),

/***/ 1495:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\handPushCase\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1670:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share.png?v=8ae7e7dd";

/***/ }),

/***/ 1671:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 1672:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ }),

/***/ 1809:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("分享至")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatSession()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1672)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatTimeline()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1671)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('h1', [_vm._v(_vm._s(_vm.detail.title))]), _vm._v(" "), _c('div', {
    staticClass: "materialList"
  }, [_c('div', {
    staticClass: "list"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.person))]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.date))])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.collect == false && _vm.isWx == false),
      expression: "collect==false && isWx==false"
    }],
    staticClass: "collect",
    on: {
      "click": function($event) {
        return _vm.toCollect();
      }
    }
  }, [_vm._v("\n                    收藏\n                ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.collect == true && _vm.isWx == false),
      expression: "collect==true && isWx==false"
    }],
    staticClass: "collected"
  }, [_vm._v("\n                    已收藏\n                ")])])]), _vm._v(" "), _c('div', {
    staticClass: "content",
    domProps: {
      "innerHTML": _vm._s(_vm.detail.content)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "wx_erweima"
  }, [_c('div', {
    staticClass: "list"
  }, [_c('div', {
    staticClass: "listImg"
  }, [(_vm.detail.shareHeadPic == '' || _vm.detail.shareHeadPic == undefined) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1282)
    }
  }) : _c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.detail.shareHeadPic
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.sharePerson))]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.shareHint))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "erweima"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'news/shareQrCode?productCode=' + _vm.detail.productCode + '&u=' + _vm.detail.shareUser
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "erweima_text"
  }, [_vm._v("\n            " + _vm._s(_vm.detail.scanHint) + "\n        ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showShare),
      expression: "showShare"
    }],
    staticClass: "share"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1670),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.Shareshow()
      }
    }
  })])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1282)
    }
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-088688b7", module.exports)
  }
}

/***/ }),

/***/ 1956:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1495);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("33480bfe", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-088688b7!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-088688b7!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 595:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1956)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1399),
  /* template */
  __webpack_require__(1809),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\handPushCase\\caseDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] caseDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-088688b7", Component.options)
  } else {
    hotAPI.reload("data-v-088688b7", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});