webpackJsonp([69],{

/***/ 1435:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            shareWechatSession: false,
            shareWechatTimeline: false,
            imgList: [],
            typeList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null
        };
    },
    mounted: function mounted() {
        this.commonajax();
        window.saveImg = this.saveImg;

        var bodyHeight = document.documentElement.clientHeight;
        var bottomBar = this.$refs.bottomBar;
        var bottomBarTop = bottomBar.getBoundingClientRect().top;

        //alert(bottomBarTop)

        //scroller.style.height = (bodyHeight-scrollerTop+44)+"px";
        this.$refs.share.style.height = bottomBarTop + "px";
    },

    methods: {
        closeShare: function closeShare() {
            this.shareWechatSession = false, this.shareWechatTimeline = false;
        },
        commonajax: function commonajax() {
            var _this = this;

            this.showLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "posters/postersPosCustomerReward",
                data: {
                    // product: this.$route.query.product
                }
                // showLoading:true
            }, function (data) {
                console.log(data);
                showLoading: false;
                _this.imgListOne = data.data.shareImgs;
                for (var i = 0; i < _this.imgListOne.length; i++) {
                    _this.imgList.push(_this.imgListOne[i].path);
                }
                console.log(_this.imgListOne);
                console.log(_this.imgList);
                _this.typeList = data.data.shareTypes;
                console.log(_this.typeList);
                _this.$nextTick(function () {
                    _this.mySwiper = new Swiper('.swiper-container', {
                        effect: 'coverflow', //3d滑动
                        centeredSlides: false,
                        initialSlide: _this.currentIndex,
                        loop: true,
                        slidesPerView: 3,
                        spaceBetween: 50,
                        observer: true,
                        observeParents: true,
                        coverflow: {
                            rotate: 0, //设置为0
                            stretch: 0,
                            depth: 60,
                            modifier: 2,
                            slideShadows: false
                        }
                    });
                });
            });
        },
        wechatSession: function wechatSession() {
            var currentIndex = this.mySwiper.realIndex + 1;
            var length = this.imgList.length;

            var currentSrc = this.imgList[currentIndex % length];
            var imgUrl = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + currentSrc;
            var currentImageIndex = currentIndex % length + 1;

            // if (this.channel == "creditCard") {
            //     common.youmeng("分享", "推荐办卡-好友-poster" + currentImageIndex);
            // } else if (this.channel == "recommendShop") {
            //     common.youmeng("分享", "推荐开店-好友-poster" + currentImageIndex);
            // } else if (this.channel == "recommendLoan") {
            //     common.youmeng("分享", "推荐贷款-好友-poster" + currentImageIndex);
            // } else {
            //     common.youmeng("分享", "其他-好友-poster" + currentImageIndex);
            // }

            var shareJson = {
                "type": "wechatSession",
                "image": imgUrl,
                "title": "商户奖励",
                "des": "",
                "thumbnail": "",
                "jumpUrl": "",
                "shareType": "img",
                "callbackName": ""

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                console.log('imgUrl:' + imgUrl);
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
                console.log('imgUrl:' + imgUrl);
            }
        },
        wechatTimeline: function wechatTimeline() {

            var currentIndex = this.mySwiper.realIndex + 1;
            var length = this.imgList.length;

            var currentSrc = this.imgList[currentIndex % length];
            var imgUrl = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + currentSrc;
            var currentImageIndex = currentIndex % length + 1;

            var shareJson = {
                "type": "wechatTimeline",
                "image": imgUrl,
                "title": "商户奖励",
                "des": "",
                "thumbnail": "",
                "jumpUrl": "",
                "shareType": "img",
                "callbackName": ""
            };
            console.log("shareJson++++", shareJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                console.log('imgUrl:' + imgUrl);
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
                console.log('imgUrl:' + imgUrl);
            }
        },
        saveImage: function saveImage() {

            var currentIndex = this.mySwiper.realIndex + 1;
            var length = this.imgList.length;

            var currentSrc = this.imgList[currentIndex % length];
            var imgUrl = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + currentSrc;
            var currentImageIndex = currentIndex % length + 1;
            var imgJson = {};
            imgJson.imgUrl = imgUrl;
            imgJson.callbackName = "saveImg";
            console.log(imgJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeSaveImage.postMessage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            } else {
                window.android.nativeSaveImage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            }
        },
        saveImg: function saveImg(j) {
            console.log("返回状态值：" + j);
            if (j == true || j == 0) {
                console.log("保存成功");
            } else {
                console.log("保存失败");
            }
        },
        shareSession: function shareSession(e) {
            console.log(e);
            if (e.shareChannel.shareType == 'img') {
                this.shareWechatSession = false;
                this.wechatSession();
            } else {
                this.shareWechatSession = false;
                var currentIndex = this.mySwiper.realIndex + 1;
                var length = this.imgList.length;

                var currentSrc = this.imgList[currentIndex % length];
                var imgUrl = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + currentSrc;
                var currentImageIndex = currentIndex % length + 1;

                var shareJson = {
                    "type": "wechatSession",
                    "image": "",
                    "title": e.shareChannel.title,
                    "des": e.shareChannel.desc,
                    "thumbnail": _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + e.shareChannel.thumbnail,
                    "jumpUrl": e.shareChannel.jumpUrl,
                    "shareType": "url",
                    "callbackName": ""

                };
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                    console.log('jumpUrl:' + e.shareChannel.jumpUrlUrl);
                } else {
                    window.android.nativeWechatShare(JSON.stringify(shareJson));
                    console.log('jumpUrl:' + imge.shareChannel.jumpUrlUrl);
                }
            }
        },
        shareTimeline: function shareTimeline(e) {
            if (e.shareChannel.shareType == 'img') {
                this.shareWechatTimeline = false;
                this.wechatTimeline();
            } else {
                this.shareWechatTimeline = false;
                var currentIndex = this.mySwiper.realIndex + 1;
                var length = this.imgList.length;

                var currentSrc = this.imgList[currentIndex % length];
                var imgUrl = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + currentSrc;
                var currentImageIndex = currentIndex % length + 1;
                var shareJson = {
                    "type": "wechatTimeline",
                    "image": "",
                    "title": e.shareChannel.title,
                    "des": e.shareChannel.desc,
                    "thumbnail": _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + e.shareChannel.thumbnail,
                    "jumpUrl": e.shareChannel.jumpUrl,
                    "shareType": "url",
                    "callbackName": ""
                };
                console.log("shareJson===", shareJson);
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                    console.log('jumpUrl:' + e.shareChannel.jumpUrlUrl);
                } else {
                    window.android.nativeWechatShare(JSON.stringify(shareJson));
                    console.log('jumpUrl:' + e.shareChannel.jumpUrl);
                }
            }
        }
    }
};

/***/ }),

/***/ 1542:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.share[data-v-383ea534] {\n  width: 100%;\n  overflow: hidden;\n}\n.share .shareSwiper[data-v-383ea534] {\n  width: 100%;\n  overflow: hidden;\n}\n.share .shareSwiper .swiper-container[data-v-383ea534] {\n  width: 200%;\n  height: 100%;\n  position: relative;\n  margin-left: -50%;\n}\n.share .shareSwiper .swiper-container .swiper-wrapper[data-v-383ea534] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.share .shareSwiper .swiper-container .swiper-wrapper .swiper-slide[data-v-383ea534] {\n  height: 90%;\n  text-align: center;\n  font-size: 0.32rem;\n  border-radius: 0.08rem;\n  overflow: hidden;\n}\n@media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n.share .shareSwiper .swiper-container .swiper-wrapper .swiper-slide[data-v-383ea534] {\n    height: 70%;\n}\n}\n.share .shareSwiper .swiper-container .swiper-wrapper .swiper-slide img[data-v-383ea534] {\n  width: 100%;\n  height: 100%;\n  display: block;\n}\n.share .bottomBar[data-v-383ea534] {\n  width: 100%;\n  height: 3.2rem;\n  background: #FAFBFF;\n  border-top-left-radius: 0.08rem;\n  border-top-right-radius: 0.08rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n}\n.share .bottomBar .bottomBar_line[data-v-383ea534] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.4rem;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 0.3rem;\n}\n.share .bottomBar .bottomBar_line em[data-v-383ea534] {\n  margin: 0 0.2rem;\n}\n.share .bottomBar .bottomBar_line span[data-v-383ea534] {\n  width: 1.0rem;\n  height: 1px;\n  display: block;\n  background: #C4C7CC;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.share .bottomBar .bottomBar_content[data-v-383ea534] {\n  width: 92%;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.share .bottomBar .bottomBar_content p[data-v-383ea534] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n}\n.share .bottomBar .bottomBar_content p img[data-v-383ea534] {\n  width: 1.0rem;\n  height: 1.0rem;\n  display: block;\n  margin: 0 auto 0.2rem;\n}\n.share .bottomBar .bottomBar_content p span[data-v-383ea534] {\n  display: block;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  font-weight: 700;\n}\n.share .share-tips[data-v-383ea534] {\n  position: fixed;\n  top: 2.89rem;\n  left: 1.16rem;\n  z-index: 3;\n  width: 5.2rem;\n  height: 3.04rem;\n  background: #FFFFFF;\n  border-radius: 4px;\n}\n.share .share-tips ul[data-v-383ea534] {\n  margin: .34rem .3rem 0;\n}\n.share .share-tips ul li[data-v-383ea534] {\n  height: .84rem;\n  padding-bottom: .34rem;\n}\n.share .share-tips ul li .tips-img[data-v-383ea534] {\n  position: relative;\n  width: 1.12rem;\n  height: .84rem;\n}\n.share .share-tips ul li .tips-img .first-img[data-v-383ea534] {\n  width: .84rem;\n  height: .84rem;\n  float: left;\n}\n.share .share-tips ul li .tips-img .last-img[data-v-383ea534] {\n  position: absolute;\n  top: -0.1rem;\n  right: 0;\n  width: .5rem;\n  height: .28rem;\n}\n.share .share-tips ul li .tips-text p[data-v-383ea534] {\n  font-size: .26rem;\n  color: rgba(61, 74, 91, 0.6);\n}\n.share .share-tips ul li .tips-text p[data-v-383ea534]:first-child {\n  font-weight: 700;\n  font-size: .3rem;\n  color: #3D4A5B;\n}\n.share .share-tips ul li[data-v-383ea534]:nth-child(2) {\n  border-top: 2px solid #F4F5FB;\n  padding-top: .3rem;\n}\n.share .masking-layer[data-v-383ea534] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.6);\n}\n", ""]);

// exports


/***/ }),

/***/ 1856:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "share"
  }, [_c('div', {
    ref: "share",
    staticClass: "shareSwiper"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.imgList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file/downloadFile?filePath=' + i,
        "alt": ""
      }
    })])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]), _vm._v(" "), _c('div', {
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [(_vm.typeList.length > 1) ? _c('p', {
    on: {
      "click": function($event) {
        _vm.shareWechatSession = true
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(759)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]) : _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatSession()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(759)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), (_vm.typeList.length > 1) ? _c('p', {
    on: {
      "click": function($event) {
        _vm.shareWechatTimeline = true
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(758)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])]) : _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatTimeline()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(758)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.saveImage()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(825)
    }
  }), _vm._v(" "), _c('span', [_vm._v("保存图片")])])])]), _vm._v(" "), (_vm.shareWechatSession) ? _c('div', {
    staticClass: "share-tips"
  }, [_c('ul', _vm._l((_vm.typeList), function(e, index) {
    return _c('li', {
      key: index,
      on: {
        "click": function($event) {
          return _vm.shareSession(e)
        }
      }
    }, [_c('div', {
      staticClass: "tips-img fl"
    }, [_c('img', {
      staticClass: "first-img",
      attrs: {
        "src": e.img,
        "alt": ""
      }
    }), _vm._v(" "), (e.recommend == true) ? _c('img', {
      staticClass: "last-img",
      attrs: {
        "src": __webpack_require__(775),
        "alt": ""
      }
    }) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "tips-text fl"
    }, [_c('p', [_vm._v(_vm._s(e.name))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(e.desc))])])])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.shareWechatTimeline) ? _c('div', {
    staticClass: "share-tips"
  }, [_c('ul', _vm._l((_vm.typeList), function(e, index) {
    return _c('li', {
      key: index,
      on: {
        "click": function($event) {
          return _vm.shareTimeline(e)
        }
      }
    }, [_c('div', {
      staticClass: "tips-img fl"
    }, [_c('img', {
      staticClass: "first-img",
      attrs: {
        "src": e.img,
        "alt": ""
      }
    }), _vm._v(" "), (e.recommend == true) ? _c('img', {
      staticClass: "last-img",
      attrs: {
        "src": __webpack_require__(775),
        "alt": ""
      }
    }) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "tips-text fl"
    }, [_c('p', [_vm._v(_vm._s(e.name))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(e.desc))])])])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.shareWechatSession || _vm.shareWechatTimeline) ? _c('div', {
    staticClass: "masking-layer",
    on: {
      "click": _vm.closeShare
    }
  }) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("分享至")]), _vm._v(" "), _c('span')])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-383ea534", module.exports)
  }
}

/***/ }),

/***/ 2003:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1542);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2435a404", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-383ea534&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-383ea534&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 638:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2003)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1435),
  /* template */
  __webpack_require__(1856),
  /* scopeId */
  "data-v-383ea534",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\shareMerchantReward.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] shareMerchantReward.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-383ea534", Component.options)
  } else {
    hotAPI.reload("data-v-383ea534", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 758:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 759:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ }),

/***/ 775:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/corner_mark_recommend.png?v=2c555d16";

/***/ }),

/***/ 825:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_save.png?v=609461df";

/***/ })

});