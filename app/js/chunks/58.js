webpackJsonp([58],{

/***/ 1362:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            shareShow: false,
            isWechat: common.isWeiXin(),
            listAry: [],
            selectObject: {}
        };
    },

    methods: {
        closeShareShadow: function closeShareShadow() {
            this.shareShow = false;
        },
        shareOrDownload: function shareOrDownload(item) {
            this.selectObject = item;
            if (this.isWechat) {
                common.youmeng("下载中心", "微信-下载-" + item.title);
                setTimeout(function () {
                    window.location.href = item.jumpUrl;
                }, 200);
            } else {
                this.shareShow = true;
            }
        },
        reuqestAppList: function reuqestAppList() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "appVersionInfo/multiBrandAppDownload",
                data: {},
                showLoading: true
            }, function (data) {
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        if (_this.isWechat || el.title !== "逍遥推手") {
                            _this.listAry.push(el);
                        }
                    });
                }
            });
        },
        shareWechat: function shareWechat() {
            common.youmeng("下载中心", "微信-分享-" + this.selectObject.title);
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.selectObject.title,
                "des": this.selectObject.des,
                "thumbnail": this.selectObject.thumbnail,
                "jumpUrl": this.selectObject.jumpUrl,
                "shareType": "url",
                "callbackName": ""

            };
            try {
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                } else {
                    window.android.nativeWechatShare(JSON.stringify(shareJson));
                }
            } catch (error) {}
        },
        sharePengyouquan: function sharePengyouquan() {
            common.youmeng("下载中心", "朋友圈-分享-" + this.selectObject.title);
            var shareJson = {
                "type": "wechatTimeline",
                "image": "",
                "title": this.selectObject.title,
                "des": this.selectObject.des,
                "thumbnail": this.selectObject.thumbnail,
                "jumpUrl": this.selectObject.jumpUrl,
                "shareType": "url",
                "callbackName": ""

            };
            try {
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
                } else {
                    window.android.nativeWechatShare(JSON.stringify(shareJson));
                }
            } catch (error) {}
        },
        downloadItem: function downloadItem() {
            var _this2 = this;

            common.youmeng("下载中心", "app-下载-" + this.selectObject.title);
            setTimeout(function () {
                window.location.href = _this2.selectObject.jumpUrl;
            }, 200);
        }
    },
    mounted: function mounted() {
        common.youmeng("下载中心", "进入页面");
        this.reuqestAppList();
    }
};

/***/ }),

/***/ 1508:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.down-load-center[data-v-5247cbed]{\n    background: #ffffff;\n}\n.list-back[data-v-5247cbed]{\n    padding: .96rem .32rem .32rem .32rem;\n}\n#down-load-list[data-v-5247cbed]{\n    padding-bottom: 1px;\n}\n#down-load-list>li[data-v-5247cbed]{\n   padding: .4rem 0;\n   background: #ffffff;\n   border-radius: .08rem;\n   box-shadow: 0 .04rem .36rem 0 #EBEDF5;\n   position: relative;\n   margin-bottom: .32rem;\n}\n.item-icon[data-v-5247cbed]{\n    position: absolute;\n    left: .32rem;\n    top: .4rem;\n    height: .8rem;\n    width: .8rem;\n    display: block;\n}\n.recommend-tip[data-v-5247cbed]{\n    position: absolute;\n    left: .92rem;\n    top: .28rem;\n    width: .5rem;\n    height: .28rem;\n    display: block;\n}\n.wechat-space[data-v-5247cbed]{\n    margin: 0 1.65rem 0 1.4rem;\n}\n.nomer-space[data-v-5247cbed]{\n    margin: 0 1.2rem 0 1.4rem;\n}\n.item-name[data-v-5247cbed]{\n    color: #434D5A;\n    font-weight: 500;\n    font-size: .32rem;\n    line-height: .44rem;\n}\n.item-des[data-v-5247cbed]{\n    color: #434D5A;\n    opacity: 0.6;\n    font-size: .26rem;\n}\n.down-load-tip[data-v-5247cbed]{\n    font-size: .24rem;\n    color: #ffffff;\n    background: #3F83FF;\n    height: .48rem;\n    width: 1.2rem;\n    line-height: .48rem;\n    border-radius: .24rem;\n    position: absolute;\n    right: .32rem;\n    top: 50%;\n    margin-top: -.24rem;\n    text-align: center;\n}\n.more-tip[data-v-5247cbed]{\n    position: absolute;\n    right: .32rem;\n    top: 50%;\n    display: block;\n    width: .08rem;\n    height: .44rem;\n    margin-top: -.22rem;\n}\n.share-shadow[data-v-5247cbed]{\n    position: fixed;\n    height: 100%;\n    width: 100%;\n    top: 0;\n    left: 0;\n    background: rgba(0, 0, 0, 0.65);\n}\n.share-content[data-v-5247cbed]{\n    position: absolute;\n    left: 0;\n    bottom: 0;\n    width: 100%;\n    background: #ffffff;\n    padding: .88rem 0;\n}\n.share-back[data-v-5247cbed]{\n    display: -webkit-flex;\n    display: flex;\n    width: 92%;\n    margin-left: 4%;\n}\n.share-item[data-v-5247cbed]{\n    flex: 1;\n}\n.share-item>img[data-v-5247cbed]{\n    display: block;\n    height: 1rem;\n    width: 1rem;\n    margin: 0 auto;\n}\n.share-item>p[data-v-5247cbed]{\n    color: #3D4A5B;\n    font-size: .24rem;\n    text-align: center;\n    margin-top: .16rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1602:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_operate.png?v=5e7bcd4a";

/***/ }),

/***/ 1790:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "down-load-center"
  }, [_c('div', {
    staticClass: "list-back"
  }, [_c('ul', {
    attrs: {
      "id": "down-load-list"
    }
  }, _vm._l((_vm.listAry), function(item, index) {
    return _c('li', {
      key: index,
      on: {
        "click": function($event) {
          return _vm.shareOrDownload(item)
        }
      }
    }, [_c('img', {
      staticClass: "item-icon",
      attrs: {
        "src": item.thumbnail,
        "alt": ""
      }
    }), _vm._v(" "), (item.isRecommend == 'Y') ? _c('img', {
      staticClass: "recommend-tip",
      attrs: {
        "src": __webpack_require__(755),
        "alt": ""
      }
    }) : _vm._e(), _vm._v(" "), _c('div', {
      staticClass: "item-info",
      class: _vm.isWechat ? 'wechat-space' : 'nomer-space'
    }, [_c('p', {
      staticClass: "item-name"
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('p', {
      staticClass: "item-des"
    }, [_vm._v(_vm._s(item.des))])]), _vm._v(" "), (_vm.isWechat) ? _c('p', {
      staticClass: "down-load-tip"
    }, [_vm._v("下载APP")]) : _c('img', {
      staticClass: "more-tip",
      attrs: {
        "src": __webpack_require__(1602),
        "alt": ""
      }
    })])
  }), 0)]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.shareShow),
      expression: "shareShow"
    }],
    staticClass: "share-shadow",
    on: {
      "click": _vm.closeShareShadow
    }
  }, [_c('div', {
    staticClass: "share-content"
  }, [_c('div', {
    staticClass: "share-back"
  }, [_c('div', {
    staticClass: "share-item",
    on: {
      "click": _vm.shareWechat
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(739),
      "alt": ""
    }
  }), _vm._v(" "), _vm._m(0)]), _vm._v(" "), _c('div', {
    staticClass: "share-item",
    on: {
      "click": _vm.sharePengyouquan
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(738),
      "alt": ""
    }
  }), _vm._v(" "), _vm._m(1)]), _vm._v(" "), _c('div', {
    staticClass: "share-item",
    on: {
      "click": _vm.downloadItem
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(808),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("下载APP")])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("分享下载链接"), _c('br'), _vm._v("至微信好友")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_vm._v("分享下载链接"), _c('br'), _vm._v("至微信朋友圈")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5247cbed", module.exports)
  }
}

/***/ }),

/***/ 1924:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1508);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("4425756c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5247cbed&scoped=true!./downloadCenter.css", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5247cbed&scoped=true!./downloadCenter.css");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 577:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1924)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1362),
  /* template */
  __webpack_require__(1790),
  /* scopeId */
  "data-v-5247cbed",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\downloadCenter\\downloadCenter.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] downloadCenter.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5247cbed", Component.options)
  } else {
    hotAPI.reload("data-v-5247cbed", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 738:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 739:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/corner_mark_recommend.png?v=2c555d16";

/***/ }),

/***/ 808:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_save.png?v=609461df";

/***/ })

});