webpackJsonp([12],{

/***/ 316:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(464)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(370),
  /* template */
  __webpack_require__(440),
  /* scopeId */
  "data-v-cf75043a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\machinesToolsApply\\machinesToolsIsApply.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] machinesToolsIsApply.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cf75043a", Component.options)
  } else {
    hotAPI.reload("data-v-cf75043a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon-row@2x.png?v=f5c6877b";

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon-xia@2x.png?v=edbec9e6";

/***/ }),

/***/ 370:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

var _vant = __webpack_require__(66);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            show: true,
            radio: '',
            productName: '',
            productCode: '',
            title: '产品品牌',
            myPicker: false,
            addArray: []
        };
    },

    methods: {
        init: function init() {
            var _this = this;
            _this.proApplyList();
        },
        proApplyList: function proApplyList() {
            var _this2 = this;

            var _this = this;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "performance/findProduct",
                "type": "post"
            }, function (data) {
                _this.machinesArray = data.data;
                _this2.radio = _this.machinesArray[0].productCode;
                for (var i in _this.machinesArray) {
                    _this.productName = _this.machinesArray[0].productName;
                }
            });
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/list",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    pageNum: _this.pageNum
                }
            }, function (data) {
                for (var i in data.data) {
                    if (_this2.$route.query && _this2.$route.query.addressId && _this2.$route.query.addressId == data.data[i].id) {
                        _this.addArray = [data.data[i]];
                    } else if (data.data[i].isDefault) {
                        _this.addArray = [data.data[i]];
                    }
                }
            });
        },
        onQuery: function onQuery() {
            this.myPicker = false;
            for (var i in this.machinesArray) {
                if (this.machinesArray[i].productCode == this.radio) {
                    this.productName = this.machinesArray[i].productName;
                }
            }
        },
        applyCheck: function applyCheck() {
            var _this3 = this;

            var _this = this;
            if (!_this.addArray[0] || !_this.addArray[0].id) {
                (0, _vant.Toast)('请选择收货地址');
                return;
            }
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posApply/toApply",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey(),
                    productCode: _this.radio,
                    type: 'NEWAPPLY',
                    addressId: _this.addArray[0].id
                }
            }, function (data) {
                if (data.code == '0000') {
                    _this3.$router.push({
                        "path": "applyRecord"
                    });
                }
                //成功后
            });
        },
        onAdressList: function onAdressList() {
            this.$router.push({
                "path": "adressList"
            });
        },
        onAdress: function onAdress() {
            this.$router.push({
                "path": "addAdress"
            });
        }
    },
    created: function created() {
        var _this = this;
        _this.init();
    },
    mounted: function mounted() {}
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-cf75043a] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-cf75043a] {\n  background: #fff;\n}\n.tips_success[data-v-cf75043a] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-cf75043a] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-cf75043a] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-cf75043a],\n.fade-leave-active[data-v-cf75043a] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-cf75043a],\n.fade-leave-to[data-v-cf75043a] {\n  opacity: 0;\n}\n.default_button[data-v-cf75043a] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-cf75043a] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-cf75043a] {\n  position: relative;\n}\n.loading-tips[data-v-cf75043a] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.machinesApply[data-v-cf75043a] {\n  overflow: hidden;\n  font-size: 0.287rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #333333;\n}\n.machinesApply .machinesApply-is[data-v-cf75043a] {\n  background: #FFFFFF;\n  padding: 0.3rem 0.293rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.287rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #333333;\n  border-top: 0.013rem solid #F7F8FA;\n}\n.machinesApply .machinesApply-is .yes-tools[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.machinesApply .machinesApply-is .yes-tools .yes-img[data-v-cf75043a] {\n  width: 0.25rem;\n  height: 0.333rem;\n  background: url(" + __webpack_require__(342) + ") no-repeat;\n  background-size: 0.25rem 0.333rem;\n  margin-left: 0.26rem;\n}\n.machinesApply .machinesApply-adress[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #FFFFFF;\n  padding: 0.25rem 0.25rem;\n  border-top: 0.013rem solid #F7F8FA;\n}\n.machinesApply .machinesApply-adress .machinesApply-adress-left .changePending-center-infor[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.257rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #b6b6bc;\n}\n.machinesApply .machinesApply-adress .machinesApply-adress-left .changePending-center-infor .pendName[data-v-cf75043a] {\n  margin-right: 0.15rem;\n}\n.machinesApply .machinesApply-adress .machinesApply-adress-right[data-v-cf75043a] {\n  width: 0.25rem;\n  height: 0.293rem;\n  background: url(" + __webpack_require__(329) + ") no-repeat;\n  background-size: 0.25rem 0.293rem;\n}\n.machinesApply .machinesApply-noadress[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #FFFFFF;\n  padding: 0.25rem 0.25rem;\n  border-top: 0.013rem solid #F7F8FA;\n}\n.machinesApply .machinesApply-noadress .machinesApply-adress-left[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n  -webkit-justify-content: flex-start;\n     -moz-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.machinesApply .machinesApply-noadress .machinesApply-adress-right[data-v-cf75043a] {\n  width: 0.25rem;\n  height: 0.293rem;\n  background: url(" + __webpack_require__(329) + ") no-repeat;\n  background-size: 0.25rem 0.293rem;\n}\n.machinesApply .machinesApply-yes[data-v-cf75043a] {\n  background: #FFFFFF;\n  padding: 0.3rem 0.293rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.287rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #333333;\n  border-top: 0.013rem solid #F7F8FA;\n}\n.machinesApply .machinesApply-yes .yes-tools[data-v-cf75043a] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n  -webkit-justify-content: flex-end;\n     -moz-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n.machinesApply .machinesApply-yes .yes-tools input[data-v-cf75043a] {\n  text-align: right !important;\n}\n.machinesApply .machinesApply-yes .yes-tools .yes-img[data-v-cf75043a] {\n  width: 0.25rem;\n  height: 0.333rem;\n  background: url(" + __webpack_require__(342) + ") no-repeat;\n  background-size: 0.25rem 0.333rem;\n  margin-left: 0.26rem;\n}\n.machinesApply .machinesApply-inform[data-v-cf75043a] {\n  height: 1rem;\n  width: 100%;\n  position: fixed;\n  bottom: 0;\n}\n.machinesApply .machinesApply-inform .van-button[data-v-cf75043a] {\n  height: 100% !important;\n  font-size: 0.32rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #FFFFFF;\n}\n.machinesApply .myPicker[data-v-cf75043a] {\n  height: 100%;\n  width: 100%;\n  position: fixed;\n  bottom: 0;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  background: rgba(0, 0, 0, 0.6);\n}\n.machinesApply .myPicker .content[data-v-cf75043a] {\n  height: 7.56rem;\n  width: 100%;\n  position: fixed;\n  bottom: 0;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  background: #ffffff;\n}\n.machinesApply .myPicker .content .myPicker-top[data-v-cf75043a] {\n  height: 1rem;\n  width: 100%;\n  font-size: 0.36rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #999999;\n  line-height: 1rem;\n  text-align: center;\n}\n.machinesApply .myPicker .content .myPicker-top .close[data-v-cf75043a] {\n  width: 0.31rem;\n  height: 0.31rem;\n  position: absolute;\n  right: 0.52rem;\n  top: 0px;\n  font-size: 0.5rem;\n}\n.machinesApply .myPicker .content .myPicker-center[data-v-cf75043a] {\n  height: 5.56rem;\n  width: 100%;\n  overflow-y: scroll;\n}\n.machinesApply .myPicker .content .myPicker-center .myPicker-center-list[data-v-cf75043a] {\n  width: 6.72rem;\n  height: 1rem;\n  margin: 0 auto;\n  border-bottom: 0.02rem solid rgba(51, 51, 51, 0.1);\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0 0.32rem;\n}\n.machinesApply .myPicker .content .myPicker-center .myPicker-center-list .text[data-v-cf75043a] {\n  font-size: 0.3rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #3F3F50;\n}\n.machinesApply .myPicker .content .myPicker-bottom[data-v-cf75043a] {\n  height: 1rem;\n  width: 100%;\n  background: #F3782C;\n  font-size: 0.32rem;\n  font-family: PingFangSC-Regular, PingFang SC;\n  font-weight: 400;\n  color: #FFFFFF;\n  line-height: 1rem;\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 440:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "machinesApply"
  }, [_c('div', [_c('div', {
    staticClass: "machinesApply-is"
  }, [_c('div', {
    staticClass: "yes-title"
  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('div', {
    staticClass: "yes-tools"
  }, [_c('div', {
    staticClass: "yes-text"
  }, [_vm._v(_vm._s(_vm.productName))]), _vm._v(" "), _c('div', {
    staticClass: "yes-img",
    on: {
      "click": function($event) {
        _vm.myPicker = true
      }
    }
  })])]), _vm._v(" "), _vm._l((_vm.addArray), function(item, index) {
    return (_vm.addArray.length > 0) ? _c('div', {
      key: index,
      staticClass: "machinesApply-adress"
    }, [_c('div', {
      staticClass: "machinesApply-adress-left"
    }, [_c('div', {
      staticClass: "yes-title"
    }, [_vm._v(_vm._s(item.province) + _vm._s(item.city) + _vm._s(item.area) + _vm._s(item.addressDetail))]), _vm._v(" "), _c('div', {
      staticClass: "changePending-center-infor"
    }, [_c('div', {
      staticClass: "pendName"
    }, [_vm._v(_vm._s(item.receivePerson))]), _vm._v(" "), _c('div', {
      staticClass: "phoneNo"
    }, [_vm._v(_vm._s(item.phoneNo))])])]), _vm._v(" "), _c('div', {
      staticClass: "machinesApply-adress-right",
      on: {
        "click": _vm.onAdressList
      }
    })]) : _vm._e()
  }), _vm._v(" "), (_vm.addArray.length == 0) ? _c('div', {
    staticClass: "machinesApply-noadress",
    on: {
      "click": _vm.onAdress
    }
  }, [_c('div', {
    staticClass: "machinesApply-adress-left"
  }, [_c('van-icon', {
    staticStyle: {
      "margin-right": "0.1rem"
    },
    attrs: {
      "name": "plus"
    }
  }), _vm._v("新增收货地址\n                ")], 1), _vm._v(" "), _c('div', {
    staticClass: "machinesApply-adress-right"
  })]) : _vm._e()], 2), _vm._v(" "), _c('div', {
    staticClass: "machinesApply-inform"
  }, [_c('van-button', {
    attrs: {
      "color": "#F3782C",
      "type": "primary",
      "block": ""
    },
    on: {
      "click": _vm.applyCheck
    }
  }, [_vm._v("提交信息")])], 1), _vm._v(" "), (_vm.myPicker) ? _c('div', {
    staticClass: "myPicker"
  }, [_c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "myPicker-top"
  }, [_vm._v("\n                    选择产品\n                    "), _c('div', {
    staticClass: "close",
    on: {
      "click": function($event) {
        _vm.myPicker = false
      }
    }
  }, [_vm._v("×")])]), _vm._v(" "), _c('van-radio-group', {
    model: {
      value: (_vm.radio),
      callback: function($$v) {
        _vm.radio = $$v
      },
      expression: "radio"
    }
  }, [_c('div', {
    staticClass: "myPicker-center"
  }, _vm._l((_vm.machinesArray), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "myPicker-center-list"
    }, [_c('div', {
      staticClass: "text"
    }, [_vm._v(_vm._s(item.productName))]), _vm._v(" "), _c('van-radio', {
      attrs: {
        "name": item.productCode,
        "checked-color": "#F3782C"
      },
      on: {
        "click": function($event) {
          _vm.radio = item.productCode
        }
      }
    })], 1)
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "myPicker-bottom",
    on: {
      "click": _vm.onQuery
    }
  }, [_vm._v("确认")])], 1)]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-cf75043a", module.exports)
  }
}

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(402);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("d5889236", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cf75043a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./machinesToolsIsApply.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cf75043a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./machinesToolsIsApply.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});