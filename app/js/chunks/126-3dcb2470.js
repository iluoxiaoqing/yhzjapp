webpackJsonp([126],{

/***/ 1400:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

common.youmeng("政策解读", "政策解读更多");

exports.default = {
    data: function data() {
        return {
            menuList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            caseList: [],
            currentPage: 1,
            id: ""
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        window.goBack = this.goBack;
        this.nativeWebviewDisabledBounces();
        this.getHeight();
        this.getLiist();
    },

    methods: {
        nativeWebviewDisabledBounces: function nativeWebviewDisabledBounces() {
            if (common.isClient() == "ios") {
                try {
                    window.webkit.messageHandlers.nativeWebviewDisabledBounces.postMessage('');
                } catch (err) {
                    console.log("err", "出错了");
                }
            } else {}
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        loadmore: function loadmore() {
            this.getCaseList(this.id);
        },
        refresh: function refresh() {
            this.getCaseList(this.id);
        },
        changeGoods: function changeGoods(i, index) {
            this.caseList = [];
            this.menuList.map(function (el) {
                el.active = false;
                el.page = 1;
                el.caseList = [];
            });
            this.menuList[index].active = true;
            i.active = true;
            this.id = this.menuList[index].id;
            this.getCaseList(this.menuList[index].id);
            common.youmeng("推手案例", "推手案例更多");
        },
        getLiist: function getLiist() {
            var _this = this;

            console.log("this.menuList", this.menuList);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/group",
                data: {},
                showLoading: false
            }, function (data) {
                _this.menuList = data.data;
                _this.menuList[0].active = true;
                _this.menuList[0].page = 1;
                _this.index = 0;
                _this.menuList[0].caseList = [];
                _this.id = _this.menuList[0].id;
                _this.getCaseList(_this.id);
            });
        },
        getCaseList: function getCaseList(u) {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/caseList",
                data: {
                    "id": u,
                    "page": this.menuList[this.index].page
                },
                showLoading: false
            }, function (data) {
                var curPage = _this2.menuList[_this2.index].page;
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        _this2.$set(el, "show", false);
                        _this2.caseList.push(el);
                    });
                    curPage++;
                    _this2.$set(_this2.menuList[_this2.index], "page", curPage);
                    _this2.$set(_this2.menuList[_this2.index], "isNoData", false);
                } else {
                    if (curPage == 1) {
                        _this2.caseList = data.data;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1575:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\handPushCase\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1888:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mall"
  }, [_c('div', {
    ref: "mall_wrap",
    staticClass: "mall_wrap"
  }, [_c('div', {
    staticClass: "mall_content"
  }, [_c('div', {
    staticClass: "mall_left"
  }, _vm._l((_vm.menuList), function(i, index) {
    return _c('span', {
      key: index,
      class: {
        'active': i.active
      },
      on: {
        "click": function($event) {
          return _vm.changeGoods(i, index)
        }
      }
    }, [_c('img', {
      attrs: {
        "src": './image/icon_' + index + '.png',
        "alt": ""
      }
    }), _vm._v("\n\t\t\t\t\t" + _vm._s(i.name) + "\n\t\t\t\t")])
  }), 0), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "mall_right"
  }, _vm._l((_vm.caseList), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('router-link', {
      attrs: {
        "to": {
          name: 'caseDetail',
          query: {
            id: j.id,
            title: j.title
          }
        }
      }
    }, [_c('div', {
      staticClass: "goodsDeatil"
    }, [_c('p', [_vm._v(_vm._s(j.title))])])])], 1)
  }), 0)])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-60f9c53b", module.exports)
  }
}

/***/ }),

/***/ 2036:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1575);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("712f41ba", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-60f9c53b&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseMore.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-60f9c53b&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseMore.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 596:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2036)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1400),
  /* template */
  __webpack_require__(1888),
  /* scopeId */
  "data-v-60f9c53b",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\handPushCase\\caseMore.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] caseMore.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-60f9c53b", Component.options)
  } else {
    hotAPI.reload("data-v-60f9c53b", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});