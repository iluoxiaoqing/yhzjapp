webpackJsonp([92],{

/***/ 1440:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import fixHead from "@/components/mHeader.vue";

exports.default = {
    data: function data() {
        return {
            addresseList: [],
            from: this.$route.query.from,
            dispayInNative: this.$route.query.dispayInNative
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.$route.query.appTitle == "" || this.$route.query.appTitle == undefined) {
            document.title = "选择";
        } else {
            document.title = this.$route.query.appTitle;
        }

        common.youmeng("地址选择列表", "进入地址选择列表");

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "addr/list"
        }, function (data) {
            _this.addresseList = data.data;
        });

        window.goBack = this.goBack;
    },

    methods: {
        goBack: function goBack() {

            if (this.$route.query.from == "APP") {
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeCloseCurrentPage.postMessage("关闭窗口");
                } else {
                    window.android.nativeCloseCurrentPage("关闭窗口");
                }
            } else {
                this.$router.push({
                    "path": "mall_order"
                });
            }
        },
        gotoOrder: function gotoOrder(i) {

            if (this.$route.query.from == "APP") {
                return;
            }

            this.$store.dispatch("saveAddress", i);

            this.$router.push({
                "path": "mall_order",
                "query": {
                    "from": this.$route.query.from
                }
            });
        },
        addYoumeng: function addYoumeng() {
            common.youmeng("地址选择列表", "点击添加地址辑按钮");
            this.$router.push({
                "path": "addAddress",
                "query": {
                    "dispayInNative": this.$route.query.dispayInNative
                }
            });
        },
        gotoEdit: function gotoEdit(i) {

            this.$store.dispatch("saveAddress", i);

            this.$router.push({
                "path": "editAddress",
                "query": {
                    "from": this.$route.query.from,
                    "dispayInNative": this.$route.query.dispayInNative
                }
            });

            common.youmeng("地址选择列表", "点击编辑按钮");
        }
    }
};

/***/ }),

/***/ 1605:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1918:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "addressList"
  }, [_vm._l((_vm.addresseList), function(i) {
    return _c('div', {
      staticClass: "item"
    }, [(_vm.dispayInNative == 1) ? _c('div', [_c('p', [_vm._v(_vm._s(i.receivePerson) + " " + _vm._s(i.phoneNo))]), _vm._v(" "), _c('span', [(i.isDefault) ? _c('i') : _vm._e(), _vm._v("\n\t\t\t\t\t" + _vm._s(i.province) + _vm._s(i.city) + _vm._s(i.addressDetail) + "\n\t\t\t\t")])]) : _c('div', {
      on: {
        "click": function($event) {
          return _vm.gotoOrder(i)
        }
      }
    }, [_c('p', [_vm._v(_vm._s(i.receivePerson) + " " + _vm._s(i.phoneNo))]), _vm._v(" "), _c('span', [(i.isDefault) ? _c('i') : _vm._e(), _vm._v(" "), _c('em', [_vm._v(_vm._s(i.province) + _vm._s(i.city) + _vm._s(i.addressDetail))])])]), _vm._v(" "), _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.gotoEdit(i)
        }
      }
    }, [_vm._v("编辑")])])
  }), _vm._v(" "), _c('div', {
    staticClass: "submit"
  }, [_c('a', {
    staticClass: "default_button",
    on: {
      "click": function($event) {
        return _vm.addYoumeng()
      }
    }
  }, [_vm._v("添加地址")])])], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-9e84a1ee", module.exports)
  }
}

/***/ }),

/***/ 2066:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1605);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("79ffc446", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-9e84a1ee&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-9e84a1ee&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 643:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2066)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1440),
  /* template */
  __webpack_require__(1918),
  /* scopeId */
  "data-v-9e84a1ee",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\addressList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] addressList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9e84a1ee", Component.options)
  } else {
    hotAPI.reload("data-v-9e84a1ee", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});