webpackJsonp([41],{

/***/ 1419:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(755);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            menuArray: [{
                title: "审核中",
                active: true
            }, {
                title: "审核成功",
                active: false
            }, {
                title: "审核失败",
                active: false
            }],
            cardList: [],
            isLoading: true,
            currentPage: 1,
            mySwiper: null,
            index: 0,
            defaultText: "",
            hasData: false,
            minHeight: 0,
            allCardList: [{
                "cardList": [],
                "page": 1,
                "isNoData": false
            }, {
                "cardList": [],
                "page": 1,
                "isNoData": false
            }, {
                "cardList": [],
                "page": 1,
                "isNoData": false
            }]
        };
    },

    components: {
        searchBox: _searchBox2.default,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default

    },
    mounted: function mounted() {
        var _this = this;

        window.goBack = this.goBack;
        this.$refs.searchBox.inputValue = "";

        this.getHeight();

        var type = this.$route.query.type || 0;
        this.index = type;

        if (this.allCardList[this.index].cardList.length == 0) {
            this.getPageList();
        }

        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            autoHeight: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {

                document.getElementById("scrollContainer").scrollTop = 0;
                document.getElementById("scrollContainer").setAttribute("scrollTop", 0);

                var index = swiper.activeIndex;
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.menuArray[index].active = true;
                _this.$refs.searchBox.inputValue = "";
                _this.index = index;

                if (_this.allCardList[_this.index].cardList.length == 0) {
                    _this.getPageList();
                }
                // this.getList();
            }
        });

        this.changeList(this.menuArray[type], type);
    },

    methods: {
        changeList: function changeList(i, index) {

            this.index = index;
            this.$refs.searchBox.inputValue = "";
            this.menuArray.map(function (el) {
                el.active = false;
            });
            i.active = true;
            this.mySwiper.slideTo(index, 500, true);
            //this.mySwiper.onResize();
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
            this.minHeight = bodyHeight - scrollerTop + "px";
        },
        infinite: function infinite() {
            this.getPageList();
        },
        refresh: function refresh() {
            this.allCardList[this.index].page = 1;
            this.allCardList[this.index].cardList = [];
            this.getPageList();
        },
        searchBtn: function searchBtn(search) {
            this.allCardList[this.index].page = 1;
            this.allCardList[this.index].cardList = [];
            this.getPageList();
        },
        getPageList: function getPageList() {
            var _this2 = this;

            var billStatus = "";

            if (this.index == 0) {
                billStatus = "AUDITING";
            } else if (this.index == 1) {
                billStatus = "SUCCESS";
            } else {
                billStatus = "REJECT";
            }

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "loanBill/queryLoanBill",
                data: {
                    "page": this.allCardList[this.index].page,
                    "billStatus": billStatus,
                    "userName": this.$refs.searchBox.inputValue
                },
                showLoading: false
            }, function (data) {

                var curPage = _this2.allCardList[_this2.index].page;
                var Index = _this2.index;

                if (data.data.object.length > 0) {

                    data.data.object.map(function (el) {
                        _this2.$set(el, "show", true);
                        _this2.allCardList[Index].cardList.push(el);
                    });

                    curPage++;

                    _this2.$set(_this2.allCardList[Index], "isNoData", false);
                    _this2.$set(_this2.allCardList[Index], "page", curPage);
                } else {
                    if (curPage == 1) {
                        _this2.allCardList[Index].isNoData = true;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        }
    }
};

/***/ }),

/***/ 1527:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1841:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('div', {
    staticClass: "queryHead"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.menuArray), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.title))])
  }), 0), _vm._v(" "), _c('search-box', {
    ref: "searchBox",
    attrs: {
      "placeholderText": '请输入姓名搜索',
      "type": false
    },
    on: {
      "confirm": _vm.searchBtn
    }
  })], 1), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.infinite
    }
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? (_vm.allCardList[0].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allCardList[0].cardList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.productName))]), _vm._v(" "), _c('b', [_vm._v("审核中")])]), _vm._v(" "), _c('em', [_vm._v("姓名：" + _vm._s(i.custName))]), _vm._v(" "), _c('em', [_vm._v("电话：" + _vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t\t\t\t" + _vm._s(i.createTime) + "\n\t\t\t\t\t\t\t\t\t")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allCardList[0].isNoData
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 1 ? (_vm.allCardList[1].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allCardList[1].cardList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.productName))]), _vm._v(" "), _c('b', [_vm._v("贷款成功")])]), _vm._v(" "), _c('em', [_vm._v("姓名：" + _vm._s(i.custName))]), _vm._v(" "), _c('em', [_vm._v("电话：" + _vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t\t\t\t" + _vm._s(i.createTime) + "\n\t\t\t\t\t\t\t\t\t")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allCardList[1].isNoData
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 2 ? (_vm.allCardList[2].isNoData ? _vm.minHeight : 'auto') : '0px'
    })
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.allCardList[2].cardList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.productName))]), _vm._v(" "), _c('b', {
      staticStyle: {
        "color": "#E95647"
      }
    }, [_vm._v("贷款失败")])]), _vm._v(" "), _c('em', [_vm._v("姓名：" + _vm._s(i.custName))]), _vm._v(" "), _c('em', [_vm._v("电话：" + _vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t\t\t\t" + _vm._s(i.createTime) + "\n\t\t\t\t\t\t\t\t\t")])])
  }), 0), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.allCardList[2].isNoData
    }
  })], 1)])])])], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-25d92ffb", module.exports)
  }
}

/***/ }),

/***/ 1988:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1527);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7128face", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-25d92ffb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanOrder.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-25d92ffb&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanOrder.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 618:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1988)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1419),
  /* template */
  __webpack_require__(1841),
  /* scopeId */
  "data-v-25d92ffb",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\loanOrder.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loanOrder.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-25d92ffb", Component.options)
  } else {
    hotAPI.reload("data-v-25d92ffb", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 751:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(757)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(750),
  /* template */
  __webpack_require__(756),
  /* scopeId */
  "data-v-7cf3f18a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7cf3f18a", Component.options)
  } else {
    hotAPI.reload("data-v-7cf3f18a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7cf3f18a", module.exports)
  }
}

/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(751);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05a769e3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});