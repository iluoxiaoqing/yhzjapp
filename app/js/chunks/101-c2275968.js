webpackJsonp([101],{

/***/ 1477:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            videoList: {},
            bannerList: {},
            imgUrl1: _apiConfig2.default.KY_IP
        };
    },

    components: {},
    mounted: function mounted() {
        this.getBanner();
        this.getList();
    },

    methods: {
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "mediaTeach/group",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this.videoList = data.data;
            });
        },
        getBanner: function getBanner() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "wonderfulActivityInfo/findPageBanner",
                "data": {
                    "bannerType": "TEACH_VIDEO"
                }
            }, function (data) {
                _this2.bannerList = data.data || [];
                _this2.$nextTick(_this2.mySwiper);
            });
        },
        mySwiper: function mySwiper() {
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2000, //可选选项，自动滑动
                loop: true,
                pagination: ".swiper-pagination"
            });
        },
        detail: function detail(id, name) {
            common.youmeng("视频播放", name);
            this.$router.push({
                "path": "video_detail",
                "query": {
                    "id": id
                }
            });
        }
    }
};

/***/ }),

/***/ 1603:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\video\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1771:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/banner_juxing.png?v=3a413061";

/***/ }),

/***/ 1916:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "main"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n\t\t\t为您推荐\n\t\t")]), _vm._v(" "), (_vm.bannerList.length > 0) ? _c('div', {
    staticClass: "shop_banner"
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.bannerList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide"
    }, [_c('a', {
      attrs: {
        "href": item.h5Url
      }
    }, [(item.imgUrl == '') ? _c('img', {
      attrs: {
        "src": __webpack_require__(1771)
      }
    }) : _c('img', {
      attrs: {
        "src": item.imgUrl,
        "alt": ""
      }
    })])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination"
  })])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v("\n\t\t\t课堂分类\n\t\t")]), _vm._v(" "), _c('div', {
    staticClass: "classType"
  }, [_c('div', {
    staticClass: "videoList"
  }, _vm._l((_vm.videoList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item",
      on: {
        "click": function($event) {
          return _vm.detail(i.id, i.name);
        }
      }
    }, [_c('div', {
      staticClass: "item_title"
    }, [_vm._v(_vm._s(i.name))]), _vm._v(" "), _c('div', {
      staticClass: "item_text"
    }, [_vm._v(_vm._s(i.desc))]), _vm._v(" "), _c('img', {
      attrs: {
        "src": './image/video_' + index + '.png',
        "alt": ""
      }
    })])
  }), 0)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-97089bb2", module.exports)
  }
}

/***/ }),

/***/ 2064:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1603);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("372a370d", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-97089bb2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-97089bb2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 688:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2064)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1477),
  /* template */
  __webpack_require__(1916),
  /* scopeId */
  "data-v-97089bb2",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\video\\video.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] video.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-97089bb2", Component.options)
  } else {
    hotAPI.reload("data-v-97089bb2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});