webpackJsonp([5],{

/***/ 1238:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num1.png?v=509e4726";

/***/ }),

/***/ 1239:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num2.png?v=8bacfc23";

/***/ }),

/***/ 1240:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num3.png?v=3e5e7cf3";

/***/ }),

/***/ 1241:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_num4.png?v=e00b3d29";

/***/ }),

/***/ 1290:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '合利宝');
    },

    methods: {
        go: function go() {
            common.setCookie("apiType", '20200709_HELIBAO');
            window.location.href = _apiConfig2.default.WEB_URL + 'jlHkrt';
            // window.location.reload();
        },
        seeJl: function seeJl() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';

            // window.location.reload();
        }
    }
};

/***/ }),

/***/ 1493:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-074296cf] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-074296cf] {\n  background: #fff;\n}\n.tips_success[data-v-074296cf] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-074296cf] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-074296cf] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-074296cf],\n.fade-leave-active[data-v-074296cf] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-074296cf],\n.fade-leave-to[data-v-074296cf] {\n  opacity: 0;\n}\n.default_button[data-v-074296cf] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-074296cf] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-074296cf] {\n  position: relative;\n}\n.loading-tips[data-v-074296cf] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.ybzf_main[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(854) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 14.44rem;\n}\n.ybzf_main .send_btn[data-v-074296cf] {\n  background: url(" + __webpack_require__(855) + ") no-repeat;\n  bottom: 0.5rem;\n  position: absolute;\n  width: 40%;\n  height: 1.2rem;\n  background-size: 100% 100%;\n  left: 10%;\n}\n.ybzf_main .seeJl[data-v-074296cf] {\n  background: url(" + __webpack_require__(856) + ") no-repeat;\n  bottom: 0.5rem;\n  position: absolute;\n  width: 40%;\n  height: 1.2rem;\n  background-size: 100% 100%;\n  right: 10%;\n}\n.haike_main[data-v-074296cf] {\n  width: 100%;\n  height: 32.6rem;\n}\n.haike_main .haike1[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(848) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 12.28rem;\n}\n.haike_main .haike2[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(849) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 10.24rem;\n}\n.haike_main .haike3[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(850) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 9.95rem;\n}\n.haike_main .send_btn[data-v-074296cf] {\n  background: url(" + __webpack_require__(851) + ") no-repeat;\n  bottom: 0.5rem;\n  position: absolute;\n  width: 40%;\n  height: 1.09rem;\n  background-size: 100% 100%;\n  left: 5%;\n  right: 5%;\n}\n.haike_main .seeJl[data-v-074296cf] {\n  background: url(" + __webpack_require__(852) + ") no-repeat;\n  bottom: 0.5rem;\n  position: absolute;\n  width: 40%;\n  height: 1.09rem;\n  background-size: 100% 100%;\n  right: 5%;\n}\n.heli_main[data-v-074296cf] {\n  background: #ff3f3a;\n}\n.heli_main .heli1[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(839) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 13.23rem;\n}\n.heli_main .heli1 .jl[data-v-074296cf] {\n  margin-top: 8.6rem;\n  display: inline-block;\n  margin-left: 1.01rem;\n}\n.heli_main .heli1 .jl span[data-v-074296cf] {\n  width: 194px;\n  height: 47px;\n  font-size: .5rem;\n  font-weight: bold;\n  color: #ffddc9;\n  line-height: 74px;\n  text-shadow: 0px 6px 10px rgba(178, 0, 27, 0.81);\n}\n.heli_main .heli1 .jl em[data-v-074296cf] {\n  width: 335px;\n  height: 106px;\n  font-size: 1.18rem;\n  font-weight: bold;\n  color: #ffd409;\n  text-shadow: 0px 6px 10px rgba(178, 0, 27, 0.81);\n}\n.heli_main .heli1 .time[data-v-074296cf] {\n  width: 96%;\n  background: url(" + __webpack_require__(790) + ") no-repeat;\n  background-size: 100% 100%;\n  margin: 1.55rem 2% 0;\n  height: 1.07rem;\n  line-height: 1.07rem;\n  color: #ffffff;\n  border-radius: 20px;\n  font-size: .4rem;\n  font-weight: bold;\n  text-align: center;\n}\n.heli_main .center_one[data-v-074296cf] {\n  width: 92%;\n  margin: 0.53rem 4% 0;\n  background: url(" + __webpack_require__(840) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.heli_main .center_one .heli_title[data-v-074296cf] {\n  text-align: center;\n  line-height: 0.9rem;\n  font-size: .36rem;\n  font-weight: 500;\n  color: #b56200;\n}\n.heli_main .center_one ul[data-v-074296cf] {\n  line-height: 25px;\n  font-size: .24rem;\n  font-weight: 400;\n  color: #b56200;\n}\n.heli_main .center_one ul li[data-v-074296cf] {\n  width: 92%;\n  margin: 0.2rem 4%;\n  display: inline-block;\n}\n.heli_main .center_one ul li img[data-v-074296cf] {\n  width: 0.46rem;\n  height: 0.46rem;\n}\n.heli_main .center_one ul li em[data-v-074296cf] {\n  right: 1.3rem;\n  position: absolute;\n}\n.heli_main .center_one ul li span[data-v-074296cf] {\n  margin-left: 0.5rem;\n}\n.heli_main .center_one ul li table[data-v-074296cf] {\n  width: 60%;\n  margin: 0 20%;\n  text-align: center;\n  font-size: 0.2rem;\n  -webkit-text-size-adjust: none;\n}\n.heli_main .center_one ul li table[data-v-074296cf],\n.heli_main .center_one ul li table tr th[data-v-074296cf],\n.heli_main .center_one ul li table tr td[data-v-074296cf] {\n  border: 1px solid #B56200;\n  border-collapse: collapse;\n}\n.heli_main .center_one ul li .text[data-v-074296cf] {\n  line-height: .4rem;\n  font-size: .2rem;\n  font-weight: 400;\n  color: #b56200;\n  -webkit-text-size-adjust: none;\n  margin-top: 0.2rem;\n}\n.heli_main .center_one .heli_down[data-v-074296cf] {\n  width: 92%;\n  margin: 0 4% 0.35rem;\n  background: url(" + __webpack_require__(841) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 1.09rem;\n  font-size: .3rem;\n  font-weight: 400;\n  font-style: italic;\n  color: #ffffff;\n  text-align: center;\n  display: inline-block;\n}\n.heli_main .heli3[data-v-074296cf] {\n  display: inline-block;\n  margin-top: 0.2rem;\n}\n.heli_main .heli3 .send_btn_heli[data-v-074296cf] {\n  background: url(" + __webpack_require__(842) + ") no-repeat;\n  bottom: 0.3rem;\n  position: absolute;\n  width: 40%;\n  height: 1.05rem;\n  background-size: 100% 100%;\n  left: 5%;\n  right: 5%;\n}\n.heli_main .heli3 .seeJl_heli[data-v-074296cf] {\n  background: url(" + __webpack_require__(843) + ") no-repeat;\n  bottom: 0.3rem;\n  position: absolute;\n  width: 40%;\n  height: 1.05rem;\n  background-size: 100% 100%;\n  right: 5%;\n}\n.helipos_main[data-v-074296cf] {\n  width: 100%;\n  background: url(" + __webpack_require__(846) + ") no-repeat;\n  background-size: 100% 100%;\n  height: 32rem;\n  overflow: scroll;\n}\n.helipos_main .time_pos[data-v-074296cf] {\n  width: 96%;\n  background: url(" + __webpack_require__(790) + ") no-repeat;\n  background-size: 100% 100%;\n  margin: 9.34rem 2% 0;\n  height: 1.07rem;\n  line-height: 1.07rem;\n  color: #ffffff;\n  border-radius: 20px;\n  font-size: .4rem;\n  font-weight: bold;\n  text-align: center;\n  display: inline-block;\n}\n.helipos_main .title_left img[data-v-074296cf] {\n  width: 3.78rem;\n  height: 0.96rem;\n  margin-top: 0.67rem;\n  display: inline-block;\n}\n.helipos_main .title_right[data-v-074296cf] {\n  width: 3.78rem;\n  height: 0.96rem;\n  margin-top: 0.76rem;\n  display: inline-block;\n  text-align: center;\n  line-height: 0.96rem;\n  font-size: .36rem;\n  font-weight: 500;\n  color: #b56200;\n  background: url(" + __webpack_require__(847) + ") no-repeat;\n  background-size: 100% 100%;\n  float: right;\n  right: -0.3rem;\n  position: absolute;\n}\n.helipos_main .center_one_pos[data-v-074296cf] {\n  width: 92%;\n  margin: 0.2rem 4% 0;\n  background: url(" + __webpack_require__(792) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.helipos_main .center_one_pos ul[data-v-074296cf] {\n  line-height: 25px;\n  font-size: .24rem;\n  font-weight: 400;\n  color: #b56200;\n}\n.helipos_main .center_one_pos ul .line[data-v-074296cf] {\n  width: 100%;\n  height: 1px;\n  background: url(" + __webpack_require__(791) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.helipos_main .center_one_pos ul li[data-v-074296cf] {\n  width: 92%;\n  margin: 0.2rem 4%;\n  display: inline-block;\n}\n.helipos_main .center_one_pos ul li img[data-v-074296cf] {\n  width: 0.46rem;\n  height: 0.46rem;\n}\n.helipos_main .center_one_pos ul li em[data-v-074296cf] {\n  right: 1.3rem;\n  position: absolute;\n}\n.helipos_main .center_one_pos ul li span[data-v-074296cf] {\n  margin-left: 0.5rem;\n}\n.helipos_main .center_one_pos ul li table[data-v-074296cf] {\n  width: 60%;\n  margin: 0 20%;\n  text-align: center;\n  font-size: 0.2rem;\n  -webkit-text-size-adjust: none;\n}\n.helipos_main .center_one_pos ul li table[data-v-074296cf],\n.helipos_main .center_one_pos ul li table tr th[data-v-074296cf],\n.helipos_main .center_one_pos ul li table tr td[data-v-074296cf] {\n  border: 1px solid #B56200;\n  border-collapse: collapse;\n}\n.helipos_main .center_one_pos ul li .text[data-v-074296cf] {\n  line-height: .4rem;\n  font-size: .2rem;\n  font-weight: 400;\n  color: #b56200;\n  -webkit-text-size-adjust: none;\n  margin-top: 0.2rem;\n}\n.helipos_main .center_one_pos .heli_down_pos[data-v-074296cf] {\n  width: 92%;\n  margin: 0 4% 0.35rem;\n  background: #f44a53;\n  background-size: 100% 100%;\n  height: 1.09rem;\n  font-size: .3rem;\n  font-weight: 400;\n  font-style: italic;\n  color: #ffffff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 0.32rem;\n}\n.helipos_main .center_two_pos[data-v-074296cf] {\n  width: 92%;\n  margin: 1.9rem 4% 0;\n  background: url(" + __webpack_require__(792) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.helipos_main .center_two_pos ul[data-v-074296cf] {\n  line-height: 25px;\n  font-size: .24rem;\n  font-weight: 400;\n  color: #b56200;\n}\n.helipos_main .center_two_pos ul .line[data-v-074296cf] {\n  width: 100%;\n  height: 1px;\n  background: url(" + __webpack_require__(791) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.helipos_main .center_two_pos ul li[data-v-074296cf] {\n  width: 92%;\n  margin: 0.2rem 4%;\n  display: inline-block;\n}\n.helipos_main .center_two_pos ul li img[data-v-074296cf] {\n  width: 0.46rem;\n  height: 0.46rem;\n}\n.helipos_main .center_two_pos ul li em[data-v-074296cf] {\n  right: 1.3rem;\n  position: absolute;\n}\n.helipos_main .center_two_pos ul li span[data-v-074296cf] {\n  margin-left: 0.5rem;\n}\n.helipos_main .center_two_pos ul li table[data-v-074296cf] {\n  width: 60%;\n  margin: 0 5% 0 35%;\n  text-align: center;\n  font-size: 0.2rem;\n  -webkit-text-size-adjust: none;\n}\n.helipos_main .center_two_pos ul li table[data-v-074296cf],\n.helipos_main .center_two_pos ul li table tr th[data-v-074296cf],\n.helipos_main .center_two_pos ul li table tr td[data-v-074296cf] {\n  border: 1px solid #B56200;\n  border-collapse: collapse;\n}\n.helipos_main .center_two_pos ul li .text[data-v-074296cf] {\n  line-height: .4rem;\n  font-size: .2rem;\n  font-weight: 400;\n  color: #b56200;\n  -webkit-text-size-adjust: none;\n  margin-top: 0.2rem;\n}\n.helipos_main .center_two_pos .heli_down_pos[data-v-074296cf] {\n  width: 92%;\n  margin: 0 4% 0.35rem;\n  background: #f44a53;\n  background-size: 100% 100%;\n  height: 1.09rem;\n  font-size: .3rem;\n  font-weight: 400;\n  font-style: italic;\n  color: #ffffff;\n  text-align: center;\n  display: inline-block;\n  border-radius: 0.32rem;\n}\n.helipos_main .heli3_pos[data-v-074296cf] {\n  display: inline-block;\n  margin-top: 1rem;\n  width: 100%;\n}\n.helipos_main .heli3_pos .send_btn_heli_pos[data-v-074296cf] {\n  background: url(" + __webpack_require__(844) + ") no-repeat;\n  /*bottom: 1.8rem; */\n  /* position: absolute; */\n  width: 40%;\n  height: 0.85rem;\n  background-size: 100% 100%;\n  margin-left: 5%;\n  margin-right: 5%;\n  float: left;\n}\n.helipos_main .heli3_pos .seeJl_heli_pos[data-v-074296cf] {\n  background: url(" + __webpack_require__(845) + ") no-repeat;\n  /* bottom: 1.8rem; */\n  /* position: absolute; */\n  width: 40%;\n  height: 0.85rem;\n  background-size: 100% 100%;\n  margin-right: 5%;\n  float: left;\n}\n.upgrade_main[data-v-074296cf] {\n  width: 100%;\n  height: 28.3rem;\n  background: url(" + __webpack_require__(853) + ") no-repeat;\n  background-size: 100% 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ 1807:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "heli_main"
  }, [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "heli3"
  }, [_c('div', {
    staticClass: "send_btn_heli",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "seeJl_heli",
    on: {
      "click": function($event) {
        return _vm.seeJl()
      }
    }
  })])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "heli1"
  }, [_c('div', {
    staticClass: "jl"
  }, [_c('span', [_vm._v("最高可获")]), _vm._v(" "), _c('em', [_vm._v("200元")])]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_vm._v("\r\n                        活动时间：即日起至2021年6月30日\r\n                ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "center_one"
  }, [_c('div', {
    staticClass: "heli_title"
  }, [_vm._v("产品介绍")]), _vm._v(" "), _c('ul', [_c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1238),
      "alt": ""
    }
  }), _vm._v("\r\n                                广州合利宝支付科技有限公司：银联认证+国企护航\r\n                        ")]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1239),
      "alt": ""
    }
  }), _vm._v("\r\n                                刷  卡  费  率： 刷卡 "), _c('em', [_vm._v("0.60%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("           \r\n                                                   \r\n                                        小额双免")]), _vm._v(" "), _c('em', [_vm._v("0.38%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("扫  码  费  率： 支付宝/微信/云闪付")]), _vm._v(" "), _c('em', [_vm._v("0.38%")]), _c('br'), _vm._v(" "), _c('span', [_vm._v("VIP刷卡费率： 0.55%")]), _c('br')]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1240),
      "alt": ""
    }
  }), _vm._v("\r\n                                分润规则：刷卡分润万5~万9\r\n                        ")]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1241),
      "alt": ""
    }
  }), _vm._v("\r\n                                产品特点：超低VIP刷卡手续费 "), _c('span', [_vm._v("帮您省钱")]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', [_vm._v("                  24小时交易3秒到账")]), _vm._v(" "), _c('span', [_vm._v("方便快捷")])])]), _vm._v(" "), _c('div', {
    staticClass: "heli_down"
  }, [_vm._v("\r\n                        商户自行下载【小利生活】APP进行注册"), _c('br'), _vm._v("\r\n                        商户注册邀请码：VGMA9T\r\n                ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "center_one"
  }, [_c('div', {
    staticClass: "heli_title"
  }, [_vm._v("活动规则")]), _vm._v(" "), _c('ul', [_c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1238),
      "alt": ""
    }
  }), _vm._v("\r\n                                机具采购价格：99元/台\r\n                        ")]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1239),
      "alt": ""
    }
  }), _vm._v("\r\n                                达标返现条件：缴纳49元服务费且激活90天"), _c('br'), _vm._v(" "), _c('span', [_vm._v("内交易金额≥10000")]), _c('br'), _vm._v(" "), _c('table', {
    attrs: {
      "width": "100%",
      "border": "0",
      "cellspacing": "1",
      "cellpadding": "0"
    }
  }, [_c('tr', [_c('td', [_vm._v("礼包")]), _vm._v(" "), _c('td', [_vm._v("返回直属推手")]), _vm._v(" "), _c('td', [_vm._v("返上级推手")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("1台")]), _vm._v(" "), _c('td', [_vm._v("110元/台")]), _vm._v(" "), _c('td', [_vm._v("40元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("5台")]), _vm._v(" "), _c('td', [_vm._v("120元/台")]), _vm._v(" "), _c('td', [_vm._v("30元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("10台")]), _vm._v(" "), _c('td', [_vm._v("130元/台")]), _vm._v(" "), _c('td', [_vm._v("20元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("30台")]), _vm._v(" "), _c('td', [_vm._v("140元/台")]), _vm._v(" "), _c('td', [_vm._v("10元/台")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("50台")]), _vm._v(" "), _c('td', [_vm._v("150元/台")]), _vm._v(" "), _c('td', [_vm._v("0元/台")])])]), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_vm._v("\r\n                                        注意：采购对应级别礼包才能获得对应级别返现，不累加\r\n                                ")])]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1240),
      "alt": ""
    }
  }), _vm._v("\r\n                                累计交易返现：激活90天内刷卡金额≥18万"), _c('br'), _vm._v(" "), _c('span', [_vm._v("返  现  金  额：50元/台")]), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "text"
  }, [_vm._v("\r\n                                        注意：累计交易量仅统计VIP刷卡与普通刷卡的交易量\r\n                                ")])]), _vm._v(" "), _c('li', [_c('img', {
    attrs: {
      "src": __webpack_require__(1241),
      "alt": ""
    }
  }), _vm._v("\r\n                                最终解释权归逍遥推手所有\r\n                        ")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-074296cf", module.exports)
  }
}

/***/ }),

/***/ 1954:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1493);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("5d6ecaf3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-074296cf&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./ybzf.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-074296cf&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./ybzf.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 653:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1954)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1290),
  /* template */
  __webpack_require__(1807),
  /* scopeId */
  "data-v-074296cf",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\heliIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] heliIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-074296cf", Component.options)
  } else {
    hotAPI.reload("data-v-074296cf", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 790:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/time_bg.png?v=e1644a30";

/***/ }),

/***/ 791:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliposBg2.png?v=005ed960";

/***/ }),

/***/ 792:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliposBg4.png?v=472c2c26";

/***/ }),

/***/ 839:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heli_bg.png?v=5b0c0690";

/***/ }),

/***/ 840:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heli_bg2.png?v=337727b2";

/***/ }),

/***/ 841:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heli_bg3.png?v=23e6b1d5";

/***/ }),

/***/ 842:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heli_btn.png?v=b576d9fd";

/***/ }),

/***/ 843:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heli_btn1.png?v=ffc83ba5";

/***/ }),

/***/ 844:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliPosjl.png?v=7c6acac1";

/***/ }),

/***/ 845:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliPosyq.png?v=6a89be2b";

/***/ }),

/***/ 846:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliposBg.png?v=676fc43e";

/***/ }),

/***/ 847:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/heliposBg1.png?v=91b8ff98";

/***/ }),

/***/ 848:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hk1.jpg?v=7fd8daac";

/***/ }),

/***/ 849:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hk2.jpg?v=c1e0a80d";

/***/ }),

/***/ 850:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hk3.jpg?v=84518f98";

/***/ }),

/***/ 851:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hk4.png?v=cb8e43e8";

/***/ }),

/***/ 852:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/hk5.png?v=11355233";

/***/ }),

/***/ 853:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/product_1126.png?v=9168dc0a";

/***/ }),

/***/ 854:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ybzf_bg.png?v=bb8e24c3";

/***/ }),

/***/ 855:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ybzf_button.png?v=1089c1a5";

/***/ }),

/***/ 856:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ybzfsearch.png?v=8d0c6f8d";

/***/ })

});