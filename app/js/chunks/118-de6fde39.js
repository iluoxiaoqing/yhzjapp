webpackJsonp([118],{

/***/ 1437:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            // allHotList:[{
            // 	title:"热门推荐",
            //     active:true,
            //     hotList:[],
            //     page:1,
            //     isNoData:false
            // },{
            // 	title:"刷卡攻略",
            //     active:false,
            //     hotList:[],
            //     page:1,
            //     isNoData:false
            // },{
            // 	title:"办卡攻略",
            //     active:false,
            //     hotList:[],
            //     page:1,
            //     isNoData:false
            // },{
            // 	title:"贷款攻略",
            //     active:false,
            //     hotList:[],
            //     page:1,
            //     isNoData:false
            // }],
            allHotList: [],
            hotList: [],
            isLoading: true,
            currentPage: 1,
            page: "",
            hasData: false,
            mySwiper: null,
            defaultText: "",
            index: 0,
            imgUrl: _apiConfig2.default.KY_IP,
            id: "0"
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        var _this = this;

        window.goBack = this.goBack;
        this.getHeight();
        var type = this.$route.query.type || 0;
        this.index = type;
        this.getAllHotList();
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            followFinger: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {

                document.getElementById("scrollContainer").scrollTop = 0;
                console.log("this.allHotList1", _this.allHotList);
                var index = swiper.activeIndex;
                _this.currentPage = 1;
                _this.allHotList.map(function (el) {
                    el.active = false;
                });
                _this.allHotList[index].active = true;

                _this.index = index;
                // if(this.allHotList[this.index].hotList.length==0){
                //     this.getPageList(this.allHotList[index].id);
                // }
                console.log("2dddd", _this.allHotList[index].id);
                _this.changeList(_this.allHotList[index], index, _this.allHotList[index].id);
            }
        });
    },

    methods: {
        getAllHotList: function getAllHotList() {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "news/group",
                data: {},
                showLoading: false
            }, function (data) {
                _this2.allHotList = data.data;
                _this2.$set(_this2.allHotList[0], "page", 1);
                _this2.$set(_this2.allHotList[0], "active", true);
                _this2.$set(_this2.allHotList[0], "isNoData", false);
                _this2.$set(_this2.allHotList[0], "hotList", "");
                console.log("this.allHotList", _this2.allHotList);
                _this2.getPageList(_this2.allHotList[0].group);
                // console.log("3333",this.allHotList[0].id);
            });
        },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        changeList: function changeList(i, index, id) {
            this.currentPage = 1;
            console.log(i, index, id, 'kankan');
            console.log("test", i);
            this.index = index;
            this.allHotList.map(function (el) {
                el.active = false;
            });
            i.active = true;
            this.$set(this.allHotList[index], "page", 1);
            this.$set(this.allHotList[index], "active", true);
            this.$set(this.allHotList[index], "isNoData", false);
            this.$set(this.allHotList[index], "hotList", "");
            this.mySwiper.slideTo(index, 300, true);

            this.getPageList(i.group);
            this.id = i.group;
            console.log(this.id, 'ididididi');
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        loadmore: function loadmore() {
            console.log('加载');
            this.currentPage++;
            this.getPageList(this.id);
        },
        refresh: function refresh() {
            console.log('刷新');
            // this.$set(this.allHotList[this.index],"page",1);
            // this.$set(this.allHotList[this.index],"hotList",[]);
            this.getPageList(this.id);
        },
        getPageList: function getPageList(id) {
            var _this3 = this;

            console.log("1111", this.index);
            console.log("222", id);
            // switch(this.index) {
            //     case 0:
            //         var group = "hot";
            //         break;
            //     case 1:
            //         var group = "shop";
            //         break;
            //     case 2:
            //         var group = "card";
            //         break;
            //     case 3:
            //         var group = "loan";
            //         break;
            // }

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "news/list",
                data: {
                    "page": this.currentPage,
                    "group": id
                },
                showLoading: false
            }, function (data) {
                console.log(data.data);

                if (data.data.length > 0) {
                    _this3.hotList = data.data;
                } else {
                    common.toast({
                        "content": "没有更多数据了"
                    });
                }

                // let curPage = this.allHotList[this.index].page;
                // let curPage = 1;

                // if(data.data.length>0){
                //     data.data.map((el)=>{
                //         this.$set(el,"show",false);
                //         this.hotList.push(el);
                //     });

                //     curPage ++;
                //     this.$set(this.allHotList[this.index],"page",curPage);
                //     this.$set(this.allHotList[this.index],"isNoData",false);

                // }
                // else{
                //     if(curPage==1){
                //         this.$set(this.allHotList[this.index],"isNoData",true);
                //     }
                //     else{
                //         common.toast({
                //             "content":"没有更多数据了"
                //         })
                //     }

                // }
            });
        }
    }
};

/***/ }),

/***/ 1556:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1869:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('div', {
    staticClass: "queryHead"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.allHotList), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.groupName))])
  }), 0)]), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "swiper-container",
    attrs: {
      "id": "swiper-container"
    }
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, _vm._l((_vm.allHotList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "swiper-slide",
      style: ({
        'height': index == 0 ? 'auto' : '0px'
      })
    }, [_vm._l((_vm.hotList), function(j, index) {
      return _c('div', {
        key: index,
        staticClass: "materialList"
      }, [_c('router-link', {
        attrs: {
          "to": {
            name: 'material_detail',
            query: {
              id: j.id
            }
          }
        }
      }, [_c('div', {
        staticClass: "list",
        attrs: {
          "id": "list1"
        }
      }, [_c('div', {
        staticClass: "listImg"
      }, [(j.thumbnail.indexOf('http') != 1) ? _c('img', {
        attrs: {
          "src": j.thumbnail,
          "alt": ""
        }
      }) : _c('img', {
        attrs: {
          "src": _vm.imgUrl + 'file/downloadFile?filePath=' + j.thumbnail
        }
      })]), _vm._v(" "), _c('div', {
        staticClass: "listDeatil"
      }, [_c('b', [_vm._v(_vm._s(j.title))]), _vm._v(" "), _c('div', {
        staticClass: "time"
      }, [_c('i', [_vm._v(_vm._s(j.time))]), _vm._v(" "), _c('em', [_c('a', {
        attrs: {
          "href": "javascript:;"
        }
      }, [_vm._v(_vm._s(j.tag))])])])])])])], 1)
    }), _vm._v(" "), _c('no-data', {
      attrs: {
        "isNoData": i.isNoData
      }
    })], 2)
  }), 0)])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-45569eb2", module.exports)
  }
}

/***/ }),

/***/ 2017:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1556);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("98d99c28", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-45569eb2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share_material.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-45569eb2&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share_material.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 641:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2017)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1437),
  /* template */
  __webpack_require__(1869),
  /* scopeId */
  "data-v-45569eb2",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\share_material.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] share_material.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45569eb2", Component.options)
  } else {
    hotAPI.reload("data-v-45569eb2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});