webpackJsonp([120],{

/***/ 1415:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            listData: "",
            pageNo: 1
        };
    },
    mounted: function mounted() {
        this.getInfo();
    },

    methods: {
        getInfo: function getInfo() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "wonderfulActivityInfo/findMarketPolicy",
                data: {
                    bannerType: "MARKET_POLICY"
                },
                showLoading: false
            }, function (data) {
                _this.listData = data.data;
            });
        },
        go: function go(i, j) {
            common.setCookie("imgTitle", j);
            common.setCookie("imgAddress", i);
            // window.location.href = api.WEB_URL + 'imgUrl?imgAddress=' + i + '&imgTitle=' + j;
            window.location.href = _apiConfig2.default.WEB_URL + 'imgUrl?filePath=' + i + '&proTitle=' + j;
            // window.location.href = i;
        }
    }
};

/***/ }),

/***/ 1564:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\policy\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1877:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, _vm._l((_vm.listData), function(i, idx) {
    return _c('div', {
      key: idx,
      staticClass: "img",
      style: ({
        backgroundImage: 'url(' + i.imgUrl + ')'
      }),
      on: {
        "click": function($event) {
          return _vm.go(i.wonderfulImgUrl, i.activityTitle)
        }
      }
    }, [(i.acticityStatus == 'EXPIRE') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("已结束")]) : (i.acticityStatus == 'EFFECT') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("进行中")]) : (i.acticityStatus == 'PREHEAT') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("未开始")]) : _vm._e()])
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-522c2eea", module.exports)
  }
}

/***/ }),

/***/ 2025:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1564);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("3a1ce798", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-522c2eea&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./marketPolicy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-522c2eea&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./marketPolicy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 612:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2025)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1415),
  /* template */
  __webpack_require__(1877),
  /* scopeId */
  "data-v-522c2eea",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\policy\\marketPolicy.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] marketPolicy.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-522c2eea", Component.options)
  } else {
    hotAPI.reload("data-v-522c2eea", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});