webpackJsonp([73],{

/***/ 1278:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ }),

/***/ 1279:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_screening_selected.png?v=ac93ddc3";

/***/ }),

/***/ 1364:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            active: 0,
            toptext: [],
            questionList: [],
            index: 0,
            type: '',
            personInfo: [],
            isLoading: true,
            currentPage: 1
        };
    },
    mounted: function mounted() {
        this.getType();
        this.getPersonInfo();
        common.youmeng("帮助反馈", "进入帮助反馈");
        this.getHeight();
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop - 58 + "px";
        },
        gotoService: function gotoService() {
            common.youmeng("帮助反馈", "点击-专属客服");
        },
        gotoFeedBack: function gotoFeedBack() {
            common.youmeng("帮助反馈", "点击-专属客服");
        },
        changeAc: function changeAc(index, type, desc) {
            this.active = index;
            this.type = type;
            this.isLoading = true;
            this.currentPage = 1;
            this.getQuestion();
            common.youmeng("帮助反馈", "点击-" + desc);
        },
        getType: function getType() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getQuestionType",
                showLoading: false
            }, function (data) {
                _this.toptext = data.data;
                _this.type = data.data[0].type;
                _this.getQuestion();
            });
        },
        getQuestion: function getQuestion() {
            var _this2 = this;

            //console.log(done)

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "custService/getQuestion",
                    data: {
                        type: this.type,
                        page: this.currentPage
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this2.currentPage == 1) {
                            _this2.questionList = [];
                        }
                        data.data.object.map(function (el) {
                            _this2.questionList.push(el);
                        });
                        _this2.currentPage++;
                        _this2.isLoading = true;
                        _this2.$refs.my_scroller.finishInfinite(true);
                    } else {
                        //done(2);
                        _this2.$refs.my_scroller.finishInfinite(2);
                    }
                    // this.questionList = data.data.object;
                });
            } else {
                //done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        },
        getPersonInfo: function getPersonInfo() {
            var _this3 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getCustName",
                data: {},
                showLoading: false
            }, function (data) {
                _this3.personInfo = data.data;
            });
        },
        refresh: function refresh() {
            var _this4 = this;

            var timer = null;
            this.currentPage = 1;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this4.getQuestion();
                _this4.$refs.my_scroller.finishPullToRefresh();
            }, 500);
        },
        infinite: function infinite() {
            var _this5 = this;

            var timer = null;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this5.getQuestion();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1560:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-a81359fa] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-a81359fa] {\n  background: #fff;\n}\n.tips_success[data-v-a81359fa] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-a81359fa] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-a81359fa] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-a81359fa],\n.fade-leave-active[data-v-a81359fa] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-a81359fa],\n.fade-leave-to[data-v-a81359fa] {\n  opacity: 0;\n}\n.default_button[data-v-a81359fa] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-a81359fa] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-a81359fa] {\n  position: relative;\n}\n.loading-tips[data-v-a81359fa] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.container[data-v-a81359fa] {\n  width: 100%;\n  background: #f4f5fb;\n  overflow: hidden;\n  padding-bottom: 1.2rem;\n}\n.container .help_top[data-v-a81359fa] {\n  height: 1.96rem;\n  width: 100%;\n  background: #313757;\n  color: #FFFFFF;\n  display: inline-block;\n}\n.container .help_top .help_top_name[data-v-a81359fa] {\n  font-size: 0.4rem;\n  margin-top: 0.68rem;\n  line-height: 0.56rem;\n  margin-left: 4%;\n  font-weight: bold;\n}\n.container .help_top .help_top_text[data-v-a81359fa] {\n  font-size: 0.28rem;\n  line-height: 0.56rem;\n  margin-left: 4%;\n}\n.container .feedBack_title[data-v-a81359fa] {\n  width: 100%;\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  opacity: 0.6;\n  width: 92%;\n  margin: 0.32rem 4% 0.18rem;\n}\n.container .help_inner[data-v-a81359fa] {\n  background: #FFFFFF;\n  width: 100%;\n  height: 2.36rem;\n}\n.container .help_inner .help_contain[data-v-a81359fa] {\n  width: 92%;\n  margin: 0 4%;\n  display: -webkit-box;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  display: -webkit-flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: row;\n     -moz-box-orient: horizontal;\n     -moz-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n}\n.container .help_inner .help_contain .item1[data-v-a81359fa] {\n  line-height: 0.72rem;\n  color: #3D4A5B;\n  font-size: 0.28rem;\n  text-align: center;\n  margin-top: 0.28rem;\n  border-radius: 0.02rem;\n  background-color: rgba(61, 74, 91, 0.05);\n  width: 2.12rem;\n  height: 0.72rem;\n}\n.container .help_inner .help_contain .active[data-v-a81359fa] {\n  background: url(" + __webpack_require__(1279) + ") no-repeat right bottom;\n  background-size: 0.4rem 0.34rem;\n  background-color: rgba(63, 131, 255, 0.15);\n}\n.container .help_question[data-v-a81359fa] {\n  background: #ffffff;\n  margin-top: 0.16rem;\n}\n.container .help_question ul li[data-v-a81359fa] {\n  margin-left: 4%;\n  opacity: 0.8;\n  font-size: 0.32rem;\n  height: .8rem;\n  overflow: hidden;\n  -webkit-box-shadow: inset 0px 0px 1px -1px #c8c7cc;\n}\n.container .help_question ul li a[data-v-a81359fa] {\n  color: #3D4A5B;\n  width: 100%;\n  height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.container .help_question ul li a p[data-v-a81359fa] {\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n.container .help_question ul li a span[data-v-a81359fa] {\n  margin: 0 0.32rem;\n}\n.container .help_question ul li a span img[data-v-a81359fa] {\n  width: 0.16rem;\n  height: 0.26rem;\n}\n.container .help_fixed[data-v-a81359fa] {\n  height: 0.98rem;\n  width: 100%;\n  background-color: #ffffff;\n  position: fixed;\n  bottom: 0;\n}\n.container .help_fixed ul .first[data-v-a81359fa] {\n  position: relative;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  text-align: center;\n  width: 49.5%;\n  float: left;\n  line-height: 0.98rem;\n}\n.container .help_fixed ul .first span[data-v-a81359fa] {\n  padding-right: .22rem;\n}\n.container .help_fixed ul .first span img[data-v-a81359fa] {\n  width: 0.4rem;\n  height: 0.36rem;\n}\n.container .help_fixed ul .first[data-v-a81359fa]:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  top: 0.22rem;\n  height: 0.6rem;\n  background: #3D4A5B;\n  opacity: 0.3;\n  width: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.container .help_fixed ul .first1[data-v-a81359fa] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  text-align: center;\n  width: 49.5%;\n  float: left;\n  line-height: 0.98rem;\n}\n.container .help_fixed ul .first1 span[data-v-a81359fa] {\n  padding-right: .22rem;\n}\n.container .help_fixed ul .first1 span img[data-v-a81359fa] {\n  width: 0.4rem;\n  height: 0.36rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1605:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_feedback.png?v=2cfce5e3";

/***/ }),

/***/ 1609:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_service.png?v=681e290d";

/***/ }),

/***/ 1841:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "help_top"
  }, [_c('div', {
    staticClass: "help_top_name"
  }, [_vm._v("\n            HI，" + _vm._s(_vm.personInfo) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "help_top_text"
  }, [_vm._v("\n            已为您定制如下内容\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "feedBack_title"
  }, [_vm._v("\n        问题分类\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "help_inner"
  }, [_c('div', {
    staticClass: "help_contain"
  }, _vm._l((_vm.toptext), function(item, index) {
    return _c('a', {
      key: index,
      staticClass: "item1",
      class: {
        active: index == _vm.active
      },
      on: {
        "click": function($event) {
          return _vm.changeAc(index, item.type, item.desc)
        }
      }
    }, [_vm._v("\n                " + _vm._s(item.desc) + "\n            ")])
  }), 0)]), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('scroller', {
    ref: "my_scroller",
    attrs: {
      "noDataText": _vm.defaultText,
      "on-refresh": _vm.refresh,
      "on-infinite": _vm.infinite
    }
  }, [_c('div', {
    staticClass: "help_question"
  }, [_c('ul', _vm._l((_vm.questionList), function(i, index) {
    return _c('li', {
      key: index
    }, [_c('router-link', {
      attrs: {
        "to": {
          name: 'question_detail',
          query: {
            id: i.questionNo
          }
        }
      }
    }, [_c('p', [_vm._v(_vm._s(i.title))]), _vm._v(" "), _c('span', [_c('img', {
      attrs: {
        "src": __webpack_require__(1278),
        "alt": ""
      }
    })])])], 1)
  }), 0)])])], 1), _vm._v(" "), _c('div', {
    staticClass: "help_fixed"
  }, [_c('ul', [_c('router-link', {
    attrs: {
      "to": "feedBack_service"
    }
  }, [_c('li', {
    staticClass: "first",
    on: {
      "click": function($event) {
        return _vm.gotoService()
      }
    }
  }, [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1609),
      "alt": ""
    }
  })]), _vm._v("\n                    专属客服\n                ")])]), _vm._v(" "), _c('router-link', {
    attrs: {
      "to": "feedBack"
    }
  }, [_c('li', {
    staticClass: "first1",
    on: {
      "click": function($event) {
        return _vm.gotoFeedBack()
      }
    }
  }, [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1605),
      "alt": ""
    }
  })]), _vm._v("\n                    意见反馈\n                ")])])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a81359fa", module.exports)
  }
}

/***/ }),

/***/ 1976:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1560);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("fba32b34", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a81359fa&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_help.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a81359fa&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_help.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1976)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1364),
  /* template */
  __webpack_require__(1841),
  /* scopeId */
  "data-v-a81359fa",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\feedBack\\feedBack_help.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] feedBack_help.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a81359fa", Component.options)
  } else {
    hotAPI.reload("data-v-a81359fa", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});