webpackJsonp([78],{

/***/ 1301:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ }),

/***/ 1394:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            active: 0,
            toptext: [],
            questionList: [],
            index: 0,
            type: '',
            personInfo: [],
            isLoading: true,
            currentPage: 1
        };
    },
    mounted: function mounted() {
        this.getType();
        this.getPersonInfo();
        common.youmeng("帮助反馈", "进入帮助反馈");
        this.getHeight();
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = this.$refs.scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop - 58 + "px";
        },
        gotoService: function gotoService() {
            common.youmeng("帮助反馈", "点击-专属客服");
        },
        gotoFeedBack: function gotoFeedBack() {
            common.youmeng("帮助反馈", "点击-专属客服");
        },
        changeAc: function changeAc(index, type, desc) {
            this.active = index;
            this.type = type;
            this.isLoading = true;
            this.currentPage = 1;
            this.getQuestion();
            common.youmeng("帮助反馈", "点击-" + desc);
        },
        getType: function getType() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getQuestionType",
                showLoading: false
            }, function (data) {
                _this.toptext = data.data;
                _this.type = data.data[0].type;
                _this.getQuestion();
            });
        },
        getQuestion: function getQuestion() {
            var _this2 = this;

            //console.log(done)

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "custService/getQuestion",
                    data: {
                        type: this.type,
                        page: this.currentPage
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this2.currentPage == 1) {
                            _this2.questionList = [];
                        }
                        data.data.object.map(function (el) {
                            _this2.questionList.push(el);
                        });
                        _this2.currentPage++;
                        _this2.isLoading = true;
                        _this2.$refs.my_scroller.finishInfinite(true);
                    } else {
                        //done(2);
                        _this2.$refs.my_scroller.finishInfinite(2);
                    }
                    // this.questionList = data.data.object;
                });
            } else {
                //done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        },
        getPersonInfo: function getPersonInfo() {
            var _this3 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "custService/getCustName",
                data: {},
                showLoading: false
            }, function (data) {
                _this3.personInfo = data.data;
            });
        },
        refresh: function refresh() {
            var _this4 = this;

            var timer = null;
            this.currentPage = 1;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this4.getQuestion();
                _this4.$refs.my_scroller.finishPullToRefresh();
            }, 500);
        },
        infinite: function infinite() {
            var _this5 = this;

            var timer = null;
            clearTimeout(timer);
            timer = setTimeout(function () {
                _this5.getQuestion();
            }, 500);
        }
    }
};

/***/ }),

/***/ 1577:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\feedBack\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1664:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_feedback.png?v=2cfce5e3";

/***/ }),

/***/ 1668:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_service.png?v=681e290d";

/***/ }),

/***/ 1890:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "help_top"
  }, [_c('div', {
    staticClass: "help_top_name"
  }, [_vm._v("\n            HI，" + _vm._s(_vm.personInfo) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "help_top_text"
  }, [_vm._v("\n            已为您定制如下内容\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "feedBack_title"
  }, [_vm._v("\n        问题分类\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "help_inner"
  }, [_c('div', {
    staticClass: "help_contain"
  }, _vm._l((_vm.toptext), function(item, index) {
    return _c('a', {
      key: index,
      staticClass: "item1",
      class: {
        active: index == _vm.active
      },
      on: {
        "click": function($event) {
          return _vm.changeAc(index, item.type, item.desc)
        }
      }
    }, [_vm._v("\n                " + _vm._s(item.desc) + "\n            ")])
  }), 0)]), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('scroller', {
    ref: "my_scroller",
    attrs: {
      "noDataText": _vm.defaultText,
      "on-refresh": _vm.refresh,
      "on-infinite": _vm.infinite
    }
  }, [_c('div', {
    staticClass: "help_question"
  }, [_c('ul', _vm._l((_vm.questionList), function(i, index) {
    return _c('li', {
      key: index
    }, [_c('router-link', {
      attrs: {
        "to": {
          name: 'question_detail',
          query: {
            id: i.questionNo
          }
        }
      }
    }, [_c('p', [_vm._v(_vm._s(i.title))]), _vm._v(" "), _c('span', [_c('img', {
      attrs: {
        "src": __webpack_require__(1301),
        "alt": ""
      }
    })])])], 1)
  }), 0)])])], 1), _vm._v(" "), _c('div', {
    staticClass: "help_fixed"
  }, [_c('ul', [_c('router-link', {
    attrs: {
      "to": "feedBack_service"
    }
  }, [_c('li', {
    staticClass: "first",
    on: {
      "click": function($event) {
        return _vm.gotoService()
      }
    }
  }, [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1668),
      "alt": ""
    }
  })]), _vm._v("\n                    专属客服\n                ")])]), _vm._v(" "), _c('router-link', {
    attrs: {
      "to": "feedBack"
    }
  }, [_c('li', {
    staticClass: "first1",
    on: {
      "click": function($event) {
        return _vm.gotoFeedBack()
      }
    }
  }, [_c('span', [_c('img', {
    attrs: {
      "src": __webpack_require__(1664),
      "alt": ""
    }
  })]), _vm._v("\n                    意见反馈\n                ")])])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-64a4eabe", module.exports)
  }
}

/***/ }),

/***/ 2038:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1577);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("81fdba84", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-64a4eabe&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_help.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-64a4eabe&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./feedBack_help.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 590:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2038)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1394),
  /* template */
  __webpack_require__(1890),
  /* scopeId */
  "data-v-64a4eabe",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\feedBack\\feedBack_help.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] feedBack_help.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-64a4eabe", Component.options)
  } else {
    hotAPI.reload("data-v-64a4eabe", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});