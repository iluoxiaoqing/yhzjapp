webpackJsonp([125],{

/***/ 1402:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	data: function data() {
		return {
			hotList: [],
			imgUrl: _apiConfig2.default.KY_IP,
			page: 1
		};
	},

	components: {
		noData: _noData2.default,
		freshToLoadmore: _freshToLoadmore2.default
	},
	mounted: function mounted() {
		this.getHotList();
	},

	methods: {
		getHotList: function getHotList() {
			var _this = this;

			common.Ajax({
				url: _apiConfig2.default.KY_IP + "bussMenu/hotBusiness",
				data: {
					"page": 1
				},
				showLoading: false
			}, function (data) {
				_this.hotList = data.data;
				console.log("hotList", _this.hotList);
			});
		},
		apply: function apply(goodsType) {
			common.Ajax({
				url: _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
				data: {
					address: "",
					goods: JSON.stringify([{ type: goodsType, count: 1 }]),
					payAmount: 0,
					accAmount: 0
				},
				showLoading: false
			}, function (data) {
				console.log("返回值", data);
				window.location.href = data.data.ky_no_pos;
			});
		},
		gotoMall: function gotoMall() {
			common.youmeng("备货开店", "点击备货开店");
			this.$router.push({
				"path": "mall"
			});
		},
		share: function share(i) {
			console.log("分享", "分享");
			this.$router.push({
				"path": "share",
				"query": {
					"product": i.product.shareCode,
					"productName": i.product.title,
					"channel": "hotProduct"
				}
			});
		}
	}
};

/***/ }),

/***/ 1599:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-8895f322] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-8895f322] {\n  background: #fff;\n}\n.tips_success[data-v-8895f322] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-8895f322] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-8895f322] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-8895f322],\n.fade-leave-active[data-v-8895f322] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-8895f322],\n.fade-leave-to[data-v-8895f322] {\n  opacity: 0;\n}\n.default_button[data-v-8895f322] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-8895f322] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-8895f322] {\n  position: relative;\n}\n.loading-tips[data-v-8895f322] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.hot_main[data-v-8895f322] {\n  widows: 92px;\n  margin: 0.48rem 4% 0;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  height: 100%;\n  overflow: auto;\n  -webkit-overflow-scrolling: touch;\n}\n.hot_main .mainList[data-v-8895f322] {\n  width: 100%;\n  height: 4.1rem;\n  display: inline-block;\n  box-shadow: 0 2px 12px 0 rgba(99, 113, 136, 0.12);\n  border-radius: 4px;\n  background: #ffffff;\n  margin-bottom: 0.16rem;\n}\n.hot_main .mainList .item[data-v-8895f322] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 0.3rem 0 0;\n  height: 3rem;\n}\n.hot_main .mainList .item .goodsImg[data-v-8895f322] {\n  width: 2.72rem;\n  height: 2.72rem;\n  margin-left: 0.16rem;\n  background: #f4f5fb;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n}\n.hot_main .mainList .item .goodsImg span[data-v-8895f322] {\n  background: #FFB740;\n  width: .6rem;\n  height: .28rem;\n  position: absolute;\n  left: 0;\n  top: 0;\n  color: #fff;\n  font-size: .24rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hot_main .mainList .item .goodsImg span i[data-v-8895f322] {\n  -webkit-transform: scale(0.8);\n      -ms-transform: scale(0.8);\n          transform: scale(0.8);\n  display: block;\n}\n.hot_main .mainList .item .goodsImg img[data-v-8895f322] {\n  width: 2.72rem;\n  height: 2.72rem;\n  display: block;\n}\n.hot_main .mainList .item .goodsDeatil[data-v-8895f322] {\n  width: 3.6rem;\n  height: 1.84rem;\n  margin-left: 0.16rem;\n  position: relative;\n}\n.hot_main .mainList .item .goodsDeatil button[data-v-8895f322] {\n  width: 1.7rem;\n  height: 0.64rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  font-size: 0.28rem;\n  color: #ffffff;\n  text-align: center;\n  float: left;\n  margin-top: 0.14rem;\n}\n.hot_main .mainList .item .goodsDeatil .gray[data-v-8895f322] {\n  width: 1.7rem;\n  height: 0.64rem;\n  background: #313757;\n  border-radius: 4px;\n  font-size: 0.28rem;\n  color: #ffffff;\n  text-align: center;\n  float: left;\n  margin-top: 0.14rem;\n  margin-left: 0.16rem;\n}\n.hot_main .mainList .item .goodsDeatil b[data-v-8895f322] {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.28rem;\n}\n.hot_main .mainList .item .goodsDeatil p[data-v-8895f322] {\n  color: #3D4A5B;\n  font-size: 0.24rem;\n  opacity: 0.6;\n  text-align: justify;\n  line-height: .34rem;\n  margin-top: 0.32rem;\n}\n.hot_main .mainList .item .goodsDeatil .number[data-v-8895f322] {\n  width: 100%;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.hot_main .mainList .item .goodsDeatil .number i[data-v-8895f322] {\n  color: #E95647;\n  font-size: 0.32rem;\n  display: block;\n}\n.hot_main .mainList .company[data-v-8895f322] {\n  display: inline-block;\n  height: 0.44rem;\n  background: rgba(49, 55, 87, 0.1);\n  border-radius: 4px;\n  opacity: 0.4;\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  padding: 0.04rem 0.16rem;\n  float: left;\n  margin-left: 0.16rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1912:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hot_main"
  }, _vm._l((_vm.hotList), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "mainList"
    }, [_c('div', {
      staticClass: "item"
    }, [_c('div', {
      staticClass: "goodsImg"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.product.pic
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "goodsDeatil"
    }, [_c('b', [_vm._v(_vm._s(i.product.title))]), _vm._v(" "), _c('div', {
      staticClass: "number"
    }, [_c('i', [_vm._v("费率：" + _vm._s(i.product.fee))])]), _vm._v(" "), _c('p', [_vm._v(_vm._s(i.hotDesc))]), _vm._v(" "), _c('button', {
      on: {
        "click": function($event) {
          return _vm.share(i)
        }
      }
    }, [_vm._v(_vm._s(i.product.shareBtnText))]), _vm._v(" "), (i.product.needBuyMall == true) ? _c('button', {
      staticClass: "gray",
      on: {
        "click": function($event) {
          return _vm.gotoMall()
        }
      }
    }, [_vm._v("\n                            " + _vm._s(i.product.buyBtnText) + "\n                    ")]) : _c('button', {
      staticClass: "gray",
      on: {
        "click": function($event) {
          return _vm.apply(i.product.buyCode)
        }
      }
    }, [_vm._v("\n                        " + _vm._s(i.product.buyBtnText) + "\n                    ")])])]), _vm._v(" "), _c('div', {
      staticClass: "company"
    }, [_vm._v("\n                " + _vm._s(i.product.company) + "\n            ")])])
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-8895f322", module.exports)
  }
}

/***/ }),

/***/ 2060:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1599);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("48f28188", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8895f322&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hotProduct.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8895f322&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./hotProduct.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 598:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2060)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1402),
  /* template */
  __webpack_require__(1912),
  /* scopeId */
  "data-v-8895f322",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\hotProduct\\hotProduct.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] hotProduct.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8895f322", Component.options)
  } else {
    hotAPI.reload("data-v-8895f322", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});