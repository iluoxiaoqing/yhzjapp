webpackJsonp([113],{

/***/ 1478:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            videoList: {},
            imgUrl1: _apiConfig2.default.KY_IP,
            active: 0,
            video: [],
            index: ""
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        this.getHeight();
        this.getList();
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "mediaTeach/group",
                "data": {}
            }, function (data) {
                _this.videoList = data.data;
                _this.id = _this.$route.query.id;
                // this.$set(this.videoList[this.id-1],"page","1");
                _this.videoList[_this.id - 1].page = 1;
                _this.index = _this.$route.query.id - 1;
                _this.videoList[_this.id - 1].video = [];
                _this.active = _this.index;
                _this.getVideo(_this.id, _this.index);
            });
        },
        toggle: function toggle(index, id) {
            this.video = [];
            this.active = index;
            this.id = id;
            this.videoList.map(function (el) {
                el.page = 1;
                el.video = [];
            });
            // this.videoList[index].page = 1;
            this.getVideo(this.id, index);
        },
        loadmore: function loadmore() {
            console.log("fresh", this.id);
            this.getVideo(this.id);
        },
        refresh: function refresh() {
            this.$set(this.videoList[this.index], "page", 1);
            // this.$set(this.video,"video",[]);
            // console.log("fresh",this.id)
            this.video = [];
            this.getVideo(this.id);
        },
        getVideo: function getVideo(id, active) {
            var _this2 = this;

            console.log("2222", this.videoList[this.index].page);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "mediaTeach/videoList",
                "data": {
                    page: this.videoList[this.index].page,
                    groupId: id
                }
            }, function (data) {
                // console.log(data.data);
                // this.video = data.data;
                var curPage = _this2.videoList[_this2.index].page;
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        _this2.$set(el, "show", false);
                        _this2.video.push(el);
                    });
                    curPage++;
                    _this2.$set(_this2.videoList[_this2.index], "page", curPage);
                    _this2.$set(_this2.videoList[_this2.index], "isNoData", false);
                } else {
                    if (curPage == 1) {
                        _this2.video = data.data;
                        // this.$set(this.menuList[this.index],"isNoData",true);
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        },
        getImg: function getImg(url) {
            if (url.substr(0, 7).toLowerCase() == "http://" || url.substr(0, 8).toLowerCase() == "https://") {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        }
    }
};

/***/ }),

/***/ 1609:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\video\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1922:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "NavSlide"
  }, [_c('nav', _vm._l((_vm.videoList), function(item, index) {
    return _c('div', {
      staticClass: "main",
      class: {
        active: index == _vm.active
      },
      on: {
        "click": function($event) {
          return _vm.toggle(index, item.id)
        }
      }
    }, [_vm._v("\n                " + _vm._s(item.name)), _c('br'), _vm._v(" "), _c('i', [_vm._v(_vm._s(item.desc))])])
  }), 0), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "classType"
  }, [_c('div', {
    staticClass: "videoList"
  }, _vm._l((_vm.video), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('a', {
      attrs: {
        "href": j.videoUrl
      }
    }, [_c('img', {
      attrs: {
        "src": _vm.getImg(j.pagePic)
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "title"
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
      staticClass: "content"
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.desc) + "\n\t\t\t\t\t\t")])])])
  }), 0)])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-aec27cce", module.exports)
  }
}

/***/ }),

/***/ 2070:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1609);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("146c5257", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aec27cce&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aec27cce&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 689:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2070)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1478),
  /* template */
  __webpack_require__(1922),
  /* scopeId */
  "data-v-aec27cce",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\video\\video_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] video_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-aec27cce", Component.options)
  } else {
    hotAPI.reload("data-v-aec27cce", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});