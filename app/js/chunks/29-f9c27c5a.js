webpackJsonp([29],{

/***/ 1459:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

common.youmeng("我要备货", "进入我要备货");

exports.default = {
    data: function data() {
        return {
            totalAmount: 0,
            totalNumber: 0,
            showCar: false,
            carList: [],
            showClear: false,
            clearInfo: {
                "title": "温馨提示",
                "content": "确定要清空购物车吗？",
                "confirmText": "确定"
            },
            purchaseList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            currentPayArray: [],
            leftMenuArray: [],
            groupCode: "",
            addCount: false,
            userNo: common.getUserNo(),
            proCode: common.getCookie('proCode'),
            num: ''
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        var _this = this;

        if (this.$route.query.type == 'link') {
            common.setCookie("proCode", '拉卡拉押金');
        }

        console.log("====", this.proCode);
        var bodyHeight = document.documentElement.clientHeight;
        //        this.$refs.mall_wrap.style.height = bodyHeight + "px";
        this.$nextTick(function () {
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseList"
            }, function (data) {

                if (data.data.length == 0) {
                    return;
                }

                console.log("======", data.data);

                _this.purchaseList = data.data;
                _this.purchaseList.map(function (el) {
                    _this.$set(el, "active", false);
                    _this.leftMenuArray.push({
                        "active": el.active,
                        "goods": el.goods,
                        "groupCode": el.groupCode,
                        "groupName": el.groupName
                    });
                    el.goods.map(function (i) {
                        _this.$set(i, "currentNum", 0);
                    });
                });
                var proCode = common.getCookie('proCode');

                //            this.tradeList.forEach((data.data) => {
                //            	if(data.data.groupName == proCode){
                //            		console.log('111')
                //            	}else{
                //            		console.log('2222')
                //            	}
                //            })


                for (var i = 0; i < data.data.length; i++) {
                    if (proCode == data.data[i].groupName) {
                        _this.num = i;
                        console.log('包含', _this.num);
                        _this.changeGoods(data.data[i], i);
                    } else {
                        console.log('不包含');
                    }
                }

                console.log('包含11', _this.num);
            });
        });
    },

    methods: {
        go: function go(i) {
            var _this2 = this;

            this.leftMenuArray[i].active = true;
            this.currentPayArray = this.purchaseList[i].goods;
            this.groupCode = this.purchaseList[i].groupCode;
            console.log("111", this.groupCode);

            this.currentPayArray.map(function (el, index) {

                _this2.$nextTick(function () {

                    var flydot = _this2.$refs['flydot' + index][i];
                    var position = flydot.getBoundingClientRect();
                    _this2.$set(el, 'left', position.left + 5.5);
                    _this2.$set(el, 'top', position.top + 5.5);
                });
            });
        },
        apply: function apply(goodsType) {
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
                data: {
                    address: "",
                    goods: JSON.stringify([{ type: goodsType, count: 1 }]),
                    payAmount: 0,
                    accAmount: 0
                },
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                window.location.href = data.data.ky_no_pos;
                // this.$router.push({
                //     path:data.data.ky_no_pos
                // })
            });
        },
        changeGoods: function changeGoods(i, index) {
            console.log("i", i);
            console.log("iindex", index);
            this.leftMenuArray.map(function (el) {
                console.log("el", el);
                el.active = false;
            });

            this.leftMenuArray[index].active = true;
            this.currentPayArray = this.purchaseList[index].goods;
            this.groupCode = this.purchaseList[index].groupCode;
            console.log("222", this.groupCode);

            common.youmeng("我要备货", i.groupName);
        },
        addGoods: function addGoods(i, index, ev) {
            var _this3 = this;

            console.log("index", index);
            console.log("进入添加");
            this.freeCount = '';
            // let clientX = ev.clientX;
            // let clientY = ev.clientY;
            // // console.log(clientX,clientY)
            // let targetPosition = this.$refs.targetPosition.getBoundingClientRect();
            // let targetPosition_left = targetPosition.left;
            // let targetPosition_top = targetPosition.top;
            // let flydot = this.$refs['flydot'+index][0];
            // let flySpan  = flydot.getElementsByTagName('span')[0]

            // flySpan.style.top = (targetPosition_top+22)+"px";
            // flySpan.style.left = (targetPosition_left+22)+"px";

            // setTimeout(function () {
            //     flySpan.style.top = "";
            //     flySpan.style.left = "";
            //     console.log(clientX,clientY)
            //     flySpan.style.top = ev.clientY-5.5+"px";
            //     flySpan.style.left = ev.clientX-5.5+"px";
            // }, 400);

            this.addCount = true;

            if (i.currentNum >= 50) {
                common.toast({
                    "content": "采购数量不能大于50"
                });
                return;
            }
            // if(){}
            if (i.freeCount && i.currentNum >= i.freeCount || i.freeCount == 0) {
                common.toast({
                    "content": "赠送机具已达到上限"
                });
                return;
            }

            i.currentNum++;
            this.sumAmount();
            var hasGoodId = this.carList.findIndex(function (el, index) {
                console.log("el", el);
                return el.goodsType == i.goodsType;
            });

            if (hasGoodId === -1) {
                this.carList.push(i);
            }

            var timer = null;

            clearTimeout(timer);

            timer = setTimeout(function () {
                _this3.addCount = false;
            }, 300);

            common.youmeng("我要备货", "加号-" + i.goodsName);

            console.log("购物车商品", this.carList);
        },
        reduceGoods: function reduceGoods(i) {

            var hasGoodId = this.carList.findIndex(function (el, index) {
                return el.goodsType == i.goodsType;
            });

            if (this.totalNumber == 1) {
                this.showCar = false;
            }

            if (i.currentNum == 1) {
                this.carList.splice(hasGoodId, 1);
            }

            i.currentNum--;
            this.sumAmount();

            common.youmeng("我要备货", "减号-" + i.goodsName);
        },
        sumAmount: function sumAmount() {
            var _this4 = this;

            this.totalAmount = 0;
            this.totalNumber = 0;
            this.purchaseList.map(function (el) {
                el.goods.map(function (i) {
                    _this4.totalNumber += i.currentNum * 1;
                    _this4.totalAmount += i.currentNum * i.price;
                });
            });
        },
        displayCar: function displayCar() {
            if (this.totalNumber == 0) {
                return;
            }
            this.showCar = !this.showCar;
            if (this.showCar) {
                common.youmeng("我要备货", "打开购物车列表");
            } else {
                common.youmeng("我要备货", "关闭购物车列表");
            }
        },
        clearCarList: function clearCarList() {
            this.showClear = true;
        },
        clearConfirm: function clearConfirm() {
            var _this5 = this;

            this.carList = [];
            this.purchaseList.map(function (el) {
                el.goods.map(function (i) {
                    _this5.$set(i, "currentNum", 0);
                });
            });
            this.totalAmount = 0;
            this.totalCount = 0;
            this.totalNumber = 0;
            this.showCar = false;
            common.youmeng("我要备货", "清除购物车");
        },
        gotoPay: function gotoPay(e) {
            if (this.totalNumber == 0) {
                common.toast({
                    content: "您还未选择商品"
                });
                return;
            }
            e.stopPropagation();
            this.showCar = false;

            var purchaseJson = {};
            purchaseJson.carList = this.carList;
            console.log("this.carList", this.carList);
            purchaseJson.totalAmount = this.totalAmount;
            console.log("this.totalAmount", this.totalAmount);
            common.youmeng("我要备货", "点击提交订单按钮");

            this.$store.dispatch('saveCarList', purchaseJson);
            console.log("purchaseJson", purchaseJson);
            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1534:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1848:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mall"
  }, [_c('div', {
    ref: "mall_wrap",
    staticClass: "mall_wrap"
  }, [(!_vm.userNo) ? _c('div', {
    staticClass: "mall_banner"
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "mall_content"
  }, [_c('div', {
    staticClass: "mall_left"
  }, _vm._l((_vm.leftMenuArray), function(i, index) {
    return _c('span', {
      key: index,
      class: {
        'active': i.active
      },
      on: {
        "click": function($event) {
          return _vm.changeGoods(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.groupName))])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "mall_right"
  }, _vm._l((_vm.currentPayArray), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('div', {
      staticClass: "goodsImg"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + j.imgPath
      }
    }), _vm._v(" "), (index == 0) ? _c('span', [_c('i', [_vm._v("推荐")])]) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "goodsDeatil"
    }, [(j.freeCount) ? _c('div', {
      staticStyle: {
        "display": "none"
      },
      attrs: {
        "id": "freeCount"
      }
    }, [_vm._v(_vm._s(j.freeCount))]) : _vm._e(), _vm._v(" "), _c('b', [_vm._v(_vm._s(j.goodsName)), (j.freeCount >= 0) ? _c('span', {
      staticStyle: {
        "font-size": "0.24rem",
        "color": "#E95647",
        "float": "right"
      }
    }, [_vm._v("共" + _vm._s(j.freeCount) + "台")]) : _vm._e()]), _vm._v(" "), _c('p', {
      domProps: {
        "innerHTML": _vm._s(JSON.parse(j.remark).goodsDesc)
      }
    }), _vm._v(" "), (j.productCode == 'APP_YS_NOPOS' || j.productCode == 'APP_YS_ZDYQ' || j.productCode == 'APP_ZFY_NOPOS' || j.productCode == 'APP_CX_NOPOS' || j.productCode == 'CX_NOPOS') ? _c('div', [_c('button', {
      on: {
        "click": function($event) {
          return _vm.apply(j.goodsType)
        }
      }
    }, [_vm._v("立即申请")])]) : _c('div', [_c('div', {
      staticClass: "number"
    }, [_c('i', [_vm._v("￥" + _vm._s(j.price))]), _vm._v(" "), _c('em', [(j.currentNum >= 1) ? _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.reduceGoods(j)
        }
      }
    }) : _vm._e(), _vm._v(" "), (j.currentNum >= 1) ? _c('small', [_vm._v(_vm._s(j.currentNum))]) : _vm._e(), _vm._v(" "), (j.stockCount > 0) ? _c('a', {
      ref: 'flydot' + index,
      refInFor: true,
      staticClass: "add",
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.addGoods(j, index, $event)
        }
      }
    }, [_c('span', {
      style: ({
        'left': j.left + 'px',
        'top': j.top + 'px'
      })
    })]) : _c('span', {
      staticStyle: {
        "color": "red",
        "font-size": "0.24rem",
        "float": "right"
      }
    }, [_vm._v("已售罄")])])])])])])
  }), 0)]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "1.1rem"
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showCar),
      expression: "showCar"
    }],
    staticClass: "buyList_mask",
    on: {
      "click": function($event) {
        return _vm.displayCar()
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showCar),
      expression: "showCar"
    }],
    staticClass: "buyList"
  }, [_c('div', {
    staticClass: "buyList_head"
  }, [_c('p', [_vm._v("已选商品")]), _vm._v(" "), _c('span', {
    on: {
      "click": function($event) {
        return _vm.clearCarList()
      }
    }
  }, [_vm._v("清空")])]), _vm._v(" "), _c('div', {
    staticClass: "buyList_body",
    class: {
      'scroll': _vm.carList.length > 4
    }
  }, _vm._l((_vm.carList), function(i) {
    return _c('div', {
      key: i,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('em', [_c('i', [_c('strong', [_vm._v(_vm._s((i.currentNum * i.price).toFixed(2)))]), _vm._v(" 元")]), _vm._v(" "), _c('a', {
      on: {
        "click": function($event) {
          return _vm.reduceGoods(i)
        }
      }
    }), _vm._v(" "), _c('small', [_vm._v(_vm._s(i.currentNum))]), _vm._v(" "), _c('a', {
      staticClass: "add",
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.addGoods(i)
        }
      }
    })])]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.desc))])])
  }), 0)])]), _vm._v(" "), (_vm.groupCode == 'APP_MOBILE_SHOUYIN') ? _c('div') : _c('div', {
    staticClass: "bottomBar",
    on: {
      "click": function($event) {
        return _vm.displayCar()
      }
    }
  }, [_c('div', {
    ref: "targetPosition",
    staticClass: "shopCar",
    class: {
      'addCount': _vm.addCount
    }
  }, [_c('div', {
    staticClass: "shopNumber"
  }, [_vm._v(_vm._s(_vm.totalNumber))])]), _vm._v(" "), _c('div', {
    staticClass: "shopCount"
  }, [_c('p', [_c('b', [_vm._v(_vm._s(_vm.totalAmount.toFixed(2)) + "元")]), _vm._v(" "), _c('span', [_vm._v("免邮费")])]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay($event)
      }
    }
  }, [_vm._v("去结算")])])]), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.showClear,
      "boxInfo": _vm.clearInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.showClear = $event
      },
      "confirm": _vm.clearConfirm
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-312f18cd", module.exports)
  }
}

/***/ }),

/***/ 1995:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1534);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("db555046", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-312f18cd&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-312f18cd&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 664:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1995)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1459),
  /* template */
  __webpack_require__(1848),
  /* scopeId */
  "data-v-312f18cd",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\mall.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mall.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-312f18cd", Component.options)
  } else {
    hotAPI.reload("data-v-312f18cd", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});