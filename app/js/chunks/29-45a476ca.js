webpackJsonp([29],{

/***/ 1306:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_close_white.png?v=fe717e21";

/***/ }),

/***/ 1348:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_shop_add.png?v=3b24a4ac";

/***/ }),

/***/ 1349:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_shop_minus.png?v=75a37fa1";

/***/ }),

/***/ 1459:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

common.youmeng("我要备货", "进入我要备货");

exports.default = {
    data: function data() {
        return {
            totalAmount: 0,
            totalNumber: 0,
            showCar: false,
            carList: [],
            showClear: false,
            clearInfo: {
                "title": "温馨提示",
                "content": "确定要清空购物车吗？",
                "confirmText": "确定"
            },
            purchaseList: [],
            imgUrl: _apiConfig2.default.KY_IP,
            currentPayArray: [],
            leftMenuArray: [],
            groupCode: "",
            addCount: false,
            userNo: common.getUserNo(),
            proCode: common.getCookie('proCode'),
            num: ''
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        var _this = this;

        if (this.$route.query.type == 'link') {
            common.setCookie("proCode", '拉卡拉押金');
        }

        console.log("====", this.proCode);
        var bodyHeight = document.documentElement.clientHeight;
        //        this.$refs.mall_wrap.style.height = bodyHeight + "px";
        this.$nextTick(function () {
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseList"
            }, function (data) {

                if (data.data.length == 0) {
                    return;
                }

                console.log("======", data.data);

                _this.purchaseList = data.data;
                _this.purchaseList.map(function (el) {
                    _this.$set(el, "active", false);
                    _this.leftMenuArray.push({
                        "active": el.active,
                        "goods": el.goods,
                        "groupCode": el.groupCode,
                        "groupName": el.groupName
                    });
                    el.goods.map(function (i) {
                        _this.$set(i, "currentNum", 0);
                    });
                });
                var proCode = common.getCookie('proCode');

                //            this.tradeList.forEach((data.data) => {
                //            	if(data.data.groupName == proCode){
                //            		console.log('111')
                //            	}else{
                //            		console.log('2222')
                //            	}
                //            })


                for (var i = 0; i < data.data.length; i++) {
                    if (proCode == data.data[i].groupName) {
                        _this.num = i;
                        console.log('包含', _this.num);
                        _this.changeGoods(data.data[i], i);
                    } else {
                        console.log('不包含');
                    }
                }

                console.log('包含11', _this.num);
            });
        });
    },

    methods: {
        go: function go(i) {
            var _this2 = this;

            this.leftMenuArray[i].active = true;
            this.currentPayArray = this.purchaseList[i].goods;
            this.groupCode = this.purchaseList[i].groupCode;
            console.log("111", this.groupCode);

            this.currentPayArray.map(function (el, index) {

                _this2.$nextTick(function () {

                    var flydot = _this2.$refs['flydot' + index][i];
                    var position = flydot.getBoundingClientRect();
                    _this2.$set(el, 'left', position.left + 5.5);
                    _this2.$set(el, 'top', position.top + 5.5);
                });
            });
        },
        apply: function apply(goodsType) {
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
                data: {
                    address: "",
                    goods: JSON.stringify([{ type: goodsType, count: 1 }]),
                    payAmount: 0,
                    accAmount: 0
                },
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                window.location.href = data.data.ky_no_pos;
                // this.$router.push({
                //     path:data.data.ky_no_pos
                // })
            });
        },
        changeGoods: function changeGoods(i, index) {
            console.log("i", i);
            console.log("iindex", index);
            this.leftMenuArray.map(function (el) {
                console.log("el", el);
                el.active = false;
            });

            this.leftMenuArray[index].active = true;
            this.currentPayArray = this.purchaseList[index].goods;
            this.groupCode = this.purchaseList[index].groupCode;
            console.log("222", this.groupCode);

            common.youmeng("我要备货", i.groupName);
        },
        addGoods: function addGoods(i, index, ev) {
            var _this3 = this;

            console.log("index", index);
            console.log("进入添加");
            this.freeCount = '';
            // let clientX = ev.clientX;
            // let clientY = ev.clientY;
            // // console.log(clientX,clientY)
            // let targetPosition = this.$refs.targetPosition.getBoundingClientRect();
            // let targetPosition_left = targetPosition.left;
            // let targetPosition_top = targetPosition.top;
            // let flydot = this.$refs['flydot'+index][0];
            // let flySpan  = flydot.getElementsByTagName('span')[0]

            // flySpan.style.top = (targetPosition_top+22)+"px";
            // flySpan.style.left = (targetPosition_left+22)+"px";

            // setTimeout(function () {
            //     flySpan.style.top = "";
            //     flySpan.style.left = "";
            //     console.log(clientX,clientY)
            //     flySpan.style.top = ev.clientY-5.5+"px";
            //     flySpan.style.left = ev.clientX-5.5+"px";
            // }, 400);

            this.addCount = true;

            if (i.currentNum >= 50) {
                common.toast({
                    "content": "采购数量不能大于50"
                });
                return;
            }
            // if(){}
            if (i.freeCount && i.currentNum >= i.freeCount || i.freeCount == 0) {
                common.toast({
                    "content": "赠送机具已达到上限"
                });
                return;
            }

            i.currentNum++;
            this.sumAmount();
            var hasGoodId = this.carList.findIndex(function (el, index) {
                console.log("el", el);
                return el.goodsType == i.goodsType;
            });

            if (hasGoodId === -1) {
                this.carList.push(i);
            }

            var timer = null;

            clearTimeout(timer);

            timer = setTimeout(function () {
                _this3.addCount = false;
            }, 300);

            common.youmeng("我要备货", "加号-" + i.goodsName);

            console.log("购物车商品", this.carList);
        },
        reduceGoods: function reduceGoods(i) {

            var hasGoodId = this.carList.findIndex(function (el, index) {
                return el.goodsType == i.goodsType;
            });

            if (this.totalNumber == 1) {
                this.showCar = false;
            }

            if (i.currentNum == 1) {
                this.carList.splice(hasGoodId, 1);
            }

            i.currentNum--;
            this.sumAmount();

            common.youmeng("我要备货", "减号-" + i.goodsName);
        },
        sumAmount: function sumAmount() {
            var _this4 = this;

            this.totalAmount = 0;
            this.totalNumber = 0;
            this.purchaseList.map(function (el) {
                el.goods.map(function (i) {
                    _this4.totalNumber += i.currentNum * 1;
                    _this4.totalAmount += i.currentNum * i.price;
                });
            });
        },
        displayCar: function displayCar() {
            if (this.totalNumber == 0) {
                return;
            }
            this.showCar = !this.showCar;
            if (this.showCar) {
                common.youmeng("我要备货", "打开购物车列表");
            } else {
                common.youmeng("我要备货", "关闭购物车列表");
            }
        },
        clearCarList: function clearCarList() {
            this.showClear = true;
        },
        clearConfirm: function clearConfirm() {
            var _this5 = this;

            this.carList = [];
            this.purchaseList.map(function (el) {
                el.goods.map(function (i) {
                    _this5.$set(i, "currentNum", 0);
                });
            });
            this.totalAmount = 0;
            this.totalCount = 0;
            this.totalNumber = 0;
            this.showCar = false;
            common.youmeng("我要备货", "清除购物车");
        },
        gotoPay: function gotoPay(e) {
            if (this.totalNumber == 0) {
                common.toast({
                    content: "您还未选择商品"
                });
                return;
            }
            e.stopPropagation();
            this.showCar = false;

            var purchaseJson = {};
            purchaseJson.carList = this.carList;
            console.log("this.carList", this.carList);
            purchaseJson.totalAmount = this.totalAmount;
            console.log("this.totalAmount", this.totalAmount);
            common.youmeng("我要备货", "点击提交订单按钮");

            this.$store.dispatch('saveCarList', purchaseJson);
            console.log("purchaseJson", purchaseJson);
            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1534:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-312f18cd] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-312f18cd] {\n  background: #fff;\n}\n.tips_success[data-v-312f18cd] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-312f18cd] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-312f18cd] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-312f18cd],\n.fade-leave-active[data-v-312f18cd] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-312f18cd],\n.fade-leave-to[data-v-312f18cd] {\n  opacity: 0;\n}\n.default_button[data-v-312f18cd] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-312f18cd] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-312f18cd] {\n  position: relative;\n}\n.loading-tips[data-v-312f18cd] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.mall[data-v-312f18cd] {\n  width: 100%;\n  overflow: hidden;\n}\n.mall .mall_wrap[data-v-312f18cd] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  overflow: hidden;\n}\n.mall .mall_banner[data-v-312f18cd] {\n  width: 6.9rem;\n  height: 2.62rem;\n  background: url(" + __webpack_require__(1718) + ") no-repeat;\n  background-size: contain;\n  margin: 0.32rem auto 0.32rem;\n  position: relative;\n}\n.mall .mall_banner span[data-v-312f18cd] {\n  width: 0.26rem;\n  height: 0.26rem;\n  display: block;\n  background: url(" + __webpack_require__(1306) + ") no-repeat;\n  background-size: 0.26rem 0.26rem;\n  position: absolute;\n  right: 0.2rem;\n  top: 0.2rem;\n}\n.mall .mall_content[data-v-312f18cd] {\n  width: 100%;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  background: #fff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.mall .mall_content .mall_left[data-v-312f18cd] {\n  background: #f4f5fb;\n  height: 100%;\n  width: 1.6rem;\n}\n.mall .mall_content .mall_left span[data-v-312f18cd] {\n  display: block;\n  width: 100%;\n  height: 1.0rem;\n  font-size: 0.26rem;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  opacity: 0.6;\n  cursor: pointer;\n}\n.mall .mall_content .mall_left span.active[data-v-312f18cd] {\n  opacity: 1;\n  background: #fff;\n  font-weight: 700;\n}\n.mall .mall_content .mall_right[data-v-312f18cd] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  padding-left: 0.32rem;\n  height: 100%;\n  overflow: auto;\n  -webkit-overflow-scrolling: touch;\n}\n.mall .mall_content .mall_right .item[data-v-312f18cd] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 0.3rem 0;\n  position: relative;\n}\n.mall .mall_content .mall_right .item[data-v-312f18cd]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #e5e5e5;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.mall .mall_content .mall_right .item[data-v-312f18cd]:last-child:after {\n  content: '';\n  background: none;\n}\n.mall .mall_content .mall_right .item[data-v-312f18cd]:last-child:after {\n  content: \"\";\n  background: none;\n}\n.mall .mall_content .mall_right .item .goodsImg[data-v-312f18cd] {\n  width: 1.84rem;\n  height: 1.84rem;\n  background: #f4f5fb;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n}\n.mall .mall_content .mall_right .item .goodsImg span[data-v-312f18cd] {\n  background: #FFB740;\n  width: .6rem;\n  height: .28rem;\n  position: absolute;\n  left: 0;\n  top: 0;\n  color: #fff;\n  font-size: .24rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .mall_content .mall_right .item .goodsImg span i[data-v-312f18cd] {\n  -webkit-transform: scale(0.8);\n      -ms-transform: scale(0.8);\n          transform: scale(0.8);\n  display: block;\n}\n.mall .mall_content .mall_right .item .goodsImg img[data-v-312f18cd] {\n  width: 1.1rem;\n  height: 1.26rem;\n  display: block;\n}\n.mall .mall_content .mall_right .item .goodsDeatil[data-v-312f18cd] {\n  width: 3.08rem;\n  height: 1.84rem;\n  margin-left: 0.32rem;\n  position: relative;\n}\n.mall .mall_content .mall_right .item .goodsDeatil button[data-v-312f18cd] {\n  width: 2.16rem;\n  height: 0.64rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  font-size: 0.28rem;\n  color: #ffffff;\n  text-align: center;\n  float: left;\n  margin-top: 0.06rem;\n}\n.mall .mall_content .mall_right .item .goodsDeatil b[data-v-312f18cd] {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.28rem;\n}\n.mall .mall_content .mall_right .item .goodsDeatil p[data-v-312f18cd] {\n  color: #3D4A5B;\n  font-size: 0.24rem;\n  opacity: 0.6;\n  text-align: justify;\n  line-height: .32rem;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number[data-v-312f18cd] {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number i[data-v-312f18cd] {\n  color: #E95647;\n  font-size: 0.28rem;\n  font-weight: 700;\n  display: block;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number em[data-v-312f18cd] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number em a[data-v-312f18cd] {\n  width: 0.44rem;\n  height: 0.44rem;\n  display: block;\n  background: url(" + __webpack_require__(1349) + ") no-repeat;\n  background-size: contain;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number em a.add[data-v-312f18cd] {\n  background: url(" + __webpack_require__(1348) + ") no-repeat;\n  background-size: contain;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number em a.add span[data-v-312f18cd] {\n  width: 0.2rem;\n  height: 0.2rem;\n  display: block;\n  background: #f00;\n  border-radius: 100%;\n  position: fixed;\n  z-index: 200;\n  -webkit-transition: all 0.4s cubic-bezier(0.25, 0.01, 0.25, 1);\n  transition: all 0.4s cubic-bezier(0.25, 0.01, 0.25, 1);\n  display: none;\n}\n.mall .mall_content .mall_right .item .goodsDeatil .number em small[data-v-312f18cd] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  margin: 0 0.2rem;\n}\n.mall .bottomBar[data-v-312f18cd] {\n  width: 100%;\n  height: 0.78rem;\n  background: #484c66;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  z-index: 20;\n  padding: 0.16rem 0;\n}\n.mall .bottomBar .shopCar[data-v-312f18cd] {\n  width: 1.0rem;\n  height: 1.0rem;\n  background: url(" + __webpack_require__(1714) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  left: 0.32rem;\n  top: -0.5rem;\n}\n.mall .bottomBar .shopCar.addCount[data-v-312f18cd] {\n  -webkit-animation: addCount ease 0.3s;\n}\n.mall .bottomBar .shopCar .shopNumber[data-v-312f18cd] {\n  width: 0.4rem;\n  height: 0.28rem;\n  background: url(" + __webpack_require__(1717) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  right: -0.2rem;\n  top: 0.1rem;\n  font-size: 0.24rem;\n  color: #fff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .bottomBar .shopCount[data-v-312f18cd] {\n  width: 5.58rem;\n  height: 100%;\n  margin-left: 1.6rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .bottomBar .shopCount p[data-v-312f18cd] {\n  height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.mall .bottomBar .shopCount p b[data-v-312f18cd] {\n  font-size: 0.34rem;\n  color: #FFFFFF;\n  display: block;\n  line-height: 0.4rem;\n}\n.mall .bottomBar .shopCount p span[data-v-312f18cd] {\n  font-size: 0.24rem;\n  color: #FFFFFF;\n  opacity: 0.3;\n  display: block;\n  line-height: 0.24rem;\n}\n.mall .bottomBar .shopCount a[data-v-312f18cd] {\n  width: 2.16rem;\n  height: 0.8rem;\n  background: #3F83FF;\n  border-radius: 0.08rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.34rem;\n  color: #FFFFFF;\n}\n.mall .buyList_mask[data-v-312f18cd] {\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.65);\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 10;\n}\n.mall .buyList[data-v-312f18cd] {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n  position: fixed;\n  left: 0;\n  z-index: 15;\n  bottom: 1.1rem;\n  padding: 0.5rem 0;\n}\n.mall .buyList .buyList_head[data-v-312f18cd] {\n  width: 92%;\n  height: 0.4rem;\n  margin: 0 auto 0.5rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .buyList .buyList_head p[data-v-312f18cd] {\n  height: 100%;\n  font-size: 0.32rem;\n  opacity: 0.6;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .buyList .buyList_head span[data-v-312f18cd] {\n  height: 100%;\n  font-size: 0.26rem;\n  opacity: 0.6;\n  color: #3D4A5B;\n  background: url(" + __webpack_require__(1715) + ") no-repeat left top 0.04rem;\n  background-size: 0.24rem 0.26rem;\n  padding-left: 0.34rem;\n}\n.mall .buyList .buyList_body[data-v-312f18cd] {\n  width: 92%;\n  margin: 0 auto 0.5rem;\n}\n.mall .buyList .buyList_body.scroll[data-v-312f18cd] {\n  overflow: auto;\n  height: 4.88rem;\n}\n.mall .buyList .buyList_body .item[data-v-312f18cd] {\n  width: 100%;\n  margin-bottom: 0.45rem;\n}\n.mall .buyList .buyList_body .item p[data-v-312f18cd] {\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mall .buyList .buyList_body .item p b[data-v-312f18cd] {\n  display: block;\n  font-size: 0.32rem;\n  color: #3D4A5B;\n}\n.mall .buyList .buyList_body .item p em[data-v-312f18cd] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n}\n.mall .buyList .buyList_body .item p em i[data-v-312f18cd] {\n  color: #E95647;\n  font-size: 0.24rem;\n  margin-right: 0.35rem;\n}\n.mall .buyList .buyList_body .item p em i strong[data-v-312f18cd] {\n  font-size: 0.30rem;\n}\n.mall .buyList .buyList_body .item p em a[data-v-312f18cd] {\n  width: 0.44rem;\n  height: 0.44rem;\n  display: block;\n  background: url(" + __webpack_require__(1349) + ") no-repeat;\n  background-size: contain;\n}\n.mall .buyList .buyList_body .item p em a.add[data-v-312f18cd] {\n  background: url(" + __webpack_require__(1348) + ") no-repeat;\n  background-size: contain;\n}\n.mall .buyList .buyList_body .item p em small[data-v-312f18cd] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  margin: 0 0.2rem;\n}\n.mall .buyList .buyList_body .item span[data-v-312f18cd] {\n  font-size: 0.24rem;\n  opacity: 0.5;\n  color: #3D4A5B;\n  display: block;\n}\n@-webkit-keyframes addCount {\n0% {\n    -webkit-transform: scale(1) translate(0, 0);\n}\n50% {\n    -webkit-transform: scale(1.3) translate(0, -15px);\n}\n100% {\n    -webkit-transform: scale(1) translate(0, 0);\n}\n}\n@keyframes addCount {\n0% {\n    -webkit-transform: scale(1) translate(0, 0);\n            transform: scale(1) translate(0, 0);\n}\n50% {\n    -webkit-transform: scale(1.3) translate(0, -15px);\n            transform: scale(1.3) translate(0, -15px);\n}\n100% {\n    -webkit-transform: scale(1) translate(0, 0);\n            transform: scale(1) translate(0, 0);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1714:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_shop_cart.png?v=c6a1c0f7";

/***/ }),

/***/ 1715:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_shop_delete.png?v=02ed837e";

/***/ }),

/***/ 1717:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_shop_number.png?v=033057c4";

/***/ }),

/***/ 1718:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_shop_process.png?v=6ddaa8aa";

/***/ }),

/***/ 1848:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mall"
  }, [_c('div', {
    ref: "mall_wrap",
    staticClass: "mall_wrap"
  }, [(!_vm.userNo) ? _c('div', {
    staticClass: "mall_banner"
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "mall_content"
  }, [_c('div', {
    staticClass: "mall_left"
  }, _vm._l((_vm.leftMenuArray), function(i, index) {
    return _c('span', {
      key: index,
      class: {
        'active': i.active
      },
      on: {
        "click": function($event) {
          return _vm.changeGoods(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.groupName))])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "mall_right"
  }, _vm._l((_vm.currentPayArray), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('div', {
      staticClass: "goodsImg"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + j.imgPath
      }
    }), _vm._v(" "), (index == 0) ? _c('span', [_c('i', [_vm._v("推荐")])]) : _vm._e()]), _vm._v(" "), _c('div', {
      staticClass: "goodsDeatil"
    }, [(j.freeCount) ? _c('div', {
      staticStyle: {
        "display": "none"
      },
      attrs: {
        "id": "freeCount"
      }
    }, [_vm._v(_vm._s(j.freeCount))]) : _vm._e(), _vm._v(" "), _c('b', [_vm._v(_vm._s(j.goodsName)), (j.freeCount >= 0) ? _c('span', {
      staticStyle: {
        "font-size": "0.24rem",
        "color": "#E95647",
        "float": "right"
      }
    }, [_vm._v("共" + _vm._s(j.freeCount) + "台")]) : _vm._e()]), _vm._v(" "), _c('p', {
      domProps: {
        "innerHTML": _vm._s(JSON.parse(j.remark).goodsDesc)
      }
    }), _vm._v(" "), (j.productCode == 'APP_YS_NOPOS' || j.productCode == 'APP_YS_ZDYQ' || j.productCode == 'APP_ZFY_NOPOS' || j.productCode == 'APP_CX_NOPOS' || j.productCode == 'CX_NOPOS') ? _c('div', [_c('button', {
      on: {
        "click": function($event) {
          return _vm.apply(j.goodsType)
        }
      }
    }, [_vm._v("立即申请")])]) : _c('div', [_c('div', {
      staticClass: "number"
    }, [_c('i', [_vm._v("￥" + _vm._s(j.price))]), _vm._v(" "), _c('em', [(j.currentNum >= 1) ? _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.reduceGoods(j)
        }
      }
    }) : _vm._e(), _vm._v(" "), (j.currentNum >= 1) ? _c('small', [_vm._v(_vm._s(j.currentNum))]) : _vm._e(), _vm._v(" "), (j.stockCount > 0) ? _c('a', {
      ref: 'flydot' + index,
      refInFor: true,
      staticClass: "add",
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.addGoods(j, index, $event)
        }
      }
    }, [_c('span', {
      style: ({
        'left': j.left + 'px',
        'top': j.top + 'px'
      })
    })]) : _c('span', {
      staticStyle: {
        "color": "red",
        "font-size": "0.24rem",
        "float": "right"
      }
    }, [_vm._v("已售罄")])])])])])])
  }), 0)]), _vm._v(" "), _c('div', {
    staticStyle: {
      "height": "1.1rem"
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showCar),
      expression: "showCar"
    }],
    staticClass: "buyList_mask",
    on: {
      "click": function($event) {
        return _vm.displayCar()
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showCar),
      expression: "showCar"
    }],
    staticClass: "buyList"
  }, [_c('div', {
    staticClass: "buyList_head"
  }, [_c('p', [_vm._v("已选商品")]), _vm._v(" "), _c('span', {
    on: {
      "click": function($event) {
        return _vm.clearCarList()
      }
    }
  }, [_vm._v("清空")])]), _vm._v(" "), _c('div', {
    staticClass: "buyList_body",
    class: {
      'scroll': _vm.carList.length > 4
    }
  }, _vm._l((_vm.carList), function(i) {
    return _c('div', {
      key: i,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('em', [_c('i', [_c('strong', [_vm._v(_vm._s((i.currentNum * i.price).toFixed(2)))]), _vm._v(" 元")]), _vm._v(" "), _c('a', {
      on: {
        "click": function($event) {
          return _vm.reduceGoods(i)
        }
      }
    }), _vm._v(" "), _c('small', [_vm._v(_vm._s(i.currentNum))]), _vm._v(" "), _c('a', {
      staticClass: "add",
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.addGoods(i)
        }
      }
    })])]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.desc))])])
  }), 0)])]), _vm._v(" "), (_vm.groupCode == 'APP_MOBILE_SHOUYIN') ? _c('div') : _c('div', {
    staticClass: "bottomBar",
    on: {
      "click": function($event) {
        return _vm.displayCar()
      }
    }
  }, [_c('div', {
    ref: "targetPosition",
    staticClass: "shopCar",
    class: {
      'addCount': _vm.addCount
    }
  }, [_c('div', {
    staticClass: "shopNumber"
  }, [_vm._v(_vm._s(_vm.totalNumber))])]), _vm._v(" "), _c('div', {
    staticClass: "shopCount"
  }, [_c('p', [_c('b', [_vm._v(_vm._s(_vm.totalAmount.toFixed(2)) + "元")]), _vm._v(" "), _c('span', [_vm._v("免邮费")])]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.gotoPay($event)
      }
    }
  }, [_vm._v("去结算")])])]), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.showClear,
      "boxInfo": _vm.clearInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.showClear = $event
      },
      "confirm": _vm.clearConfirm
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-312f18cd", module.exports)
  }
}

/***/ }),

/***/ 1995:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1534);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("db555046", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-312f18cd&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-312f18cd&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 664:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1995)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1459),
  /* template */
  __webpack_require__(1848),
  /* scopeId */
  "data-v-312f18cd",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\mall.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mall.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-312f18cd", Component.options)
  } else {
    hotAPI.reload("data-v-312f18cd", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});