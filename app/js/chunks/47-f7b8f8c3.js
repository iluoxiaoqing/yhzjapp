webpackJsonp([47],{

/***/ 1225:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/close_btn.png?v=97f71807";

/***/ }),

/***/ 1242:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1247)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(959),
  /* template */
  __webpack_require__(1245),
  /* scopeId */
  "data-v-5140d656",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\confirmInfo.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] confirmInfo.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5140d656", Component.options)
  } else {
    hotAPI.reload("data-v-5140d656", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 1245:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "confirmInfo"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_mask",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_box"
  }, [_c('div', {
    staticClass: "confirmInfo_close",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_text"
  }, [_c('h1', [_vm._v("确认信息")]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("姓名")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.realname))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("身份证号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.identityNo))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("手机号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.userName))])]), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_line"
  }), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.boxInfo.content))]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.submitBox()
      }
    }
  }, [_vm._v("确认办理")])])]) : _vm._e()])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5140d656", module.exports)
  }
}

/***/ }),

/***/ 1247:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(963);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("48f57cb6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1288:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _confirmInfo = __webpack_require__(1242);

var _confirmInfo2 = _interopRequireDefault(_confirmInfo);

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showConfirm: false,
            imgUrl: _apiConfig2.default.KY_IP,
            loanData: {
                custDesc: {}
            },
            comfirmText: {
                "content": "请确认以上信息与贷款申请信息完全一致，填写错误将会导致申请无法通过。"
            }
        };
    },

    components: {
        confirmInfo: _confirmInfo2.default
    },
    mounted: function mounted() {

        window.goBack = this.goBack;

        if (this.$store.state.loanData.logo) {
            this.loanData = this.$store.state.loanData;
            console.log("loanDate", this.loanData);
        } else {
            this.loanData = JSON.parse(sessionStorage.getItem("loanData"));
        }
        console.log("loanDate", this.loanData);
    },

    methods: {
        goBack: function goBack() {
            this.$router.push({
                "path": "applyLoan",
                "query": {
                    buss: "LOAN_CODE"
                }
            });
        },
        comfirmInfo: function comfirmInfo() {
            console.log(this.loanData.productCode);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "/loan/createLoanOrder",
                "data": {
                    productCode: this.loanData.applyCode
                }
            }, function (data) {
                window.location.href = data.data;
            });
        },
        confirmApply: function confirmApply() {
            common.youmeng("贷款申请", "点击确认申请");
            this.showConfirm = true;
        },
        gotoAgent: function gotoAgent(j) {
            var _this = this;

            common.youmeng("贷款申请", "点击立即申请");

            this.selectObject = j;
            var buss = this.$route.query.buss;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "user/isShow",
                "data": {
                    "businessCode": LOAN_CODE,
                    "productCode": j.productCode
                }
            }, function (data) {
                if (data.data.isShow) {
                    _this.repeatShow = true;
                    _this.repeatInfo.content = data.data.showMessage;
                } else {
                    _this.showInfoContent(j);
                }
            });
        },
        showInfoContent: function showInfoContent(j) {
            this.$store.dispatch("saveLoanData", j);

            this.$router.push({
                "path": "loanAgent"
            });
        },
        repeatConfirm: function repeatConfirm() {
            this.showInfoContent(this.selectObject);
        }
    }
};

/***/ }),

/***/ 1562:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-50678f9c] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-50678f9c] {\n  background: #fff;\n}\n.tips_success[data-v-50678f9c] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-50678f9c] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-50678f9c] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-50678f9c],\n.fade-leave-active[data-v-50678f9c] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-50678f9c],\n.fade-leave-to[data-v-50678f9c] {\n  opacity: 0;\n}\n.default_button[data-v-50678f9c] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-50678f9c] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-50678f9c] {\n  position: relative;\n}\n.loading-tips[data-v-50678f9c] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.loanAgent[data-v-50678f9c] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: .2rem;\n  padding-bottom: 1.54rem;\n}\n.loanAgent .loanAgent_banner[data-v-50678f9c] {\n  width: 6.86rem;\n  margin: 0 auto;\n  overflow: hidden;\n  background-image: -webkit-linear-gradient(314deg, #3F83FF 0%, #51ACFF 100%);\n  background-image: linear-gradient(136deg, #3F83FF 0%, #51ACFF 100%);\n  border-radius: .08rem;\n  padding-top: .6rem;\n  padding: 0.6rem 0 .32rem;\n}\n.loanAgent .loanAgent_banner .banner_top[data-v-50678f9c] {\n  overflow: hidden;\n  padding: 0 .32rem .5rem;\n  position: relative;\n}\n.loanAgent .loanAgent_banner .banner_top[data-v-50678f9c]:after {\n  content: \"\";\n  width: 100%;\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #fff;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  opacity: 0.29;\n}\n.loanAgent .loanAgent_banner .banner_top p[data-v-50678f9c] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  color: #fff;\n}\n.loanAgent .loanAgent_banner .banner_top p b[data-v-50678f9c] {\n  font-size: .6rem;\n  font-weight: normal;\n}\n.loanAgent .loanAgent_banner .banner_top p span[data-v-50678f9c] {\n  font-size: .24rem;\n}\n.loanAgent .loanAgent_banner .banner_top em[data-v-50678f9c] {\n  font-size: .24rem;\n  color: #fff;\n  display: block;\n  opacity: 0.4;\n}\n.loanAgent .loanAgent_banner .banner_bottom[data-v-50678f9c] {\n  overflow: hidden;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: .3rem .32rem 0;\n}\n.loanAgent .loanAgent_banner .banner_bottom p[data-v-50678f9c] {\n  color: #fff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: start;\n  -webkit-align-items: flex-start;\n     -moz-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n}\n.loanAgent .loanAgent_banner .banner_bottom p span[data-v-50678f9c] {\n  font-size: .32rem;\n  display: block;\n}\n.loanAgent .loanAgent_banner .banner_bottom p i[data-v-50678f9c] {\n  font-size: .24rem;\n  display: block;\n  -webkit-transform: scale(0.83);\n      -ms-transform: scale(0.83);\n          transform: scale(0.83);\n}\n.loanAgent .loanAgent_banner .banner_bottom p em[data-v-50678f9c] {\n  height: .7rem;\n  width: 1px;\n  background: #fff;\n  display: block;\n  opacity: 0.3;\n  margin: 0 .29rem;\n}\n.loanAgent .loanAgent_about[data-v-50678f9c] {\n  width: 6.86rem;\n  margin: .16rem auto;\n  overflow: hidden;\n  background: #FFFFFF;\n  box-shadow: 0 0 0.32rem 0 rgba(63, 131, 255, 0.15);\n  border-radius: .08rem;\n  padding-top: .32rem;\n}\n.loanAgent .loanAgent_about .about_title[data-v-50678f9c] {\n  font-size: .32rem;\n  text-align: center;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.loanAgent .loanAgent_about .about_title img[data-v-50678f9c] {\n  width: .4rem;\n  height: .4rem;\n  margin-right: .2rem;\n}\n.loanAgent .loanAgent_about .about_content[data-v-50678f9c] {\n  padding: .32rem .32rem .2rem;\n}\n.loanAgent .loanAgent_about .about_content p[data-v-50678f9c] {\n  color: #3D4A5B;\n  margin-bottom: .48rem;\n}\n.loanAgent .loanAgent_about .about_content p b[data-v-50678f9c] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: .05rem;\n}\n.loanAgent .loanAgent_about .about_content p b i[data-v-50678f9c] {\n  width: .08rem;\n  height: .08rem;\n  background: #3F83FF;\n  margin-right: .08rem;\n  border-radius: 100%;\n}\n.loanAgent .loanAgent_about .about_content p b i.yellow[data-v-50678f9c] {\n  background: #FBBF00;\n}\n.loanAgent .loanAgent_about .about_content p b small[data-v-50678f9c] {\n  opacity: 0.8;\n  font-size: .32rem;\n}\n.loanAgent .loanAgent_about .about_content p span[data-v-50678f9c] {\n  display: block;\n  font-size: .24rem;\n  opacity: 0.4;\n  text-align: justify;\n  margin-left: .16rem;\n}\n.loanAgent .loanAgent_button[data-v-50678f9c] {\n  width: 6.86rem;\n  height: .94rem;\n  position: fixed;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  bottom: .32rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: .32rem;\n  color: #fff;\n  border-radius: .08rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1875:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loanAgent"
  }, [_c('div', {
    staticClass: "loanAgent_banner"
  }, [_c('div', {
    staticClass: "banner_top"
  }, [_c('p', [_c('b', [_vm._v(_vm._s(_vm.loanData.amountInterval))])]), _vm._v(" "), _c('em', [_vm._v("额度范围(元)")])]), _vm._v(" "), _c('div', {
    staticClass: "banner_bottom"
  }, [_c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.interest))]), _vm._v(" "), _c('i', [_vm._v("利息")])]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.remandLimit))]), _vm._v(" "), _c('i', [_vm._v("贷款期限")])]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.auditTime))]), _vm._v(" "), _c('i', [_vm._v("放款时长")])])])]), _vm._v(" "), _c('div', {
    staticClass: "loanAgent_about"
  }, [_c('div', {
    staticClass: "about_title"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.loanData.logo,
      "alt": ""
    }
  }), _vm._v("\n            " + _vm._s(_vm.loanData.title) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "about_content"
  }, [_c('p', [_vm._m(2), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.applyProcess)
    }
  })]), _vm._v(" "), _c('p', [_vm._m(3), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.applyClaim)
    }
  })]), _vm._v(" "), _c('p', [_vm._m(4), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.prompt)
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "loanAgent_button",
    on: {
      "click": function($event) {
        return _vm.confirmApply()
      }
    }
  }, [_vm._v("确认申请")]), _vm._v(" "), _c('confirm-info', {
    attrs: {
      "visible": _vm.showConfirm,
      "boxInfo": _vm.comfirmText
    },
    on: {
      "update:visible": function($event) {
        _vm.showConfirm = $event
      },
      "submitBox": _vm.comfirmInfo
    }
  })], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('em')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('em')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i'), _vm._v(" "), _c('small', [_vm._v("申请流程")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i'), _vm._v(" "), _c('small', [_vm._v("申请条件")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i', {
    staticClass: "yellow"
  }), _vm._v(" "), _c('small', [_vm._v("特别提示")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-50678f9c", module.exports)
  }
}

/***/ }),

/***/ 2023:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1562);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("78aaf0c8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50678f9c&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanAgent.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50678f9c&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanAgent.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 616:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2023)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1288),
  /* template */
  __webpack_require__(1875),
  /* scopeId */
  "data-v-50678f9c",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\loanAgent.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loanAgent.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-50678f9c", Component.options)
  } else {
    hotAPI.reload("data-v-50678f9c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 959:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            userInfo: {}
        };
    },

    props: {
        visible: {
            type: Boolean,
            default: false
        },
        boxInfo: {
            type: Object,
            default: ""
        }
    },
    watch: {
        visible: function visible(value) {
            var _this = this;

            if (value) {
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "user/userIC"
                }, function (data) {
                    console.log(data);
                    _this.userInfo = data.data;
                });
            }
        }
    },
    mounted: function mounted() {},

    methods: {
        closeBox: function closeBox() {
            this.$emit("update:visible", false);
        },
        submitBox: function submitBox() {
            this.$emit("submitBox");
            this.$emit("update:visible", false);
        }
    }
};

/***/ }),

/***/ 963:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "\n.confirmInfo_mask[data-v-5140d656] {\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  position: fixed;\n  left: 0;\n  top: 0;\n  z-index: 10;\n}\n.confirmInfo_box[data-v-5140d656] {\n  width: 6.3rem;\n  background: #fff;\n  padding: .62rem 0;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n      -ms-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  z-index: 10;\n  border-radius: .08rem;\n}\n.confirmInfo_box .confirmInfo_close[data-v-5140d656] {\n  width: .6rem;\n  height: .6rem;\n  background: url(" + __webpack_require__(1225) + ") no-repeat;\n  background-size: contain;\n  position: absolute;\n  right: 0;\n  top: -0.92rem;\n}\n.confirmInfo_box .confirmInfo_text[data-v-5140d656] {\n  width: 4.72rem;\n  overflow: hidden;\n  margin: 0 auto;\n  color: #3D4A5B;\n}\n.confirmInfo_box .confirmInfo_text h1[data-v-5140d656] {\n  display: block;\n  font-size: .56rem;\n}\n.confirmInfo_box .confirmInfo_text .cell span[data-v-5140d656] {\n  font-size: .24rem;\n  opacity: 0.8;\n  display: block;\n  margin-top: .16rem;\n}\n.confirmInfo_box .confirmInfo_text .cell b[data-v-5140d656] {\n  font-size: .32rem;\n  display: block;\n}\n.confirmInfo_box .confirmInfo_text .confirmInfo_line[data-v-5140d656] {\n  width: 100%;\n  height: 1px;\n  background: #3F83FF;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n  margin: .46rem 0 .3rem;\n}\n.confirmInfo_box .confirmInfo_text p[data-v-5140d656] {\n  font-size: .24rem;\n  text-align: justify;\n  opacity: 0.8;\n}\n.confirmInfo_box .confirmInfo_text a[data-v-5140d656] {\n  width: 100%;\n  height: .8rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: .32rem;\n  color: #fff;\n  border-radius: .08rem;\n  margin-top: .32rem;\n}\n", ""]);

// exports


/***/ })

});