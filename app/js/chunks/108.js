webpackJsonp([108],{

/***/ 1383:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            listData: "",
            pageNo: 1
        };
    },
    mounted: function mounted() {
        this.getInfo();
    },

    methods: {
        getInfo: function getInfo() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "wonderfulActivityInfo/findMarketPolicy",
                data: {
                    bannerType: "MARKET_POLICY"
                },
                showLoading: false
            }, function (data) {
                _this.listData = data.data;
            });
        },
        go: function go(i, j) {
            common.setCookie("imgTitle", j);
            common.setCookie("imgAddress", i);
            // window.location.href = api.WEB_URL + 'imgUrl?imgAddress=' + i + '&imgTitle=' + j;
            window.location.href = _apiConfig2.default.WEB_URL + 'imgUrl?filePath=' + i + '&proTitle=' + j;
            // window.location.href = i;
        }
    }
};

/***/ }),

/***/ 1476:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-1e7a692e] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-1e7a692e] {\n  background: #fff;\n}\n.tips_success[data-v-1e7a692e] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-1e7a692e] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-1e7a692e] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-1e7a692e],\n.fade-leave-active[data-v-1e7a692e] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-1e7a692e],\n.fade-leave-to[data-v-1e7a692e] {\n  opacity: 0;\n}\n.default_button[data-v-1e7a692e] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-1e7a692e] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-1e7a692e] {\n  position: relative;\n}\n.loading-tips[data-v-1e7a692e] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.contain .imgBak[data-v-1e7a692e] {\n  background-size: 100% 100%;\n}\n.contain .imgBak .addressImg[data-v-1e7a692e] {\n  width: 100%;\n}\n.contain .img[data-v-1e7a692e] {\n  width: 94%;\n  height: 2.8rem;\n  margin: 0.24rem 3% 0;\n  background-size: 100% 100%;\n  border-radius: 0.1rem;\n}\n.contain .img .status[data-v-1e7a692e] {\n  font-size: 0.22rem;\n  font-weight: 500;\n  color: #FFFFFF;\n  line-height: 0.5rem;\n  float: right;\n  text-align: center;\n}\n.contain .img .EFFECT[data-v-1e7a692e] {\n  width: 1.56rem;\n  height: 0.52rem;\n  background: #FFC925;\n  border-radius: 0px 0px 8px 8px;\n  float: right;\n}\n.contain .img .PREHEAT[data-v-1e7a692e] {\n  width: 1.56rem;\n  height: 0.52rem;\n  background: #57CC64;\n  border-radius: 0px 0px 8px 8px;\n  float: right;\n}\n.contain .img .EXPIRE[data-v-1e7a692e] {\n  width: 1.56rem;\n  height: 0.52rem;\n  background: #FF1C43;\n  border-radius: 0px 0px 8px 8px;\n  float: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 1758:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, _vm._l((_vm.listData), function(i, idx) {
    return _c('div', {
      key: idx,
      staticClass: "img",
      style: ({
        backgroundImage: 'url(' + i.imgUrl + ')'
      }),
      on: {
        "click": function($event) {
          return _vm.go(i.wonderfulImgUrl, i.activityTitle)
        }
      }
    }, [(i.acticityStatus == 'EXPIRE') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("已结束")]) : (i.acticityStatus == 'EFFECT') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("进行中")]) : (i.acticityStatus == 'PREHEAT') ? _c('div', {
      staticClass: "status",
      class: i.acticityStatus
    }, [_vm._v("未开始")]) : _vm._e()])
  }), 0)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1e7a692e", module.exports)
  }
}

/***/ }),

/***/ 1892:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1476);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("25ce295d", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1e7a692e&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./marketPolicy.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1e7a692e&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./marketPolicy.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 599:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1892)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1383),
  /* template */
  __webpack_require__(1758),
  /* scopeId */
  "data-v-1e7a692e",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\policy\\marketPolicy.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] marketPolicy.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1e7a692e", Component.options)
  } else {
    hotAPI.reload("data-v-1e7a692e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});