webpackJsonp([63],{

/***/ 1287:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showMenu: false,
            showType: false,
            showIncomePay: false,
            showIncomeType: false,
            showMask: false,
            showArrow: false,
            incomePayText: "全部",
            incomeTypeText: "全部",
            typeText: "业务类型",
            incomePayArray: [{
                "select": true,
                "content": "全部",
                "detailCode": ""
            }, { "select": false, "content": "收入", "detailCode": "PLUS" }, { "select": false, "content": "支出", "detailCode": "SUB" }],
            typeArray: [{
                "select": true,
                "content": "全部",
                "bussCode": ""
            }, { "select": false, "content": "邀请推手", "bussCode": "RECOMMEND_ORDER_CODE" }, { "select": false, "content": "推荐开店", "bussCode": "SHOP_CODE" }, { "select": false, "content": "推荐办卡", "bussCode": "CREDIT_CARD_CODE" }, { "select": false, "content": "推荐贷款", "bussCode": "LOAN_CODE" }, { "select": false, "content": "店铺分润", "bussCode": "TRANS_COMM_CODE" }, { "select": false, "content": "活动奖励", "bussCode": "ACTIVITY_REWARD_CODE" }],
            incomeTypeArray: [{
                "select": true,
                "content": "全部",
                "accCode": ""
            }, { "select": false, "content": "直接", "accCode": "ZHIJIE_INCOME" }, { "select": false, "content": "间接", "accCode": "JIANJIE_INCOME" }, { "select": false, "content": "平台奖励", "accCode": "LEVEL_BUTIE" }],
            canClick: true,
            getAllList: [],
            currentPage: 1,
            isNoData: false
        };
    },
    mounted: function mounted() {

        this.getHeight();

        this.detailCode = this.incomePayArray[0].detailCode;
        this.bussCode = this.typeArray[0].bussCode;
        this.accCode = this.incomeTypeArray[0].accCode;

        common.youmeng("收支明细", "进入收支明细");
        this.getpageList();
    },

    filters: {
        keepTwoNum: function keepTwoNum(value) {
            value = Number(value);
            return value.toFixed(2);
        }
    },
    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        closeMenu: function closeMenu() {
            this.showMenu = false;
        },
        incomePay: function incomePay() {
            this.showMenu = !this.showMenu;
            this.showMask = true;
            this.showIncomePay = true;

            if (!this.showMenu) {
                console.log(this.detailCode + "|" + this.bussCode + "|" + this.accCode);
                this.getAllList = [];
                this.currentPage = 1;
                this.getpageList();
            }
        },
        getpageList: function getpageList() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "acc/getDetailInfo",
                data: {
                    detailCode: this.detailCode,
                    bussCode: this.bussCode,
                    accCode: this.accCode,
                    page: this.currentPage
                },
                showLoading: false
            }, function (data) {

                if (data.data.object.length > 0) {
                    if (_this.currentPage == 1) {
                        _this.getAllList = [];
                    }
                    data.data.object.map(function (el) {
                        _this.$set(el, "show", true);
                        _this.getAllList.push(el);
                    });

                    _this.isNoData = false;
                    _this.currentPage++;
                } else {
                    if (_this.currentPage == 1) {
                        _this.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
            });
        },
        refresh: function refresh(loaded) {
            this.currentPage = 1;
            this.getpageList();
        },
        loadmore: function loadmore(loaded) {
            this.getpageList();
        },
        submit: function submit() {
            console.log("detailCode", this.detailCode);
        },
        checkItem: function checkItem(i, index, detailCode) {

            common.youmeng("收支明细", i.content);

            this.incomePayArray.map(function (el) {
                el.select = false;
            });
            this.detailCode = detailCode;

            i.select = true;
            if (i.content == "收入") {
                this.showArrow = true;
                this.showType = true;
            } else {
                this.showArrow = false;
                this.showType = false;
                this.typeText = "业务类型";
            }
            this.incomePayText = i.content;
            if (this.detailCode == "SUB" || this.detailCode == "") {
                this.bussCode = "";
                this.accCode = "";
            }

            console.log("detailCode", this.detailCode);
        },
        chooseType: function chooseType() {
            this.showMenu = !this.showMenu;
        },
        checkIncomeType: function checkIncomeType(i, index, accCode) {

            common.youmeng("收支明细", i.content);

            this.incomeTypeArray.map(function (el) {
                el.select = false;
            });
            this.accCode = accCode;
            console.log("accCode", this.accCode);
            i.select = true;
        },
        checkType: function checkType(i, index, bussCode) {

            common.youmeng("收支明细", i.content);

            this.typeArray.map(function (el) {
                el.select = false;
            });
            this.bussCode = bussCode;
            console.log("bussCode", this.bussCode);
            i.select = true;
            if (i.content == "全部") {
                this.typeText = "业务类型";
                // return;
            }
            this.typeText = i.content;
        }
    }
};

/***/ }),

/***/ 1570:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\budgetDetail\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1883:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "budgetDetail"
  }, [_c('div', {
    staticClass: "header"
  }, [_c('div', {
    staticClass: "budgetDetail_head"
  }, [(_vm.incomePayText == '全部') ? _c('button', {
    on: {
      "click": function($event) {
        return _vm.incomePay()
      }
    }
  }, [_vm._v("\n                " + _vm._s(_vm.incomePayText) + "\n                "), _c('i', {
    class: {
      'active': _vm.showMenu
    }
  })]) : _c('button', {
    staticClass: "blue",
    on: {
      "click": function($event) {
        return _vm.incomePay()
      }
    }
  }, [_vm._v("\n                " + _vm._s(_vm.incomePayText) + "\n                "), _c('i', {
    class: {
      'active': _vm.showMenu
    }
  })]), _vm._v(" "), (_vm.typeText == '业务类型') ? _c('button', {
    attrs: {
      "disabled": !_vm.showArrow
    },
    on: {
      "click": function($event) {
        return _vm.chooseType()
      }
    }
  }, [_vm._v("\n                " + _vm._s(_vm.typeText) + "\n                "), (_vm.showArrow) ? _c('i', {
    class: {
      'active': _vm.showMenu
    }
  }) : _vm._e()]) : _c('button', {
    staticClass: "blue",
    attrs: {
      "disabled": !_vm.showArrow
    },
    on: {
      "click": function($event) {
        return _vm.chooseType()
      }
    }
  }, [_vm._v("\n                " + _vm._s(_vm.typeText) + "\n                "), (_vm.showArrow) ? _c('i', {
    class: {
      'active': _vm.showMenu
    }
  }) : _vm._e()])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showMenu),
      expression: "showMenu"
    }],
    staticClass: "budgetDetail_menu"
  }, [(_vm.showIncomePay) ? _c('div', {
    staticClass: "item"
  }, [_c('span', [_vm._v("明细类型")]), _vm._v(" "), _c('ul', _vm._l((_vm.incomePayArray), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.select
      },
      on: {
        "click": function($event) {
          return _vm.checkItem(i, index, i.detailCode)
        }
      }
    }, [_vm._v(_vm._s(i.content))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.showType) ? _c('div', {
    staticClass: "item"
  }, [_c('span', [_vm._v("业务类型")]), _vm._v(" "), _c('ul', _vm._l((_vm.typeArray), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.select
      },
      on: {
        "click": function($event) {
          return _vm.checkType(i, index, i.bussCode)
        }
      }
    }, [_vm._v(_vm._s(i.content))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.showType) ? _c('div', {
    staticClass: "item"
  }, [_c('span', [_vm._v("收入类型")]), _vm._v(" "), _c('ul', _vm._l((_vm.incomeTypeArray), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.select
      },
      on: {
        "click": function($event) {
          return _vm.checkIncomeType(i, index, i.accCode)
        }
      }
    }, [_vm._v(_vm._s(i.content))])
  }), 0)]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "budgetDetail_btn default_button",
    on: {
      "click": function($event) {
        return _vm.incomePay()
      }
    }
  }, [_vm._v("确定")])])]), _vm._v(" "), _c('div', {
    staticClass: "budgetDetail_blank"
  }), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.showMenu) ? _c('div', {
    staticClass: "budgetDetail_mask",
    on: {
      "click": function($event) {
        return _vm.closeMenu()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('scroller', {
    ref: "my_scroller",
    attrs: {
      "noDataText": _vm.defaultText,
      "on-refresh": _vm.refresh,
      "on-infinite": _vm.infinite
    }
  }, [_c('div', {
    staticClass: "budgetDetail_content",
    staticStyle: {
      "overflow": "scroll"
    }
  }, [_c('div', {
    staticClass: "page-infinite-wrapper"
  }, [_c('ul', _vm._l((_vm.getAllList), function(i, index) {
    return _c('li', {
      key: index
    }, [_c('b', [_vm._v(_vm._s(i.desc))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(i.createTime))]), _vm._v(" "), _c('div', {
      staticClass: "amount"
    }, [(i.operateType == 'PLUS') ? _c('b', [_vm._v("+" + _vm._s(_vm._f("keepTwoNum")(i.amount)) + " ")]) : _vm._e(), _vm._v(" "), (i.operateType == 'SUB') ? _c('b', {
      staticClass: "pay"
    }, [_vm._v("\n                                    -" + _vm._s(_vm._f("keepTwoNum")(i.amount)) + "\n                                ")]) : _vm._e(), _vm._v(" "), (i.cash == true) ? _c('i', [_vm._v("包含代扣税额" + _vm._s(i.taxAmount))]) : _vm._e()])])
  }), 0)])])])], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-58372b93", module.exports)
  }
}

/***/ }),

/***/ 2031:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1570);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("5e2f709d", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-58372b93!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./budgetDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-58372b93!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./budgetDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 577:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2031)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1287),
  /* template */
  __webpack_require__(1883),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\budgetDetail\\budgetDetail2.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] budgetDetail2.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-58372b93", Component.options)
  } else {
    hotAPI.reload("data-v-58372b93", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});