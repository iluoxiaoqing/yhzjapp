webpackJsonp([79],{

/***/ 1379:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            goodsPrice: "",
            productCode: "",
            canClick: false
        };
    },
    mounted: function mounted() {
        var _this = this;

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "recommOrder/good",
            "data": {
                "productCode": this.$route.query.productCode
            }
        }, function (data) {
            console.log(data);
            _this.canClick = true;
            _this.productCode = data.data.productCode;
            _this.goodsPrice = data.data.price;
        });
    },

    methods: {
        gotoPay: function gotoPay() {

            if (!this.canClick) {
                return;
            }

            common.youmeng("广告-成为推手", "点击申请按钮");
            this.$router.push({
                path: "upgradeRight_pay",
                query: {
                    "productCode": this.productCode,
                    "goodsPrice": this.goodsPrice
                }
            });
        }
    }
};

/***/ }),

/***/ 1526:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-24e8ae4a] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-24e8ae4a] {\n  background: #fff;\n}\n.tips_success[data-v-24e8ae4a] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-24e8ae4a] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-24e8ae4a] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-24e8ae4a],\n.fade-leave-active[data-v-24e8ae4a] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-24e8ae4a],\n.fade-leave-to[data-v-24e8ae4a] {\n  opacity: 0;\n}\n.default_button[data-v-24e8ae4a] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-24e8ae4a] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-24e8ae4a] {\n  position: relative;\n}\n.loading-tips[data-v-24e8ae4a] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.pusher[data-v-24e8ae4a] {\n  width: 100%;\n  overflow: hidden;\n  padding-bottom: 2rem;\n}\n.pusher .pusher_banner[data-v-24e8ae4a] {\n  width: 100%;\n  overflow: hidden;\n}\n.pusher .pusher_banner img[data-v-24e8ae4a] {\n  width: 100%;\n  display: block;\n}\n.pusher .pusher_content[data-v-24e8ae4a] {\n  width: 7rem;\n  height: 5.24rem;\n  margin: -0.9rem auto 0;\n  background: url(" + __webpack_require__(1646) + ") no-repeat;\n  background-size: contain;\n  position: relative;\n  z-index: 10;\n  padding-top: .74rem;\n}\n.pusher .pusher_content .pusher_title[data-v-24e8ae4a] {\n  width: 100%;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: .3rem;\n}\n.pusher .pusher_content .pusher_title img[data-v-24e8ae4a] {\n  width: .42rem;\n  height: .26rem;\n  display: block;\n}\n.pusher .pusher_content .pusher_title p[data-v-24e8ae4a] {\n  color: #3B3B3D;\n  font-size: .36rem;\n  font-weight: 700;\n  margin: 0 .16rem;\n}\n.pusher .pusher_content .pusher_main[data-v-24e8ae4a] {\n  width: 5.5rem;\n  overflow: hidden;\n  margin: 0 auto;\n  font-size: .32rem;\n  color: #525252;\n}\n.pusher .pusher_content .pusher_main p[data-v-24e8ae4a] {\n  opacity: 0.8;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  white-space: nowrap;\n  margin-bottom: .35rem;\n}\n.pusher .pusher_content .pusher_main p i[data-v-24e8ae4a] {\n  background: #348BFF;\n  width: .12rem;\n  height: .12rem;\n  border-radius: 100%;\n  margin-right: .2rem;\n}\n.pusher .pusher_content .pusher_main p span[data-v-24e8ae4a] {\n  font-size: .28rem;\n  color: #8E8E8E;\n}\n.pusher .pusher_content .pusher_price[data-v-24e8ae4a] {\n  width: 4.06rem;\n  height: .84rem;\n  color: #FFE590;\n  font-size: .44rem;\n  border-radius: .53rem;\n  background: #FB5D5C;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  bottom: .6rem;\n}\n.pusher .banner_button[data-v-24e8ae4a] {\n  width: 100%;\n  height: 1.24rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  z-index: 100;\n  background: #FADF3D;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n     -moz-box-orient: vertical;\n     -moz-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.pusher .banner_button p[data-v-24e8ae4a] {\n  color: #D6353D;\n  font-size: .24rem;\n}\n.pusher .banner_button p b[data-v-24e8ae4a] {\n  font-size: .4rem;\n}\n.pusher .banner_button p i[data-v-24e8ae4a] {\n  opacity: 0.7;\n}\n.pusher .banner_button span[data-v-24e8ae4a] {\n  font-size: .24rem;\n  color: #D6353D;\n  opacity: 0.7;\n  margin-top: -0.05rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1645:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_banner.png?v=e9f749fe";

/***/ }),

/***/ 1646:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_title.png?v=f6e49ba1";

/***/ }),

/***/ 1647:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_you.png?v=3071e036";

/***/ }),

/***/ 1648:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/pusher_zuo.png?v=f4e62bc2";

/***/ }),

/***/ 1840:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "pusher_content"
  }, [_vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "pusher_price"
  }, [_vm._v("\n              礼包价格：" + _vm._s(_vm.goodsPrice) + "元\n          ")])]), _vm._v(" "), _c('div', {
    staticClass: "banner_button",
    on: {
      "click": function($event) {
        return _vm.gotoPay()
      }
    }
  }, [_vm._m(3), _vm._v(" "), _c('span', [_vm._v("申请时间：2019.3.1-2019.6.30.")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_banner"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1645)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_title"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1648),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("购买推手礼包")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1647),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pusher_main"
  }, [_c('p', [_c('i'), _vm._v("成为推手，马上开启赚钱之路")]), _vm._v(" "), _c('p', [_c('i'), _vm._v("获赠一台MPOS"), _c('span', [_vm._v("(自用省钱，推荐赚钱)")])]), _vm._v(" "), _c('p', [_c('i'), _vm._v("90天0.50%+3超低费率")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('b', [_vm._v("立即申请")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-24e8ae4a", module.exports)
  }
}

/***/ }),

/***/ 1987:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1526);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("264c7164", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-24e8ae4a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./pusher.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-24e8ae4a&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./pusher.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 573:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1987)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1379),
  /* template */
  __webpack_require__(1840),
  /* scopeId */
  "data-v-24e8ae4a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\banner\\pusher.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] pusher.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-24e8ae4a", Component.options)
  } else {
    hotAPI.reload("data-v-24e8ae4a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});