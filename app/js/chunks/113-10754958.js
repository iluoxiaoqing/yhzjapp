webpackJsonp([113],{

/***/ 1478:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            videoList: {},
            imgUrl1: _apiConfig2.default.KY_IP,
            active: 0,
            video: [],
            index: ""
        };
    },

    components: {
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
    },
    mounted: function mounted() {
        this.getHeight();
        this.getList();
    },

    methods: {
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = document.getElementById("pull-wrapper");
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + "px";
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "mediaTeach/group",
                "data": {}
            }, function (data) {
                _this.videoList = data.data;
                _this.id = _this.$route.query.id;
                // this.$set(this.videoList[this.id-1],"page","1");
                _this.videoList[_this.id - 1].page = 1;
                _this.index = _this.$route.query.id - 1;
                _this.videoList[_this.id - 1].video = [];
                _this.active = _this.index;
                _this.getVideo(_this.id, _this.index);
            });
        },
        toggle: function toggle(index, id) {
            this.video = [];
            this.active = index;
            this.id = id;
            this.videoList.map(function (el) {
                el.page = 1;
                el.video = [];
            });
            // this.videoList[index].page = 1;
            this.getVideo(this.id, index);
        },
        loadmore: function loadmore() {
            console.log("fresh", this.id);
            this.getVideo(this.id);
        },
        refresh: function refresh() {
            this.$set(this.videoList[this.index], "page", 1);
            // this.$set(this.video,"video",[]);
            // console.log("fresh",this.id)
            this.video = [];
            this.getVideo(this.id);
        },
        getVideo: function getVideo(id, active) {
            var _this2 = this;

            console.log("2222", this.videoList[this.index].page);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "mediaTeach/videoList",
                "data": {
                    page: this.videoList[this.index].page,
                    groupId: id
                }
            }, function (data) {
                // console.log(data.data);
                // this.video = data.data;
                var curPage = _this2.videoList[_this2.index].page;
                if (data.data.length > 0) {
                    data.data.map(function (el) {
                        _this2.$set(el, "show", false);
                        _this2.video.push(el);
                    });
                    curPage++;
                    _this2.$set(_this2.videoList[_this2.index], "page", curPage);
                    _this2.$set(_this2.videoList[_this2.index], "isNoData", false);
                } else {
                    if (curPage == 1) {
                        _this2.video = data.data;
                        // this.$set(this.menuList[this.index],"isNoData",true);
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        });
                    }
                }
            });
        },
        getImg: function getImg(url) {
            if (url.substr(0, 7).toLowerCase() == "http://" || url.substr(0, 8).toLowerCase() == "https://") {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        }
    }
};

/***/ }),

/***/ 1609:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-aec27cce] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-aec27cce] {\n  background: #fff;\n}\n.tips_success[data-v-aec27cce] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-aec27cce] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-aec27cce] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-aec27cce],\n.fade-leave-active[data-v-aec27cce] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-aec27cce],\n.fade-leave-to[data-v-aec27cce] {\n  opacity: 0;\n}\n.default_button[data-v-aec27cce] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-aec27cce] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-aec27cce] {\n  position: relative;\n}\n.loading-tips[data-v-aec27cce] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.NavSlide[data-v-aec27cce] {\n  width: 100%;\n  overflow: hidden;\n}\n.NavSlide nav[data-v-aec27cce] {\n  padding: 0.14rem 0.3rem;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: flex;\n  -webkit-box-align: middle;\n  -ms-flex-align: middle;\n  -webkit-align-items: middle;\n     -moz-box-align: middle;\n          align-items: middle;\n  overflow: auto;\n}\n.NavSlide nav .main[data-v-aec27cce] {\n  height: 0.96rem;\n  background: #F4F5FB;\n  border-radius: 0.02rem;\n  font-size: 0.28rem;\n  -ms-flex-negative: 0;\n  -webkit-flex-shrink: 0;\n          flex-shrink: 0;\n  padding: 0.12rem 0.12rem 0;\n  margin-right: 0.16rem;\n  color: #3D4A5B;\n}\n.NavSlide nav .main i[data-v-aec27cce] {\n  opacity: 0.6;\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  line-height: 0.34rem;\n}\n.NavSlide nav .active[data-v-aec27cce] {\n  background: #313757 !important;\n  color: #ffffff !important;\n}\n.NavSlide nav .active i[data-v-aec27cce] {\n  color: #ffffff !important;\n}\n.NavSlide .classType[data-v-aec27cce] {\n  width: 92%;\n  margin: 0.48rem 4% 0;\n}\n.NavSlide .classType .videoList[data-v-aec27cce] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-flex-wrap: wrap;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  bottom: 0.5rem;\n}\n.NavSlide .classType .videoList .item[data-v-aec27cce] {\n  width: 3.36rem;\n  position: relative;\n  margin-bottom: 0.2rem;\n}\n.NavSlide .classType .videoList .item.height0[data-v-aec27cce] {\n  height: 0px;\n  visibility: hidden;\n}\n.NavSlide .classType .videoList .item img[data-v-aec27cce] {\n  width: 100%;\n  height: 1.96rem;\n}\n.NavSlide .classType .videoList .item .title[data-v-aec27cce] {\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n.NavSlide .classType .videoList .item .content[data-v-aec27cce] {\n  opacity: 0.6;\n  font-size: 0.24rem;\n  color: #3D4A5B;\n  overflow: hidden;\n  -webkit-line-clamp: 2;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n}\n", ""]);

// exports


/***/ }),

/***/ 1922:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "NavSlide"
  }, [_c('nav', _vm._l((_vm.videoList), function(item, index) {
    return _c('div', {
      staticClass: "main",
      class: {
        active: index == _vm.active
      },
      on: {
        "click": function($event) {
          return _vm.toggle(index, item.id)
        }
      }
    }, [_vm._v("\n                " + _vm._s(item.name)), _c('br'), _vm._v(" "), _c('i', [_vm._v(_vm._s(item.desc))])])
  }), 0), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [_c('div', {
    staticClass: "classType"
  }, [_c('div', {
    staticClass: "videoList"
  }, _vm._l((_vm.video), function(j, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('a', {
      attrs: {
        "href": j.videoUrl
      }
    }, [_c('img', {
      attrs: {
        "src": _vm.getImg(j.pagePic)
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "title"
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.title) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
      staticClass: "content"
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(j.desc) + "\n\t\t\t\t\t\t")])])])
  }), 0)])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-aec27cce", module.exports)
  }
}

/***/ }),

/***/ 2070:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1609);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("146c5257", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aec27cce&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aec27cce&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./video_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 689:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2070)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1478),
  /* template */
  __webpack_require__(1922),
  /* scopeId */
  "data-v-aec27cce",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\video\\video_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] video_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-aec27cce", Component.options)
  } else {
    hotAPI.reload("data-v-aec27cce", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});