webpackJsonp([129],{

/***/ 1377:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _methods;

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _noData = __webpack_require__(71);

var _noData2 = _interopRequireDefault(_noData);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = {
    data: function data() {
        return {
            currentGroup: "1",
            currentGroupWd: "1",
            productName: this.$route.query.productName, //客户端放到cookie中
            productCode: this.$route.query.productCode, //客户端放到cookie中
            type: "PERSONAL",
            timeType: "DAY",
            page: "1",
            detailArr: [],
            // detailArr: [{
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }, {
            //     time: "2020年4月",
            //     totalSubTrans: "1987.00",
            //     totalNum: "13",
            //     kuaijieTrans: "11.00",
            //     saomaTrans: "12.00",
            //     shuakaTrans: "13.00",
            //     yuanshanfuTrans: "14.00"
            // }],
            isMore: "init", //默认是init 加载完毕没有数据为 'nodata'
            isLoading: false,
            total: "",
            currentPage: 1,
            isNoData: false
        };
    },

    components: {
        // scrollView,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noData2.default
        // rebateList,
    },
    mounted: function mounted() {
        // TODO
        if (this.$route.query.from == 'push') {
            document.title = "推手数据详情-" + this.$route.query.productName;
        } else {
            document.title = "交易额统计-" + this.$route.query.productName;
        }

        var bodyHeight = document.documentElement.clientHeight;
        var scroller = document.getElementById("pull-wrapper");
        var scrollerTop = scroller.getBoundingClientRect().top;
        scroller.style.height = bodyHeight - scrollerTop + "px";
        this.getList();
    },

    methods: (_methods = {
        infinite: function infinite() {
            this.getList();
        },
        refresh: function refresh() {
            this.currentPage = 1;
            this.detailArr = [];
            this.getList();
        },
        changeTab: function changeTab(idx) {
            if (idx == this.currentGroup) {
                return;
            }
            this.currentPage = 1;
            this.detailArr = [];
            this.currentGroupWd = 1;
            this.timeType = "DAY";
            this.currentGroup = idx;
            if (this.currentGroup == 1) {
                this.type = 'PERSONAL';
            } else {
                this.type = 'TEAM';
            }
            this.getList();
        },
        changeTabWd: function changeTabWd(id) {
            if (id == this.currentGroupWd) {
                return;
            }
            this.currentPage = 1;
            this.detailArr = [];
            this.currentGroupWd = id;
            if (this.currentGroupWd == 1) {
                this.timeType = 'DAY';
            } else {
                this.timeType = 'MONTH';
            }
            this.getList();
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "achHomepage/transInfo",
                "type": "post",
                "data": {
                    "type": this.type,
                    "productCode": this.$route.query.productCode,
                    "timeType": this.timeType,
                    "page": this.currentPage,
                    "countUserNo": this.$route.query.countUserNo || ''
                }
            }, function (data) {
                console.log('数据列表', data);
                _this.total = data.data.totalTrans;
                if (data.data.subInfo.object.length > 0) {
                    if (_this.currentPage == 1) {
                        _this.detailArr = [];
                    }
                    data.data.subInfo.object.map(function (el) {
                        _this.$set(el, "show", true);
                        _this.detailArr.push(el);
                    });

                    _this.isNoData = false;
                    _this.currentPage++;
                } else {
                    if (_this.currentPage == 1) {
                        _this.isNoData = true;
                    } else {
                        common.toast({
                            content: "没有更多数据啦"
                        });
                    }
                }
            });
        }
    }, _defineProperty(_methods, "refresh", function refresh(loaded) {
        this.currentPage = 1;
        this.detailArr = [];
        this.getList();
    }), _defineProperty(_methods, "loadmore", function loadmore(loaded) {
        this.getList();
    }), _methods)

};

/***/ }),

/***/ 1608:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1921:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "trade_main"
  }, [_c('div', {
    staticClass: "detail-con"
  }, [_c('ul', {
    staticClass: "tab",
    attrs: {
      "id": "tab"
    }
  }, [_c('li', {
    class: {
      'active': _vm.currentGroup == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(1)
      }
    }
  }, [_vm._v("个人交易")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroup == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeTab(2)
      }
    }
  }, [_vm._v("团队交易")])]), _vm._v(" "), _c('div', {
    staticClass: "volume_main"
  }, [_c('div', {
    staticClass: "total"
  }, [_vm._v("累计交易额(元)")]), _vm._v(" "), _c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.total))])]), _vm._v(" "), _c('div', {
    staticClass: "trade_detail"
  }, [_c('ul', {
    staticClass: "tabTwo"
  }, [_c('li', {
    class: {
      'active': _vm.currentGroupWd == 1
    },
    on: {
      "click": function($event) {
        return _vm.changeTabWd(1)
      }
    }
  }, [_vm._v("日维度")]), _vm._v(" "), _c('li', {
    class: {
      'active': _vm.currentGroupWd == 2
    },
    on: {
      "click": function($event) {
        return _vm.changeTabWd(2)
      }
    }
  }, [_vm._v("月维度")])]), _vm._v(" "), _c('freshToLoadmore', {
    attrs: {
      "type": 'all'
    },
    on: {
      "refresh": _vm.refresh,
      "loadmore": _vm.loadmore
    }
  }, [(_vm.detailArr.length > 0) ? _c('div', {
    staticClass: "detail-list"
  }, _vm._l((_vm.detailArr), function(item, j) {
    return _c('div', {
      staticClass: "item"
    }, [
      [_c('div', [_c('div', {
        staticClass: "time"
      }, [_c('span', [_vm._v(_vm._s(item.time))])]), _vm._v(" "), _c('ul', [_c('li', [_c('p', [_vm._v("全部交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.totalSubTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("全部笔数")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.totalNum))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("刷卡借记卡")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.shuaKaDebitTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("刷卡贷记卡")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.shuaKaCreditTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("扫码交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.saomaTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("云闪付交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.yunshanfuTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("快捷交易额")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.kuaijieTrans))])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("其他")]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.otherTrans))])])])])]
    ], 2)
  }), 0) : _vm._e()])], 1), _vm._v(" "), _c('no-data', {
    attrs: {
      "isNoData": _vm.isNoData
    }
  })], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-aba55904", module.exports)
  }
}

/***/ }),

/***/ 2069:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1608);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("37b5d7ee", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aba55904&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./tradeVolume.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-aba55904&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./tradeVolume.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 571:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2069)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1377),
  /* template */
  __webpack_require__(1921),
  /* scopeId */
  "data-v-aba55904",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\tradeVolume.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] tradeVolume.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-aba55904", Component.options)
  } else {
    hotAPI.reload("data-v-aba55904", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});