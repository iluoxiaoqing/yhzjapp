webpackJsonp([46],{

/***/ 1242:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1247)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(959),
  /* template */
  __webpack_require__(1245),
  /* scopeId */
  "data-v-5140d656",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\confirmInfo.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] confirmInfo.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5140d656", Component.options)
  } else {
    hotAPI.reload("data-v-5140d656", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 1245:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "confirmInfo"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_mask",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.visible) ? _c('div', {
    staticClass: "confirmInfo_box"
  }, [_c('div', {
    staticClass: "confirmInfo_close",
    on: {
      "click": function($event) {
        return _vm.closeBox()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_text"
  }, [_c('h1', [_vm._v("确认信息")]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("姓名")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.realname))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("身份证号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.identityNo))])]), _vm._v(" "), _c('div', {
    staticClass: "cell"
  }, [_c('span', [_vm._v("手机号")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.userInfo.userName))])]), _vm._v(" "), _c('div', {
    staticClass: "confirmInfo_line"
  }), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.boxInfo.content))]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.submitBox()
      }
    }
  }, [_vm._v("确认办理")])])]) : _vm._e()])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5140d656", module.exports)
  }
}

/***/ }),

/***/ 1247:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(963);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("48f57cb6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5140d656&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./confirmInfo.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1288:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _confirmInfo = __webpack_require__(1242);

var _confirmInfo2 = _interopRequireDefault(_confirmInfo);

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showConfirm: false,
            imgUrl: _apiConfig2.default.KY_IP,
            loanData: {
                custDesc: {}
            },
            comfirmText: {
                "content": "请确认以上信息与贷款申请信息完全一致，填写错误将会导致申请无法通过。"
            }
        };
    },

    components: {
        confirmInfo: _confirmInfo2.default
    },
    mounted: function mounted() {

        window.goBack = this.goBack;

        if (this.$store.state.loanData.logo) {
            this.loanData = this.$store.state.loanData;
            console.log("loanDate", this.loanData);
        } else {
            this.loanData = JSON.parse(sessionStorage.getItem("loanData"));
        }
        console.log("loanDate", this.loanData);
    },

    methods: {
        goBack: function goBack() {
            this.$router.push({
                "path": "applyLoan",
                "query": {
                    buss: "LOAN_CODE"
                }
            });
        },
        comfirmInfo: function comfirmInfo() {
            console.log(this.loanData.productCode);
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "/loan/createLoanOrder",
                "data": {
                    productCode: this.loanData.applyCode
                }
            }, function (data) {
                window.location.href = data.data;
            });
        },
        confirmApply: function confirmApply() {
            common.youmeng("贷款申请", "点击确认申请");
            this.showConfirm = true;
        },
        gotoAgent: function gotoAgent(j) {
            var _this = this;

            common.youmeng("贷款申请", "点击立即申请");

            this.selectObject = j;
            var buss = this.$route.query.buss;
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "user/isShow",
                "data": {
                    "businessCode": LOAN_CODE,
                    "productCode": j.productCode
                }
            }, function (data) {
                if (data.data.isShow) {
                    _this.repeatShow = true;
                    _this.repeatInfo.content = data.data.showMessage;
                } else {
                    _this.showInfoContent(j);
                }
            });
        },
        showInfoContent: function showInfoContent(j) {
            this.$store.dispatch("saveLoanData", j);

            this.$router.push({
                "path": "loanAgent"
            });
        },
        repeatConfirm: function repeatConfirm() {
            this.showInfoContent(this.selectObject);
        }
    }
};

/***/ }),

/***/ 1573:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1886:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loanAgent"
  }, [_c('div', {
    staticClass: "loanAgent_banner"
  }, [_c('div', {
    staticClass: "banner_top"
  }, [_c('p', [_c('b', [_vm._v(_vm._s(_vm.loanData.custDesc.loanAmountInterval))])]), _vm._v(" "), _c('em', [_vm._v("额度范围(元)")])]), _vm._v(" "), _c('div', {
    staticClass: "banner_bottom"
  }, [_c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.custDesc.loanInterest))]), _vm._v(" "), _c('i', [_vm._v("利息")])]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.custDesc.loanRemandLimit))]), _vm._v(" "), _c('i', [_vm._v("贷款期限")])]), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('p', [_c('span', [_vm._v(_vm._s(_vm.loanData.custDesc.loanAuditTime))]), _vm._v(" "), _c('i', [_vm._v("放款时长")])])])]), _vm._v(" "), _c('div', {
    staticClass: "loanAgent_about"
  }, [_c('div', {
    staticClass: "about_title"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.loanData.logo,
      "alt": ""
    }
  }), _vm._v("\n            " + _vm._s(_vm.loanData.productName) + "\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "about_content"
  }, [_c('p', [_vm._m(2), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.custDesc.loanApplyProcess)
    }
  })]), _vm._v(" "), _c('p', [_vm._m(3), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.custDesc.loanApplyClaim)
    }
  })]), _vm._v(" "), _c('p', [_vm._m(4), _vm._v(" "), _c('span', {
    domProps: {
      "innerHTML": _vm._s(_vm.loanData.custDesc.loanPrompt)
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "loanAgent_button",
    on: {
      "click": function($event) {
        return _vm.confirmApply()
      }
    }
  }, [_vm._v("确认申请")]), _vm._v(" "), _c('confirm-info', {
    attrs: {
      "visible": _vm.showConfirm,
      "boxInfo": _vm.comfirmText
    },
    on: {
      "update:visible": function($event) {
        _vm.showConfirm = $event
      },
      "submitBox": _vm.comfirmInfo
    }
  })], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('em')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', [_c('em')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i'), _vm._v(" "), _c('small', [_vm._v("申请流程")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i'), _vm._v(" "), _c('small', [_vm._v("申请条件")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('b', [_c('i', {
    staticClass: "yellow"
  }), _vm._v(" "), _c('small', [_vm._v("特别提示")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5dd5e945", module.exports)
  }
}

/***/ }),

/***/ 2034:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1573);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("db4c9ec6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5dd5e945&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanAgent.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5dd5e945&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./loanAgent.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 617:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2034)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1288),
  /* template */
  __webpack_require__(1886),
  /* scopeId */
  "data-v-5dd5e945",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\recommendLoan\\loanAgentOld.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loanAgentOld.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5dd5e945", Component.options)
  } else {
    hotAPI.reload("data-v-5dd5e945", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 959:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            userInfo: {}
        };
    },

    props: {
        visible: {
            type: Boolean,
            default: false
        },
        boxInfo: {
            type: Object,
            default: ""
        }
    },
    watch: {
        visible: function visible(value) {
            var _this = this;

            if (value) {
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "user/userIC"
                }, function (data) {
                    console.log(data);
                    _this.userInfo = data.data;
                });
            }
        }
    },
    mounted: function mounted() {},

    methods: {
        closeBox: function closeBox() {
            this.$emit("update:visible", false);
        },
        submitBox: function submitBox() {
            this.$emit("submitBox");
            this.$emit("update:visible", false);
        }
    }
};

/***/ }),

/***/ 963:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ })

});