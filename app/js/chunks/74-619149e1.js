webpackJsonp([74],{

/***/ 1429:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _common2 = __webpack_require__(7);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            emptyConDia: false,
            conDia: false,
            loginKey: common.getLoginKey(),
            rewardList: [],
            bagList: []
        };
    },

    props: {},
    components: {},
    mounted: function mounted() {
        var _this = this;
        _this.rewardArray();
    },

    methods: {
        rewardArray: function rewardArray() {
            var _this2 = this;

            var _this = this;
            _this.rewardList = [];
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "dawnPlan/taskList",
                "type": "post",
                "data": {
                    loginKey: common.getLoginKey()
                }
            }, function (data) {
                var bagList = data.data.taskList;
                for (var i in bagList) {
                    bagList[i].changeJian = false;
                    bagList[i].rewardShow = true;
                }
                if (data.data.dawnPlanStatus == 'LIMING_NOJOIN') {
                    _this.emptyConDia = true;
                }
                if (data.data.dawnPlanStatus == 'LIMING_JOIN_FINISH') {
                    _this.conDia = true;
                }
                _this2.rewardList = bagList;
            });
        },
        clickUnfold: function clickUnfold(item, index) {
            var _this = this;
            if (_this.rewardList[index].changeJian == true) {
                _this.rewardList[index].changeJian = false;
                _this.rewardList[index].rewardShow = true;
                return;
            }
            for (var _item in _this.rewardList) {
                _this.rewardList[_item].changeJian = false;
                _this.rewardList[_item].rewardShow = true;
            }
            _this.rewardList[index].changeJian = true;
            _this.rewardList[index].rewardShow = false;
        },
        clickGetReward: function clickGetReward(item, index, type) {
            var _this = this;
            var dawnPlan = _this.rewardList[index].dawnPlan;
            if (type == 'PEND') {
                common.Ajax({
                    "url": _apiConfig2.default.KY_IP + "dawnPlan/receive",
                    "type": "post",
                    "data": {
                        loginKey: common.getLoginKey(),
                        dawnPlan: dawnPlan
                    }
                }, function (data) {
                    _this.$nextTick(function () {
                        _this.rewardArray();
                    });
                });
            }
        },

        //返回首页
        goBackPage: function goBackPage() {
            window.history.back();
        },

        //关闭
        confirmDia: function confirmDia() {
            var _this = this;
            _this.emptyConDia = false;
            _this.conDia = false;
            // this.$router.go(-1)
            // window.history.go(-1);
        }
    }
};

/***/ }),

/***/ 1522:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\reward\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1696:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/liming@2x.png?v=5cf6d374";

/***/ }),

/***/ 1836:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward"
  }, [_vm._m(0), _vm._v(" "), _vm._l((_vm.rewardList), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "reward-bottom"
    }, [(item.rewardShow) ? _c('div', {
      staticClass: "reward-bottom-list"
    }, [_c('div', {
      staticClass: "rewardLeft"
    }, [_vm._v(_vm._s(item.selfRewardAmount) + "元")]), _vm._v(" "), _c('div', {
      class: item.standardStatus == 'SATISFY' ? 'rewardImg ' : ''
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight"
    }, [_c('div', {
      staticClass: "rewardRight-title"
    }, [_vm._v(_vm._s(item.taskTitle) + "\n                        "), _c('span', {
      class: item.changeJian ? 'rewardRightCow' : 'rewardRightRow',
      on: {
        "click": function($event) {
          return _vm.clickUnfold(item, index)
        }
      }
    })]), _vm._v(" "), _c('div', {
      class: item.sendStatus == 'NO_RECEIVE' ? 'rewardRight-hide' : item.sendStatus == 'PEND' ? 'rewardRight-show' : 'rewardRight-hide',
      on: {
        "click": function($event) {
          $event.stopPropagation();
          return _vm.clickGetReward(item, index, item.sendStatus)
        }
      }
    }, [_vm._v(_vm._s(item.sendStatus == 'NO_RECEIVE' ? '领取' : item.sendStatus == 'PEND' ? '领取' : '已领取'))])])]) : _vm._e(), _vm._v(" "), (!item.rewardShow) ? _c('div', {
      staticClass: "reward-bottom-listShow"
    }, [_c('div', {
      staticClass: "rewardLeft"
    }, [_vm._v(_vm._s(item.selfRewardAmount) + "元")]), _vm._v(" "), _c('div', {
      class: item.standardStatus == 'SATISFY' ? 'rewardImg ' : ''
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight"
    }, [_c('div', {
      staticClass: "rewardRight-title"
    }, [_vm._v(_vm._s(item.taskTitle) + "\n                        "), _c('span', {
      class: item.changeJian ? 'rewardRightCow' : 'rewardRightRow',
      on: {
        "click": function($event) {
          return _vm.clickUnfold(item, index)
        }
      }
    }), _vm._v(" "), _c('div', {
      staticClass: "rewardRight-unFoldTitle"
    }, [_vm._v(_vm._s(item.taskContent))])]), _vm._v(" "), _c('div', {
      class: item.sendStatus == 'NO_RECEIVE' ? 'rewardRight-hide' : item.sendStatus == 'PEND' ? 'rewardRight-show' : 'rewardRight-hide',
      on: {
        "click": function($event) {
          $event.stopPropagation();
          return _vm.clickGetReward(item, index, item.sendStatus)
        }
      }
    }, [_vm._v(_vm._s(item.sendStatus == 'NO_RECEIVE' ? '领取' : item.sendStatus == 'PEND' ? '领取' : '已领取'))])])]) : _vm._e()])
  }), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.emptyConDia) ? _c('div', {
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      },
      "mousewheel": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "backImg"
  }), _vm._v(" "), _c('div', {
    staticClass: "conDia"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n                        亲爱的推手，您不符合活动参与条件，请关注推手平台其他活动，谢谢。\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.confirmDia(1)
      }
    }
  }, [_vm._v("关闭")])])])]) : _vm._e()]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.conDia) ? _c('div', {
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      },
      "mousewheel": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "backImg"
  }), _vm._v(" "), _c('div', {
    staticClass: "conDia"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("\n                        亲爱的推手，恭喜您，您已完成全部任务，可继续参与推手平台其他任务，谢谢。\n                    ")]), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "confirm",
    on: {
      "click": function($event) {
        return _vm.confirmDia(1)
      }
    }
  }, [_vm._v("关闭")])])])]) : _vm._e()])], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward-bg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1696)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "reward-bg-title"
  }, [_vm._v("奖励只可从上到下依次领取")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "reward-letter"
  }, [_c('div', {
    staticClass: "reward-letter-text"
  }, [_vm._v("规则说明")]), _vm._v(" "), _c('div', [_vm._v("活动面向人群：")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-log"
  }, [_vm._v("本活动仅对2021年9月1日后注册逍遥推手的会员以及2021年9月1日前注册但无机具采购、无上级划拨机具、无团队、无任何激活、无任何增值业务的会员有效。")]), _vm._v(" "), _c('div', [_vm._v("活动参与方式：")]), _vm._v(" "), _c('div', [_vm._v("1、参与本活动的会员需要根据逍遥推手APP内发布的任务1-任务6的任务指引按照顺序逐个完成每项任务，并领取该项任务的奖励，")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-margin"
  }, [_vm._v(" 2、任务7、任务8、任务9三项若在一个月内完成，每个月只能按顺序领取一项任务奖励；")]), _vm._v(" "), _c('div', [_vm._v("活动时间：")]), _vm._v(" "), _c('div', [_vm._v("1、本活动任务截止日期为2022年8月31日，结束后将发布新的活动任务;")]), _vm._v(" "), _c('div', {
    staticClass: "reward-letter-margin"
  }, [_vm._v("2、本活动最终解释权归逍遥推手所有。")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-21866e8d", module.exports)
  }
}

/***/ }),

/***/ 1983:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1522);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("727ae1fe", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21866e8d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewarList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21866e8d&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewarList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 632:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1983)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1429),
  /* template */
  __webpack_require__(1836),
  /* scopeId */
  "data-v-21866e8d",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\reward\\rewardList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-21866e8d", Component.options)
  } else {
    hotAPI.reload("data-v-21866e8d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});