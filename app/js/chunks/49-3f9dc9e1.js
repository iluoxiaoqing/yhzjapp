webpackJsonp([49],{

/***/ 1323:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_android.png?v=21a8b9f1";

/***/ }),

/***/ 1350:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_iphone.png?v=f726c911";

/***/ }),

/***/ 1479:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            showDia: false,
            showDia1: false,
            showDia2: false,
            index: 0,
            downUrl: _apiConfig2.default.KY_IP,
            showLoading: false,
            androidUrl: "",
            iosUrl: "",
            ios: false,
            android: false,
            isios: false,
            isAndioid: false,
            isApi: false,
            isOpt: false
        };
    },
    mounted: function mounted() {
        this.getUrl();
        document.getElementById("container").style.minHeight = window.screen.height + "px";
        common.youmeng("下载", "进入下载页面");
    },

    methods: {
        isWeixin: function isWeixin() {

            var ua = window.navigator.userAgent.toLowerCase();

            if (common.isClient() == "ios") {
                this.isios = true;
                this.isAndioid = false;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                    this.showDia2 = false;
                    this.showDia1 = true;
                    return true;
                } else {
                    this.showDia2 = true;
                    this.showDia1 = false;
                    return false;
                }
            } else {
                console.log(this.androidUrl);
                this.isios = false;
                this.isAndioid = true;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                    this.showDia2 = false;
                    this.showDia1 = true;
                    return true;
                } else {
                    this.showDia2 = true;
                    this.showDia1 = false;
                    return false;
                }
            }
        },
        show: function show() {
            this.showDia = true;
        },
        iosClick: function iosClick() {
            common.youmeng("ios-下载", "按钮点击");
        },
        androidClick: function androidClick() {
            common.youmeng("Android-下载", "按钮点击");
        },
        getUrl: function getUrl() {
            var _this = this;

            common.Ajax({
                type: "get",
                url: _apiConfig2.default.KY_IP + "appVersionInfo/downApp",
                data: {
                    appCode: "HM_PARTNER"
                },
                showLoading: false
            }, function (data) {
                console.log(data.data);
                for (var i = 0; i < data.data.length; i++) {
                    if (data.data[i].operSys == "IOS") {
                        _this.iosUrl = data.data[i].appDownloadUrl;
                        console.log("iosurl:", _this.iosUrl);
                    } else {
                        _this.androidUrl = data.data[i].appDownloadUrl;
                        console.log("androidUrl:", _this.androidUrl);
                        if (_this.androidUrl.substr(0, 4).toLowerCase() == "http") {
                            _this.androidUrl = _this.androidUrl;
                            console.log("androidUrl", _this.androidUrl);
                            _this.isOpt = true;
                        } else {
                            _this.androidUrl = "app";
                            _this.isApi = true;
                            // this.androidUrl = this.androidUrl;
                        }
                    }
                }
                // this.androidUrl = data.data[1].appDownloadUrl;
                _this.isWeixin();
            });
        }
    }
};

/***/ }),

/***/ 1601:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\wxDown\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1773:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/img_zhishi.png?v=06a58886";

/***/ }),

/***/ 1774:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1914:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    attrs: {
      "id": "container"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1773),
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "wx_main"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia1),
      expression: "showDia1"
    }],
    staticClass: "wx"
  }, [_c('button', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isAndioid),
      expression: "isAndioid"
    }],
    staticClass: "andiro_down",
    on: {
      "click": function($event) {
        return _vm.show();
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1323),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])]), _vm._v(" "), _c('button', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isios),
      expression: "isios"
    }],
    staticClass: "ios_down",
    on: {
      "click": function($event) {
        return _vm.show();
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1350),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("IOS版下载")])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia2),
      expression: "showDia2"
    }],
    staticClass: "sefire"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isAndioid),
      expression: "isAndioid"
    }]
  }, [_c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isApi),
      expression: "isApi"
    }],
    attrs: {
      "href": _vm.downUrl + 'appVersionInfo/h5DownHhm'
    },
    on: {
      "click": function($event) {
        return _vm.androidClick()
      }
    }
  }, [_vm._m(1)]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isOpt),
      expression: "isOpt"
    }],
    attrs: {
      "href": _vm.androidUrl
    },
    on: {
      "click": function($event) {
        return _vm.androidClick()
      }
    }
  }, [_vm._m(2)])]), _vm._v(" "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isios),
      expression: "isios"
    }],
    attrs: {
      "href": _vm.iosUrl
    },
    on: {
      "click": function($event) {
        return _vm.iosClick()
      }
    }
  }, [_vm._m(3)])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "app_logo"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1774),
      "alt": ""
    }
  }), _c('br'), _vm._v(" "), _c('p', [_vm._v("逍遥推手")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "andiro_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1323),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "andiro_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1323),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("Android版下载")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('button', {
    staticClass: "ios_down"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1350),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("IOS版下载")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-8af89c62", module.exports)
  }
}

/***/ }),

/***/ 2062:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1601);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("4d29ca65", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8af89c62&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wxDown.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-8af89c62&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wxDown.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 690:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2062)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1479),
  /* template */
  __webpack_require__(1914),
  /* scopeId */
  "data-v-8af89c62",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\wxDown\\wxDown.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] wxDown.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8af89c62", Component.options)
  } else {
    hotAPI.reload("data-v-8af89c62", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});