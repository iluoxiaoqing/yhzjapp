webpackJsonp([102],{

/***/ 1471:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            activityTime: '',
            activityIntroduce: '',
            activityRule: ''
        };
    },
    mounted: function mounted() {
        this.getDetail();
    },

    methods: {
        getDetail: function getDetail() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "jl/showPopMessage",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                _this.activityTime = data.data.activityTime;
                _this.activityIntroduce = data.data.activityIntroduce;
                _this.activityRule = data.data.activityRule;
            });
        },
        goSearch: function goSearch() {
            this.$router.push({
                "path": "jlSearch"
            });
        },
        go: function go() {
            var address = {};
            var purchaseJson = {};

            // currentNum: 6
            // goodsName: "【嘉联】万元工资礼包"
            // goodsType: "app_jialian_20200115_5"
            // imgPath: "/opt/haomai/posPurchase/icon-jialian2.png"
            // left: 341.5
            // price: 495
            // productCode: "APP_JIALIAN_MPOS"
            // remark: "{"
            // commissionDesc ":" (除获取奖励外， 还可以享受万14的分润)
            // ","
            // company ":"
            // 嘉联支付有限公司 ","
            // customerFeeDesc ":"
            // 0.60 % +3 ","
            // feature ":["
            // 资金秒到 ","
            // 安全稳定 "],"
            // goodsDesc ":"
            // 5 台采购， 激活一台送一台， 万元工资等你来拿 ","
            // salesEndTime ":"
            // 2020.9 .1 ","
            // salesStartTime ":"
            // 2020.1 .15 "}"
            // top: 253.5


            // currentNum: 1,
            //     goodsName: "【嘉联】开店礼包",
            //     goodsType: "app_jialian_20200115_30",
            //     imgPath: "/opt/haomai/posPurchase/img_shop_youshua.png",
            //     left: 341.5,
            //     price: 2970,
            //     productCode: "APP_JIALIAN_MPOS",
            //     remark: '{ "commissionDesc": "(除获取奖励外，还可以享受万14的分润)", "company": "卡友支付服务有限公司", "customerFeeDesc": "0.60%+3", "feature": ["资金秒到", "安全稳定"], "goodsDesc": "内含5台友刷开店宝，参加激活返现和达标返现活动，执行机具服务费规则", "salesEndTime": "2020.1.1", "salesStartTime": "2019.3.1" }',
            //     top: 348.03125


            purchaseJson.carList = [{
                currentNum: 1,
                goodsName: "【嘉联】万元工资礼包",
                goodsType: "app_jialian_20200115_30",
                imgPath: "/opt/haomai/posPurchase/icon-jialian2.png",
                price: 2970,
                productCode: "APP_JIALIAN_MPOS",
                remark: '{"commissionDesc":"(除获取奖励外，还可以享受万14的分润)","company":"嘉联支付服务有限公司","customerFeeDesc":"0.50%+3","feature":["无限循环赠机"],"goodsDesc":"内含30台，激活返现100元/1台，无限循环赠机，万元工资等你来拿","salesEndTime":"2020.9.1","salesStartTime":"2020.1.15"}',
                left: 341.5,
                top: 253.5

                // currentNum: 6,
                // goodsName: "【嘉联】万元工资礼包",
                // goodsType: "app_jialian_20200115_5",
                // imgPath: "/opt/haomai/posPurchase/icon-jialian2.png",
                // left: 341.5,
                // price: 495,
                // productCode: "APP_JIALIAN_MPOS",
                // remark: '{"commissionDesc":" (除获取奖励外， 还可以享受万14的分润)","company":"嘉联支付有限公司 ","customerFeeDesc":"0.60 % +3 ","feature":["资金秒到 ","安全稳定 "],"goodsDesc":"5 台采购， 激活一台送一台， 万元工资等你来拿 ","salesEndTime":"2020.9 .1 ","salesStartTime":" 2020.1 .15 "}',
                // top: 253.5
                // currentNum: 1,
                // goodsName: "【嘉联】开店礼包",
                // goodsType: "app_jialian_20200115_30",
                // imgPath: "/opt/haomai/posPurchase/img_shop_youshua.png",
                // left: 341.5,
                // price: 2970,
                // productCode: "APP_JIALIAN_MPOS",
                // remark: '{ "commissionDesc": "(除获取奖励外，还可以享受万14的分润)", "company": "卡友支付服务有限公司", "customerFeeDesc": "0.60%+3", "feature": ["资金秒到", "安全稳定"], "goodsDesc": "内含5台友刷开店宝，参加激活返现和达标返现活动，执行机具服务费规则", "salesEndTime": "2020.1.1", "salesStartTime": "2019.3.1" }',
                // top: 348.03125
            }];
            console.log("this.carList", purchaseJson.carList);
            purchaseJson.totalAmount = '2970';

            common.youmeng("我要备货", "点击提交订单按钮");

            this.$store.dispatch('saveCarList', purchaseJson);
            // this.$store.dispatch('address', address);
            console.log("purchaseJson", purchaseJson);
            this.$router.push({
                "path": "mall_order"
            });
        }
    }
};

/***/ }),

/***/ 1624:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-dd8b8552] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-dd8b8552] {\n  background: #fff;\n}\n.tips_success[data-v-dd8b8552] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-dd8b8552] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-dd8b8552] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-dd8b8552],\n.fade-leave-active[data-v-dd8b8552] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-dd8b8552],\n.fade-leave-to[data-v-dd8b8552] {\n  opacity: 0;\n}\n.default_button[data-v-dd8b8552] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-dd8b8552] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-dd8b8552] {\n  position: relative;\n}\n.loading-tips[data-v-dd8b8552] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.wy_main[data-v-dd8b8552] {\n  width: 100%;\n  background-image: -webkit-linear-gradient(top, #316FE1 2%, #184BAA 10%);\n  background-image: linear-gradient(180deg, #316FE1 2%, #184BAA 10%);\n}\n.wy_main .wy_topone[data-v-dd8b8552] {\n  width: 100%;\n  height: 7.7rem;\n}\n.wy_main .wy_topone img[data-v-dd8b8552] {\n  width: 100%;\n}\n.wy_main .wy_bottom[data-v-dd8b8552] {\n  width: 100%;\n}\n.wy_main .wy_bottom .wy_toptwo[data-v-dd8b8552] {\n  width: 92%;\n  margin: 0 4%;\n}\n.wy_main .wy_bottom .wy_toptwo .two_one[data-v-dd8b8552] {\n  width: 100%;\n  background: #FF9D58;\n  border-radius: 0.14rem;\n  height: 1.38rem;\n}\n.wy_main .wy_bottom .wy_toptwo .two_one .bottomBar_line[data-v-dd8b8552] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.2rem;\n  font-size: 0.28rem;\n  color: #ffffff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 0.08rem;\n}\n.wy_main .wy_bottom .wy_toptwo .two_one .bottomBar_line em[data-v-dd8b8552] {\n  margin: 0 0.2rem;\n}\n.wy_main .wy_bottom .wy_toptwo .two_one .bottomBar_line span[data-v-dd8b8552] {\n  width: 1.0rem;\n  height: 1px;\n  display: block;\n  background-image: -webkit-linear-gradient(top, #FF9D58 2%, #ffffff 10%);\n  background-image: linear-gradient(180deg, #FF9D58 2%, #ffffff 10%);\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.wy_main .wy_bottom .wy_toptwo .two_one p[data-v-dd8b8552] {\n  font-size: 0.36rem;\n  color: #FFFFFF;\n  font-weight: bold;\n  text-align: center;\n}\n.wy_main .wy_bottom .wy_topthree[data-v-dd8b8552] {\n  width: 92%;\n  margin: 0 4%;\n}\n.wy_main .wy_bottom .wy_topthree .bottomBar_line_blue[data-v-dd8b8552] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.4rem;\n  font-size: 0.28rem;\n  color: #ffffff;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 0.36rem;\n}\n.wy_main .wy_bottom .wy_topthree .bottomBar_line_blue em[data-v-dd8b8552] {\n  margin: 0 0.2rem;\n}\n.wy_main .wy_bottom .wy_topthree .bottomBar_line_blue span[data-v-dd8b8552] {\n  width: 0.6rem;\n  height: 1px;\n  display: block;\n  background-image: -webkit-linear-gradient(top, #ffffff 2%, #ffffff 10%);\n  background-image: linear-gradient(180deg, #ffffff 2%, #ffffff 10%);\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.wy_main .wy_bottom .wy_topthree .detail_main[data-v-dd8b8552] {\n  width: 100%;\n  background: #ffffff;\n  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.07);\n  border-radius: 0.14rem;\n}\n.wy_main .wy_bottom .wy_topthree .detail_main .text_main[data-v-dd8b8552] {\n  width: 92%;\n  margin: 0.28rem 4%;\n  background: #E6EFFF;\n  border-radius: 0.16rem;\n  display: inline-block;\n  padding: 0.14rem 0;\n}\n.wy_main .wy_bottom .wy_topthree .detail_main .text_main p[data-v-dd8b8552] {\n  font-size: 0.28rem;\n  color: #00389A;\n  margin: 0 0.16rem;\n  line-height: 0.53rem;\n}\n.wy_main .wy_button[data-v-dd8b8552] {\n  width: 100%;\n  height: 1rem;\n  margin-top: 0.4rem;\n}\n.wy_main .wy_button .serach[data-v-dd8b8552] {\n  width: 50%;\n  float: left;\n  background: #FFFFFF;\n  line-height: 1rem;\n  font-size: 18px;\n  font-weight: bold;\n  color: #A30035;\n}\n.wy_main .wy_button .go[data-v-dd8b8552] {\n  width: 50%;\n  float: right;\n  background-image: -webkit-linear-gradient(top, #FFE34A 0%, #FFA429 100%);\n  background-image: linear-gradient(180deg, #FFE34A 0%, #FFA429 100%);\n  line-height: 1rem;\n  font-size: 18px;\n  font-weight: bold;\n  color: #A30035;\n}\n", ""]);

// exports


/***/ }),

/***/ 1751:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/tu-1.png?v=6d715ae0";

/***/ }),

/***/ 1937:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wy_main"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "wy_bottom"
  }, [_c('div', {
    staticClass: "wy_toptwo"
  }, [_c('div', {
    staticClass: "two_one"
  }, [_vm._m(1), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.activityTime))])])]), _vm._v(" "), _c('div', {
    staticClass: "wy_topthree"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('div', {
    staticClass: "text_main"
  }, [(_vm.activityIntroduce[0]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[0]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[1]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[1]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[2]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[2]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[3]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[3]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[4]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[4]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[5]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[5]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[6]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[6]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[7]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[7]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[8]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[8]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[9]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[9]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[10]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[10]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[11]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[11]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[12]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[12]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[13]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[13]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[14]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[14]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[15]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[15]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[16]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[16]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[17]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[17]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[18]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[18]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[19]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[19]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[20]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[20]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[21]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[21]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[22]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[22]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[23]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[23]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[24]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[24]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[25]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[25]))]) : _vm._e(), _vm._v(" "), (_vm.activityIntroduce[26]) ? _c('p', [_vm._v(_vm._s(_vm.activityIntroduce[26]))]) : _vm._e()])]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('div', {
    staticClass: "text_main"
  }, [(_vm.activityRule[0]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[0]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[1]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[1]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[2]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[2]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[3]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[3]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[4]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[4]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[5]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[5]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[6]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[6]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[7]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[7]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[8]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[8]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[9]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[9]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[10]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[10]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[11]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[11]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[12]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[12]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[13]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[13]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[14]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[14]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[15]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[15]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[16]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[16]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[17]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[17]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[18]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[18]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[19]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[19]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[20]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[20]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[21]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[21]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[22]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[22]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[23]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[23]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[24]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[24]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[25]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[25]))]) : _vm._e(), _vm._v(" "), (_vm.activityRule[26]) ? _c('p', [_vm._v(_vm._s(_vm.activityRule[26]))]) : _vm._e()])])])]), _vm._v(" "), _c('div', {
    staticClass: "wy_button"
  }, [_c('button', {
    staticClass: "serach",
    on: {
      "click": function($event) {
        return _vm.goSearch()
      }
    }
  }, [_vm._v("奖励查询")]), _vm._v(" "), _c('button', {
    staticClass: "go",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }, [_vm._v("立即领取")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wy_topone"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1751),
      "alt": ""
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("活动时间")]), _vm._v(" "), _c('span')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "bottomBar_line_blue"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("活动产品介绍")]), _vm._v(" "), _c('span')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "bottomBar_line_blue"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("活动规则")]), _vm._v(" "), _c('span')])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-dd8b8552", module.exports)
  }
}

/***/ }),

/***/ 2085:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1624);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("28c855bd", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-dd8b8552&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wygzPlan.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-dd8b8552&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./wygzPlan.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 678:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2085)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1471),
  /* template */
  __webpack_require__(1937),
  /* scopeId */
  "data-v-dd8b8552",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\wygzPlan.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] wygzPlan.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dd8b8552", Component.options)
  } else {
    hotAPI.reload("data-v-dd8b8552", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});