webpackJsonp([75],{

/***/ 1426:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            // currentGroup: 1,
            allRankList: [{
                active: true,
                rankList: []
            }, {
                active: false,
                rankList: []
            }],
            gatherList: [],
            rankList: [],
            userName0: "",
            userName1: "",
            userName2: "",
            userName3: "",
            userName4: "",
            amount0: "",
            amount1: "",
            amount2: "",
            amount3: "",
            amount4: "",
            userLogo0: "",
            userLogo1: "",
            userLogo2: "",
            userLogo3: "",
            userLogo4: "",
            rewardName: "",
            amountTotal: "",
            custTotal: "",
            activeTotal: "",
            transTotal: "",
            activityCode: ""
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getRewardGather();
        this.commonAjax();
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            followFinger: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                var index = swiper.activeIndex;
                // this.actiove = index;
                _this.allRankList.map(function (el) {
                    el.active = false;
                });
                _this.allRankList[index].active = true;
                _this.index = index;
            }
        });
    },

    methods: {
        commonAjax: function commonAjax() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rank",
                "data": {}
            }, function (data) {
                _this2.rankList = data.data;
                _this2.userName0 = data.data[0] ? data.data[0].userName : "";
                _this2.userName1 = data.data[1] ? data.data[1].userName : "";
                _this2.userName2 = data.data[2] ? data.data[2].userName : "";
                _this2.userName3 = data.data[3] ? data.data[3].userName : "";
                _this2.userName4 = data.data[4] ? data.data[4].userName : "";
                _this2.amount0 = data.data[0] ? data.data[0].amount : "";
                _this2.amount1 = data.data[1] ? data.data[1].amount : "";
                _this2.amount2 = data.data[2] ? data.data[2].amount : "";
                _this2.amount3 = data.data[3] ? data.data[3].amount : "";
                _this2.amount4 = data.data[4] ? data.data[4].amount : "";
                _this2.userLogo0 = data.data[0] ? data.data[0].userLogo : "";
                _this2.userLogo1 = data.data[1] ? data.data[1].userLogo : "";
                _this2.userLogo2 = data.data[2] ? data.data[2].userLogo : "";
                _this2.userLogo3 = data.data[3] ? data.data[3].userLogo : "";
                _this2.userLogo4 = data.data[4] ? data.data[4].userLogo : "";
            });
        },
        getRewardGather: function getRewardGather() {
            var _this3 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "rewardCenter/rewardGather",
                "data": {}
            }, function (data) {
                console.log(data.data);
                _this3.gatherList = data.data;
                _this3.rewardName = data.data[1].rewardName;
                _this3.amountTotal = data.data[1].amountTotal;
                _this3.custTotal = data.data[1].custTotal;
                _this3.activeTotal = data.data[1].activeTotal;
                _this3.transTotal = data.data[1].transTotal;
                _this3.activityCode = data.data[1].activityCode;
            });
        },
        gotoDetail: function gotoDetail(title, activityCode) {
            this.$router.push({
                "path": "rewardDetail",
                "query": {
                    title: title,
                    activityCode: activityCode
                }
            });
        },
        getImg: function getImg(url) {
            if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
                return url = url;
            } else {
                return url = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + url;
            }
        }
    }
};

/***/ }),

/***/ 1622:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1685:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/ico_unionpay.png?v=c2663128";

/***/ }),

/***/ 1686:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump_reward.png?v=716cb1b4";

/***/ }),

/***/ 1935:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "rewardMain"
  }, [_c('div', {
    staticClass: "rewardHead"
  }, [_c('div', {
    staticClass: "headMain"
  }, [_c('div', {
    staticClass: "headTitle"
  }, [_vm._v("\n                    奖励排行榜\n                ")]), _vm._v(" "), _c('div', {
    staticClass: "headLogoMain"
  }, [_c('div', {
    staticClass: "headLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo0),
      "alt": ""
    }
  })]), _vm._v(" "), _c('p', [_vm._v("top1 " + _vm._s(_vm.userName0))]), _vm._v(" "), _c('div', {
    staticClass: "totalAmount"
  }, [_vm._v("总额 " + _vm._s(_vm.amount0) + "元")])]), _vm._v(" "), _c('div', {
    staticClass: "swiMain"
  }, [_c('div', {
    staticClass: "swiper-container",
    staticStyle: {
      "overflow": "inherit !important"
    },
    attrs: {
      "id": "swiper-container"
    }
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "rankDetail"
  }, [_c('div', {
    staticClass: "rankList",
    staticStyle: {
      "border-bottom": "1px solid rgba(234, 234, 238, 0.4)"
    }
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        02\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo1),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName1))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount1) + "元\n                                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "rankList"
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        03\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo2),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName2))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount2) + "元\n                                    ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide",
    style: ({
      'height': _vm.index == 0 ? 'auto' : '0px'
    })
  }, [_c('div', {
    staticClass: "rankDetail"
  }, [_c('div', {
    staticClass: "rankList",
    staticStyle: {
      "border-bottom": "1px solid rgba(234, 234, 238, 0.4)"
    }
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        04\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo3),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName3))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount3) + "元\n                                    ")])]), _vm._v(" "), _c('div', {
    staticClass: "rankList"
  }, [_c('div', {
    staticClass: "rankLogo"
  }, [_vm._v("\n                                        05\n                                    ")]), _vm._v(" "), _c('div', {
    staticClass: "nickLogo"
  }, [_c('img', {
    attrs: {
      "src": _vm.getImg(_vm.userLogo4),
      "alt": ""
    }
  })]), _vm._v(" "), _c('span', [_vm._v(_vm._s(_vm.userName4))]), _vm._v(" "), _c('div', {
    staticClass: "numRight"
  }, [_vm._v("\n                                        " + _vm._s(_vm.amount4) + "元\n                                    ")])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "rankMain"
  }, [_c('ul', {
    staticClass: "tab"
  }, _vm._l((_vm.allRankList), function(i, index) {
    return _c('li', {
      key: index,
      class: {
        'active': i.active
      }
    })
  }), 0)])])]), _vm._v(" "), _c('div', {
    staticClass: "rewardBottom"
  }, [_c('div', {
    staticClass: "bottomTitle"
  }, [_vm._v("\n                我的奖励\n            ")]), _vm._v(" "), _c('div', {
    staticClass: "rewardList"
  }, [_c('div', {
    staticClass: "item"
  }, [_c('div', {
    staticClass: "itemTitle"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1685),
      "alt": ""
    }
  }), _vm._v("\n                        " + _vm._s(_vm.rewardName) + "\n                    ")]), _vm._v(" "), _c('b', [_vm._v(_vm._s(_vm.amountTotal))]), _vm._v(" "), _c('span', [_vm._v("奖励总额（元）")]), _vm._v(" "), _c('div', {
    staticClass: "rewardDetail"
  }, [_c('div', {
    staticClass: "detail"
  }, [_c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.custTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntro"
  }, [_vm._v("\n                                    商户总数（个）\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.activeTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntro"
  }, [_vm._v("\n                                    激活达标（个）\n                                ")])]), _vm._v(" "), _c('div', {
    staticClass: "detailMain"
  }, [_c('span', [_vm._v(_vm._s(_vm.transTotal))]), _vm._v(" "), _c('div', {
    staticClass: "detailIntroLast"
  }, [_vm._v("\n                                    交易达标（个）\n                                ")])])])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.gotoDetail(_vm.rewardName, _vm.activityCode);
      }
    }
  }, [_vm._v("查看明细"), _c('img', {
    attrs: {
      "src": __webpack_require__(1686),
      "alt": ""
    }
  })])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-d7049322", module.exports)
  }
}

/***/ }),

/***/ 2083:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1622);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("22ed77eb", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d7049322!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardCenter.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d7049322!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./rewardCenter.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 629:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2083)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1426),
  /* template */
  __webpack_require__(1935),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\rewardCenter\\rewardCenter.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] rewardCenter.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d7049322", Component.options)
  } else {
    hotAPI.reload("data-v-d7049322", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});