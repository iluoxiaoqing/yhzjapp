webpackJsonp([44],{

/***/ 1371:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

var _searchBox = __webpack_require__(755);

var _searchBox2 = _interopRequireDefault(_searchBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            menuArray: [{
                title: "推荐开店",
                active: "",
                num: ""
            }, {
                title: "推荐交易",
                active: "",
                num: ""
            }, {
                title: "推荐购物",
                active: "",
                num: ""
            }],
            recommendShop: [],
            recommendCard: [],
            recommendLoan: [],
            isLoading: true,
            currentPage: 1,
            page: "",
            hasData: false,
            expressFee: "",
            bussFlowNo: "",
            orderIndex: 0,
            mySwiper: null,
            index: 0,
            defaultText: ""
        };
    },

    components: {
        alertBox: _alertBox2.default,
        searchBox: _searchBox2.default
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        console.log("this为============", this);
        if (!sessionStorage.askPositon || from.path == '/') {
            sessionStorage.askPositon = '';
            next();
        } else {
            next(function (vm) {
                if (vm && vm.$refs.my_scroller) {
                    //通过vm实例访问this
                    setTimeout(function () {
                        vm.$refs.my_scroller.scrollTo(0, sessionStorage.askPositon, false);
                    }, 20); //同步转异步操作
                }
            });
        }
    },
    beforeRouteLeave: function beforeRouteLeave(to, from, next) {
        //记录离开时的位置
        sessionStorage.askPositon = this.$refs.my_scroller && this.$refs.my_scroller.getPosition() && this.$refs.my_scroller.getPosition().top;
        next();
    },
    mounted: function mounted() {
        var _this = this;

        this.$refs.searchBox.inputValue = "";
        window.goBack = this.goBack;
        this.getHeight();
        // this.getActive();
        // let i = this.$route.query.type;
        this.getShop();
        this.getCard();
        this.getLoan();
        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            autoHeight: true,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: function onSlideChangeEnd(swiper) {
                var index = swiper.activeIndex;
                _this.menuArray.map(function (el) {
                    el.active = false;
                });
                _this.menuArray[index].active = true;
                _this.$refs.searchBox.inputValue = "";
                var wrapper = document.getElementById("swiper-wrapper");
                wrapper.style.height = 'auto';
                // if(index==0){
                // 	this.getShop();
                // }else if(index==1){
                // 	this.getCard();
                // }else{
                // 	this.getLoan();
                // }
            }
        });
        var type = this.$route.query.type || 0;
        this.changeList(this.menuArray[type], type);
    },

    methods: {
        searchBtn: function searchBtn(search, done) {
            switch (this.index) {
                case 0:
                    this.getShop();
                    break;
                case 1:
                    this.getCard();
                    break;
                case 2:
                    this.getLoan();
                    break;
            }
        },

        // getActive(){
        // 	let i = this.$route.query.type;
        // 	this.menuArray[i].active =true;
        // },
        getHeight: function getHeight() {
            var bodyHeight = document.documentElement.clientHeight;
            var scroller = this.$refs.scroller;
            var scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = bodyHeight - scrollerTop + 44 + "px";
        },
        changeList: function changeList(i, index) {
            this.index = index;
            this.$refs.searchBox.inputValue = "";
            // 新增
            this.currentPage = 1;
            this.defaultText = "";
            // 删除
            // if(this.index==0){
            // 	this.getShop();
            // }else if(this.index==1){
            // 	this.getCard();
            // }else{
            // 	this.getLoan();
            // }
            this.menuArray.map(function (el) {
                el.active = false;
            });
            i.active = true;
            // 新增
            this.refresh();
            this.mySwiper.slideTo(index, 500, false);
        },
        scrollBy: function scrollBy(ev) {
            console.log(ev);
        },
        getCard: function getCard() {
            var _this2 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "cardBill/queryCardBill",
                data: {
                    "page": 1,
                    "billStatus": "SUCCESS",
                    userName: this.$refs.searchBox.inputValue
                },
                showLoading: false
            }, function (data) {
                _this2.recommendCard = data.data.object;
                _this2.menuArray[1].num = data.data.totalResult;
                if (_this2.recommendCard.length > 0) {
                    _this2.hasData = false;
                    _this2.defaultText = "没有更多了";
                } else {
                    _this2.hasData = true;
                    _this2.defaultText = "暂无数据";
                }
            });
        },
        getShop: function getShop() {
            var _this3 = this;

            this.isLoading = true;
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "salesBill/querySalesBill",
                data: {
                    "page": 1,
                    userName: this.$refs.searchBox.inputValue
                },
                showLoading: false
            }, function (data) {
                _this3.recommendShop = data.data.object;
                _this3.menuArray[0].num = data.data.totalResult;
                if (_this3.recommendShop.length > 0) {
                    _this3.hasData = false;
                    _this3.defaultText = "没有更多了";
                } else {
                    _this3.hasData = true;
                    _this3.defaultText = "暂无数据";
                }
            });
        },
        getLoan: function getLoan() {
            var _this4 = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "loanBill/queryLoanBill",
                data: {
                    "page": 1,
                    userName: this.$refs.searchBox.inputValue,
                    billStatus: "SUCCESS"
                },
                showLoading: false
            }, function (data) {
                _this4.recommendLoan = data.data.object;
                _this4.menuArray[2].num = data.data.totalResult;
                if (_this4.recommendLoan.length > 0) {
                    _this4.hasData = false;
                    _this4.defaultText = "没有更多了";
                } else {
                    _this4.hasData = true;
                    _this4.defaultText = "暂无数据";
                }
            });
        },
        infinite: function infinite() {
            var _this5 = this;

            setTimeout(function () {
                if (_this5.index == 0) {
                    _this5.getPageList();
                } else if (_this5.index == 1) {
                    _this5.getPageList1();
                } else {
                    _this5.getPageList2();
                }
            }, 1000);
        },
        refresh: function refresh() {
            var _this6 = this;

            setTimeout(function () {
                _this6.currentPage = 1;
                _this6.isLoading = true;
                var wrapper = document.getElementById("swiper-wrapper");
                wrapper.style.height = 'auto';
                if (_this6.index == 0) {
                    _this6.getPageList();
                } else if (_this6.index == 1) {
                    _this6.getPageList1();
                } else {
                    _this6.getPageList2();
                }
                _this6.$refs.my_scroller.finishPullToRefresh();
            }, 1000);
        },
        getPageList: function getPageList() {
            var _this7 = this;

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "salesBill/querySalesBill",
                    data: {
                        "page": this.currentPage,
                        userName: this.$refs.searchBox.inputValue
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this7.currentPage == 1) {
                            _this7.recommendShop = [];
                        }
                        data.data.object.map(function (el) {
                            _this7.$set(el, "checked", false);
                            _this7.recommendShop.push(el);
                        });

                        _this7.currentPage++;
                        _this7.isLoading = true;
                        // done(true);
                        _this7.$refs.my_scroller.finishInfinite(true);
                    } else {
                        // done(2)
                        _this7.$refs.my_scroller.finishInfinite(2);
                    }
                    if (_this7.recommendShop.length > 0) {
                        _this7.hasData = false;
                        _this7.defaultText = "没有更多了";
                    } else {
                        _this7.hasData = true;
                        _this7.defaultText = "暂无数据";
                    }
                });
            } else {
                // done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        },
        getPageList1: function getPageList1() {
            var _this8 = this;

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "cardBill/queryCardBill",
                    data: {
                        "page": this.currentPage,
                        "billStatus": "SUCCESS",
                        userName: this.$refs.searchBox.inputValue
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this8.currentPage == 1) {
                            _this8.recommendCard = [];
                        }
                        data.data.object.map(function (el) {
                            _this8.$set(el, "show", true);
                            _this8.recommendCard.push(el);
                        });

                        _this8.currentPage++;
                        _this8.isLoading = true;
                        // done(true);
                        _this8.$refs.my_scroller.finishInfinite(true);
                    } else {
                        // done(2)
                        _this8.$refs.my_scroller.finishInfinite(2);
                    }
                    if (_this8.recommendCard.length > 0) {
                        _this8.hasData = false;
                        _this8.defaultText = "没有更多了";
                    } else {
                        _this8.hasData = true;
                        _this8.defaultText = "暂无数据";
                    }
                });
            } else {
                // done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        },
        getPageList2: function getPageList2() {
            var _this9 = this;

            if (this.isLoading) {
                this.isLoading = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "loanBill/queryLoanBill",
                    data: {
                        "page": this.currentPage,
                        userName: this.$refs.searchBox.inputValue,
                        billStatus: "SUCCESS"
                    },
                    showLoading: false
                }, function (data) {
                    if (data.data.object.length > 0) {
                        if (_this9.currentPage == 1) {
                            _this9.recommendLoan = [];
                        }
                        data.data.object.map(function (el) {
                            _this9.$set(el, "show", true);
                            _this9.recommendLoan.push(el);
                        });

                        _this9.currentPage++;
                        _this9.isLoading = true;
                        // done(true);
                        _this9.$refs.my_scroller.finishInfinite(true);
                    } else {
                        // done(2)
                        _this9.$refs.my_scroller.finishInfinite(2);
                    }
                    if (_this9.recommendLoan.length > 0) {
                        _this9.hasData = false;
                        _this9.defaultText = "没有更多了";
                    } else {
                        _this9.hasData = true;
                        _this9.defaultText = "暂无数据";
                    }
                });
            } else {
                // done(2)
                this.$refs.my_scroller.finishInfinite(2);
            }
        }
    }
};

/***/ }),

/***/ 1541:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1855:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "myOrder"
  }, [_c('div', {
    staticClass: "queryHead"
  }, [_c('div', {
    staticClass: "queryHead_menu"
  }, _vm._l((_vm.menuArray), function(i, index) {
    return _c('a', {
      key: index,
      class: {
        'active': i.active
      },
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.changeList(i, index)
        }
      }
    }, [_vm._v(_vm._s(i.title) + "(" + _vm._s(i.num) + ")")])
  }), 0), _vm._v(" "), _c('search-box', {
    ref: "searchBox",
    attrs: {
      "placeholderText": '请输入姓名搜索',
      "type": false
    },
    on: {
      "confirm": _vm.searchBtn
    }
  })], 1), _vm._v(" "), _c('div', {
    ref: "scroller",
    staticClass: "scroller"
  }, [_c('scroller', {
    ref: "my_scroller",
    attrs: {
      "noDataText": _vm.defaultText,
      "on-refresh": _vm.refresh,
      "on-infinite": _vm.infinite
    }
  }, [_c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper",
    attrs: {
      "id": "swiper-wrapper"
    }
  }, [_c('div', {
    staticClass: "swiper-slide"
  }, [_c('div', {
    staticClass: "recommendList"
  }, _vm._l((_vm.recommendShop), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.buyUserName) + "购买" + _vm._s(i.goodsCount) + "台设备")]), _vm._v(" "), _c('b', [_vm._v("已返佣")])]), _vm._v(" "), _c('span', [_vm._v(_vm._s(i.createTime))])])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide"
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.recommendCard), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.productName))]), _vm._v(" "), _c('b', [_vm._v("交易成功，已返佣")])]), _vm._v(" "), _c('em', [_vm._v("姓名：" + _vm._s(i.custName))]), _vm._v(" "), _c('em', [_vm._v("电话：" + _vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t\t\t\t" + _vm._s(i.createTime) + "\n\t\t\t\t\t\t\t\t\t\t")])])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "swiper-slide"
  }, [_c('div', {
    staticClass: "orderList"
  }, _vm._l((_vm.recommendLoan), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "item"
    }, [_c('p', [_c('b', [_vm._v(_vm._s(i.productName))]), _vm._v(" "), _c('b', [_vm._v("购物成功,已返佣")])]), _vm._v(" "), _c('em', [_vm._v("姓名：" + _vm._s(i.custName))]), _vm._v(" "), _c('em', [_vm._v("电话：" + _vm._s(i.custPhone))]), _vm._v(" "), _c('span', [_vm._v("\n\t\t\t\t\t\t\t\t\t\t" + _vm._s(i.createTime) + "\n\t\t\t\t\t\t\t\t\t\t")])])
  }), 0)])])])])], 1), _vm._v(" "), _c('alert-box', {
    attrs: {
      "visible": _vm.deleteShow,
      "boxInfo": _vm.deleteInfo
    },
    on: {
      "update:visible": function($event) {
        _vm.deleteShow = $event
      },
      "confirm": _vm.deleteConfirm
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-366f3194", module.exports)
  }
}

/***/ }),

/***/ 2002:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1541);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("2edd79ce", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-366f3194&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./achievement.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-366f3194&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./achievement.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 565:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2002)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1371),
  /* template */
  __webpack_require__(1855),
  /* scopeId */
  "data-v-366f3194",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\achievement\\achievement_one.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] achievement_one.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-366f3194", Component.options)
  } else {
    hotAPI.reload("data-v-366f3194", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 751:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(757)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(750),
  /* template */
  __webpack_require__(756),
  /* scopeId */
  "data-v-7cf3f18a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7cf3f18a", Component.options)
  } else {
    hotAPI.reload("data-v-7cf3f18a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7cf3f18a", module.exports)
  }
}

/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(751);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05a769e3", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7cf3f18a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});