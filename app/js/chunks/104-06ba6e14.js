webpackJsonp([104],{

/***/ 1447:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _api = __webpack_require__(11);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            allShareCode: {},
            shopGroups: [],
            showAd: "1",
            context: '',
            title: ""
        };
    },

    components: {
        alertBox: _alertBox2.default
    },
    mounted: function mounted() {
        this.getList();
        // this.getShowAd();
    },

    methods: {
        share: function share(j) {
            console.log("分享", "分享");
            this.$router.push({
                "path": "share",
                "query": {
                    "product": j.mallCode,
                    "productName": j.mallName,
                    "channel": "gouwuShop"
                }
            });
        },


        // allShare(j) {
        //     this.$router.push({
        //         "path": "share",
        //         "query": {
        //             "product": j,
        //             "productName": "全部推荐",
        //             "channel": "mallShop"
        //         }
        //     });
        // },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "bussMenu/mallBusiness",
                "data": {},
                type: "GET"
            }, function (data) {
                console.log(data.data);
                _this.shopGroups = data.data.details;
                console.log("shopGroups", _this.shopGroups);
            });
        },
        apply: function apply(goodsType) {
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "mall/register",
                data: {
                    productCode: goodsType
                },
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                // window.location.href = data.data.ky_no_pos;
                window.location.href = _api.WEB_URL + "gouwuLiucheng";
            });
        }
    }
};

/***/ }),

/***/ 1582:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1895:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "mallShop"
  }, [_vm._l((_vm.shopGroups), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "main"
    }, [_c('div', {
      staticClass: "shopMain"
    }, [_c('div', {
      staticClass: "shopTop"
    }, [_c('div', {
      staticClass: "listImg"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file/downloadFile?filePath=' + i.logoPath
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "listDeatil"
    }, [_c('div', {
      staticClass: "titleLeft"
    }, [_vm._v("\n\t\t\t\t\t\t\t" + _vm._s(i.mallName) + "\n\t\t\t\t\t\t")]), _vm._v(" "), _c('div', {
      staticClass: "time"
    }, [_c('i', [_vm._v(_vm._s(i.remark))]), _vm._v(" "), (i.allowShare == true) ? _c('div', {
      staticClass: "aaaa"
    }, [_c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.share(i)
        }
      }
    }, [_vm._v(_vm._s(i.shareBtnText))])]) : _vm._e()])])]), _vm._v(" "), _c('div', {
      staticClass: "shopBottom"
    }, [_c('div', {
      on: {
        "click": function($event) {
          return _vm.apply(i.mallCode)
        }
      }
    }, [_vm._v("\n\t\t\t\t\t\t" + _vm._s(i.applyBtnText) + "\n\t\t\t\t\t")])])])])
  }), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.showAd == 0) ? _c('div', {
    staticClass: "whyShop_bg",
    on: {
      "touchmove": function($event) {
        $event.preventDefault();
      }
    }
  }, [_c('div', {
    staticClass: "btn_close"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(953),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.close();
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "whyShop"
  }, [_c('h1', [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), (_vm.context[0]) ? _c('p', [_vm._v(_vm._s(_vm.context[0]))]) : _vm._e(), _vm._v(" "), (_vm.context[1]) ? _c('p', [_vm._v(_vm._s(_vm.context[1]))]) : _vm._e(), _vm._v(" "), (_vm.context[2]) ? _c('p', [_vm._v(_vm._s(_vm.context[2]))]) : _vm._e(), _vm._v(" "), (_vm.context[3]) ? _c('p', [_vm._v(_vm._s(_vm.context[3]))]) : _vm._e(), _vm._v(" "), (_vm.context[4]) ? _c('p', [_vm._v(_vm._s(_vm.context[4]))]) : _vm._e(), _vm._v(" "), (_vm.context[5]) ? _c('p', [_vm._v(_vm._s(_vm.context[5]))]) : _vm._e(), _vm._v(" "), (_vm.context[6]) ? _c('p', [_vm._v(_vm._s(_vm.context[6]))]) : _vm._e(), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:void(0)"
    },
    on: {
      "click": function($event) {
        return _vm.close()
      }
    }
  }, [_vm._v("我知道了")])])]) : _vm._e()])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-68fa7e58", module.exports)
  }
}

/***/ }),

/***/ 2043:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1582);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7298bc5f", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-68fa7e58&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-68fa7e58&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mallShop.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 651:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2043)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1447),
  /* template */
  __webpack_require__(1895),
  /* scopeId */
  "data-v-68fa7e58",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\gouwuShop.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] gouwuShop.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68fa7e58", Component.options)
  } else {
    hotAPI.reload("data-v-68fa7e58", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 953:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/close.png?v=ff0c5a72";

/***/ })

});