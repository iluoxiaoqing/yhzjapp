webpackJsonp([13],{

/***/ 1243:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1246)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(960),
  /* template */
  __webpack_require__(1244),
  /* scopeId */
  "data-v-21be181a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\searchBox4.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBox4.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-21be181a", Component.options)
  } else {
    hotAPI.reload("data-v-21be181a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 1244:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "searchBox",
    class: {
      'grey': _vm.type
    },
    attrs: {
      "id": "searchBox"
    }
  }, [_c('div', {
    staticClass: "searchBox_content"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.inputValue),
      expression: "inputValue"
    }],
    ref: "searchInput",
    class: {
      'focus': _vm.isFocus
    },
    attrs: {
      "id": "search",
      "type": "text",
      "placeholder": _vm.placeholderText
    },
    domProps: {
      "value": (_vm.inputValue)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.inputValue = $event.target.value
      }
    }
  }), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.confirm()
      }
    }
  }, [_vm._v("搜索")])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-21be181a", module.exports)
  }
}

/***/ }),

/***/ 1246:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(962);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("69b749b1", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21be181a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox4.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-21be181a&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBox4.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1408:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _searchBox = __webpack_require__(1243);

var _searchBox2 = _interopRequireDefault(_searchBox);

var _noDataHigh = __webpack_require__(860);

var _noDataHigh2 = _interopRequireDefault(_noDataHigh);

var _alertBox = __webpack_require__(41);

var _alertBox2 = _interopRequireDefault(_alertBox);

var _freshToLoadmore = __webpack_require__(70);

var _freshToLoadmore2 = _interopRequireDefault(_freshToLoadmore);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    components: {
        alertBox: _alertBox2.default,
        searchBox: _searchBox2.default,
        freshToLoadmore: _freshToLoadmore2.default,
        noData: _noDataHigh2.default
    },
    data: function data() {
        return {
            totalNum: 0, //机具台数
            diaList: [], //机具列表
            showCar: false,
            menuList: [],
            currentGroup: "", //默认值
            operateNum: this.$route.query.operateNum, //共多少台
            successNum: this.$route.query.successNum, //成功多少台
            failNum: this.$route.query.failNum, //失败多少台
            // productCode: ""   //TODO需要打开，目前是没有数据
            showNodata: false,
            diaMenulist: [], //菜单列表
            group: 0

        };
    },
    mounted: function mounted() {
        // this.getMenu();
        this.getList(0); //获取列表
        this.getMenu();
    },

    methods: {
        // 获取列表
        getList: function getList(i) {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "pos/terminalManagement/findDetailLogList",
                "type": "post",
                "data": {
                    flowNo: this.$route.query.flowNo,
                    productCode: this.productCode,
                    status: ""
                }
            }, function (data) {
                // this.diaMenulist = data.data.object;
                data.data.map(function (el) {
                    _this.$set(el, "lastCode", "");
                    _this.$set(el, "firstCode", "");
                    _this.diaList.push(el);
                });
                _this.diaList.map(function (item) {
                    var disLength = item.posSn.length;
                    item.lastCode = item.posSn.substring(disLength - 6, disLength);
                    item.firstCode = item.posSn.substring(0, disLength - 6);
                    item.expireTime = common.commonResetDate(new Date(item.expireTime), "yyyy-MM-dd");
                });
                if (_this.diaList.length > 0) {
                    _this.showNodata = false;
                } else {
                    _this.showNodata = true;
                }
                // if (i == 0) {
                //     this.diaMenulist = JSON.parse(sessionStorage.getItem("diaMenulist"))
                // }

                console.log("++++++++", _this.diaList);
            });
        },
        continue1: function continue1() {
            window.location.href = _apiConfig2.default.WEB_URL + 'myMachines';
        },
        goRecords: function goRecords() {
            window.location.href = _apiConfig2.default.WEB_URL + 'getRecords';
        },

        // 获取弹窗菜单
        getMenu: function getMenu() {
            var _this2 = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "pos/terminalManagement/findDetailProductList",
                "type": "post",
                "data": {
                    flowNo: this.$route.query.flowNo
                }
            }, function (data) {
                _this2.diaMenulist = data.data;
                var arr = {
                    productCode: "",
                    productName: "全部"
                };
                _this2.diaMenulist.splice(0, 0, arr);
                for (var i = 0; i < _this2.diaMenulist.length; i++) {
                    _this2.$set(_this2.diaMenulist[i], 'active', false);
                }
                _this2.diaMenulist[0].active = 'true';
                _this2.productCode = _this2.diaMenulist[0].productCode;
                console.log("product=====", _this2.productCode);
                // this.getList();
            });
        },

        // 获取顶部菜单列表
        // getMenu() {
        //     common.Ajax({
        //         "url": api.KY_IP + "pos/terminalManagement/findProductList",
        //         "type": "post",
        //         "data": {

        //         }
        //     }, (data) => {
        //         this.diaMenulist = data.data;
        //         let arr = {
        //             productCode: "",
        //             productName: "全部",
        //         };
        //         this.diaMenulist.splice(0, 0, arr);
        //         for (var i = 0; i < this.diaMenulist.length; i++) {
        //             this.$set(this.diaMenulist[i], 'active', false)
        //         }
        //         this.diaMenulist[0].active = 'true';
        //         this.productCode = this.diaMenulist[0].productCode;
        //         console.log("product=====", this.productCode);
        //         // TODO   因后端数据有问题暂时关闭
        //         // this.getList();
        //     });
        // },
        changeTab11: function changeTab11(idx, productCode) {
            var _this3 = this;

            // if (idx == this.group) {
            //     return;
            // }
            // console.log("idx===", idx)
            // console.log("this.diaMenulist===", this.diaMenulist)
            // this.diaMenulist.map((el) => {
            //     this.$set(el, "active", false);
            // });
            // this.$set(this.diaMenulist[idx - 1], "active", true);
            // this.group = idx;
            // this.productCode = productCode;
            // console.log("====", this.productCode)
            // this.diaList = [];
            // this.getList();
            if (idx == this.group) {
                return;
            }
            this.diaMenulist.map(function (el) {
                _this3.$set(el, "active", false);
            });
            this.$set(this.diaMenulist[idx - 1], "active", true);
            this.diaList = [];
            this.productCode = productCode;
            this.getList();
        }
    }

};

/***/ }),

/***/ 1584:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\machines\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1897:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "buyList"
  }, [_c('div', {
    staticClass: "buyList_head"
  }, [_c('p', [_vm._v("共计划划" + _vm._s(_vm.operateNum) + "台，成功"), _c('i', [_vm._v(_vm._s(_vm.successNum))]), _vm._v("台，失败"), _c('em', [_vm._v(_vm._s(_vm.failNum))]), _vm._v("台")])]), _vm._v(" "), _c('div', {
    staticClass: "menu"
  }, [_c('ul', _vm._l((_vm.diaMenulist), function(itm, idx) {
    return _c('li', {
      class: {
        'active': itm.active
      },
      on: {
        "click": function($event) {
          return _vm.changeTab11(idx + 1, itm.productCode)
        }
      }
    }, [_vm._v("\n\t\t\t\t\t\t\t\t" + _vm._s(itm.productName) + "\n\t\t\t\t\t\t\t")])
  }), 0)]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showNodata),
      expression: "showNodata"
    }],
    staticClass: "empty"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(957),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("暂无数据")])]), _vm._v(" "), _c('div', {
    staticClass: "buyList_body"
  }, _vm._l((_vm.diaList), function(i) {
    return _c('div', {
      key: i,
      staticClass: "item"
    }, [_c('div', {
      staticClass: "list"
    }, [_c('div', {
      staticClass: "list_left"
    }, [_c('div', [_c('em', [_vm._v("Sn:" + _vm._s(i.firstCode))]), _c('em', {
      staticStyle: {
        "color": "red"
      }
    }, [_vm._v(_vm._s(i.lastCode))])])]), _vm._v(" "), _c('div', {
      staticClass: "list_right"
    }, [(i.status == 'SUCCESS') ? _c('span', {
      staticStyle: {
        "color": "#57CC64"
      }
    }, [_vm._v("划拨成功")]) : _c('span', {
      staticStyle: {
        "color": "#FF1E45"
      }
    }, [_vm._v("划拨失败")])])])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "btn"
  }, [_c('button', {
    staticClass: "left",
    on: {
      "click": function($event) {
        return _vm.continue1()
      }
    }
  }, [_vm._v("继续划拨")]), _vm._v(" "), _c('button', {
    staticClass: "right",
    on: {
      "click": function($event) {
        return _vm.goRecords()
      }
    }
  }, [_vm._v("查看记录")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6b17b446", module.exports)
  }
}

/***/ }),

/***/ 2045:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1584);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("29348d37", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6b17b446&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./transRecords.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6b17b446&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./transRecords.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 604:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2045)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1408),
  /* template */
  __webpack_require__(1897),
  /* scopeId */
  "data-v-6b17b446",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\machines\\transRecords.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] transRecords.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6b17b446", Component.options)
  } else {
    hotAPI.reload("data-v-6b17b446", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 837:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

__webpack_require__(42);

exports.default = {
  data: function data() {
    return {};
  },

  props: {
    isNoData: {
      "type": Boolean,
      "default": false
    }
  }
}; //
//
//
//
//
//
//

/***/ }),

/***/ 838:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 860:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(862)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(837),
  /* template */
  __webpack_require__(861),
  /* scopeId */
  "data-v-ec155454",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\components\\noDataHigh.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] noDataHigh.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ec155454", Component.options)
  } else {
    hotAPI.reload("data-v-ec155454", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 861:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isNoData),
      expression: "isNoData"
    }],
    staticClass: "noData"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(163),
      "alt": ""
    }
  }), _vm._v(" "), _c('p', [_vm._v("暂无数据")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-ec155454", module.exports)
  }
}

/***/ }),

/***/ 862:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(838);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("5036894e", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-ec155454&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./noDataHigh.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-ec155454&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./noDataHigh.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 957:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/empty.png?v=4a57060c";

/***/ }),

/***/ 960:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			placeholder: "",
			isFocus: false,
			inputValue: ""
		};
	},

	props: {
		placeholderText: {
			type: String,
			default: "搜索"
		},
		type: {
			type: Boolean,
			default: false
		},
		inputText: String
	},
	mounted: function mounted() {},

	methods: {
		// enterInput(){
		// 	this.isFocus = true;
		// },
		// outInput(){
		// 	this.isFocus = false;
		// },
		confirm: function confirm() {
			this.$emit("confirm", this.inputValue);
		}
	}
};

/***/ }),

/***/ 962:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\components\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ })

});