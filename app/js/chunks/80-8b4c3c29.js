webpackJsonp([80],{

/***/ 1462:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            hasAddress: true,
            address: {},
            goodInfo: [],
            goodNum: 1,
            payShow: false,
            totalAmount: "",
            imgUrl: _apiConfig2.default.KY_IP,
            accountInfo: {},
            checkOut: false,
            balanceDeduc: 0
        };
    },
    activated: function activated() {
        var _this = this;

        window.alipayResult = this.alipayResult;
        var purchaseJson = this.$store.state.purchaseJson;
        var address = this.$store.state.address;
        console.log("22222", address);

        console.log("1111111", purchaseJson);

        this.goodInfo = purchaseJson.carList || "";
        console.log("商品信息为=====", this.goodInfo);
        this.totalAmount = purchaseJson.totalAmount;

        if (JSON.stringify(address) == "{}") {
            console.log("122222");
            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "addr/one"
            }, function (data) {
                if (data.data) {
                    _this.hasAddress = true;
                    _this.address = data.data;
                    console.log("地址为", _this.address);
                } else {
                    _this.hasAddress = false;
                }
            });
        } else {
            console.log("234444");
            this.address = address;
            this.hasAddress = true;
        }

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "acc/accountInfo"
        }, function (data) {

            _this.accountInfo = data.data;

            // if(data.data.balance>0){
            // 	this.checkOut = true;
            // }
            // else{
            // 	this.checkOut = false;
            // }
        });

        common.youmeng("提交订单", "进入提交订单");
    },
    mounted: function mounted() {},

    watch: {
        checkOut: function checkOut(value) {

            var balance = this.accountInfo.balance * 1;

            if (this.accountInfo.accStatus == 'NORMAL') {

                if (value) {
                    if (balance >= this.totalAmount * 1) {
                        this.balanceDeduc = this.totalAmount * 1;
                    } else {
                        this.balanceDeduc = balance;
                    }
                } else {
                    this.balanceDeduc = 0;
                }
            } else {
                common.toast({
                    "content": "您的账户暂时不允许使用余额抵扣"
                });
                this.checkOut = false;
            }

            //console.log(this.balanceDeduc)
        }
    },
    methods: {
        addYoumeng: function addYoumeng() {
            common.youmeng("提交订单", "点击添加地址");
        },
        alipayResult: function alipayResult(state) {
            var _this2 = this;

            if (state == "true" || state == 1 || state == true || state == '1') {
                common.toast({
                    "content": "支付成功"
                });
                common.youmeng("提交订单", "支付成功");
            } else {
                common.toast({
                    "content": "支付失败"
                });
                common.youmeng("提交订单", "支付失败");
            }

            setTimeout(function () {
                _this2.$router.push({
                    "path": "myOrder",
                    "query": {
                        "type": 1
                    }
                });
            }, 1000);
        },
        gotoAddress: function gotoAddress() {

            common.youmeng("提交订单", "打开地址列表选择");

            this.$router.push({
                "path": "addressList",
                "query": {
                    "appTitle": "",
                    "dispayInNative": ""
                }
            });
        },
        submitOrder: function submitOrder() {
            var _this3 = this;

            if (!this.hasAddress) {
                common.toast({
                    "content": "请先选择收货地址"
                });
                return;
            }

            common.youmeng("提交订单", "点击支付按钮");

            if (this.checkOut) {
                common.youmeng("提交订单", "使用账户余额抵扣");
            } else {
                common.youmeng("提交订单", "不使用使用账户余额抵扣");
            }

            var carList = this.$store.state.purchaseJson.carList;
            var goods = [];
            carList.map(function (el) {
                console.log(el);
                goods.push({
                    "type": el.goodsType,
                    "count": el.currentNum
                });
            });
            console.log("goods", goods);

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "posPurchase/purchaseSelf",
                "data": {
                    "address": this.address.id,
                    "goods": JSON.stringify(goods),
                    "payAmount": this.totalAmount - this.balanceDeduc,
                    "accAmount": this.balanceDeduc
                }
            }, function (data) {

                console.log(data.data);

                if (data.data.alipay) {
                    var aliPay = {
                        "alipay": data.data.alipay,
                        "redirectFunc": "alipayResult"
                    };

                    if (common.isClient() == "ios") {
                        window.webkit.messageHandlers.nativeAlipay.postMessage(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    } else {
                        window.android.nativeAlipay(JSON.stringify(aliPay));
                        console.log(JSON.stringify(aliPay));
                    }
                } else {

                    common.toast({
                        "content": "恭喜你下单成功！"
                    });

                    setTimeout(function () {
                        _this3.$router.push({
                            "path": "myOrder",
                            "query": {
                                "type": 1
                            }
                        });
                    }, 1000);
                }
            });
        }
    }
};

/***/ }),

/***/ 1547:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1712:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_pay_alipay.png?v=185475a9";

/***/ }),

/***/ 1861:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "mall_order"
  }, [(_vm.hasAddress) ? _c('div', {
    staticClass: "address",
    on: {
      "click": function($event) {
        return _vm.gotoAddress()
      }
    }
  }, [_c('div', {
    staticClass: "title"
  }, [(_vm.address.isDefault) ? _c('i', [_vm._v("默认")]) : _vm._e(), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.address.province) + " " + _vm._s(_vm.address.city) + " " + _vm._s(_vm.address.area))])]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_vm._v(_vm._s(_vm.address.addressDetail))]), _vm._v(" "), _c('div', {
    staticClass: "info"
  }, [_vm._v(_vm._s(_vm.address.receivePerson) + " " + _vm._s(_vm.address.phoneNo))])]) : _c('div', {
    staticClass: "addAddress"
  }, [_c('router-link', {
    attrs: {
      "to": "addAddress"
    }
  }, [_c('span', {
    on: {
      "click": function($event) {
        return _vm.addYoumeng()
      }
    }
  }, [_vm._v("添加收货地址")])])], 1), _vm._v(" "), _c('div', {
    staticClass: "order_goods"
  }, _vm._l((_vm.goodInfo), function(i, index) {
    return _c('div', {
      key: index,
      staticClass: "mall_top"
    }, [_c('div', {
      staticClass: "mall_img"
    }, [_c('img', {
      attrs: {
        "src": _vm.imgUrl + 'file//downloadFile?filePath=' + i.imgPath
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "mall_buy"
    }, [_c('div', {
      staticClass: "goods_description"
    }, [_c('h2', [_vm._v(_vm._s(i.goodsName))]), _vm._v(" "), _c('p', {
      domProps: {
        "innerHTML": _vm._s(JSON.parse(i.remark).goodsDesc)
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "goods_price"
    }, [_c('span', [_vm._v("¥" + _vm._s(i.price))]), _vm._v(" "), _c('div', {
      staticClass: "addGoods"
    }, [_c('i', [_vm._v("X" + _vm._s(i.currentNum))])])])])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "order_button"
  }, [_c('p', [_c('img', {
    attrs: {
      "src": __webpack_require__(1712)
    }
  }), _vm._v(" "), _c('span', [_vm._v("合计金额：")]), _vm._v(" "), _c('i', [_vm._v("￥" + _vm._s((_vm.totalAmount - _vm.balanceDeduc).toFixed(2)))])]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "click": function($event) {
        return _vm.submitOrder()
      }
    }
  }, [_vm._v("确认付款")])])]), _vm._v(" "), _c('choose-bank', {
    attrs: {
      "visible": _vm.payShow
    },
    on: {
      "update:visible": function($event) {
        _vm.payShow = $event
      }
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-3e3707dc", module.exports)
  }
}

/***/ }),

/***/ 2008:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1547);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("7b18ae0a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3e3707dc&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_order.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-3e3707dc&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./mall_order.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 667:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2008)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1462),
  /* template */
  __webpack_require__(1861),
  /* scopeId */
  "data-v-3e3707dc",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\mall_order.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] mall_order.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3e3707dc", Component.options)
  } else {
    hotAPI.reload("data-v-3e3707dc", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});