webpackJsonp([87],{

/***/ 1482:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

var _base = __webpack_require__(162);

var _base2 = _interopRequireDefault(_base);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            phone: ''
        };
    },
    mounted: function mounted() {},

    methods: {
        goPay: function goPay() {
            var _this = this;

            var myReg = _base2.default.phoneNumberTwo.reg;
            if (!myReg.test(this.phone)) {
                var errorMsg = _base2.default.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            common.Ajax({
                type: "post",
                url: _apiConfig2.default.KY_IP + "XYVIP/xyvip/wyyx",
                data: {
                    phone: this.phone,
                    htmlFlag: 'H5',
                    userNo: this.$route.query.userNo
                },
                showLoading: false
            }, function (data) {
                _this.$router.push({
                    "path": "wyyxPay",
                    "query": {
                        "orderNo": data.data.orderNo,
                        "payAmount": data.data.payAmount,
                        "userNo": _this.$route.query.userNo
                    }
                });
            });
        },
        test: function test() {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            document.documentElement.scrollTop ? document.documentElement.scrollTop = scrollTop + 'px' : document.body.scrollTop = scrollTop + 'px';
            this.goPay();
        }
    }
};

/***/ }),

/***/ 1521:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\wyyx\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1835:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "main",
    attrs: {
      "id": "main"
    }
  }, [_c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "vipList"
  }, [_c('div', {
    staticClass: "phoneInput"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "请输入您的手机号"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "blur": _vm.test,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  }), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.goPay()
      }
    }
  }, [_vm._v("立即购买")])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2095d2ff", module.exports)
  }
}

/***/ }),

/***/ 1982:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1521);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("77960e98", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2095d2ff&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xyvipWyyx.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-2095d2ff&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./xyvipWyyx.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 693:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1982)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1482),
  /* template */
  __webpack_require__(1835),
  /* scopeId */
  "data-v-2095d2ff",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\wyyx\\xyvipWyyx.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] xyvipWyyx.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2095d2ff", Component.options)
  } else {
    hotAPI.reload("data-v-2095d2ff", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});