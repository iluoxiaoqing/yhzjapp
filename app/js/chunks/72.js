webpackJsonp([72],{

/***/ 1260:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1367:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            detail: [],
            showDia: false,
            title: "",
            desc: "",
            thumbnail: "",
            u: "",
            showShare: false,
            show1: false,
            collect: "",
            isWx: false
        };
    },
    mounted: function mounted() {
        this.getDetail();
        window.shareSucces = this.shareSucces;
    },

    methods: {
        Shareshow: function Shareshow() {
            this.showDia = true;
            this.show1 = true;
        },
        getDetail: function getDetail() {
            var _this = this;

            var u = this.$route.query.u;
            if (u == undefined) {
                var _u = "";
                this.showShare = true;
                this.isWx = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "tuiShouCase/detail",
                    data: {
                        shareUser: _u,
                        id: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.u = data.data.shareUser;
                    _this.title = data.data.title;
                    _this.thumbnail = data.data.pic;
                    _this.collect = data.data.hasCollect;
                    _this.desc = data.data.desc;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            } else {
                var _u2 = this.$route.query.u;
                this.showShare = false;
                this.isWx = true;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "tuiShouCase/detail",
                    data: {
                        shareUser: _u2,
                        id: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.u = data.data.shareUser;
                    _this.title = data.data.title;
                    _this.thumbnail = data.data.pic;

                    _this.desc = data.data.desc;
                    _this.collect = data.data.hasCollect;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            }
        },
        wechatSession: function wechatSession() {
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.desc,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            var shareJson = {
                "type": "wechatTimeline",
                "image": "",
                "title": this.title,
                "des": this.desc,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        shareSucces: function shareSucces() {
            this.showDia = false;
            this.show1 = false;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },
        showBottom: function showBottom() {
            this.showDia = true;
        },
        toCollect: function toCollect() {
            var _this2 = this;

            common.youmeng("推手案例", "收藏" + this.$route.query.title);
            common.Ajax({
                url: _apiConfig2.default.KY_IP + "tuiShouCase/collect",
                data: {
                    id: this.$route.query.id
                },
                showLoading: false
            }, function (data) {
                if (data.code == "0000") {
                    _this2.collect = true;
                    common.toast({
                        content: data.msg
                    });
                } else {
                    common.toast({
                        content: data.msg
                    });
                }
            });
        }
    }
};

/***/ }),

/***/ 1472:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error {\n  background: #fff;\n}\n.tips_success {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer {\n  height: 1.6rem;\n}\n.fade-enter-active,\n.fade-leave-active {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter,\n.fade-leave-to {\n  opacity: 0;\n}\n.default_button {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller {\n  position: relative;\n}\n.loading-tips {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.contain {\n  /*弹窗*/\n}\n.contain .detail_main {\n  width: 92%;\n  margin: 0 4%;\n  overflow: hidden;\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n}\n.contain .detail_main h1 {\n  font-size: 0.4rem;\n  color: #3D4A5B;\n  margin: 0.32rem 0 0;\n}\n.contain .detail_main .materialList {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.contain .detail_main .materialList .list {\n  width: 100%;\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 0.48rem 0 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .detail_main .materialList .list .listImg {\n  width: 0.8rem;\n  height: 0.8rem;\n}\n.contain .detail_main .materialList .list .listImg img {\n  width: 100%;\n  height: 100%;\n  display: block;\n  border-radius: 0.4rem;\n  margin: 0 0 ;\n}\n.contain .detail_main .materialList .list .listDeatil {\n  width: 4.7rem;\n  height: 0.8rem;\n  margin-left: 0.16rem;\n  position: relative;\n}\n.contain .detail_main .materialList .list .listDeatil b {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  line-height: 0.44rem;\n}\n.contain .detail_main .materialList .list .listDeatil .time {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  line-height: 0.34rem;\n}\n.contain .detail_main .materialList .list .listDeatil .time i {\n  color: rgba(61, 74, 91, 0.4);\n  font-size: 0.24rem;\n  display: block;\n}\n.contain .detail_main .materialList .list .collect {\n  width: 1.02rem;\n  height: 0.44rem;\n  background: #3F83FF;\n  border-radius: 4px;\n  color: #ffffff;\n  font-size: 0.24rem;\n  text-align: center;\n  margin-top: 0.4rem;\n}\n.contain .detail_main .materialList .list .collected {\n  width: 1.02rem;\n  height: 0.44rem;\n  background: rgba(61, 74, 91, 0.4) !important;\n  border-radius: 4px;\n  color: #ffffff;\n  font-size: 0.24rem;\n  text-align: center;\n  margin-top: 0.4rem;\n}\n.contain .detail_main .content .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content p {\n  font-family: PingFangSC-Regular;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  line-height: 0.4rem;\n}\n.contain .detail_main .content p .red {\n  color: red !important;\n  display: inline;\n}\n.contain .detail_main .content p .yellow {\n  color: yellow !important;\n  display: inline;\n}\n.contain .detail_main .content p .green {\n  color: green !important;\n  display: inline;\n}\n.contain .detail_main .content p .blue {\n  color: blue !important;\n  display: inline;\n}\n.contain .detail_main .content p .font {\n  font-weight: bold;\n  display: inline;\n}\n.contain .detail_main .content img {\n  width: 100%;\n  height: auto;\n  margin: 0.48rem 0 0;\n}\n.contain .detail_main .content h2 {\n  font-family: PingFangSC-Medium;\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  margin: 0.48rem 0 0.16rem;\n}\n.contain .detail_main .erweima img {\n  width: 37%;\n  margin: 0 31.5%;\n}\n.contain .detail_main .erweima_text {\n  text-align: center;\n  font-size: 0.22rem;\n  color: #3D4A5B;\n  margin: 0.16rem 0 0.8rem;\n}\n.contain .detail_main .wx_erweima {\n  width: 100%;\n  overflow: hidden;\n  background: #fff;\n}\n.contain .detail_main .wx_erweima .list {\n  width: 38%;\n  margin: 0 auto;\n  overflow: hidden;\n  padding: 0.96rem 0 0.32rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .detail_main .wx_erweima .list .listImg {\n  width: 0.96rem;\n  height: 0.96rem;\n}\n.contain .detail_main .wx_erweima .list .listImg img {\n  width: 100%;\n  height: 100%;\n  display: block;\n  border-radius: 0.48rem;\n  margin: 0 0 ;\n}\n.contain .detail_main .wx_erweima .list .listDeatil {\n  width: 1.44rem;\n  height: 0.96rem;\n  margin-left: 0.16rem;\n  position: relative;\n}\n.contain .detail_main .wx_erweima .list .listDeatil b {\n  display: block;\n  color: #3D4A5B;\n  font-size: 0.32rem;\n  line-height: 0.44rem;\n}\n.contain .detail_main .wx_erweima .list .listDeatil .time {\n  width: 100%;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  line-height: 0.34rem;\n}\n.contain .detail_main .wx_erweima .list .listDeatil .time i {\n  color: rgba(61, 74, 91, 0.8);\n  font-size: 0.24rem;\n  display: block;\n  white-space: nowrap;\n}\n.contain .detail_main .share {\n  position: fixed;\n  bottom: 1.16rem;\n  right: 4%;\n}\n.contain .detail_main .share img {\n  width: 1.24rem;\n  height: 1.24rem;\n}\n.contain .dialog {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.contain .dialog img {\n  width: 5.6rem;\n  height: 2.17rem;\n}\n.contain .bottomBar {\n  z-index: 10000;\n  width: 100%;\n  height: 3.9rem;\n  background: #FAFBFF;\n  border-top-left-radius: 0.08rem;\n  border-top-right-radius: 0.08rem;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n}\n.contain .bottomBar .bottomBar_line {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.6rem;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 0.6rem;\n}\n.contain .bottomBar .bottomBar_line em {\n  margin: 0 0.2rem;\n}\n.contain .bottomBar .bottomBar_line span {\n  width: 1.0rem;\n  height: 1px;\n  display: block;\n  background: #C4C7CC;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.contain .bottomBar .bottomBar_content {\n  width: 92%;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.contain .bottomBar .bottomBar_content p {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n}\n.contain .bottomBar .bottomBar_content p img {\n  width: 1.0rem;\n  height: 1.0rem;\n  display: block;\n  margin: 0 auto 0.2rem;\n}\n.contain .bottomBar .bottomBar_content p span {\n  display: block;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  font-weight: 700;\n}\n", ""]);

// exports


/***/ }),

/***/ 1610:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share.png?v=8ae7e7dd";

/***/ }),

/***/ 1611:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 1612:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ }),

/***/ 1754:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("分享至")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatSession()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1612)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatTimeline()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1611)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('h1', [_vm._v(_vm._s(_vm.detail.title))]), _vm._v(" "), _c('div', {
    staticClass: "materialList"
  }, [_c('div', {
    staticClass: "list"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.person))]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.date))])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.collect == false && _vm.isWx == false),
      expression: "collect==false && isWx==false"
    }],
    staticClass: "collect",
    on: {
      "click": function($event) {
        return _vm.toCollect();
      }
    }
  }, [_vm._v("\n                    收藏\n                ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.collect == true && _vm.isWx == false),
      expression: "collect==true && isWx==false"
    }],
    staticClass: "collected"
  }, [_vm._v("\n                    已收藏\n                ")])])]), _vm._v(" "), _c('div', {
    staticClass: "content",
    domProps: {
      "innerHTML": _vm._s(_vm.detail.content)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "wx_erweima"
  }, [_c('div', {
    staticClass: "list"
  }, [_c('div', {
    staticClass: "listImg"
  }, [(_vm.detail.shareHeadPic == '' || _vm.detail.shareHeadPic == undefined) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1260)
    }
  }) : _c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.detail.shareHeadPic
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.sharePerson))]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.shareHint))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "erweima"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'news/shareQrCode?productCode=' + _vm.detail.productCode + '&u=' + _vm.detail.shareUser
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "erweima_text"
  }, [_vm._v("\n            " + _vm._s(_vm.detail.scanHint) + "\n        ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showShare),
      expression: "showShare"
    }],
    staticClass: "share"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1610),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.Shareshow()
      }
    }
  })])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1260)
    }
  })])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-195266d6", module.exports)
  }
}

/***/ }),

/***/ 1888:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1472);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("05b3f642", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-195266d6!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseDetail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-195266d6!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./caseDetail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 582:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1888)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1367),
  /* template */
  __webpack_require__(1754),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\page\\handPushCase\\caseDetail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] caseDetail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-195266d6", Component.options)
  } else {
    hotAPI.reload("data-v-195266d6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});