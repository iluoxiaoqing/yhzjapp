webpackJsonp([92],{

/***/ 1347:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_moren.png?v=4cc2054b";

/***/ }),

/***/ 1440:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import fixHead from "@/components/mHeader.vue";

exports.default = {
    data: function data() {
        return {
            addresseList: [],
            from: this.$route.query.from,
            dispayInNative: this.$route.query.dispayInNative
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.$route.query.appTitle == "" || this.$route.query.appTitle == undefined) {
            document.title = "选择";
        } else {
            document.title = this.$route.query.appTitle;
        }

        common.youmeng("地址选择列表", "进入地址选择列表");

        common.Ajax({
            "url": _apiConfig2.default.KY_IP + "addr/list"
        }, function (data) {
            _this.addresseList = data.data;
        });

        window.goBack = this.goBack;
    },

    methods: {
        goBack: function goBack() {

            if (this.$route.query.from == "APP") {
                if (common.isClient() == "ios") {
                    window.webkit.messageHandlers.nativeCloseCurrentPage.postMessage("关闭窗口");
                } else {
                    window.android.nativeCloseCurrentPage("关闭窗口");
                }
            } else {
                this.$router.push({
                    "path": "mall_order"
                });
            }
        },
        gotoOrder: function gotoOrder(i) {

            if (this.$route.query.from == "APP") {
                return;
            }

            this.$store.dispatch("saveAddress", i);

            this.$router.push({
                "path": "mall_order",
                "query": {
                    "from": this.$route.query.from
                }
            });
        },
        addYoumeng: function addYoumeng() {
            common.youmeng("地址选择列表", "点击添加地址辑按钮");
            this.$router.push({
                "path": "addAddress",
                "query": {
                    "dispayInNative": this.$route.query.dispayInNative
                }
            });
        },
        gotoEdit: function gotoEdit(i) {

            this.$store.dispatch("saveAddress", i);

            this.$router.push({
                "path": "editAddress",
                "query": {
                    "from": this.$route.query.from,
                    "dispayInNative": this.$route.query.dispayInNative
                }
            });

            common.youmeng("地址选择列表", "点击编辑按钮");
        }
    }
};

/***/ }),

/***/ 1605:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-9e84a1ee] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-9e84a1ee] {\n  background: #fff;\n}\n.tips_success[data-v-9e84a1ee] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-9e84a1ee] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-9e84a1ee] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-9e84a1ee],\n.fade-leave-active[data-v-9e84a1ee] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-9e84a1ee],\n.fade-leave-to[data-v-9e84a1ee] {\n  opacity: 0;\n}\n.default_button[data-v-9e84a1ee] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-9e84a1ee] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-9e84a1ee] {\n  position: relative;\n}\n.loading-tips[data-v-9e84a1ee] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.addressList[data-v-9e84a1ee] {\n  width: 100%;\n  overflow: hidden;\n  padding-top: 0.16rem;\n  padding-bottom: 1.7rem;\n}\n.addressList .item[data-v-9e84a1ee] {\n  width: 92%;\n  padding: 0.3rem 4%;\n  position: relative;\n  background: url(" + __webpack_require__(800) + ") no-repeat right 4% center #fff;\n  background-size: 0.14rem 0.24rem;\n  background: #fff;\n}\n.addressList .item[data-v-9e84a1ee]:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  top: 0;\n  background: #e5e5e5;\n  width: 96%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n      -ms-transform: scaleY(0.5);\n          transform: scaleY(0.5);\n}\n.addressList .item a[data-v-9e84a1ee] {\n  display: block;\n  overflow: hidden;\n}\n.addressList .item p[data-v-9e84a1ee] {\n  color: #3D4A5B;\n  font-size: 0.3rem;\n  margin-bottom: 0.16rem;\n  font-weight: 700;\n}\n.addressList .item span[data-v-9e84a1ee] {\n  width: 80%;\n  display: block;\n  color: #3d4a5b;\n  font-size: 0.28rem;\n  line-height: 0.45rem;\n  clear: both;\n}\n.addressList .item span em[data-v-9e84a1ee] {\n  opacity: 0.6;\n}\n.addressList .item span i[data-v-9e84a1ee] {\n  width: 0.88rem;\n  height: 0.4rem;\n  background: url(" + __webpack_require__(1347) + ") no-repeat left top;\n  background-size: 0.88rem 0.4rem;\n  display: block;\n  float: left;\n  margin-right: 0.2rem;\n  opacity: 1;\n}\n.addressList .item span.default[data-v-9e84a1ee] {\n  padding-left: 1.08rem;\n  background: url(" + __webpack_require__(1347) + ") no-repeat left top;\n  background-size: 0.88rem 0.4rem;\n}\n.addressList .item a[data-v-9e84a1ee] {\n  position: absolute;\n  width: 1.0rem;\n  height: 100%;\n  right: 0.3rem;\n  top: 0;\n  z-index: 50;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n  -webkit-justify-content: flex-end;\n     -moz-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  color: #3d4a5b;\n  opacity: 0.6;\n  text-align: right;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.28rem;\n}\n.addressList .item a[data-v-9e84a1ee]:after {\n  content: '';\n  position: absolute;\n  left: 0;\n  top: 50%;\n  background: #e5e5e5;\n  width: 1px;\n  height: 0.5rem;\n  -webkit-transform: scaleX(0.5) translateY(-50%);\n      -ms-transform: scaleX(0.5) translateY(-50%);\n          transform: scaleX(0.5) translateY(-50%);\n}\n.addressList .submit[data-v-9e84a1ee] {\n  width: 6.9rem;\n  height: 0.94rem;\n  position: fixed;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  bottom: 0.4rem;\n  z-index: 100;\n}\n.addressList .submit a[data-v-9e84a1ee] {\n  width: 100%;\n  height: 100%;\n  display: block;\n  text-align: center;\n  line-height: 0.94rem;\n}\n", ""]);

// exports


/***/ }),

/***/ 1918:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrap"
  }, [_c('div', {
    staticClass: "addressList"
  }, [_vm._l((_vm.addresseList), function(i) {
    return _c('div', {
      staticClass: "item"
    }, [(_vm.dispayInNative == 1) ? _c('div', [_c('p', [_vm._v(_vm._s(i.receivePerson) + " " + _vm._s(i.phoneNo))]), _vm._v(" "), _c('span', [(i.isDefault) ? _c('i') : _vm._e(), _vm._v("\n\t\t\t\t\t" + _vm._s(i.province) + _vm._s(i.city) + _vm._s(i.addressDetail) + "\n\t\t\t\t")])]) : _c('div', {
      on: {
        "click": function($event) {
          return _vm.gotoOrder(i)
        }
      }
    }, [_c('p', [_vm._v(_vm._s(i.receivePerson) + " " + _vm._s(i.phoneNo))]), _vm._v(" "), _c('span', [(i.isDefault) ? _c('i') : _vm._e(), _vm._v(" "), _c('em', [_vm._v(_vm._s(i.province) + _vm._s(i.city) + _vm._s(i.addressDetail))])])]), _vm._v(" "), _c('a', {
      attrs: {
        "href": "javascript:;"
      },
      on: {
        "click": function($event) {
          return _vm.gotoEdit(i)
        }
      }
    }, [_vm._v("编辑")])])
  }), _vm._v(" "), _c('div', {
    staticClass: "submit"
  }, [_c('a', {
    staticClass: "default_button",
    on: {
      "click": function($event) {
        return _vm.addYoumeng()
      }
    }
  }, [_vm._v("添加地址")])])], 2)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-9e84a1ee", module.exports)
  }
}

/***/ }),

/***/ 2066:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1605);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("79ffc446", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-9e84a1ee&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-9e84a1ee&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./addressList.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 643:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2066)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1440),
  /* template */
  __webpack_require__(1918),
  /* scopeId */
  "data-v-9e84a1ee",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\addressList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] addressList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9e84a1ee", Component.options)
  } else {
    hotAPI.reload("data-v-9e84a1ee", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 800:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_jump1.png?v=7fe25269";

/***/ })

});