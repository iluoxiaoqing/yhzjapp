webpackJsonp([73],{

/***/ 1346:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/logo_xiaoyaotuishou.png?v=44727221";

/***/ }),

/***/ 1431:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            mySwiper: null,
            detail: [],
            showDia: false,
            title: "",
            des: "",
            thumbnail: "",
            u: "",
            showShare: false,
            show1: false
        };
    },
    mounted: function mounted() {
        this.getDetail();
        window.shareSucces = this.shareSucces;
    },

    methods: {
        Shareshow: function Shareshow() {
            this.showDia = true;
            this.show1 = true;
        },
        getDetail: function getDetail() {
            var _this = this;

            var u = this.$route.query.u;
            if (u == undefined) {
                var _u = "";
                this.showShare = true;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "news/detail",
                    data: {
                        u: _u,
                        news: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.title = data.data.title;
                    _this.des = data.data.desc;
                    _this.u = data.data.u;
                    _this.thumbnail = data.data.thumbnail;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            } else {
                var _u2 = this.$route.query.u;
                this.showShare = false;
                common.Ajax({
                    url: _apiConfig2.default.KY_IP + "news/detail",
                    data: {
                        u: _u2,
                        news: this.$route.query.id
                    },
                    showLoading: false
                }, function (data) {
                    _this.detail = data.data;
                    _this.title = data.data.title;
                    _this.des = data.data.desc;
                    _this.u = data.data.u;
                    _this.thumbnail = data.data.thumbnail;
                    if (_this.detail.length > 0) {
                        _this.hasData = false;
                        _this.defaultText = "没有查到此用户";
                    } else {
                        _this.hasData = true;
                        _this.defaultText = "";
                    }
                });
            }
        },
        wechatSession: function wechatSession() {
            var thumbnail = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail;
            var shareJson = {
                "type": "wechatSession",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            var thumbnail = _apiConfig2.default.KY_IP + 'file/downloadFile?filePath=' + this.thumbnail;
            var shareJson = {
                "type": "wechatTimeline",
                "image": "",
                "title": this.title,
                "des": this.des,
                "thumbnail": "",
                "jumpUrl": window.location.href + '&u=' + this.u,
                "shareType": "url",
                "callbackName": "shareSucces"

            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShare.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShare(JSON.stringify(shareJson));
            }
        },
        shareSucces: function shareSucces() {
            this.showDia = false;
            this.show1 = false;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },
        showBottom: function showBottom() {
            this.showDia = true;
        }
    }
};

/***/ }),

/***/ 1585:
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: No PostCSS Config found in: E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\css\n    at E:\\jinkong\\xyts-app\\xyts\\node_modules\\_postcss-load-config@1.2.0@postcss-load-config\\index.js:51:26");

/***/ }),

/***/ 1701:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share.png?v=8ae7e7dd";

/***/ }),

/***/ 1898:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contain"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    staticClass: "bottomBar_line"
  }, [_c('span'), _vm._v(" "), _c('em', [_vm._v("分享至")]), _vm._v(" "), _c('span')]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatSession()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(759)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.wechatTimeline()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(758)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "detail_main"
  }, [_c('h1', [_vm._v(_vm._s(_vm.detail.title))]), _vm._v(" "), _c('div', {
    staticClass: "materialList"
  }, [_c('div', {
    staticClass: "list"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v("逍遥推手")]), _vm._v(" "), _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v(_vm._s(_vm.detail.time))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "content",
    domProps: {
      "innerHTML": _vm._s(_vm.detail.content)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "wx_erweima"
  }, [_c('div', {
    staticClass: "list"
  }, [_c('div', {
    staticClass: "listImg"
  }, [(_vm.detail.touxiang == '' || _vm.detail.touxiang == undefined) ? _c('img', {
    attrs: {
      "src": __webpack_require__(1346)
    }
  }) : _c('img', {
    attrs: {
      "src": _vm.imgUrl + 'file/downloadFile?filePath=' + _vm.detail.touxiang
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "listDeatil"
  }, [_c('b', [_vm._v(_vm._s(_vm.detail.userName))]), _vm._v(" "), _vm._m(1)])])]), _vm._v(" "), _c('div', {
    staticClass: "erweima"
  }, [_c('img', {
    attrs: {
      "src": _vm.imgUrl + 'news/shareQrCode?productCode=' + _vm.detail.pro + '&u=' + _vm.detail.u
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "erweima_text"
  }, [_vm._v("\n            " + _vm._s(_vm.detail.shareLang) + "\n        ")]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showShare),
      expression: "showShare"
    }],
    staticClass: "share"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1701),
      "alt": ""
    },
    on: {
      "click": function($event) {
        return _vm.Shareshow()
      }
    }
  })])])], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "listImg"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(1346)
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "time"
  }, [_c('i', [_vm._v("您的专属客服")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6f16d579", module.exports)
  }
}

/***/ }),

/***/ 2046:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1585);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("6a5c1aa2", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6f16d579!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./material_detail.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-6f16d579!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./material_detail.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 634:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2046)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1431),
  /* template */
  __webpack_require__(1898),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\share\\material_detail.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] material_detail.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6f16d579", Component.options)
  } else {
    hotAPI.reload("data-v-6f16d579", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 758:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_pengyouquan.png?v=52f5eba2";

/***/ }),

/***/ 759:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_share_wechat.png?v=70e9026c";

/***/ })

});