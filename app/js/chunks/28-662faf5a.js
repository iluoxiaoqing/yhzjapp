webpackJsonp([28],{

/***/ 1467:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            imgUrl: _apiConfig2.default.KY_IP,
            activityTime: '',
            activityIntroduce: '',
            activityRules: ''
        };
    },
    mounted: function mounted() {
        this.getDetail();
    },

    methods: {
        getDetail: function getDetail() {
            var _this = this;

            common.Ajax({
                url: _apiConfig2.default.KY_IP + "ptx/landingPage",
                data: {},
                showLoading: false
            }, function (data) {
                console.log("返回值", data);
                _this.activityTime = data.data.activityTime;
                _this.activityIntroduce = data.data.prodIntrod;
                _this.activityRules = data.data.activityRules;
            });
        },
        gotoShare: function gotoShare(j) {
            this.$router.push({
                "path": "share",
                "query": {
                    "product": 'PING_TOU_XIANG_NOPOS',
                    "productName": '【钱宝】招钱宝贝',
                    "channel": "qianbaoIndex"
                }
            });
            common.youmeng("推荐贷款", "点击钱宝首页");
        }
    }
};

/***/ }),

/***/ 1617:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-c64233c0] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-c64233c0] {\n  background: #fff;\n}\n.tips_success[data-v-c64233c0] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-c64233c0] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-c64233c0] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-c64233c0],\n.fade-leave-active[data-v-c64233c0] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-c64233c0],\n.fade-leave-to[data-v-c64233c0] {\n  opacity: 0;\n}\n.default_button[data-v-c64233c0] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-c64233c0] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-c64233c0] {\n  position: relative;\n}\n.loading-tips[data-v-c64233c0] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.qianbao_main[data-v-c64233c0] {\n  width: 100%;\n  background: #bed3ff;\n}\n.qianbao_main .qianbao_top1[data-v-c64233c0] {\n  width: 100%;\n  background: url(" + __webpack_require__(1740) + ") no-repeat;\n  height: 12.38rem;\n  background-size: 100% 100%;\n}\n.qianbao_main .qianbao_top1 .time_main[data-v-c64233c0] {\n  width: 80%;\n  margin: 0 10%;\n  background: url(" + __webpack_require__(1741) + ") no-repeat;\n  height: 0.6rem;\n  background-size: 100% 100%;\n  display: inline-block;\n  margin-top: 10.7rem;\n  font-size: 0.28rem;\n  color: #ffffff;\n  text-align: center;\n  line-height: 0.6rem;\n}\n.qianbao_main .title1[data-v-c64233c0] {\n  width: 70%;\n  margin: 0.1rem 15%;\n  height: 0.4rem;\n  background: url(" + __webpack_require__(1736) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.qianbao_main .intro[data-v-c64233c0] {\n  width: 96%;\n  height: auto;\n  margin: 0.4rem 2%;\n  background: url(" + __webpack_require__(1737) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.qianbao_main .intro .intro_item[data-v-c64233c0] {\n  padding: 0.4rem 0.4rem 0 ;\n  display: inline-block;\n}\n.qianbao_main .intro .intro_item .intro_list[data-v-c64233c0] {\n  font-size: 0.24rem;\n  color: #ffffff;\n  margin: 0 0.16rem;\n  line-height: 0.53rem;\n  display: inline-block;\n  width: 100%;\n  float: left;\n}\n.qianbao_main .intro .intro_item .intro_list .intro_right[data-v-c64233c0] {\n  width: 75%;\n  float: left;\n}\n.qianbao_main .intro .intro_item .intro_list .intro_left[data-v-c64233c0] {\n  width: 25%;\n  float: left;\n}\n.qianbao_main .intro .intro_item .intro_list .intro_left img[data-v-c64233c0] {\n  width: 0.17rem;\n  height: 0.17rem;\n  display: inline-block;\n}\n.qianbao_main .intro .intro_item .intro_list .intro_left span[data-v-c64233c0] {\n  font-weight: 200;\n}\n.qianbao_main .intro p[data-v-c64233c0] {\n  font-size: 0.24rem;\n  color: #ffffff;\n  line-height: 0.53rem;\n}\n.qianbao_main .intro p i[data-v-c64233c0] {\n  width: 0.17rem;\n  height: 0.17rem;\n  display: inline-block;\n  background-size: 100% 100%;\n  background: url(" + __webpack_require__(826) + ") no-repeat;\n}\n.qianbao_main .title2[data-v-c64233c0] {\n  width: 70%;\n  margin: 0.6rem 15% 0.4rem;\n  height: 0.4rem;\n  background: url(" + __webpack_require__(1738) + ") no-repeat;\n  background-size: 100% 100%;\n}\n.qianbao_main button[data-v-c64233c0] {\n  width: 96%;\n  height: 0.9rem;\n  margin: 0 2% 0;\n  background: url(" + __webpack_require__(1739) + ") no-repeat;\n  background-size: 100% 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ 1736:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbao02.png?v=ba2ffbd4";

/***/ }),

/***/ 1737:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbao03.png?v=1880a39c";

/***/ }),

/***/ 1738:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbao04.png?v=51af8f8b";

/***/ }),

/***/ 1739:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbaoButton.png?v=32117e1e";

/***/ }),

/***/ 1740:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbao_01.jpg?v=9ee072da";

/***/ }),

/***/ 1741:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbaotime.png?v=7ad281b9";

/***/ }),

/***/ 1930:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "qianbao_main"
  }, [_c('div', {
    staticClass: "qianbao_top1"
  }, [_c('div', {
    staticClass: "time_main"
  }, [_vm._v("\r\n                        活动时间：" + _vm._s(_vm.activityTime) + "\r\n                ")])]), _vm._v(" "), _c('div', {
    staticClass: "title1"
  }), _vm._v(" "), _c('div', {
    staticClass: "intro"
  }, [_c('div', {
    staticClass: "intro_item"
  }, [_c('div', {
    staticClass: "intro_list"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityIntroduce.company))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, _vm._l((_vm.activityIntroduce.cardRate), function(i, index) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, _vm._l((_vm.activityIntroduce.commRules), function(i, index) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(" " + _vm._s(_vm.activityIntroduce.characteristic))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityIntroduce.settleRule))])])])])]), _vm._v(" "), _c('div', {
    staticClass: "title2"
  }), _vm._v(" "), _c('div', {
    staticClass: "intro"
  }, [_c('div', {
    staticClass: "intro_item"
  }, [_c('div', {
    staticClass: "intro_list"
  }, [_vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityRules.actTime))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityRules.actObject))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityRules.standard))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(8), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(" " + _vm._s(_vm.activityRules.rewardTarget))])])]), _vm._v(" "), _c('div', {
    staticClass: "intro_list"
  }, [_vm._m(9), _vm._v(" "), _c('div', {
    staticClass: "intro_right"
  }, [_c('p', [_vm._v(_vm._s(_vm.activityRules.otherRules))])])])])]), _vm._v(" "), _c('button', {
    on: {
      "click": function($event) {
        return _vm.gotoShare()
      }
    }
  })])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("支付公司：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("刷卡费率：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("分润规则：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("产品特点：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("结算规则：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("活动时间：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("活动对象：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("达标条件：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("奖励对象：")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "intro_left"
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(826),
      "alt": ""
    }
  }), _vm._v(" "), _c('span', [_vm._v("其他规则：")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-c64233c0", module.exports)
  }
}

/***/ }),

/***/ 2078:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1617);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("9eb2c5e8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-c64233c0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./qianbaoIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-c64233c0&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./qianbaoIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 673:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(2078)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1467),
  /* template */
  __webpack_require__(1930),
  /* scopeId */
  "data-v-c64233c0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\qianbaoIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] qianbaoIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c64233c0", Component.options)
  } else {
    hotAPI.reload("data-v-c64233c0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 826:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/qianbaoxuehua.png?v=adf03760";

/***/ })

});