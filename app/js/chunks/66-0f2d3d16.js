webpackJsonp([66],{

/***/ 1468:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _apiConfig = __webpack_require__(11);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(7);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            height: 0,
            listData: ""
        };
    },
    mounted: function mounted() {
        common.setCookie("proCode", '盛付通MPOS');
        this.getList();
    },

    methods: {
        go: function go() {
            window.location.href = _apiConfig2.default.WEB_URL + 'mall';
        },
        getList: function getList() {
            var _this = this;

            common.Ajax({
                "url": _apiConfig2.default.KY_IP + "sft/showPopMessage",
                "data": {}
            }, function (data) {
                _this.listData = data.data;
            });
        }
    }
};

/***/ }),

/***/ 1501:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-0ea4ceec] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-0ea4ceec] {\n  background: #fff;\n}\n.tips_success[data-v-0ea4ceec] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-0ea4ceec] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-0ea4ceec] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-0ea4ceec],\n.fade-leave-active[data-v-0ea4ceec] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-0ea4ceec],\n.fade-leave-to[data-v-0ea4ceec] {\n  opacity: 0;\n}\n.default_button[data-v-0ea4ceec] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-0ea4ceec] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-0ea4ceec] {\n  position: relative;\n}\n.loading-tips[data-v-0ea4ceec] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.sft[data-v-0ea4ceec] {\n  width: 100%;\n  background-color: #4C38D8;\n}\n.sft .sftTop[data-v-0ea4ceec] {\n  width: 100%;\n  background: #4C38D8;\n}\n.sft .sftTop .mainBg img[data-v-0ea4ceec] {\n  width: 100%;\n  margin-bottom: -0.8rem;\n}\n.sft .sftTop .mainBg .time_main[data-v-0ea4ceec] {\n  width: 100%;\n  text-align: center;\n  font-size: .28rem;\n  color: #ffffff;\n  font-weight: 500;\n  position: absolute;\n  top: 6rem;\n}\n.sft .sftTop .mainBg .sftBottom[data-v-0ea4ceec] {\n  width: 100%;\n  margin-top: -1.3rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain[data-v-0ea4ceec] {\n  width: 100%;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_title[data-v-0ea4ceec] {\n  width: 2.68rem;\n  height: 0.93rem;\n  background: url(" + __webpack_require__(1743) + ") no-repeat;\n  position: relative;\n  top: 50%;\n  left: 50%;\n  margin-top: -0.8rem;\n  margin-left: -1.25rem;\n  font-size: .32rem;\n  background-size: 100% 100%;\n  font-weight: bold;\n  color: #ffffff;\n  text-align: center;\n  line-height: 0.8rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_title1[data-v-0ea4ceec] {\n  width: 2.68rem;\n  height: 0.93rem;\n  background: url(" + __webpack_require__(1744) + ") no-repeat;\n  position: relative;\n  top: 50%;\n  left: 50%;\n  margin-top: -0.8rem;\n  margin-left: -1.25rem;\n  font-size: .32rem;\n  background-size: 100% 100%;\n  font-weight: bold;\n  color: #ffffff;\n  text-align: center;\n  line-height: 0.8rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom[data-v-0ea4ceec] {\n  width: 92%;\n  margin: 0 4%;\n  background: url(" + __webpack_require__(1742) + ") no-repeat;\n  background-size: 100% 100%;\n  border-radius: 0.36rem;\n  margin-top: -0.61rem;\n  display: inline-block;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact[data-v-0ea4ceec] {\n  display: inline-block;\n  float: left;\n  width: 92%;\n  margin: 0 5%;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_left[data-v-0ea4ceec] {\n  font-size: 0.24rem;\n  font-weight: bold;\n  width: 20%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_left .leftLogo[data-v-0ea4ceec] {\n  width: 0.05rem;\n  height: 0.23rem;\n  background: #FFDB00;\n  border-radius: 0.03rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_right[data-v-0ea4ceec] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 76%;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_right p[data-v-0ea4ceec] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .remark[data-v-0ea4ceec] {\n  font-weight: 500;\n  color: #F17C41;\n  font-size: 0.2rem;\n  white-space: nowrap;\n  display: inline-block;\n  float: left;\n  margin-left: 0.17rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_left1[data-v-0ea4ceec] {\n  font-size: 0.24rem;\n  width: 27.5%;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  text-align: center;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_left1 .leftLogo[data-v-0ea4ceec] {\n  width: 0.05rem;\n  height: 0.23rem;\n  background: #FFDB00;\n  border-radius: 0.03rem;\n  position: relative;\n  top: 0.03rem;\n  display: inline-block;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_right1[data-v-0ea4ceec] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n  line-height: 0.44rem;\n  margin-top: 0.1rem;\n  display: inline-block;\n  float: left;\n  margin-left: 0.1rem;\n  margin-right: 0.1rem;\n  width: 65%;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact .contact_right1 p[data-v-0ea4ceec] {\n  font-size: .24rem;\n  font-weight: 500;\n  color: #E4E0FF;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact[data-v-0ea4ceec]:nth-of-type(1) {\n  margin-top: 0.5rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain .contant_bottom .contact[data-v-0ea4ceec]:last-child {\n  margin-bottom: 0.5rem;\n}\n.sft .sftTop .mainBg .sftBottom .contantMain2[data-v-0ea4ceec] {\n  margin-top: 0.8rem !important;\n}\n.sft .sftTop .mainBg .sftBottom .button[data-v-0ea4ceec] {\n  width: 100%;\n  height: 1rem;\n  line-height: 1rem;\n  text-align: center;\n  background: #7D6CFA;\n  font-size: 0.4rem;\n  font-weight: bold;\n  color: #FFFFFF;\n  display: inline-block;\n  float: left;\n}\n", ""]);

// exports


/***/ }),

/***/ 1742:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/r_1.png?v=d44cd3a8";

/***/ }),

/***/ 1743:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/t_1.png?v=82842295";

/***/ }),

/***/ 1744:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/t_2.png?v=3c4aa21d";

/***/ }),

/***/ 1745:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/top.png?v=8dba4f03";

/***/ }),

/***/ 1815:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "sft"
  }, [_c('div', {
    staticClass: "sftTop"
  }, [_c('div', {
    staticClass: "mainBg"
  }, [_c('div', {
    staticClass: "time_main"
  }, [_vm._v("\r\n                                活动时间：" + _vm._s(_vm.listData.activityTime) + "\r\n                        ")]), _vm._v(" "), _c('img', {
    attrs: {
      "src": __webpack_require__(1745),
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "sftBottom"
  }, [_c('div', {
    staticClass: "contantMain"
  }, [_c('div', {
    staticClass: "contant_title"
  }), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.paymentCompany) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.paymentCompany) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.cardRate) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.cardRate), function(item) {
    return _c('p', [_vm._v(_vm._s(item))])
  }), 0)]) : _vm._e(), _vm._v(" "), (_vm.listData.commRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.commRule) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.productFeatures) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.productFeatures) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.settleRule) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.settleRule) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "contantMain contantMain2"
  }, [_c('div', {
    staticClass: "contant_title1"
  }), _vm._v(" "), _c('div', {
    staticClass: "contant_bottom"
  }, [(_vm.listData.posPrice) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "contact_right1"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.posPrice) + "\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.actRecurrence) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, _vm._l((_vm.listData.actRecurrence), function(i) {
    return _c('p', [_vm._v(_vm._s(i))])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "remark"
  }, [_vm._v("\r\n                                                        备注：首刷扣除手续费及39元服务费后，不足99的，D+1到账\r\n                                        ")])]) : _vm._e(), _vm._v(" "), (_vm.listData.platformStatement) ? _c('div', {
    staticClass: "contact"
  }, [_vm._m(7), _vm._v(" "), _c('div', {
    staticClass: "contact_right"
  }, [_vm._v("\r\n                                                " + _vm._s(_vm.listData.platformStatement) + "\r\n                                        ")])]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "button",
    on: {
      "click": function($event) {
        return _vm.go()
      }
    }
  }, [_vm._v("\r\n                立即邀请\r\n                ")])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                支付公司:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                刷卡费率:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                分润规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                产品特点:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                结算规则:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left1"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                机具采购价格:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                激活返现:\r\n                                        ")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact_left"
  }, [_c('div', {
    staticClass: "leftLogo"
  }), _vm._v("\r\n                                                平台申明:\r\n                                        ")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0ea4ceec", module.exports)
  }
}

/***/ }),

/***/ 1962:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1501);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(4)("24b27b68", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ea4ceec&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./sftIndex.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0ea4ceec&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./sftIndex.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 674:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(1962)

var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(1468),
  /* template */
  __webpack_require__(1815),
  /* scopeId */
  "data-v-0ea4ceec",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\xyts-app\\xyts\\static\\page\\shop\\sftIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] sftIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0ea4ceec", Component.options)
  } else {
    hotAPI.reload("data-v-0ea4ceec", Component.options)
  }
})()}

module.exports = Component.exports


/***/ })

});