webpackJsonp([22],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.commonResetDate = exports.validator = exports.clearCookie = exports.getCookie = exports.setCookie = exports.Jsonp = exports.XNAjax = exports.AjaxToken = exports.decryptpByRSA = exports.encryptByRSA = exports.decryptByAES = exports.encryptByAES = exports.AjaxMerchant = exports.Ajax = exports.getUserNo = exports.getLoginKey = exports.filterNull = exports.getUrlParam = exports.isClient = exports.loading = exports.useMd5 = exports.jsonToArray = exports.alertBox = exports.toast = exports.trim = exports.timestampToTime = exports.siblingElem = exports.isWeiXin = exports.youmeng = exports.addScroll = exports.preventScroll = exports.bodyScroll = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _md = __webpack_require__(177);

var _md2 = _interopRequireDefault(_md);

var _base = __webpack_require__(125);

var _base2 = _interopRequireDefault(_base);

var _cryptoJs = __webpack_require__(146);

var _cryptoJs2 = _interopRequireDefault(_cryptoJs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import JSEncrypt from 'jsencrypt';

//是否走网关
var isWay = _apiConfig2.default.isWay;

var bodyScroll = exports.bodyScroll = function bodyScroll(event) {
    event.preventDefault();
};

var preventScroll = exports.preventScroll = function preventScroll() {
    document.body.addEventListener('touchmove', bodyScroll, false);
    document.body.style.cssText = "position:fixed";
};

var addScroll = exports.addScroll = function addScroll() {
    document.body.removeEventListener('touchmove', bodyScroll, false);
    document.body.style.cssText = "position:initial";
};

var youmeng = exports.youmeng = function youmeng(id, text) {
    try {
        _czc.push(["_trackEvent", id, text]);
        console.log(id + "|" + text);
    } catch (e) {
        console.log(e);
    }
};

var isWeiXin = exports.isWeiXin = function isWeiXin() {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        return true;
    } else {
        return false;
    }
};

var siblingElem = exports.siblingElem = function siblingElem(elem) {
    var _nodes = [],
        _elem = elem;
    while (elem = elem.previousSibling) {
        if (elem.nodeType === 1) {
            _nodes.push(elem);
            break;
        }
    }

    elem = _elem;
    while (elem = elem.nextSibling) {
        if (elem.nodeType === 1) {
            _nodes.push(elem);
            break;
        }
    }

    return _nodes;
};

var timestampToTime = exports.timestampToTime = function timestampToTime(timestamp) {
    var date = new Date(timestamp);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y + M + D + h + m + s;
};

var trim = exports.trim = function trim(str) {
    return str.replace(/\s*/g, '');
};

var toast = exports.toast = function toast(params) {
    var Height = document.documentElement.clientHeight || document.body.clientHeight;

    var toastMask = document.createElement("div");
    var el = document.createElement("div");

    toastMask.setAttribute("class", "toastMask");
    el.setAttribute("id", "toast");
    toastMask.appendChild(el);

    el.innerHTML = params.content;

    document.body.appendChild(toastMask);
    // document.body.style.height = Height + "px";

    toastMask.classList.add("fadeIn");
    setTimeout(function () {
        toastMask.classList.remove("fadeIn");
        toastMask.classList.add("fadeOut");
        //el.addEventListener("webkitAnimationEnd", function(){
        toastMask.classList.add("hide");
        document.body.removeChild(toastMask);
        // document.body.style.height = "";
        //});
    }, params.time || 2000);
};

var alertBox = exports.alertBox = function alertBox() {
    var oWrap = document.getElementsByClassName("wrap")[0];
    var mask = document.createElement("div");
    var box = document.createElement("div");

    mask.className = "alertBox_mask";
    box.className = "alertBox";
    // oWrap.removeChild(mask);
    // oWrap.removeChild(box);

    oWrap.appendChild(mask);
    oWrap.appendChild(box);
};

var jsonToArray = exports.jsonToArray = function jsonToArray(json) {

    var arr = [];
    for (var i in json) {

        var baseType = "";
        var toString = "";

        try {
            baseType = JSON.parse(json[i]);
        } catch (e) {
            baseType = json[i];
        }

        if ((typeof baseType === "undefined" ? "undefined" : _typeof(baseType)) == "object") {
            toString = '{"' + i + '":' + json[i] + '}';
        } else {
            toString = '{"' + i + '":"' + json[i] + '"}';
        }

        var toJson = JSON.parse(toString);
        arr.push(toJson);
    }

    return arr;
};

var useMd5 = exports.useMd5 = function useMd5(array) {

    var signKey = '86eb1492-6e08-481d-b377-678acd5c3de5';

    var resultString = "";

    if (array.length > 0) {

        var parmas = [];
        var resultArray = [];

        for (var i = 0; i < array.length; i++) {
            for (var key in array[i]) {
                parmas.push(key);
            }
        }

        for (var _i = 0; _i < parmas.sort(function (a, b) {
            if (a.toString().toLowerCase() > b.toString().toLowerCase()) {
                return 1;
            } else {
                return -1;
            }
        }).length; _i++) {
            for (var j = 0; j < array.length; j++) {

                //console.log(array[j])

                for (var _key in array[j]) {
                    if (parmas[_i] == _key) {

                        if (Array.isArray(array[j][_key])) {
                            array[j][_key] = JSON.stringify(array[j][_key]);
                        }
                        resultArray.push(array[j]);
                    }
                }
            }
        }

        resultArray.map(function (el, index) {
            for (var _key2 in el) {
                resultString += _key2 + "=" + el[_key2];
            }
        });
    }

    return (0, _md2.default)(resultString + signKey);
};

var loading = exports.loading = function loading(state, text) {

    var domLength = document.getElementsByClassName('loadingBg').length;
    var loadingBg = document.createElement("div");
    var loading = document.createElement("div");
    var img = document.createElement("img");
    var p = document.createElement("p");

    loadingBg.className = "loadingBg";
    loading.className = "loading";
    img.src = "image/icon_loading.png";
    p.innerHTML = text || "数据加载中";

    loading.appendChild(img);
    loading.appendChild(p);
    loadingBg.appendChild(loading);
    if (domLength == 0) {
        document.body.appendChild(loadingBg);
    }

    if (state == "show") {
        document.getElementsByClassName('loadingBg')[0].style.display = "block";
    } else if (state == "hide") {

        //console.log( loadingBg )

        document.getElementsByClassName('loadingBg')[0].style.display = "none";
        // document.removeEventListener("touchmove",function(e){
        //     //e.preventDefault();
        // });
    }
};

var isClient = exports.isClient = function isClient() {
    var u = navigator.userAgent;
    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端;

    if (isAndroid) {
        return "android";
    } else if (isiOS) {
        return "ios";
    }
};

var getUrlParam = exports.getUrlParam = function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substring(1).match(reg); //匹配目标参数
    if (r !== null) return decodeURI(r[2]);
    return ''; //返回参数值
};

// 参数过滤函数
var filterNull = exports.filterNull = function filterNull(o) {
    for (var key in o) {
        if (o[key] === null) {
            delete o[key];
        }
        if (toType(o[key]) === 'string') {
            o[key] = o[key].trim();
        } else if (toType(o[key]) === 'object') {
            o[key] = filterNull(o[key]);
        } else if (toType(o[key]) === 'array') {
            o[key] = filterNull(o[key]);
        }
    }
    return o;
};

var getLoginKey = exports.getLoginKey = function getLoginKey() {

    var nativeJson = {};
    var loginKey = "";

    if (isClient() == "ios") {
        try {
            loginKey = window.webkit.messageHandlers.getLoginKey.postMessage("获取loginkey");
            console.log("调用了getLoginkey:" + loginKey);
        } catch (e) {
            loginKey = getCookie("LOGINKEY");
        }
        // loginKey = getCookie("LOGINKEY");
    } else {
        try {
            nativeJson = JSON.parse(window.android.getLoginKey());
            loginKey = nativeJson.loginKey;
            console.log("调用了getLoginkey:" + loginKey);
        } catch (e) {
            loginKey = getCookie("LOGINKEY");
        }
    }

    return loginKey || "";
};

var getUserNo = exports.getUserNo = function getUserNo() {

    var userNo = "";

    if (isClient() == "ios") {

        userNo = getCookie("USERNO");
    } else {
        try {
            var nativeJson = JSON.parse(window.android.getLoginKey());
            userNo = nativeJson.userNo;
            console.log("调用了userNo:" + userNo);
        } catch (e) {
            userNo = getCookie("USERNO");
        }
    }

    return userNo || "";
};

var Ajax = exports.Ajax = function Ajax(options, success, failed) {

    var osType = isClient() == "ios" ? "IOS" : "ANDROID";

    // setCookie("DEVICETYPE","1");
    // setCookie("APPCODE","HM_PARTNER");
    // setCookie("VERSION","1.0.0");
    //setCookie("LOGINKEY","00047c5dd5c1323e727d8a5ac0fe5b41");

    var defaults = {
        url: '',
        type: 'POST',
        dataType: 'json',
        responseType: 'json',
        data: {},
        showLoading: true
    };

    options = Object.assign({}, defaults, options);

    var dataJson = Object.assign({
        "osType": osType,
        "deviceType": getCookie("DEVICETYPE") || "test",
        "appCode": getCookie("APPCODE") || "test",
        "version": getCookie("VERSION") || "1.0.0",
        "loginKey": getLoginKey()
    }, options.data);

    var signData = { "sign": useMd5(jsonToArray(dataJson)) };
    var ajaxData = Object.assign({}, dataJson, signData);

    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }

    var type = options.type.toUpperCase();
    // 用于清除缓存
    var random = Math.random();

    if ((typeof ajaxData === "undefined" ? "undefined" : _typeof(ajaxData)) == 'object') {
        var str = '';
        for (var key in ajaxData) {
            str += key + '=' + ajaxData[key] + '&';
        }
        ajaxData = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (ajaxData) {
            xhr.open('GET', options.url + '?' + ajaxData, options.sync === false ? false : true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, options.sync === false ? false : true);
        }
        xhr.send();
        if (options.showLoading) {
            loading("show");
        }
        // loading("show");
    } else if (type == 'POST') {
        xhr.open('POST', options.url, options.sync === false ? false : true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.setRequestHeader("traditional", true);
        xhr.traditional = true;

        xhr.send(ajaxData);

        if (options.showLoading) {
            loading("show");
        }
    }

    // 处理返回数据
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                if (options.showLoading) {
                    loading("hide");
                }
                var res = JSON.parse(xhr.response);
                // console.log( res )
                if (res.code == "0000") {
                    if (typeof success == "function") {
                        success(res);
                    }
                } else {
                    console.log("222222222", res.code);
                    toast({
                        "content": res.msg
                    });
                    if (typeof failed == "function") {
                        failed(res);
                    }
                }
            } else {
                if (options.showLoading) {
                    loading("hide");
                }
                toast({
                    "content": "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function () {
        if (options.showLoading) {
            loading("hide");
        }
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    };
};

var AjaxMerchant = exports.AjaxMerchant = function AjaxMerchant(options, success, failed) {

    var osType = isClient() == "ios" ? "IOS" : "ANDROID";

    // setCookie("DEVICETYPE","1");
    // setCookie("APPCODE","HM_PARTNER");
    // setCookie("VERSION","1.0.0");
    //setCookie("LOGINKEY","00047c5dd5c1323e727d8a5ac0fe5b41");

    var defaults = {
        url: '',
        type: 'POST',
        dataType: 'json',
        responseType: 'json',
        data: {},
        showLoading: true
    };

    options = Object.assign({}, defaults, options);

    var dataJson = Object.assign({
        "osType": osType,
        "deviceType": getCookie("DEVICETYPE") || "test",
        "appCode": getCookie("APPCODE") || "test",
        "version": getCookie("VERSION") || "1.0.0",
        "loginKey": getLoginKey()
    }, options.data);

    var signData = { "sign": useMd5(jsonToArray(dataJson)) };
    var ajaxData = Object.assign({}, dataJson, signData);

    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }

    var type = options.type.toUpperCase();
    // 用于清除缓存
    var random = Math.random();

    if ((typeof ajaxData === "undefined" ? "undefined" : _typeof(ajaxData)) == 'object') {
        var str = '';
        for (var key in ajaxData) {
            str += key + '=' + ajaxData[key] + '&';
        }
        ajaxData = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (ajaxData) {
            xhr.open('GET', options.url + '?' + ajaxData, options.sync === false ? false : true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, options.sync === false ? false : true);
        }
        xhr.send();
        if (options.showLoading) {
            loading("show");
        }
        // loading("show");
    } else if (type == 'POST') {
        xhr.open('POST', options.url, options.sync === false ? false : true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.setRequestHeader("traditional", true);
        xhr.traditional = true;

        xhr.send(ajaxData);

        if (options.showLoading) {
            loading("show");
        }
    }

    // 处理返回数据
    xhr.onreadystatechange = function () {

        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                if (options.showLoading) {
                    loading("hide");
                }
                var res = JSON.parse(xhr.response);
                // return res;
                // console.log("res====", res)
                // if (res.code == "0000") {
                if (typeof success == "function") {
                    success(res);
                }
                // } else {
                //     console.log("222222222", res.code);
                //     toast({
                //         "content": res.msg
                //     });
                //     if (typeof failed == "function") {
                //         failed(res);
                //     }
                // }
            } else {
                if (options.showLoading) {
                    loading("hide");
                }
                toast({
                    "content": "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function () {
        if (options.showLoading) {
            loading("hide");
        }
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    };
};

//DES 加密
var encryptByAES = exports.encryptByAES = function encryptByAES(ciphertext, key) {
    var keyHex = _cryptoJs2.default.enc.Hex.parse(key);
    var encrypted = _cryptoJs2.default.AES.encrypt(ciphertext, keyHex, {
        mode: _cryptoJs2.default.mode.ECB,
        padding: _cryptoJs2.default.pad.Pkcs7
    });
    return encrypted.ciphertext.toString().toUpperCase();
};
//AES 解密
var decryptByAES = exports.decryptByAES = function decryptByAES(ciphertext, key) {
    var keyHex = _cryptoJs2.default.enc.Hex.parse(key);
    // direct decrypt ciphertext
    var decrypted = _cryptoJs2.default.AES.decrypt({
        ciphertext: _cryptoJs2.default.enc.Hex.parse(ciphertext)
    }, keyHex, {
        mode: _cryptoJs2.default.mode.ECB,
        padding: _cryptoJs2.default.pad.Pkcs7
    });
    return decrypted.toString(_cryptoJs2.default.enc.Utf8);
};

//公钥加密
var encryptByRSA = exports.encryptByRSA = function encryptByRSA(source, key) {
    var encrypt = new JSEncrypt();
    // key=publick_key;
    encrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + hex2b64(key) + '-----END PUBLIC KEY-----');
    // console.log("hex2b64(key):::"+hex2b64(key));
    var result = encrypt.encrypt(source);
    result = b64tohex(result);
    // console.log('加密结果######################',result);
    return result;
};

//公钥解密
var decryptpByRSA = exports.decryptpByRSA = function decryptpByRSA(encrypted, key) {
    // console.log("公钥解密...");
    // debugger
    var decrypt = new JSEncrypt();
    decrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + hex2b64(key) + '-----END PUBLIC KEY-----');
    var str = decrypt.decryptp(encrypted);
    // console.log('解密结果:::',str);
    return str;
};

//这个方法纯为获取token使用的
var AjaxToken = exports.AjaxToken = function AjaxToken(options, success) {
    var defaults = {
        url: '',
        type: 'POST',
        data: {},
        ContentType: "application/x-www-form-urlencoded"
    };
    options = Object.assign({}, defaults, options);
    var dataJson = {};
    var encryptMsg = "";

    // 创建ajax对象
    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }

    var type = options.type.toUpperCase();
    // 用于清除缓存
    var random = Math.random();
    dataJson = options.data;
    if ((typeof dataJson === "undefined" ? "undefined" : _typeof(dataJson)) == 'object') {
        var str = '';
        for (var key in dataJson) {
            str += key + '=' + encodeURIComponent(dataJson[key]) + '&';
        }
        dataJson = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (dataJson) {
            xhr.open('GET', options.url + '?' + dataJson, true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, true);
        }

        xhr.setRequestHeader("Content-type", options.ContentType);

        xhr.send();
        loading("show");
    } else if (type == 'POST') {
        xhr.open('POST', options.url, true);
        xhr.setRequestHeader("Content-type", options.ContentType);
        xhr.send(dataJson);
        loading("show");
    }

    // 处理返回数据
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {

                loading("hide");
                var res = JSON.parse(xhr.response);
                if (typeof success == "function") {
                    success(res);
                }
            } else {
                if (xhr.status == 400) {
                    var _res = JSON.parse(xhr.response);
                    if (_res.error_description) {
                        var content = getwayErrorMsg(_res.error_description);
                        loading("hide");
                        toast({
                            content: content
                        });
                    } else {
                        loading("hide");
                        toast({
                            content: "服务器开小差啦~"
                        });
                    }
                } else {

                    loading("hide");
                    toast({
                        content: "服务器开小差啦~"
                    });
                }
            }
        }
    };

    xhr.ontimeout = function () {
        loading("hide");
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    };
};

function getwayErrorMsg(code) {
    switch (code) {
        case "0001":
            return "未登录";
        case "0002":
            return "未绑定手机号";
        case "0003":
            return "验证码错误";
        case "0004":
            return "手机号已被绑定";
        case "0005":
            return "参数错误";
        case "0006":
            return "手机号格式错误";
        case "0007":
            return "接入方未绑定";
        case "0008":
            return "已绑定";
        case "0009":
            return "集团接口异常";
        case "9999":
            return "请求失败";
        default:
            return "未知错误";
    }
}

//犀牛ajax
var XNAjax = exports.XNAjax = function XNAjax(options, success, failed) {

    var access_token = "";
    var aesKey = "";
    var uuid = "";
    // if(isClient()=="ios"){
    var sesUUid = sessionStorage.getItem("UUID");
    if (options.sesstionType !== "true") {
        uuid = getCookie("UUID");
        access_token = getCookie("ACCESSTOKEN");
        aesKey = getCookie("AESKEY");
    } else {
        uuid = sessionStorage.getItem("UUID");
        access_token = sessionStorage.getItem("ACCESSTOKEN");
        aesKey = sessionStorage.getItem("AESKEY");
    }
    console.log("access_token:" + access_token + "| aesKey:" + aesKey);
    var defaults = {
        url: '',
        type: 'POST',
        data: {},
        ContentType: "application/x-www-form-urlencoded"
    };
    options = Object.assign({}, defaults, options);
    var dataJson = {};
    var encryptMsg = "";
    console.log(options.url);
    // 创建ajax对象
    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }
    var type = options.type.toUpperCase();
    // 用于清除缓存
    var random = Math.random();

    if (isWay) {
        //是否走网关
        encryptMsg = encryptByAES(JSON.stringify(options.data), aesKey);
        dataJson = Object.assign({}, options.data, {
            "access_token": access_token,
            "uuid": uuid,
            "msg": encodeURIComponent(encryptMsg)
        });
    } else {
        dataJson = Object.assign({}, options.data, {
            "uuid": uuid
        });
    }
    if ((typeof dataJson === "undefined" ? "undefined" : _typeof(dataJson)) == 'object') {
        var str = '';
        for (var key in dataJson) {
            str += key + '=' + dataJson[key] + '&';
        }
        dataJson = str.replace(/&$/, '');
    }
    if (type === 'GET') {
        if (dataJson) {
            xhr.open('GET', options.url + (options.url.indexOf('?') > -1 ? '&' : '?') + dataJson, true);
        } else {
            xhr.open('GET', options.url + (options.url.indexOf('?') > -1 ? '&' : '?') + 't=' + random, true);
        }
        xhr.setRequestHeader("Content-type", options.ContentType);
        xhr.send();

        loading("show");
    } else if (type === 'POST') {
        console.log('333');
        console.log(dataJson);
        xhr.open('POST', options.url, true);
        xhr.setRequestHeader("Content-type", options.ContentType);
        console.log(dataJson);
        xhr.send(dataJson);
        loading("show");
    }
    // 处理返回数据
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                loading("hide");
                var res = {};
                if (isWay) {
                    //是否走网关
                    res = JSON.parse(decryptByAES(xhr.response, aesKey));
                } else {
                    res = JSON.parse(xhr.response);
                }
                // let res = JSON.parse(decryptByAES(xhr.response, aesKey));
                console.log("ajax返回", res);
                if (res.code == "0000") {
                    if (typeof success == "function") {
                        success(res);
                    }
                } else {
                    toast({
                        "content": res.msg
                    });
                    if (typeof failed == "function") {
                        failed(res);
                    }
                }
            } else {
                loading("hide");
                toast({
                    content: "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function () {
        loading("hide");
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    };
};

var Jsonp = exports.Jsonp = function Jsonp(url, config) {

    var data = config.data || [];
    var paraArr = [],
        paraString = ''; //get请求的参数。
    var urlArr;
    var callbackName; //每个回调函数一个名字。按时间戳。
    var script, head; //要生成script标签。head标签。
    var supportLoad; //是否支持 onload。是针对IE的兼容处理。
    var onEvent; //onload或onreadystatechange事件。
    var timeout = config.timeout || 0; //超时功能。

    for (var i in data) {
        if (data.hasOwnProperty(i)) {
            paraArr.push(encodeURIComponent(i) + "=" + encodeURIComponent(data[i]));
        }
    }

    urlArr = url.split("?"); //链接中原有的参数。
    if (urlArr.length > 1) {
        paraArr.push(urlArr[1]);
    }

    callbackName = 'callback';
    paraArr.push('callback=' + callbackName);
    paraString = paraArr.join("&");
    url = urlArr[0] + "?" + paraString;

    script = document.createElement("script");
    script.loaded = false;

    //将回调函数添加到全局。
    window[callbackName] = function (arg) {
        var callback = config.callback;
        callback(arg);
        script.loaded = true;
    };

    head = document.getElementsByTagName("head")[0];
    head.insertBefore(script, head.firstChild); //chrome下第二个参数不能为null
    script.src = url;

    supportLoad = "onload" in script;
    onEvent = supportLoad ? "onload" : "onreadystatechange";

    script[onEvent] = function () {

        if (script.readyState && script.readyState != "loaded") {
            return;
        }
        if (script.readyState == 'loaded' && script.loaded == false) {
            script.onerror();
            return;
        }
        //删除节点。
        script.parentNode && script.parentNode.removeChild(script) && head.removeNode && head.removeNode(this);
        script = script[onEvent] = script.onerror = window[callbackName] = null;
    };

    script.onerror = function () {
        if (window[callbackName] == null) {
            console.log("请求超时，请重试！");
        }
        config.error && config.error(); //如果有专门的error方法的话，就调用。
        script.parentNode && script.parentNode.removeChild(script) && head.removeNode && head.removeNode(this);
        script = script[onEvent] = script.onerror = window[callbackName] = null;
    };

    if (timeout != 0) {
        setTimeout(function () {
            if (script && script.loaded == false) {
                window[callbackName] = null; //超时，且未加载结束，注销函数
                script.onerror();
            }
        }, timeout);
    }
};

var setCookie = exports.setCookie = function setCookie(name, value, Days) {
    if (Days === null || Days === '') {
        Days = 30;
    }
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + "; path=/;expires=" + exp.toGMTString();
};

var getCookie = exports.getCookie = function getCookie(name) {
    var arr,
        reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return unescape(arr[2]);
    } else {
        return null;
    }
};

//让首写字母大写
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * 批量返回验证方法 @heliang
 */
function generatorValidator() {
    var validator = {};

    var _loop = function _loop(key) {
        if (_typeof(_base2.default[key]) === 'object' && _base2.default[key].reg != null) {
            validator['check' + capitalize(key)] = function (str) {
                var errorMsg = "";
                if (str === '') {
                    errorMsg = _base2.default[key].empty;
                    return errorMsg;
                } else {
                    var reg = _base2.default[key].reg;
                    if (!reg.test(str)) {
                        errorMsg = _base2.default[key].error;
                        return errorMsg;
                    } else {
                        return errorMsg;
                    }
                }
            };
        }
    };

    for (var key in _base2.default) {
        _loop(key);
    }
    return validator;
}

var clearCookie = exports.clearCookie = function clearCookie(name) {
    setCookie(name, '', -1);
};

var validator = generatorValidator();

// console.log(validator);
exports.validator = validator;

/**
 * 时间戳转时间
 * @param date Thu Nov 21 2019 14:57:05 GMT+0800 (中国标准时间)
 * @param fmt 格式：yyyy-MM-dd hh:mm:ss(年-月-日 时:分:秒) 注：- : 符号随便换
 * @returns {*} 返回日期格式
 */

var commonResetDate = exports.commonResetDate = function commonResetDate(date, fmt) {
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    var o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };

    // 遍历这个对象
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            // console.log(`${k}`)
            var str = o[k] + '';
            fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str));
        }
    }

    function padLeftZero(str) {
        return ('00' + str).substr(str.length);
    }
    return fmt;
};

/***/ }),
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setCookie = undefined;

var _vconsoleMin = __webpack_require__(281);

var _vconsoleMin2 = _interopRequireDefault(_vconsoleMin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import vconsole

var ENV = "local";
var KY_IP = void 0,
    KY_H5_IP = void 0,
    TEST_ID = void 0,
    KY_LOGIN = "";
var KY_USER_IP = '';
//h5犀牛会员下载地址(推手里)
var USER_DOWNLOAD_URL = "";
var isWay = true;
var WEB_URL = '';
//电商详情地址前缀
var SHOP_URL_PREFIX = '';
//网关相关
var tokenConfig = {};
var setCookie = exports.setCookie = function setCookie(name, value, Days) {
    if (Days === null || Days === '') {
        Days = 30;
    }
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + "; path=/;expires=" + exp.toGMTString();
};
if (window.location.host == "yhzj.wangaijunrenfeng.com") {
    ENV = "正式环境";
    KY_IP = 'https://yhzj.wangaijunrenfeng.com/opay-business/';
    USER_DOWNLOAD_URL = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html?type=pushhand#home";
    KY_USER_IP = "https://vip.xiaoyaoxinyong.com/public-gateway";
    WEB_URL = 'https://yhzj.wangaijunrenfeng.com/opay-sources/app/index.html#/';
    //网关
    tokenConfig = {
        publicKey: "305C300D06092A864886F70D0101010500034B0030480241009192D14E605E30CCD88524031EE5C1C67C9187BA8129F77DEE28638BDA1C10059BA72DAAE66A9530DB3D7D603C8EC58A50196463A969FFC015EFE2EE3C8A86510203010001",
        clientId: "client_youshua",
        grantTypes: 'password', // "client_credentials,refresh_token",
        clientSecret: "3ae0f46ca81d40c1a8d6567f09e20940",
        scope: "select",
        requestBusiType: "member"
    };
    SHOP_URL_PREFIX = 'https://ec.xiaoyaoxinyong.com/ptGoods.html';
} else if (window.location.host == "xyts.ihaomai88.com") {
    ENV = "正式环境";
    KY_IP = "https://xyts.ihaomai88.com/dirsell-mobile-front/";
    USER_DOWNLOAD_URL = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html?type=pushhand#home";
    KY_USER_IP = "https://vip.xiaoyaoxinyong.com/public-gateway";
    WEB_URL = 'https://xyts.ihaomai88.com/dirsell-source/app/index.html#/';
    setCookie("APPCODE", "YHZJ");
    //网关
    tokenConfig = {
        publicKey: "305C300D06092A864886F70D0101010500034B0030480241009192D14E605E30CCD88524031EE5C1C67C9187BA8129F77DEE28638BDA1C10059BA72DAAE66A9530DB3D7D603C8EC58A50196463A969FFC015EFE2EE3C8A86510203010001",
        clientId: "client_youshua",
        grantTypes: 'password', // "client_credentials,refresh_token",
        clientSecret: "3ae0f46ca81d40c1a8d6567f09e20940",
        scope: "select",
        requestBusiType: "member"
    };
    SHOP_URL_PREFIX = 'https://ec.xiaoyaoxinyong.com/ptGoods.html';
} else if (window.location.host == "yhzj.wangaijunrenfeng.com") {
    ENV = "测试环境";
    KY_IP = 'https://yhzj.wangaijunrenfeng.com/opay-business/';
    WEB_URL = 'https://yhzj.wangaijunrenfeng.com/opay-sources/app/index.html#/';
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    setCookie("APPCODE", "YHZJ");
    //网关
    tokenConfig = {
        publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
        clientId: "client_youshua",
        grantTypes: "password",
        clientSecret: "5274206178d44e5cad27f6af21cf50dd",
        scope: "select",
        requestBusiType: "member"
    };
    SHOP_URL_PREFIX = 'https://ec.xiaoyaoxinyong.com/ptGoods.html';
} else if (window.location.host == "39.106.209.55") {
    ENV = "测试环境";
    KY_IP = "http://39.106.209.55/opay-business/";
    WEB_URL = 'http://39.106.209.55/opay-sources/app/index.html#/';
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    setCookie("APPCODE", "YHZJ");
    new _vconsoleMin2.default();
    //网关
    tokenConfig = {
        publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
        clientId: "client_youshua",
        grantTypes: "password",
        clientSecret: "5274206178d44e5cad27f6af21cf50dd",
        scope: "select",
        requestBusiType: "member"
    };
    SHOP_URL_PREFIX = 'https://ec.xiaoyaoxinyong.com/ptGoods.html';
} else {
    ENV = "其他环境";
    setCookie("DEVICETYPE", "1");
    setCookie("APPCODE", "YHZJ");
    setCookie("VERSION", "1.0.0");
    setCookie("LOGINKEY", "2ed2844d315f57244d2909f804bbfeca");
    // setCookie("LOGINKEY", "b328dcd2d854575b50e31770f71b862f");
    // new VConsole();
    setCookie("USERNO", "");
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    isWay = true;
    KY_IP = "http://39.106.209.55/opay-business/";
    WEB_URL = 'http://192.168.3.246:8800/#/';
    // WEB_URL = `http://172.20.10.3:8800/#/`;
    SHOP_URL_PREFIX = 'http://fs.onebity.com/ptGoods.html';
    //测试网关
    tokenConfig = {
        publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
        clientId: "client_youshua",
        grantTypes: "password",
        clientSecret: "5274206178d44e5cad27f6af21cf50dd",
        scope: "select",
        requestBusiType: "member"
    };
}

//犀牛会员线上下载地址
var USER_DOWNLOAD_ONLINE = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html#home";
console.log({ "当前环境:": ENV, "后端接口:": KY_IP });

module.exports = {
    ENV: ENV,
    KY_USER_IP: KY_USER_IP,
    USER_DOWNLOAD_URL: USER_DOWNLOAD_URL,
    USER_DOWNLOAD_ONLINE: USER_DOWNLOAD_ONLINE,
    KY_IP: KY_IP,
    isWay: isWay,
    WEB_URL: WEB_URL,
    tokenConfig: tokenConfig,
    KY_H5_IP: KY_H5_IP,
    TEST_ID: TEST_ID,
    KY_LOGIN: KY_LOGIN,
    SHOP_URL_PREFIX: SHOP_URL_PREFIX
};

/***/ }),
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _vue = __webpack_require__(10);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _vue2.default();

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getDialogTypeNew = exports.buyShopIsLogin = exports.getDialogType = exports.pushHandRegSms = exports.pushHandReg = exports.pushHandSms = exports.pushHand2User = undefined;

var _api = __webpack_require__(24);

var _api2 = _interopRequireDefault(_api);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**推手首页登录会员**/
//推手登录会员
/** 2019-09-05
 *作者:heliang
 *功能:
 */
function pushHand2User(phone, verCode) {
    // let uuid = getCookie("UUID");
    var url = _api.KY_IP + "rhinoMembership/register";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: { phoneNo: phone, smsCode: verCode },
            showLoading: false
        }, function (res) {
            console.log("推手注册会员 pushHand2User ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手注册会员 pushHand2User *************", res);
            reject(res);
        });
    });
}

//发送验证码推手
function pushHandSms(phoneNo) {
    var url = _api.KY_IP + "rhinoMembership/sendVerCode";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: { phoneNo: phoneNo },
            showLoading: false
        }, function (res) {
            console.log("发送验证码推手 pushHandSms ######################", res);
            resolve(res);
        }, function (res) {
            console.log("发送验证码推手 pushHandSms *************", res);
            reject(res);
        });
    });
}

//获取首页弹窗类型 
function getDialogType() {
    var url = _api.KY_IP + "rhinoMembership/popType";
    // let data = {
    //     "type": 0,
    //     "isNeedGuide": true
    // };
    // return new Promise((resolve, reject) => {
    //     resolve({
    //         data
    //     });
    // });
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, function (res) {
            console.log("获取首页弹窗类型 getDialogType ######################", res);
            resolve(res);
        }, function (res) {
            console.log("获取首页弹窗类型 getDialogType *************", res);
            reject(res);
        });
    });
}

//首页弹窗展示图片
function getDialogTypeNew() {
    var url = _api.KY_IP + "appHome/popUpsDetail";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, function (res) {
            console.log("获取首页弹窗类型 getDialogType ######################", res);
            resolve(res);
        }, function (res) {
            console.log("获取首页弹窗类型 getDialogType *************", res);
            reject(res);
        });
    });
}

//购买商品是否需要提示去注册犀牛会员
function buyShopIsLogin() {
    var url = _api.KY_IP + "rhinoMembership/needToRegister";
    // let data = {
    //     "partnerUserNo": "352707103",
    //     "flag": false
    // };
    // return new Promise((resolve, reject) => {
    //     resolve({
    //         data
    //     });
    // });
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, function (res) {
            console.log("购买商品是否需要提示去注册犀牛会员 buyShopIsLogin ######################", res);
            resolve(res);
        }, function (res) {
            console.log("购买商品是否需要提示去注册犀牛会员 buyShopIsLogin *************", res);
            reject(res);
        });
    });
}

/**我要当推手注册**/
//推手注册
function pushHandReg(phoneNo, checkCode, parentUserNo) {
    // /user/registerMobilePhone
    var url = _api.KY_IP + "user/registerMobilePhone";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: { phoneNo: phoneNo, checkCode: checkCode, parentUserNo: parentUserNo },
            showLoading: false
        }, function (res) {
            console.log("推手注册 pushHandReg ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手注册 pushHandReg *************", res);
            reject(res);
        });
    });
}

//推手注册发送验证码
function pushHandRegSms(phoneNo) {
    var url = _api.KY_IP + "user/sendCheckCode";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            data: { phoneNo: phoneNo, businessType: "REGISTER" },
            showLoading: false
        }, function (res) {
            console.log("推手注册发送验证码 pushHandRegSms ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手注册发送验证码 pushHandRegSms *************", res);
            reject(res);
        });
    });
}

/**微信中会员登录**/
//会员登录绑定
// function userLogin(pushUserNo,phoneNo,smsCode) {
//     let url = `${KY_USER_IP}/resource/vip-user/user/pushLogin`;
//     return new Promise((resolve, reject) => {
//
//
//         common.Ajax({
//             url: url,
//             data: {phoneNo,businessType:"REGISTER"},
//             showLoading: false
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//             resolve(res)
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms *************", res);
//             reject(res)
//         });
//
//         // common.XNAjax({
//         //     url: url,
//         //     sesstionType:"true",
//         //     data: {phoneNo,pushUserNo,smsCode,equityNo:'',source:'XYTS',caller:'H5',isOpenEquity:'N'},
//         //     showLoading: false
//         // }, (res) => {
//         //     console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//         //     resolve(res)
//         // }, (res) => {
//         //     console.log("推手注册发送验证码 pushHandRegSms *************", res);
//         //     reject(res)
//         // });
//     });
// }
//
// // 会员发送验证码
// function userSms(phoneNo) {
//     let url = `${KY_USER_IP}/resource/vip-user/userCode/getSmsCode`;
//     return new Promise((resolve, reject) => {
//         common.XNAjax({
//             url: url,
//             data: {phoneNo,caller:'H5'},
//             showLoading: false,
//             sesstionType:"true",
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//             resolve(res)
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms *************", res);
//             reject(res)
//         });
//     });
// }


exports.pushHand2User = pushHand2User;
exports.pushHandSms = pushHandSms;
exports.pushHandReg = pushHandReg;
exports.pushHandRegSms = pushHandRegSms;
exports.getDialogType = getDialogType;
exports.buyShopIsLogin = buyShopIsLogin;
exports.getDialogTypeNew = getDialogTypeNew;

/***/ }),
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.startPhone = startPhone;
function startPhone(phoneNum) {
    if (phoneNum) {
        var phnumAfter = phoneNum.substr(0, 3) + "****" + phoneNum.substr(7);
        var result = phoneNum.replace(phoneNum, phnumAfter);
        return phoneNum = result;
    } else {
        return phoneNum;
    }
}

/***/ }),
/* 106 */,
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

!function (t, e) {
  if ("object" == ( false ? "undefined" : _typeof(exports)) && "object" == ( false ? "undefined" : _typeof(module))) module.exports = e(__webpack_require__(10));else if (true) !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(10)], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {
    var i = "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? e(require("vue")) : e(t.Vue);for (var n in i) {
      ("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports : t)[n] = i[n];
    }
  }
}(window, function (t) {
  return function (t) {
    var e = {};function i(n) {
      if (e[n]) return e[n].exports;var s = e[n] = { i: n, l: !1, exports: {} };return t[n].call(s.exports, s, s.exports, i), s.l = !0, s.exports;
    }return i.m = t, i.c = e, i.d = function (t, e, n) {
      i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
    }, i.r = function (t) {
      "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
    }, i.t = function (t, e) {
      if (1 & e && (t = i(t)), 8 & e) return t;if (4 & e && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t && t.__esModule) return t;var n = Object.create(null);if (i.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t) for (var s in t) {
        i.d(n, s, function (e) {
          return t[e];
        }.bind(null, s));
      }return n;
    }, i.n = function (t) {
      var e = t && t.__esModule ? function () {
        return t.default;
      } : function () {
        return t;
      };return i.d(e, "a", e), e;
    }, i.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }, i.p = "", i(i.s = 15);
  }([function (t, e, i) {}, function (t, e, i) {}, function (t, e, i) {}, function (t, e, i) {}, function (e, i) {
    e.exports = t;
  }, function (t, e, i) {},, function (t, e, i) {
    "use strict";
    var n = i(0);i.n(n).a;
  },, function (t, e, i) {
    "use strict";
    var n = i(1);i.n(n).a;
  },, function (t, e, i) {
    "use strict";
    var n = i(2);i.n(n).a;
  },, function (t, e, i) {
    "use strict";
    var n = i(3);i.n(n).a;
  },, function (t, e, i) {
    "use strict";
    i.r(e);i(5);var n = i(4),
        s = function s(t) {
      t.component(this.name, this);
    },
        r = function r(t) {
      return t.name = "wv-" + t.name, t.mixins = t.mixins || [], t.components = t.components || {}, t.install = t.install || s, t.methods = t.methods || {}, t;
    };i.n(n).a.prototype.$isServer;var a = r({ name: "cell", mixins: [{ props: { url: String, replace: Boolean, to: [String, Object] }, methods: {
          routerLink: function routerLink() {
            var t = this.to,
                e = this.url,
                i = this.$router,
                n = this.replace;
            t && i ? i[n ? "replace" : "push"](t) : e && (n ? location.replace(e) : location.href = e);
          }
        } }], props: { title: { type: [String, Number] }, value: { type: [String, Number] }, isLink: Boolean }, methods: {
        onClick: function onClick() {
          this.$emit("click"), this.routerLink();
        }
      } });i(7);function o(t, e, i, n, s, r, a, o) {
      var u,
          l = "function" == typeof t ? t.options : t;if (e && (l.render = e, l.staticRenderFns = i, l._compiled = !0), n && (l.functional = !0), r && (l._scopeId = "data-v-" + r), a ? (u = function u(t) {
        (t = t || this.$vnode && this.$vnode.ssrContext || this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) || "undefined" == typeof __VUE_SSR_CONTEXT__ || (t = __VUE_SSR_CONTEXT__), s && s.call(this, t), t && t._registeredComponents && t._registeredComponents.add(a);
      }, l._ssrRegister = u) : s && (u = o ? function () {
        s.call(this, this.$root.$options.shadowRoot);
      } : s), u) if (l.functional) {
        l._injectStyles = u;var c = l.render;l.render = function (t, e) {
          return u.call(e), c(t, e);
        };
      } else {
        var h = l.beforeCreate;l.beforeCreate = h ? [].concat(h, u) : [u];
      }return { exports: t, options: l };
    }var u = o(a, function () {
      var t = this,
          e = t.$createElement,
          i = t._self._c || e;return i("div", { staticClass: "weui-cell", class: { "weui-cell_access": t.isLink }, on: { click: t.onClick } }, [i("div", { staticClass: "weui-cell_hd" }, [t._t("icon")], 2), t._v(" "), i("div", { staticClass: "weui-cell__bd" }, [t._t("bd", [i("p", { domProps: { innerHTML: t._s(t.title) } })])], 2), t._v(" "), i("div", { staticClass: "weui-cell__ft" }, [t._t("ft", [t._v(t._s(t.value))])], 2)]);
    }, [], !1, null, "c2ca5714", null);u.options.__file = "index.vue";var l = u.exports;var c = function c(t, e, i) {
      return Math.min(Math.max(t, e), i);
    };var h = r({ name: "picker-column", props: { options: { type: Array, default: function _default() {
            return [];
          } }, value: {}, valueKey: String, visibleItemCount: { type: Number, default: 7, validator: function validator(t) {
            return [3, 5, 7].indexOf(t) > -1;
          } }, defaultIndex: { type: Number, default: 0 }, divider: { type: Boolean, default: !1 }, content: { type: String, default: "" } }, data: function data() {
        return { startY: 0, startOffset: 0, offset: 0, prevY: 0, prevTime: null, velocity: 0, transition: "", currentIndex: this.defaultIndex };
      },
      computed: {
        minTranslateY: function minTranslateY() {
          return 34 * (Math.ceil(this.visibleItemCount / 2) - this.options.length);
        },
        maxTranslateY: function maxTranslateY() {
          return 34 * Math.floor(this.visibleItemCount / 2);
        },
        wrapperStyle: function wrapperStyle() {
          return { transition: this.transition, transform: "translate3d(0, " + this.offset + "px, 0)" };
        },
        pickerIndicatorStyle: function pickerIndicatorStyle() {
          return { top: 34 * Math.floor(this.visibleItemCount / 2) + "px" };
        },
        pickerMaskStyle: function pickerMaskStyle() {
          return { backgroundSize: "100% " + 34 * Math.floor(this.visibleItemCount / 2) + "px" };
        },
        count: function count() {
          return this.options.length;
        },
        currentValue: function currentValue() {
          return this.options[this.currentIndex];
        }
      }, created: function created() {
        this.$parent && this.$parent.children.push(this);
      },
      mounted: function mounted() {
        this.setIndex(this.currentIndex);
      },
      destroyed: function destroyed() {
        this.$parent && this.$parent.children.splice(this.$parent.children.indexOf(this), 1);
      },
      methods: {
        getOptionText: function getOptionText(t) {
          return "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) ? t[this.valueKey] : t;
        },
        isDisabled: function isDisabled(t) {
          return "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t.disabled;
        }, indexToOffset: function indexToOffset(t) {
          return -34 * (t - Math.floor(this.visibleItemCount / 2));
        },
        offsetToIndex: function offsetToIndex(t) {
          return -((t = 34 * Math.round(t / 34)) - 34 * Math.floor(this.visibleItemCount / 2)) / 34;
        },
        onTouchstart: function onTouchstart(t) {
          this.startOffset = this.offset, this.startY = t.touches[0].clientY, this.prevY = t.touches[0].clientY, this.prevTime = new Date(), this.transition = "";
        },
        onTouchmove: function onTouchmove(t) {
          var e = +new Date(),
              i = t.touches[0].clientY,
              n = i - this.startY;this.offset = this.startOffset + n, this.velocity = (t.touches[0].clientY - this.prevY) / (e - this.prevTime), this.prevY = i, this.prevTime = e;
        },
        onTouchend: function onTouchend() {
          this.transition = "all 150ms ease";var t = this.offset + 150 * this.velocity,
              e = this.offsetToIndex(t);this.setIndex(e, !0);
        },
        onClick: function onClick(t) {
          var e = this.$refs.indicator;this.transition = "all 150ms ease";var i = e.getBoundingClientRect(),
              n = 34 * Math.floor((t.clientY - i.top) / 34),
              s = this.offset - n;this.offset = c(s, this.minTranslateY, this.maxTranslateY);var r = this.offsetToIndex(this.offset);this.setIndex(r, !0);
        },
        adjustIndex: function adjustIndex(t) {
          for (var _e = t = c(t, 0, this.count); _e < this.count; _e++) {
            if (!this.isDisabled(this.options[_e])) return _e;
          }for (var _e2 = t - 1; _e2 >= 0; _e2--) {
            if (!this.isDisabled(this.options[_e2])) return _e2;
          }
        },
        setIndex: function setIndex(t) {
          var e = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : !1;
          t = this.adjustIndex(t), this.offset = this.indexToOffset(t), t !== this.currentIndex && (this.currentIndex = t, e && this.$emit("change", t));
        },
        setValue: function setValue(t) {
          var _this = this;

          var e = this.options,
              i = e.findIndex(function (e) {
            return _this.getOptionText(e) === t;
          });i > -1 && this.setIndex(i);
        }
      }, watch: {
        defaultIndex: function defaultIndex(t) {
          this.setIndex(t);
        },
        options: function options(t, e) {
          JSON.stringify(t) !== JSON.stringify(e) && this.setIndex(0);
        }
      } }),
        d = (i(9), o(h, function () {
      var t = this,
          e = t.$createElement,
          i = t._self._c || e;return t.divider ? i("div", { staticClass: "wv-picker-column-divider", domProps: { innerHTML: t._s(t.content) } }) : i("div", { staticClass: "weui-picker__group", on: { touchstart: t.onTouchstart, touchmove: function touchmove(e) {
            return e.preventDefault(), t.onTouchmove(e);
          }, touchend: t.onTouchend, touchcancel: t.onTouchend, click: t.onClick } }, [i("div", { staticClass: "weui-picker__mask", style: t.pickerMaskStyle }), t._v(" "), i("div", { ref: "indicator", staticClass: "weui-picker__indicator", style: t.pickerIndicatorStyle }), t._v(" "), i("div", { staticClass: "weui-picker__content", style: t.wrapperStyle }, t._l(t.options, function (e, n) {
        return i("div", { key: n, staticClass: "weui-picker__item", class: { "weui-picker__item_disabled": t.isDisabled(e) }, domProps: { textContent: t._s(t.getOptionText(e)) } });
      }))]);
    }, [], !1, null, "11eafb6a", null));d.options.__file = "picker-column.vue";var p = r({ name: "picker", components: { PickerColumn: d.exports }, props: { visible: Boolean, confirmText: { type: String, default: "确定" }, cancelText: { type: String, default: "取消" }, columns: { type: Array, default: function _default() {
            return [];
          } }, valueKey: String, visibleItemCount: { type: Number, default: 7, validator: function validator(t) {
            return [3, 5, 7].indexOf(t) > -1;
          } }, value: { type: Array, default: function _default() {
            return [];
          } } }, data: function data() {
        return { children: [], currentColumns: [], currentValue: this.value };
      },
      computed: {
        columnCount: function columnCount() {
          return this.columns.filter(function (t) {
            return !t.divider;
          }).length;
        },
        pickerBodyStyle: function pickerBodyStyle() {
          return { height: 34 * this.visibleItemCount + "px" };
        }
      }, created: function created() {
        this.initialize();
      },
      methods: {
        initialize: function initialize() {
          this.currentColumns = this.columns;
        },
        columnValueChange: function columnValueChange(t) {
          this.currentValue = this.getValues(), this.$emit("change", this, this.getValues(), t);
        },
        getColumn: function getColumn(t) {
          return this.children.find(function (e, i) {
            return "wv-picker-column" === e.$options.name && !e.divider && i === t;
          });
        },
        getColumnValue: function getColumnValue(t) {
          return (this.getColumn(t) || {}).currentValue;
        },
        setColumnValue: function setColumnValue(t, e) {
          var i = this.getColumn(t);i && i.setValue(e);
        },
        getColumnValues: function getColumnValues(t) {
          return (this.currentColumns[t] || {}).values;
        },
        setColumnValues: function setColumnValues(t, e) {
          var i = this.currentColumns[t];i && (i.values = e);
        },
        getValues: function getValues() {
          return this.children.map(function (t) {
            return t.currentValue;
          });
        },
        setValues: function setValues(t) {
          var _this2 = this;

          if (this.columnCount !== t.length) throw new Error("Length values is not equal to columns count.");t.forEach(function (t, e) {
            _this2.setColumnValue(e, t);
          });
        },
        getColumnIndex: function getColumnIndex(t) {
          return (this.getColumn(t) || {}).currentIndex;
        },
        setColumnIndex: function setColumnIndex(t, e) {
          var i = this.getColumn(t);i && i.setIndex(e);
        },
        getIndexes: function getIndexes() {
          return this.children.map(function (t) {
            return t.currentIndex;
          });
        },
        setIndexes: function setIndexes(t) {
          var _this3 = this;

          t.forEach(function (t, e) {
            _this3.setColumnIndex(e, t);
          });
        },
        onCancel: function onCancel() {
          this.$emit("cancel", this), this.$emit("update:visible", !1);
        },
        onConfirm: function onConfirm() {
          this.$emit("confirm", this), this.$emit("update:visible", !1);
        }
      }, watch: {
        value: function value(t) {
          this.setValues(t), this.currentValue = t;
        },
        currentValue: function currentValue(t) {
          this.$emit("input", t);
        }
      } }),
        f = (i(11), o(p, function () {
      var t = this,
          e = t.$createElement,
          i = t._self._c || e;return i("div", [i("transition", { attrs: { "enter-active-class": "weui-animate-fade-in", "leave-active-class": "weui-animate-fade-out" } }, [i("div", { directives: [{ name: "show", rawName: "v-show", value: t.visible, expression: "visible" }], staticClass: "weui-mask" })]), t._v(" "), i("transition", { attrs: { "enter-active-class": "weui-animate-slide-up", "leave-active-class": "weui-animate-slide-down" } }, [i("div", { directives: [{ name: "show", rawName: "v-show", value: t.visible, expression: "visible" }], staticClass: "weui-picker" }, [i("div", { staticClass: "weui-picker__hd" }, [i("div", { staticClass: "weui-picker__action", domProps: { textContent: t._s(t.cancelText) }, on: { click: t.onCancel } }), t._v(" "), i("div", { staticClass: "weui-picker__action", domProps: { textContent: t._s(t.confirmText) }, on: { click: t.onConfirm } })]), t._v(" "), i("div", { staticClass: "weui-picker__bd", style: t.pickerBodyStyle }, t._l(t.columns, function (e, n) {
        return i("picker-column", { key: n, attrs: { options: e.values || [], "value-key": t.valueKey, divider: e.divider, content: e.content, "default-index": e.defaultIndex, "visible-item-count": t.visibleItemCount }, on: { change: function change(e) {
              t.columnValueChange(n);
            } } });
      }))])])], 1);
    }, [], !1, null, "de496102", null));f.options.__file = "index.vue";var m = f.exports;var v = function v(t) {
      return "[object Date]" === Object.prototype.toString.call(t) && !isNaN(t.getTime());
    };var y = o(r({ name: "datetime-picker", components: { WvPicker: m }, props: { visible: Boolean, confirmText: { type: String, default: "确定" }, cancelText: { type: String, default: "取消" }, type: { type: String, default: "datetime" }, startDate: { type: Date, default: function _default() {
            return new Date(new Date().getFullYear() - 60, 0, 1);
          }, validator: v }, endDate: { type: Date, default: function _default() {
            return new Date(new Date().getFullYear(), 12, 31);
          }, validator: v }, startHour: { type: Number, default: 0 }, endHour: { type: Number, default: 23 }, yearFormat: { type: String, default: "{value}" }, monthFormat: { type: String, default: "{value}" }, dateFormat: { type: String, default: "{value}" }, hourFormat: { type: String, default: "{value}" }, minuteFormat: { type: String, default: "{value}" }, visibleItemCount: { type: Number, default: 7 }, value: {} }, data: function data() {
        return { currentVisible: this.visible, currentValue: this.correctValue(this.value) };
      },
      computed: {
        ranges: function ranges() {
          if ("time" === this.type) return { hour: [this.startHour, this.endHour], minute: [0, 59] };
          var _getBoundary = this.getBoundary("start", this.currentValue),
              t = _getBoundary.startYear,
              e = _getBoundary.startMonth,
              i = _getBoundary.startDate,
              n = _getBoundary.startHour,
              s = _getBoundary.startMinute,
              _getBoundary2 = this.getBoundary("end", this.currentValue),
              r = _getBoundary2.endYear,
              a = _getBoundary2.endMonth,
              o = _getBoundary2.endDate,
              u = _getBoundary2.endHour,
              l = _getBoundary2.endMinute;

          return "datetime" === this.type ? { year: [t, r], month: [e, a], date: [i, o], hour: [n, u], minute: [s, l] } : { year: [t, r], month: [e, a], date: [i, o] };
        },
        pickerColumns: function pickerColumns() {
          var t = [];for (var _e3 in this.ranges) {
            t.push({ values: this.fillColumnValues(_e3, this.ranges[_e3][0], this.ranges[_e3][1]) });
          }return t;
        }
      }, methods: {
        startFunc: function startFunc() {
          this.$refs.startTime.className = "active", this.$refs.endTime.className = "";
        },
        endFunc: function endFunc() {
          this.$refs.startTime.className = "", this.$refs.endTime.className = "active";
        },
        open: function open() {
          this.currentVisible = !0;
        },
        close: function close() {
          this.currentVisible = !1;
        },
        isLeapYear: function isLeapYear(t) {
          return t % 400 == 0 || t % 100 != 0 && t % 4 == 0;
        }, isShortMonth: function isShortMonth(t) {
          return [4, 6, 9, 11].indexOf(t) > -1;
        }, getMonthEndDay: function getMonthEndDay(t, e) {
          return this.isShortMonth(e) ? 30 : 2 === e ? this.isLeapYear(t) ? 29 : 28 : 31;
        },
        getTrueValue: function getTrueValue(t) {
          if (t) {
            for (; isNaN(parseInt(t, 10));) {
              t = t.slice(1);
            }return parseInt(t, 10);
          }
        },
        correctValue: function correctValue(t) {
          var e = this.type.indexOf("date") > -1;if (e && !v(t)) t = this.startDate;else if (!t) {
            var _e4 = this.startHour;
            t = (_e4 > 10 ? _e4 : "0" + _e4) + ":00";
          }if (!e) {
            var _t$split = t.split(":"),
                _t$split2 = _slicedToArray(_t$split, 2),
                _e5 = _t$split2[0],
                _i = _t$split2[1];

            var _n = Math.max(_e5, this.startHour);return (_n = Math.min(_n, this.endHour)) + ":" + _i;
          }
          var _getBoundary3 = this.getBoundary("end", t),
              i = _getBoundary3.endYear,
              n = _getBoundary3.endDate,
              s = _getBoundary3.endMonth,
              r = _getBoundary3.endHour,
              a = _getBoundary3.endMinute,
              _getBoundary4 = this.getBoundary("start", t),
              o = _getBoundary4.startYear,
              u = _getBoundary4.startDate,
              l = _getBoundary4.startMonth,
              c = _getBoundary4.startHour,
              h = _getBoundary4.startMinute,
              d = new Date(o, l - 1, u, c, h),
              p = new Date(i, s - 1, n, r, a);

          return t = Math.max(t, d), t = Math.min(t, p), new Date(t);
        },
        onChange: function onChange(t) {
          var e = t.getValues();var i = void 0;if ("time" === this.type) i = e.join(":");else {
            var _t = this.getTrueValue(e[0]),
                _n2 = this.getTrueValue(e[1]);var _s = this.getTrueValue(e[2]);var _r = this.getMonthEndDay(_t, _n2);_s = _s > _r ? _r : _s;var _a = 0,
                _o = 0;"datetime" === this.type && (_a = this.getTrueValue(e[3]), _o = this.getTrueValue(e[4])), i = new Date(_t, _n2 - 1, _s, _a, _o);
          }i = this.correctValue(i), this.currentValue = i, this.$emit("change", t), this.$emit("input", i);
        },
        fillColumnValues: function fillColumnValues(t, e, i) {
          var n = [];for (var _s2 = e; _s2 <= i; _s2++) {
            _s2 < 10 ? n.push(this[t + "Format"].replace("{value}", ("0" + _s2).slice(-2))) : n.push(this[t + "Format"].replace("{value}", _s2));
          }return n;
        },
        getBoundary: function getBoundary(t, e) {
          var _ref;

          var i = this[t + "Date"],
              n = i.getFullYear();var s = 1,
              r = 1,
              a = 0,
              o = 0;return "end" === t && (s = 12, r = this.getMonthEndDay(e.getFullYear(), e.getMonth() + 1), a = 23, o = 59), e.getFullYear() === n && (s = i.getMonth() + 1, e.getMonth() + 1 === s && (r = i.getDate(), e.getDate() === r && (a = i.getHours(), e.getHours() === a && (o = i.getMinutes())))), (_ref = {}, _defineProperty(_ref, t + "Year", n), _defineProperty(_ref, t + "Month", s), _defineProperty(_ref, t + "Date", r), _defineProperty(_ref, t + "Hour", a), _defineProperty(_ref, t + "Minute", o), _ref);
        },
        updateColumnValue: function updateColumnValue(t) {
          var _this4 = this;

          var e = [];if ("time" === this.type) {
            var _i2 = t.split(":");e = [this.hourFormat.replace("{value}", ("0" + _i2[0]).slice(-2)), this.minuteFormat.replace("{value}", ("0" + _i2[1]).slice(-2))];
          } else e = [this.yearFormat.replace("{value}", "" + t.getFullYear()), this.monthFormat.replace("{value}", ("0" + (t.getMonth() + 1)).slice(-2)), this.dateFormat.replace("{value}", ("0" + t.getDate()).slice(-2))], "datetime" === this.type && e.push(this.hourFormat.replace("{value}", ("0" + t.getHours()).slice(-2)), this.minuteFormat.replace("{value}", ("0" + t.getMinutes()).slice(-2)));this.$nextTick(function () {
            _this4.setColumnByValues(e);
          });
        },
        setColumnByValues: function setColumnByValues(t) {
          this.$refs.picker.setValues(t);
        },
        onConfirm: function onConfirm() {
          this.visible = !1, this.$emit("confirm", this.currentValue);
        },
        onCancel: function onCancel() {
          this.visible = !1, this.$emit("cancel");
        }
      }, mounted: function mounted() {
        this.value ? this.currentValue = this.value : this.currentValue = this.type.indexOf("date") > -1 ? this.startDate : ("0" + this.startHour).slice(-2) + ":00", this.updateColumnValue(this.currentValue);
      },
      watch: {
        value: function value(t) {
          t = this.correctValue(t), ("time" === this.type ? t === this.currentValue : t.valueOf() === this.currentValue.valueOf()) || (this.currentValue = t);
        },
        currentValue: function currentValue(t) {
          this.updateColumnValue(t), this.$emit("input", t);
        }
      } }), function () {
      var t = this,
          e = t.$createElement;return (t._self._c || e)("wv-picker", { ref: "picker", attrs: { visible: t.currentVisible, columns: t.pickerColumns, "confirm-text": t.confirmText, "cancel-text": t.cancelText, "visible-item-count": t.visibleItemCount }, on: { "update:visible": function updateVisible(e) {
            t.currentVisible = e;
          }, change: t.onChange, confirm: t.onConfirm, cancel: t.onCancel } });
    }, [], !1, null, null, null);y.options.__file = "index.vue";var g = y.exports,
        _ = r({ name: "group", props: { title: String, titleColor: String } }),
        x = (i(13), o(_, function () {
      var t = this.$createElement,
          e = this._self._c || t;return e("div", [this.title ? e("div", { staticClass: "weui-cells__title", style: { color: this.titleColor }, domProps: { innerHTML: this._s(this.title) } }) : this._e(), this._v(" "), e("div", { staticClass: "weui-cells" }, [this._t("default")], 2)]);
    }, [], !1, null, "3445f486", null));x.options.__file = "index.vue";var C = x.exports;i.d(e, "Cell", function () {
      return l;
    }), i.d(e, "DatetimePicker", function () {
      return g;
    }), i.d(e, "Group", function () {
      return C;
    }), i.d(e, "Picker", function () {
      return m;
    });var b = [l, g, C, m],
        V = function V(t) {
      var e = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      b.forEach(function (e) {
        t.use(e);
      });
    };"undefined" != typeof window && window.Vue && V(window.Vue);e.default = { install: V, version: "2.2.6" };
  }]);
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(303)(module)))

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var applyRecord = function applyRecord() {
    return __webpack_require__.e/* import() */(18).then(__webpack_require__.bind(null, 306));
};
var changePending = function changePending() {
    return __webpack_require__.e/* import() */(14).then(__webpack_require__.bind(null, 308));
};
var changeAgreed = function changeAgreed() {
    return __webpack_require__.e/* import() */(15).then(__webpack_require__.bind(null, 307));
};
var changeRejected = function changeRejected() {
    return __webpack_require__.e/* import() */(17).then(__webpack_require__.bind(null, 309));
};

exports.default = [
// 新增
{
    path: "/applyRecord",
    name: "applyRecord",
    component: applyRecord,
    meta: {
        title: "申请记录",
        keepAlive: false
    }
}, {
    path: "/changePending",
    name: "changePending",
    component: changePending,
    meta: {
        title: "换机待处理",
        keepAlive: false
    }
}, {
    path: "/changeAgreed",
    name: "changeAgreed",
    component: changeAgreed,
    meta: {
        title: "换机已同意",
        keepAlive: false
    }
}, {
    path: "/changeRejected",
    name: "changeRejected",
    component: changeRejected,
    meta: {
        title: "换机已驳回",
        keepAlive: false
    }
}];

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var creditCard = function creditCard() {
    return __webpack_require__.e/* import() */(6).then(__webpack_require__.bind(null, 310));
};
var creditCard_order = function creditCard_order() {
    return __webpack_require__.e/* import() */(1).then(__webpack_require__.bind(null, 311));
};
var onlineCard = function onlineCard() {
    return __webpack_require__.e/* import() */(3).then(__webpack_require__.bind(null, 313));
};
var exchangeOrder = function exchangeOrder() {
    return __webpack_require__.e/* import() */(0).then(__webpack_require__.bind(null, 312));
};
var shop_order = function shop_order() {
    return __webpack_require__.e/* import() */(2).then(__webpack_require__.bind(null, 314));
};

exports.default = [{
    path: "/creditCard",
    name: "creditCard",
    component: creditCard,
    meta: {
        title: "推荐办卡",
        keepAlive: false
    }
}, {
    path: "/creditCard_order",
    name: "creditCard_order",
    component: creditCard_order,
    meta: {
        title: "办卡订单",
        keepAlive: false
    }
}, {
    path: "/onlineCard",
    name: "onlineCard",
    component: onlineCard,
    meta: {
        title: "在线办卡",
        keepAlive: false
    }
}, {
    path: "/exchangeOrder",
    name: "exchangeOrder",
    component: exchangeOrder,
    meta: {
        title: "兑换订单",
        keepAlive: false
    }
}, {
    path: "/shop_order",
    name: "shop_order",
    component: shop_order,
    meta: {
        title: "商城订单",
        keepAlive: false
    }
}];

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var machinesToolsApply = function machinesToolsApply() {
    return __webpack_require__.e/* import() */(13).then(__webpack_require__.bind(null, 315));
};
var machinesToolsIsApply = function machinesToolsIsApply() {
    return __webpack_require__.e/* import() */(12).then(__webpack_require__.bind(null, 316));
};
var machinesToolsYesApply = function machinesToolsYesApply() {
    return __webpack_require__.e/* import() */(9).then(__webpack_require__.bind(null, 317));
};

exports.default = [
// 新增
{
    path: "/machinesToolsApply",
    name: "machinesToolsApply",
    component: machinesToolsApply,
    meta: {
        title: "机具申请",
        keepAlive: false
    }
}, {
    path: "/machinesToolsIsApply",
    name: "machinesToolsIsApply",
    component: machinesToolsIsApply,
    meta: {
        title: "机具申请",
        keepAlive: false
    }
}, {
    path: "/machinesToolsYesApply",
    name: "machinesToolsYesApply",
    component: machinesToolsYesApply,
    meta: {
        title: "机具申请",
        keepAlive: false
    }
}];

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var detail = function detail() {
    return __webpack_require__.e/* import() */(16).then(__webpack_require__.bind(null, 318));
};
var privacyPolicy = function privacyPolicy() {
    return __webpack_require__.e/* import() */(20).then(__webpack_require__.bind(null, 319));
};
var register = function register() {
    return __webpack_require__.e/* import() */(10).then(__webpack_require__.bind(null, 320));
};
var userAgreement = function userAgreement() {
    return __webpack_require__.e/* import() */(19).then(__webpack_require__.bind(null, 321));
};
var wxDown = function wxDown() {
    return __webpack_require__.e/* import() */(5).then(__webpack_require__.bind(null, 322));
};
exports.default = [{
    path: "/detail",
    name: "detail",
    component: detail,
    meta: {
        title: "详情",
        keepAlive: false
    }
}, {
    path: "/privacyPolicy",
    name: "privacyPolicy",
    component: privacyPolicy,
    meta: {
        title: "隐私保护政策",
        keepAlive: false
    }
}, {
    path: "/register",
    name: "register",
    component: register,
    meta: {
        title: "注册",
        keepAlive: false
    }
}, {
    path: "/userAgreement",
    name: "userAgreement",
    component: userAgreement,
    meta: {
        title: "用户协议",
        keepAlive: false
    }
}, {
    path: "/wxDown",
    name: "wxDown",
    component: wxDown,
    meta: {
        title: "下载",
        keepAlive: false
    }
}];

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var rewardList = function rewardList() {
    return __webpack_require__.e/* import() */(4).then(__webpack_require__.bind(null, 323));
};
exports.default = [{
    path: "/rewardList",
    name: "rewardList",
    component: rewardList,
    meta: {
        title: "奖励列表",
        keepAlive: false
    }
}];

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var addAdress = function addAdress() {
    return __webpack_require__.e/* import() */(8).then(__webpack_require__.bind(null, 324));
};
var adressList = function adressList() {
    return __webpack_require__.e/* import() */(11).then(__webpack_require__.bind(null, 325));
};
var editorAdress = function editorAdress() {
    return __webpack_require__.e/* import() */(7).then(__webpack_require__.bind(null, 326));
};
// let addAddress = () => import ('@/page/shop/addAddress');

exports.default = [
// 新增
{
    path: "/addAdress",
    name: "addAdress",
    component: addAdress,
    meta: {
        title: "新建收获地址",
        keepAlive: false
    }
}, {
    path: "/adressList",
    name: "adressList",
    component: adressList,
    meta: {
        title: "收获地址",
        keepAlive: false
    }
}, {
    path: "/editorAdress",
    name: "editorAdress",
    component: editorAdress,
    meta: {
        title: "收货地址",
        keepAlive: false
    }
}];

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _vue = __webpack_require__(10);

var _vue2 = _interopRequireDefault(_vue);

var _vuex = __webpack_require__(302);

var _vuex2 = _interopRequireDefault(_vuex);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_vue2.default.use(_vuex2.default);

exports.default = new _vuex2.default.Store({
    state: {
        purchaseJson: {},
        address: {},
        activityJson: {},
        loanData: {},
        list2: {}
    },
    actions: {
        saveCarList: function saveCarList(ctx, data) {
            ctx.commit('saveCarList', data);
        },
        saveList: function saveList(ctx, data) {
            ctx.commit('saveList', data);
        },
        saveAddress: function saveAddress(ctx, data) {
            ctx.commit('saveAddress', data);
        },
        saveLoanData: function saveLoanData(ctx, data) {
            ctx.commit('saveLoanData', data);
        }
    },
    mutations: {
        saveCarList: function saveCarList(state, data) {
            state.purchaseJson = data;
        },
        saveList: function saveList(state, data) {
            state.list2 = data;
        },
        saveAddress: function saveAddress(state, data) {
            state.address = data;
        },
        saveLoanData: function saveLoanData(state, data) {
            state.loanData = data;
            sessionStorage.setItem("loanData", JSON.stringify(data));
        }
    }
});

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(133),
  /* template */
  __webpack_require__(290),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\App.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] App.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4325e4d3", Component.options)
  } else {
    hotAPI.reload("data-v-4325e4d3", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(297)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(137),
  /* template */
  __webpack_require__(288),
  /* scopeId */
  "data-v-27595902",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\pushHandLogin\\login.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] login.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-27595902", Component.options)
  } else {
    hotAPI.reload("data-v-27595902", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(295)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(138),
  /* template */
  __webpack_require__(286),
  /* scopeId */
  "data-v-136bdbe6",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\sharePic\\share.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] share.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-136bdbe6", Component.options)
  } else {
    hotAPI.reload("data-v-136bdbe6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(293)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(135),
  /* template */
  __webpack_require__(284),
  /* scopeId */
  "data-v-0b4539d4",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\swiper\\swiper.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] swiper.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0b4539d4", Component.options)
  } else {
    hotAPI.reload("data-v-0b4539d4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(296)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(139),
  /* template */
  __webpack_require__(287),
  /* scopeId */
  "data-v-1898a39e",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\userLoginWx\\login.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] login.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1898a39e", Component.options)
  } else {
    hotAPI.reload("data-v-1898a39e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(294)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(140),
  /* template */
  __webpack_require__(285),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\userLogin\\login.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] login.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-135e949d", Component.options)
  } else {
    hotAPI.reload("data-v-135e949d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(300)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(136),
  /* template */
  __webpack_require__(292),
  /* scopeId */
  "data-v-64de57f2",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\userLogin\\verifiCode.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] verifiCode.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-64de57f2", Component.options)
  } else {
    hotAPI.reload("data-v-64de57f2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(298)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(141),
  /* template */
  __webpack_require__(289),
  /* scopeId */
  "data-v-32c7ffa0",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\userPayDialog\\userPayDialog.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] userPayDialog.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32c7ffa0", Component.options)
  } else {
    hotAPI.reload("data-v-32c7ffa0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 123 */,
/* 124 */,
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    overtime: 30000,
    httpError: "请求服务失败，请稍后重试！",
    timeoutError: "请求超时，请稍后重试！",
    shopName: {
        empty: '请输入商户名称',
        error: '请输入正确的商户名称'
    },
    name: {
        empty: '请输入姓名',
        error: '请输入正确的姓名',
        reg: /^[\u4E00-\u9FA5]{2,}(?:(·|.)[\u4E00-\u9FA5]{2,})*$/

    },
    phoneNumber: {
        empty: '请输入的手机号',
        error: '请输入您正确的手机号',
        reg: /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|166|199|18[0-9]|17[0-9])[0-9]{8}$/
    },
    phoneNumberTwo: {
        empty: '请输入的手机号',
        error: '请输入您正确的手机号',
        reg: /^1\d{10}$/
    },
    registerNumber: {
        empty: '请输入手机号',
        error: '请输入正确的手机号',
        reg: /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|18[0-9]|17[0-9])[0-9]{8}$/
    },
    idCard: {
        empty: '请输入法人身份证号',
        error: '请输入正确的法人身份证号',
        reg: /(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    },
    checkFile: {
        empty: '请上传对应的图片'
    },
    smallTicket: {
        empty: '请输入小票名称'
    },
    linkmanName: {
        empty: '请输入联系人姓名',
        error: '请输入正确的联系人姓名',
        reg: /^[\u4E00-\u9FA5]{2,}(?:(·|.)[\u4E00-\u9FA5]{2,})*$/
    },
    linkmanNumber: {
        empty: '请输入联系人手机号',
        error: '请输入您正确的手机号',
        reg: /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|18[0-9]|17[0-9])[0-9]{8}$/
    },
    addressee: {
        empty: '请输入联系电话',
        error: '请输入您正确的联系电话',
        reg: /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|18[0-9]|17[0-9])[0-9]{8}$/
    },
    businessLicense: {
        empty: '请输入营业执照注册号',
        error: '请输入正确的营业执照注册号',
        reg: /^\d{15}$/
    },
    corporateName: {
        empty: '请输入法人姓名',
        error: '请输入正确的法人姓名',
        reg: /^[\u4E00-\u9FA5]{2,}(?:(·|.)[\u4E00-\u9FA5]{2,})*$/
    },
    postalCost: {
        empty: '请输入邮政编码',
        error: '请输入正确的邮政编码',
        reg: /^[1-9][0-9]{5}$/
    },
    corporateIdcard: {
        empty: '请输入法人身份证号',
        error: '请输入正确的法人身份证号',
        reg: /(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    },
    legalName: {
        empty: '请输入授权结算人姓名',
        error: '请输入正确的授权结算人姓名',
        reg: /^[\u4E00-\u9FA5]{2,}(?:(·|.)[\u4E00-\u9FA5]{2,})*$/
    },
    legalIdcard: {
        empty: '请输入授权结算人身份证号',
        error: '请输入正确的授权结算人身份证号',
        reg: /(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    },
    bankCard: {
        empty: '请输入银行卡号',
        error: '银行卡号格式不正确',
        reg: /^\d{6,}$/
    },
    merchantNumber: {
        empty: '请输入商户编号',
        error: '请输入正确的商户编号',
        reg: /^\d{10}$/
    },
    verificationCode: {
        empty: '请输入验证码',
        error: '请输入正确的验证码',
        reg: /^\d{6}$/
    },
    effectiveDate: {
        empty: '请输入有效日期',
        error: '请输入正确的有效日期',
        reg: /^\d{6}$/
    },
    safeCode: {
        empty: '请输入信用卡安全码',
        error: '请输入正确的信用卡安全码',
        reg: /^\d{3}$/
    },
    recommendetPhone: {
        empty: '请输入合伙人手机号',
        error: '请输入正确的合伙人手机号',
        reg: /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|18[0-9]|17[0-9])[0-9]{8}$/
    },
    accountName: {
        empty: '请输入开户名',
        error: '请输入正确的开户名',
        reg: ""
    },
    password: {
        empty: '请输入密码',
        error: '请输入正确的密码',
        reg: /^[A-Za-z0-9]{6,15}$/
    },
    password2: {
        empty: '请输入重复密码',
        error: '请输入正确的重复密码',
        reg: /^[A-Za-z0-9]{6,15}$/
    },
    password3: {
        empty: '请输入密码',
        error: '请输入6到18位字母加数字组合,且不包含特殊符号',
        reg: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,18}$/
    },
    allNum: {
        empty: '',
        error: '请输入数字',
        reg: /^[0-9]*$/
    }
};

/***/ }),
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

//游客身份下要求username为固定前缀USER:加32位设备唯一号，
//例如USER:BDAF6B4D-5DC0-4AEF-BCF8-6C7EFC94DE97，password为固定字符串TOURIST
//密码(游客)
var PASSWORD = "TOURIST";
var NO_SMS = "NO_SMS"; //无需验证码
var PHONE = "phone"; //手机号需要验证码
var PREFIX = PHONE;

var tokenConfig = _apiConfig2.default.tokenConfig;
// 权限控制

var SCOPE = tokenConfig.scope; // "select";
//授权模式
var GRANT_TYPE = tokenConfig.grantTypes; //"password";
//客户端标识
var CLIENT_ID = tokenConfig.clientId; // "client_youshua";
//认证客户端标识
var CLIENT_SECRET = tokenConfig.clientSecret; // "5274206178d44e5cad27f6af21cf50dd";
var BUSITYPE = tokenConfig.requestBusiType; // "member";

//定义公钥
// const publicKey="305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001";
var getWay = {};
getWay.getAuthParams = function (uuid, password) {
    var tempParams = {
        username: uuid,
        password: password != null ? password : PASSWORD
    };
    if (tempParams.username.indexOf(PHONE) > -1 || tempParams.username.indexOf(NO_SMS) > -1) {
        tempParams.loginType = tempParams.username.split(":")[0]; //phone:手机号 拆成两个参数
        tempParams.phoneNo = tempParams.username.split(":")[1];
        tempParams.smsCode = tempParams.password;
        delete tempParams.password;
    }
    var msg = JSON.stringify(tempParams);
    var params = {
        busiType: BUSITYPE,
        username: uuid,
        password: password != null ? password : PASSWORD,
        client_secret: CLIENT_SECRET,
        msg: msg
    };
    // window.params = params;
    return params;
};

function parseUrl(str) {
    var reg = /([^=&]*)=([^=&]*)/g;
    var obj = {};
    str.replace(reg, function () {
        var key = arguments[1];
        var val = arguments[2];
        obj[key] = val;
    });
    return obj;
}

function getwayErrorMsg(code) {
    switch (code) {
        case "0001":
            return "未登录";
        case "0002":
            return "未绑定手机号";
        case "0003":
            return "验证码错误";
        case "0004":
            return "手机号已被绑定";
        case "0005":
            return "参数错误";
        case "0006":
            return "手机号格式错误";
        case "0007":
            return "接入方未绑定";
        case "0008":
            return "已绑定";
        case "0009":
            return "集团接口异常";
        case "9999":
            return "请求失败";
        default:
            return "未知错误";
    }
}

//解密token等信息
getWay.decipheringToken = function (decipheringJson) {
    //解密
    var data = decipheringJson;
    var tempData = {};
    for (var key in data) {
        if (key === 'access_token' || key === 'refresh_token' || key === 'fault') {
            tempData[key] = common.decryptpByRSA(data[key], _apiConfig2.default.tokenConfig.publicKey);
        } else {
            tempData[key] = data[key];
        }
    }
    data = tempData;
    // debugger
    console.log("tempData", tempData);
    // let jsonStr=common.decryptpByRSA(decipheringStr,api.tokenConfig.publicKey);
    // let data = JSON.parse(jsonStr);
    window.data = data;
    console.log("解密后数据", data);
    var loginType = common.getCookie('LoginType') || sessionStorage.getItem('LoginType');

    //解密后保存到本地
    common.setCookie("ACCESSTOKEN", common.encryptByAES(data.access_token, data.fault));
    sessionStorage.setItem("ACCESSTOKEN", common.encryptByAES(data.access_token, data.fault));
    //设置为未登录
    common.setCookie("isLogin", false); //loginType === 'phone' ? true : false 这里默认先写死 之后要判断登录和失效 和退出
    sessionStorage.setItem("isLogin", false);
    // console.log(",data.access_token"+data.access_token)
    common.setCookie("AESKEY", data.fault);
    sessionStorage.setItem("AESKEY", data.fault);

    // alert(data.access_token);

    common.setCookie("REFRESH_TOKEN", common.encryptByAES(data.refresh_token, data.fault));
    sessionStorage.setItem("REFRESH_TOKEN", common.encryptByAES(data.refresh_token, data.fault));

    //执行回调
    if (window.cb && typeof window.cb === 'function') {
        console.log('执行回调####');
        // setTimeout(() => {
        window.cb(); //执行回调
        // }, 1000)
    }
    if (loginType === PHONE || loginType === NO_SMS) {
        //获取用户信息
        common.XNAjax({
            "url": _apiConfig2.default.KY_USER_IP + "/resource/vip-user/user/getLoginData",
            "type": "post",
            "data": {}
        }, function (res) {
            // console.log(res);
            var data = res.data,
                code = res.code;

            if (code === '0000') {
                common.setCookie("UUID", data.uuid);
                sessionStorage.setItem("UUID", data.uuid);
                common.setCookie("isLogin", true);
                sessionStorage.setItem("isLogin", true);
            }
        });
    }
};

getWay.getEncryptObjByRSA = function (encryptParams) {
    // console.log("APP返回..加密后返回....",encryptParams);
    var params = encryptParams;
    params.grant_type = GRANT_TYPE; //password
    params.scope = SCOPE; //写死 select
    params.client_id = CLIENT_ID; //明文
    console.log("params", params);
    common.AjaxToken({
        "url": _apiConfig2.default.KY_USER_IP + "/oauth/token",
        "type": "POST",
        // "sesstionType":"true",
        "data": params
    }, function (data) {
        //开始解密调用原生方法
        getWay.decipheringToken(data);
    });
};

//向原生交互获取游客或正式用户token
getWay.postToken = function (phone, code, cb) {
    console.log("postToken...");
    var uuid = "USER:111111111111111111111111111111111";
    var loginType = "user";
    if (phone) {
        if (code === "any") {
            //无需验证码登录
            uuid = NO_SMS + ":" + phone;
            loginType = NO_SMS;
        } else {
            uuid = PHONE + ":" + phone;
            loginType = PHONE;
        }
    }
    common.setCookie("LoginType", loginType);
    window.sessionStorage.setItem("LoginType", loginType);
    //全局注册回调函数
    console.log("loginType:::::" + loginType);
    if (cb && typeof cb === 'function') {
        console.log('注册函数...');
        window.cb = cb;
    }
    //获取授权需要加密的信息
    var params = this.getAuthParams(uuid, code);
    // console.log("加密前params:::::",params);
    var encryptParams = {};
    for (var key in params) {
        encryptParams[key] = common.encryptByRSA(params[key], _apiConfig2.default.tokenConfig.publicKey);
    }
    this.getEncryptObjByRSA(encryptParams);
};
exports.default = getWay;

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

var _vue = __webpack_require__(10);

var _vue2 = _interopRequireDefault(_vue);

var _vueRouter = __webpack_require__(123);

var _vueRouter2 = _interopRequireDefault(_vueRouter);

var _App = __webpack_require__(115);

var _App2 = _interopRequireDefault(_App);

var _weVue = __webpack_require__(107);

var _vueScroller = __webpack_require__(124);

var _vueScroller2 = _interopRequireDefault(_vueScroller);

var _creditCard = __webpack_require__(109);

var _creditCard2 = _interopRequireDefault(_creditCard);

var _register = __webpack_require__(111);

var _register2 = _interopRequireDefault(_register);

var _applyRecord = __webpack_require__(108);

var _applyRecord2 = _interopRequireDefault(_applyRecord);

var _machinesToolsApply = __webpack_require__(110);

var _machinesToolsApply2 = _interopRequireDefault(_machinesToolsApply);

var _bus = __webpack_require__(46);

var _bus2 = _interopRequireDefault(_bus);

var _share = __webpack_require__(117);

var _share2 = _interopRequireDefault(_share);

var _login = __webpack_require__(120);

var _login2 = _interopRequireDefault(_login);

var _login3 = __webpack_require__(119);

var _login4 = _interopRequireDefault(_login3);

var _api = __webpack_require__(24);

var _common = __webpack_require__(15);

var _common2 = _interopRequireDefault(_common);

var _filters = __webpack_require__(105);

var _userPayDialog = __webpack_require__(122);

var _userPayDialog2 = _interopRequireDefault(_userPayDialog);

var _swiper = __webpack_require__(118);

var _swiper2 = _interopRequireDefault(_swiper);

var _login5 = __webpack_require__(116);

var _login6 = _interopRequireDefault(_login5);

var _verifiCode = __webpack_require__(121);

var _verifiCode2 = _interopRequireDefault(_verifiCode);

var _vant = __webpack_require__(66);

var _vant2 = _interopRequireDefault(_vant);

var _index = __webpack_require__(114);

var _index2 = _interopRequireDefault(_index);

var _reward = __webpack_require__(112);

var _reward2 = _interopRequireDefault(_reward);

var _shop = __webpack_require__(113);

var _shop2 = _interopRequireDefault(_shop);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
// import pushHandShare from '@/page/vipUser/pushHandShare.vue';


_vue2.default.prototype.$startPhone = _filters.startPhone;
//去犀牛会员购买


_vue2.default.use(_vueScroller2.default);
_vue2.default.use(_weVue.Picker);
_vue2.default.use(_weVue.DatetimePicker);

_vue2.default.use(_vant2.default);

_vue2.default.component("share", _share2.default);
// Vue.component("pushHandShare", pushHandShare);
//去犀牛会员购买
_vue2.default.component("userPayDialog", _userPayDialog2.default);
_vue2.default.component("login", _login2.default);
_vue2.default.component("loginWx", _login4.default);
_vue2.default.component("pushHandLogin", _login6.default);
_vue2.default.component("swiper", _swiper2.default);
_vue2.default.component("verifiCode", _verifiCode2.default);

//手机号指令
_vue2.default.directive('enterNumber', {
    inserted: function inserted(el) {
        el.addEventListener("keypress", function (e) {
            e = e || window.event;
            var charcode = typeof e.charCode == 'number' ? e.charCode : e.keyCode;
            var re = /\d/;
            if (!re.test(String.fromCharCode(charcode)) && charcode > 9 && !e.ctrlKey) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                }
            }
        });
    }
});
//结局ios下input错误问题指令
_vue2.default.directive('iosInputBug', {
    inserted: function inserted(el) {
        if ((0, _common.isClient)() === "ios") {
            //ios下指令生效
            var temp = void 0;
            el.onfocus = function () {
                temp = document.body.scrollHeight;
            };
            el.onblur = function () {
                setTimeout(function () {
                    document.body.scrollTop = temp;
                    document.activeElement.scrollIntoViewIfNeeded(true);
                }, 100);
                // toast({
                //     content:"焦点移除"
                // })
            };
        }
    }
});
_vue2.default.directive('iosInputBug2', {
    inserted: function inserted(el) {
        if ((0, _common.isClient)() === "ios") {
            //ios下指令生效
            var temp = void 0;
            el.onfocus = function () {
                temp = 0; // document.body.scrollHeight;
                document.body.scrollTop = temp;
                // alert(1);
                // console.log('=====');
                // toast({
                //     content:'===='
                // })
            };
            el.onblur = function () {
                setTimeout(function () {
                    document.body.scrollTop = temp;
                    document.activeElement.scrollIntoViewIfNeeded(true);
                }, 100);
                // toast({
                //     content:"焦点移除"
                // })
            };
        }
    }
});

_vue2.default.filter('textOverflow', function (val, max) {
    if (val) {
        var len = val.length;
        var maxNum = max == null ? 20 : max;
        return len > maxNum ? val.substr(0, maxNum) + "..." : val;
    } else {
        return val;
    }
});

//手机号隐藏过滤器
_vue2.default.filter('hidePhoneNo', function (val) {
    if (val) {
        var len = val.length;
        return val.substring(0, 3) + "****" + val.substring(len - 4, len);
    } else {
        return val;
    }
});

_vue2.default.mixin({
    // data(){
    //   return{
    //       imgUrl:KY_IP
    //   }
    // },
    mounted: function mounted() {
        var _this = this;

        setTimeout(function () {
            _this.setImgWidth();
        }, 1000);
    },

    methods: {
        //给商城详情页面设置宽度
        setImgWidth: function setImgWidth() {
            try {
                var images = document.querySelectorAll('.shop_pics img');
                for (var i = 0; i < images.length; i++) {
                    images[i].style.width = "100%";
                }
            } catch (e) {}
        },
        getImg: function getImg(url) {
            // alert(1);
            var imgUrl = _api.KY_IP;
            if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
                return url = url;
            } else {
                return url = imgUrl + 'file/downloadFile?filePath=' + url;
            }
        },

        //关闭原生app页面
        closeNativeApp: function closeNativeApp() {
            var _os = (0, _common.isClient)();
            if (_os === "ios") {
                // console.log("nativeCloseCurrentPage");
                window.webkit.messageHandlers.nativeCloseCurrentPage.postMessage("");
            } else {
                window.android.nativeCloseCurrentPage("");
            }
        },
        nativeOpenPage: function nativeOpenPage(url) {
            var isNeedClosePage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "false";

            var _os = (0, _common.isClient)();
            var _dataJson = {
                isH5: true,
                path: url,
                isNeedClosePage: isNeedClosePage
            };
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeOpenPage.postMessage(JSON.stringify(_dataJson));
            } else {
                window.android.nativeOpenPage(JSON.stringify(_dataJson));
            }
        },

        //改变webview尺寸
        changeWebViewSize: function changeWebViewSize(w, h, cb) {
            var _os = (0, _common.isClient)();
            var json = {
                "width": w,
                "height": h,
                "callbackName": cb
            };
            console.log("nativeChangeDialog.....");
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeChangeDialog.postMessage(JSON.stringify(json));
            } else {
                window.android.nativeChangeDialog(JSON.stringify(json));
            }
        },

        //判断是否安装了某个应用
        nativeIsInstalledApp: function nativeIsInstalledApp(callbackName) {
            var _os = (0, _common.isClient)();
            var pak = _os === "ios" ? "FSVipApp://" : "com.xinxiaoyao.blackrhino";
            var json = {
                "appIdentifier": pak, //包名或其他约定的字段
                "callbackName": callbackName
            };
            console.log("nativeIsInstalledApp.....");
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeIsInstalledApp.postMessage(JSON.stringify(json));
            } else {
                window.android.nativeIsInstalledApp(JSON.stringify(json));
            }
        },

        //打开android应用
        nativeOpenApp: function nativeOpenApp(callbackName) {
            // let _os = isClient();
            var json = {
                "pkg": "com.xinxiaoyao.blackrhino",
                "schema": "",
                "callbackName": callbackName
            };
            window.android.nativeOpenApp(JSON.stringify(json));
        },

        //iso打开犀牛会员app
        nativeOpenBrowser: function nativeOpenBrowser() {
            var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "FSVipApp://";

            var json = { url: url };
            window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify(json));
        },

        //推手显示引导
        nativeShowAppHomeGuide: function nativeShowAppHomeGuide() {
            var _os = (0, _common.isClient)();
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeShowAppHomeGuide.postMessage("");
            } else {
                window.android.nativeShowAppHomeGuide();
            }
        }
    }
});

var routes = [].concat(_toConsumableArray(_applyRecord2.default), _toConsumableArray(_machinesToolsApply2.default), _toConsumableArray(_creditCard2.default), _toConsumableArray(_register2.default), _toConsumableArray(_reward2.default), _toConsumableArray(_shop2.default));

// console.log(routes);

_vue2.default.use(_vueRouter2.default);
var router = new _vueRouter2.default({
    base: __dirname,
    routes: routes
}); //这里可以带有路由器的配置参数

router.beforeEach(function (to, from, next) {

    var toPath = to.name;

    _bus2.default.$emit('isLoading', true);

    if (toPath == "share") {
        document.body.setAttribute("style", "background:#757a9b");
    } else if (toPath == "upgradeRight") {
        document.body.setAttribute("style", "background:#f4f5fb");
    } else if (toPath == "upgradeRight_ad") {
        document.body.setAttribute("style", "background:#4082fc");
    } else if (toPath == "question_detail" || toPath == "regist_agreement" || toPath == "material_detail" || toPath == "downloadCenter" || toPath == "totalIncome" || toPath == "totalIncomeDetail" || toPath == "myTeam" || toPath == "myTeamDetail" || toPath == "payResultXYTS" || toPath == "giftBag" || toPath == "handBagDetail") {
        document.body.setAttribute("style", "background:#ffffff");
    } else if (toPath == "feedBack" || toPath == "feedBack_service" || toPath == "feedBack_help" || toPath == "mall" || toPath == "mall_order" || toPath == "addressList") {
        document.body.setAttribute("style", "background:#f4f5fb");
    } else if (toPath == "youshua") {
        document.body.setAttribute("style", "background:#5C64DC");
    } else if (toPath == "hebao") {
        document.body.setAttribute("style", "background:#FF5431");
    } else if (toPath == "xinghui" || toPath == "pusher") {
        document.body.setAttribute("style", "background:#3494FF");
    } else if (toPath == "applyLoan") {
        document.body.setAttribute("style", "background:#fafbff");
    } else if (toPath == "mallShop" || toPath == "caseDetail" || toPath == "caseDetail" || toPath == "policy" || toPath == "policyDetail" || toPath == "recoLoan" || toPath == "regZfy" || toPath == "cxReg" || toPath == "video" || toPath == "dayShare" || toPath == "rewardCenter" || toPath == "regZdyqSuccess" || toPath == "tradeVolume" || toPath == "myMachines" || toPath == "getRecords" || toPath == "conTransfer" || toPath == "transRecords" || toPath == "yszhsft" || toPath == "addAdress" || toPath == "adressList" || toPath == "editorAdress") {
        document.body.setAttribute("style", "background:#ffffff");
    } else if (toPath == "crabShop" || toPath == "register" || toPath == "changePending" || toPath == "changeAgreed" || toPath == "changeRejected") {
        document.body.setAttribute("style", "background:#f6f6f6");
    } else if (toPath == "applyRecord") {
        document.body.setAttribute("style", "background:#F7F8FA");
    } else {
        document.body.setAttribute("style", "background:#f9faff");
    }

    document.title = to.meta.title;

    next();
});

router.afterEach(function (to, from) {
    _bus2.default.$emit('isLoading', false);
});

// 路由器会创建一个 App 实例，并且挂载到选择符 #app 匹配的元素上。
var app = new _vue2.default({
    router: router,
    store: _index2.default,
    render: function render(h) {
        return h(_App2.default);
    }
}).$mount('#app');
/* WEBPACK VAR INJECTION */}.call(exports, "/"))

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.editFirstEnter = exports.lastBuyGoods = exports.getPushIdAndName = exports.getPushHandInfo = exports.shareQrCode = exports.getShopDetail = exports.shopList = undefined;

var _api = __webpack_require__(24);

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

//查询商品列表信息
/** 2019-09-09
 *作者:heliang
 *功能: 商品信息查询
 */
function shopList() {
    var url = _api.KY_IP + "explosiveProducts/list";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'get',
            data: {},
            showLoading: false
        }, function (res) {
            console.log("推手商品列表 shopList ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手商品列表 shopList *************", res);
            reject(res);
        });
    });
}

//获取商品详情
function getShopDetail(productId, type) {
    var url = _api.KY_IP + "rhinoMembership/productDetails";
    if (type == null) {
        type = '';
    }
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'post',
            data: {
                productSPU: productId,
                type: type
            },
            showLoading: false
        }, function (res) {
            console.log("推手商品详情 getShopDetail ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手商品详情 getShopDetail *************", res);
            reject(res);
        });
    });
}

//推手相关信息
function getPushHandInfo(userNo) {
    var url = _api.KY_IP + "rhinoMembership/getUserInfo";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'post',
            data: {
                userNo: userNo
            },
            showLoading: false
        }, function (res) {
            console.log("推手相关信息 getPushHandInfo ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手相关信息 getPushHandInfo *************", res);
            reject(res);
        });
    });
}

//获取推荐人名称和Id
function getPushIdAndName(productId) {
    var url = _api.KY_IP + "rhinoMembership/data";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'post',
            data: { productId: productId },
            showLoading: false
        }, function (res) {
            console.log("获取推荐人名称和Id getParentName ######################", res);
            resolve(res);
        }, function (res) {
            console.log("获取推荐人名称和Id getParentName *************", res);
            reject(res);
        });
    });
}

//商品推广二维码
function shareQrCode(productId) {
    var url = _api.KY_IP + "news/shareQrCode";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'get',
            data: {
                productCode: productId
            },
            showLoading: false
        }, function (res) {
            console.log("推手商品列表 shopList ######################", res);
            resolve(res);
        }, function (res) {
            console.log("推手商品列表 shopList *************", res);
            reject(res);
        });
    });
}

//app立即前往埋点
function lastBuyGoods(goodsNo) {
    var url = _api.KY_IP + "rhinoMembership/lastBuyGoods";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'POST',
            data: { goodsNo: goodsNo },
            showLoading: false
        }, function (res) {
            console.log("app立即前往埋点 lastBuyGoods ######################", res);
            resolve(res);
        }, function (res) {
            console.log("app立即前往埋点 lastBuyGoods *************", res);
            reject(res);
        });
    });
}

//商品列表页告诉后端是否点击过
function editFirstEnter() {
    var url = _api.KY_IP + "explosiveProducts/editFirstEnter";
    return new Promise(function (resolve, reject) {
        common.Ajax({
            url: url,
            type: 'get',
            data: {},
            showLoading: false
        }, function (res) {
            console.log("商品列表页告诉后端是否点击过 editFirstEnter #############", res);
            resolve(res);
        }, function (res) {
            console.log("商品列表页告诉后端是否点击过 editFirstEnter *************", res);
            reject(res);
        });
    });
}

exports.shopList = shopList;
exports.getShopDetail = getShopDetail;
exports.shareQrCode = shareQrCode;
exports.getPushHandInfo = getPushHandInfo;
exports.getPushIdAndName = getPushIdAndName;
exports.lastBuyGoods = lastBuyGoods;
exports.editFirstEnter = editFirstEnter;

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _loading = __webpack_require__(283);

var _loading2 = _interopRequireDefault(_loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    components: {
        Loading: _loading2.default
    },
    mounted: function mounted() {
        ;/iphone|ipod|ipad/i.test(navigator.appVersion) && document.addEventListener('blur', function (e) {
            // 这里加了个类型判断，因为a等元素也会触发blur事件
            ['input', 'textarea'].includes(e.target.localName) && document.body.scrollIntoView(false);
        }, true);
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _bus = __webpack_require__(46);

var _bus2 = _interopRequireDefault(_bus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	data: function data() {
		return {
			isLoading: false
		};
	},
	mounted: function mounted() {
		var _this = this;

		_bus2.default.$on('isLoading', function (content) {
			_this.isLoading = content;
		});
	}
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//

// import Swiper from 'swiper';
//import './css/swiper.css';
exports.default = {
    data: function data() {
        return {};
    },

    props: {
        'swiper_config': Object
    },
    mounted: function mounted() {

        var me = this;

        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            bulletActiveClass: me.swiper_config.bulletsColor ? 'my-bullet-active' : 'swiper-pagination-bullet-active',
            paginationClickable: false,
            loop: true,
            speed: 600,
            autoplay: 3000,
            autoplayDisableOnInteraction: false,
            observer: true,
            observeParents: true,
            onAutoplay: function onAutoplay() {
                $('.swiper-pagination-bullet').css('background', '#A8A8A8');
                $(".my-bullet-active").css({
                    'background': me.swiper_config.bulletsColor
                });
            },
            onSlideChangeStart: function onSlideChangeStart() {
                $('.swiper-pagination-bullet').css('background', '#A8A8A8');
                $(".my-bullet-active").css({
                    'background': me.swiper_config.bulletsColor
                });
            },
            onInit: function onInit(swiper) {
                setTimeout(function () {
                    $('.swiper-pagination-bullet').css({ 'background': '#A8A8A8', opacity: .8 });
                    $(".my-bullet-active").css({
                        'background': me.swiper_config.bulletsColor
                    });
                }, 500);
            }
        });

        // $(".my-bullet-active").css({
        //     'background':me.swiper_config.bulletsColor
        // });
    }
};

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _apiConfig = __webpack_require__(24);

var _apiConfig2 = _interopRequireDefault(_apiConfig);

var _common = __webpack_require__(15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	data: function data() {
		return {
			imgUrl: "",
			content: "",
			cancelText: "",
			confirmText: "",
			vCode: ""
		};
	},

	props: {
		isShowImgCode: Boolean,
		phoneNo: String,
		confirmText: {
			type: String,
			default: '确认'
		},
		cancelText: {
			type: String,
			default: '取消'
		}
	},
	watch: {
		isShowImgCode: function isShowImgCode(newVal) {
			//犀牛的图形验证码
			if (this.phoneNo && newVal) {
				this.refresh();
			}
		}
	},
	methods: {
		confirmSubmit: function confirmSubmit(e) {
			var _this = this;

			//为了防止用户不点击完成直接点击按钮 这里获取一下焦点以防止指令失效
			e.target.focus();
			if (this.vCode === "") {
				(0, _common.toast)({
					content: '请输入图形验证码'
				});
				return;
			}
			//发送短信验证码和校验当前图形验证码是否正确
			(0, _common.Ajax)({
				"url": _apiConfig2.default.KY_IP + "/resource/vip-user/userCode/getSmsVerificationCode",
				"type": "POST",
				"data": {
					"phoneNo": this.phoneNo,
					"vCode": this.vCode
				}
			}, function (data) {
				// toast({content:data.code});
				if (data.code === "0000") {
					//确认验证码开始倒计时
					_this.$emit('confirmCode');
					_this.$emit('confirm', _this);
					_this.$emit('update:isShowImgCode', false);
					_this.vCode = "";
				}
			});
		},
		refresh: function refresh() {
			var _this2 = this;

			//发送图形验证码
			(0, _common.Ajax)({
				"url": _apiConfig2.default.KY_IP + "/resource/vip-user/userCode/getImgVerificationCode",
				"type": "get",
				"data": { phoneNo: this.phoneNo }
			}, function (res) {
				var code = res.code,
				    data = res.data,
				    msg = res.msg;

				_this2.imgUrl = "data:image/jpg;base64," + data.img;
			});
		},
		cancelBox: function cancelBox(e) {
			//为了防止用户不点击完成直接点击按钮 这里获取一下焦点以防止指令失效
			e.target.focus();
			this.$emit('cancel', this);
			this.$emit('update:isShowImgCode', false);
			document.body.removeEventListener("touchmove", bodyScroll, false);
		}
	}
};

function bodyScroll(event) {
	event.preventDefault();
}

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var _loginQuery = __webpack_require__(47);

exports.default = {
    props: {
        isPushHandShow: {
            type: Boolean,
            default: false
        },
        parentUserNo: {
            type: String,
            default: ""
        },
        parentUserName: {
            type: String,
            default: ""
        },
        headImagePath: {
            type: String,
            default: ""
        }
    },
    data: function data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        };
    },

    watch: {
        isStartTime: function isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    methods: {
        openAgreement: function openAgreement(url) {},
        closePage: function closePage() {
            this.$emit('update:isPushHandShow', false);
            this.phone = '';
            this.vCode = '';
        },

        //改变状态开始发送验证码
        changeStartTime: function changeStartTime() {
            this.isStartTime = true;
        },

        //timer处理函数
        setRemainTimes: function setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode: function sendCode() {
            var _this = this;

            var isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //发送验证码
                (0, _loginQuery.pushHandRegSms)(this.phone).then(function (res) {
                    var msg = res.msg;

                    (0, _common.toast)({
                        content: msg
                    });
                    _this.isStartTime = true;
                    // this.isShowImgCode = true;
                });
            }
        },
        validatePhone: function validatePhone() {
            if (!this.phone) {
                (0, _common.toast)({
                    "content": "请输入手机号"
                });
                return false;
            }
            var str = _common.validator.checkPhoneNumber(this.phone);
            if (str) {
                (0, _common.toast)({
                    content: str.replace("您", '')
                });
                return false;
            }
            return true;
        },
        validateInfo: function validateInfo() {
            var flag = this.validatePhone();
            var temp = true;
            if (flag) {
                if (this.vCode === "") {
                    (0, _common.toast)({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }

            return temp && flag;
        },
        startTimer: function startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        openUser: function openUser() {
            var _this2 = this;

            // alert("推手注册或登录...");
            if (this.validateInfo()) {
                //注册推手
                (0, _loginQuery.pushHandReg)(this.phone, this.vCode, this.parentUserNo).then(function (res) {
                    // toast({
                    //     "content":res.msg
                    // });
                    var loginKey = res.data.loginKey;

                    _this2.closePage();
                    sessionStorage.setItem("loginKey", loginKey);
                    _this2.$router.push('/handBagDetail');
                });
            }
        }
    }
};

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var common = _interopRequireWildcard(_common);

var _html2canvas = __webpack_require__(175);

var _html2canvas2 = _interopRequireDefault(_html2canvas);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.default = {
    props: {
        visible: {
            type: Boolean,
            default: false
        }
    },
    watch: {
        visible: function visible(newVal, oldVal) {
            if (newVal !== oldVal) {
                this.showDia = newVal;
                this.show1 = newVal;
            }
        },
        showDia: function showDia(newVal, oldVal) {
            if (newVal !== oldVal) {
                // document.body.scrollTop = 0;
                // document.documentElement.scrollTop = 0;
                if (newVal === true) {
                    (document.documentElement || document.body).style.height = (document.documentElement || document.body).clientHeight + 'px';
                    document.querySelector('#page').style.height = (document.documentElement || document.body).clientHeight + 'px';
                    (document.documentElement || document.body).style.overflow = "hidden";
                    document.querySelector('#page').style.overflow = "hidden";
                    this.$emit("update:visible", newVal);
                } else {
                    document.querySelector('#page').style.height = "auto";
                    (document.documentElement || document.body).style.height = "auto";
                    document.querySelector('#page').style.overflow = "auto";
                    (document.documentElement || document.body).style.overflow = "auto";
                    this.$emit("update:visible", newVal);
                }
            }
        }
    },
    data: function data() {
        return {
            isLoading: true,
            showDia: false,
            show1: false,
            htmlUrl: ""
        };
    },

    components: {
        html2canvas: _html2canvas2.default
    },
    activated: function activated() {
        window.saveImg = this.saveImg;
        // this.getHeight();
        // common.youmeng("每日一推","进入每日一推");
    },
    mounted: function mounted() {
        window.goBack = this.goBack;
        // this.getHeight();
        window.shareSucces = this.shareSucces;
        // setTimeout(()=>{
        //    this.openShare();
        //     console.log("===showDia");
        // },1000)
    },

    methods: {
        openShare: function openShare() {
            this.showDia = true;
            this.show1 = true;
        },
        closeShare: function closeShare() {
            this.showDia = false;
            this.show1 = false;
        },

        // reloadImg(){
        //     let shareImgPathPath= document.getElementById("shareImgPath").src;
        //     let ext=shareImgPathPath.substring(shareImgPathPath.indexOf('&temp='),shareImgPathPath.length);
        //     shareImgPathPath=shareImgPathPath.replace(ext,'')+'&temp='+Math.random();
        //     let codePath= document.getElementById("code").src;
        //     let ext2=shareImgPathPath.substring(shareImgPathPath.indexOf('&temp='),shareImgPathPath.length);
        //     codePath=codePath.replace(ext2,'')+'&temp='+Math.random();
        //
        //     document.getElementById("shareImgPath").src=shareImgPathPath;
        //     document.getElementById("code").src=codePath;
        //
        // },
        toImage: function toImage(type) {
            var _this = this;

            common.loading("show");
            // 第一个参数是需要生成截图的元素,第二个是自己需要配置的参数,宽高等
            var content = this.$refs.imageTofile;
            // this.reloadImg();
            // setTimeout(()=>{
            (0, _html2canvas2.default)(content, {
                // logging:true,
                useCORS: true
            }).then(function (canvas) {
                var url = canvas.toDataURL('image/png');
                // let img=new Image();
                // img.src=url;
                // document.body.appendChild(canvas);
                console.log(url);
                _this.htmlUrl = url;
                common.loading("hide");
                if (type === "wechatSession") {
                    _this.wechatSession();
                } else if (type === "wechatTimeline") {
                    _this.wechatTimeline();
                } else if (type === "nativeSaveImage") {
                    _this.nativeSaveImage();
                }
            }).catch(function (err) {
                console.log('err:::::', err);
            });
            // },500)
        },
        wechatSession: function wechatSession() {
            // this.toImage();
            var thumbnail = this.htmlUrl;
            var shareJson = {
                "type": "wechatSession",
                "base64String": thumbnail,
                "shareType": "img",
                "callbackName": "shareSucces"
            };
            if (common.isClient() === "ios") {
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShareBase64(JSON.stringify(shareJson));
            }
        },
        wechatTimeline: function wechatTimeline() {
            // this.toImage();
            var thumbnail = this.htmlUrl;
            var shareJson = {
                "type": "wechatTimeline",
                "base64String": thumbnail,
                "shareType": "img",
                "callbackName": "shareSucces"
            };
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage(JSON.stringify(shareJson));
            } else {
                window.android.nativeWechatShareBase64(JSON.stringify(shareJson));
            }
        },
        nativeSaveImage: function nativeSaveImage() {
            // this.toImage();
            var imgJson = {};
            imgJson.base64String = this.htmlUrl;
            imgJson.callbackName = "saveImg";
            console.log(imgJson);
            if (common.isClient() == "ios") {
                window.webkit.messageHandlers.nativeSaveImageBase64.postMessage(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            } else {
                window.android.nativeSaveImageBase64(JSON.stringify(imgJson));
                console.log("imgJson:" + JSON.stringify(imgJson));
            }
        },
        saveImg: function saveImg(j) {
            console.log("返回状态值：" + j);
            if (j == true || j == 0) {
                console.log("保存成功");
            } else {
                console.log("保存失败");
            }
        },
        shareSucces: function shareSucces() {
            window.location.reload();
            //            this.showDia = false;
            //            this.show1=false;
        }
    }
};

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var _getwayHelper = __webpack_require__(130);

var _getwayHelper2 = _interopRequireDefault(_getwayHelper);

var _loginQuery = __webpack_require__(47);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: {
        isShow: {
            type: Boolean,
            default: false
        },
        isLogin: { //是否调用匿名登录
            type: Boolean,
            default: false
        }
    },
    data: function data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        };
    },

    watch: {
        isStartTime: function isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    mounted: function mounted() {
        // if(this.isLogin==true){ //需要调用自动登录
        //     let isLogin = getCookie('isLogin');
        //     //判断是否登录
        //     if (isLogin==="false"||isLogin==null) {
        //         //匿名登录
        //         getWayHelper.postToken(null, null, () => {
        //             console.log('匿名登录....');
        //
        //         });
        //     }
        // }
    },

    methods: {
        openAgreement: function openAgreement(url) {},

        //改变状态开始发送验证码
        changeStartTime: function changeStartTime() {
            this.isStartTime = true;
        },

        //timer处理函数
        setRemainTimes: function setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode: function sendCode() {
            var _this = this;

            var isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //发送验证码了
                (0, _loginQuery.pushHandSms)(this.phone).then(function (res) {
                    var msg = res.msg;

                    (0, _common.toast)({
                        content: msg
                    });
                    _this.isStartTime = true;
                    // this.isShowImgCode = true;
                });
            }
        },
        validatePhone: function validatePhone() {
            if (!this.phone) {
                (0, _common.toast)({
                    "content": "请输入手机号"
                });
                return false;
            }
            var str = _common.validator.checkPhoneNumber(this.phone);
            if (str) {
                (0, _common.toast)({
                    content: str.replace("您", '')
                });
                return false;
            }
            return true;
        },
        validateInfo: function validateInfo() {
            var flag = this.validatePhone();
            var temp = true;
            if (flag) {
                if (this.vCode === "") {
                    (0, _common.toast)({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }
            return temp && flag;
        },
        startTimer: function startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        openUser: function openUser() {
            // alert("会员登录...");
            // let parentUserNo= window.sessionStorage.getItem("parentUserNo");
            // if(parentUserNo==null||parentUserNo===''){
            //     toast({
            //        content:'推手编号为空'
            //     });
            //     return ;
            // }
            //调用登录
            (0, _loginQuery.pushHand2User)(this.phone, this.vCode).then(function (res) {
                (0, _common.toast)({
                    "content": res.msg
                });
            });
            // userLogin(parentUserNo,this.phone,this.vCode).then(()=>{
            //
            //     this.$emit('update:isShow',false);
            // });
        }
    }
};

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var _loginQuery = __webpack_require__(47);

exports.default = {
    props: {

        isNeedGuide: {
            type: Boolean,
            default: false //是否显示引导
        },
        isFirst: {
            type: Boolean, //是否首页进入
            default: true
        },
        cb: {
            type: Function,
            default: function _default() {}
        }
    },
    data: function data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        };
    },

    watch: {
        isStartTime: function isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    methods: {
        openAgreement: function openAgreement(url) {
            this.nativeOpenPage(url);
        },

        //改变状态开始发送验证码
        changeStartTime: function changeStartTime() {
            this.isStartTime = true;
        },

        //timer处理函数
        setRemainTimes: function setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode: function sendCode() {
            var _this = this;

            var isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //告诉外边要发送验证码了
                // this.$emit('sendImgCode', true, this.phone);
                //发送验证码
                (0, _loginQuery.pushHandSms)(this.phone).then(function (res) {
                    (0, _common.toast)({
                        content: "验证码已发送"
                    });
                    _this.isStartTime = true;
                    // this.isShowImgCode = true;
                });
            }
        },
        validatePhone: function validatePhone() {
            if (!this.phone) {
                (0, _common.toast)({
                    "content": "请输入手机号"
                });
                return false;
            }
            var str = _common.validator.checkPhoneNumber(this.phone);
            if (str) {
                (0, _common.toast)({
                    content: str.replace("您", '')
                });
                return false;
            }
            return true;
        },
        validateInfo: function validateInfo() {
            var flag = this.validatePhone();
            var temp = true;
            if (flag) {
                if (this.vCode === "") {
                    (0, _common.toast)({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }
            return temp && flag;
        },
        startTimer: function startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        confirm: function confirm() {
            var _this2 = this;

            // this.nativeShowAppHomeGuide();
            // return;
            if (this.validateInfo()) {
                //注册犀牛会员
                (0, _loginQuery.pushHand2User)(this.phone, this.vCode).then(function (res) {
                    // toast({
                    //     "content":res.msg
                    // });
                    // alert('显示引导'+this.isNeedGuide);
                    if (_this2.isNeedGuide) {
                        //显示引导
                        //开启引导
                        _this2.nativeShowAppHomeGuide();
                    } else {
                        //关闭弹窗
                        if (_this2.isFirst) {
                            _this2.closeNativeApp();
                        }
                        //跳转
                        _this2.cb();
                    }
                });
            }
        },
        openUser: function openUser() {
            alert("会员登录...");
        }
    }
};

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(15);

var _api = __webpack_require__(24);

var _shopQuery = __webpack_require__(132);

exports.default = {
    props: {
        showPay: {
            type: Boolean,
            default: false
        },
        goodsName: {
            type: String,
            default: ''
        },
        //价格小数位
        priceDecimal: {
            type: Number,
            default: 0
        },
        //价格整数位
        priceIntegerDigits: {
            type: Number,
            default: 0
        },
        price: {
            type: String,
            default: '0.0'
        },
        goodsNo: {
            type: String,
            default: ''
        }

    },
    data: function data() {
        return {
            // goodsName:'小度智能车载支架 标准版百度旗下小度智能车载支架 标准版百度旗下',
            // price:99.00,
            // priceDecimal:'99',//价格整数位
            // priceIntegerDigits:'12', //价格小数位
        };
    },
    mounted: function mounted() {
        window.installAppCallback = this.installAppCallback;
        window.openAppCallback = this.openAppCallback;
    },

    methods: {
        closePage: function closePage() {
            this.$emit('update:showPay', false);
        },
        installAppCallback: function installAppCallback(isInstall) {

            var _os = (0, _common.isClient)();
            if (isInstall) {
                //安装app
                if (_os === 'ios') {
                    this.nativeOpenBrowser();
                } else {
                    this.nativeOpenApp('openAppCallback');
                }
            } else {
                //未安装
                // alert(USER_DOWNLOAD_URL);
                window.location.href = _api.USER_DOWNLOAD_URL;
            }
        },
        openAppCallback: function openAppCallback() {
            "安卓打开app回调...";
        },
        goPay: function goPay() {
            var _this = this;

            (0, _shopQuery.lastBuyGoods)(this.goodsNo).then(function (res) {
                //先判断是否安装
                _this.nativeIsInstalledApp("installAppCallback");
                _this.closePage();
            });
        }
    }
};

/***/ }),
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.swiper-container[data-v-0b4539d4] {\n  width: 100%;\n  height: 7.2rem;\n  position: relative;\n}\n.swiper-container .swiper-wrapper[data-v-0b4539d4] {\n  width: 100%;\n  height: 100%;\n}\n.swiper-container .swiper-slide[data-v-0b4539d4] {\n  background-position: center;\n  background-size: cover;\n  width: 100%;\n  height: 100%;\n}\n.swiper-container .swiper-slide a[data-v-0b4539d4] {\n  width: 100%;\n  height: 100%;\n  display: block;\n}\n.swiper-container .swiper-pagination-bullets span.swiper-pagination-bullet[data-v-0b4539d4] {\n  width: 8px;\n  height: 8px;\n  display: inline-block;\n  border-radius: 100%;\n  background: #000;\n  opacity: 1;\n}\n.swiper-container .swiper-pagination-bullets span.my-bullet-active[data-v-0b4539d4] {\n  width: 0.2rem;\n  height: 0.2rem;\n  display: inline-block;\n  background: #ccc;\n  opacity: 1;\n}\n", ""]);

// exports


/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.login_wrap {\n  overflow: hidden;\n  width: 7.5rem;\n  height: 8.8rem;\n  position: absolute;\n  left: 50%;\n  margin-left: -3.75rem;\n  margin-top: -4.4rem;\n  top: 50%;\n  z-index: 2;\n  background: #FFF;\n}\n.login_wrap .login_wrap_inner {\n  font-size: .42rem;\n  padding: 0 .77rem;\n}\n.login_wrap .login_wrap_inner .logo {\n  width: 1.45rem;\n  height: 1.45rem;\n  border-radius: .29rem;\n  margin: 1.3rem auto .19rem;\n  display: block;\n}\n.login_wrap .login_wrap_inner h4 {\n  text-align: center;\n  font-size: .42rem;\n  color: #313757;\n  letter-spacing: 0;\n}\n.login_wrap .login_wrap_inner .login_form {\n  margin: .6rem 0 .58rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group {\n  padding-bottom: .32rem;\n  border-bottom: 1px solid #ECF0FF;\n  font-size: .42rem;\n  position: relative;\n}\n.login_wrap .login_wrap_inner .login_form .form_group input {\n  width: 100%;\n  color: #313757;\n  margin-top: .33rem;\n  font-size: .36rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group span {\n  color: #3F83FF;\n  letter-spacing: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  top: 0;\n  right: 0;\n  font-size: .36rem;\n  height: 100%;\n  line-height: 100%;\n}\n.login_wrap .login_wrap_inner .login_btn {\n  background: #3F83FF;\n  border-radius: 3px;\n  display: block;\n  width: 6rem;\n  height: .96rem;\n  line-height: .96rem;\n  font-size: .36rem;\n  color: #FFFFFF;\n  letter-spacing: 0;\n  text-align: center;\n}\n.login_wrap .login_wrap_inner .login_agreement {\n  font-size: .28rem;\n  margin: .38rem 0 .5rem;\n}\n.login_wrap .login_wrap_inner .login_agreement span {\n  display: block;\n  color: #313757;\n}\n.login_wrap .login_wrap_inner .login_agreement a {\n  color: #3F83FF;\n  position: relative;\n  left: -0.14rem;\n}\n", ""]);

// exports


/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*顶部报错提示*/\n.tips[data-v-136bdbe6] {\n  width: 100%;\n  height: 0.933333rem;\n  position: fixed;\n  top: -0.933333rem;\n  left: 0;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #000;\n  line-height: 0.933333rem;\n  z-index: 9999;\n  white-space: nowrap;\n}\n.tips_error[data-v-136bdbe6] {\n  background: #fff;\n}\n.tips_success[data-v-136bdbe6] {\n  background: #04be02;\n}\n/*顶部报错提示*/\n#contentPage[data-v-136bdbe6] {\n  width: 100%;\n  -webkit-overflow-scrolling: touch;\n  overflow-scrolling: touch;\n  overflow: hidden;\n  height: 100%;\n  position: relative;\n}\n.pull-to-refresh-layer[data-v-136bdbe6] {\n  height: 1.6rem;\n}\n.fade-enter-active[data-v-136bdbe6],\n.fade-leave-active[data-v-136bdbe6] {\n  -webkit-transition: opacity 0.5s;\n  transition: opacity 0.5s;\n}\n.fade-enter[data-v-136bdbe6],\n.fade-leave-to[data-v-136bdbe6] {\n  opacity: 0;\n}\n.default_button[data-v-136bdbe6] {\n  width: 100%;\n  height: 0.94rem;\n  background: #3F83FF;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n     -moz-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 0.36rem;\n  border-radius: 0.08rem;\n  color: #fff;\n  margin: 0.1rem 0;\n}\n.bold[data-v-136bdbe6] {\n  font-size: \"PingFangSC-Semibold\";\n}\n.scroller[data-v-136bdbe6] {\n  position: relative;\n}\n.loading-tips[data-v-136bdbe6] {\n  padding: 0 .15rem;\n  height: .5rem;\n  text-align: center;\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  left: 50%;\n  bottom: .5rem;\n  -webkit-transform: translateX(-50%);\n      -ms-transform: translateX(-50%);\n          transform: translateX(-50%);\n  font-size: .24rem;\n  color: #fff;\n  border-radius: .05rem;\n  line-height: .5rem;\n}\n.sharePic[data-v-136bdbe6] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n}\n.sharePic .main[data-v-136bdbe6] {\n  width: 100%;\n  height: 9.16rem;\n  background: #ffffff;\n}\n/*弹窗*/\n.dialog[data-v-136bdbe6] {\n  position: fixed;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  background-color: rgba(0, 0, 0, 0.65) !important;\n  filter: alpha(opacity=66);\n}\n.dialog img[data-v-136bdbe6] {\n  width: 5.6rem;\n  height: 2.17rem;\n}\n.bottomBar[data-v-136bdbe6] {\n  z-index: 10000;\n  width: 6.3rem;\n  border-top-left-radius: 0.08rem;\n  border-top-right-radius: 0.08rem;\n  position: fixed;\n  left: 50%;\n  top: 52%;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n}\n.bottomBar .mui-content[data-v-136bdbe6] {\n  width: 100%;\n  height: 9.16rem;\n  margin: 1.18rem auto 0.32rem;\n}\n.bottomBar .bottomBar_content[data-v-136bdbe6] {\n  width: 92%;\n  margin: .32rem auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.bottomBar .bottomBar_content p[data-v-136bdbe6] {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  text-align: center;\n}\n.bottomBar .bottomBar_content p img[data-v-136bdbe6] {\n  width: 1.0rem;\n  height: 1.0rem;\n  display: block;\n  margin: 0 auto 0.2rem;\n}\n.bottomBar .bottomBar_content p span[data-v-136bdbe6] {\n  display: block;\n  text-align: center;\n  font-size: 0.28rem;\n  color: #ffffff;\n}\n", ""]);

// exports


/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.login_wrap[data-v-1898a39e] {\n  width: 6.2rem;\n  height: 7.32rem;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  z-index: 11;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n  background: #FFF;\n  border-radius: .08rem;\n}\n.login_wrap .login_wrap_inner[data-v-1898a39e] {\n  font-size: .34rem;\n  padding: 0 .64rem;\n}\n.login_wrap .login_wrap_inner .logo[data-v-1898a39e] {\n  width: 1.2rem;\n  height: 1.2rem;\n  border-radius: .24rem;\n  margin: .48rem auto .3rem;\n  display: block;\n}\n.login_wrap .login_wrap_inner h4[data-v-1898a39e] {\n  text-align: center;\n  font-size: .36rem;\n  color: #313757;\n  letter-spacing: 0;\n}\n.login_wrap .login_wrap_inner .login_form[data-v-1898a39e] {\n  margin: .4rem 0 .48rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group[data-v-1898a39e] {\n  padding-bottom: .26rem;\n  border-bottom: 1px solid #ECF0FF;\n  font-size: .34rem;\n  position: relative;\n}\n.login_wrap .login_wrap_inner .login_form .form_group input[data-v-1898a39e] {\n  width: 100%;\n  color: #313757;\n  margin-top: .28rem;\n  font-size: .34rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group span[data-v-1898a39e] {\n  color: #3D4A5B;\n  letter-spacing: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  top: 0;\n  right: 0;\n  font-size: .32rem;\n  height: 100%;\n  line-height: 100%;\n}\n.login_wrap .login_wrap_inner .login_btn[data-v-1898a39e] {\n  background: #EDC78D;\n  border-radius: 3px;\n  display: block;\n  width: 4.92rem;\n  height: .8rem;\n  line-height: .8rem;\n  font-size: .32rem;\n  color: #2C2628;\n  letter-spacing: 0;\n  text-align: center;\n}\n.login_wrap .login_wrap_inner .login_agreement[data-v-1898a39e] {\n  font-size: .24rem;\n  margin: .32rem 0 .34rem;\n  text-align: center;\n  color: #3D4A5B;\n}\n.login_wrap .login_wrap_inner .login_agreement span[data-v-1898a39e] {\n  display: block;\n}\n.login_wrap .login_wrap_inner .login_agreement a[data-v-1898a39e] {\n  color: #3F83FF;\n  position: relative;\n  left: -0.06rem;\n}\n.mask[data-v-1898a39e] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.6);\n  z-index: 10;\n}\n[data-v-1898a39e]::-webkit-input-placeholder {\n  /* WebKit browsers */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-1898a39e]::-moz-placeholder {\n  /* Mozilla Firefox 19+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-1898a39e]:-ms-input-placeholder {\n  /* Internet Explorer 10+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n", ""]);

// exports


/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.login_container[data-v-27595902] {\n  width: 6.5rem;\n  height: 8.5rem;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n  z-index: 100;\n}\n.logo[data-v-27595902] {\n  width: 1.56rem;\n  height: 1.56rem;\n  border-radius: 50%;\n  margin: .48rem auto .3rem;\n  display: block;\n  position: fixed;\n  left: 50%;\n  top: -0.5rem;\n  z-index: 13;\n  -webkit-transform: translate3d(-50%, 0%, 0);\n          transform: translate3d(-50%, 0%, 0);\n}\n.login_wrap[data-v-27595902] {\n  width: 6.2rem;\n  height: 7.22rem;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  z-index: 11;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n  background: #FFF;\n  border-radius: .08rem;\n}\n.login_wrap .close[data-v-27595902] {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n.login_wrap .close i[data-v-27595902] {\n  margin: .2rem;\n  display: block;\n  width: .28rem;\n  height: .28rem;\n  background: url(" + __webpack_require__(180) + ") no-repeat;\n  background-size: cover;\n}\n.login_wrap .login_wrap_inner[data-v-27595902] {\n  font-size: .34rem;\n  padding: 0 .64rem;\n  position: relative;\n}\n.login_wrap .login_wrap_inner h4[data-v-27595902] {\n  text-align: center;\n  font-size: .36rem;\n  color: #313757;\n  letter-spacing: 0;\n  width: 2.88rem;\n  margin: 1.2rem auto 0;\n  line-height: .7rem;\n}\n.login_wrap .login_wrap_inner .login_form[data-v-27595902] {\n  margin: .32rem 0 .48rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group[data-v-27595902] {\n  padding-bottom: .26rem;\n  border-bottom: 1px solid #ECF0FF;\n  font-size: .34rem;\n  position: relative;\n}\n.login_wrap .login_wrap_inner .login_form .form_group input[data-v-27595902] {\n  width: 100%;\n  color: #313757;\n  margin-top: .28rem;\n  font-size: .34rem;\n}\n.login_wrap .login_wrap_inner .login_form .form_group span[data-v-27595902] {\n  color: #3F83FF;\n  letter-spacing: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  top: 0;\n  right: 0;\n  font-size: .32rem;\n  height: 100%;\n  line-height: 100%;\n}\n.login_wrap .login_wrap_inner .login_btn[data-v-27595902] {\n  background: #3F83FF;\n  border-radius: 3px;\n  display: block;\n  width: 4.92rem;\n  height: .8rem;\n  line-height: .8rem;\n  font-size: .32rem;\n  color: #FFF;\n  letter-spacing: 0;\n  text-align: center;\n}\n.login_wrap .login_wrap_inner .login_agreement[data-v-27595902] {\n  font-size: .24rem;\n  margin: .32rem 0 .34rem;\n  text-align: center;\n  color: #3D4A5B;\n}\n.login_wrap .login_wrap_inner .login_agreement span[data-v-27595902] {\n  display: block;\n}\n.login_wrap .login_wrap_inner .login_agreement a[data-v-27595902] {\n  color: #3F83FF;\n  position: relative;\n  left: -0.06rem;\n}\n.mask[data-v-27595902] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.6);\n  z-index: 10;\n}\n[data-v-27595902]::-webkit-input-placeholder {\n  /* WebKit browsers */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-27595902]::-moz-placeholder {\n  /* Mozilla Firefox 19+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-27595902]:-ms-input-placeholder {\n  /* Internet Explorer 10+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n", ""]);

// exports


/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.login_wrap[data-v-32c7ffa0] {\n  width: 6.2rem;\n  height: auto;\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  z-index: 11;\n  -webkit-transform: translate3d(-50%, -50%, 0);\n          transform: translate3d(-50%, -50%, 0);\n  background: #FFF;\n  border-radius: .08rem;\n}\n.login_wrap .close[data-v-32c7ffa0] {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n.login_wrap .close i[data-v-32c7ffa0] {\n  margin: .2rem;\n  display: block;\n  width: .32rem;\n  height: .32rem;\n  background: url(" + __webpack_require__(186) + ") no-repeat;\n  background-size: cover;\n}\n.login_wrap .login_wrap_inner[data-v-32c7ffa0] {\n  font-size: .34rem;\n  padding: 0 .64rem;\n  position: relative;\n}\n.login_wrap .login_wrap_inner .logo[data-v-32c7ffa0] {\n  width: 1.2rem;\n  height: 1.2rem;\n  border-radius: .24rem;\n  margin: .48rem auto .3rem;\n  display: block;\n}\n.login_wrap .login_wrap_inner h4[data-v-32c7ffa0] {\n  text-align: center;\n  font-size: .36rem;\n  color: #313757;\n  letter-spacing: 0;\n}\n.login_wrap .login_wrap_inner .shop_info[data-v-32c7ffa0] {\n  margin-bottom: .72rem;\n}\n.login_wrap .login_wrap_inner .shop_info .shop_title[data-v-32c7ffa0] {\n  opacity: 0.6;\n  font-size: .3rem;\n  color: #3D4A5B;\n  margin: .32rem 0 .12rem 0;\n  letter-spacing: 0;\n}\n.login_wrap .login_wrap_inner .shop_info .pay_info[data-v-32c7ffa0] {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.login_wrap .login_wrap_inner .shop_info .shop_price_dia[data-v-32c7ffa0] {\n  color: #FF3A7A;\n  font-weight: normal;\n}\n.login_wrap .login_wrap_inner .shop_info .shop_price_dia[data-v-32c7ffa0]::before {\n  content: '';\n  width: .36rem;\n  height: .3rem;\n  background: url(" + __webpack_require__(187) + ") no-repeat;\n  background-size: cover;\n  display: inline-block;\n  margin-right: .08rem;\n}\n.login_wrap .login_wrap_inner .shop_info .shop_price_dia em[data-v-32c7ffa0] {\n  font-size: .48rem;\n  line-height: .48rem;\n}\n.login_wrap .login_wrap_inner .shop_info .price_del[data-v-32c7ffa0] {\n  position: relative;\n  left: .1rem;\n  top: .02rem;\n  font-weight: normal;\n  margin-left: .1rem;\n  opacity: 0.6;\n  font-size: .24rem;\n  color: #3D4A5B;\n}\n.login_wrap .login_wrap_inner .shop_info .price_del[data-v-32c7ffa0]::after {\n  content: '';\n  position: absolute;\n  left: 0;\n  bottom: 50%;\n  background: #3D4A5B;\n  width: 100%;\n  height: 1px;\n  -webkit-transform: scaleY(0.5);\n  -ms-transform: scaleY(0.5);\n  transform: scaleY(0.5);\n}\n.login_wrap .login_wrap_inner .login_btn[data-v-32c7ffa0] {\n  background: #3F83FF;\n  border-radius: 3px;\n  display: block;\n  width: 4.92rem;\n  height: .8rem;\n  line-height: .8rem;\n  font-size: .32rem;\n  color: #FFF;\n  letter-spacing: 0;\n  text-align: center;\n  margin-bottom: .7rem;\n}\n.login_wrap .login_wrap_inner .login_agreement[data-v-32c7ffa0] {\n  font-size: .24rem;\n  margin: .32rem 0 .34rem;\n  text-align: center;\n  color: #3D4A5B;\n}\n.login_wrap .login_wrap_inner .login_agreement span[data-v-32c7ffa0] {\n  display: block;\n}\n.login_wrap .login_wrap_inner .login_agreement a[data-v-32c7ffa0] {\n  color: #3F83FF;\n  position: relative;\n  left: -0.06rem;\n}\n.mask[data-v-32c7ffa0] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.6);\n  z-index: 10;\n}\n[data-v-32c7ffa0]::-webkit-input-placeholder {\n  /* WebKit browsers */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-32c7ffa0]::-moz-placeholder {\n  /* Mozilla Firefox 19+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n[data-v-32c7ffa0]:-ms-input-placeholder {\n  /* Internet Explorer 10+ */\n  color: #3D4A5B;\n  font-size: .34rem;\n}\n", ""]);

// exports


/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "/*loading加载*/\n.loadPage[data-v-43fba061] {\n  width: 100%;\n  height: 100%;\n  position: fixed;\n  z-index: 99999;\n  left: 0;\n  top: 0;\n  background: rgba(0, 0, 0, 0);\n}\n.loadPage .loading[data-v-43fba061] {\n  width: 2rem;\n  padding: 0.32rem 0 0.24rem;\n  background: rgba(0, 0, 0, 0.5);\n  position: relative;\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n  border-radius: 0.15rem;\n  text-align: center;\n}\n.loadPage .loading img[data-v-43fba061] {\n  width: 0.7rem;\n  height: 0.7rem;\n  display: block;\n  margin: 0 auto;\n  -webkit-animation: loading 0.8s linear infinite;\n  animation: loading 0.8s linear infinite;\n}\n.loadPage .loading p[data-v-43fba061] {\n  color: #fff;\n  opacity: 0.9;\n  font-size: 0.28rem;\n  margin-top: 0.14rem;\n}\n@-webkit-keyframes loading {\nfrom {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n}\nto {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n}\n}\n@keyframes loading {\nfrom {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n}\nto {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n}\n}\n/*loading加载*/\n", ""]);

// exports


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(22)();
// imports


// module
exports.push([module.i, "\n.alertBox[data-v-64de57f2] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  z-index: 200;\n}\n.alertBox .content[data-v-64de57f2] {\n  width: 5.4rem;\n  overflow: hidden;\n  background: #fff;\n  position: absolute;\n  -webkit-transform: translate(-50%, -50%);\n      -ms-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  left: 50%;\n  top: 50%;\n  padding: 0.6rem;\n  border-radius: 0.06rem;\n}\n.alertBox .content .title[data-v-64de57f2] {\n  font-size: 0.36rem;\n  color: #3D4A5B;\n  text-align: center;\n  font-family: \"PingFangSC-Regular\";\n}\n.alertBox .content .body[data-v-64de57f2] {\n  font-size: 0.32rem;\n  color: #3D4A5B;\n  margin-top: 0.4rem;\n  text-align: right;\n}\n.alertBox .content .body p[data-v-64de57f2] {\n  width: 100%;\n  height: 1rem;\n  background: #F5F8FF;\n  border-radius: 0.06rem;\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n     -moz-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n}\n.alertBox .content .body p[data-v-64de57f2]:after {\n  content: \" \";\n  border: 1px solid #ECF0F8;\n  -webkit-transform: scale(0.5, 0.5);\n      -ms-transform: scale(0.5, 0.5);\n          transform: scale(0.5, 0.5);\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  left: -50%;\n  top: -50%;\n  width: 200%;\n  height: 200%;\n  display: block;\n  position: absolute;\n  z-index: -1;\n}\n.alertBox .content .body p input[data-v-64de57f2] {\n  margin-left: 0.3rem;\n  font-size: 0.28rem;\n  color: #3D4A5B;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n     -moz-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n}\n.alertBox .content .body p input[data-v-64de57f2]::-webkit-input-placeholder {\n  color: #3D4A5B;\n  opacity: 0.15;\n  font-size: 0.28rem;\n}\n.alertBox .content .body p img[data-v-64de57f2] {\n  width: 1.58rem;\n  height: 0.8rem;\n  display: block;\n  margin-right: 0.1rem;\n}\n.alertBox .content .body span[data-v-64de57f2] {\n  color: #3D4A5B;\n  font-size: 0.24rem;\n  opacity: 0.5;\n  -webkit-transform: scale(0.8, 0.8);\n      -ms-transform: scale(0.8, 0.8);\n          transform: scale(0.8, 0.8);\n}\n.alertBox .content .footer[data-v-64de57f2] {\n  width: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -moz-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n     -moz-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 0.1rem;\n}\n.alertBox .content .footer a[data-v-64de57f2] {\n  display: block;\n  width: 2.24rem;\n  height: 0.8rem;\n  color: #000;\n  font-size: 0.32rem;\n  position: relative;\n  text-align: center;\n  line-height: 0.8rem;\n}\n.alertBox .content .footer a[data-v-64de57f2]:first-child:after {\n  content: \" \";\n  width: 200%;\n  height: 200%;\n  position: absolute;\n  left: -50%;\n  top: -50%;\n  border: 1px solid #3F83FF;\n  -webkit-transform: scale(0.5, 0.5);\n      -ms-transform: scale(0.5, 0.5);\n          transform: scale(0.5, 0.5);\n  -moz-box-sizing: border-box;\n       box-sizing: border-box;\n  border-radius: 0.06rem;\n}\n.alertBox .content .footer a[data-v-64de57f2]:last-child {\n  /*background-image: linear-gradient(-214deg, #9373FF 0%, #3F83FF 100%);*/\n  color: #FFF;\n  background: #3F83FF;\n  border-radius: 0.06rem;\n}\n", ""]);

// exports


/***/ }),
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/guanbi.png?v=f4010dcf";

/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_circle_of_riends.png?v=52f5eba2";

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_save.png?v=609461df";

/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon_wechat.png?v=70e9026c";

/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon.png?v=59d1a3d0";

/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon.png?v=59d1a3d0";

/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/guanbi.png?v=f4010dcf";

/***/ }),
/* 187 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/huangguan.png?v=a4580656";

/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "image/icon.png?v=59d1a3d0";

/***/ }),
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(299)

var Component = __webpack_require__(20)(
  /* script */
  __webpack_require__(134),
  /* template */
  __webpack_require__(291),
  /* scopeId */
  "data-v-43fba061",
  /* cssModules */
  null
)
Component.options.__file = "E:\\jinkong\\userHome-app\\static\\components\\loading.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loading.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-43fba061", Component.options)
  } else {
    hotAPI.reload("data-v-43fba061", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 284 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "swiper-container"
  }, [_c('div', {
    staticClass: "swiper-wrapper"
  }, _vm._l((_vm.swiper_config.listImg), function(i) {
    return _c('div', {
      staticClass: "swiper-slide",
      style: ({
        backgroundImage: 'url(' + i.url + ')'
      })
    }, [_c('a', {
      attrs: {
        "href": i.link ? i.link : 'javascript:;'
      }
    })])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "swiper-pagination swiper-pagination-white"
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0b4539d4", module.exports)
  }
}

/***/ }),
/* 285 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "login_wrap"
  }, [_c('div', {
    staticClass: "login_wrap_inner",
    staticStyle: {
      "height": "6.7rem"
    }
  }, [_c('img', {
    staticClass: "logo",
    attrs: {
      "src": __webpack_require__(185),
      "alt": ""
    }
  }), _vm._v(" "), _c('h4', [_vm._v("领取会员权益")]), _vm._v(" "), _c('div', {
    staticClass: "login_form"
  }, [_c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }, {
      name: "enterNumber",
      rawName: "v-enterNumber"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "请输入手机号码"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.vCode),
      expression: "vCode"
    }],
    ref: "vCode",
    attrs: {
      "type": "tel",
      "placeholder": "请输入验证码",
      "maxlength": "6"
    },
    domProps: {
      "value": (_vm.vCode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.vCode = $event.target.value
      }
    }
  }), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.sendCode
    }
  }, [_vm._v(_vm._s(_vm.mes))])])]), _vm._v(" "), _c('span', {
    staticClass: "login_btn",
    on: {
      "click": _vm.confirm
    }
  }, [_vm._v("开启高返佣赚钱")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-135e949d", module.exports)
  }
}

/***/ }),
/* 286 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "sharePic"
  }, [_c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showDia),
      expression: "showDia"
    }],
    staticClass: "dialog",
    attrs: {
      "id": "dialog"
    },
    on: {
      "click": function($event) {
        return _vm.closeShare();
      }
    }
  })]), _vm._v(" "), _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.show1),
      expression: "show1"
    }],
    ref: "bottomBar",
    staticClass: "bottomBar"
  }, [_c('div', {
    ref: "imageTofile",
    staticClass: "main"
  }, [_vm._t("default")], 2), _vm._v(" "), _c('div', {
    staticClass: "mui-content",
    staticStyle: {
      "display": "none"
    }
  }, [_c('img', {
    staticStyle: {
      "width": "100%"
    },
    attrs: {
      "src": _vm.htmlUrl,
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "bottomBar_content"
  }, [_c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('wechatSession')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(183)
    }
  }), _vm._v(" "), _c('span', [_vm._v("微信好友")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('wechatTimeline')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(181)
    }
  }), _vm._v(" "), _c('span', [_vm._v("朋友圈")])]), _vm._v(" "), _c('p', {
    on: {
      "click": function($event) {
        return _vm.toImage('nativeSaveImage')
      }
    }
  }, [_c('img', {
    attrs: {
      "src": __webpack_require__(182)
    }
  }), _vm._v(" "), _c('span', [_vm._v("保存图片")])])])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-136bdbe6", module.exports)
  }
}

/***/ }),
/* 287 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isShow),
      expression: "isShow"
    }]
  }, [_c('div', {
    staticClass: "login_wrap"
  }, [_c('div', {
    staticClass: "login_wrap_inner"
  }, [_c('img', {
    staticClass: "logo",
    attrs: {
      "src": __webpack_require__(184),
      "alt": ""
    }
  }), _vm._v(" "), _c('h4', [_vm._v("开通犀牛会员账号")]), _vm._v(" "), _c('div', {
    staticClass: "login_form"
  }, [_c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "请输入手机号码"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.vCode),
      expression: "vCode"
    }],
    ref: "vCode",
    attrs: {
      "type": "tel",
      "placeholder": "请输入验证码",
      "maxlength": "6"
    },
    domProps: {
      "value": (_vm.vCode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.vCode = $event.target.value
      }
    }
  }), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.sendCode
    }
  }, [_vm._v(_vm._s(_vm.mes))])])]), _vm._v(" "), _c('span', {
    staticClass: "login_btn",
    on: {
      "click": _vm.openUser
    }
  }, [_vm._v("立即开通")]), _vm._v(" "), _vm._m(0)])]), _vm._v(" "), _c('div', {
    staticClass: "mask"
  })])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', {
    staticClass: "login_agreement"
  }, [_vm._v("\n                注册代表您已同意 "), _c('a', {
    attrs: {
      "href": "#",
      "target": "_blank"
    }
  }, [_vm._v("《犀牛会员协议》")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1898a39e", module.exports)
  }
}

/***/ }),
/* 288 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isPushHandShow),
      expression: "isPushHandShow"
    }]
  }, [_c('div', {
    staticClass: "login_container"
  }, [_c('img', {
    staticClass: "logo",
    attrs: {
      "src": _vm.headImagePath,
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "login_wrap"
  }, [_c('div', {
    staticClass: "login_wrap_inner"
  }, [_c('h4', [_vm._v(_vm._s(_vm.parentUserName) + "邀请您成为逍遥推手用户")]), _vm._v(" "), _c('div', {
    staticClass: "login_form"
  }, [_c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.phone),
      expression: "phone"
    }, {
      name: "enterNumber",
      rawName: "v-enterNumber"
    }],
    attrs: {
      "type": "tel",
      "maxlength": "11",
      "placeholder": "请输入手机号码"
    },
    domProps: {
      "value": (_vm.phone)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.phone = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('section', {
    staticClass: "form_group"
  }, [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.vCode),
      expression: "vCode"
    }],
    ref: "vCode",
    attrs: {
      "type": "tel",
      "placeholder": "请输入验证码",
      "maxlength": "6"
    },
    domProps: {
      "value": (_vm.vCode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.vCode = $event.target.value
      }
    }
  }), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.sendCode
    }
  }, [_vm._v(_vm._s(_vm.mes))])])]), _vm._v(" "), _c('span', {
    staticClass: "login_btn",
    on: {
      "click": _vm.openUser
    }
  }, [_vm._v("立即开通")]), _vm._v(" "), _vm._m(0)]), _vm._v(" "), _c('div', {
    staticClass: "close",
    on: {
      "click": function($event) {
        return _vm.closePage()
      }
    }
  }, [_c('i')])])]), _vm._v(" "), _c('div', {
    staticClass: "mask"
  })])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('p', {
    staticClass: "login_agreement"
  }, [_vm._v("\n                        注册代表您已同意 "), _c('a', {
    attrs: {
      "href": "http://www.baidu.com",
      "target": "_blank"
    }
  }, [_vm._v("《逍遥推手协议》")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-27595902", module.exports)
  }
}

/***/ }),
/* 289 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showPay),
      expression: "showPay"
    }]
  }, [_c('div', {
    staticClass: "login_wrap"
  }, [_c('div', {
    staticClass: "login_wrap_inner"
  }, [_c('img', {
    staticClass: "logo",
    attrs: {
      "src": __webpack_require__(188),
      "alt": ""
    }
  }), _vm._v(" "), _c('h4', [_vm._v("前往犀牛会员购买")]), _vm._v(" "), _c('div', {
    staticClass: "shop_info"
  }, [_c('h5', {
    staticClass: "shop_title"
  }, [_vm._v(_vm._s(_vm._f("textOverflow")(_vm.goodsName, 16)))]), _vm._v(" "), _c('section', {
    staticClass: "pay_info"
  }, [_c('section', {
    staticClass: "shop_price_dia"
  }, [_c('span', [_vm._v("￥")]), _c('em', [_vm._v(_vm._s(_vm.priceIntegerDigits))]), _vm._v("." + _vm._s(_vm.priceDecimal) + "\n                     ")]), _vm._v(" "), _c('section', {
    staticClass: "price_del"
  }, [_c('span', [_vm._v("¥")]), _c('em', [_vm._v(_vm._s(_vm.price))])])])]), _vm._v(" "), _c('span', {
    staticClass: "login_btn",
    on: {
      "click": _vm.goPay
    }
  }, [_vm._v("立即前往")])]), _vm._v(" "), _c('div', {
    staticClass: "close",
    on: {
      "click": function($event) {
        return _vm.closePage()
      }
    }
  }, [_c('i')])]), _vm._v(" "), _c('div', {
    staticClass: "mask"
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-32c7ffa0", module.exports)
  }
}

/***/ }),
/* 290 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "page"
    }
  }, [_c('Loading', {
    attrs: {
      "isLoading": _vm.isLoading
    }
  }), _vm._v(" "), _c('keep-alive', [(_vm.$route.meta.keepAlive) ? _c('router-view') : _vm._e()], 1), _vm._v(" "), (!_vm.$route.meta.keepAlive) ? _c('router-view') : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-4325e4d3", module.exports)
  }
}

/***/ }),
/* 291 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.isLoading),
      expression: "isLoading"
    }],
    staticClass: "loadPage"
  }, [_vm._m(0)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loading"
  }, [_c('img', {
    attrs: {
      "src": "image/icon_loading.png"
    }
  }), _vm._v(" "), _c('p', [_vm._v("数据加载中")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-43fba061", module.exports)
  }
}

/***/ }),
/* 292 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('transition', {
    attrs: {
      "name": "fade"
    }
  }, [(_vm.isShowImgCode) ? _c('div', {
    ref: "alertBox",
    staticClass: "alertBox"
  }, [_c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("请输入图片验证码")]), _vm._v(" "), _c('div', {
    staticClass: "body"
  }, [_c('p', [_c('input', {
    directives: [{
      name: "iosInputBug",
      rawName: "v-iosInputBug"
    }, {
      name: "model",
      rawName: "v-model",
      value: (_vm.vCode),
      expression: "vCode"
    }],
    attrs: {
      "placeholder": "请输入图片验证码",
      "type": "tel",
      "maxlength": "4"
    },
    domProps: {
      "value": (_vm.vCode)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.vCode = $event.target.value
      }
    }
  }), _vm._v(" "), _c('img', {
    staticStyle: {
      "border-width": "0"
    },
    attrs: {
      "src": _vm.imgUrl
    },
    on: {
      "touchstart": function($event) {
        $event.stopPropagation();
        $event.preventDefault();
        return _vm.refresh()
      }
    }
  })]), _vm._v(" "), _c('span', [_vm._v("点击图片可刷新")])]), _vm._v(" "), _c('div', {
    staticClass: "footer"
  }, [_c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "touchstart": function($event) {
        $event.stopPropagation();
        $event.preventDefault();
        return _vm.cancelBox.apply(null, arguments)
      }
    }
  }, [_vm._v(_vm._s(_vm.cancelText))]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "javascript:;"
    },
    on: {
      "touchstart": function($event) {
        $event.stopPropagation();
        $event.preventDefault();
        return _vm.confirmSubmit.apply(null, arguments)
      }
    }
  }, [_vm._v(_vm._s(_vm.confirmText))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-64de57f2", module.exports)
  }
}

/***/ }),
/* 293 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(167);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("f5872864", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0b4539d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./index.less", function() {
     var newContent = require("!!../../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-0b4539d4&scoped=true!../../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./index.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 294 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(168);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("8577eeee", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-135e949d!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-135e949d!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 295 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(169);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("01e90a39", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-136bdbe6&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share.less", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-136bdbe6&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./share.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 296 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(170);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("2d950e0a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1898a39e&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1898a39e&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 297 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(171);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("0fe5ab15", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-27595902&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-27595902&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./login.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 298 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(172);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("4116b838", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32c7ffa0&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./userPayDialog.less", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32c7ffa0&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!./userPayDialog.less");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 299 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(173);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("33bf1222", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-43fba061&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./loading.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-43fba061&scoped=true!../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./loading.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 300 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(174);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(23)("a1071806", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-64de57f2&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./verifiCode.vue", function() {
     var newContent = require("!!../../../node_modules/_css-loader@0.25.0@css-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-64de57f2&scoped=true!../../../node_modules/_postcss-loader@1.3.3@postcss-loader/index.js!../../../node_modules/_less-loader@2.2.3@less-loader/index.js!../../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./verifiCode.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })
],[131]);