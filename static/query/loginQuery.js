/** 2019-09-05
 *作者:heliang
 *功能:
 */
import api, { KY_IP, KY_USER_IP } from '@/api.config';
import * as common from "@/libs/common";

/**推手首页登录会员**/
//推手登录会员
function pushHand2User(phone, verCode) {
    // let uuid = getCookie("UUID");
    let url = `${KY_IP}rhinoMembership/register`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: { phoneNo: phone, smsCode: verCode },
            showLoading: false
        }, (res) => {
            console.log("推手注册会员 pushHand2User ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手注册会员 pushHand2User *************", res);
            reject(res)
        });
    });
}

//发送验证码推手
function pushHandSms(phoneNo) {
    let url = `${KY_IP}rhinoMembership/sendVerCode`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: { phoneNo },
            showLoading: false
        }, (res) => {
            console.log("发送验证码推手 pushHandSms ######################", res);
            resolve(res)
        }, (res) => {
            console.log("发送验证码推手 pushHandSms *************", res);
            reject(res)
        });
    });
}


//获取首页弹窗类型 
function getDialogType() {
    let url = `${KY_IP}rhinoMembership/popType`;
    // let data = {
    //     "type": 0,
    //     "isNeedGuide": true
    // };
    // return new Promise((resolve, reject) => {
    //     resolve({
    //         data
    //     });
    // });
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, (res) => {
            console.log("获取首页弹窗类型 getDialogType ######################", res);
            resolve(res)
        }, (res) => {
            console.log("获取首页弹窗类型 getDialogType *************", res);
            reject(res)
        });
    });
}

//首页弹窗展示图片
function getDialogTypeNew() {
    let url = `${KY_IP}appHome/popUpsDetail`
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, (res) => {
            console.log("获取首页弹窗类型 getDialogType ######################", res);
            resolve(res)
        }, (res) => {
            console.log("获取首页弹窗类型 getDialogType *************", res);
            reject(res)
        });
    });
}

//购买商品是否需要提示去注册犀牛会员
function buyShopIsLogin() {
    let url = `${KY_IP}rhinoMembership/needToRegister`;
    // let data = {
    //     "partnerUserNo": "352707103",
    //     "flag": false
    // };
    // return new Promise((resolve, reject) => {
    //     resolve({
    //         data
    //     });
    // });
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: {},
            showLoading: false
        }, (res) => {
            console.log("购买商品是否需要提示去注册犀牛会员 buyShopIsLogin ######################", res);
            resolve(res)
        }, (res) => {
            console.log("购买商品是否需要提示去注册犀牛会员 buyShopIsLogin *************", res);
            reject(res)
        });
    });
}

/**我要当推手注册**/
//推手注册
function pushHandReg(phoneNo, checkCode, parentUserNo) {
    // /user/registerMobilePhone
    let url = `${KY_IP}user/registerMobilePhone`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: { phoneNo, checkCode, parentUserNo },
            showLoading: false
        }, (res) => {
            console.log("推手注册 pushHandReg ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手注册 pushHandReg *************", res);
            reject(res)
        });
    });
}

//推手注册发送验证码
function pushHandRegSms(phoneNo) {
    let url = `${KY_IP}user/sendCheckCode`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            data: { phoneNo, businessType: "REGISTER" },
            showLoading: false
        }, (res) => {
            console.log("推手注册发送验证码 pushHandRegSms ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手注册发送验证码 pushHandRegSms *************", res);
            reject(res)
        });
    });
}

/**微信中会员登录**/
//会员登录绑定
// function userLogin(pushUserNo,phoneNo,smsCode) {
//     let url = `${KY_USER_IP}/resource/vip-user/user/pushLogin`;
//     return new Promise((resolve, reject) => {
//
//
//         common.Ajax({
//             url: url,
//             data: {phoneNo,businessType:"REGISTER"},
//             showLoading: false
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//             resolve(res)
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms *************", res);
//             reject(res)
//         });
//
//         // common.XNAjax({
//         //     url: url,
//         //     sesstionType:"true",
//         //     data: {phoneNo,pushUserNo,smsCode,equityNo:'',source:'XYTS',caller:'H5',isOpenEquity:'N'},
//         //     showLoading: false
//         // }, (res) => {
//         //     console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//         //     resolve(res)
//         // }, (res) => {
//         //     console.log("推手注册发送验证码 pushHandRegSms *************", res);
//         //     reject(res)
//         // });
//     });
// }
//
// // 会员发送验证码
// function userSms(phoneNo) {
//     let url = `${KY_USER_IP}/resource/vip-user/userCode/getSmsCode`;
//     return new Promise((resolve, reject) => {
//         common.XNAjax({
//             url: url,
//             data: {phoneNo,caller:'H5'},
//             showLoading: false,
//             sesstionType:"true",
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms ######################", res);
//             resolve(res)
//         }, (res) => {
//             console.log("推手注册发送验证码 pushHandRegSms *************", res);
//             reject(res)
//         });
//     });
// }



export {
    pushHand2User,
    pushHandSms,
    pushHandReg,
    pushHandRegSms,
    // userLogin,
    // userSms,
    getDialogType,
    buyShopIsLogin,
    getDialogTypeNew
}