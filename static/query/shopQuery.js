/** 2019-09-09
 *作者:heliang
 *功能: 商品信息查询
 */
import {KY_IP} from "@/api.config";
import * as common from "@/libs/common";

//查询商品列表信息
function shopList() {
    let url = `${KY_IP}explosiveProducts/list`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type:'get',
            data: {},
            showLoading: false
        }, (res) => {
            console.log("推手商品列表 shopList ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手商品列表 shopList *************", res);
            reject(res)
        });
    });
}

//获取商品详情
function getShopDetail(productId,type) {
    let url=`${KY_IP}rhinoMembership/productDetails`;
    if(type==null){
        type=''
    }
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type: 'post',
            data: {
                productSPU:productId,
                type
            },
            showLoading: false
        }, (res) => {
            console.log("推手商品详情 getShopDetail ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手商品详情 getShopDetail *************", res);
            reject(res)
        });
    });
}

//推手相关信息
function getPushHandInfo(userNo) {
    let url=`${KY_IP}rhinoMembership/getUserInfo`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type: 'post',
            data: {
                userNo
            },
            showLoading: false
        }, (res) => {
            console.log("推手相关信息 getPushHandInfo ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手相关信息 getPushHandInfo *************", res);
            reject(res)
        });
    });
}

//获取推荐人名称和Id
function getPushIdAndName(productId) {
    let url=`${KY_IP}rhinoMembership/data`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type: 'post',
            data: {productId},
            showLoading: false
        }, (res) => {
            console.log("获取推荐人名称和Id getParentName ######################", res);
            resolve(res)
        }, (res) => {
            console.log("获取推荐人名称和Id getParentName *************", res);
            reject(res)
        });
    });
}

//商品推广二维码
function shareQrCode(productId) {
    let url = `${KY_IP}news/shareQrCode`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type:'get',
            data: {
                productCode:productId
            },
            showLoading: false
        }, (res) => {
            console.log("推手商品列表 shopList ######################", res);
            resolve(res)
        }, (res) => {
            console.log("推手商品列表 shopList *************", res);
            reject(res)
        });
    });
}

//app立即前往埋点
function lastBuyGoods(goodsNo) {
    let url=`${KY_IP}rhinoMembership/lastBuyGoods`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type: 'POST',
            data: {goodsNo},
            showLoading: false
        }, (res) => {
            console.log("app立即前往埋点 lastBuyGoods ######################", res);
            resolve(res)
        }, (res) => {
            console.log("app立即前往埋点 lastBuyGoods *************", res);
            reject(res)
        });
    });
}

//商品列表页告诉后端是否点击过
function editFirstEnter() {
    let url=`${KY_IP}explosiveProducts/editFirstEnter`;
    return new Promise((resolve, reject) => {
        common.Ajax({
            url: url,
            type: 'get',
            data: {},
            showLoading: false
        }, (res) => {
            console.log("商品列表页告诉后端是否点击过 editFirstEnter #############", res);
            resolve(res)
        }, (res) => {
            console.log("商品列表页告诉后端是否点击过 editFirstEnter *************", res);
            reject(res)
        });
    });
}

export {
    shopList, getShopDetail,shareQrCode,getPushHandInfo,getPushIdAndName,lastBuyGoods,editFirstEnter
}