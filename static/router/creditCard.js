let creditCard = () =>
    import ('@/page/creditCard/creditCard.vue');
let creditCard_order = () =>
    import ('@/page/creditCard/creditCard_order.vue');
let onlineCard = () =>
    import ('@/page/creditCard/onlineCard.vue');
let exchangeOrder = () =>
    import ('@/page/creditCard/exchangeOrder.vue');
let shop_order = () =>
    import ('@/page/creditCard/shop_order.vue');

export default [{
        path: "/creditCard",
        name: "creditCard",
        component: creditCard,
        meta: {
            title: "推荐办卡",
            keepAlive: false
        }
    },
    {
        path: "/creditCard_order",
        name: "creditCard_order",
        component: creditCard_order,
        meta: {
            title: "办卡订单",
            keepAlive: false
        }
    },
    {
        path: "/onlineCard",
        name: "onlineCard",
        component: onlineCard,
        meta: {
            title: "在线办卡",
            keepAlive: false
        }
    },
    {
        path: "/exchangeOrder",
        name: "exchangeOrder",
        component: exchangeOrder,
        meta: {
            title: "兑换订单",
            keepAlive: false
        }
    },
    {
        path: "/shop_order",
        name: "shop_order",
        component: shop_order,
        meta: {
            title: "商城订单",
            keepAlive: false
        }
    },
]