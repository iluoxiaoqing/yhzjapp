const detail = () => import('@/page/register/detail.vue');
const privacyPolicy = () => import('@/page/register/privacyPolicy.vue');
const register = () => import('@/page/register/register.vue');
const userAgreement = () => import('@/page/register/userAgreement.vue');
const wxDown = () => import('@/page/register/wxDown.vue');
export default[
    {
        path:"/detail",
        name:"detail",
        component: detail,
        meta:{
            title:"详情",
            keepAlive:false
        }
    },
    {
        path:"/privacyPolicy",
        name:"privacyPolicy",
        component: privacyPolicy,
        meta:{
            title:"隐私保护政策",
            keepAlive:false
        }
    },
    {
        path:"/register",
        name:"register",
        component: register,
        meta:{
            title:"注册",
            keepAlive:false
        }
    },
    {
        path:"/userAgreement",
        name:"userAgreement",
        component: userAgreement,
        meta:{
            title:"用户协议",
            keepAlive:false
        }
    },
    {
        path:"/wxDown",
        name:"wxDown",
        component: wxDown,
        meta:{
            title:"下载",
            keepAlive:false
        }
    }
]
