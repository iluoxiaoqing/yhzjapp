let machinesToolsApply = () => import ('@/page/machinesToolsApply/machinesToolsApply');
let machinesToolsIsApply = () => import ('@/page/machinesToolsApply/machinesToolsIsApply');
let machinesToolsYesApply = () => import ('@/page/machinesToolsApply/machinesToolsYesApply');

export default [
    // 新增
    {
        path: "/machinesToolsApply",
        name: "machinesToolsApply",
        component: machinesToolsApply,
        meta: {
            title: "机具申请",
            keepAlive: false
        }
    },
    {
        path: "/machinesToolsIsApply",
        name: "machinesToolsIsApply",
        component: machinesToolsIsApply,
        meta: {
            title: "机具申请",
            keepAlive: false
        }
    },
    {
        path: "/machinesToolsYesApply",
        name: "machinesToolsYesApply",
        component: machinesToolsYesApply,
        meta: {
            title: "机具申请",
            keepAlive: false
        }
    }
]
