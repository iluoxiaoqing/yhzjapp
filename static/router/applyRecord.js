let applyRecord = () => import ('@/page/applyRecord/applyRecord');
let changePending = () => import ('@/page/applyRecord/changePending');
let changeAgreed = () => import ('@/page/applyRecord/changeAgreed');
let changeRejected = () => import ('@/page/applyRecord/changeRejected');

export default [
    // 新增
    {
        path: "/applyRecord",
        name: "applyRecord",
        component: applyRecord,
        meta: {
            title: "申请记录",
            keepAlive: false
        }
    },
    {
        path: "/changePending",
        name: "changePending",
        component: changePending,
        meta: {
            title: "换机待处理",
            keepAlive: false
        }
    },
    {
        path: "/changeAgreed",
        name: "changeAgreed",
        component: changeAgreed,
        meta: {
            title: "换机已同意",
            keepAlive: false
        }
    },
    {
        path: "/changeRejected",
        name: "changeRejected",
        component: changeRejected,
        meta: {
            title: "换机已驳回",
            keepAlive: false
        }
    }
]
