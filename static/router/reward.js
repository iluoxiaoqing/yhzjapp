let rewardList = () => import('@/page/reward/rewardList.vue');
export default[
    {
        path: "/rewardList",
        name: "rewardList",
        component: rewardList,
        meta: {
            title: "奖励列表",
            keepAlive: false
        }
    }
]
