let addAdress = () => import ('@/page/shop/addAdress');
let adressList = () => import ('@/page/shop/adressList');
let editorAdress = () => import ('@/page/shop/editorAdress');
// let addAddress = () => import ('@/page/shop/addAddress');

export default [
    // 新增
    {
        path: "/addAdress",
        name: "addAdress",
        component: addAdress,
        meta: {
            title: "新建收获地址",
            keepAlive: false
        }
    },
    {
        path: "/adressList",
        name: "adressList",
        component: adressList,
        meta: {
            title: "收获地址",
            keepAlive: false
        }
    },
    {
        path: "/editorAdress",
        name: "editorAdress",
        component: editorAdress,
        meta: {
            title: "收货地址",
            keepAlive: false
        }
    }
]
