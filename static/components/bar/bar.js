export default {
    props: {
        tabIndex: {
            type: String
        },
        barData: {
            type: Object,
            default: {
                buss: [],
                total: 0
            }
        }
    },
    data() {
        return {
            //是否显示更多
            isMore:false,
            zData: {},
            colorList: ['#464C6E', '#FF9F89', '#3F83FF', '#FF7F96', '#36E0D5', '#E0BC36']
        }
    },
    watch:{
        barData(newVal,oldVal){
            if(newVal !== oldVal){
                this.zData = newVal.contribute;
                console.log("zData", this.zData);
            }
        }
    },
    mounted() {
        console.log("ppp",this.barData);
    },
    methods: {

    }
}