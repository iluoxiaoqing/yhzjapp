import * as common from "@/libs/common.js";
import html2canvas from 'html2canvas';

export default{
    props:{
        visible:{
            type:Boolean,
            default:false
        }
    },
    watch:{
        visible(newVal,oldVal){
            if(newVal!==oldVal){
                this.showDia=newVal;
                this.show1=newVal;

            }
        },
        showDia(newVal,oldVal){
            if(newVal!==oldVal){
                // document.body.scrollTop = 0;
                // document.documentElement.scrollTop = 0;
                if(newVal===true){
                    (document.documentElement||document.body).style.height=(document.documentElement||document.body).clientHeight+'px';
                    document.querySelector('#page').style.height=(document.documentElement||document.body).clientHeight+'px';
                    (document.documentElement||document.body).style.overflow="hidden";
                    document.querySelector('#page').style.overflow="hidden";
                    this.$emit("update:visible",newVal);
                }else{
                    document.querySelector('#page').style.height="auto";
                    (document.documentElement||document.body).style.height="auto";
                    document.querySelector('#page').style.overflow="auto";
                    (document.documentElement||document.body).style.overflow="auto";
                    this.$emit("update:visible",newVal);
                }
            }
        }
    },
    data(){
        return{
            isLoading:true,
            showDia:false,
            show1:false,
            htmlUrl:""
        }
    },
    components: {
        html2canvas
    },
    activated() {
        window.saveImg = this.saveImg;
        // this.getHeight();
        // common.youmeng("每日一推","进入每日一推");
    },
    mounted(){
        window.goBack = this.goBack;
        // this.getHeight();
        window.shareSucces = this.shareSucces;
        // setTimeout(()=>{
        //    this.openShare();
        //     console.log("===showDia");
        // },1000)
    },
    methods:{
        openShare(){
            this.showDia=true;
            this.show1=true;
        },
        closeShare(){
            this.showDia = false;
            this.show1=false;
        },
        // reloadImg(){
        //     let shareImgPathPath= document.getElementById("shareImgPath").src;
        //     let ext=shareImgPathPath.substring(shareImgPathPath.indexOf('&temp='),shareImgPathPath.length);
        //     shareImgPathPath=shareImgPathPath.replace(ext,'')+'&temp='+Math.random();
        //     let codePath= document.getElementById("code").src;
        //     let ext2=shareImgPathPath.substring(shareImgPathPath.indexOf('&temp='),shareImgPathPath.length);
        //     codePath=codePath.replace(ext2,'')+'&temp='+Math.random();
        //
        //     document.getElementById("shareImgPath").src=shareImgPathPath;
        //     document.getElementById("code").src=codePath;
        //
        // },
        toImage(type) {
            common.loading("show");
            // 第一个参数是需要生成截图的元素,第二个是自己需要配置的参数,宽高等
            let content=this.$refs.imageTofile;
            // this.reloadImg();
            // setTimeout(()=>{
                html2canvas(content, {
                    // logging:true,
                    useCORS: true
                }).then((canvas) => {
                    let url = canvas.toDataURL('image/png');
                    // let img=new Image();
                    // img.src=url;
                    // document.body.appendChild(canvas);
                    console.log(url);
                    this.htmlUrl = url;
                    common.loading("hide");
                    if (type === "wechatSession"){
                        this.wechatSession();
                    } else if (type === "wechatTimeline"){
                        this.wechatTimeline();
                    } else if (type === "nativeSaveImage"){
                        this.nativeSaveImage();
                    }
                }).catch((err)=>{
                    console.log('err:::::',err);
                })
            // },500)

        },
        wechatSession(){
            // this.toImage();
            let thumbnail = this.htmlUrl;
            let shareJson = {
                "type":"wechatSession",
                "base64String":thumbnail,
                "shareType":"img",
                "callbackName":"shareSucces"
            }
            if(common.isClient()==="ios"){
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage( JSON.stringify(shareJson) );
            }
            else{
                window.android.nativeWechatShareBase64( JSON.stringify(shareJson) );
            }
        },
        wechatTimeline(){
            // this.toImage();
            let thumbnail = this.htmlUrl;
            let shareJson = {
                "type":"wechatTimeline",
                "base64String":thumbnail,
                "shareType":"img",
                "callbackName":"shareSucces"
            }
            if(common.isClient()=="ios"){
                window.webkit.messageHandlers.nativeWechatShareBase64.postMessage( JSON.stringify(shareJson) );
            }
            else{
                window.android.nativeWechatShareBase64( JSON.stringify(shareJson) );
            }
        },
        nativeSaveImage(){
            // this.toImage();
            let imgJson = {};
            imgJson.base64String = this.htmlUrl;
            imgJson.callbackName = "saveImg";
            console.log(imgJson);
            if(common.isClient()=="ios"){
                window.webkit.messageHandlers.nativeSaveImageBase64.postMessage(JSON.stringify(imgJson));
                console.log("imgJson:"+JSON.stringify(imgJson))
            }else{
                window.android.nativeSaveImageBase64(JSON.stringify(imgJson));
                console.log("imgJson:"+JSON.stringify(imgJson))
            }
        },
        saveImg(j){
            console.log("返回状态值："+j);
            if(j == true || j == 0){
                console.log("保存成功");
            }else{
                console.log("保存失败");
            }
        },
        shareSucces(){
        	window.location.reload();
//            this.showDia = false;
//            this.show1=false;
        }
    }
}