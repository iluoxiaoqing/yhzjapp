//默认环形图的索引位置
let currentIndex = -1;
export default {
    props: {
        pieIndex: {
            type: String
        },
        pieInfo: {
            type: Object,
            default: {
                buss: [],
                total: 0
            }
        },
    },
    data(){
        return{
            currentPieData: [],
            pageName: this.$route.name,
            colorList: []
        }
    },
    watch: {
        pieIndex(newVal, oldVal) {
            if (newVal !== oldVal) {
                this.selectPieArea(newVal);
            }
        },
        pieInfo(newVal,oldVal){
            if (newVal !== oldVal) {
                this.pieData();
                this.initPie();
            }
        },
        $route(to,from){
            if (to.name !== from.name) {
                this.pageName = to.name;
            }
        }
    }
    ,
    mounted() {
        console.log("pieRoute", this.pageName);
        window.addEventListener("resize", () => {
            this.pieChar.resize();
        });
    }
    ,
    methods: {
        pieData() {
            this.currentPieData = this.pieInfo.buss.map((item) => ({
                value: item.amount,
                name: item.message
            }));
            console.log("this.currentPieData", this.currentPieData);
        },
        //初始化饼图
        initPie() {
            this.$nextTick(() => {
                this.pieChar = echarts.init(document.getElementById('pieChar'), 'macarons');
                let _colorList = [];
                if (this.$route.name == "source") {
                    _colorList = ['#3F83FF', '#649BFE', '#8AB3FD', '#AEC9FB', '#D9E6FF'];
                } else {
                    _colorList = ['#464C6E', '#FF9F89', '#3F83FF', '#FF7F96', '#36E0D5', 'E0BC36'];
                }
                var option = {
                    selectedOffset: 1,
                    series: [
                        {
                            type: 'pie',
                            radius: ['70%', '86%'],
                            itemStyle: {
                                normal: {
                                    color: function (params) {
                                        // build a color map as your need.
                                        var colorList = _colorList;
                                        return colorList[params.dataIndex]
                                    }
                                }
                            },
                            labelLine: {
                                normal: {
                                    show: false
                                }
                            },
                            label: {
                                normal: {
                                    show: false
                                }
                            },
                            data: this.currentPieData
                        }
                    ]
                };
                // option.series[0].data[2].selected=true;
                // 使用刚指定的配置项和数据显示图表。
                // this.pieChar.showLoading ('default',{
                //     text: '加载中...',
                //     color: '#c23531',
                //     textColor: '#000',
                //     maskColor: 'rgba(255, 255, 255, 0.8)',
                //     zlevel: 11
                // });
                setTimeout(() => {
                    this.pieChar.setOption(option);
                    // this.pieChar.hideLoading();
                }, 1000);

                //这个先模拟个假的之后传入真实值
                setTimeout(() => {
                    this.selectPieArea(this.pieIndex);
                }, 1500);

                //注册点击事件
                let _this = this;
                this.pieChar.on('globalout', function (params) {
                    console.log('globalout....')
                })
                this.pieChar.on('click', function (params) {
                    console.log(params.dataIndex);
                    console.log(_this.pieIndex);
                    _this.$emit('changeDataIndex', params.dataIndex)
                    // _this.pieIndex= params.dataIndex;
                    // this.dispatchAction({
                    //     type: 'downplay',
                    //     seriesIndex: 0,
                    //     dataIndex: _this.pieIndex
                    // });
                    // this.selectPieArea(params.dataIndex)

                });

            })
        }
        ,
        //选中
        selectPieArea(curIndex) {
            // 取消之前高亮的图形
            this.pieChar.dispatchAction({
                type: 'downplay',
                seriesIndex: 0,
                dataIndex: currentIndex
            });
            currentIndex = curIndex;
            // 高亮当前图形
            this.pieChar.dispatchAction({
                type: 'highlight',
                seriesIndex: 0,
                dataIndex: currentIndex
            });
        }
    }

}