import {getCookie, toast, validator} from '@/libs/common';
import getWayHelper from '@/helper/getwayHelper'
import {pushHand2User,pushHandSms} from '@/query/loginQuery'

export default {
    props: {
        isShow:{
            type:Boolean,
            default:false
        },
        isLogin:{ //是否调用匿名登录
            type:Boolean,
            default:false
        }
    },
    data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        }
    },
    watch: {
        isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    mounted(){
        // if(this.isLogin==true){ //需要调用自动登录
        //     let isLogin = getCookie('isLogin');
        //     //判断是否登录
        //     if (isLogin==="false"||isLogin==null) {
        //         //匿名登录
        //         getWayHelper.postToken(null, null, () => {
        //             console.log('匿名登录....');
        //
        //         });
        //     }
        // }
    },
    methods: {
        openAgreement(url){

        },
        //改变状态开始发送验证码
        changeStartTime() {
            this.isStartTime = true;
        },
        //timer处理函数
        setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode() {
            let isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //发送验证码了
                pushHandSms(this.phone).then((res)=>{
                    let {msg}=res;
                    toast({
                        content:msg
                    })
                    this.isStartTime = true;
                    // this.isShowImgCode = true;
                })
            }
        },
        validatePhone() {
            if (!this.phone) {
                toast({
                    "content": "请输入手机号"
                });
                return false;
            }
            let str = validator.checkPhoneNumber(this.phone);
            if (str) {
                toast({
                    content: str.replace("您", '')
                });
                return false;
            }
            return  true;

        },
        validateInfo() {
            let flag = this.validatePhone();
            let temp = true;
            if(flag){
                if (this.vCode === "") {
                    toast({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }
            return temp && flag;
        },
        startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        openUser() {
            // alert("会员登录...");
            // let parentUserNo= window.sessionStorage.getItem("parentUserNo");
            // if(parentUserNo==null||parentUserNo===''){
            //     toast({
            //        content:'推手编号为空'
            //     });
            //     return ;
            // }
            //调用登录
            pushHand2User(this.phone, this.vCode).then((res) => {
                toast({
                    "content": res.msg
                });
            });
            // userLogin(parentUserNo,this.phone,this.vCode).then(()=>{
            //
            //     this.$emit('update:isShow',false);
            // });
        }
    }
}