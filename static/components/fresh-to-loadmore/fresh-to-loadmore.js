import pullTo from "./vue-pull-to.vue";
require('../../assets/icon/iconfont');

export default{
	props:{
		type:String,
        showMask:{
            "type":Boolean,
            "default":false
        }
	},
	mounted(){
		// let bodyHeight = document.documentElement.clientHeight;
        // let scroller = this.$refs.wrapper;
        // let scrollerTop = this.$refs.wrapper.getBoundingClientRect().top;
        // scroller.style.height = (bodyHeight-scrollerTop)+"px";	
	},
	components:{
		pullTo
	},
	methods:{
		refresh(loaded){
			let timer = null;
			clearTimeout(timer);
			timer = setTimeout(()=>{
	        	this.$emit('refresh','loaded');
                loaded('done');
                this.showMask = false;
	        }, 500);
		},
		loadmore(loaded){
			let timer = null;
			clearTimeout(timer);
			timer = setTimeout(()=>{
    			this.$emit('loadmore','loaded');
                loaded('done');
                this.showMask = false;
		    },500); 
		},
		stateChange(state) {
            if (state === 'pull' || state === 'trigger') {
              this.iconLink = '#icon-arrow-bottom';
            } else if (state === 'loading') {
                this.showMask = true;
              this.iconLink = '#icon-loading';
            } else if (state === 'loaded-done') {
              this.iconLink = '#icon-finish';
            }
        },
	}
}