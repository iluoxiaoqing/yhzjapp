import {toast, validator, isClient} from '@/libs/common';
import {USER_DOWNLOAD_URL} from '@/api.config';
import {lastBuyGoods} from '@/query/shopQuery';

export default {
    props: {
        showPay: {
            type: Boolean,
            default: false
        },
        goodsName:{
            type: String,
            default: ''
        },
        //价格小数位
        priceDecimal:{
            type: Number,
            default: 0
        },
        //价格整数位
        priceIntegerDigits:{
            type: Number,
            default: 0
        },
        price:{
            type: String,
            default: '0.0'
        },
        goodsNo:{
            type:String,
            default:''
        }

    },
    data() {
        return {
            // goodsName:'小度智能车载支架 标准版百度旗下小度智能车载支架 标准版百度旗下',
            // price:99.00,
            // priceDecimal:'99',//价格整数位
            // priceIntegerDigits:'12', //价格小数位
        }
    },
    mounted() {
        window.installAppCallback = this.installAppCallback;
        window.openAppCallback = this.openAppCallback;
    },
    methods: {
        closePage() {
            this.$emit('update:showPay', false);
        },
        installAppCallback(isInstall) {



            let _os = isClient();
            if (isInstall) { //安装app
                if (_os === 'ios') {
                    this.nativeOpenBrowser();
                } else {
                    this.nativeOpenApp('openAppCallback')
                }
            } else {//未安装
                // alert(USER_DOWNLOAD_URL);
                window.location.href=USER_DOWNLOAD_URL;
            }
        },
        openAppCallback() {
            ("安卓打开app回调...");
        },
        goPay() {
            lastBuyGoods(this.goodsNo).then((res)=>{
                //先判断是否安装
                this.nativeIsInstalledApp("installAppCallback");
                this.closePage();
            });
        }
    }
}
