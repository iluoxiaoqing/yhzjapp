import {toast, validator} from '@/libs/common';
import {pushHandReg, pushHandRegSms} from '@/query/loginQuery'

export default {
    props: {
        isPushHandShow:{
            type:Boolean,
            default:false
        },
        parentUserNo:{
            type:String,
            default:""
        },
        parentUserName:{
            type:String,
            default:""
        },
        headImagePath:{
            type:String,
            default:""
        }
    },
    data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        }
    },
    watch: {
        isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    methods: {
        openAgreement(url){

        },
        closePage() {
            this.$emit('update:isPushHandShow',false);
            this.phone='';
            this.vCode='';
        },
        //改变状态开始发送验证码
        changeStartTime() {
            this.isStartTime = true;
        },
        //timer处理函数
        setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode() {
            let isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //发送验证码
                pushHandRegSms(this.phone).then((res)=>{
                    let {msg}=res;
                    toast({
                        content:msg
                    });
                    this.isStartTime = true;
                    // this.isShowImgCode = true;
                })
            }
        },
        validatePhone() {
            if (!this.phone) {
                toast({
                    "content": "请输入手机号"
                });
                return false;
            }
            let str = validator.checkPhoneNumber(this.phone);
            if (str) {
                toast({
                    content: str.replace("您", '')
                });
                return false;
            }
            return  true;

        },
        validateInfo() {
            let flag = this.validatePhone();
            let temp = true;
            if(flag){
                if (this.vCode === "") {
                    toast({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }

            return temp && flag;
        },
        startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        openUser() {
            // alert("推手注册或登录...");
            if (this.validateInfo()) {
                //注册推手
                pushHandReg(this.phone, this.vCode,this.parentUserNo).then((res) => {
                    // toast({
                    //     "content":res.msg
                    // });
                    let {loginKey}=res.data;
                    this.closePage();
                    sessionStorage.setItem("loginKey",loginKey);
                    this.$router.push('/handBagDetail');

                });
            }
        }
    }
}