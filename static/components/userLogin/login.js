import {toast, validator} from '@/libs/common';
import {pushHand2User, pushHandSms} from '@/query/loginQuery'


export default {
    props: {

        isNeedGuide:{
            type:Boolean,
            default:false //是否显示引导
        },
        isFirst:{
            type:Boolean, //是否首页进入
            default:true
        },
        cb:{
            type:Function,
            default:()=>{}
        }
    },
    data() {
        return {
            phone: '',
            vCode: '',
            mes: '发送验证码',
            curCount: 59,
            isStartTime: false,
            isShowImgCode: false,
            isEnd: true //倒计时是否结束
        }
    },
    watch: {
        isStartTime(newVal) {
            if (newVal) {
                this.startTimer();
            }
        }
    },
    methods: {
        openAgreement(url) {
            this.nativeOpenPage(url);
        },
        //改变状态开始发送验证码
        changeStartTime() {
            this.isStartTime = true;
        },
        //timer处理函数
        setRemainTimes() {
            if (this.curCount === 0) {
                window.clearInterval(this.timer); //停止计时器
                this.mes = "重新发送验证码";
                this.curCount = 59;
                this.isEnd = true;
                this.isStartTime = false;
                // this.startTimer();
                // code = ""; //清除验证码。如果不清除，过时间后，输入收到的验证码依然有效
            } else {
                if (this.curCount > 0) {
                    this.mes = "请在" + this.curCount-- + "秒内输入";
                    this.isEnd = false;
                }
            }
        },
        sendCode() {
            let isFlag = this.validatePhone();
            if (this.isEnd && isFlag) {
                //告诉外边要发送验证码了
                // this.$emit('sendImgCode', true, this.phone);
                //发送验证码
                pushHandSms(this.phone).then((res) => {
                    toast({
                        content:"验证码已发送"
                    });
                    this.isStartTime = true;
                    // this.isShowImgCode = true;
                })
            }
        },
        validatePhone() {
            if (!this.phone) {
                toast({
                    "content": "请输入手机号"
                });
                return false;
            }
            let str = validator.checkPhoneNumber(this.phone);
            if (str) {
                toast({
                    content: str.replace("您", '')
                });
                return false;
            }
            return true;

        },
        validateInfo() {
            let flag = this.validatePhone();
            let temp = true;
            if(flag){
                if (this.vCode === "") {
                    toast({
                        "content": "请输入短信验证码"
                    });
                    temp = false;
                }
            }
            return temp && flag;
        },
        startTimer() {
            this.timer = setInterval(this.setRemainTimes, 1000);
        },
        confirm() {
            // this.nativeShowAppHomeGuide();
            // return;
            if (this.validateInfo()) {
                //注册犀牛会员
                pushHand2User(this.phone, this.vCode).then((res) => {
                    // toast({
                    //     "content":res.msg
                    // });
                    // alert('显示引导'+this.isNeedGuide);
                    if(this.isNeedGuide){ //显示引导
                        //开启引导
                        this.nativeShowAppHomeGuide();
                    }else{
                        //关闭弹窗
                        if(this.isFirst){
                            this.closeNativeApp();
                        }
                        //跳转
                        this.cb();
                    }
                });
            }
        },
        openUser() {
            alert("会员登录...");
        }
    }
}