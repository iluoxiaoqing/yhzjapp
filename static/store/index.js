import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        purchaseJson: {},
        address: {},
        activityJson: {},
        loanData: {},
        list2: {}
    },
    actions: {
        saveCarList(ctx, data) {
            ctx.commit('saveCarList', data)
        },
        saveList(ctx, data) {
            ctx.commit('saveList', data)
        },
        saveAddress(ctx, data) {
            ctx.commit('saveAddress', data)
        },
        saveLoanData(ctx, data) {
            ctx.commit('saveLoanData', data)
        }
    },
    mutations: {
        saveCarList(state, data) {
            state.purchaseJson = data
        },
        saveList(state, data) {
            state.list2 = data
        },
        saveAddress(state, data) {
            state.address = data
        },
        saveLoanData(state, data) {
            state.loanData = data;
            sessionStorage.setItem("loanData", JSON.stringify(data));
        }
    }
})