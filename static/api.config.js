import VConsole from 'vconsole/dist/vconsole.min.js'; //import vconsole

var ENV = process.ENV;
let KY_IP, KY_H5_IP, TEST_ID, KY_LOGIN = "";
let KY_USER_IP = '';
//h5犀牛会员下载地址(推手里)
let USER_DOWNLOAD_URL = "";
let isWay = true;
let WEB_URL = ``;
//电商详情地址前缀
let SHOP_URL_PREFIX = ``;
//网关相关
let tokenConfig = {};
export const setCookie = function(name, value, Days) {
    if (Days === null || Days === '') {
        Days = 30;
    }
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + "; path=/;expires=" + exp.toGMTString();
}
if (window.location.host == "yhzj.wangaijunrenfeng.com") {
    ENV = "正式环境";
    KY_IP = `https://yhzj.wangaijunrenfeng.com/opay-business/`;
    USER_DOWNLOAD_URL = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html?type=pushhand#home";
    KY_USER_IP = "https://vip.xiaoyaoxinyong.com/public-gateway";
    WEB_URL = `https://yhzj.wangaijunrenfeng.com/opay-sources/app/index.html#/`;
    //网关
    tokenConfig = {
        publicKey: "305C300D06092A864886F70D0101010500034B0030480241009192D14E605E30CCD88524031EE5C1C67C9187BA8129F77DEE28638BDA1C10059BA72DAAE66A9530DB3D7D603C8EC58A50196463A969FFC015EFE2EE3C8A86510203010001",
        clientId: "client_youshua",
        grantTypes: 'password', // "client_credentials,refresh_token",
        clientSecret: "3ae0f46ca81d40c1a8d6567f09e20940",
        scope: "select",
        requestBusiType: "member"
    }
    SHOP_URL_PREFIX = `https://ec.xiaoyaoxinyong.com/ptGoods.html`;
}else if (window.location.host == "xyts.ihaomai88.com") {
    ENV = "正式环境";
    KY_IP = "https://xyts.ihaomai88.com/dirsell-mobile-front/";
    USER_DOWNLOAD_URL = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html?type=pushhand#home";
    KY_USER_IP = "https://vip.xiaoyaoxinyong.com/public-gateway";
    WEB_URL = `https://xyts.ihaomai88.com/dirsell-source/app/index.html#/`;
    setCookie("APPCODE", "YHZJ");
    //网关
    tokenConfig = {
        publicKey: "305C300D06092A864886F70D0101010500034B0030480241009192D14E605E30CCD88524031EE5C1C67C9187BA8129F77DEE28638BDA1C10059BA72DAAE66A9530DB3D7D603C8EC58A50196463A969FFC015EFE2EE3C8A86510203010001",
        clientId: "client_youshua",
        grantTypes: 'password', // "client_credentials,refresh_token",
        clientSecret: "3ae0f46ca81d40c1a8d6567f09e20940",
        scope: "select",
        requestBusiType: "member"
    }
    SHOP_URL_PREFIX = `https://ec.xiaoyaoxinyong.com/ptGoods.html`;
} else if (window.location.host == "yhzj.wangaijunrenfeng.com") {
    ENV = "测试环境";
    KY_IP = `https://yhzj.wangaijunrenfeng.com/opay-business/`;
    WEB_URL = `https://yhzj.wangaijunrenfeng.com/opay-sources/app/index.html#/`;
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    setCookie("APPCODE", "YHZJ");
    //网关
    tokenConfig = {
            publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
            clientId: "client_youshua",
            grantTypes: "password",
            clientSecret: "5274206178d44e5cad27f6af21cf50dd",
            scope: "select",
            requestBusiType: "member"
        }
    SHOP_URL_PREFIX = `https://ec.xiaoyaoxinyong.com/ptGoods.html`;
}else if (window.location.host == "39.106.209.55") {
    ENV = "测试环境";
    KY_IP = "http://39.106.209.55/opay-business/";
    WEB_URL = `http://39.106.209.55/opay-sources/app/index.html#/`;
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    setCookie("APPCODE", "YHZJ");
    new VConsole();
    //网关
    tokenConfig = {
            publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
            clientId: "client_youshua",
            grantTypes: "password",
            clientSecret: "5274206178d44e5cad27f6af21cf50dd",
            scope: "select",
            requestBusiType: "member"
        }
    SHOP_URL_PREFIX = `https://ec.xiaoyaoxinyong.com/ptGoods.html`;
} else {
    ENV = "其他环境";
    setCookie("DEVICETYPE", "1");
    setCookie("APPCODE", "YHZJ");
    setCookie("VERSION", "1.0.0");
    setCookie("LOGINKEY", "2ed2844d315f57244d2909f804bbfeca");
    // setCookie("LOGINKEY", "b328dcd2d854575b50e31770f71b862f");
    // new VConsole();
    setCookie("USERNO", "");
    KY_USER_IP = "http://10.10.129.34:8558/public-gateway";
    USER_DOWNLOAD_URL = "http://testi.phoenixpay.com/downLoadXNHY/downLoad.html?type=pushhand#home";
    isWay = true;
    KY_IP = "http://39.106.209.55/opay-business/"
    WEB_URL = `http://192.168.3.246:8800/#/`;
    // WEB_URL = `http://172.20.10.3:8800/#/`;
    SHOP_URL_PREFIX = `http://fs.onebity.com/ptGoods.html`;
    //测试网关
    tokenConfig = {
        publicKey: '305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001',
        clientId: "client_youshua",
        grantTypes: "password",
        clientSecret: "5274206178d44e5cad27f6af21cf50dd",
        scope: "select",
        requestBusiType: "member"
    }
}

//犀牛会员线上下载地址
let USER_DOWNLOAD_ONLINE = "https://img.xiaoyaoxinyong.com/static/downLoadXNHY/downLoad.html#home";
console.log({ "当前环境:": ENV, "后端接口:": KY_IP });

module.exports = {
    ENV: ENV,
    KY_USER_IP,
    USER_DOWNLOAD_URL,
    USER_DOWNLOAD_ONLINE,
    KY_IP: KY_IP,
    isWay,
    WEB_URL,
    tokenConfig,
    KY_H5_IP: KY_H5_IP,
    TEST_ID: TEST_ID,
    KY_LOGIN: KY_LOGIN,
    SHOP_URL_PREFIX
};
