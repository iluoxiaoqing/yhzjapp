import api from "@/api.config.js";
import * as common from "@/libs/common.js";

export default {
    data() {
        return {
            showDia: false,
            showDia1: false,
            showDia2: false,
            index: 0,
            downUrl: api.KY_IP,
            showLoading: false,
            androidUrl: "",
            iosUrl: "",
            ios: false,
            android: false,
            isios: false,
            isAndioid: false,
            isApi: false,
            isOpt: false
        }
    },
    mounted() {
        let _this = this
        _this.getUrl();
        // _this.isWeixin();
        document.getElementById("container").style.minHeight  = window.screen.height + "px";
    },
    methods: {
        isWeixin() {
            let _this = this
            var ua = window.navigator.userAgent.toLowerCase();
            if (common.isClient() == "ios") {
                _this.isios = true;
                _this.isAndioid = false;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                    _this.showDia2 = false;
                    _this.showDia1 = true;
                    return true;
                } else {
                    _this.showDia2 = true;
                    _this.showDia1 = false;
                    return false;
                }
            } else {
                _this.isios = false;
                _this.isAndioid = true;
                if (ua.match(/MicroMessenger/i) == 'micromessenger') {

                    _this.showDia2 = false;
                    _this.showDia1 = true;
                    return true;
                } else {
                    _this.showDia2 = true;
                    _this.showDia1 = false;
                    return false;
                }
            }
        },
        showDown() {
            let _this = this
            _this.showDia = true;
        },
        getUrl() {
            let _this = this
            common.Ajax({
                url: api.KY_IP + "appVersion/download",
                data: {
                    appCode: "YHZJ",
                    osType: common.isClient(),
                },
                showLoading: false
            }, (data) => {
                if (common.isClient() == 'ios') {
                    _this.iosUrl = data.data;
                } else {
                    _this.androidUrl = data.data;
                }
                _this.isWeixin();
            });
        }
    }
}
