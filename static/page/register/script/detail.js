import api from "@/api.config.js";
import * as common from "@/libs/common.js";
import baseJs from "@/libs/base.js";

export default {
    data() {
        return {
            address: ''
        }
    },
    mounted() {
        this.getImg();
    },
    methods: {
        getImg() {
            common.Ajax({
                "url": api.KY_IP + "forum/findDetailHtml",
                "data": {
                    "userNo": this.$route.query.userNo,
                    "id": this.$route.query.id
                        // "sourceUser": "95191739" //测试用编号
                },
                showLoading: true
            }, (data) => {
                console.log(data);
                if (data.code == "0000") {
                    console.log(data);
                    let imgurl = data.data.detailUrl;
                    if (imgurl.indexOf("http") != -1) {
                        this.address = imgurl;
                    } else {
                        this.address = api.KY_IP + 'file/downloadFile?filePath=' + imgurl;
                    }
                }
            });
        },
    }
}
