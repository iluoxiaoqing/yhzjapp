import api from "@/api.config.js";
import baseJs from "@/libs/base.js";
import * as common from "@/libs/common.js";

export default {
    data() {
        return {
            butnTitle: "获取验证码",
            imgUrl: '',
            vCode: "",
            phoneNo: "",
            code: "",
            password: "",
            imgKey: "",
            timerNum: 60,
            clock: '',
            isNotClick: false,
            isAcitive: false,
            inviteName: ''
        }
    },
    watch: {
        timerNum(newValue) {
            let _this = this
            if (newValue > 0) {
                _this.butnTitle = newValue + 's';
            } else {
                clearInterval(this.clock);
                _this.isAcitive = false;
                _this.isNotClick = false;
                _this.butnTitle = '获取验证码';
            }
        }
    },
    mounted() {
        this.getName();
    },
    methods: {
        jumpUser() {
            window.location.href = api.WEB_URL + 'userAgreement'
        },
        jumpZheng() {
            window.location.href = api.WEB_URL + 'privacyPolicy'
        },
        getName() {
            let _this = this
            common.Ajax({
                url: api.KY_IP + "expansion/findUserName",
                "data": {
                    "sourceUser": this.$route.query.sourceUser,
                    // "sourceUser": "95191739" //测试用
                },
                showLoading: true
            }, (data) => {
                if (data.code === "0000") {
                    _this.inviteName = data.data;
                }
            });
        },
        downloadUser() {
            window.location.href = api.WEB_URL + 'wxDown';
        },
        //发送验证码
        requestVeriCode() {
            var myReg = baseJs.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                var errorMsg = baseJs.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            common.Ajax({
                url: api.KY_IP + "user/sendCheckCode",
                data: {
                    'phoneNo': this.phoneNo,
                    'businessType': 'SIGNUP',
                    'appCode': 'YHZJ',
                    'version': '',
                    'osType': common.isClient(),
                },
                showLoading: true
            }, (data) => {
                if (data.code === "0000") {
                    common.toast({
                        content: "验证码已经发送"
                    });
                    this.sendVericdoe();
                } else if (data.code === "0001") {
                    this.sendVericdoe();
                    common.toast({
                        content: data.msg
                    });
                }
            });
        },
        sendVericdoe() {
            let _this = this
            _this.timerNum = 60;
            _this.butnTitle = this.timerNum + 's';
            _this.isAcitive = true;
            _this.isNotClick = true;
            _this.clock = setInterval(() => {
                _this.timerNum--;
            }, 1000);
        },
        openUser() {
            // 验证手机号是否正确
            let myReg = baseJs.phoneNumberTwo.reg;
            if (!myReg.test(this.phoneNo)) {
                let errorMsg = baseJs.phoneNumberTwo.error;
                common.toast({
                    content: errorMsg
                });
                return;
            }
            // 验证密码
            let veriReg = baseJs.password3.reg;

            if (!veriReg.test(this.password)) {
                let veriErrorMsg = baseJs.password3.error;
                common.toast({
                    content: veriErrorMsg
                });
                return;
            }
            common.Ajax({
                url: api.KY_IP + "user/signUp",
                data: {
                    phoneNo: this.phoneNo,
                    // parentUserNo: '95191739',
                    parentUserNo: this.$route.query.sourceUser,
                    password: this.password,
                    checkCode: this.code,
                    appCode: 'YHZJ'
                },
                showLoading: true
            }, (data) => {
                if ( data.code === '0000') {
                    common.toast({
                        content: '注册成功'
                    });
                    this.$router.push({
                        "path": "wxDown",
                    });
                }
            });
        }
    }
}
