import api from "@/api.config.js";
import * as common from "@/libs/common.js";
import searchBox from "@/components/searchBox.vue";
import freshToLoadmore from "@/components/fresh-to-loadmore/fresh-to-loadmore.vue";
import noData from "@/components/noData.vue";

export default{
	data(){
		return{
			menuArray:[{
				title:"审核中",
				active:true
			},{
				title:"审核成功",
				active:false
			},{
				title:"审核失败",
				active:false
			}],
			showDia:false,
            cardList:[],
            allCardList:[{
                "cardList":[],
                "page":1,
                "isNoData":false
            },
            {
                "cardList":[],
                "page":1,
                "isNoData":false
            },
            {
                "cardList":[],
                "page":1,
                "isNoData":false
            }],
			isLoading:true,
			dialog1:false,
			dialog2:false,
			dialog3:false,
			dialog4:false,
			currentPage:1,
			mySwiper:null,
			index:0,
			page:"",
			hasData:false,
			needMess:false,
			needPicture:false,
			bussFlowNo:"",
			imgUrl:"",
			imgCode:"",
			msgCode:"",
			idNo:"",
			custName:"",
			custPhone:"",
			// isLoading:true,
			defaultText:"",

            loginKey:common.getLoginKey(),
			minHeight:0,

			/*倒计时*/
			countTotal: 60,
			countDown: 0,
			isCounted: false,
			countDownText: "获取验证码",
			timer: null
		}
	},
	components:{
        searchBox,
        freshToLoadmore,
        noData
	},
	beforeRouteEnter(to,from,next){
		if(!sessionStorage.askPositon || from.path == '/'){
			sessionStorage.askPositon = '';
			next();
		  }else{
			next(vm => {
				if(vm && vm.$refs.my_scroller){//通过vm实例访问this
					  setTimeout(function () {
						vm.$refs.my_scroller.scrollTo(0, sessionStorage.askPositon, false);
					  },20)//同步转异步操作
				   }
			})
		  }
	},
	beforeRouteLeave(to,from,next){//记录离开时的位置
		sessionStorage.askPositon = this.$refs.my_scroller && this.$refs.my_scroller.getPosition() && this.$refs.my_scroller.getPosition().top;
		next();
	},
	mounted(){
		window.goBack = this.goBack;
		// this.getYzm();
        this.$refs.searchBox.inputValue="";

        this.getHeight();
        this.getPageList();

        let type = this.$route.query.type||0;

        this.index = type;

        this.imgCode="";

		this.mySwiper = new Swiper('.swiper-container', {
			slidesPerView:"auto",
			autoplay: false,//可选选项，自动滑动
			loop:false,
            autoHeight: false,
			resistanceRatio : 0,
			observer:true,
			observeParents:true,//修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: (swiper)=>{

                this.currentPage = 1;
                this.cardList = [];
		    	let index = swiper.activeIndex;
		    	this.menuArray.map((el)=>{
					el.active = false;
				});
				this.menuArray[index].active = true;
                this.index = index;

				document.getElementById("scrollContainer").scrollTop = 0;
                document.getElementById("scrollContainer").setAttribute("scrollTop",0);

                if(this.allCardList[index].cardList.length==0){
                    this.getPageList();
                }
		    }
        });


		this.changeList(this.menuArray[type],type);
    },
	methods:{
		changeList(i,index){
			this.index=index;
			this.$refs.searchBox.inputValue="";
			//this.refresh();
			this.menuArray.map((el)=>{
				el.active = false;
			});
			i.active = true;
			this.mySwiper.slideTo(index, 500, true);
		},
		getHeight(){
            let bodyHeight = document.documentElement.clientHeight;
            let scroller = this.$refs.scroller;
            let scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = (bodyHeight-scrollerTop)+"px";
            this.minHeight = (bodyHeight-scrollerTop)+"px";
		},
		hideMiddle(val){
			return `${val.substring(0,4)}***********${val.substring(val.length-4)}`
		},
		loadmore(){
            this.getPageList();
        },
        refresh(){
            this.allCardList[this.index].page = 1;
            this.allCardList[this.index].cardList = [];
            this.getPageList();
		},
		checkProgress(bussFlowNo,index){
			common.Ajax({
                url:api.KY_IP+"creditCard/getCreditCardSchedule",
                data:{
					"bussFlowNo":bussFlowNo
                },
                showLoading:false
            },(data)=>{
				if(data.code == "0007"){
					common.toast({
						content:data.msg
					})
				}else if(data.code == "0000"){

					this.bussFlowNo = bussFlowNo;
					this.needMess = data.data.needMess;
                    this.needPicture = data.data.needPicture;

					if(this.needMess == false && this.needPicture==false){
						this.showDia = true;
						this.dialog1 = true;
					}else if(this.needMess==true && this.needPicture==false){
						this.showDia = true;
						this.dialog3 = true;
					}else if(this.needMess==false && this.needPicture==true){
						this.showDia = true;
						this.dialog2 = true;
						this.getYzm();
					}else{
						this.showDia = true;
						this.dialog4 = true;
						this.getYzm();
                    }

                    console.log( this.allCardList[this.index].cardList[index] )

					this.idNo = this.allCardList[this.index].cardList[index].idNo;
					this.custName = this.allCardList[this.index].cardList[index].custName;
					this.custPhone = this.allCardList[this.index].cardList[index].custPhone;

				}else{
					common.toast({
						content:data.msg
					})
				}

			});
		},
		know(){
			this.dialog1=false;
			this.showDia=false;
		},
		searchBtn(){

            // this.$set( this.allCardList[this.index],"page",1 );
            // this.$set( this.allCardList[this.index],"cardList",[] );

            this.allCardList[this.index].page = 1;
            this.allCardList[this.index].cardList = [];

            this.getPageList();
        },
		getPageList(){

            let billStatus = "";

            switch(this.index) {
                case 0:
                    billStatus = "AUDITING";
                    break;
                case 1:
                    billStatus = "SUCCESS";
                    break;
                case 2:
                    billStatus = "REJECT";
                    break;
            }

            common.Ajax({
                url:api.KY_IP+"cardBill/queryCardBill",
                data:{
                    "page":this.allCardList[this.index].page,
                    "billStatus":billStatus,
                    "userName":this.$refs.searchBox.inputValue
                },
                showLoading:false
            },(data)=>{

                let curPage = this.allCardList[this.index].page;
                let Index = this.index;

                if(data.data.object.length>0){

                    data.data.object.map((el)=>{
                        this.$set(el,"show",true);
                        this.allCardList[Index].cardList.push(el);
                    });

                    curPage++;

                    this.$set(this.allCardList[Index],"isNoData",false);
                    this.$set(this.allCardList[Index],"page",curPage);

                }
                else{
                    if(curPage==1){
                        this.allCardList[Index].isNoData = true;
                    }
                    else{
                        common.toast({
                            "content":"没有更多数据了"
                        })
                    }
                }
            })
		},
		confirm(index){

            console.log("测试列表index",index);

            this.imgCode = this.$refs.imgCode?this.$refs.imgCode.value:"";
            this.msgCode = this.$refs.msgCode?this.$refs.msgCode.value:"";

            if(this.needMess==true && this.needPicture==true){
                if(!this.imgCode){
                    common.toast({
                        content:"请输入图形验证码！"
                    });
                    return;
                }
                if(!this.msgCode){
                    common.toast({
                        content:"请输入短信验证码"
                    });
                    return;
                }
            }
            else if(this.needMess==true && this.needPicture==false){
                if(!this.msgCode){
                    common.toast({
                        content:"请输入短信验证码"
                    });
                    return;
                }
            }
            else if(this.needMess==false && this.needPicture==true){
                if(!this.imgCode){
                    common.toast({
                        content:"请输入图形验证码！"
                    });
                    return;
                }
            }

			common.Ajax({
                url:api.KY_IP+"creditCard/getCreditCardResult",
                data:{
					"bussFlowNo":this.bussFlowNo,
					"imgCode":this.imgCode,
					"msgCode":this.msgCode
                },
                showLoading:true
            },(data)=>{
				console.log("1111");
				if(data.code == "0000"){
					console.log("0000");
					this.showDia=false;
					this.dialog1=false;
					this.dialog2=false;
					this.dialog3=false;
					this.dialog4=false;
					this.imgCode="";
					if(this.$refs.imgCode){
						this.$refs.imgCode.value = "";
					}
                    if(this.$refs.msgCode){
						this.$refs.msgCode.value = "";
					}
                    this.msgCode="";
                    // this.msgCode = this.$refs.msgCode?this.$refs.msgCode.value:"";
					this.closeClock();
					common.toast({
						content:data.data.resultDesc,
						time: 4000
					})

				}else{
					common.toast({
						content:data.msg
					})
				}
            });

		},
		dia2Cancel(){
			this.showDia=false;
			this.dialog2=false;
            this.imgCode="";
            this.$refs.imgCode.value = "";
		},
		dia3Cancel(){
			this.showDia=false;
			this.dialog3=false;
            this.$refs.msgCode.value = "";

			this.closeClock();
		},
		dia4Cancel(){
			this.showDia=false;
			this.dialog4=false;
            this.imgCode="";
            this.$refs.imgCode.value = "";
            this.msgCode="";
            this.$refs.msgCode.value = "";

			this.closeClock();
		},
		closeClock(){
			this.countDownText = "获取验证码";
			this.isCounted = true;
            this.countDown = 0;
            clearInterval(this.timer);
		},
		getMess(bussFlowNo){

            this.imgCode = this.$refs.imgCode?this.$refs.imgCode.value:"";

			common.Ajax({
                url:api.KY_IP+"creditCard/getMsmCode",
                data:{
					"bussFlowNo":bussFlowNo,
					"imgCode":this.imgCode
                },
                showLoading:false
            },(data)=>{
				if(data.code == "0000"){
					common.toast({
						content:data.data
					});
					setTimeout(this.countDownFn(),2000);
					// this.countDownFn();
				}else{
					common.toast({
						content:data.msg
					})
				}

            });

		},
		countDownFn(){
            // let timer = null;

            this.isCounted = false;
            this.countDown = this.countTotal;
            this.countDownText = this.countTotal+"s";
            clearInterval(this.timer);
            this.timer = setInterval(()=>{

                if(this.countDown==1){
                    clearInterval(this.timer);
                    this.countDownText = "重新获取";
                    this.countDown = this.countTotal;
                    this.isCounted = true;

                }else{
                    this.countDown --;
                    this.countDownText =
                        (this.countDown>9?
                        this.countDown:
                        "0"+this.countDown)+"s";
                    this.isCounted = false;
                }

            },1000);
        },
		getMess1(bussFlowNo){

            this.imgCode = this.$refs.imgCode.value;

			if(this.imgCode==""){
				common.toast({
					content:"请先输入图片验证码"
				})
			}else{
				common.Ajax({
					url:api.KY_IP+"creditCard/getMsmCode",
					data:{
						"bussFlowNo":bussFlowNo,
						"imgCode":this.imgCode
					},
					showLoading:false
				},(data)=>{
					if(data.code == "0000"){
						common.toast({
							content:data.data
						});
						setTimeout(this.countDownFn(),2000);
					}else{
						common.toast({
							content:data.msg
						})
					}

				});
			}
		},
		getYzm(){
            this.imgUrl = api.KY_IP+'creditCard/getVCodePic?bussFlowNo='+this.bussFlowNo+'&flag='+Math.random()+'&loginKey='+this.loginKey
            // document.getElementById("yzmimage").src = `${api.KY_IP}creditCard/getVCodePic?bussFlowNo=${this.bussFlowNo}&flag=${Math.random()}&loginKey=${this.loginKey}`
		}
	}
}
