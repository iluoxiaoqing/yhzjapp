import api from "@/api.config.js";
import * as common from "@/libs/common";
import confirmInfo from "@/components/confirmInfo.vue";
import alertBox from "@/components/alertBox.vue";

export default{
    data(){
        return{
            bankList:[],
            showConfirm:false,
            imgUrl:api.KY_IP,
            productCode:"",
            bankName:"",
            comfirmText:{
                "content":"请确认以上信息与信用卡申请信息完全一致，填写错误将会导致信用卡申请无法通过，或者无法查询卡片办理。"
            },
            repeatShow:false,
            repeatInfo:{
                "title":"",
				"content":"",
                "confirmText":"我知道了",
                "cancelText":"继续办理"
            }
        }
    },
    components: {
        confirmInfo,
        alertBox
    },
    mounted(){
        window.goBack = this.goBack;

        common.Ajax({
            "url":api.KY_IP+"bussMenu/selfBusiness",
            "data":{
                "buss":"CREDIT_CARD_CODE"
            }
        },(data)=>{
            this.bankList = data.data.groups;
            this.bankList.map((el)=>{
                el.products.map((i)=>{
                    this.$set(i,"custDesc",JSON.parse(i.custDesc))
                })
            });
            console.log( this.bankList )
        });
        common.youmeng("推荐办卡","进入推荐办卡");
    },
    methods:{
        goBack(){
            this.$router.push({
                "path":"creditCard"
            })
        },
        confirmCard(j){

            common.youmeng("在线办卡",j.productName);

            this.productCode = j.productCode;
            this.bankName = j.productName;
            common.Ajax({
                "url":api.KY_IP+"user/isShow",
                "data":{
                    "businessCode":"CREDIT_CARD_CODE",
                    "productCode":this.productCode
                }
            },(data)=>{
                if (data.data.isShow) {
                    this.repeatShow = true;
                    this.repeatInfo.content = data.data.showMessage;
                } else {
                    this.showConfirm = true;
                }
            });
        },
        comfirmInfo(){

            common.youmeng("在线办卡",this.bankName+"-确认办理");

            common.Ajax({
                "url":api.KY_IP+"creditCard/transact",
                "data":{
                    "productCode":this.productCode
                }
            },(data)=>{
                window.location.href = data.data;
            });
            
        },
        repeatConfirm(){
            this.showConfirm = true;
        }
    }  
}