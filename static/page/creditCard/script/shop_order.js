import api from "@/api.config.js";
import * as common from "@/libs/common.js";
import searchBox from "@/components/searchBox.vue";
import freshToLoadmore from "@/components/fresh-to-loadmore/fresh-to-loadmore.vue";
import noData from "@/components/noData.vue";

export default {
    data() {
        return {
            menuArray: [{
                title: "已支付",
                active: true
            }, {
                title: "已返佣",
                active: false
            }, {
                title: "已退款",
                active: false
            }],
            showDia: false,
            cardList: [],
            allCardList: [{
                    "cardList": [],
                    "page": 1,
                    "isNoData": false
                },
                {
                    "cardList": [],
                    "page": 1,
                    "isNoData": false
                },
                {
                    "cardList": [],
                    "page": 1,
                    "isNoData": false
                }
            ],
            isLoading: true,
            // dialog1: false,
            // dialog2: false,
            // dialog3: false,
            // dialog4: false,
            currentPage: 1,
            mySwiper: null,
            index: 0,
            page: "",
            hasData: false,
            // needMess: false,
            // needPicture: false,
            // bussFlowNo: "",
            // imgUrl: "",
            // imgCode: "",
            // msgCode: "",
            idNo: "",
            custName: "",
            custPhone: "",
            defaultText: "",

            loginKey: common.getLoginKey(),
            minHeight: 0,

            /*倒计时*/
            countTotal: 60,
            countDown: 0,
            isCounted: false,
            countDownText: "获取验证码",
            timer: null
        }
    },
    components: {
        // searchBox,
        freshToLoadmore,
        noData
    },
    beforeRouteEnter(to, from, next) {
        if (!sessionStorage.askPositon || from.path == '/') {
            sessionStorage.askPositon = '';
            next();
        } else {
            next(vm => {
                if (vm && vm.$refs.my_scroller) { //通过vm实例访问this
                    setTimeout(function() {
                            vm.$refs.my_scroller.scrollTo(0, sessionStorage.askPositon, false);
                        }, 20) //同步转异步操作
                }
            })
        }
    },
    beforeRouteLeave(to, from, next) { //记录离开时的位置
        sessionStorage.askPositon = this.$refs.my_scroller && this.$refs.my_scroller.getPosition() && this.$refs.my_scroller.getPosition().top;
        next();
    },
    mounted() {
        window.goBack = this.goBack;
        // this.$refs.searchBox.inputValue = "";

        this.getHeight();
        this.getPageList();

        let type = this.$route.query.type || 0;

        this.index = type;

        this.imgCode = "";

        this.mySwiper = new Swiper('.swiper-container', {
            slidesPerView: "auto",
            autoplay: false, //可选选项，自动滑动
            loop: false,
            autoHeight: false,
            resistanceRatio: 0,
            observer: true,
            observeParents: true, //修改swiper的父元素时，自动初始化swiper
            onSlideChangeEnd: (swiper) => {

                this.currentPage = 1;
                this.cardList = [];
                let index = swiper.activeIndex;
                this.menuArray.map((el) => {
                    el.active = false;
                });
                this.menuArray[index].active = true;
                this.index = index;

                document.getElementById("scrollContainer").scrollTop = 0;
                document.getElementById("scrollContainer").setAttribute("scrollTop", 0);

                if (this.allCardList[index].cardList.length == 0) {
                    this.getPageList();
                }
            }
        });


        this.changeList(this.menuArray[type], type);
    },
    methods: {
        changeList(i, index) {
            this.index = index;
            // this.$refs.searchBox.inputValue = "";
            //this.refresh();
            this.menuArray.map((el) => {
                el.active = false;
            });
            i.active = true;
            this.mySwiper.slideTo(index, 500, true);
        },
        getHeight() {
            let bodyHeight = document.documentElement.clientHeight;
            let scroller = this.$refs.scroller;
            let scrollerTop = scroller.getBoundingClientRect().top;
            scroller.style.height = (bodyHeight - scrollerTop) + "px";
            this.minHeight = (bodyHeight - scrollerTop) + "px";
        },

        loadmore() {
            this.getPageList();
        },
        refresh() {
            this.allCardList[this.index].page = 1;
            this.allCardList[this.index].cardList = [];
            this.getPageList();
        },

        getPageList() {

            let billStatus = "";

            switch (this.index) {
                case 0:
                    billStatus = "PAYED";
                    break;
                case 1:
                    billStatus = "SETTLED";
                    break;
                case 2:
                    billStatus = "PAY_CANCEL";
                    break;
            }

            common.Ajax({
                url: api.KY_IP + "mall/order",
                data: {
                    "pageNo": this.allCardList[this.index].page,
                    "status": billStatus,
                    // "userName": this.$refs.searchBox.inputValue
                },
                showLoading: false
            }, (data) => {

                let curPage = this.allCardList[this.index].page;
                let Index = this.index;

                if (data.data.length > 0) {

                    data.data.map((el) => {
                        this.$set(el, "show", true);
                        this.allCardList[Index].cardList.push(el);
                    });

                    curPage++;

                    this.$set(this.allCardList[Index], "isNoData", false);
                    this.$set(this.allCardList[Index], "page", curPage);

                } else {
                    if (curPage == 1) {
                        this.allCardList[Index].isNoData = true;
                    } else {
                        common.toast({
                            "content": "没有更多数据了"
                        })
                    }
                }
            })
        },

    }
}
