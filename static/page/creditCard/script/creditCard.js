import api from "@/api.config.js";
import * as common from "@/libs/common";

export default{
    data(){
        return{
            bussGroups:[],
            business:{},
            imgUrl1:api.KY_IP,
            bannerList:[],
            mainCode:"",
            mainCodeName:"",

        }
    },
    mounted(){

        common.Ajax({
            "url":api.KY_IP+"bussMenu/appBusiness",
            "data":{
                "buss":"CREDIT_CARD_CODE"
            }
        },(data)=>{
            console.log(data.data)
            this.business = data.data.business;
            this.bussGroups = data.data.groups;
            this.mainCode = data.data.business.productCode;
            this.mainCodeName = data.data.business.productName;
            data.data.groups.map((el)=>{
                el.products.map((value)=>{
                    let userDesc = value.userDesc||"";
                    this.$set(value,"userDesc",userDesc?JSON.parse(userDesc):{})
                });
            })
        });

        common.Ajax({
            "url":api.KY_IP+"wonderfulActivityInfo/findPageBanner",
            "data":{
                "bannerType":"CREDIT_CRAD_CODE "
            }
        },(data)=>{
            this.bannerList=data.data||[];
            this.$nextTick(this.mySwiper)
        });

        common.youmeng("推荐办卡","进入推荐办卡");
    },
    methods:{
        gotoShare(j){
            
            if(!j.isShare){
                return;
            }

            this.$router.push({
                "path":"share",
                "query":{
                    "product":j.productCode,
                    "productName":j.productName,
                    "channel":"creditCard"
                }
            });

            common.youmeng("推荐办卡","点击"+j.productName);
        },
        mySwiper(){
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2000,//可选选项，自动滑动
                loop:true,
                pagination:".swiper-pagination"
            });
        },
        gotoOnline(){

            common.youmeng("贷款申请","点击我要办卡");

            this.$router.push({
                "path":"onlineCard",
                "query":{
                    "buss":this.mainCode
                }
            });
        }
    }
}