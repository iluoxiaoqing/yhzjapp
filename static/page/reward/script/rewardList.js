import api from "@/api.config.js";
import * as common from "@/libs/common.js";
import {getCookie} from "@/libs/common";
export default {
    data() {
        return {
            emptyConDia:false,
            conDia:false,
            loginKey:common.getLoginKey(),
            rewardList:[],
            bagList:[]
        }
    },
    props: {

    },
    components: {

    },
    mounted() {
        let _this = this
        _this.rewardArray()
    },
    methods: {
        rewardArray(){
            let _this= this;
            _this.rewardList = []
            common.Ajax({
                "url":api.KY_IP+"dawnPlan/taskList",
                "type": "post",
                "data":{
                    loginKey : common.getLoginKey()
                }
            },(data)=>{
                let bagList = data.data.taskList;
                for(let i in bagList){
                    bagList[i].changeJian = false
                    bagList[i].rewardShow = true
                }
                if (data.data.dawnPlanStatus == 'LIMING_NOJOIN') {
                    _this.emptyConDia = true
                }
                if (data.data.dawnPlanStatus == 'LIMING_JOIN_FINISH') {
                    _this.conDia = true
                }
                this.rewardList = bagList
            });
        },
        clickUnfold(item,index){
            let _this = this
            if ( _this.rewardList[index].changeJian == true){
                _this.rewardList[index].changeJian = false
                _this.rewardList[index].rewardShow = true
                return
            }
            for (let item in _this.rewardList) {
                _this.rewardList[item].changeJian = false
                _this.rewardList[item].rewardShow = true
            }
            _this.rewardList[index].changeJian = true
            _this.rewardList[index].rewardShow = false
        },
        clickGetReward(item,index,type){
            let _this = this
            let dawnPlan=  _this.rewardList[index].dawnPlan
            if(type == 'PEND'){
                common.Ajax({
                    "url":api.KY_IP+"dawnPlan/receive",
                    "type": "post",
                    "data":{
                        loginKey : common.getLoginKey(),
                        dawnPlan:dawnPlan
                    }
                },(data)=>{
                    _this.$nextTick(()=>{
                        _this.rewardArray()
                    })
                });
            }
        },
        //返回首页
        goBackPage(){
            window.history.back();
        },
        //关闭
        confirmDia(){
            let _this = this
            _this.emptyConDia = false
            _this.conDia = false
            // this.$router.go(-1)
            // window.history.go(-1);
        }
    }
}
