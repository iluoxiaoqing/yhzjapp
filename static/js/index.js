import Vue from "vue";
import VueRouter from "vue-router";
import App from "../App.vue";
import { Picker, DatetimePicker } from '@/libs/we-vue';
import VueScroller from 'vue-scroller'
import creditCard from "@/router/creditCard";
import register from "@/router/register";
import applyRecord from "@/router/applyRecord";
import machinesToolsApply from "@/router/machinesToolsApply";

import Bus from "@/components/bus.js";
import sharePic from '@/components/sharePic/share.vue';
// import pushHandShare from '@/page/vipUser/pushHandShare.vue';
import login from '@/components/userLogin/login.vue';
import loginWx from '@/components/userLoginWx/login.vue';
import { KY_IP } from '@/api.config';
import { toast } from '@/libs/common'
import {startPhone} from "@/libs/filters";
Vue.prototype.$startPhone = startPhone;
//去犀牛会员购买
import userPayDialog from '@/components/userPayDialog/userPayDialog.vue';
import swiper from '@/components/swiper/swiper.vue';
import pushHandLogin from '@/components/pushHandLogin/login.vue';
import verifiCode from '@/components/userLogin/verifiCode.vue';

import { default as common, isClient } from '@/libs/common';


Vue.use(VueScroller);
Vue.use(Picker);
Vue.use(DatetimePicker);
import Vant from 'vant';
Vue.use(Vant);

Vue.component("share", sharePic);
// Vue.component("pushHandShare", pushHandShare);
//去犀牛会员购买
Vue.component("userPayDialog", userPayDialog);
Vue.component("login", login);
Vue.component("loginWx", loginWx);
Vue.component("pushHandLogin", pushHandLogin);
Vue.component("swiper", swiper);
Vue.component("verifiCode", verifiCode);

//手机号指令
Vue.directive('enterNumber', {
    inserted: function(el) {
        el.addEventListener("keypress", function(e) {
            e = e || window.event;
            let charcode = typeof e.charCode == 'number' ? e.charCode : e.keyCode;
            let re = /\d/;
            if (!re.test(String.fromCharCode(charcode)) && charcode > 9 && !e.ctrlKey) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                }
            }
        });
    }
});
//结局ios下input错误问题指令
Vue.directive('iosInputBug', {
    inserted: function(el) {
        if (isClient() === "ios") { //ios下指令生效
            let temp;
            el.onfocus = function() {
                temp = document.body.scrollHeight;
            };
            el.onblur = function() {
                setTimeout(() => {
                        document.body.scrollTop = temp;
                        document.activeElement.scrollIntoViewIfNeeded(true);
                    }, 100)
                    // toast({
                    //     content:"焦点移除"
                    // })
            }
        }
    }
})
Vue.directive('iosInputBug2', {
    inserted: function(el) {
        if (isClient() === "ios") { //ios下指令生效
            let temp;
            el.onfocus = function() {
                temp = 0; // document.body.scrollHeight;
                document.body.scrollTop = temp;
                // alert(1);
                // console.log('=====');
                // toast({
                //     content:'===='
                // })
            };
            el.onblur = function() {
                setTimeout(() => {
                        document.body.scrollTop = temp;
                        document.activeElement.scrollIntoViewIfNeeded(true);
                    }, 100)
                    // toast({
                    //     content:"焦点移除"
                    // })
            }
        }
    }
})

Vue.filter('textOverflow', (val, max) => {
    if (val) {
        let len = val.length;
        let maxNum = max == null ? 20 : max;
        return len > maxNum ? val.substr(0, maxNum) + "..." : val;
    } else {
        return val;
    }
});

//手机号隐藏过滤器
Vue.filter('hidePhoneNo', (val) => {
    if (val) {
        let len = val.length;
        return val.substring(0, 3) + "****" + val.substring(len - 4, len);
    } else {
        return val;
    }
});

Vue.mixin({
    // data(){
    //   return{
    //       imgUrl:KY_IP
    //   }
    // },
    mounted() {
        setTimeout(() => {
            this.setImgWidth();
        }, 1000);
    },
    methods: {
        //给商城详情页面设置宽度
        setImgWidth() {
            try {
                let images = document.querySelectorAll('.shop_pics img');
                for (let i = 0; i < images.length; i++) {
                    images[i].style.width = "100%";
                }
            } catch (e) {}
        },
        getImg(url) {
            // alert(1);
            let imgUrl = KY_IP;
            if (url.indexOf("http://") > -1 || url.indexOf("https://") > -1) {
                return url = url;
            } else {
                return url = imgUrl + 'file/downloadFile?filePath=' + url;
            }
        },
        //关闭原生app页面
        closeNativeApp() {
            let _os = isClient();
            if (_os === "ios") {
                // console.log("nativeCloseCurrentPage");
                window.webkit.messageHandlers.nativeCloseCurrentPage.postMessage("");
            } else {
                window.android.nativeCloseCurrentPage("")
            }
        },

        nativeOpenPage(url, isNeedClosePage = "false") {
            let _os = isClient();
            let _dataJson = {
                isH5: true,
                path: url,
                isNeedClosePage
            };
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeOpenPage.postMessage(JSON.stringify(_dataJson));
            } else {
                window.android.nativeOpenPage(JSON.stringify(_dataJson));
            }
        },
        //改变webview尺寸
        changeWebViewSize(w, h, cb) {
            let _os = isClient();
            let json = {
                "width": w,
                "height": h,
                "callbackName": cb
            };
            console.log("nativeChangeDialog.....");
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeChangeDialog.postMessage(JSON.stringify(json));
            } else {
                window.android.nativeChangeDialog(JSON.stringify(json))
            }
        },
        //判断是否安装了某个应用
        nativeIsInstalledApp(callbackName) {
            let _os = isClient();
            let pak = _os === "ios" ? "FSVipApp://" : "com.xinxiaoyao.blackrhino";
            let json = {
                "appIdentifier": pak, //包名或其他约定的字段
                "callbackName": callbackName
            };
            console.log("nativeIsInstalledApp.....");
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeIsInstalledApp.postMessage(JSON.stringify(json));
            } else {
                window.android.nativeIsInstalledApp(JSON.stringify(json))
            }
        },
        //打开android应用
        nativeOpenApp(callbackName) {
            // let _os = isClient();
            let json = {
                "pkg": "com.xinxiaoyao.blackrhino",
                "schema": "",
                "callbackName": callbackName
            };
            window.android.nativeOpenApp(JSON.stringify(json))
        },
        //iso打开犀牛会员app
        nativeOpenBrowser(url = "FSVipApp://") {
            let json = { url };
            window.webkit.messageHandlers.nativeOpenBrowser.postMessage(JSON.stringify(json));
        },
        //推手显示引导
        nativeShowAppHomeGuide() {
            let _os = isClient();
            if (_os === "ios") {
                window.webkit.messageHandlers.nativeShowAppHomeGuide.postMessage("");
            } else {
                window.android.nativeShowAppHomeGuide()
            }
        }
    }
});

import store from '../store/index.js';
import reward from "../router/reward";
import shop from "../router/shop";

const routes = [
    ...applyRecord,
    ...machinesToolsApply,
    ...creditCard,
    ...register,
    ...reward,
    ...shop
]

// console.log(routes);

Vue.use(VueRouter);
const router = new VueRouter({
    base: __dirname,
    routes
}); //这里可以带有路由器的配置参数

router.beforeEach((to, from, next) => {

    let toPath = to.name;

    Bus.$emit('isLoading', true);

    if (toPath == "share") {
        document.body.setAttribute("style", "background:#757a9b");
    } else if (toPath == "upgradeRight") {
        document.body.setAttribute("style", "background:#f4f5fb");
    } else if (toPath == "upgradeRight_ad") {
        document.body.setAttribute("style", "background:#4082fc");
    } else if (toPath == "question_detail" || toPath == "regist_agreement" || toPath == "material_detail" || toPath == "downloadCenter" || toPath == "totalIncome" || toPath == "totalIncomeDetail" || toPath == "myTeam" || toPath == "myTeamDetail" || toPath == "payResultXYTS" || toPath == "giftBag" || toPath == "handBagDetail") {
        document.body.setAttribute("style", "background:#ffffff");
    } else if (toPath == "feedBack" || toPath == "feedBack_service" || toPath == "feedBack_help" || toPath == "mall" || toPath == "mall_order" || toPath == "addressList") {
        document.body.setAttribute("style", "background:#f4f5fb");
    } else if (toPath == "youshua") {
        document.body.setAttribute("style", "background:#5C64DC");
    } else if (toPath == "hebao") {
        document.body.setAttribute("style", "background:#FF5431");
    } else if (toPath == "xinghui" || toPath == "pusher") {
        document.body.setAttribute("style", "background:#3494FF");
    } else if (toPath == "applyLoan") {
        document.body.setAttribute("style", "background:#fafbff");
    } else if (toPath == "mallShop" || toPath == "caseDetail" || toPath == "caseDetail" || toPath == "policy" || toPath == "policyDetail" ||
        toPath == "recoLoan" || toPath == "regZfy" || toPath == "cxReg" || toPath == "video" || toPath == "dayShare" || toPath == "rewardCenter" ||
        toPath == "regZdyqSuccess" || toPath == "tradeVolume" || toPath == "myMachines" ||
        toPath == "getRecords" || toPath == "conTransfer" || toPath == "transRecords" || toPath == "yszhsft" || toPath == "addAdress" || toPath == "adressList" || toPath == "editorAdress") {
        document.body.setAttribute("style", "background:#ffffff");
    } else if (toPath == "crabShop" || toPath == "register" || toPath == "changePending" || toPath == "changeAgreed" || toPath == "changeRejected") {
        document.body.setAttribute("style", "background:#f6f6f6");
    } else if (toPath == "applyRecord") {
        document.body.setAttribute("style", "background:#F7F8FA");
    }else {
        document.body.setAttribute("style", "background:#f9faff");
    }

    document.title = to.meta.title;

    next();

});

router.afterEach((to, from) => {
    Bus.$emit('isLoading', false);
})

// 路由器会创建一个 App 实例，并且挂载到选择符 #app 匹配的元素上。
const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
