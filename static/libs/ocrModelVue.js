import api from "@/api.config.js";
import axios from 'axios';
import qs from 'qs';
import {loading,toast} from "@/libs/wyl.js";

//封装ocr识别；
export const ocrModel = function(options, success, error){
	// options : imgUrl, ocrType, resultType
    axios.defaults.timeout = 30000;
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

    //添加一个请求拦截器
    axios.interceptors.request.use(function(config){
        loading("show");
        return config;
    },function(err){
        return Promise.reject(err);
    });

    let defaults = {
        url:'',
        method:'post',
        dataType:'json',
        responseType:'json',
        data:{}
    };

    options = Object.assign({},defaults,options);

    let dataJson = Object.assign(options.data);

    axios({
        method: 'post',
        url: 'https://ocr.cardinfo.com.cn/ocr/ocrBase64',
        dataType:'json',
        responseType:'json',
        data:  qs.stringify(dataJson),
        withCredentials: false
    })
    .then((response)=>{
        loading("hide");
        console.log(response);
        if(response.data.message.value=="识别成功"){
            if(typeof success=="function"){
                success(response.data);
            }
        }else{
            toast({
                content:response.message.value
            })
        }
        
    })
    .catch((error)=>{

        loading("hide");
        if(error.code === 'ECONNABORTED' && error.message.indexOf('timeout') !== -1) {
            toast({
                content:"接口请求超时，请稍后重试"
            })
            return;
        }

        if(typeof error=="function"){
            error(response)
        }
        
    })
};
