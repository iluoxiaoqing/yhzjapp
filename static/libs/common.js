import api from "@/api.config.js";
import md5 from "md5";
import base from "@/libs/base";
import CryptoJS from "crypto-js";

// import JSEncrypt from 'jsencrypt';

//是否走网关
let isWay = api.isWay;

export const bodyScroll = function(event) {
    event.preventDefault();
}

export const preventScroll = function() {
    document.body.addEventListener('touchmove', bodyScroll, false);
    document.body.style.cssText = "position:fixed";
}

export const addScroll = function() {
    document.body.removeEventListener('touchmove', bodyScroll, false);
    document.body.style.cssText = "position:initial";
}

export const youmeng = function(id, text) {
    try {
        _czc.push(["_trackEvent", id, text]);
        console.log(id + "|" + text)
    } catch (e) {
        console.log(e);
    }
}

export const isWeiXin = function() {
    var ua = window.navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == 'micromessenger') {
        return true;
    } else {
        return false;
    }
}

export const siblingElem = function(elem) {
    var _nodes = [],
        _elem = elem;
    while ((elem = elem.previousSibling)) {
        if (elem.nodeType === 1) {
            _nodes.push(elem);
            break;
        }
    }

    elem = _elem;
    while ((elem = elem.nextSibling)) {
        if (elem.nodeType === 1) {
            _nodes.push(elem);
            break;
        }
    }

    return _nodes;
}

export const timestampToTime = function(timestamp) {
    var date = new Date(timestamp);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y + M + D + h + m + s;
}

export const trim = function(str) {
    return str.replace(/\s*/g, '');
}

export const toast = function(params) {
    var Height = document.documentElement.clientHeight || document.body.clientHeight;

    var toastMask = document.createElement("div");
    var el = document.createElement("div");

    toastMask.setAttribute("class", "toastMask");
    el.setAttribute("id", "toast");
    toastMask.appendChild(el);

    el.innerHTML = params.content;

    document.body.appendChild(toastMask);
    // document.body.style.height = Height + "px";

    toastMask.classList.add("fadeIn");
    setTimeout(function() {
        toastMask.classList.remove("fadeIn");
        toastMask.classList.add("fadeOut");
        //el.addEventListener("webkitAnimationEnd", function(){
        toastMask.classList.add("hide");
        document.body.removeChild(toastMask);
        // document.body.style.height = "";
        //});

    }, params.time || 2000);
}

export const alertBox = function() {
    let oWrap = document.getElementsByClassName("wrap")[0];
    let mask = document.createElement("div");
    let box = document.createElement("div");

    mask.className = "alertBox_mask";
    box.className = "alertBox";
    // oWrap.removeChild(mask);
    // oWrap.removeChild(box);

    oWrap.appendChild(mask);
    oWrap.appendChild(box);
}


export const jsonToArray = function(json) {

    var arr = []
    for (let i in json) {

        var baseType = "";
        var toString = "";

        try {
            baseType = JSON.parse(json[i]);
        } catch (e) {
            baseType = json[i]
        }

        if (typeof baseType == "object") {
            toString = '{"' + i + '":' + json[i] + '}';
        } else {
            toString = '{"' + i + '":"' + json[i] + '"}';
        }

        var toJson = JSON.parse(toString);
        arr.push(toJson);
    }

    return arr;

}

export const useMd5 = function(array) {

    var signKey = '86eb1492-6e08-481d-b377-678acd5c3de5';

    var resultString = "";

    if (array.length > 0) {

        var parmas = [];
        var resultArray = [];

        for (let i = 0; i < array.length; i++) {
            for (let key in array[i]) {
                parmas.push(key);
            }
        }

        for (let i = 0; i < parmas.sort(function(a, b) {
                if (a.toString().toLowerCase() > b.toString().toLowerCase()) {
                    return 1;
                } else {
                    return -1;
                }
            }).length; i++) {
            for (let j = 0; j < array.length; j++) {

                //console.log(array[j])

                for (let key in array[j]) {
                    if (parmas[i] == key) {

                        if (Array.isArray(array[j][key])) {
                            array[j][key] = JSON.stringify(array[j][key])
                        }
                        resultArray.push(array[j]);

                    }
                }
            }
        }

        resultArray.map(function(el, index) {
            for (let key in el) {
                resultString += (key + "=" + el[key]);
            }
        });

    }

    return md5(resultString + signKey);
}

export const loading = function(state, text) {

    let domLength = document.getElementsByClassName('loadingBg').length;
    let loadingBg = document.createElement("div");
    let loading = document.createElement("div");
    let img = document.createElement("img");
    let p = document.createElement("p");

    loadingBg.className = "loadingBg";
    loading.className = "loading";
    img.src = "image/icon_loading.png";
    p.innerHTML = text || "数据加载中";

    loading.appendChild(img);
    loading.appendChild(p);
    loadingBg.appendChild(loading);
    if (domLength == 0) {
        document.body.appendChild(loadingBg);
    }

    if (state == "show") {
        document.getElementsByClassName('loadingBg')[0].style.display = "block";
    } else if (state == "hide") {

        //console.log( loadingBg )

        document.getElementsByClassName('loadingBg')[0].style.display = "none";
        // document.removeEventListener("touchmove",function(e){
        //     //e.preventDefault();
        // });
    }
}

export const isClient = function() {
    var u = navigator.userAgent;
    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端;

    if (isAndroid) {
        return "android";
    } else if (isiOS) {
        return "ios";
    }
};

export const getUrlParam = function(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substring(1).match(reg); //匹配目标参数
    if (r !== null) return decodeURI(r[2]);
    return ''; //返回参数值
};

// 参数过滤函数
export const filterNull = function(o) {
    for (var key in o) {
        if (o[key] === null) {
            delete o[key]
        }
        if (toType(o[key]) === 'string') {
            o[key] = o[key].trim()
        } else if (toType(o[key]) === 'object') {
            o[key] = filterNull(o[key])
        } else if (toType(o[key]) === 'array') {
            o[key] = filterNull(o[key])
        }
    }
    return o
}

export const getLoginKey = function() {

    let nativeJson = {};
    let loginKey = "";

    if (isClient() == "ios") {
        try {
            loginKey = window.webkit.messageHandlers.getLoginKey.postMessage("获取loginkey");
            console.log("调用了getLoginkey:" + loginKey);
        } catch (e) {
            loginKey = getCookie("LOGINKEY");
        }
        // loginKey = getCookie("LOGINKEY");
    } else {
        try {
            nativeJson = JSON.parse(window.android.getLoginKey());
            loginKey = nativeJson.loginKey;
            console.log("调用了getLoginkey:" + loginKey);
        } catch (e) {
            loginKey = getCookie("LOGINKEY");
        }
    }

    return loginKey || "";

}


export const getUserNo = function() {

    let userNo = "";


    if (isClient() == "ios") {

        userNo = getCookie("USERNO");

    } else {
        try {
            let nativeJson = JSON.parse(window.android.getLoginKey());
            userNo = nativeJson.userNo;
            console.log("调用了userNo:" + userNo);
        } catch (e) {
            userNo = getCookie("USERNO");
        }
    }

    return userNo || "";

}

export const Ajax = function(options, success, failed) {

    let osType = isClient() == "ios" ? "IOS" : "ANDROID";

    // setCookie("DEVICETYPE","1");
    // setCookie("APPCODE","HM_PARTNER");
    // setCookie("VERSION","1.0.0");
    //setCookie("LOGINKEY","00047c5dd5c1323e727d8a5ac0fe5b41");

    let defaults = {
        url: '',
        type: 'POST',
        dataType: 'json',
        responseType: 'json',
        data: {},
        showLoading: true
    };

    options = Object.assign({}, defaults, options);

    let dataJson = Object.assign({
        "osType": osType,
        "deviceType": getCookie("DEVICETYPE") || "test",
        "appCode": getCookie("APPCODE") || "test",
        "version": getCookie("VERSION") || "1.0.0",
        "loginKey": getLoginKey()
    }, options.data);

    let signData = { "sign": useMd5(jsonToArray(dataJson)) };
    let ajaxData = Object.assign({}, dataJson, signData);

    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP')
    }

    let type = options.type.toUpperCase();
    // 用于清除缓存
    let random = Math.random();

    if (typeof ajaxData == 'object') {
        let str = '';
        for (let key in ajaxData) {
            str += key + '=' + ajaxData[key] + '&';
        }
        ajaxData = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (ajaxData) {
            xhr.open('GET', options.url + '?' + ajaxData, options.sync === false ? false : true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, options.sync === false ? false : true);
        }
        xhr.send();
        if (options.showLoading) {
            loading("show");
        }
        // loading("show");

    } else if (type == 'POST') {
        xhr.open('POST', options.url, options.sync === false ? false : true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.setRequestHeader("traditional", true);
        xhr.traditional = true;

        xhr.send(ajaxData);

        if (options.showLoading) {
            loading("show");
        }
    }

    // 处理返回数据
    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                if (options.showLoading) {
                    loading("hide");
                }
                let res = JSON.parse(xhr.response);
                // console.log( res )
                if (res.code == "0000") {
                    if (typeof success == "function") {
                        success(res);
                    }
                } else {
                    console.log("222222222", res.code);
                    toast({
                        "content": res.msg
                    });
                    if (typeof failed == "function") {
                        failed(res);
                    }
                }

            } else {
                if (options.showLoading) {
                    loading("hide");
                }
                toast({
                    "content": "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function() {
        if (options.showLoading) {
            loading("hide");
        }
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    }
}


export const AjaxMerchant = function(options, success, failed) {

    let osType = isClient() == "ios" ? "IOS" : "ANDROID";

    // setCookie("DEVICETYPE","1");
    // setCookie("APPCODE","HM_PARTNER");
    // setCookie("VERSION","1.0.0");
    //setCookie("LOGINKEY","00047c5dd5c1323e727d8a5ac0fe5b41");

    let defaults = {
        url: '',
        type: 'POST',
        dataType: 'json',
        responseType: 'json',
        data: {},
        showLoading: true
    };

    options = Object.assign({}, defaults, options);

    let dataJson = Object.assign({
        "osType": osType,
        "deviceType": getCookie("DEVICETYPE") || "test",
        "appCode": getCookie("APPCODE") || "test",
        "version": getCookie("VERSION") || "1.0.0",
        "loginKey": getLoginKey()
    }, options.data);

    let signData = { "sign": useMd5(jsonToArray(dataJson)) };
    let ajaxData = Object.assign({}, dataJson, signData);

    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP')
    }

    let type = options.type.toUpperCase();
    // 用于清除缓存
    let random = Math.random();

    if (typeof ajaxData == 'object') {
        let str = '';
        for (let key in ajaxData) {
            str += key + '=' + ajaxData[key] + '&';
        }
        ajaxData = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (ajaxData) {
            xhr.open('GET', options.url + '?' + ajaxData, options.sync === false ? false : true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, options.sync === false ? false : true);
        }
        xhr.send();
        if (options.showLoading) {
            loading("show");
        }
        // loading("show");

    } else if (type == 'POST') {
        xhr.open('POST', options.url, options.sync === false ? false : true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // xhr.setRequestHeader("traditional", true);
        xhr.traditional = true;

        xhr.send(ajaxData);

        if (options.showLoading) {
            loading("show");
        }
    }

    // 处理返回数据
    xhr.onreadystatechange = function() {

        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                if (options.showLoading) {
                    loading("hide");
                }
                let res = JSON.parse(xhr.response);
                // return res;
                // console.log("res====", res)
                // if (res.code == "0000") {
                if (typeof success == "function") {
                    success(res);
                }
                // } else {
                //     console.log("222222222", res.code);
                //     toast({
                //         "content": res.msg
                //     });
                //     if (typeof failed == "function") {
                //         failed(res);
                //     }
                // }

            } else {
                if (options.showLoading) {
                    loading("hide");
                }
                toast({
                    "content": "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function() {
        if (options.showLoading) {
            loading("hide");
        }
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    }
}


//DES 加密
export const encryptByAES = function(ciphertext, key) {
        var keyHex = CryptoJS.enc.Hex.parse(key);
        var encrypted = CryptoJS.AES.encrypt(ciphertext, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.ciphertext.toString().toUpperCase();
    }
    //AES 解密
export const decryptByAES = function(ciphertext, key) {
    var keyHex = CryptoJS.enc.Hex.parse(key);
    // direct decrypt ciphertext
    var decrypted = CryptoJS.AES.decrypt({
        ciphertext: CryptoJS.enc.Hex.parse(ciphertext)
    }, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}



//公钥加密
export const encryptByRSA = function(source, key) {
    let encrypt = new JSEncrypt();
    // key=publick_key;
    encrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + hex2b64(key) + '-----END PUBLIC KEY-----');
    // console.log("hex2b64(key):::"+hex2b64(key));
    let result = encrypt.encrypt(source);
    result = b64tohex(result);
    // console.log('加密结果######################',result);
    return result;
};

//公钥解密
export const decryptpByRSA = function(encrypted, key) {
    // console.log("公钥解密...");
    // debugger
    let decrypt = new JSEncrypt();
    decrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + hex2b64(key) + '-----END PUBLIC KEY-----');
    let str = decrypt.decryptp(encrypted);
    // console.log('解密结果:::',str);
    return str;
};

//这个方法纯为获取token使用的
export const AjaxToken = function(options, success) {
    let defaults = {
        url: '',
        type: 'POST',
        data: {},
        ContentType: "application/x-www-form-urlencoded",
    };
    options = Object.assign({}, defaults, options);
    let dataJson = {};
    let encryptMsg = ""

    // 创建ajax对象
    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP')
    }

    let type = options.type.toUpperCase();
    // 用于清除缓存
    let random = Math.random();
    dataJson = options.data;
    if (typeof dataJson == 'object') {
        let str = '';
        for (let key in dataJson) {
            str += key + '=' + encodeURIComponent(dataJson[key]) + '&';
        }
        dataJson = str.replace(/&$/, '');
    }

    if (type == 'GET') {
        if (dataJson) {
            xhr.open('GET', options.url + '?' + dataJson, true);
        } else {
            xhr.open('GET', options.url + '?t=' + random, true);
        }

        xhr.setRequestHeader("Content-type", options.ContentType);

        xhr.send();
        loading("show");

    } else if (type == 'POST') {
        xhr.open('POST', options.url, true);
        xhr.setRequestHeader("Content-type", options.ContentType);
        xhr.send(dataJson);
        loading("show");
    }

    // 处理返回数据
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {

                loading("hide");
                let res = JSON.parse(xhr.response);
                if (typeof success == "function") {
                    success(res);
                }
            } else {
                if (xhr.status == 400) {
                    let res = JSON.parse(xhr.response);
                    if (res.error_description) {
                        let content = getwayErrorMsg(res.error_description);
                        loading("hide");
                        toast({
                            content
                        });
                    } else {
                        loading("hide");
                        toast({
                            content: "服务器开小差啦~"
                        });
                    }
                } else {

                    loading("hide");
                    toast({
                        content: "服务器开小差啦~"
                    });
                }

            }
        }
    };

    xhr.ontimeout = function() {
        loading("hide");
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    }
};

function getwayErrorMsg(code) {
    switch (code) {
        case "0001":
            return "未登录";
        case "0002":
            return "未绑定手机号";
        case "0003":
            return "验证码错误";
        case "0004":
            return "手机号已被绑定";
        case "0005":
            return "参数错误";
        case "0006":
            return "手机号格式错误";
        case "0007":
            return "接入方未绑定";
        case "0008":
            return "已绑定";
        case "0009":
            return "集团接口异常";
        case "9999":
            return "请求失败";
        default:
            return "未知错误"
    }
}

//犀牛ajax
export const XNAjax = function(options, success, failed) {

    let access_token = "";
    let aesKey = "";
    let uuid = "";
    // if(isClient()=="ios"){
    let sesUUid = sessionStorage.getItem("UUID");
    if (options.sesstionType !== "true") {
        uuid = getCookie("UUID");
        access_token = getCookie("ACCESSTOKEN");
        aesKey = getCookie("AESKEY");
    } else {
        uuid = sessionStorage.getItem("UUID");
        access_token = sessionStorage.getItem("ACCESSTOKEN");
        aesKey = sessionStorage.getItem("AESKEY");
    }
    console.log("access_token:" + access_token + "| aesKey:" + aesKey);
    let defaults = {
        url: '',
        type: 'POST',
        data: {},
        ContentType: "application/x-www-form-urlencoded",
    };
    options = Object.assign({}, defaults, options);
    let dataJson = {};
    let encryptMsg = "";
    console.log(options.url);
    // 创建ajax对象
    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject('Microsoft.XMLHTTP')
    }
    let type = options.type.toUpperCase();
    // 用于清除缓存
    let random = Math.random();

    if (isWay) { //是否走网关
        encryptMsg = encryptByAES(JSON.stringify(options.data), aesKey);
        dataJson = Object.assign({}, options.data, {
            "access_token": access_token,
            "uuid": uuid,
            "msg": encodeURIComponent(encryptMsg)
        });
    } else {
        dataJson = Object.assign({}, options.data, {
            "uuid": uuid
        });
    }
    if (typeof dataJson == 'object') {
        let str = '';
        for (let key in dataJson) {
            str += key + '=' + dataJson[key] + '&';
        }
        dataJson = str.replace(/&$/, '');
    }
    if (type === 'GET') {
        if (dataJson) {
            xhr.open('GET', options.url + (options.url.indexOf('?') > -1 ? '&' : '?') + dataJson, true);
        } else {
            xhr.open('GET', options.url + (options.url.indexOf('?') > -1 ? '&' : '?') + 't=' + random, true);
        }
        xhr.setRequestHeader("Content-type", options.ContentType);
        xhr.send();

        loading("show");
    } else if (type === 'POST') {
        console.log('333');
        console.log(dataJson);
        xhr.open('POST', options.url, true);
        xhr.setRequestHeader("Content-type", options.ContentType);
        console.log(dataJson);
        xhr.send(dataJson);
        loading("show");
    }
    // 处理返回数据
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                loading("hide");
                let res = {};
                if (isWay) { //是否走网关
                    res = JSON.parse(decryptByAES(xhr.response, aesKey));
                } else {
                    res = JSON.parse(xhr.response);
                }
                // let res = JSON.parse(decryptByAES(xhr.response, aesKey));
                console.log("ajax返回", res);
                if (res.code == "0000") {
                    if (typeof success == "function") {
                        success(res);
                    }
                } else {
                    toast({
                        "content": res.msg
                    });
                    if (typeof failed == "function") {
                        failed(res);
                    }
                }
            } else {
                loading("hide");
                toast({
                    content: "服务器开小差啦~"
                });
            }
        }
    };

    xhr.ontimeout = function() {
        loading("hide");
        toast({
            "content": "接口请求超时，请稍后重试"
        });
    }
};

export const Jsonp = function(url, config) {

    var data = config.data || [];
    var paraArr = [],
        paraString = ''; //get请求的参数。
    var urlArr;
    var callbackName; //每个回调函数一个名字。按时间戳。
    var script, head; //要生成script标签。head标签。
    var supportLoad; //是否支持 onload。是针对IE的兼容处理。
    var onEvent; //onload或onreadystatechange事件。
    var timeout = config.timeout || 0; //超时功能。

    for (var i in data) {
        if (data.hasOwnProperty(i)) {
            paraArr.push(encodeURIComponent(i) + "=" + encodeURIComponent(data[i]));
        }
    }

    urlArr = url.split("?"); //链接中原有的参数。
    if (urlArr.length > 1) {
        paraArr.push(urlArr[1]);
    }

    callbackName = 'callback';
    paraArr.push('callback=' + callbackName);
    paraString = paraArr.join("&");
    url = urlArr[0] + "?" + paraString;

    script = document.createElement("script");
    script.loaded = false;

    //将回调函数添加到全局。
    window[callbackName] = function(arg) {
        var callback = config.callback;
        callback(arg);
        script.loaded = true;
    }

    head = document.getElementsByTagName("head")[0];
    head.insertBefore(script, head.firstChild) //chrome下第二个参数不能为null
    script.src = url;

    supportLoad = "onload" in script;
    onEvent = supportLoad ? "onload" : "onreadystatechange";

    script[onEvent] = function() {

        if (script.readyState && script.readyState != "loaded") {
            return;
        }
        if (script.readyState == 'loaded' && script.loaded == false) {
            script.onerror();
            return;
        }
        //删除节点。
        (script.parentNode && script.parentNode.removeChild(script)) && (head.removeNode && head.removeNode(this));
        script = script[onEvent] = script.onerror = window[callbackName] = null;

    }

    script.onerror = function() {
        if (window[callbackName] == null) {
            console.log("请求超时，请重试！");
        }
        config.error && config.error(); //如果有专门的error方法的话，就调用。
        (script.parentNode && script.parentNode.removeChild(script)) && (head.removeNode && head.removeNode(this));
        script = script[onEvent] = script.onerror = window[callbackName] = null;
    }

    if (timeout != 0) {
        setTimeout(function() {
            if (script && script.loaded == false) {
                window[callbackName] = null; //超时，且未加载结束，注销函数
                script.onerror();
            }
        }, timeout);
    }

}

export const setCookie = function(name, value, Days) {
    if (Days === null || Days === '') {
        Days = 30;
    }
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + "; path=/;expires=" + exp.toGMTString();
}

export const getCookie = function(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return unescape(arr[2]);
    } else {
        return null;
    }
};

//让首写字母大写
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

/**
 * 批量返回验证方法 @heliang
 */
function generatorValidator() {
    let validator = {};
    for (let key in base) {
        if (typeof base[key] === 'object' && base[key].reg != null) {
            validator['check' + capitalize(key)] = function(str) {
                let errorMsg = "";
                if (str === '') {
                    errorMsg = base[key].empty;
                    return errorMsg;
                } else {
                    let reg = base[key].reg;
                    if (!reg.test(str)) {
                        errorMsg = base[key].error;
                        return errorMsg;
                    } else {
                        return errorMsg;
                    }
                }
            }
        }
    }
    return validator;
}

export const clearCookie = function(name) {
    setCookie(name, '', -1);
};

const validator = generatorValidator();

// console.log(validator);
export {
    validator
}

/**
 * 时间戳转时间
 * @param date Thu Nov 21 2019 14:57:05 GMT+0800 (中国标准时间)
 * @param fmt 格式：yyyy-MM-dd hh:mm:ss(年-月-日 时:分:秒) 注：- : 符号随便换
 * @returns {*} 返回日期格式
 */
export const commonResetDate = (date, fmt) => {
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };

    // 遍历这个对象
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            // console.log(`${k}`)
            let str = o[k] + '';
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
        }
    }

    function padLeftZero(str) {
        return ('00' + str).substr(str.length);
    }
    return fmt;
};
