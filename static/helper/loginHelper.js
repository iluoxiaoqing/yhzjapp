import  {getCookie,setCookie} from '@/libs/common';
import  getWayHelper from './getwayHelper';

let LoginHelper={
    //自动登录
    login(cb,phone) {
        let isLogin = getCookie('isLogin');
        //判断是否登录
        if (isLogin==="false"||isLogin==null) {
            debugger
            //进来先匿名
            getWayHelper.postToken(null, null, () => {
                //获取用户手机号
                console.log("postToken.....");
                // let phone = getCookie('phoneNo')||getCookie('PHONENO');
                // if(outPhone){ //返回的手机号登录
                //     phone=outPhone;
                // }
                console.log('phone',phone);
                //发送登录请求
                getWayHelper.postToken(phone, '123456', () => {
                    setCookie('isLogin', true);
                    //登录后回调
                    cb();
                });
            });
        }else{
            cb();
        }
    },
    //匿名登录
    anonymousLogin(cb){
        getWayHelper.postToken(null,null,cb);
    }

};

export default  LoginHelper;