import * as common from "@/libs/common";
import api from "@/api.config.js";
import {getCookie} from "@/libs/common";

import {toast} from "@/libs/common";

//游客身份下要求username为固定前缀USER:加32位设备唯一号，
//例如USER:BDAF6B4D-5DC0-4AEF-BCF8-6C7EFC94DE97，password为固定字符串TOURIST
//密码(游客)
const PASSWORD = "TOURIST";
const  NO_SMS="NO_SMS"; //无需验证码
const  PHONE="phone"; //手机号需要验证码
const  PREFIX=PHONE;

let {tokenConfig}=api;
// 权限控制
const SCOPE = tokenConfig.scope ;// "select";
//授权模式
const GRANT_TYPE = tokenConfig.grantTypes; //"password";
//客户端标识
const CLIENT_ID =tokenConfig.clientId;// "client_youshua";
//认证客户端标识
const CLIENT_SECRET =tokenConfig.clientSecret;   // "5274206178d44e5cad27f6af21cf50dd";
const BUSITYPE =tokenConfig.requestBusiType; // "member";

//定义公钥
// const publicKey="305C300D06092A864886F70D0101010500034B003048024100AEF4C234230F97DB2E64E01B0904946F5F0FF041AF38834AFBAC83E7D6B8047979EF2FE6F587228075D6061539409B5FA6E5316CAECE01040CA11AC3C58AC3FD0203010001";
let getWay = {};
getWay.getAuthParams = function (uuid, password) {
    let tempParams = {
        username: uuid,
        password: password != null ? password : PASSWORD
    };
    if (tempParams.username.indexOf(PHONE) > -1||tempParams.username.indexOf(NO_SMS) > -1) {
        tempParams.loginType = tempParams.username.split(":")[0]; //phone:手机号 拆成两个参数
        tempParams.phoneNo = tempParams.username.split(":")[1];
        tempParams.smsCode = tempParams.password;
        delete tempParams.password;
    }
    let msg = JSON.stringify(tempParams);
    let params = {
        busiType: BUSITYPE,
        username: uuid,
        password: password != null ? password : PASSWORD,
        client_secret: CLIENT_SECRET,
        msg
    };
    // window.params = params;
    return params;
};

function parseUrl(str) {
    let reg = /([^=&]*)=([^=&]*)/g;
    let obj = {};
    str.replace(reg, function () {
        let key = arguments[1];
        let val = arguments[2];
        obj[key] = val;
    });
    return obj;
}

function getwayErrorMsg(code) {
    switch (code) {
        case "0001":
            return "未登录";
        case "0002":
            return "未绑定手机号";
        case "0003":
            return "验证码错误";
        case "0004":
            return "手机号已被绑定";
        case "0005":
            return "参数错误";
        case "0006":
            return "手机号格式错误";
        case "0007":
            return "接入方未绑定";
        case "0008":
            return "已绑定";
        case "0009":
            return "集团接口异常";
        case "9999":
            return "请求失败";
        default:
            return "未知错误"
    }
}

//解密token等信息
getWay.decipheringToken = function (decipheringJson) {
    //解密
    let data=decipheringJson;
    let tempData={};
    for(let key in data){
        if(key==='access_token'||key==='refresh_token'||key==='fault'){
            tempData[key]=common.decryptpByRSA(data[key],api.tokenConfig.publicKey);
        }else{
            tempData[key]=data[key];
        }
    }
    data=tempData;
    // debugger
    console.log("tempData",tempData);
    // let jsonStr=common.decryptpByRSA(decipheringStr,api.tokenConfig.publicKey);
    // let data = JSON.parse(jsonStr);
    window.data=data;
    console.log("解密后数据", data);
    let loginType = common.getCookie('LoginType')||sessionStorage.getItem('LoginType');

    //解密后保存到本地
    common.setCookie("ACCESSTOKEN", common.encryptByAES(data.access_token, data.fault));
    sessionStorage.setItem("ACCESSTOKEN", common.encryptByAES(data.access_token, data.fault));
    //设置为未登录
    common.setCookie("isLogin", false);   //loginType === 'phone' ? true : false 这里默认先写死 之后要判断登录和失效 和退出
    sessionStorage.setItem("isLogin",false);
    // console.log(",data.access_token"+data.access_token)
    common.setCookie("AESKEY", data.fault);
    sessionStorage.setItem("AESKEY",data.fault);

    // alert(data.access_token);

    common.setCookie("REFRESH_TOKEN", common.encryptByAES(data.refresh_token, data.fault));
    sessionStorage.setItem("REFRESH_TOKEN",common.encryptByAES(data.refresh_token, data.fault));



    //执行回调
    if (window.cb && typeof window.cb === 'function') {
        console.log('执行回调####');
        // setTimeout(() => {
        window.cb(); //执行回调
        // }, 1000)
    }
    if(loginType===PHONE||loginType===NO_SMS){
        //获取用户信息
        common.XNAjax({
            "url": api.KY_USER_IP + "/resource/vip-user/user/getLoginData",
            "type": "post",
            "data": {}
        }, (res) => {
            // console.log(res);
            let {data, code} = res;
            if (code === '0000') {
                common.setCookie("UUID", data.uuid);
                sessionStorage.setItem("UUID", data.uuid);
                common.setCookie("isLogin", true);
                sessionStorage.setItem("isLogin", true);
            }
        });
    }
};


getWay.getEncryptObjByRSA = function (encryptParams) {
    // console.log("APP返回..加密后返回....",encryptParams);
    let params = encryptParams;
    params.grant_type = GRANT_TYPE; //password
    params.scope = SCOPE; //写死 select
    params.client_id = CLIENT_ID; //明文
    console.log("params",params);
    common.AjaxToken({
        "url": api.KY_USER_IP + "/oauth/token",
        "type": "POST",
        // "sesstionType":"true",
        "data": params
    }, (data) => {
        //开始解密调用原生方法
        getWay.decipheringToken(data);
    });
}

//向原生交互获取游客或正式用户token
getWay.postToken = function (phone, code, cb) {
    console.log("postToken...");
    let uuid = "USER:111111111111111111111111111111111";
    let loginType = "user";
    if (phone) {
        if(code==="any"){//无需验证码登录
            uuid = NO_SMS+":" + phone;
            loginType =NO_SMS;
        }else{
            uuid = PHONE+":" + phone;
            loginType = PHONE
        }
    }
    common.setCookie("LoginType", loginType);
    window.sessionStorage.setItem("LoginType", loginType);
    //全局注册回调函数
    console.log("loginType:::::" + loginType);
    if (cb && typeof cb === 'function') {
        console.log('注册函数...');
        window.cb = cb;
    }
    //获取授权需要加密的信息
    let params = this.getAuthParams(uuid, code);
    // console.log("加密前params:::::",params);
    let encryptParams={};
    for(let key in params){
        encryptParams[key]=common.encryptByRSA(params[key],api.tokenConfig.publicKey);
    }
    this.getEncryptObjByRSA(encryptParams);
};
export default getWay;
