//H5调用支付宝

import * as common from "@/libs/common";
import api from "@/api.config";

let Pay = {};

//h5支付宝下单
Pay.h5Alipay = function (amount, payment, productCode, productType, extParam, extFn) {
    let uuid = common.getCookie("xnUUID");
    let {outOrderNo, notifyParam} = extParam == null ? {} : extParam;
    // console.log("outOrderNo::::", outOrderNo);
    //获取token
    this.getOrderToken(uuid).then((token) => {
        // console.log('下单token:'+token);
        //开始请求真正下单
        let params = {
            amount,
            payment,
            productCode,
            productType,
            uuid,
            token
        };
        if (outOrderNo) {
            params["outOrderNo"] = outOrderNo;
        }
        if (notifyParam) {
            params["notifyParam"] = notifyParam;
        }
        console.log("JSON::::", extParam)
        let isOne = extFn != null ? (extFn.isOne === null ? false : true) : false;
        this.alipay(params, isOne).then((res) => {
            let {code, msg, data} = res;
            if (code === '0000') {
                let html = data;
                console.log('html::::' + html);
                let form = html.split('<script')[0];
                let myFormContainer = document.createElement("div");
                let body = document.body;
                myFormContainer.style.display = "none";
                myFormContainer.innerHTML = form;
                body.appendChild(myFormContainer);
                //通过扩展参数的形式设置友盟统计
                extFn != null && typeof extFn === 'function' ? extFn.success() : null;
                document.forms[0].submit();
                // console.log('返回消息::::'+form);
            } else {
                console.log("res::::", res);
                //需要调用检查订单接口 看一下订单是否已经生成 未生成则表示失败

                common.toast({
                    content: msg
                });
                extFn != null && typeof extFn === 'function' ? extFn.fail(msg) : null;
            }
        });
    }).catch((err) => {
        console.log("err:::" + err);
        common.toast({
            content: err
        });
        //查询订单状态如果有
        if (outOrderNo) {
            this.queryOrder(outOrderNo).then((res) => {
                let {order_status} = res;
                if (order_status === "SUCCESS") { //订单存在 不执行失败回调
                    // extFn!=null&&typeof extFn==='function' ? extFn.fail("系统错误"):null;
                }
            }).catch((err) => {
                //订单不存在
                extFn != null && typeof extFn === 'function' ? extFn.fail("订单不存在") : null;
            })
        } else {
            extFn != null && typeof extFn === 'function' ? extFn.fail("系统错误") : null;
        }

    })
};

Pay.queryOrder = function (orderNo) {
    return new Promise((resolve, reject) => {
        common.Ajax({
            "url": api.KY_IP + `/resource/vip-user/order/query`,
            "type": "post",
            "data": {orderNo}
        }, (res) => {
            let {code, msg, data} = res;
            resolve(res);
        }, function (err) {
            console.log('err====', err);
            reject(err);
        });
    })
};

//请求支付宝
Pay.alipay = function (params, isOne) { //isOne 是否一元购
    return new Promise((resolve, reject) => {
        let urlAction = "h5";
        if (isOne) {
            urlAction = "activityH5"
        }
        common.Ajax({
            "url": api.KY_IP + `/resource/vip-user/order/create/${urlAction}`,
            "type": "post",
            "data": params
        }, (res) => {
            let {code, msg, data} = res;

            resolve(res);

        }, function (err) {
            console.log('err====', err);
            reject(err);
        });
    })
};


//获取下单token
Pay.getOrderToken = function () {
    return new Promise((resolve, reject) => {
        common.Ajax({
            "url": api.KY_IP + `/resource/vip-user/order/token`,
            "type": "get",
            "data": {}
        }, (res) => {
            //返回的数据是加密的所以需要调用原生app解密
            console.log(res);
            let {code, data, msg} = res;
            if (code === '0000') {
                resolve(data)
            } else {
                console.log(JSON.stringify(res));
                reject(msg);
            }
        }, function (err) {
            console.log('err====', err);
            reject(err);
        });
    })
};

export default Pay;