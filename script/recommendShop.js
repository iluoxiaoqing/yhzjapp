import api from "@/api.config.js";
import * as common from "@/libs/common";

export default{
    data(){
        return{
            bussGroups:[],
            business:{},
            imgUrl1:api.KY_IP,
            bannerList:[],
            mainCode:"",
            mainCodeName:"",
            imgUrl:""
        }
    },
    mounted(){
        common.Ajax({
            "url":api.KY_IP+"bussMenu/appBusiness",
            "data":{
                "buss":"SHOP_CODE"
            }
        },(data)=>{
            
            this.business = data.data.business;
            this.bussGroups = data.data.groups;
            this.mainCode = data.data.business.productCode;
            this.mainCodeName = data.data.business.productName;
            data.data.groups.map((el)=>{
                el.products.map((value)=>{
                    let userDesc = value.userDesc||"";
                    this.$set(value,"userDesc",userDesc?JSON.parse(userDesc):{})
                });
            })
        });

        common.Ajax({
            "url":api.KY_IP+"wonderfulActivityInfo/findPageBanner",
            "data":{
                "bannerType":"SHOP_CODE"
            }
        },(data)=>{
            console.log(data.data)
            this.bannerList=data.data;
            this.imgUrl=data.data.imgUrl;
            this.$nextTick(this.mySwiper)
        });

        common.youmeng("推荐开店","进入推荐开店页面");
    },
    methods:{
        gotoShare(j){

            if(!j.isShow){
                return;
            }

            this.$router.push({
                "path":"share",
                "query":{
                    "product":j.productCode,
                    "productName":j.productName,
                    "channel":"recommendShop"
                }
            });

            common.youmeng("推荐开店","点击"+j.productName);

        },
        gotoMall(){
            this.$router.push({
                "path":"mall"
            });
            common.youmeng("推荐开店","点击我要备货");
        },
        mySwiper(){
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: 2000,//可选选项，自动滑动
                loop:true,
                pagination:".swiper-pagination"
            });
        }
    }
}