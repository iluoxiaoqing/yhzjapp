const address =  require("address");

var server  = {
    port:8800,
    noInfo: true,
    contentBase:"./app/",
    host:address.ip()
}

console.log( "http://" + server.host +":"+ server.port )

module.exports = server;